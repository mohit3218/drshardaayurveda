<?php

/**
* Constants
* 
* @created    08/04/2020
* @package    Dr Sharda Ayurveda
* @copyright  Copyright (C) 2020
* @license    Proprietary
* @author     Mohit Thakur
*/

//Pagination Limit
define('PAGINATION_LIMIT', 50);
//Image Path
define('GRAPHIC_IMAGES', 'uploads/posts/');
define('PAGE_IMAGES', 'uploads/pages/');

//Post Category
define('Endocrine', 1);
define('Digestive', 2);
define('Skin', 3);
define('Joint Pain', 4);
define('Respiratory', 5);
define('Sexual Disorders', 6);
define('De Addiction', 7);
define('Kidney', 8);
define('Neurological', 9);
define('Gynae', 10);
define('Circulatory', 11);
define('Liver', 12);

//Site Name
define('SITE_NAME', ' | Dr Sharda Ayurveda');

//Home Page Id
define('HOME_PAGE', 3);
define('DR_ARJUN_SHARDA', 2);
define('OBESITY', 9);
define('ASTHMA', 4);
define('DIABETES', 5);
define('IBS', 11);
define('THYROID', 6);
define('PSORIASIS', 7);
define('CONSTIPATION', 8);
define('RHEUMATOID_ARTHRITIS', 10);
define('GOUT', 12);
define('OSTEOARTHRITIS', 13);
define('PSORIATIC_ARTHRITIS', 14);
define('CHRONIC_FATIGUE_SYNDROME', 17);
define('REACTIVE_ARTHRITIS', 16);
define('ANKYLOSING_SPONDYLITIS', 54);
define('NECK_PAIN', 18);
define('ERECTILE_DYSFUNCTION', 19);
define('BACK_PAIN', 20);
define('ACIDITY', 21);
define('KNEE_PAIN', 22);
define('ABDOMINAL_PAIN', 23);
define('KIDNEY_FAILURE', 24);
define('KIDNEY_STONES', 25);
define('SCIATICA_PAIN', 26);
define('URIC_ACID', 27);
define('FIBRO_MYALAGIA', 28);
define('OSTEOPOROSIS', 29);
define('HIP_JOINT_PAIN', 30);
define('FLATULENCE', 31);
define('PILES', 32);
define('ANEMIA', 33);
define('CERVICAL_SPONYLOSIS', 34);
define('LUMBAR_SPONDYLOSIS', 35);
define('PRE_MATURE_EJACULATION', 36);
define('DE_AADDICTION_ALCOHOL', 37);
define('DRUG_DE_ADDICTION', 38);
define('MENOPAUSE', 39);
define('LEUCORRHOEA', 40);
define('SINUSITIS', 41);
define('DEPRESSION', 43);
define('PCOD_PCOS', 44);
define('OLIGOSPERMIA', 45);
define('FATTY_LIVER', 46);
define('PRIVACY_POLICY', 47);
define('ABOUT_US_PAGE', 48);
define('LUDHIANA', 49);
define('MOHALI', 50);
define('MUKTSAR', 51);
define('BATHINDA', 52);
define('SUCCESS_STORIES', 53);
define('SMOKING', 54);
define('JAUNDICE', 48);
define('URINARY_TRACT_INFECTION', 56);
define('FUNGAL_INFECTION', 57);
define('LIVER_CIRRHOSIS', 58);
define('CONTACT_US', 59);
define('SERVICES', 60);
define('BLOGS', 61);
define('ECZEMA', 62);
define('POST_VIRAL_ARTHRITIS', 63);
define('INSOMNIA', 64);
define('ANXIETY_DISORDER', 65);
define('MIGRAINE', 66);
define('ULCERATIVE_COLITIS', 67);
define('RHINITIS', 58);
define('CHRONIC_LARYNGITIS', 69);
define('BLOOD_PRESSURE', 70);
define('DR_MUKESH_SHARDA', 55);
define('HIGH_CHOLESTEROL', 48);
define('DR_MUKESH_SHARDA_AUTHOR', 48);
define('DR_SHARDA_AUTHOR', 48);
define('CORONARY_ARTERY_DISEASE', 48);
define('CYSTITIS', 48);
define('IMPORTANT_DISCLAIMER', 48);
define('ONLINE_CONSULTATION', 10);
define('WELLNESS_PROGRAM', 5);
define('TYPES_OF_ECZEMA', 79);
define('INVERSE_PSORIASIS_TREATMENT', 79);
define('STASIS_DERMATITIS', 79);
define('CONTACT_DERMATITIS', 79);