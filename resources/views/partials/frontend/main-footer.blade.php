<footer class="dsa-foo-sec">
    <div class="container-fluid">
        <div class="row">
        </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="foo-logo">
                    <img src="{{ URL::asset('front/images/dr-sharda-logo.webp') }}" class="cp-logo" alt="Dr sharda ayurveda center logo">
                </div>
            </div>
        </div>
        <div class="row cust-address">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12 caso">
                <div class="foo-address-box">
                    <a href="https://goo.gl/maps/Ht7pDqvuRSWjiB4m7"><p class="fp"><span><i class="fa fa-map-marker" aria-hidden="true"></i></span>Ludhiana (H.O.)</p></a>			
                    <p class="csr">562-L, Opposite Suman <br>Hospital, Model Town,<br> Ludhiana, <br>Punjab 141002</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12 caso">
                <div class="foo-address-box">
                    <a href="https://goo.gl/maps/kvDTxs55bc6T1gN78"><p class="fp"><span><i class="fa fa-map-marker" aria-hidden="true"></i></span>Mohali</p></a>	
                    <p class="csr">Opposite Army Canteen,<br>Sco 105, Phase 10, Sector 64, Sahibzada Ajit Singh Nagar,<br>Punjab 160062</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12 caso caso-1">
                <div class="foo-address-box">
                    <a href="https://goo.gl/maps/rZAymdETLwHFzwR58"><p class="fp"><span><i class="fa fa-map-marker" aria-hidden="true"></i></span>Bathinda</p></a>	
                    <p class="csr">Opposite Sepal Hotel,<br>Gate No.1, Near Tinkoni <br>Chowk Bathinda , <br>Punjab 151001</p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                <div class="foo-address-box">
                    <a href="https://goo.gl/maps/8JCpaT6hWMRETjBQ9"><p class="fp"><span><i class="fa fa-map-marker" aria-hidden="true"></i></span>Sri Muktsar Sahib</p></a>	
                    <p class="csr">Abohar Road, <br>Near Bus Stand, <br>Sri Muktsar Sahib,<br>Punjab 152026</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="brd"></div>
            </div>
            <div class="col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12">
                <p class="text-white copy">Copyright  2021 Dr. Sharda Ayurveda. All Rights Reserved</p>
            </div>
            <div class="col-xl-7 col-lg-7 col-md-6 col-sm-12 col-12 list-1">
                <ul class="listing">
                    <li>
                        <a href="/">Home <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a  href="{{ route('about-us') }}">About Us</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="{{ route('ayurvedic-treatments') }}">Ayurvedic Treatments</a><span class="ln">&#124;</span>
                    </li>
<!--                     <li>
                        <a href="{{ route('testimonial') }}">Testimonials</a><span class="ln">&#124;</span>
                    </li> -->
                    <li>
                        <a href="{{ route('blogs') }}">Blogs</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="{{ route('privacy-policy') }}">Privacy Policy</a><span class="ln">&#124;</span>
                    </li> 
                    <li>
                        <a href="{{ route('shipping-policy') }}">Shipping Policy</a><span class="ln">&#124;</span>
                    </li> 
                    <li>
                        <a href="{{ route('important-disclaimer') }}">Disclaimer</a><span class="ln">&#124;</span>
                    </li>     
                    <li>
                        <a href="{{ route('ayurvedic-online-consultation') }}">Online Consultation</a>
                    </li>              
                </ul>
            </div>
        </div>
    </div>

    <div class="row book-hide">
        <div class="footer-mobile" style="min-height:40px; background-color:#d49925; position: fixed ;bottom:0; z-index: 999999; padding: 5px 0 5px 0;   box-shadow: 0 4px 12px #e5e5e5; width:105%">
            <div class="col-md-12">
                <ul class="list-inline">

                    <li class="list-inline-item text-center ml-auto">
                        <a href="tel:+919876035500" class="ico-facebook "><i class="fab fa fa-phone mb-0 fa-1x" style="color: #fff; font-size: 17px;"></i><br>Call Us</a>
                    </li>
                    <li class="list-inline-item text-center ml-auto">
                        <a data-action="open" data-phone="919876035500" data-message="Hi." href="https://wa.me/919876035500" target="_blank" class=""><i class="fa fa-whatsapp mb-0" style="font-size:19px; color:#fff;"></i> <br>WhatsApp</a>
                    </li>
                    <li class="list-inline-item text-center">
                        <a href="mailto:info@drshardaayurveda.com" class="ico-facebook"><i class="fab fa fa-envelope-o mb-0 fa-1x" style="color:#fff; font-size:15px;"></i><br>Email Us</a>
                    </li>
<!--                     <li class="list-inline-item text-center ml-auto">
                        <a href="https://www.drshardaayurveda.com/contact-us" class="ico-facebook"><i class="fab fa fa-calendar mb-0 fa-1x" style="color:#fff; font-size: 15px;"></i><br>Appointment</a>
                    </li> -->
                </ul>
            </div>
        </div>
    </div>
    @include('../partials/frontend/modal-form')
</footer>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" >
<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">
<script>
    jQuery('#client-test').owlCarousel({
        loop: true,
        margin: 10,
        nav: false,
        dots: true,
        responsive: {
            0: {
                items: 1
            }
        }
    })

    jQuery(window).scroll(function () {
        var scroll = jQuery(window).scrollTop();
        if (scroll >= 300) {
            jQuery(".navbar-menu").addClass("sticky_header");
        } else {
            $(".navbar-menu").removeClass("sticky_header");
        }
    });

    $(document).ready(function () {
        $(".mobile").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
                $("#errmsgMobile").html("Only number are allowed!").show().fadeOut(5000);
                return false;
            }
        });
    });

    function onSubmit(token) {

        var mobNum = $(".mobile").val();
        if (mobNum.length != 10) {
            $("#errmsgMobile").html("Number should be 10 digit!").show().fadeOut(5000);
            return false;
        }
        document.getElementById("form_contact").submit();
   }

    $('#sbtForm').click(function () {
        var email = $('#email').val();
        if (IsEmail(email) == false) {
            $("#errmsgEmail").html("Invalid email").show().fadeOut(5000);
            return false;
        }
        var mobNum = $(".mobile").val();
        if (mobNum.length != 10) {
            $("#errmsgMobile").html("Number should be 10 digit!").show().fadeOut(5000);
            return false;
        }
    });

    function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!regex.test(email)) {
            return false;
        } else {
            return true;
        }
    }

    function readMoreReadLess() {
        var dots = document.getElementById("dots");
        var moreText = document.getElementById("more-content");
        var btnText = document.getElementById("myBtn");

        if (dots.style.display === "none") {
            dots.style.display = "inline";
            btnText.innerHTML = "Read more";
            moreText.style.display = "none";
        } else {
            dots.style.display = "none";
            btnText.innerHTML = "Read less";
            moreText.style.display = "inline";
        }
    }

    $(document).on('click', '.dropdown-menu', function (e) {
        e.stopPropagation();
    });
    
    if ($(window).width() < 992) {
  $('.dropdown-menu a').click(function(e){
    if($(this).attr('href') == '#')
      e.preventDefault();
    if($(this).next('.submenu').length){
      $(this).next('.submenu').toggle();
    }
    $('.dropdown').on('hide.bs.dropdown', function () {
      $(this).find('.submenu').hide();
    })
  });
}

$('.carousel.carousel-multi-item.v-2 .carousel-item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));

  for (var i=0;i<4;i++) {
    next=next.next();
    if (!next.length) {
      next=$(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));
  }
});

setTimeout(function() {
    //$('.bd-example-modal-xl').modal();
}, 15000);

AOS.init();

</script>
<script> setTimeout((function() { var v = document.getElementsByClassName("youtube-player"); for (var n = 0; n < v.length; n++) { v[n].onclick = function () { var iframe = document.createElement("iframe"); iframe.setAttribute("src", "//www.youtube.com/embed/" + this.dataset.id + "?autoplay=1&autohide=2&border=0&wmode=opaque&enablejsapi=1&rel=" + this.dataset.related + "&controls=" + this.dataset.control + "&showinfo=" + this.dataset.info); iframe.setAttribute("frameborder", "0"); iframe.setAttribute("id", "youtube-iframe"); iframe.setAttribute("style", "width: 100%; height: 100%; position: absolute; top: 0; left: 0;"); if (this.dataset.fullscreen == 1){ iframe.setAttribute("allowfullscreen", ""); } while (this.firstChild) { this.removeChild(this.firstChild); } this.appendChild(iframe); }; } })(), 5000);</script>

<script type='text/javascript'>
		(function(I, L, T, i, c, k, s) {if(I.iticks) return;I.iticks = {host:c, settings:s
, clientId:k, cdn:L, queue:[]};var h = T.head || T.documentElement;var e = T.createElement(i);var l = I.location;e.async = true;e.src = (L||c)+'/client/inject-v2.min.js';h.insertBefore(e, h.firstChild);I.iticks.call = function(a, b) {I.iticks.queue.push([a, b]);};})(window, 'https://cdn.intelliticks.com/prod/common', document, 'script', 'https://app.intelliticks.com', 'MwLRkSDf9gjMF9nfX_c', {});
</script>