<?php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$routeName = \Route::currentRouteName();
?>
<style>.success, .failure{display:none;}</style>
<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog"  aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title header-color">BOOK AN APPOINTMENT</h5>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">&times;</span>
		</button>
	  </div>
	  <div class="modal-body">
	  	<div class="container-fluid">
			<div class="row">
				<!-- style="background-image:#244236; modal-con-area-->
				<div class="col-md-6">
					<div class="col-md-12 text-div" style="margin-top:10px;">
						<!-- <p>Dr. Sharda Ayurveda has several branches located in Punjab and is one of the Best Ayurvedic Clinics in India offering a New Approach to Holistic Living.</p> -->
					</div>
					<div class="col-md-12 img-div">
						<div class="right-img">
							<img src="{{ URL::asset('front/images/Ayurvedic-doctor-dr-mukesh-sharda.webp') }}" class="doctor-mob-img doctor img-fluid" alt="Ayurvedic Doctor Dr. Mukesh Sharda">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="alert alert-success success" role="alert"><span class='succ-msg'></spna></div>
					<div class="alert alert-danger failure" role="alert"><span class='fail-msg'></spna></div>
					<form class="lead-form" action="{{ route('lead-submission') }}">
						@csrf
						<input type='hidden' name="msubject" value="<?= $routeName; ?>">
						<input type='hidden' name="mpage_url" value="<?= $actual_link; ?>">
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Name:</label>
							<input type="text" class="form-control" id="name" name="mname"  placeholder="Name">
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Phone:</label>
							<input type="number" min="0" class="form-control" id="mobile" name="mmobile"  placeholder="Phone">
						</div>
						<div class="form-group">
							<label for="recipient-name" class="col-form-label">Email:</label>
							<input type="email" class="form-control" id="email" name="memail" placeholder="E-Mail">
						</div>
						<div class="form-group">
							<label for="message-text" class="col-form-label">Message:</label>
							<textarea class="form-control" id="message-text" name="mdescription"></textarea  placeholder="Message">
						</div>

						<!-- <div class="form-group">
							<label for="message-text" class="col-form-label">Message:</label>
							<div class="g-recaptcha" data-sitekey="6LdfUSEgAAAAAI-yGA5aTWpp-1xdchwBNdQimeuP"></div>
						</div> -->
						<!-- <div class="form-group">
							<div class="captcha">
								<span>{!! captcha_img() !!}</span>
								<button type="button" class="btn btn-success btn-refresh"><i class="fa fa-refresh"></i></button>
							</div>
                          	<input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha">
						</div> -->
					</form>
				</div>
			</div>
		</div>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		<button type="button" class="btn btn-primary submit-lead">Submit</button>
	  </div>
	</div>
  </div>
</div>

<script type="text/javascript">
	$('.submit-lead').on('click', function(e){
		e.preventDefault(); // avoid to execute the actual submit of the form.
		var form = $('.lead-form');
		var url = form.attr('action');

		$.ajax({
			type: "POST",
			url: url,
			data: form.serialize(), // serializes the form's elements.
			success: function(data)
			{
				if(data.status == 'success'){
					$('.succ-msg').html(data.message);
					$('.success').show();
					setTimeout(function() {
						location.reload();
					}, 3000);
				}else if(data.status == 'failed'){
					$('.fail-msg').html(data.message);
					$('.failure').show();
					return false;
				}
			}
			});
	});

	$(".btn-refresh").click(function(){
		$.ajax({
			type:'GET',
			url:"{{ route('refresh_captcha') }}",
			success:function(data){
				$(".captcha span").html(data.captcha);
			}
		});
	});

</script>