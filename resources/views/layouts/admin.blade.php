<!DOCTYPE html>
<html lang="en">
    <!-- BEGIN HEAD -->
    <head>
        @include('../partials/head')
    </head>
    <!-- END HEAD -->
    <body class="hold-transition sidebar-mini">
        <div class="wrapper">
            <!-- BEGIN HEADER -->
            @include('../partials/header')
            <!-- END HEADER -->
            <!-- Main Sidebar Container -->
            @include('../partials/sidebar')
            <!-- End Sidebar Container -->
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                @yield('content')
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            @include('../partials/footer')
            <!-- END FOOTER -->
        </div>
        <!-- jQuery -->
        <script src="{{ URL::asset('admin/plugins/jquery/jquery.min.js') }}" type="text/javascript"></script>
        <!-- Bootstrap 4 -->
        <script src="{{ URL::asset('admin/plugins/bootstrap/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{ URL::asset('admin/dist/js/adminlte.min.js') }}" type="text/javascript"></script>
        <!-- DataTables  & Plugins -->
        <script src="{{ URL::asset('admin/plugins/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}" type="text/javascript"></script>
        <script src="{{ URL::asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}" type="text/javascript"></script>
        <!-- <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script> -->
        <script src="//cdn.ckeditor.com/4.20.2/full/ckeditor.js"></script>
        <!-- <script src="{{ url('path/ckeditor.js') }}"></script> -->
        <script type="text/javascript">
            CKEDITOR.replace('post_contant', {
                filebrowserUploadUrl: "{{route('posts.upload', ['_token' => csrf_token() ])}}",
                filebrowserUploadMethod: 'form',
                filebrowserBrowseUrl     : "{{ route('ckfinder_browser') }}",
                //filebrowserImageBrowseUrl: "{{ route('ckfinder_browser') }}?type=Images&token=123",
                //filebrowserFlashBrowseUrl: "{{ route('ckfinder_browser') }}?type=Flash&token=123",
                //filebrowserImageUploadUrl: "{{ route('ckfinder_connector') }}?command=QuickUpload&type=Images",
                //filebrowserFlashUploadUrl: "{{ route('ckfinder_connector') }}?command=QuickUpload&type=Flash",
            });
        </script>
        @include('ckfinder::setup')
        <script>
            $(function () {
                $('.example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                });
            });
        </script>
    </body>
</html>