<!DOCTYPE html>
<html lang="en-US">
    <head>
        @include('../partials/frontend/main-head')
    </head>
    <body>
        @include('../partials/frontend/main-header')
        <div class="main-sec">
        <a style="cursor: pointer;z-index:99999;" data-toggle="modal" data-target=".bd-example-modal-xl" class="online-consult">ONLINE CONSULTATION</a>
            @yield('content')
        </div>
        @include('../partials/frontend/main-footer')
    </body>
</html>