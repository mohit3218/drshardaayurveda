<?php
/**
 * Post Add/Edit Form 
 * 
 * @created    29/11/2021
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2021
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.admin')
@section('content')

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Posts</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Posts</li>
                </ol>
            </div>
        </div>
    </div>
</section>
@include('../partials/message')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Post Add/Edit</h3>
                    </div>
                    <!-- /.card-header -->
                    
                     @if(isset($post))
                        {{ Form::model($post, array('route' => array('posts.update', $post->id), 'class' => 'form-horizontal', 'method' => 'PUT', 'enctype' => 'multipart/form-data')) }}
                        @else
                        {!! Form::open(array('url' => 'admin/posts', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')) !!}
                        @endif	
                        {{ csrf_field() }}
                        {{ Form::hidden('is_active', '1') }}
                        <div class="card-body">
                            <div class="form-group row">
                                {!! Form::label('Meta Name', 'Meta Name', array('class' => 'control-label col-md-4 col-sm-4 col-xs-12')) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Meta Name', 'required' => 'required')) !!}
                                    <!--<input type="text" name="name" />-->
                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        {{ $errors->first('name') }}
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('Meta Title', 'Meta Title', array('class' => 'control-label col-md-4 col-sm-4 col-xs-12')) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::text('meta_title', null, array('class' => 'form-control', 'placeholder' => 'Meta Title', 'required' => 'required')) !!}
                                    <!--<input type="text" name="name" />-->
                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        {{ $errors->first('name') }}
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('Meta Keyword', 'Meta Keyword', array('class' => 'control-label col-md-4 col-sm-4 col-xs-12')) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::text('meta_keywords', null, array('class' => 'form-control', 'placeholder' => 'Meta Keyword')) !!}
                                    <!--<input type="text" name="name" />-->
                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        {{ $errors->first('name') }}
                                    </span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                {!! Form::label('Meta Description', 'Meta Description', array('class' => 'control-label col-md-4 col-sm-4 col-xs-12')) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::textarea('meta_description',null,['class'=>'form-control', 'rows' => 5, 'cols' => 40, 'placeholder' => 'Meta Description']) !!}
                                    <!--<input type="text" name="name" />-->
                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        {{ $errors->first('name') }}
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('Post Title', 'Post Title', array('class' => 'control-label col-md-4 col-sm-4 col-xs-12')) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::text('post_title', null, array('class' => 'form-control', 'placeholder' => 'Post Title', 'required' => 'required')) !!}
                                    <!--<input type="text" name="name" />-->
                                    @if ($errors->has('post_title'))
                                    <span class="help-block">
                                        {{ $errors->first('post_title') }}
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('Post Thumbnail ', 'Post Thumbnail', array('class' => 'control-label col-md-4 col-sm-4 col-xs-12')) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::file('post_image', null, array('class' => 'form-control', 'placeholder' => 'Post Thumbnail', 'required' => 'required')) !!}
                                    <!--<input type="text" name="name" />-->
                                    @if ($errors->has('post_title'))
                                    <span class="help-block">
                                        {{ $errors->first('post_title') }}
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('Post Category ', 'Post Category', array('class' => 'control-label col-md-4 col-sm-4 col-xs-12')) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select name="category" class="form-control category " required autofocus> 
                                        <option value="">Please Select</option>
                                        <?php  foreach (StaticArrays::$post_categories as $id => $name) : ?>
                                            <?php $sel = ''; if(isset($post) && $post->category == $id) {$sel = 'selected'; } ?>
                                            <option value="<?= $id; ?>" <?= $sel; ?>><?= ucfirst($name); ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <!--<input type="text" name="name" />-->
                                    @if ($errors->has('category'))
                                    <span class="help-block">
                                        {{ $errors->first('category') }}
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('Post Contant', 'Post Contant', array('class' => 'control-label col-md-4 col-sm-4 col-xs-12')) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::textarea('post_contant',null,['class'=>'ckeditor form-control ', 'rows' => 5, 'cols' => 40, 'placeholder' => 'Post Contant']) !!}
                                    @if ($errors->has('post_contant'))
                                    <span class="help-block">
                                        {{ $errors->first('post_contant') }}
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('OG Title', 'OG Title', array('class' => 'control-label col-md-4 col-sm-4 col-xs-12')) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::text('og_title', null, array('class' => 'form-control', 'placeholder' => 'OG Title')) !!}
                                    <!--<input type="text" name="name" />-->
                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        {{ $errors->first('name') }}
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('OG Description', 'OG Description', array('class' => 'control-label col-md-4 col-sm-4 col-xs-12')) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::textarea('og_description',null,['class'=>'form-control', 'rows' => 5, 'cols' => 40, 'placeholder' => 'OG Description']) !!}
                                    <!--<input type="text" name="name" />-->
                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        {{ $errors->first('name') }}
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('OG Image', 'OG Image', array('class' => 'control-label col-md-4 col-sm-4 col-xs-12')) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::text('og_image', null, array('class' => 'form-control', 'placeholder' => 'OG Image')) !!}
                                    <!--<input type="text" name="name" />-->
                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        {{ $errors->first('name') }}
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('Canonical URL', 'Canonical URL', array('class' => 'control-label col-md-4 col-sm-4 col-xs-12')) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::text('canonical_url', null, array('class' => 'form-control', 'placeholder' => 'Canonical URL')) !!}
                                    <!--<input type="text" name="name" />-->
                                    @if ($errors->has('canonical_url'))
                                    <span class="help-block">
                                        {{ $errors->first('canonical_url') }}
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                {!! Form::label('Schema', 'Schema', array('class' => 'control-label col-md-4 col-sm-4 col-xs-12')) !!}
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    {!! Form::textarea('post_schema', null, array('class' => 'form-control', 'placeholder' => 'Schema')) !!}
                                    <!--<input type="text" name="name" />-->
                                    @if ($errors->has('post_schema'))
                                    <span class="help-block">
                                        {{ $errors->first('post_schema') }}
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                            <label for="tag">Author By:<span class="asterisk">*</span></label>
                                <select name="created_by" class="form-control select2" id="created_by" required>
                                    <option value="">Select Author</option>
                                    @if(isset($users))
                                    @foreach ($users as $k => $value)
                                    <option value="{{ $value->id }}"
                                        {{ $value->id == $post['created_by'] ?'selected':'' }}>
                                        {{ $value->name }} </option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="card-footer">
                                <div class="col-md-8 col-sm-8 col-xs-12 col-md-offset-4 col-sm-offset-4 col-xs-offset-0">
                                    <button type="submit" class="btn btn-info">Submit</button>
                                    <button type="reset" class="btn btn-default">Cancel</button>
                                </div> 
                            </div>
                        </div>
                        {!! Form::close() !!}
                    <!-- form start -->
                </div>
                <!-- /.card -->
            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
@stop