<?php
/**
 * psoriasis Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="3000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4">Psoriasis</h3>
                    <p style="font-size: 18px;">SKIN-VEST-MENT It is going to with you for life time</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/psoriasis">Psoriasis</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a  href="#types">TYPES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="3000">
                    <img src="{{ URL::asset('front/images/psoraises.webp') }}" class="why-choose-us img-fluid" alt="Psoraises treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1">Ayurveda for <b>Psoriasis</b></h1>
                <p>People often ignore psoriasis as assuming it is a normal infection of the skin. The adverse impact of psoriasis even causes all the skin of the body to shed off. Psoriasis is an autoimmune, non-contagious, chronic disease of the skin. It is a skin disorder that causes skin cells to multiply up to 10 times faster than normal. This makes theskin build up into bumpy red patches covered with white scales. In psoriasis, the production of skin cells occurs in 3-4 days instead of 30 days hence causing shedding of skin. They can grow anywhere, but mostly appears on the scalp, elbows, knees, and lower back. Psoriasis can't be spread from person to person. It does sometimes occur in members of the same family.
                <span id="dots" data-aos="fade-up" data-aos-duration="3000">...</span>
                </p>
                <span id="more-content">
                      <p>
                      Psoriasis usually appears in early adulthood. For most people, it affects just a few areas. In severe cases, psoriasis can cover large parts of the body. The patches can heal and then come back throughout a person's life.
                     </p>
                <div class="btn-banner crd">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                        </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec" id="types"> 
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="heading1">Types of <b>Psoriasis</b></h2>
            </div>
        </div>
        <div class="row cust-iba">
            <div class="owl-carousel owl-theme" >
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/psoraisess_2.webp') }}" class="card-img-top lazyload" alt="Plaque Psoriasis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Plaque Psoriasis</h4>
                            <p class="card-text">
                                In Plaque Psoriasis, rashes are seen in red color on the body, inflamed in nature, and sometimes whitish silvery scales are found on elbows, knees, and the scalp.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/psoraises_3.webp') }}" alt="Guttate Psoriasis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Guttate Psoriasis</h4>
                            <p class="card-text">
                                In Guttate Psoriasis, pink and small spot-like rashes develop over arms and legs. This type of Psoriasis is mostly seen in children.
                            </p>
                        </div>
                    </div>
                </div>


                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/psoraises_4.webp') }}"  alt="Inverse Psoriasis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Inverse Psoriasis</h4>
                            <p class="card-text">As the name indicates, it is present in folds of skin like under the breast, armpits, groin. In Inverse Psoriasis skin appears red, shiny, and inflamed.</p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/Pustular-Psoriasis.webp') }}"  alt="Pustular Psoriasis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Pustular Psoriasis</h4>
                            <p class="card-text">
                                Pustular Psoriasis is localized and limited to smaller areas of the body. For example, they are seen on hands and feet. Rashes are white with puss-filled blisters and areas are red with inflammation.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/Erythrodermic-Psoriasis.webp') }}"  alt="Erythrodermic Psoriasis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Erythrodermic Psoriasis</h4>
                            <p class="card-text">Erythrodermic Psoriasis is the rarest form of Psoriasis but is very dangerous. A large section of the body get involves, scales slough off in large sections and sometimes even is life-threatening.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="3000">
                    <h2 class="heading1">Causes of <b>Psoriasis</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Excessive Intake of Alcohol
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                High consumption of alcohol causes psoriasis to take birth. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Stress
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                A person who is staying under stress all day long is prone to be affected with psoriasis. 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Changing Weather Conditions
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                Changing weather i.e. - chilly cold and dry weather also causes psoriasis.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                   Injury to Skin
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">
                                An injury caused to the skin such as a cut or scrape, a bug bite.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Medications
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                            <div class="card-body">
                               Certain medications are considered as psoriasis trigger for example- High dose of medicines for blood pressure and antimalarial medications.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	

<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/psoriasis-symptoms.webp') }}" class="img-fluid"alt="Psoriasis symptoms">
        </div>
        <div class="cvn sym" >
            <h3><span>Symptoms</span> of Psoriasis </h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Red Patches on Skin</p>
            <p>Red and raised patches are seen on skin that is covered with thick silvery scales.</p>
            <br>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Dry, Cracked Skin</p>
            <p>The skin becomes dry and cracked that may bleed or cause itching. </p>
            <br>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Swollen Joints</p>
            <p>The joints become stiff and also get swollen and leads to pain in joints too. </p>
            <br>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Weight Loss</p>
            <p>Pitted Nails.</p>
            <p>Not every person will experience all of these above symptoms. </p>
            <p>Symptoms may vary if one has a less common type of psoriasis.</p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Advantages of <b>Ayurvedic Treatment for Psoriasis</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurveda works on treating the disease from its root cause.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurveda does not work on treating symptoms, it heals an individual within.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Psoriasis can never be cured with Steroid based creams. It demands a proper Ayurvedic treatment for lifelong healing.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Rasayanachikitsa (immunomodulation) is advised for boosting immunity.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Garlic and Onions are used to purify the blood.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="G0kvtmoIvX8" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/G0kvtmoIvX8.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
<section class="dsa-why-choose" id="testimonials">
    <div class="container-fluid">
        <h2 text align="center">Kind words of patients of Dr. Sharda Ayurveda</h2>
        <div class="row cut-row" >
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div id="dna_video">
                    <div class="youtube-player" data-id="CKThTWLTaoI" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                        <img src="{{ URL::asset('front/images/CKThTWLTaoI.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                        <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div id="dna_video">
                    <div class="youtube-player" data-id="UAIIVsXq5Mo" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                        <img src="{{ URL::asset('front/images/UAIIVsXq5Mo.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                        <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="3000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Psoriasis Testimonial" >
                            <h3 class="usrname">Surbhi</h3>
                            <p class="desig">Psoriasis Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p><span><i class="fa fa-quote-left" aria-hidden="true"></i>
                                </span>I was suffering from psoriasis for the past 3 years. Always got temporary relief till the time I was intaking medicines. After Ayurvedic treatment for psoriasis from Dr. Sharda Ayurveda, I was finally relaxed and free from psoriasis. <span><i class="fa fa-quote-right" aria-hidden="true"></i>
                                </span></p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Psoriasis Testimonial" >
                            <h3 class="usrname">Ajay Kumar</h3>
                            <p class="desig">Psoriasis Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Due to psoriasis my father’s skin was always shedding and he was in great discomfort. We consulted Dr. Sharda Ayurveda for treatment and within few months of Ayurvedic treatment, we were highly satisfied with the results.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Psoriasis Testimonial" >
                            <h3 class="usrname">Priyanka</h3>
                            <p class="desig">Psoriasis Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I just wanted to take a moment and thank Dr. Sharda Ayurveda for awesome <strong>Ayurvedic treatment for psoriasis</strong>. Their medicines are quite effective and helped in treating my 8 years old Psoriasis.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="3000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                How is psoriasis diagnosed?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            There is no special blood tests or diagnostic parameter to diagnose it. Qualified and experienced doctors can diagnose it after seeing its symptoms and by pulse reading. A skin biopsy may be sometimes useful in diagnosis.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                What Ayurveda says about psoriasis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            Ayurvedic texts say that psoriasis arises due to an imbalance of two “Doshas,” or areas of energy. Practitioners of Ayurvedic medicine call these the “Vata” and “Kapha.” Vata is responsible for controlling bodily functions and could contribute to the dryness and skin scaling of psoriasis. Increase in Kapha leads to any skin disease.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Is there any dietary restrictions for psoriasis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            Yes, sour, salty, heat-producing food items like pepper, curd, milk, sugar, meat or nonveg, sesame seeds black gram are contraindications, alcoholic and carbonated drinks, packed food, junk food are to be avoided.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Whether psoriasis trigger in different seasons?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                            Yes, seasonal changes play an important role in its outbreaks. Sunlight sometimes in summers heals Psoriasis and leads to a decrease in symptoms while cold and dry air in winters aggravates or triggers the disease.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFive" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <i class="fa" aria-hidden="true"></i>
                                Which is the best herb for psoriasis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="card-body">
                            Turmeric is part of the same family as the ginger plant. Its active ingredient is called curcumin. Turmeric has unique anti-inflammatory and antibacterial properties, which have led scientists to study it as a treatment for psoriasis.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Does psoriasis has a permanent cure?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                        <div class="card-body">
                            In Ayurveda, it can be cured but it depends upon the patient's power, faith in medicine, and Doctor, his dedication towards recommended diet and lifestyle.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Is Ayurvedic treatment effective for psoriasis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                        <div class="card-body">
                           Ayurvedic treatment for psoriasis is quite effective as Ayurveda has various Herbs that help fight psoriasis. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                How long does Ayurvedic treatment last for psoriasis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                        <div class="card-body">
                            Ayurvedic treatment is never the same for 2 human beings. It varies according to Dosha. So the time for treatment also varies according to individuals and their type of disease.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                What is the root cause of psoriasis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                        <div class="card-body">
                            Ill eating habits, overeating, and undigested food can lead to psoriasis. Some people who intake sour things with milk are more prone to have psoriasis.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Will psoriasis go away naturally?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                        <div class="card-body">
                            With modifications in diet, climatic changes will show fewer symptoms but will need Ayurvedic treatment for life long cure.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Psoriasis",
    "item": "https://www.drshardaayurveda.com/psoriasis"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "How is Psoriasis diagnosed?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "There is no special blood tests or diagnostic parameter to diagnose it. Qualified and experienced Doctors can diagnose it after seeing its symptoms and by Pulse reading.  A skin biopsy may be sometimes useful in diagnosis."
    }
  },{
    "@type": "Question",
    "name": "What Ayurveda says about Psoriasis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "All the Ayurvedic texts describe that Psoriasis arises due to an imbalance of two “Doshas,” or areas of energy. In body Vata is responsible for controlling bodily functions and contributes to the dryness and skin scaling of Psoriasis. Increase in Kapha leads to any skin disease."
    }
  },{
    "@type": "Question",
    "name": "Is there any dietary restrictions for Psoriasis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, Sour, Salty, heat-producing food items like Pepper, Curd, Milk, Sugar, Meat or nonveg, Sesame seeds Black gram are contraindications, alcoholic and carbonated drinks, packed food, junk food are to be avoided."
    }
  },{
    "@type": "Question",
    "name": "Whether Psoriasis trigger in different seasons?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, seasonal changes play an important role in its outbreaks. Sunlight sometimes in summers heals Psoriasis and leads to a decrease in symptoms while cold and dry air in winters aggravates or triggers the disease."
    }
  },{
    "@type": "Question",
    "name": "Which is the best herb for Psoriasis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Turmeric is considered as one of the best Healing herb. Its active ingredient is called Curcumin. Turmeric is known for its unique anti-inflammatory and antibacterial properties, which is a good substitute as a treatment for psoriasis."
    }
  },{
    "@type": "Question",
    "name": "Does Psoriasis has a permanent cure?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "In Ayurveda, it can be cured but it depends upon the patient's power, faith in Medicine, and Doctor, his dedication towards recommended Diet and Lifestyle."
    }
  },{
    "@type": "Question",
    "name": "Is Ayurvedic treatment effective for Psoriasis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurvedic treatment for Psoriasis is quite effective as Ayurveda has various Herbs that help fight Psoriasis."
    }
  },{
    "@type": "Question",
    "name": "How long does Ayurvedic treatment last for Psoriasis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurvedic treatment is never the same for 2 human beings. It varies according to Dosha. So the time for treatment also varies according to individuals and their type of Disease."
    }
  },{
    "@type": "Question",
    "name": "What is the root cause of Psoriasis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ill eating habits, overeating, and undigested Food can lead to Psoriasis. Some people who intake sour things with Milk are more prone to have Psoriasis."
    }
  },{
    "@type": "Question",
    "name": "Will Psoriasis go away naturally?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "With modifications in Diet, Climatic changes will show fewer symptoms but will need Ayurvedic treatment for life long cure."
    }
  }]
}
</script>
<style>
    .carousel-inner {position: relative;width: 100%;overflow: hidden;}
    .carousel-item {position: relative;float: left;width: 100%;margin-right: -100%;-webkit-backface-visibility: hidden;backface-visibility: hidden;transition: -webkit-transform .6s ease-in-out;transition: transform .6s ease-in-out;transition: transform .6s ease-in-out,-webkit-transform .6s ease-in-out;}
    .card-body {padding-top: 1.5rem;padding-bottom: 1.5rem;-webkit-border-radius: 0!important;border-radius: 0!important;flex: 1 1 auto;-ms-flex: 1 1 auto;}
    .card .card-body a h3{font-size: 18px;color: #ff9000;line-height: 1.4;}
    .card .card-body h3{color: #373433;font-size: 18px;    line-height: 1.4;}
    .font-weight-bold {font-weight: 700!important;}
    .card-title {margin-bottom: .75rem;}
    .card-img-top {width: 100%;border-top-left-radius: calc(.25rem - 1px);border-top-right-radius: calc(.25rem - 1px);}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            responsiveClass: !0,
            navigation : true,
            autoplay: true,
            loop: true,
            smartSpeed: 2500,
            singleItem : true,
            navigationText: ["<i class='fa fa-angle-left fa-2'></i>", "<i class='fa fa-angle-right fa-2'></i>"],
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                480: {
                    items: 1,
                    nav: !1
                },
                768: {
                    items: 2,
                    nav: !1
                },
                1000: {
                    items: 2,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1024: {
                    items: 3,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1366: {
                    items: 3,
                    nav: !1
                }
            }
        })
    });

</script>
@stop