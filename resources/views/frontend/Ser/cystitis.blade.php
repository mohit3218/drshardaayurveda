<?php
/**
 * CYSTITIS Page 
 * 
 * @created    12/04/2021
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2021
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4">Cystitis (UTI)</h3>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/cystitis">Cystitis</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatments">TREATMENTS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="3000">
                    <img src="{{ URL::asset('front/images/cystitis.webp') }}" class="why-choose-us img-fluid" alt="cystitis treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1">What is <b>Cystitis (UTI)</b></h1>
                <p>As per the Ayurvedic text Cystitis is called <strong style="font-weight: bold;">“Mutrakrichcha”</strong>. The type of urinary tract infection which mainly occurs due to the imbalance of <strong style="font-weight: bold;">Vata and Pitta doshas</strong>. Cystitis refers to infections of the lower urinary tract, more specifically the urinary bladder. The disease occurs when a bacteria which was earlier living on the skin and bowel harmlessly instantly enters the urethra and bladder. As soon as it enters it proliferate and fastens to the lining of the bladder. As a result, causes extreme itchiness and inflammation. It is commonly prevalent in females compare to males reason being females have a shorter urethra that is closer to the anus thus allowing bacteria to intrude more easily. <a class="green-anchor"href="https://www.drshardaayurveda.com/urinary-tract-infection">Treatment of UTI</a> through the right approach and treatment can help prevent and control the infection to flare-up.
                <span id="dots" data-aos="fade-up" data-aos-duration="3000">...</span>
                </p>
                <span id="more-content">
                      <p>
                      Why is <strong style="font-weight: bold;">Ayurvedic Treatment for Cystitis</strong> is consider the most effective medical approach for best results? Because Ayurveda applies a holistic approach that mainly aims in treating and restoring health by understanding the root cause of the diseases. Managing and treating urinary tract infections involves detoxification (flushing out of infection), and clearing blockages of the urinary tract through the incorporation of herbal medications along with expert suggested dietary and lifestyle changes for efficient and long-term recovery. For best and most effective results choose <strong style="font-weight: bold;">Ayurvedic Treatment for Interstitial Cystitis</strong> from Dr. Sharda Ayurveda which is proven to provide long-term relief from discomforting symptoms.
                     </p>
                <div class="btn-banner crd">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                        </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="3000">
                    <h2 class="heading1">Causes of <b>Cystitis</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Bacterial Infection
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                             The invasion and proliferation of bacteria on the lining of the bladder is the main reason for cystitis.  
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Menopause
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                The epithelium lining of the bladder gets affected when women attain menopause. The lower part of the urinary tract is sensitive to estrogen i.e., a decline in the levels of this hormone favors the growth of “bad” bacteria thus increasing the chances of cystitis to occur.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Constipation 
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                The discomforting bowel movement can increase the chances of bladder infection in two ways i.e., sticking of stool in the rectum and urine retention which altogether leads to bladder infection.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Weak Immune System
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">
                                The weak and ill-functioned immune system may account for bladder infection. The immune system's overactive response to urinary tract infection may increase the risk to develop cystitis. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Pre-Existing Medical Conditions
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                            <div class="card-body">
                                Diabetes and kidney stones can cause the development of described disease.
                                <ul>
                                    <li><strong>Diabetes</strong>: Higher sugar levels in the blood and urine can favor the growth of bacterial infection.</li>
                                    <li><strong>Kidney stones</strong>: The kidney stones are passed out from the bladder and if a person experiences pain and burning sensation while urinating it is direct indication of bladder infection.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	

<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/cystitis-symptoms.webp') }}" class="img-fluid"alt="Cystitis symptoms">
        </div>
        <div class="cvn sym" >
            <h3>Symptoms of <span>Cystitis</span></h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Frequent Urge to Urinate</p>
            <p>If suffering from a urinary tract infection the person may experience pressure on the bladder and an urge to urinate more frequently.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png"> Cloudy Urine</p>
            <p>Cloudy urine is characterized by milky color urine instead of clear or transparent. It is generally harmless, but if it appears more repetitively then indicates a sign of urinary tract infection.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Abdominal Pain</p>
            <p>A person suffering from bladder infection may experience pain in the lower abdomen and a burning sensation while urinating.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Blood in Urine</p>
            <p>When an extreme amount of pressure is felt on the bladder. The walls of the bladder become irritated and inflamed. This leads to bleeding while passing urine.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Smelly Urine</p>
            <p>Bacterial infection in the bladder can result in a strong fish-like smell in the urine.</p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Ayurvedic Treatment for <b>Cystitis</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Panchakarma therapy is the principal procedure of Ayurvedic treatment which helps in detoxification and cleansing of the body system. The procedures followed for treatment of UTI are:
                    <li><b>Snehapana:</b> Oral administration of medicated ghee.</li>
                    <li><b>Abhyanga:</b> Whole body oil massage which helps refresh both physical and mental health. It additionally improves circulation and unblocks channels.</li>
                    <li><b>Swedana:</b> The medicated steam bath that restores metabolism by flushing out accumulated body toxins.</li>
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Performing yoga asanas daily can help strengthen the pelvic region and urinary system which altogether effectively treat and manage the symptoms of cystitis. Some best yoga poses recommended are Gomukhasana and Ardha Matsyendrasana.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurvedic Treatment for Cystitis along with herbal medications which also involves certain dietary and lifestyle changes that aid in effective recovery from the disease symptoms.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    The best home remedy which gives positive results is to drink daily coriander juice which helps nourish and heal the urinary tract by flushing out all the toxins.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Having more fibers in the diet promotes a healthy bladder. Some of the tastiest choices include legumes, broccoli, banana, and strawberries.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="PGKM-htmG9A" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/backpain.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="3000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Psoriasis Testimonial" >
                            <h3 class="usrname">Jatinder Singh</h3>
                            <p class="desig">UTI Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p><span><i class="fa fa-quote-left" aria-hidden="true"></i>
                                </span>I was diagnosed with chronic cystitis (bladder infection) 2 years back. Earlier neglected the early emerging signs but soon after the symptoms of the disease caused extreme discomfort. The symptoms included are abdominal pain accompanied by smelly and cloudy urine. Even after consulting many doctors there was no sign of improvement and I questioned myself if my bladder will ever get back to normal. Therefore, as a recommendation from a friend visited Dr. Sharda Ayurveda. Here, with regular Ayurvedic Treatment for Interstitial Cystitis, I got fully recovered from bladder infection.<span><i class="fa fa-quote-right" aria-hidden="true"></i>
                                </span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="3000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                Which bacteria most commonly triggers bladder infection?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            The bacteria Escherichia coli (E. coli) mainly trigger urinary tract infection. But, there are certain other bacteria also which can cause urinary tract infections.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                               Can stress cause bladder infection?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            Disturbed mental health can greatly impact overall health. The bladder infection can get triggered and flared up if the mental and physical stability is not maintained well.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Which treatment is best to treat urinary tract infection?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            <a class="green-anchor"href="https://www.drshardaayurveda.com/cystitis">Ayurvedic Treatment for Cystitis</a> is the best and most effective treatment for long-term and positive results. The urinary tract infection are the outcomes of the imbalanced Vata and Pitta Doshas. Ayurveda focuses on balancing these aggravated body energies through herbal medications and by adopting certain diet and lifestyle changes. It helps to flush out infections from the body and cleaning of urinary tract blockages.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                What are the best tips suggested by Ayurveda to prevent urinary tract infection?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                           Some of the best Ayurvedic tips are:
                           <li>Avoid food items that are cold and hard to digest.</li>
                           <li>Heat compression on the lower abdomen with a towel helps provide relief from pain.</li>
                           <li>Drink plenty of water that helps detox the body.</li>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFive" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <i class="fa" aria-hidden="true"></i>
                                What are the complications associated with a persistent bladder infection?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="card-body">
                            There are several serious health complications associated with cystitis including hematuria, acute renal failure, bladder rupture, and pyelonephritis.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Which hospital or clinic provides effective treatment for cystitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                        <div class="card-body">
                            Dr. Sharda Ayurveda provides the best and most effective treatment for bladder infection. They provide root cause and specialized Ayurvedic treatment for cystitis which gives long-term recovery and help prevent the reoccurrence of the infection symptoms.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Does an unhealthy diet cause bladder infection to flare up?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                        <div class="card-body">
                           The diet plays a pivotal role which ensuring healthy and disease-free living. Therefore, Ayurvedic Treatment for Interstitial Cystitis mainly focuses on adopting healthy diet choices along with herbal medications which aid in effective recovery.
                           <li>Foods that are suggested to be avoided are artificial sweeteners, spicy foods, caffeinated products, and acidic fruits.</li>
                           <li>Foods that are recommended to prevent cystitis must be rich in vitamin C, and calcium.</li>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                What can one do to relieve bladder pain symptoms?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                        <div class="card-body">
                            Some best and most effective tips are:
                            <li>Reduce stress.</li>
                            <li>Switch to a healthy diet.</li>
                            <li>Avoid wearing tight-fitted cloth.</li>
                            <li>Keep yourself physically active.</li>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                What measures should be taken to prevent bladder infection reoccurrence?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                        <div class="card-body">
                           The preventive measures are:
                           <li>Drink plenty of water.</li>
                           <li>Avoid holding pee for long.</li>
                           <li>Avoid scented personal hygiene products.</li>
                           <li>Wipe front to back.</li>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                               Is ginger good to prevent cystitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                        <div class="card-body">
                            Yes, because ginger possesses antimicrobial properties which act powerfully against bacterial strains. It is Ayurveda's suggested best herb to prevent bladder infection or cystitis.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Psoriasis",
    "item": "https://www.drshardaayurveda.com/cystitis"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "Which bacteria most commonly triggers bladder infection?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The bacteria Escherichia coli (E. coli) mainly trigger urinary tract infection. But, there are certain other bacteria also which can cause urinary tract infections."
    }
  },{
    "@type": "Question",
    "name": "Can stress cause bladder infection?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Disturbed mental health can greatly impact overall health. The bladder infection can get triggered and flared up if the mental and physical stability is not maintained well."
    }
  },{
    "@type": "Question",
    "name": "Which treatment is best to treat urinary tract infection?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurvedic Treatment for Cystitis is the best and most effective treatment for long-term and positive results. The urinary tract infection are the outcomes of the imbalanced Vata and Pitta Doshas. Ayurveda focuses on balancing these aggravated body energies through herbal medications and by adopting certain diet and lifestyle changes. It helps to flush out infections from the body and cleaning of urinary tract blockages."
    }
  },{
    "@type": "Question",
    "name": "What are the best tips suggested by Ayurveda to prevent urinary tract infection?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Some of the best Ayurvedic tips are:
•   Avoid food items that are cold and hard to digest.
•   Heat compression on the lower abdomen with a towel helps provide relief from pain.
•   Drink plenty of water that helps detox the body."
    }
  },{
    "@type": "Question",
    "name": "What are the complications associated with a persistent bladder infection?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "There are several serious health complications associated with cystitis including hematuria, acute renal failure, bladder rupture, and pyelonephritis."
    }
  },{
    "@type": "Question",
    "name": "Which hospital or clinic provides effective treatment for cystitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Dr. Sharda Ayurveda provides the best and most effective treatment for bladder infection. They provide root cause and specialized Ayurvedic treatment for cystitis which gives long-term recovery and help prevent the reoccurrence of the infection symptoms."
    }
  },{
    "@type": "Question",
    "name": "Does an unhealthy diet cause bladder infection to flare up?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The diet plays a pivotal role which ensuring healthy and disease-free living. Therefore, Ayurvedic Treatment for Interstitial Cystitis mainly focuses on adopting healthy diet choices along with herbal medications which aid in effective recovery. 
•   Foods that are suggested to be avoided are artificial sweeteners, spicy foods, caffeinated products, and acidic fruits. 
•   Foods that are recommended to prevent cystitis must be rich in vitamin C, and calcium."
    }
  },{
    "@type": "Question",
    "name": "What can one do to relieve bladder pain symptoms?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Some best and most effective tips are:
•   Reduce stress.
•   Switch to a healthy diet. 
•   Avoid wearing tight-fitted cloth.
•   Keep yourself physically active."
    }
  },{
    "@type": "Question",
    "name": "What measures should be taken to prevent bladder infection reoccurrence?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The preventive measures are:
•   Drink plenty of fluid. 
•   Avoid holding pee for long. 
•   Avoid scented personal hygiene products. 
•   Wipe front to back."
    }
  },{
    "@type": "Question",
    "name": "Is ginger good to prevent cystitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, because ginger possesses antimicrobial properties which act powerfully against bacterial strains. It is Ayurveda's suggested best herb to prevent bladder infection or cystitis."
    }
  }]
}
</script>

<style>
    .carousel-inner {position: relative;width: 100%;overflow: hidden;}
    .carousel-item {position: relative;float: left;width: 100%;margin-right: -100%;-webkit-backface-visibility: hidden;backface-visibility: hidden;transition: -webkit-transform .6s ease-in-out;transition: transform .6s ease-in-out;transition: transform .6s ease-in-out,-webkit-transform .6s ease-in-out;}
    .card-body {padding-top: 1.5rem;padding-bottom: 1.5rem;-webkit-border-radius: 0!important;border-radius: 0!important;flex: 1 1 auto;-ms-flex: 1 1 auto;}
    .card .card-body a h3{font-size: 18px;color: #ff9000;line-height: 1.4;}
    .card .card-body h3{color: #373433;font-size: 18px;    line-height: 1.4;}
    .font-weight-bold {font-weight: 700!important;}
    .card-title {margin-bottom: .75rem;}
    .card-img-top {width: 100%;border-top-left-radius: calc(.25rem - 1px);border-top-right-radius: calc(.25rem - 1px);}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            responsiveClass: !0,
            navigation : true,
            autoplay: true,
            loop: true,
            smartSpeed: 2500,
            singleItem : true,
            navigationText: ["<i class='fa fa-angle-left fa-2'></i>", "<i class='fa fa-angle-right fa-2'></i>"],
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                480: {
                    items: 1,
                    nav: !1
                },
                768: {
                    items: 2,
                    nav: !1
                },
                1000: {
                    items: 2,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1024: {
                    items: 3,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1366: {
                    items: 3,
                    nav: !1
                }
            }
        })
    });

</script>
@stop