<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Ankylosing Spondylitis</h3>
                    <p style="font-size: 18px;">Protect Your Joints with Timely Treatment</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/ankylosing-spondylitis">Ankylosing Spondylitis </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
<!--                     <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li> -->
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/Ankylosing-Spondylitis.webp') }}" class="why-choose-us img-fluid" alt="Ankylosing Spondylitis">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Ankylosing Spondylitis</b></h1>
                <p>
                    Ankylosing Spondylitis is a form of arthritis with inflammation in the spine which causes pain and stiffness in the back. "Ankylosis" means fused bones or other hard tissue and "Spondylitis" means inflammation in spinal bones or vertebrae. It usually starts with lower back pain. It also spreads up to the neck and can also damage these joints. Sometimes other body parts like the shoulder, hip, ribs, heels, and small joints of the hand and feet also get affected. Internal organs like the eye might show iritis i.e. inflammation of the iris. Significant features of lower <a class="green-anchor" href="https://www.drshardaayurveda.com/back-pain">back pain</a> are observed due to the involvement of sacroiliac joints. Usually, people of age late teens or 20s suffer from this disease. When inflammation occurs, it leads to extra production of calcium around bones hence bone growth and stiffness occurs, sometimes even bones get fused. This fusion makes the spine less flexible and can result in hunched forward posture.
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                    <p>
                        When ribs get affected, it causes difficulty in breathing deeply. In Ankylosing Spondylitis the ethereal fibrocartilage gets affected which causes synovitis in bone and hence original cartilage gets replace by bone through fusion. This fusion 
                        overall causes stiffness and immobility. The inflammation is immune-mediated and there is close relation with <a class="green-anchor"href="https://labtestsonline.org/tests/hla-b27#:~:text=HLA%2DB27%20is%20a%20specific,a%20person's%20white%20blood%20cells."rel=nofollow>HLA-B27</a>. No specific agent is known that triggers the onset of the disease, but there is interest to connect stomach infection (Irritable bowel syndrome) to synovial inflammation due to the interrelation between HLA-B27 and certain enteric bacteria. Ankylosing Spondylitis pain can be controlled with Ayurvedic medicines.
                    </p>
                    <div class="btn-banner crd">
                        <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                        </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Ankylosing Spondylitis </b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Genetics
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                If your parents or grandparents have Ankylosing spondylitis, you're more likely to develop it yourself.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Exertion
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                It is believed that excessive work and less rest also cause ankylosing spondylitis.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Bones Weakness
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                                Weakness of bones due to lack of nutrients, calcium, and vitamin D in food is the biggest reason for spondylitis.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Incorrect Posture
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                                The wrong way to sit or stand can lead you to the problem of spondylitis.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Presence of HLA-B27
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                                Genetic marker HLA-B27 is found almost in 95 percent of people suffering from this disease.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                   <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz px-2.5">
            <img src="{{ URL::asset('front/images/ASS-Symstoms.webp') }}" class="img-fluid"alt="Ankylosing Spondylitis Symptoms">
        </div>
        <div class="cvn sym px-4">
            <h3><span>Symptoms of</span> Ankylosing Spondylitis</h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Morning Stiffness</p>
            <p>Stiffness and pain in the lower back early in the morning that lasts at least 30 minutes.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Inflammation</p>
            <p>Pain and swelling are seen on fingers and toes.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Soreness in Heels</p>
            <p>Soreness of heels and in the arch of the foot.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Bodyache</p>
            <p>Severe pain and stiffness over multiple areas of the body for a longer period.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Fatigue</p>
            <p>A person remains in a state of tiredness almost every time.</p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Benefits of <b>Ankylosing Spondylitis</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Try walking as it increases bone mass and be physically active.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Always sleep on a comfortable bed. Keep in mind that the bed is neither too stiff nor too soft.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    People with ankylosing spondylitis should avoid the habit of placing a pillow under the neck or the feet. 
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurveda offers many excellent therapies for treatment for ankylosing spondylitis which includes detoxification by Panchkarma.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Massage the neck with warm sesame oil for 5 to 10 minutes, put a bandage of warm water there. It will decrease the swelling.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="ox-AqxY0FaY" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/ox-AqxY0FaY.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                What is a serious complication of ankylosing spondylitis? 
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                            It causes difficulty in breathing it reduced the ability to expand the chest and hence it causes ankylosing spondylitis.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Can you live a long life with ankylosing spondylitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                            Almost all people with ankylosing spondylitis can expect to lead normal and productive lives. Despite the chronic nature of the illness, only a few people with Ankylosing spondylitis will become severely disabled.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Do I need to follow a special diet to cure ankylosing spondylitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                            Yes, it is very important to follow a healthy diet plan to cure such disease and reduce weight, carrying extra weight adds stress to joints.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Describe the pain associated with ankylosing spondylitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                            Pain origin in the back from deep within the buttocks and is accompanied by morning stiffness. It worsens or gets better or stops completely at regular intervals.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Is heat or cold better for ankylosing spondylitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                            Heat is good for pain and stiffness in joints and muscles. Hence hot fermentation helps patients to get relief from pain in the morning. Cold reduces inflammation of joints.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    Can ankylosing spondylitis cause bowel problems?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                                Yes, it can develop bowel problems known as inflammatory bowel disease or colitis. Ayurvedic treatment for ankylosing spondylitis is best for a speedy recovery. 
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Will I have to take medications for ankylosing spondylitis lifetime?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                            The symptoms and the level of disease an individual suffers from depends on how long they have to keep taking Ayurvedic medicines. Ayurvedic medicines do take a little extra time to recover but treat disease from its root cause.
                            <a class="green-anchor" href="https://www.drshardaayurveda.com/">Ankylosing spondylitis treatment clinic</a> in India at Dr. Sharda Ayurveda is reliable and effective.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Does ankylosing spondylitis in pregnant women cause any complications to their baby?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                            Ankylosing spondylitis will not harm a baby in anyways. However, a pregnant woman is advised to take Ayurvedic medicines to control her symptoms but only after consulting an Ayurvedic expert before.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                What dietary precautions should be taken in ankylosing spondylitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                            Include more fruits and veggies in your daily diet. Add whole food and grains. 
                            avoid alcohol, sugar, sodium, and fat.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Does a sedentary lifestyle cause ankylosing spondylitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                            Yes, lack of physical activity, wrong posture, and prolonged sitting also cause ankylosing spondylitis.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Ankylosing Spondylitis",
    "item": "https://www.drshardaayurveda.com/ankylosing-spondylitis"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What is a serious complication of Ankylosing spondylitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It causes difficulty in breathing it reduced the ability to expand the chest and hence it causes Ankylosing spondylitis."
    }
  },{
    "@type": "Question",
    "name": "Can you live a long life with Ankylosing spondylitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Almost all people with Ankylosing spondylitis can expect to lead normal and productive lives. Despite the chronic nature of the illness, only a few people with Ankylosing spondylitis will become severely disabled."
    }
  },{
    "@type": "Question",
    "name": "Do I need to follow a special diet to cure Ankylosing Spondylitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it is very important to follow a healthy diet plan to cure such disease and reduce weight, carrying extra weight adds stress to joints."
    }
  },{
    "@type": "Question",
    "name": "Describe the pain associated with Ankylosing Spondylitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Pain origin in the back from deep within the buttocks and is accompanied by morning stiffness. It worsens or gets better or stops completely at regular intervals."
    }
  },{
    "@type": "Question",
    "name": "Is heat or cold better for Ankylosing Spondylitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Heat is good for pain and stiffness in joints and muscles. Hence hot fermentation helps patients to get relief from pain in the morning. Cold reduces inflammation of joints."
    }
  },{
    "@type": "Question",
    "name": "Can Ankylosing Spondylitis cause bowel problems?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it can develop bowel problems known as inflammatory bowel disease or colitis. Ayurvedic Treatment for Ankylosing Spondylitis is best for a speedy recovery."
    }
  },{
    "@type": "Question",
    "name": "Will I have to take medications for Ankylosing Spondylitis lifetime?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The symptoms and the level of disease an Individual suffers from depends on how long they have to keep taking Ayurvedic medicines. Ayurvedic medicines do take a little extra time to recover but treat disease from its root cause. Ankylosing Spondylitis treatment clinic in India at Dr. Sharda Ayurveda is reliable and effective."
    }
  },{
    "@type": "Question",
    "name": "Does Ankylosing Spondylitis in pregnant women cause any complications to their baby?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ankylosing Spondylitis will not harm a baby in anyways. However, a pregnant woman is advised to take Ayurvedic medicines to control her symptoms but only after consulting an Ayurvedic expert before."
    }
  },{
    "@type": "Question",
    "name": "What Dietary precautions should be taken in Ankylosing Spondylitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Include more Fruits and Veggies in your daily diet. Add whole food and grains. 
 Avoid Alcohol, Sugar, sodium, and fat."
    }
  },{
    "@type": "Question",
    "name": "Does a sedentary lifestyle cause Ankylosing Spondylitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, lack of physical activity, wrong posture, and prolonged sitting also cause Ankylosing Spondylitis."
    }
  }]
}
</script>
@stop