<?php
/**
 * Constipation Services Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4">Constipation</h3>
                    <p style="font-size: 15px;">Easy Bowel Movements with Ayurveda</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/constipation">Constipation</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/constipation 1.webp') }}" class="why-choose-us img-fluid" alt="constipation treatment">
                </div>
            </div>
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Constipation</b> Ayurvedic Treatment</h1>
                <p>
                    Constipation has become a serious disorder and is one of the most common health problems these days. According to a survey almost about <a class="green-anchor" href="https://www.abbott.in/media-center/press-releases/indian-adults-suffer-from-constipation.html"rel=nofollow><strong style="font-weight: bold;">22% of Indians suffer from constipation</strong></a>. Constipation is an irregular and incomplete evacuation of hard and dried stool. An individual faces problem to pass stool and it can be frustrating and also affect a person’s quality of life. Constipation refers to passing stool that are extremely hard and dry. Constipation is not just a state of troublesome but sometimes can be very painful too. Severe episodes of constipation can leads to problems like- anal fissure or growth of <a class="green-anchor" href="https://www.drshardaayurveda.com/piles">hemorrhoids</a>. Constipation is termed as Vibandh in Ayurveda. After having incompatible food, there is an increase of Vata and Pitta Dosha in body which causes constipation.
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                    <p>Elderly people are more seen sufferer of constipation due to changes in diet, medication and decreased mobility. Women are more prone to constipation in comparison to men. To get relieve choose Dr. Sharda Ayurvedic medicine for constipation. We have an expertise team of <strong>Ayurvedic doctors for constipation in India</strong>. 
                </p>
                <div class="btn-banner crd">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                        </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Constipation</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Obstruction in Intenstine
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Presence of hernia, stricture, TB in small and large intestine causes obstruction. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Dietary Factor
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                Less intake of fibers and Liquids in diet causes constipation. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Decreased Expulsive Power Due to Weakness of Abdominal Muscles
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                When movement of intestine decreases, stool does not passes from small to large intestine and finally rectum. 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Abnormal Neurological Control
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">
                                The nerves which control movement of intestine are hampered. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Spinal Pelvic Surgery
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                            <div class="card-body">
                                Due to surgery if there is damage to nerve controlling intestine movements. 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	

<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="advantages">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/Constipation_Symtoms.webp') }}" class="img-fluid"alt="Constipation symptoms">
        </div>
        <div class="cvn sym px-4" >
            <h3><span>Symptoms</span> of Constipation</h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Increased difficulty in bowel movement</p>
            <p>It gets very hard to pass stool and is quite painful. </p>
            <br>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Stomach ache</p>
            <p>one goes through severe stomach pain.</p>
            <br>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Bleeding during bowel movement</p>
            <p>If constipation gets worsen, bleeding begins while passing stool.</p>
            <br>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Nausea</p>
            <p>As stomach does not get clear up, there is a feeling of nausea often.</p>
            <br>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Loss of Appetite</p>
            <p>One does not feel like eating anything due to bloated stomach.</p>
        </div>
    </div>
</div>
<div class="split"></div>
<br>
<div  class="row mx-0 wrp pt-5 mt-5" data-aos="zoom-in" data-aos-duration="4000">
    <div id="hgte" class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
        <div class="row mx-0 p-4 mb-4" >
            <h3>Advantage of Ayurvedic Treatment for <span>Constipation</span></h3>
            <p class="im3">
            <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
            One of the best ways to get relieve from constipation is following a religious Vata balancing diet.</p>
            <p class="im3">
            <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
            <strong style="font-weight: bold;">Triphala-</strong> Triphala is a magical, most effective herbal remedy to treat constipation.</p>
            <p class="im3">
            <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
            Composition of milk and Ghee before going to bed helps in clearing stomach early inmorning and promotes smooth passing of stool. </p>
            <p class="im3">
            <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
            Engage yourself in maximum 30 minutes of workout that promotes digestion.</p>
            <p class="im3">
            <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
            Flax seeds and castor oil is a remedy that acts a laxative in constipation</p>
        </div>
    </div>
    <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="sR-v6exBo2s" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/sR-v6exBo2s.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
    </div>
</div>
<div class="split"></div>
<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Constipation Testimonial" >
                            <h3 class="usrname">Mahipal Singh</h3>
                            <p class="desig">Constipation Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                My grandfather at the age of 67 was survivor of constipation and he was in much pain. He had to daily take some tablet for passing stool but was not comfortable. We visited Dr. Sharda Ayurveda as we got to know about their herbal powder which treats chronic constipation too. Within few hours my grandfather got relieved and he is finally free from all the pain and gets fresh easily every morning.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Constipation Testimonial" >
                            <h3 class="usrname">Nikita</h3>
                            <p class="desig">Constipation Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Myself Nikita from Ludhiana was suffering from constipation since few months. I visited Dr. Sharda Ayurveda for my treatment and I am so grateful to them for the amazing treatment. I am all good now without any medications.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Constipation Testimonial" >
                            <h3 class="usrname">Rajesh Kumar</h3>
                            <p class="desig">Constipation Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                My problem of chronic constipation recovered almost immediately when I started with Ayurvedic treatment for constipation from Dr. Sharda Ayurveda. With slight dietary changes I could recover from my chronic constipation. 
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                How long does it take to clear out constipation by Ayurvedic treatment?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            Often it will go away on its own within a few days of Ayurvedic treatment followed by some dietary changes and following a workout routine.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Why do i feel constipated after pooping?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            It is due to incomplete evacuation of stool which is clinical symptom of constipation. In state of chronic constipation one feels constipated after passing stool too because of incomplete bowel movement.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Which Dosha causes constipation?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                            Ayurveda categorize constipation as a type of Vata disorder. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFive" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <i class="fa" aria-hidden="true"></i>
                                Do Ayurveda has permanent treatment of constipation?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="card-body">
                            Yes, at Dr. Sharda Ayurveda we have many natural formulation of Ayurvedic medicines which have tremendous result in constipation which have no side effects on health.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                How often should one poo?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                        <div class="card-body">
                            This is the most asked question. The frequency of bowel movement varies from 1 person to another depending upon their diet and workout pattern.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                What is the cost of treatment for constipation in Ayurveda?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                        <div class="card-body">
                            Cost of treatment for constipation depends on the type of Dosha and then treatment is suggested by our expert. Treatment in <a class="green-anchor"href="https://www.drshardaayurveda.com/blogs/7-effective-tips-to-improve-digestive-system">Ayurveda for constipation</a> is not quite expensive and is very effective. Constipation treatment in Ayurveda from Dr. Sharda has highly effective results.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                Which Yoga poses helps in aid Constipation?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                        <div class="card-body">
                            Some of the Yoga poses that helps relieve constipation are:
                            <ul>
                                <li>Sarvangasana</li>
                                <li>Vajrasana</li>
                                <li>Rikonasana</li>
                                <li>Janusirsasana</li>
                                <li>Bhujangasana</li>
                                <li>Balasana</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                How can I treat constipation at home?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            By drinking adequate water, eating fruits, salad and fiber promoting food in diet.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                What precautions should be taken to avoid Constipation?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                        <div class="card-body">
                            <ul>
                                <li>Avoid cold and soft drinks. </li>
                                <li>Regular exercising promotes healthy digestion that helps in easy bowel movements. </li>
                                <li>Avoid eating Red meat as it is hard to digest.</li>
                                <li>Promoting healthy lifestyle with correct diet and being in harmony with nature helps preventing constipation. </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Is drinking warm water good for constipation?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                        <div class="card-body">
                            Drinking hot water early in the morning for easy bowel movements and then drinking it usually helps to break down food faster which do not causes constipation.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')

<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Constipation",
    "item": "https://www.drshardaayurveda.com/constipation"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "How long does it take to clear out constipation by Ayurvedic treatment?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Often it will go away on its own within a few days of Ayurvedic treatment followed by some dietary changes and following a workout routine."
    }
  },{
    "@type": "Question",
    "name": "Why do I feel constipated after pooping?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It is due to incomplete evacuation of stool which is clinical symptom of constipation. In state of chronic constipation one feels constipated after passing stool too because of incomplete bowel movement."
    }
  },{
    "@type": "Question",
    "name": "How can I treat Constipation at home?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "By drinking adequate water, eating Fruits, Salad and fiber promoting food in diet."
    }
  },{
    "@type": "Question",
    "name": "Which Dosha causes constipation?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurveda categorize constipation as a type of Vata disorder."
    }
  },{
    "@type": "Question",
    "name": "Do Ayurveda has permanent treatment of constipation?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, at Dr. Sharda Ayurveda we have many natural formulation of Ayurvedic medicines which have tremendous result in constipation which have no Side effects on health."
    }
  },{
    "@type": "Question",
    "name": "How often should one poo?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "This is the most asked question. The frequency of Bowel movement varies from 1 person to another depending upon their Diet and workout pattern."
    }
  },{
    "@type": "Question",
    "name": "What is the cost of treatment for constipation in Ayurveda?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Cost of treatment for constipation depends on the type of Dosha and then treatment is suggested by our expert. Treatment in Ayurveda for constipation is not quite expensive and is very effective. Constipation treatment in Ayurveda from Dr. Sharda has highly effective results."
    }
  },{
    "@type": "Question",
    "name": "Which Yoga poses helps in aid Constipation?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Some of the Yoga poses that helps relieve constipation are:
Sarvangasana, Vajrasana, Trikonasana, Janusirsasana, Bhujangasana and Balasana"
    }
  },{
    "@type": "Question",
    "name": "What precautions should be taken to avoid Constipation?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "1- Avoid cold and soft drinks. 2- Regular exercising promotes healthy digestion that helps in easy bowel movements. 3- Avoid eating Red meat as it is hard to digest. 4- Promoting healthy lifestyle with correct diet and being in harmony with nature helps preventing constipation."
    }
  },{
    "@type": "Question",
    "name": "Is drinking warm water good for constipation?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Drinking hot water early in the morning for easy bowel movements and then drinking it usually helps to break down food faster which do not causes constipation."
    }
  }]
}
</script>
@stop