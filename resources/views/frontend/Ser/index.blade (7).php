<?php
/**
 * Contact Page 
 * 
 * @created    18/11/2021
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2021
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<?php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$routeName = \Route::currentRouteName();
?>
<style>.main-sec{width:100% !important;margin:0;}</style>
<div class='contact-us'>
    <div class="container-fluidd">
        <div class="row">
            <div class="col-lg-12 col-md-12 text-center">
                <a data-toggle="modal" data-target=".bd-example-modal-xl">
                    <img src="{{ URL::asset('front/images/treatment-banner.webp') }}" class="doctor img-fluid" alt="Ayurvedic Doctor Dr. Mukesh Sharda">
                </a>
            </div>
        </div>
    </div>

<!--     <section class="achivement_section">
        <div class="container">
            <div class="achivement">
			<div class="row row-cols-2" id="dynamic-cls">
                <div class="col-sm-6">
                    <div class="row row-cols-2">
                        <div class="col-md-3 col-xs-6">
							<div class="box">
								<div class="icon">
									<img class="achivement-icon" src="{{ URL::asset('front/images/rate.png') }}" title="Success Rate" alt="Success Rate">
								</div>
								<div class="text_box">
									<div class="numbers">90%</div>
									<div class="text">Success<br>Rate</div>
								</div>
							</div>
						</div>

                        <div class="col-md-3 col-xs-6">
							<div class="box">
								<div class="icon">
									<img class="achivement-icon" src="{{ URL::asset('front/images/clinics.png') }}" title="150+ Ayurvedic Clinics" alt="150+ Ayurvedic Clinics">
								</div>
								<div class="text_box">
									<div class="numbers">4</div>
									<div class="text">Clinics<br>Globally</div>
								</div>
							</div>
						</div>

                        <div class="col-md-3 col-xs-6">
							<div class="box">
								<div class="icon">
									<img class="achivement-icon" src="{{ URL::asset('front/images/doctors.png') }}" title="200+ Doctors Trained" alt="200+ Doctors Trained">
								</div>
								<div class="text_box">
									<div class="numbers">10+</div>
									<div class="text">Doctors<br>Trained</div>
								</div>
							</div>
						</div>

                        <div class="col-md-3 col-xs-6">
							<div class="box">
								<div class="icon">
									<img class="achivement-icon" src="{{ URL::asset('front/images/smile.png') }}" title="1.5M+ Happy Clients" alt="1.5M+ Happy Clients">
								</div>
								<div class="text_box">
									<div class="numbers">50K+</div>
									<div class="text">Happy<br>Clients</div>
								</div>
							</div>
						</div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="row row-cols-2">

                        <div class="col-md-3 col-xs-6">
							<div class="box">
								<div class="icon">
									<img class="achivement-icon" src="{{ URL::asset('front/images/review.png') }}" title="10K+ Total Reviews" alt="10K+ Total Reviews">
								</div>
								<div class="text_box">
									<div class="numbers">10K+</div>
									<div class="text">Reviews</div>
								</div>
							</div>
						</div>

                        <div class="col-md-3 col-xs-6">
							<div class="box">
								<div class="icon">
									<img class="achivement-icon" src="{{ URL::asset('front/images/consultation.png') }}" title="4.5M+ Consultation Done" alt="4.5M+ Consultation Done">
								</div>
								<div class="text_box">
									<div class="numbers">10K+</div>
									<div class="text">Consultation<br>Done</div>
								</div>
							</div>
						</div>

                        <div class="col-md-3 col-xs-6">
							<div class="box">
								<div class="icon">
									<img class="achivement-icon" src="{{ URL::asset('front/images/patient.png') }}" title="20M+ Active Groups" alt="20M+ Active Groups">
								</div>
								<div class="text_box">
									<div class="numbers">5K+</div>
									<div class="text">Active<br>Groups</div>
								</div>
							</div>
						</div>

                        <div class="col-md-3 col-xs-6">
							<div class="box">
								<div class="icon">
									<img class="achivement-icon" src="{{ URL::asset('front/images/excellence.png') }}" title="Years of Excellence" alt="Years of Excellence">
								</div>
								<div class="text_box">
									<div class="numbers">10+</div>
									<div class="text">Years of<br>Excellence</div>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
			</div>
        </div>
    </section> -->

    <section class="review_section" style="padding-bottom: 0 !important;">
        <div class="container">
			<div class="row">
          		<div class="review_box">
					<p class="review">
						<i>
							"The advantage of Ayurvedic treatment is that it yields side benefits, not side effects."
						</i>
					</p>
					<p class="name">- Dr. Mukesh Sharda</p>
				</div>
			</div>
        </div>
    </section>

    <section class="book_appointment_form" >
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 basic_info">
                    <div class="review_box">
                        <p class="review">For the past 9 years, Dr. Sharda Ayurveda is healing mankind worldwide with ancient effective Ayurvedic herbs. The highly professional experienced team of doctors are pioneers in pulse reading and experienced in treating a patient with Ayurvedic medicines that are formulated and manufactured under the guidance of experts after years of research.</p>
                        <p class="review">Dr. Mukesh Sharda, founder of Dr. Sharda Ayurveda and renowned <a class="green-anchor"href="https://www.drshardaayurveda.com/dr-mukesh-sharda">Ayurvedic Doctor in India</a> is crossing the bars daily by treating more and more people with Ayurveda. Successfully treated more than 10,00,000+ patients of several diseases related to joints, circulatory, kidney, skin, gynae, digestive, sexual, liver, endocrine, neurological, respiratory, urinary and de-addiction and many other acute and chronic diseases.</p>
                    </div>
                    <hr>
                    <button type="button" class="btn btn-primary book-button" data-toggle="modal" data-target=".bd-example-modal-xl" style="background-color:#d49925 !important">
                      Book a consultation with Dr Sharda Ayurveda
                    </button>
                </div>
            </div>
        </div>
    </section>

	<section class="our_treatment_section">
        <div class="container">
          	<div class="our_treatment">
				<div class="text-center">
					<h1 class="f-size2em"><b>AYURVEDIC TREATMENTS BY DR SHARDA AYURVEDA</b></h1>
				</div>
            	<div class="our_treatment_title">Expertise in treatment of all chronic diseases and more than 95% of patients relieved their condition within just 50 days of Ayurvedic treatment. <br>Suffering from any disease? Want a proven solution for it?</div>
				<div class="" style="text-align: center"><h4>Here you go and get the best-personalized treatment.</h4></div>
				<div class="type_row row">
					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="{{ route('rheumatoid-arthritis') }}">
							<div class="icon_box">
								<div class="icon">
									<img class="treatment-image" src="{{ URL::asset('front/images/rheumatoid-arthritis-icon.webp') }}">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Rheumatoid Arthritis</span>
							</div>
						</a>
					</div>

					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="{{ route('ankylosing-spondylitis') }}">
							<div class="icon_box">
								<div class="icon">
									<img class="treatment-image" src="{{ URL::asset('front/images/ankylosing_spondylitis-icon.webp') }}">
								</div>
								<div class="clr"></div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Ankylosing Spondylitis</span>
							</div>
						</a>
					</div>

					<div class="col-md-3 col-sm-6 col-12 treatment">
	                    <a href="{{ route('back-pain') }}">
    						<div class="icon_box">
    							<div class="icon">
    								<img src="{{ URL::asset('front/images/back-pain-icon.webp') }}">
    							</div>
    						</div>
    						<div class="clr"></div>
    						<div class="icon-name">
    							<span>Back Pain</span>
    						</div>
	                    </a>
					</div>

					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="{{ route('cervical') }}">
							<div class="icon_box">
								<div class="icon">
									<img class="treatment-image" src="{{ URL::asset('front/images/cervical-icon.webp') }}">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Cervical Spondylosis</span>
							</div>
						</a>
					</div>
					<div class="clr"></div>
				</div>

				<div class="type_row row">
					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="{{ route('fibromyalgia') }}">
						<div class="icon_box">
							<div class="icon">
								<img class="treatment-image" src="{{ URL::asset('front/images/fibromyalgia-icon.webp') }}">
							</div>
						</div>
						<div class="clr"></div>
						<div class="icon-name">
							<span>Fibromyalgia</span>
						</div>
						 </a>
					</div>

					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="{{ route('gout') }}">
							<div class="icon_box">
								<div class="icon">
									<img src="{{ URL::asset('front/images/gout-icon.webp') }}">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Gout</span>
							</div>
						</a>
					</div>

					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="{{ route('hip-joint-pain') }}">
							<div class="icon_box">
								<div class="icon">
									<img class="treatment-image" src="{{ URL::asset('front/images/hip-joint-icon.webp') }}">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Hip Joint Pain</span>
							</div>
						</a>
					</div>

					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="{{ route('knee-pain') }}">
							<div class="icon_box">
								<div class="icon">
									<img class="treatment-image" src="{{ URL::asset('front/images/knee-pain-icon.webp') }}">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Knee Pain</span>
							</div>
						</a>
					</div>
					<div class="clr"></div>
				</div>

				<div class="type_row row">
					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="{{ route('lumbar-spondylosis') }}">
							<div class="icon_box">
								<div class="icon">
									<img class="treatment-image" src="{{ URL::asset('front/images/lumbar-spondylosis-icon.webp') }}">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Lumbar Spondylosis</span>
							</div>
						</a>
					</div>

					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="{{ route('neck-pain') }}">
							<div class="icon_box">
								<div class="icon">
									<img class="treatment-image" src="{{ URL::asset('front/images/neck pain-icon.webp') }}">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Neck Pain</span>
							</div>
						</a>
					</div>

					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="{{ route('osteoarthritis') }}">
						<div class="icon_box">
							<div class="icon">
								<img class="treatment-image" src="{{ URL::asset('front/images/osteoarthritis-icon.webp') }}">
							</div>
						</div>
						<div class="clr"></div>
						<div class="icon-name">
							<span>Osteoarthritis</span>
						</div>
						</a>
					</div>

					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="{{ route('osteoporosis') }}">
							<div class="icon_box">
								<div class="icon">
									<img class="treatment-image" src="{{ URL::asset('front/images/osteoporosis-icon.webp') }}">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Osteoporosis</span>
							</div>
						</a>
					</div>
					<div class="clr"></div>
				</div>

				<div class="type_row row">
					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="{{ route('post-viral-arthritis') }}">
							<div class="icon_box">
								<div class="icon">
									<img class="treatment-image" src="{{ URL::asset('front/images/post-viral-arthritis-icon.webp') }}">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Post-Viral Arthritis</span>
							</div>
						</a>
					</div>

					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="{{ route('psoriatic-arthritis') }}">
							<div class="icon_box">
								<div class="icon">
									<img src="{{ URL::asset('front/images/psoriatic-arthritis-icon.webp') }}">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Psoriatic Arthritis</span>
							</div>
						</a>
					</div>

					<div class="col-md-3 col-sm-6 col-12 treatment">
                        <a href="{{ route('reactive-arthritis') }}">
						<div class="icon_box">
							<div class="icon">
								<img class="treatment-image" src="{{ URL::asset('front/images/reactive-arthritis-icon.webp') }}">
							</div>
						</div>
						<div class="clr"></div>
						<div class="icon-name">
							<span>Reactive Arthritis</span>
						</div>
						</a>
					</div>

					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="{{ route('uric-acid') }}">
							<div class="icon_box">
								<div class="icon">
									<img src="{{ URL::asset('front/images/uric-acid-icon.webp') }}">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Uric Acid</span>
							</div>
						</a>
					</div>
					<div class="clr"></div>
				</div>

				<div class="type_row row">
					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('anemia') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/anemia-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Anemia</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('blood-pressure') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/blood-pressure-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Blood Pressure</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('high-cholesterol') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/high-cholesterol-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>High Cholesterol</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('coronary-artery-disease') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/coronary-artery-disease-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Coronary Artery Disease</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('alcohol-de-addiction') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/alcohol-de-addiction-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Alcohol De-Addiction</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('drug-de-addiction') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/drug-deaddiction-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Drug De-Addiction</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('smoking-de-addiction') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/smoking-deaddiction-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Smoking De-addiction</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('abdominal-pain') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/abdominal-pain-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Abdominal Pain</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('acidity') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/acidity-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Acidity</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('chronic-fatigue-syndrome') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/chronic-fatigue-syndrome-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Chronic Fatigue Syndrome</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('constipation') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/constipation-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Constipation</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('flatulence') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/flatulance-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Flatulence</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('ibs') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/ibs-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Irritable Bowel Syndrome (IBS)</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('piles') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/piles-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Piles</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('ulcerative-colitis') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/ulcerative-colitis-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Ulcerative Colitis</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('diabetes') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/diabetes-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Diabetes</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('obesity') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/obesity-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Obesity</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('thyroid') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/thyroid-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Thyroid</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('leucorrhoea') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/leucorrhoea-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Leucorrhoea</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('menopause') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/menopause-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Menopause</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('pcod-pcos') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/pcod-pcos-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Polycystic Ovarian Disease</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('kidney-failure') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/kidney-failure-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Kidney Failure</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('kidney-stones') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/kidney-stone-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Kidney Stones</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('fatty-liver') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/fatty liver-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Fatty Liver</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('jaundice') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/jaundie-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Jaundice</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('liver-cirrhosis') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/liver-cirrhosis-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Liver Cirrhosis</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('anxiety-disorder') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/anxiety-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Anxiety Disorder</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('depression') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/depression-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Depression</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('insomnia') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/insomnia-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Insomnia</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('migraine') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/migraine-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Migraine</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('sciatica-pain') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/sciatica-pain-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Sciatica Pain</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('asthma') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/asthma-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Asthma</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('sinusitis') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/sinusitis-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Sinusitis</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('rhinitis') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/rhinitis-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Rhinitis</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('chronic-laryngitis') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/chronic-laryngitis-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Chronic Laryngitis</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('erectile-dysfunction') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/erectile disfunction-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Erectile Dysfunction</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('oligospermia') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/oligospermia-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Oligospermia</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('premature-ejaculation') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/premature-ejaculation-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Premature Ejaculation</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('eczema') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/eczema-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Eczema</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('fungal-infection') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/fungal infection-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Fungal Infection</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('psoriasis') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/psoriasis-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Psoriasis</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="{{ route('urinary-tract-infection') }}">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="{{ URL::asset('front/images/urinary-tract-infection-icon.webp') }}">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Urinary Tract Infection (UTI)</span>
							  </div>
						  </a>
					  </div>
					  <div class="clr"></div>
				</div>
			</div>
		</div>
	</section>

	<section class="sign_up_newsletter">
        <div class="container">
          	<div class="row">
			  	<div class="col-md-5 col-sm-5 col-xs-12">
					<div class="img_box">
						<img src="{{ URL::asset('front/images/nadi-parikshan-2.webp') }}" class="why-choose-us img-fluid" alt="nadi parikshan">
					</div>
				</div>
				<div class="col-md-7 col-sm-7 col-xs-12 ">
					<div class="newsletter_box">
						<div class="title">Analyze Your Dosha</div>
						<div class="form_box">
							<form id="form_contact" action="https://www.drshardaayurveda.com/submit_lp_page.php" method='POST'>
								<input type='hidden' name="subject" value="<?= $routeName; ?>">
								<input type='hidden' name="action" value="user_mail">
								<input type='hidden' name="return_url" value="https://www.drshardaayurveda.com/thanks.html">
								<input type='hidden' name="page_url" value="<?= $actual_link; ?>">
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-12">
										<div class="">
											<label for="fname">Name*
												<abbr class="required" title="required">*</abbr>
											</label>
											<div class="error form_error" id="form-error-fname"></div>
											<input type="text" class="input-text" placeholder="name" name="name" id="fname" required="">
										</div>
									</div>

									<div class="col-md-6 col-sm-6 col-xs-12">
										<div class="form-row-first">
											<label for="email">E-mail*
												<abbr class="required" title="required">*</abbr>
											</label>
										<div class="error form_error" id="form-error-email"></div>
										<input type="email" class="input-text" placeholder="email" name="email" id="email" required="">
									</div>
								</div>

							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="">
									<label for="mobile">Mobile*
										<abbr class="required" title="required">*</abbr>
									</label>
									<div class="error form_error" id="form-error-mobile"></div>
									<input type="number" class="input-text" placeholder="Mobile" name="mobile" id="mobile" required="">
								</div>
							</div>

							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="">
									<label for="mobile">Description*
										<abbr class="required" title="required">*</abbr>
									</label>
									<div class="error form_error" id="form-error-mobile"></div>
									<textarea rows="1" cols="50" name="description" id="comment" placeholder="Message" rows="9"></textarea>
								</div>
							</div>

							<div class="clr"></div>

							<div class="col-md-6 col-sm-6 col-xs-12" style="text-align:center">
								<div class="form-row">
									<input type="submit"  class="btn btn-lg btn-color Register __web-inspector-hide-shortcut__" id="Register" value="Submit">
								</div>
							</div>
						</div>
								<div class="clr"></div>
							</form>
						</div>
            		</div>
          		</div>
			</div>
        </div>
	</section>
</div>

<script>
  jQuery(document).ready(function($) {
  	var alterClass = function() {
		var ww = document.body.clientWidth;
		if (ww < 768) {
			console.log('here');
			$('#dynamic-cls').removeClass('row row-cols-2');
		} else if (ww >= 769) {
			$('#dynamic-cls').addClass('row row-cols-2');
		};
	};
	$(window).resize(function(){
		alterClass();
	});
  	//Fire it when the page first loads:
  	alterClass();
});
</script>

@stop