<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Irritable Bowel Syndrome (IBS)</h3>
                    <p style="font-size: 15px;">Not just symptomatic, Get Root cause treatment for IBS</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/ibs">IBS</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/IBS.webp') }}" class="why-choose-us img-fluid" alt="IBS Treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Irritable Bowel Syndrome (IBS)</b> Ayurvedic Treatment</h1>
                <p>
                    Irritable Bowel Syndrome (IBS) is a digestive disorder and in this era when people of all age groups are too much into junk and unhygienic food and disturbed lifestyle usually complain about IBS, which disturbs the digestive system. Irritable bowel syndrome affects daily bowel functioning. An individual is not able to digest even a small amount of food. After repeated defecation also they do not get the satisfaction of clear bowel movement. In Ayurveda, IBS is called as <strong style="font-weight: bold;">Grahani</strong>. Inflammation in the intestine leads to irritable bowel syndrome. In IBS the small intestine somehow gets structurally as well as functionally contaminated and hence the digestion gets disturbed.
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                    <p>
                        Ayurveda experts evaluate the Prakriti (Vata, Pitta, Kapha) of a patient before beginning with Ayurvedic treatment for irritable bowel syndrome and provide with best of effective Ayurvedic medicines which have no side- effects on health followed by Panchakarma with some dietary changes i.e. fasting helps in recovery of a patient soon. 
                    </p>
                    <div class="btn-banner crd">
                        <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                        </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Irritable Bowel Syndrome (IBS) </b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Irregular Eating Habits
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Irregular and unhealthy eating habits lead to irritable bowel syndrome.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Eating at Irregular Intervals
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                Not consuming meals on time and taking them at odd timings can also cause IBS.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Excessive Intake of Junk and Acidic Food
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                                Overconsumption of junk and acidic food causes irritable bowel syndrome.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Excessive Intake of Spices
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                                Over consumption of Spices in food leads to slower bowel movement and hence causes IBS.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Excessive Consumption of Alcohol
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                                High intake of alcohol causes irritable bowel syndrome. 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/IBS-Symptoms.webp') }}" class="img-fluid"alt="IBS symptoms">
        </div>
        <div class="cvn sym" >
            <h3>Symptoms of <span>Irritable Bowel Syndrome (IBS)</span></h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Irregular Bowel Function</p>
            <p>In IBS a person suffers from irregular bowel movements.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Constipation</p>
            <p>A person is unable to freely pass stool.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Abdominal Pain</p>
            <p>Due to the disturbance in the passing of stool, one goes through abdominal pain too. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Bloating in Abdomen or Gas</p>
            <p>One is unable to digest food so suffers from gastric issues.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Excessive Thirst</p>
            <p>An individual always feels thirsty.</p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Ayurveda Advantages For <b>Irritable Bowel Syndrome (IBS)</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Practice yoga for all body-wellness.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Try out meditation and massage therapies to reduce stress.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    It not only heals the lining of the intestine but also relieve mental stress caused by irritable bowel syndrome (IBS).
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    It solves all the problems related to Agni (Fire). It controls all the gastric enzyme secretion needed for good digestion.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    It also heals the body by providing strength through medicines. It covers up all the deficiencies that occurred due to IBS.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="G2SXEXmtMqY" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/G2SXEXmtMqY.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
    </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="IBS Testimonial">
                            <h3 class="usrname">Harbhajan Kaur</h3>
                            <p class="desig">IBS Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                My younger sister has been struggling with irritable bowel syndrome for many years. Her IBS symptoms always used to shift between <a class="green-anchor" href="https://www.drshardaayurveda.com/constipation">constipation</a> and diarrhea. She tried several medications, several doctors, and natural remedies, but none of them worked. We began with treatment from Dr. Sharda Ayurveda clinic and she has seen huge changes in her health. By following the regular medications and diet, now all of her symptoms of IBS are gone.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="IBS Testimonial" >
                            <h3 class="usrname">Ram Kumar</h3>
                            <p class="desig">IBS Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I have been suffering from Irritable Bowel Syndrome for the past 2 years. I had consulted so many doctors and have also gone through many treatments. I switched from Allopathy to Ayurvedic treatment. After Ayurvedic treatment from Dr. Sharda Ayurveda, I am finally back to normal life after 4 months of their treatment. I would suggest anyone suffering from irritable bowel syndrome, should consider the Ayurvedic treatment. 
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="IBS Testimonial" >
                            <h3 class="usrname">Manjinder Kaur</h3>
                            <p class="desig">IBS Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Thankfully, I am no longer suffering from Irritable Bowel Syndrome and my health is stable after taking Ayurvedic medicines from Dr. Sharda Ayurveda. I took medicines for approximately 3 months and I am not taking medicines currently but maintaining my diet prescribed by them. Thanks to Dr. Sharda Ayurveda for the best medicines and advice. 
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1"><b>FAQ'S</b></h2>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                What type of treatment is available in IBS?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            Ayurvedic Medicines with dietary changes followed by Panchakarma therapy. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                How to treat abdominal pain in IBS with Home Remedy?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            Take a teaspoon of Ajwain along with warm water twice a day to get relief from abdominal pain. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Does IBS has a co-relation with the mind?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            Yes, it is co-related with a mind as the motor nerve gets activated which is responsible for Peristaltic movement in the abdomen.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                I have Abdominal pain for few days and I pass a stool after every meal. Are following the clinical signs of IBS?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                            Yes, Stool with mucus, abdominal pain, and motion after every meal is the Sign of IBS.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion2">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    Should one eliminate dairy products to treat IBS?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion2">
                            <div class="card-body">
                                There is no such specification to eliminate Dairy products rather it can affect the amount of calcium intake
                                in the body. Consult your specialist according to the symptoms.
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Do Spicy foods affect IBS? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion2">
                        <div class="card-body">
                            There is no such specification that Spicy food affects the symptoms of IBS. But if they create any problem or provoke 
                            the symptoms then one should avoid taking them.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                What can untreated IBS lead to?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion2">
                        <div class="card-body">
                            Untreated IBS leads to long-term abdominal pain and can also lead to anxiety or depression. It may lead to pain and discomfort in the abdominal region.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Is IBS permanent?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion2">
                        <div class="card-body">
                            IBS does not cause damage to the digestive system. There is a sure cure for IBS in Ayurveda. Timely treatment of symptoms of IBS can stop the disease from getting chronic.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                How to distinguish between IBS and IBD?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion2">
                        <div class="card-body">
                            IBS and IBD are both chronic conditions that cause <a class="green-anchor"href="https://www.drshardaayurveda.com/abdominal-pain">abdominal pains</a>, sometimes cramping, and urgent bowel movements. 
                            IBS affects the small and large intestines with the colon whereas IBD causes inflammation in the intestines.
                            IBD can be genetic too. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Can IBS be treated without a treatment?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion2">
                        <div class="card-body">
                            IBS is not a disease to be ignored. It becomes chronic if not treated timely. It can never be treated without a proper
                            course of Ayurvedic treatment. Lifestyle and dietary changes may help in reducing symptoms but proper treatment is required
                            to treat it so that there are no more complications in the future.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "IBS",
    "item": "https://www.drshardaayurveda.com/ibs"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What type of treatment is available in IBS?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurvedic Medicines with dietary changes followed by Panchakarma therapy."
    }
  },{
    "@type": "Question",
    "name": "How to treat abdominal pain in IBS with Home Remedy?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Take a teaspoon of Ajwain along with warm water twice a day to get relief from abdominal pain."
    }
  },{
    "@type": "Question",
    "name": "Does IBS has a co-relation with the mind?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it is co-related with a mind as the motor nerve gets activated which is responsible for Peristaltic movement in the abdomen."
    }
  },{
    "@type": "Question",
    "name": "I have Abdominal pain for few days and I pass a stool after every meal. Are following the clinical signs of IBS?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, Stool with mucus, abdominal pain, and motion after every meal is the Sign of IBS."
    }
  },{
    "@type": "Question",
    "name": "Should one eliminate dairy products to treat IBS?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "There is no such specification to eliminate Dairy products rather it can affect the amount of calcium intake in the body. Consult your specialist according to the symptoms."
    }
  },{
    "@type": "Question",
    "name": "Do Spicy foods affect IBS?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "There is no such specification that Spicy food affects the symptoms of IBS. But if they create any problem or provoke the symptoms then one should avoid taking them."
    }
  },{
    "@type": "Question",
    "name": "What can untreated IBS lead to?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Untreated IBS leads to long-term abdominal pain and can also lead to anxiety or depression. It may lead to pain and discomfort in the abdominal region."
    }
  },{
    "@type": "Question",
    "name": "Is IBS permanent?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "IBS does not cause damage to the digestive system. There is a sure cure for IBS in Ayurveda. Timely treatment of symptoms of IBS can stop the disease from getting chronic."
    }
  },{
    "@type": "Question",
    "name": "How to distinguish between IBS and IBD?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "IBS and IBD are both chronic conditions that cause abdominal pains, sometimes cramping, and urgent bowel movements.IBS affects the small and large intestines with the colon whereas IBD causes inflammation in the intestines. IBD can be genetic too."
    }
  },{
    "@type": "Question",
    "name": "Can IBS be treated without a treatment?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "IBS is not a disease to be ignored. It becomes chronic if not treated timely. It can never be treated without a proper course of Ayurvedic treatment. Lifestyle and dietary changes may help in reducing symptoms but proper treatment is required to treat it so that there are no more complications in the future."
    }
  }]
}
</script>
@stop