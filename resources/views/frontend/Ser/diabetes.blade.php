<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4">Diabetes</h3>
                    <p style="font-size: 24px;">Don't Let Diabetes Harm Your Body</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/diabetes">Diabetes</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a  href="#types">TYPES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/diabities 2.webp') }}" class="why-choose-us img-fluid" alt="why choose">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Diabetes</b> Ayurvedic Treatment</h1>
                <p>
                    Diabetes is a condition called Madhumeha in Ayurveda. <strong style="font-weight: bold;">Madhumeha</strong> (Madhu- sweet; Meha-to flow or urination) it is a condition in which a person passes honey-like urine. Reduced insulin production leads to this disease. Dr. Sharda Ayurveda experts say that intake of excess food like heavy, salty, oily, and sour are contributing factors of diabetes (Madhumeha). It is a metabolic disorder that is due to imbalanced Kapha Dosha and increased fat in the body. Poor digestion gives birth to diabetes. Diabetes is becoming the biggest health concern in today’s era and one in each house is a survivor. “The less active you are, the greater is the risk of you being diabetic”. Adopt Ayurveda for healthy living.
                </p>
                <div class="btn-banner crd">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                        </div>
            </div>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec" id="types"> 
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="heading1">Types of <b>Diabetes</b></h2>
            </div>
        </div>
        <div class="row cust-iba">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                <div class="iba-box box1 dib1">
                    <div class="xd">
                        <div class="cont">	
                            <div class="wht-space"></div>
                            <h3>Insulin Dependent</h3>
                            <p>
                                In type 1 diabetes body fails to produce insulin. People suffering from type 1 diabetes are insulin-dependent, which implies they are dependent on insulin.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                <div class="iba-box box2 dib2">
                    <div class="xd">
                        <div class="cont">
                            <div class="wht-space"></div>
                            <h3>Non Insulin Dependent</h3>
                            <p>
                                Type 2 diabetes affects the way the body uses insulin.
                                The cells in the body get weak and do not respond effectively as they used to do earlier. This is mostly caused due to <a class="green-anchor"href="https://www.drshardaayurveda.com/obesity">obesity</a> and also affects kidneys and the liver.
                                </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!--         <div class="row">
            <div class="col-md-12">
                <div class="btn-ayurvedic">
                    <a href="#" class="btn-appointment">Find More Treatment</a>
                </div>
            </div>
        </div> -->
    </div>
</section>



<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Diabetes</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Excessive intake of dairy products
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                As said “Excess of everything is bad”. So overconsumption of dairy products can cause diabetes.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    High Blood Pressure
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                If blood pressure fluctuates and is not stable it also causes diabetes.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Family history can also cause diabetes
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                Diabetes is hereditary. It can be passed down through the genes.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Excessive Sleep
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">
                                No work and only sleep can be the biggest invite to diabetes.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Overweight
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                            <div class="card-body">
                                More the fatty tissues, the more resistant cells become to insulin. 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/Diabetes-Symptoms.webp') }}" class="img-fluid">
        </div>
        <div class="cvn sym" >
            <h3>Symptoms of <span>Diabetes</span></h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Increased Thirst</p>
            <p>One is always in a state of thirst. Even after drinking water, one is thirsty.</p>
            <br>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Frequent Urination</p>
            <p>Urge to urinate always is the biggest symptom of diabetes.</p>
            <br>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Fatigue</p>
            <p>An individual is always in a state of restlessness and tiredness.</p>
            <br>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Unexplained Weight Loss</p>
            <p>Serious weight loss issues can be seen in diabetic patients.</p>
            <br>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Blurred Vision</p>
            <p>Vision capability gets lowered if diabetes hits hard. </p>
        </div>
    </div>
</div>
<div class="split"></div>


<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Advantages of <b>Ayurvedic Treatment For Diabetes</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    <strong style="font-weight: bold;">Natural Ayurvedic ingredients e.g.-</strong> Amla and Turmeric help in improving metabolism, therefore reducing cellular resistance.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    <strong style="font-weight: bold;">Detoxification-</strong>Firstly, Ayurveda focuses on improving metabolism and remove blockages through a special anti-clog detoxifying treatment.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    <strong style="font-weight: bold;">Repairing and restoring-</strong> Secondly, Ayurveda focuses on the rejuvenation process with Rasayana herbs to improve and strengthen the pancreas functions and renew the cells and tissues. Therefore, restoring the normal functions of all the body organs.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    <strong style="font-weight: bold;">Maintenance-</strong> Thirdly, Ayurveda focuses on maintaining blood sugar levels naturally under control. Ayurvedic treatment with diet plans is recommended to control blood sugar levels naturally throughout life.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    <a class="green-anchor" href="https://www.drshardaayurveda.com/diabetes">Ayurvedic treatment for Diabetes</a> is effective that even an insulin dependent patient can slowly get back to normal sugar levels.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="O5fbnGSGaVQ" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/O5fbnGSGaVQ.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Patient <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Gurdeep Singh</h3>
                            <p class="desig">Diabetes patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p><span><i class="fa fa-quote-left" aria-hidden="true"></i>
                                </span>Myself Gurdeep Singh from Ropar. I am a sugar patient and was insulin-dependent for 8 months and was under prescribed Ayurvedic medicines for diabetes from Dr. Sharda Ayurveda. I was amazed by the results of Ayurvedic medicines that helped me in speedy healthy recovery.<span><i class="fa fa-quote-right" aria-hidden="true"></i>
                                </span></p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Harsimran Kaur</h3>
                            <p class="desig">Diabetes patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I visited <b>Dr. Mukesh Sharda</b> Ayurvedic doctor for diabetes treatment before 2 months. I was impressed by the way she treats her patients with smooth Ayurvedic treatment and diet changes. I took medication for almost 2 months and I am in my healing stage. I am very thankful to her.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Mrs Preet Kaur</h3>
                            <p class="desig">Diabetes patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Dr. Sharda Ayurveda has a team of expert Ayurvedic doctors for <b>diabetes treatment in India</b>. My 25 years old son was detected with diabetes. Without any doubt, I visited Dr. Sharda Ayurveda for treatment and I cannot thank them enough. Because of them my son is diabetic free in younger age only and is Healthy.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1"><b>FAQ'S</b></h2>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                Is Ayurveda safe for diabetes?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            Ayurveda offers natural and effective medicines to manage diabetes. Clinical studies show that herbal medicines when combined with lifestyle and dietary changes can bring remarkable results in diabetes.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Which food does one need to avoid in diabetes?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            Sugar (sweetened beverages), trans fats, white bread, Rice, Pasta, Fruit flavored Yogurt, flavored coffee drink, and Maple syrup, etc.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Can diabetes be treated without medicine?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            Dietary changes, weight loss may lead to management of type 2 diabetes but this is not true for type 1 diabetes. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                How much water can a diabetic patient take?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                            Men can drink 12-13 cups per day and women can drink 9-10 cups.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFive" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <i class="fa" aria-hidden="true"></i>
                                Is it possible to reverse diabetes? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="card-body">
                            Through positive lifestyle choices and under proper care with Ayurvedic medications, expert physician, diabetes can be managed and one can live a normal life. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                What is the main cause of diabetes?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse show" aria-labelledby="headingSix" data-parent="#accordion">
                        <div class="card-body">
                            Excessive weight, family history, old age, polycystic ovary syndrome, high blood pressure, abnormal cholesterol, and triglycerides levels are the main causes of diabetes.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                How do I lower my sugar immediately?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                        <div class="card-body">
                            Consistent exercises, managed diet plan, proper hydration, following doctor advice, one can decrease sugar immediately.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                What can one do to prevent diabetes?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                        <div class="card-body">
                            For the prevention of diabetes, one needs to follow up a routine of physical efforts every day, maintaining a well-balanced and healthy diet that has low-fat content and contains very few calories. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                How to manage diabetes?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                        <div class="card-body">
                            The most important part of managing diabetes is diet, one should follow a healthy, balanced, and well-nourished diet. A person suffering from diabetes should exercise regularly and stay active. Take medications as prescribed. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                If it is sugar-free food, can I eat as much as I want?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                        <div class="card-body">
                            Sugar-free food can only be a part of a healthy meal plan in small amounts. Always keep in mind that some of the food items still have carbs and can affect blood glucose levels. Many sugar-free foods have calories and carbohydrates and lots of fat in them. Make sure you read the nutrition labels before consuming. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Diabetes",
    "item": "https://www.drshardaayurveda.com/diabetes"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "Is Ayurveda safe for Diabetes?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurveda offers natural and effective medicines to manage Diabetes. Clinical studies show that Herbal Medicines when combined with Lifestyle and Dietary changes can bring remarkable results in diabetes."
    }
  },{
    "@type": "Question",
    "name": "Which food one need to avoid in Diabetes?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Sugar (sweetened beverages), trans fats, white bread, Rice, Pasta, Fruit flavored Yogurt, flavored Coffee drink, and Maple syrup, etc."
    }
  },{
    "@type": "Question",
    "name": "Can Diabetes be treated without medicine?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Dietary changes, weight loss may lead to management of type 2 diabetes but this is not true for type 1 Diabetes."
    }
  },{
    "@type": "Question",
    "name": "How much water can a Diabetic patient take?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Men can drink 12-13 cups per day and women can drink 9-10 cups."
    }
  },{
    "@type": "Question",
    "name": "Is it possible to reverse Diabetes?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Through positive lifestyle choices and under proper care with Ayurvedic Medications ,expert physician, diabetes can be managed and one can live a normal Life."
    }
  },{
    "@type": "Question",
    "name": "What is the main cause of Diabetes?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Excessive Weight , Family history, Old age, polycystic ovary syndrome, High blood pressure, abnormal cholesterol, and triglycerides levels are the main causes of diabetes."
    }
  },{
    "@type": "Question",
    "name": "How do I lower my sugar immediately?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Consistent exercises, managed diet plan, proper hydration, following doctor advice, one can decrease sugar immediately."
    }
  },{
    "@type": "Question",
    "name": "What can one do to prevent Diabetes?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "For the prevention of diabetes, one needs to follow up a routine of physical efforts every day, maintaining a well-balanced and healthy diet that has low-fat content and contains very few calories."
    }
  },{
    "@type": "Question",
    "name": "How to manage Diabetes?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The most important part of managing diabetes is Diet, one should follow a healthy, balanced, and well-nourished diet. A person suffering from Diabetes should exercise regularly and stay active. Take medications as prescribed."
    }
  },{
    "@type": "Question",
    "name": "If it is sugar-free food, can I eat as much as I want?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Sugar-free food can only be a part of a healthy meal plan in small amounts. Always keep in mind that some of the food items still have carbs and can affect blood glucose levels. Many sugar-free foods have calories and carbohydrates and lots of fat in them. Make sure to check the nutrition labels before consuming."
    }
  }]
}
</script>
@stop