<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Kidney Failure</h3>
                    <p style="font-size: 15px;">Get Healthy Kidneys with Ayurveda</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/kidney-failure">Kidney Failure</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatments">TREATMENTS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="definition">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/kidney-failure.webp') }}" class="why-choose-us img-fluid" alt="Kidney failure">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1">What is <b>Kidney Failure</b></h1>
                <p>
                    Kidney diseases are becoming the 9th leading cause of death. It is also termed a silent killer disease. Every year about more than 2 lakhs of people is diagnosed with renal disease in a year in India. Renal failure or kidney failure means when kidneys no longer work properly as they did in a healthier state earlier. Renal failure can be temporary or permanent or can be acute or chronic. When the kidney function hinders suddenly it is called temporary or acute renal failure. When kidney functioning gets worse slowly, it is chronic renal failure. Kidney failure occurs when kidneys lose the ability to sufficiently filter waste material from blood and balance fluid. Kidneys are bean-shaped organs that remove waste products and extra water from the body and help in the production of red blood cells and controlling blood pressure.
                <span id="dots" data-aos="fade-up" data-aos-duration="3000">...</span>
                </p>
                <span id="more-content">
                    <p>
                        The kidney is also responsible for the erythropoietin hormone and synthesis of Vitamin D. Two major causes like Diabetes and high blood pressure lead to failure of kidney function. Kidney Failure is the last end stage of chronic kidney disease. Kidney failure means kidneys have stopped functioning and require treatment for normal functioning.
                    </p>
                <div class="btn-banner crd">
                <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Kidney Failure</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Glomerular Diseases
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                            Where glomerulus cells of the kidney where filtration occurs, when these cells get damaged due to some reasons, it leads to chronic renal failure.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Diabetes
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                Diabetes when remaining uncontrolled with medication leads to proteinuria i.e. albumin loss from urine which further harms kidney nephrons and causes renal failure.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                High blood Pressure
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                                High Blood pressure causes direct damage to nephrons and hence excretion functions get affected.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Polycystic Kidney Disease
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                                It is a disorder with which one is born with i.e.- genetic disease.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                Loss of Blood Flow to the Kidneys
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                                If there is a sudden loss of blood flow to the kidney, it can lead to <a class="green-anchor"href="https://www.drshardaayurveda.com/kidney-failure">kidney failure</a> that can be due to a heart attack, some allergic reaction, or can be due to dehydration too.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/kidney-failure-symptoms.webp') }}" class="img-fluid"alt="Kidney failure symptoms">
        </div>
        <div class="cvn sym" >
            <h3>Symptoms of <span>Kidney Failure</span></h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Itching</p>
            <p>Itching over the body without any rashes, or simply patient face dry itching.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Nausea and Vomiting</p>
            <p>Due to an increase in urea in blood, a patient feels heaviness in the epigastric region and has no feeling of hunger.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Excessive urine or not enough urine</p>
            <p>As kidney functions are altered there is a change in urine output which signifies the stage of glomerular filtrate in the body.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Muscle Cramps</p>
            <p>Muscle cramps usually at night in lower limbs, which are sometimes extremely painful and hard to bear for a patient.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Breathlessness</p>
            <p>Sometimes due to decreased urine output patients develop pleural effusion or low hemoglobin levels due to the non-production of red blood cells in the body.</p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="treatments">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Ayurvedic Treatment for <b>Kidney Failure</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Some of the Ayurvedic herbs and spices which have been beneficial in saving the kidney from failure are <strong style="font-weight: bold;">Varuna, Gokhru, Ginger, Triphala, and Turmeric</strong>. With their natural properties, they do not give any side effects.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    The dietary changes which aid the functioning of the kidney include intake of fresh fruits and vegetables, juices, loads of water, and coconut water.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    The vegetarian diet must be preferred as the animal-based diet will worsen the already existing conditions.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    As the way of living impacts the lifestyle, it needs to be modified too. An active lifestyle must be followed.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Lastly, to curb the stress, Meditation and Yoga should be practiced.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <br>
                <br>
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="KHjV6BGm07s" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/KHjV6BGm07s.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">
                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Mrs Ramanpreet Testimonial" >
                            <h3 class="usrname">Mrs Ramanpreet</h3>
                            <p class="desig">Kidney Failure Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Ramanpreet, a kidney patient from Haryana. I have recovered without kidney dialysis through Ayurvedic treatment from Dr. Sharda Ayurveda. I consulted so many doctors and was recommended to begin with kidney dialysis as they showed no improvement with western medications. So I thought of beginning with Ayurvedic treatment. I consulted Dr. Sharda Ayurveda, they got my investigations done again and began with a course of treatment. Their medicines were highly effective that my kidneys started recovering without dialysis. I highly recommend the Doctors of Dr. Sharda Ayurveda. They have the best team of <strong>Ayurvedic doctors for kidney treatment</strong>.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Harshpreet</h3>
                            <p class="desig">Kidney Failure Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                After getting diagnosed with chronic kidney disease, I was ongoing dialysis for almost 5 months. I got to know about kidney failure treatment in Ayurveda from Dr. Sharda Ayurveda. I began with treatment and met a specialist of Kidney at Dr. Sharda Ayurveda. I started with Ayurvedic medicines and slowly my dialysis dropped out. My kidneys started healing and I am in my recovery state. A big thanks to them.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Navneet Kaur</h3>
                            <p class="desig">Kidney Failure Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I got my Kidney treatment done by Dr. Sharda Ayurveda Ludhiana which I was suffering from last 5 months. My creatinine level was increasing each passing day. For my chronic kidney disease treatment, they began with detoxification of my body and then started with Ayurvedic treatment. Within few days of treatment, I could see the results and I am satisfied and I am ongoing treatment from them. 
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                What causes kidney failure?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                        Kidney failure can be caused due to some injury or serious diseases like diabetes and hypertension causes kidney failure.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Is drinking water good for kidneys?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                        Yes, drinking water is good for kidneys until the patient has low output then this water can get accumulated in the body causing more symptomatic changes.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Do alcohol consumption harm kidneys?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                        Alcohol will surely hamper the functioning of kidneys as it affects the ability to regulate fluid and electrolytes in the body.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Which color of urine signifies when kidneys are failing?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                        Dark-colored urine results in kidney failure due to an increase in concentration and accumulation of substances in the urine. This color is due to abnormal protein, sugar, red blood cells in urine.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Can kidney failure be reversed?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                        Yes, it can be reversed at a specified stage by following Ayurvedic principles, Ayurvedic treatment, diet, and all the other essentials required for a speedy recovery.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    Why do I pass urine right after I drink water?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                            It might be due to the fullness of the bladder which sends signals to the brain for evacuation as soon as the pressure of urine load increases. 
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                What are the early signs of kidney diseases?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                            Some of the early signs and symptoms of acute kidney failure include-
                            <ul>
                                <li>Decrease in urine output, although sometimes urine output remains normal</li>
                                <li>Fluid retention</li>
                                <li>Swelling in legs, ankles, or feet</li>
                                <li>Shortness of breath</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                How can I keep my kidneys working as long as possible?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                        What’s better than the functioning of a kidney in a healthy way by managing our dietary changes and exercising regularly. If there is some issue caused then there are several treatments including medications and lifestyle changes, that help kidneys work longer healthy.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                How can I repair my kidneys naturally?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                        For healthy functioning of kidneys-
                        <ul>
                            <li>Eat healthily- A balanced diet ensures your body inserts all the vitamins and minerals required</li>
                            <li>Keep an eye on your blood pressure. Get your blood pressure checked regularly.</li>
                            <li>Avoid smoking and drinking too much alcohol.</li>
                        </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                    Is Ayurvedic medicine safe for kidney patients?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                        Yes, kidney treatment in Ayurveda is quite effective and safe for kidney patients. Ayurvedic treatment helps to treat a patient without any dialysis or transplant doness.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Kidney Failure",
    "item": "https://www.drshardaayurveda.com/kidney-failure"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What causes Kidney failure?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Kidney failure can be caused due to some injury or serious diseases like Diabetes and hypertension causes kidney failure."
    }
  },{
    "@type": "Question",
    "name": "Is drinking water good for kidneys?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, drinking water for kidneys is good until the patient has low output then this water can get accumulated in the body causing more symptomatic changes."
    }
  },{
    "@type": "Question",
    "name": "Do alcohol harm kidneys?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Alcohol will surely hamper the functioning of kidneys as it affects the ability to regulate fluid and electrolytes in the body."
    }
  },{
    "@type": "Question",
    "name": "Which color of urine signifies when kidneys are failing?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Dark-colored urine results in kidney failure due to an increase in concentration and accumulation of substances in the urine. This color is due to abnormal protein, sugar, red blood cells in urine."
    }
  },{
    "@type": "Question",
    "name": "Can kidney failure be reversed?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it can be reversed at a specified stage by following all the Ayurvedic principles, Ayurvedic treatment, diet, and all the other essentials required for a speedy recovery."
    }
  },{
    "@type": "Question",
    "name": "Why do I pass urine right after I drink water?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It might be due to the fullness of the bladder which sends signals to the brain for evacuation as soon as the pressure of urine load increases."
    }
  },{
    "@type": "Question",
    "name": "What are the early signs of kidney diseases?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Some of the early signs and symptoms of acute kidney failure include-
•   Decrease in urine output, although sometimes urine output remains normal. 
•   Fluid retention, 
•   Swelling in legs, ankles, or feet. 
•   Shortness of breath."
    }
  },{
    "@type": "Question",
    "name": "How can I keep my kidneys working as long as possible?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "What’s better than the functioning of a Kidney in a healthy way by managing our dietary changes and exercising regularly. If there is some issue caused then there are several treatments including medications and lifestyle changes, that help kidneys work longer healthy."
    }
  },{
    "@type": "Question",
    "name": "How can I repair my kidneys naturally?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "For healthy functioning of Kidneys 
•   Eat healthily- A balanced diet ensures your body inserts all the vitamins and minerals required.
•   Keep an eye on your blood pressure. Get your blood pressure checked regularly. 
•   Avoid smoking and drinking too much alcohol."
    }
  },{
    "@type": "Question",
    "name": "Is Ayurvedic medicine safe for kidney patients?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, kidney treatment in Ayurveda is quite effective and safe for kidney patients. Ayurvedic treatment helps to treat a patient without any dialysis or transplant done."
    }
  }]
}
</script>
@stop