<?php
/**
 * Fungal Infaction Page 
 * 
 * @created    03/01/2021
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2021
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4">Fungal Infection</h3>
                    <p style="font-size: 15px;">SKIN-VEST-MENT It is going to with you for life time</p>
                        <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/fungal-infection">Fungal Infection</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatments">TREATMENTS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="definition">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc">
                    <img src="{{ URL::asset('front/images/fungal-infection.webp') }}" class="why-choose-us img-fluid" alt="Fungal Infection">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <h1 class="heading1"><b>Fungal Infection</b></h1>
                <p>
                    Today over 300 million population worldwide is affected with the skin fungal infection. In Ayurveda, the skin disorder is known as the <strong style="font-weight: bold;">Kushtha</strong> and the fungal infection is classified as <strong style="font-weight: bold;">Dadru</strong>. In humans, the prevalence of fungal infections occurs when the fungus invades the body and thereby takes over it. The most affected area includes feet, groin, and folds of skin. The infection causes rashes on the scalp or discoloration of the skin and this will overall cause itchiness. This correspondingly leads to a decrease in the immune system. The main causative agents are the micro-organism which is fungi. Fungal infection is an inflammatory condition that is caused by the fungus. These are characterized by a substance in their cell walls called chitin. The infection when mild shows superficial symptoms but when it affects internally it can become chronic.
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">...</span>
                </p>
                <span id="more-content">
                    <p>
                        We come across several people who are dependent on ointments and what not to treat their infection but the results they get are temporary as for the period they are good applying creams they get the results and as soon as they quit they can see the infection coming again which is a cause for concern. But Ayurvedic treatment for fungal infection of the skin is proven to provide effective results as it is a root cause, systematic treatment not symptomatic. Our changing lifestyle is the major cause of these skin infections which can be treated naturally through antifungal herbs. Fungal infection treatment in Ayurveda is a safe and effective way to treat the fungus without causing any side effects.
                    </p>
                    <div class="btn-banner crd">
                        <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                        </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate">
                    <h2 class="heading1">Causes of <b>Fungal Infection</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Weak Immune System
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                The immune system of a person plays a crucial role therefore a weak immune system would allow various pathogens to enter our body and can hinder various major activities of the body. It is found in almost every age group and also due to pre-existing health conditions.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Stress 
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                Stress affects your immune system to a greater extend. Due to this an individual sympathetic nervous system is triggered as a result immune system releases signal that redirects nutrients to major organs of our body. Therefore, increase in blood sugar level can lead to bacterial growth leading to fungal infection.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Obesity 
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                                An obese person is likely to sweat more, especially in the crease of your skin which can act as a medium for the growth of fungus and bacteria. <a class="green-anchor"href="https://www.drshardaayurveda.com/obesity">Obesity</a> can also be linked to our immune system.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Continuous Intake of Several Medications
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                                Some antibiotics can make you more prone to fungal skin infection by attacking the bacterial of the gut, which in response regulates the population of Candida yeast. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Poor Nutritional Diet 
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                                Nutritional diet is essential for maintaining a healthy life. But lack of nutritional diet or consumption more of sugar can lead to activation of bacteria in our gut which thereby provides the home for fungal reproduction.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/fungal-infection-symptoms.webp') }}" class="img-fluid"alt="Fungal Infection symptoms">
        </div>
        <div class="cvn sym" >
            <h3>Symptoms of <span>Fungal Infection</span></h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Fungal Eye Infection</p>
            <p>Redness or pain in the eyes and sometimes even watery discharge eyes. If not treated can even lead to blurring of the eyesight.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Fungus On Scalp</p>
            <p>The common symptoms associated with it is bald patches and black spots on the scalp these, in turn, can cause itchiness. The serious condition is when pus-filled sores starts appearing. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Oral Cavity Patches</p>
            <p>The reddish-white patches over the tongue or buccal mucosa can also be a visible sign of fungal infection.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Itchiness and Irritation</p>
            <p>Over the affected dark-colored area on the skin, a person experiences itchiness and irritation.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Genital Area Infection</p>
            <p>It is one of the most common signs where individual experiences red, itchy, and white discharge from the area commonly in females. It is associated with a lack of personal hygiene.
            </p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="treatments">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 ">
                <h2 class="client-test-title" style="font-size: 24px !important;">Ayurvedic Treatment for <b>Fungal Infection</b></h2>
                <p class="im3"> 
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Vamana, a part of Panchkarma uses herbs that would help in cleansing the stomach and eliminate toxins from the body which will prevent fungal growth.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Turmeric is highly recommended as it has antiseptic and antifungal properties. It can be applied as a mixture of turmeric with water/lemon juice in the form of paste once or twice a day.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Herbs like Neem and Tulsi also work wonder for preventing fungal infection as they both have anti-inflammation and wound healing property.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    A nutrition-rich diet followed by Exercise and Yoga is also beneficial as it increases the immune system which in turn would not allow fungus growth.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    <a class="green-anchor"href="https://www.drshardaayurveda.com/blogs/top-effective-home-remedies-for-recovering-from-fungal-infection-according-to-ayurveda">Ayurvedic ingredients have proven to be effective against preventing fungal infection</a> as these are rich in antibacterial and antifungal properties which are mostly found in black seed oil.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="6nwSYM8KtKM" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/fungal-infection-video-thumbnail.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Fungal Infection Testimonial" >
                            <h3 class="usrname">Shivani</h3>
                            <p class="desig">Fungal Infection Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                For the last 5 months, I was noticing the appearance of red patches on my skin. I consulted many doctors but did not get any effective results. One of a friend suggested the Ayurvedic treatment for it so I visited Dr. Sharda Ayurveda. After one week of the Ayurvedic Treatment from them, I got positive results. I am thankful to Dr. Sharda Ayurveda.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Fungal Infection Testimonial" >
                            <h3 class="usrname">Aditi</h3>
                            <p class="desig">Fungal Infection Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was observing reddish dark-colored patches under my armpit. So for this, I visited Dr. Sharda Ayurveda for the treatment. With expert guidance and Ayurvedic Treatment, I could feel the change after 10 days of regular medicine.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                What are the symptoms of fungal infection in the body?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            The symptoms of fungal infection in the body can be:
                            <ul>
                                <li>Itching and swelling around the affected area.</li>
                                <li>Redness and soreness around intimate area.</li>
                                <li>Oral thrush.</li>
                                <li>Patches on nails and skin.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                How does fungal infection occur?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                        The invasion of fungus into our body depends upon the body's immune system. If a person's immunity is low it can act as a ground for the growth of the fungal infection and mostly affected parts are the nails. The fungus enters our body through wounds and cuts.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                How can Ayurveda help prevent fungal infection?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                        Many Ayurvedic herbs are found to be effective for treating fungal infections. They include Neem, Tulsi, Ashwagandha, and Haridra as they have antiseptic, antispasmodic, anti-inflammatory, and antibacterial properties respectively.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                What lifestyle changes can help in avoiding fungal infection?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                            The following lifestyle change can help avoid infection:
                            <ul>
                                <li>Wear loose-fitting clothes.</li>
                                <li>Maintain personal hygiene.</li>
                                <li>Wear clean clothes and change your clothes twice a day</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion2">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    Which food items should be avoided during fungal infection?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion2">
                            <div class="card-body">
                                The food items which should not be consumed in fungal infection are:
                                <ul>
                                    <li>Dairy products.</li>
                                    <li>Bread and cakes.</li>
                                    <li>High sugar content products.</li>
                                    <li>Beverages.</li>
                                    <li>Dry fruits.</li>
                                    <li>Potatoes.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Can one consume milk while having fungal infection?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion2">
                        <div class="card-body">
                        No, it is suggested that one should avoid consuming dairy products if having fungal infection.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Can fungal infections be cured completely with Ayurvedic Treatment?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion2">
                        <div class="card-body">
                            Yes, it can be treated completely with Ayurvedic Treatment. Ayurveda provides the best Treatment which can prevent the reoccurrence of the infection.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                What can happen if the fungal infection is left untreated?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion2">
                        <div class="card-body">
                            Fungal infection usually begins affecting smaller areas of the body but if left untreated it can easily spread itself in a fast motion affecting other parts too of the body. The complications of fungal infection can be many but the main cause is that it can affect our heart, blood, brain, and bones. Ayurveda provides the <a class="green-anchor"href="https://www.drshardaayurveda.com/fungal-infection">best treatment for preventing fungal infection</a> safely and effectively.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                Can garlic be consumed if a person is having fungal infection?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion2">
                        <div class="card-body">
                        Yes, garlic can be consumed in fungal infection as it has anti-fungal and anti-bacterial properties which help in healing the infection. One can consume garlic with warm water daily to get effective results.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Can one prevent ringworm infection through Ayurveda?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion2">
                        <div class="card-body">
                            Yes, it can be prevented through Ayurvedic treatment. It is a contagious disease that is spreadable. The herbs which can be used for getting relief are Butea, Cassia, and Coriander, etc.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')

@stop