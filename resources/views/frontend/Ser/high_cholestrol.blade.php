<?php
/**
 * High Cholesterol Page 
 * 
 * @created    03/02/2022
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2022
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4">High Cholesterol</h3>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/high-cholesterol">High Cholesterol</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a  href="#types">TYPES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#complications">COMPLICATIONS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatment">TREATMENT</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc">
                    <img src="{{ URL::asset('front/images/high-cholesterol.webp') }}" class="why-choose-us img-fluid" alt="High Cholesterol">
                </div>
            </div>
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " >
                <h1 class="heading1"><b>High Cholesterol</b></h1>
                <p>
                High cholesterol is defined as the increase in the levels of cholesterol in the blood, which mainly raise the risk of developing serious ailments including heart disease and stroke in the future. Cholesterol is a type of lipid which is a waxy and fat-like substance produced by the liver in the natural processes of the body. Its production is essentially vital for the formation of cell membranes, certain hormones, and Vitamin D. For transferring the cholesterol, the liver produces lipoproteins which are mainly made of fat and protein. They are responsible for carrying cholesterol and triglycerides, another type of lipid, through bloodstream. High cholesterol also known as <strong style="font-weight: bold;">“Hyperlipidemia”</strong> or <strong style="font-weight: bold;">“Hypercholesterolemia”</strong>. If various studies have to be believed then the problem of high cholesterol is at high peak making approximately 25–30% of urban and 15–20% rural subjects to suffer from the disease (<a class="green-anchor"href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5485409/">Recent trends in the epidemiology of dyslipidemias in India (nih.gov))</a>. Ayurveda focus on balancing <strong style="font-weight: bold;">MedaDhatu</strong> (lipid tissue) which helps to maintain balanced cholesterol level.
                <span id="dots">...</span>
                </p>
                <span id="more-content">
                <p>
                In Ayurveda, high cholesterol is the outcome of imbalanced Kapha Dosha, which is responsible for the fat metabolism process. This misbalancing results in disturbed digestion, assimilation, and elimination processes. The levels of cholesterol as per Ayurveda need to be lowered and balanced with effective <strong style="font-weight: bold;">Ayurvedic treatment for high cholesterol</strong> with proper guidance. It essentially needs to be balanced through High Cholesterol Treatment with Ayurveda that provides positive results and thereby promotes healthy and disease-free living.
                </p>
                <div class="btn-banner crd">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                        </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn">Read more</button>
            </div>
        </div>
    </div>
</section>

<!-- <section class="dsa-india-ayurvedic-sec" id="types"> 
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="heading1">There are mainly two <b>Types of Lipoprotein</b></h2>
            </div>
        </div>
        <div class="row cust-iba">
            <div class="owl-carousel owl-theme" >
                <div class="item col-md-12 text-center aos-init aos-animate" >
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/hypertension.webp') }}" class="card-img-top lazyload" alt="Hypertension">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Low-Density Lipoprotein (LDL)</h4>
                            <p class="card-text">
                                Stated as “Bad” Cholesterol which makes most of the body’s cholesterol. A high level of LDL means a person is at high risk of developing heart diseases.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" >
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/hypotension.webp') }}" class="card-img-top lazyload" alt="High-Density Lipoprotein (HDL) ">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">High-Density Lipoprotein (HDL)</h4>
                            <p class="card-text">
                                Stated as “Good” Cholesterol which absorbs cholesterol and carries it back to the liver. The liver will then flushes it out from the body. A high level of HDL signifies lower risk of heart disease and stroke.
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section> -->

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate">
                    <h2 class="heading1">Causes of <b>High Cholesterol</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="causes">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Unhealthy Diet
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                High intake of saturated and Trans fat in the diet increases the accumulation of LDL cholesterol in blood, thus causing higher chances of heart disease occurrence.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Lack of Physical Activity
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                It can significantly lower the HDL cholesterol levels in your blood and make it difficult for the body system to eliminate LDL from the blood for smooth functioning. Moderate to intense levels of exercise can help increase HDL cholesterol levels naturally.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion">
                        <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Hereditary
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                In some cases, people of the younger age group happen to suffer from heart diseases due to high cholesterol. This pattern is often observed in genetics classified as familial hypercholesterolemia.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Medical Conditions
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">
                                Many underlying health conditions can also increase the chances of occurrence of high cholesterol issues. Individuals suffering from high blood pressure, diabetes, kidney, and liver disease are more prone to suffer.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Lifestyle 
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                            <div class="card-body">
                                Lifestyle preferences can bring about a big difference in one's living. A sedentary lifestyle can increase the chances of disease emergence.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>			


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="complications">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/High-cholesterol-symptoms.webp') }}" class="img-fluid" alt="Complications of High Cholesterol">
        </div>
        <div class="cvn sym" >
            <h3>Complications of <span>High Cholesterol</span></h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Chest Pain</p>
            <p>The high cholesterol blocks the blood supply through arteries thus causing extreme chest pain and tightening.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Shortness of Breath </p>
            <p>Coronary artery diseases are the outcome of increased levels of LDL Cholesterol which get deposited in the heart’s small arteries, causing them to narrow and stiffen. This interferes with the blood flow, which can cause difficulty breathing.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Numbness </p>
            <p>High cholesterol levels in blood make blood flow thick and thereby affecting the normal supply to the nerves causing numbness.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">High Blood Pressure</p>
            <p>The deposition of cholesterol in blood blocks the arteries, which thereby become stiff and narrow. It subsequently makes it difficult for the heart to work efficiently or pump blood thus leading to shooting high blood pressure.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Diabetes</p>
            <p>People with diabetes are more likely to get affected by ischemic stroke compared to non-diabetic individuals.</p>
        </div>
    </div>
</div>
<div class="split"></div>
<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " >
                <h2 class="client-test-title"style="font-size: 24px !important;">Ayurvedic Treatment for <b>High Cholesterol</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurveda suggests with just mere changes in the diet one can get effectively recover from the disease such as cutting out on caffeine-based products, saturated fats, and processed food. These changes can bring a significant difference in health.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Yoga and meditation have an important role to play in the management of symptoms. Some of the best Yoga poses recommended for the purpose are Chakrasana, Kapalbhati Pranayam, and Paschimottanasana.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    <a class="green-anchor"href="https://www.drshardaayurveda.com/blogs/how-to-manage-high-cholesterol-through-ayurveda">Ayurvedic remedies for high cholesterol</a> are proven to provide an enormous number of benefits and give relief from the symptoms naturally if followed accurately.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Garlic is effective for preventing high cholesterol. Mix one clove of fresh finely chopped garlic, ½ tsp grated ginger root, and ½ tsp lime juice mix thoroughly and have it before each meal throughout the day.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurvedic medicine for cholesterol has two basic ingredients i.e., Guggul and Arjuna which are proven for their higher benefits that it provides to manage high cholesterol.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Include more millets, quinoa, oatmeal, wheat, apples, grapefruit, and almonds which are known to essentially reduce cholesterol levels in the body. Also, make sure to engage yourself in more physical activities to remain fit and active.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="uGj18H61CDc" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/uGj18H61CDc.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Asthma Testimonial" >
                            <h3 class="usrname">Ritu Shah</h3>
                            <p class="desig">High Cholesterol Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was diagnosed with high cholesterol 1 year back from now with majorly facing the issue of chest pain and shortness of breath. This caused extreme discomfort and hampers my daily functioning to a great extent. Even after trying every way out, there was no improvement in my health. Therefore, as per recommendation from a friend visited Dr. Sharda Ayurveda. The regular intake of effective medication and also the following expert suggested dietary and lifestyle changes helped me recover within just 3 months. I am thankful to Dr. Sharda Ayurveda.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Flatulence Testimonial" >
                            <h3 class="usrname">Simranpreet Singh</h3>
                            <p class="desig">High Cholesterol Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was on insulin for the past 6 months due to diabetes, but soon after I was observing other changes in my health i.e., always lethargic and facing high blood pressure issues. For this, I consulted various renowned specialists, but sadly none of the treatments were able to provide any satisfactory results. So after a suggestion from a friend consulted Dr. Sharda Ayurveda for the treatment where within just 2 months of the Ayurvedic treatment I am fully recovered from high cholesterol issues as well as high blood pressure. I am thankful to Dr. Sharda Ayurveda for the best treatment.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               
                                <i class="fa" aria-hidden="true"></i>
                               What is considered to be adverse high cholesterol?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            The normal level of cholesterol is below 150mg/dL. If the level approach 200 mg/dL or more then without wasting time consult with the experts for controlling its levels with proper medications. As if not treated timely the person is at high risk of developing cardiovascular diseases.
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Which treatment is best for the <a class="green-anchor"href="https://www.drshardaayurveda.com/blogs/how-to-manage-high-cholesterol-through-ayurveda">effective management of high cholesterol</a>?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            Every treatment is good in one or another way, but Ayurvedic treatment holds a special and strong ground for providing the best and most effective recovery from the disease. The root cause, systematic, and specialized treatment management help in early and natural recovery.
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Which drinks are preferred that helps lower high cholesterol?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            Some of the best drinks and smoothies that help lower high cholesterol are:
                            <ul>
                                <li>Pomegranate juice</li>
                                It contains a higher level of antioxidants, the property that makes this drink beneficial for lowering LDL Cholesterol levels in the blood.
                                <li>Plant-based smoothies</li>
                                Plant-based smoothies contain ingredients that assist in cholesterol management. Some best add-ons are bananas, grapes, mangos, and melons which are ideal for maintaining a healthy heart.
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                               What are the natural ways to control high cholesterol?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                            The key highlighter changes that should be incorporated for controlling high cholesterol are:
                            <ul>
                                <li>Have a nutritious diet full of healthy fruits and vegetables.</li>
                                <li>Avoid saturated and processed food.</li>
                                <li>Adding omega-3 food to the diet provides reduced LDL cholesterol levels in the body.</li>
                                <li>Do regular exercise to keep the body active.</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingFive" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <i class="fa" aria-hidden="true"></i>
                               Is lemon helps lower high cholesterol?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="card-body">
                            Lemon juice may help lower cholesterol levels and thus improve cardiovascular health. This is so because of the increased levels of flavonoids and vitamin C present in it.
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Can stress be linked with the emergence and flare-up of high cholesterol?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                        <div class="card-body">
                           When you are under stress, the body releases certain types of hormones as a response which as cortisol and adrenaline. And studies have proved that higher levels of these hormones in the body are directly linked to increased blood cholesterol, and if the condition persists it indulges an individual to develop heart disease. Therefore, early management is essential for healthy well-being.
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Which Ayurvedic clinic in India is best to approach for the treatment?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                        <div class="card-body">
                            As per observing the treatment method, and the patient's recovery rate Dr. Sharda Ayurveda is recommended as the best solution for effective management of high cholesterol. Here the emphasis is on diet and lifestyle changes, systematic management and natural Ayurvedic herbs formulated medications for long term recovery.
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Why high cholesterol is a question of concern?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                        <div class="card-body">
                           High cholesterol levels in the blood increase the chances of developing cardiovascular diseases. This is because the cholesterol gets built up inside the walls of the arteries that carry blood to the heart. This build-up, over time, causes less nutrients and oxygen supply to the heart, thus causing chest pain and strokes.
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                               What are Ayurvedic home remedies that help manage high cholesterol?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                        <div class="card-body">
                            Some of the best home remedies are:
                            <ul>
                                <li>The Indian Gooseberry (Amla)</li>
                                Has shown miraculous effects on controlling cholesterol and sugar levels. Eating raw Indian gooseberry daily can provide immense goodness to health. For this take amla powder and mix it in a cup of warm water. It is also one of the best-known blood purifiers.
                                <li>Coriander</li>
                                Start your day by having a coriander drink. For the same take 2 tsp of coriander powder and mix it in 1 cup of warm water. This has been proven for lowering cholesterol levels naturally.
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                What is the most common cause of high cholesterol?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                        <div class="card-body">
                            The most common cause of high cholesterol highlighted is the sedentary lifestyle which includes lack of physical activity, sitting for a longer period, and many others.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<style>
    .carousel-inner {position: relative;width: 100%;overflow: hidden;}
    .carousel-item {position: relative;float: left;width: 100%;margin-right: -100%;-webkit-backface-visibility: hidden;backface-visibility: hidden;transition: -webkit-transform .6s ease-in-out;transition: transform .6s ease-in-out;transition: transform .6s ease-in-out,-webkit-transform .6s ease-in-out;}
    .card-body {padding-top: 1.5rem;padding-bottom: 1.5rem;-webkit-border-radius: 0!important;border-radius: 0!important;flex: 1 1 auto;-ms-flex: 1 1 auto;}
    .card .card-body a h3{font-size: 18px;color: #ff9000;line-height: 1.4;}
    .card .card-body h3{color: #373433;font-size: 18px;    line-height: 1.4;}
    .font-weight-bold {font-weight: 700!important;}
    .card-title {margin-bottom: .75rem;}
    .card-img-top {width: 100%;border-top-left-radius: calc(.25rem - 1px);border-top-right-radius: calc(.25rem - 1px);}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            responsiveClass: !0,
            navigation : true,
            autoplay: true,
            loop: true,
            smartSpeed: 2500,
            singleItem : true,
            navigationText: ["<i class='fa fa-angle-left fa-2'></i>", "<i class='fa fa-angle-right fa-2'></i>"],
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                480: {
                    items: 1,
                    nav: !1
                },
                768: {
                    items: 2,
                    nav: !1
                },
                1000: {
                    items: 2,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1024: {
                    items: 2,
                    nav: !1,
                    loop: 0,
                    margin: 20
                },
                1366: {
                    items: 2,
                    nav: !1
                }
            }
        })
    });

</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "High Cholesterol",
    "item": "https://www.drshardaayurveda.com/blood-pressure"  
  }]
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What is considered to be adverse high cholesterol?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The normal level of cholesterol is below 150mg/dL. If the level approach 200 mg/dL or more then without wasting time consult with the experts for controlling its levels with proper medications. As if not treated timely the person is at high risk of developing cardiovascular diseases."
    }
  },{
    "@type": "Question",
    "name": "Which treatment is best for the effective management of high cholesterol?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Every treatment is good in one or another way, but Ayurvedic treatment holds a special and strong ground for providing the best and most effective recovery from the disease. The root cause, systematic, and specialized treatment management help in early and natural recovery."
    }
  },{
    "@type": "Question",
    "name": "Which drinks are preferred that help lower high cholesterol?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Some of the best drinks and smoothies that help lower high cholesterol are:
•   Pomegranate juice
It contains a higher level of antioxidants, the property that makes this drink beneficial for lowering LDL Cholesterol levels in the blood. 
•   Plant-based smoothies
Plant-based smoothies contain ingredients that assist in cholesterol management. Some best add-ons are bananas, grapes, mangos, and melons which are ideal for maintaining a healthy heart."
    }
  },{
    "@type": "Question",
    "name": "What are the natural ways to control high cholesterol?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The key highlighter changes that should be incorporated for controlling high cholesterol are:
•   Have a nutritious diet full of healthy fruits and vegetables. 
•   Avoid saturated and processed food. 
•   Adding omega-3 food to the diet provides reduced LDL cholesterol levels in the body. 
•   Do regular exercise to keep the body active."
    }
  },{
    "@type": "Question",
    "name": "Is lemon helps lower cholesterol levels?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Lemon juice may help lower cholesterol levels and thus improve cardiovascular health. This is so because of the increased levels of flavonoids and vitamin C present in it."
    }
  },{
    "@type": "Question",
    "name": "Can stress be linked with the emergence and flare-up of high cholesterol?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "When you are under stress, the body releases certain types of hormones as a response which as cortisol and adrenaline. And studies have proved that higher levels of these hormones in the body are directly linked to increased blood cholesterol, and if the condition persists it indulges an individual to develop heart disease. Therefore, early management is essential for healthy well-being."
    }
  },{
    "@type": "Question",
    "name": "Which Ayurvedic clinic in India is best to approach for the treatment?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "As per observing the treatment method, and the patient's recovery rate Dr. Sharda Ayurveda is recommended as the best solution for effective management of high cholesterol. Here the emphasis is on diet and lifestyle changes, systematic management and natural Ayurvedic herbs formulated medications for long term recovery."
    }
  },{
    "@type": "Question",
    "name": "Why high cholesterol is a question of concern?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "High cholesterol levels in the blood increase the chances of developing cardiovascular diseases. This is because the cholesterol gets built up inside the walls of the arteries that carry blood to the heart. This build-up, over time, causes less nutrients and oxygen supply to the heart, thus causing chest pain and strokes."
    }
  },{
    "@type": "Question",
    "name": "What are Ayurvedic home remedies that help manage high cholesterol?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Some of the best home remedies are:
•   The Indian Gooseberry (Amla)
Has shown miraculous effects on controlling cholesterol and sugar levels. Eating raw Indian gooseberry daily can provide immense goodness to health. For this take amla powder and mix it in a cup of warm water. It is also one of the best-known blood purifiers.
•   Coriander 
Start your day by having a coriander drink. For the same take 2 tsp of coriander powder and mix it in 1 cup of warm water. This has been proven for lowering cholesterol levels naturally."
    }
  },{
    "@type": "Question",
    "name": "What is the most common cause of high cholesterol?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The most common cause of high cholesterol highlighted is the sedentary lifestyle which includes lack of physical activity, sitting for a longer period, and many others."
    }
  }]
}
</script>

@stop