<?php
/**
 * Insomina Page 
 * 
 * @created    03/02/2022
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2022
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4">Insomnia (Nidranasha)</h3>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/insomnia">Insomnia</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a  href="#types">TYPES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatments">TREATMENTS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose"id="definition">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc">
                    <img src="{{ URL::asset('front/images/insomnia.webp') }}" class="why-choose-us img-fluid" alt="insomnia treatment">
                </div>
            </div>
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " >
                <h1 class="heading1"><b>Insomnia</b> (Nidranasha)</h1>
                <p>
                Insomnia is characterized as a sleeping disorder that in Ayurveda is known as <strong style="font-weight: bold;">“Andira” or “Nidranasha”</strong>. An individual faces difficulty falling asleep or staying asleep. The constant fatigue and anxiety do not allow the individual to concentrate thus hampering their ability to perform the daily tasks. Insomnia with it brings other problems including – daytime sleepiness, mood swings, and lethargicness. The persistence of the symptoms can vary according to their severity, and underlying reasons which become a major contributor of disease to flare-ups. Additionally, it can also be influenced by neurological, endocrine conditions, and overuse of certain medications.
                <span id="dots">...</span>
                </p>
                <span id="more-content">
                <p>
                Insomnia is not a life-threatening disease, but it is essentially important to manage it early to avoid discomfort. The three pillars of good health are a balanced diet, regular exercise, and sufficient sleep. Insomnia is caused due to increase in Vata and Pitta Doshas as an outcome of physical and mental exertion. The <a class="green-anchor" href="https://www.drshardaayurveda.com/insomnia">Ayurvedic treatment for insomnia</a> can help you get rid of the pills and naturally manage your Doshas that are responsible for the disease emergence and flare-up. For the best and most effective results opt for Dr. Sharda Ayurvedic treatment.
                </p>
                <div class="btn-banner crd">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn">Read more</button>
            </div>
        </div>
    </div>
</section>
<section class="dsa-india-ayurvedic-sec" id="types"> 
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="heading1">Types of <b>Insomnia</b></h2>
                <p class="text-center">Insomnia is categorized into two types</p>
            </div>
        </div>
        <div class="row cust-iba">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                <div class="iba-box box1 Insomnia1">
                    <div class="xd">
                        <div class="cont">  
                            <div class="wht-space"></div>
                            <h3>Primary Insomnia</h3>
                            <p>
                                Primary insomnia is defined as the decreased ability to fall asleep or stay asleep, which all outcomes in sleep deprivation, fatigue, and irritability. This type is caused due to physical or emotional stress, anxiety, and excessive exertion.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                <div class="iba-box box2 Insomnia2">
                    <div class="xd">
                        <div class="cont">
                            <div class="wht-space"></div>
                            <h3>Secondary Insomnia</h3>
                            <p>
                                The cause for this type of insomnia is underlying neurological conditions as well as medications.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate">
                    <h2 class="heading1">Causes of <b>Insomnia</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Stress and anxiety
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Both can potentially affect the mental health condition of an individual thus directly influencing the daily sleeping schedule.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Lack of sleep
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            Following of disturbed sleeping pattern can also be a main reason for insomnia.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Certain medications
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                Some medications including anti-depressants, anti-asthma, and blood pressure drugs can cause side effects.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Physical health condition
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">
                                Many underlying health conditions have the potential to affect the sleeping pattern of the individual.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Lifestyle
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                            <div class="card-body">
                                Poor eating habits, drinking excessive alcohol and lack of physical activity can cause insomnia.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>			

<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/insomnia-symptoms.webp') }}" class="img-fluid" alt="insomnia symptoms">
        </div>
        <div class="cvn sym" >
            <h3>Symptoms of <b>Insomnia</b></h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Fatigue</p>
            <p>A person often feels lethargic and restless.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Loss of concentration</p>
            <p>Not having a sound full sleep can negatively impact your brain functioning.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Mood swings</p>
            <p>Instant and persistent mood swings is the most common symptom of insomnia thus making an individual always irritated and disturbed.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Headache</p>
            <p>A person may experience a continuous headache.</p>
        </div>
    </div>
</div>
<div class="split"></div>
<section class="dsa-why-choose" id="treatments">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " >
                <h2 class="client-test-title"style="font-size: 24px !important;">Ayurvedic Treatment For <b>Insomnia</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Panchakarma therapy helps in the detoxification of the body, thereby releasing the physical and mental toxins. The common procedure involved in Vasti (medicated enema).
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Practicing yoga and meditation daily is considered the most important aspect as it helps in relaxing the mind, thus favoring the release of <a class="green-anchor"href="https://www.drshardaayurveda.com/anxiety-disorder">stress and anxiety</a>.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Massaging your body with medicated oil helps improve blood circulation throughout the body. It balances all the Doshas responsible for insomnia, which directly stables the body and mind.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Brahmi and Ashwagandha are the two main Ayurvedic herbs that help improve brain functioning.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Consuming a nutrient-rich diet that includes proteins, fats, vitamins, and minerals helps treat Insomnia and is considered part of a balanced diet.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="3SGdFQJiJaw" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/3SGdFQJiJaw.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">
                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Asthma Testimonial" >
                            <h3 class="usrname">Jagdeesh Verma</h3>
                            <p class="desig">Insomnia Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                A past incident led me towards depression. I was not able to concentrate on my work and do not prefer to interact with anyone. This significantly affected my personal and professional life. My sleeping schedule always remains disturbed and even after consulting many doctors, my situation remained the same. Therefore, I consulted Dr. Sharda Ayurveda as suggested by one of my friends. With their expert guidance followed by lifestyle changes and within just 2 months of the medications, I am now at the recovery stage. I could not thank enough Dr. Sharda Ayurveda for blessing me with a new life. 
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Asthma Testimonial" >
                            <h3 class="usrname">Jagmeet</h3>
                            <p class="desig">Insomnia Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Myself Jagmeet I was suffering from anxiety and obsessive thinking for the past few years. This caused me a disturbed sleeping schedule and depression. I visited various doctors but the results were always disheartening. Therefore I consulted Dr. Sharda Ayurveda for my situation. The expert guidance with regular Ayurvedic medications followed by lifestyle changes helped me to recover from insomnia within just 3 months. I am thankful to Dr. Sharda Ayurveda for their best, and most effective Ayurvedic treatment.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                How long does insomnia last? 
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            The condition varies from person to person. Acute insomnia lasts for a day or a few weeks but when the situation becomes chronic the attack may persist for months.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Is insomnia treatable through Ayurvedic treatment?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            Sleep disorders treatment in Ayurveda is safe and effective. Dr. Sharda's Ayurveda insomnia Ayurvedic treatment has proven to provide positive results. The root cause management with the suggestion of certain lifestyle and dietary changes can help you get recover well from the disorder naturally.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Can insomnia be treated without the intake of sleeping pills? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            Yes, it can be treated by adopting a healthy lifestyle and diet. Some of the best-recommended changes that must be adopted are:
                            <li>Avoid day sleeping and short naps.</li>
                            <li>Never skip a day without a physical workout</li> 
                            <li>Diet rich in selenium, calcium, potassium, magnesium, Vitamin C, Vitamin D, Vitamin B6, and Omega.</li>
                            <li>Practice yoga and meditation daily.</li>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                What home remedies can help overcome insomnia? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                            Some of the best home remedies that help overcome insomnia are :
                            <li>Consume warm milk daily before bedtime as it is rich in melatonin and tryptophan.</li>
                            <li>Regular oil massage can help reduce pain and release anxiety and depression. Example – lavender oil</li> 
                            <li>Must have a magnesium-rich diet as it relaxes muscles and reduces stress which would directly promote healthy sleeping.</li>
                            It is always advisable to consult experts before following any of these remedies.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFive" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <i class="fa" aria-hidden="true"></i>
                                What is the role of stress is insomnia?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="card-body">
                            It is mainly related to having a disturbed mind that hampers the brain's functioning. Stress can also cause sleep deprivation that can significantly impair physical and mental health. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Why do women suffer from insomnia compared to men?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                        <div class="card-body">
                            Women are likely to suffer from insomnia compare to men because of hormonal changes during the menstruation cycle, menopause, and pregnancy. They are also more likely to develop neurological medical conditions which lead to the emergence of this sleeping disorder.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                How Ayurvedic treatment helps overcome insomnia? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                        <div class="card-body">
                            Ayurvedic treatment helps in early recovery from insomnia naturally and effectively. Some best Ayurvedic recommendations that show positive and successful recovery of insomnia are:
                            <li>Panchakarma therapy</li> 
                            <li>Oil massaging</li> 
                            <li>Yoga and meditation</li> 
                            <li>Having a nutrient-rich diet</li>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                What food items should be included in the diet to treat insomnia?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                        <div class="card-body">
                            The food items to be included in the diet to have peaceful sleep are:
                            <li>Banana</li>
                            <li>Basil</li> 
                            <li>Walnuts</li> 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                What are the risk factors of insomnia?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                        <div class="card-body">
                            The risk factors of insomnia are:
                            <li>Mental health condition</li> 
                            <li>Irregular sleeping schedules</li>
                            <li>Age</li>
                            <li>Lack of physical activity</li> 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                What food items should be eliminated from the diet to overcome insomnia?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                        <div class="card-body">
                            The foods that should be eliminated from the diet to overcome insomnia are:
                            <li>Caffeinated products</li>
                            <li>High sugar items</li>
                            <li>Spicy food</li>
                            <li>Processed food</li>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<style>
    .carousel-inner {position: relative;width: 100%;overflow: hidden;}
    .carousel-item {position: relative;float: left;width: 100%;margin-right: -100%;-webkit-backface-visibility: hidden;backface-visibility: hidden;transition: -webkit-transform .6s ease-in-out;transition: transform .6s ease-in-out;transition: transform .6s ease-in-out,-webkit-transform .6s ease-in-out;}
    .card-body {padding-top: 1.5rem;padding-bottom: 1.5rem;-webkit-border-radius: 0!important;border-radius: 0!important;flex: 1 1 auto;-ms-flex: 1 1 auto;}
    .card .card-body a h3{font-size: 18px;color: #ff9000;line-height: 1.4;}
    .card .card-body h3{color: #373433;font-size: 18px;    line-height: 1.4;}
    .font-weight-bold {font-weight: 700!important;}
    .card-title {margin-bottom: .75rem;}
    .card-img-top {width: 100%;border-top-left-radius: calc(.25rem - 1px);border-top-right-radius: calc(.25rem - 1px);}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            responsiveClass: !0,
            navigation : true,
            autoplay: true,
            loop: true,
            smartSpeed: 2500,
            singleItem : true,
            navigationText: ["<i class='fa fa-angle-left fa-2'></i>", "<i class='fa fa-angle-right fa-2'></i>"],
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                480: {
                    items: 1,
                    nav: !1
                },
                768: {
                    items: 2,
                    nav: !1
                },
                1000: {
                    items: 2,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1024: {
                    items: 3,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1366: {
                    items: 3,
                    nav: !1
                }
            }
        })
    });

</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Insomnia",
    "item": "https://www.drshardaayurveda.com/insomnia"  
  }]
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "How long does insomnia last?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The condition varies from person to person. Acute insomnia lasts for a day or a few weeks but when the condition becomes chronic the attack lasts for months."
    }
  },{
    "@type": "Question",
    "name": "Is insomnia treatable through Ayurvedic treatment?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Sleep disorders treatment in Ayurveda is safe and effective. Dr. Sharda Ayurveda insomnia Ayurvedic treatment has proven to provide the best and most effective results. The root cause treatment with a certain lifestyle and dietary changes recommended by the experts can help you get to recover from the disease naturally."
    }
  },{
    "@type": "Question",
    "name": "Can insomnia be treated without the intake of sleeping pills?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it can be treated by adopting a healthy lifestyle and diet. Some of the best-recommended lifestyle and diet changes include are:
•   Avoid day sleeping and short naps.
•   Never skip a day without a physical workout 
•   Diet rich in selenium, calcium, potassium, magnesium, Vitamin C, Vitamin D, Vitamin B6, and Omega Practice yoga and meditation daily
•   Adopt a constant sleeping schedule"
    }
  },{
    "@type": "Question",
    "name": "What home remedies can help overcome insomnia?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Some of the best home remedies that help overcome insomnia are :
•   Consume warm milk daily before bedtime as it is rich in melatonin and tryptophan.
•   Regular oil massage can help reduce pain, releases anxiety, and depression. Example – lavender oil 
•   Add magnesium to your diet as it relaxes muscles and releases stress which would directly promote healthy sleeping.
It is always advisable to consult experts before following any of these remedies."
    }
  },{
    "@type": "Question",
    "name": "What is the role of stress is insomnia?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It is related to the disturbed mind hampering brain functioning. Stress can cause sleep deprivation causing impaired physical and mental health."
    }
  },{
    "@type": "Question",
    "name": "Why do women suffer from insomnia compared to men?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Women are likely to suffer from insomnia compare to men because of hormonal changes during the menstruation cycle, menopause, and pregnancy which directly impact the sleeping schedules. They are also more likely to develop medical conditions such as stress, depression, and anxiety which lead to the development of this sleeping disorder."
    }
  },{
    "@type": "Question",
    "name": "How Ayurvedic treatment helps overcome insomnia?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurvedic treatment helps in early recovery from insomnia naturally and effectively. Some of the best Ayurvedic recommendations helpful to overcome insomnia are:
•   Panchakarma therapy 
•   Oil massaging 
•   Yoga and meditation 
•   Having a nutrient-rich diet"
    }
  },{
    "@type": "Question",
    "name": "What food items should be included in the diet to treat insomnia?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The food items to be included in the diet to have peaceful sleep are:
•   Banana
•   Basil 
•   Vitamin B6 
•   Walnuts"
    }
  },{
    "@type": "Question",
    "name": "What are the risk factors of insomnia?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The risk factors of insomnia are:
•   Mental health condition 
•   Irregular sleeping schedules
•   Age 
•   Lack of physical activity"
    }
  },{
    "@type": "Question",
    "name": "What food items should be eliminated from the diet to overcome insomnia?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The foods that should be eliminated from the diet to overcome insomnia are:
•   Caffeinated products
•   High sugar items
•   Spicy food 
•   Processed food"
    }
  }]
}
</script>
@stop