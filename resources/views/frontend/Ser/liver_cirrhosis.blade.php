<?php
/**
 * Liver Cirrhosis Page 
 * 
 * @created    03/01/2021
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2021
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4">Liver Cirrhosis</h3>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/liver-cirrhosis">Liver Cirrhosis</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatments">TREATMENTS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="definition">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" >
                    <img src="{{ URL::asset('front/images/liver-cirrhosis.webp') }}" class="why-choose-us img-fluid" alt="Liver Cirrhosis">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Liver Cirrhosis</b></h1>
                <p>
                    Liver Cirrhosis is a life-threatening disease that can even lead to death if not treated timely. <strong style="font-weight: bold;">Kumbha Kamala</strong> is the classical term to describe Cirrhosis of the liver in Ayurveda. Liver Cirrhosis cure in Ayurveda has the most effective and visible results within a few days of Ayurvedic Treatment. Liver Cirrhosis can be said as the scarring of the liver i.e., the healthy liver tissues are replaced with the scar tissues. Cirrhosis develops when the toxins remain present in our body for a longer period of time. The liver is considered to be the hard organ responsible for the regeneration of the damaged cells. But when toxins fail to eliminate from the body this leads to an injured and scarred liver. Thus the infected liver would not be able to function properly, which ultimately results in Cirrhosis i.e., shrinkage of the liver. Cirrhosis of the liver thus makes it difficult to flow nutrient-rich blood into the liver through the veins. The other functions that are hindered due to Cirrhosis that leads to the inability of the liver to control infections, protein production, and avoid contamination.
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">...</span>
                </p>
                <span id="more-content">
                    <p>
                        <a class="green-anchor"href="https://www.drshardaayurveda.com/blogs/how-ayurveda-can-be-beneficial-for-treating-liver-cirrhosis">Ayurvedic Treatment for Liver Cirrhosis</a> has proven to be effective and has so far given positive results. Ayurvedic medicine for liver cirrhosis is the formulation of natural herbs, plants, and shrubs that have no side effects. <b>Liver Cirrhosis Treatment in Ayurveda</b> mainly focuses on systematic and natural treatment.
                    </p>
                    <div class="btn-banner crd">
                        <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                        </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" >
                    <h2 class="heading1">Causes of <b>Liver Cirrhosis</b></h2>
                    <p>The causes of Liver Cirrhosis are still unknown but some causes are mostly seen</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Hepatitis Infection
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                It is a viral infection that leads to inflammation of the liver. This infection is chronic where hepatitis B and C are most commonly affecting individuals worldwide.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Excessive Alcohol Consumption
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            Excessive consumption of alcohol causes inflammation of the liver and can lead to permanent damage of the liver cells and conversion to scar cells.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Autoimmune Disease
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                                The infections where the body cells instead of attacking the invasive microorganisms start attacking the healthy tissues of the body including organs which in turn lowers the immune system.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Non-Alcoholic Fatty Liver
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                            Deposition of fat in the liver for a prolonged period can lead to Cirrhosis. The scar tissues formed are linked with other conditions such as diabetes, obesity, high cholesterol, and protein malfunctioning.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Hereditary 
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                                The family lineage sometimes hinders the metabolic process in the liver, inability to accumulate iron and copper in the system. Cystic fibrosis could be another genetic disorder causing liver cirrhosis.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/liver-cirrhosis-symptoms.webp') }}" class="img-fluid"alt="Liver Cirrhosis symptoms">
        </div>
        <div class="cvn sym" >
            <h3>Symptoms of <span>Liver Cirrhosis</span></h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Spider Blood Vessels</p>
            <p>The appearance of spider-shaped blood vessels when an artery surrounded by small vessels is affected due to damaged liver. Release of hormone estradiol leads to expansion of vessels.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Accumulation of Fluid in Abdomen</p>
            <p>The majority of people develop an accumulation of fluid in the abdomen which is called ascites. If not treated timely can even lead to organ damage and ultimately death.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png"><a class="green-anchor" href="https://www.drshardaayurveda.com/jaundice">Jaundice</a></p>
            <p>When RBC’s breakdown bilirubin is released which travels from the liver and passed through stools. But in liver cirrhosis, bilirubin gets accumulated in the toxic body. This condition mainly happens due to lifestyle modification.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Fatigue </p>
            <p>It is the most common symptom seen in people affected with liver cirrhosis. It varies from every different individual i.e., some may experience on and off fatigue and sometimes constant. Its intensity can be mild or sometimes chronic.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Shrinkage in Size of Liver</p>
            <p>Conversion of healthy tissue into scar tissue can lead to shrinkage of the liver. The development of scars affects the blood flow leading to damage to the liver
            </p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="treatments">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Ayurvedic Treatment for <b>Liver Cirrhosis</b></h2>
                <p class="im3"> 
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    The therapies like <b>Agneya Chikistha and Rasayana Chikistha</b> are proven to promote liver functioning and reduce scarring of tissues.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Panchkarma Treatment is also beneficial as it removes toxins from the body which in turn releases stress and promotes blood circulation.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurvedic Treatment followed with yoga and dietary modification can help reduce the chances of liver cirrhosis.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Vitamin C-rich diet is beneficial as it helps in flushing out toxins and fat from the body.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    It is essential to keep the body always hydrated. Also, it is suggested to eat a low sodium diet followed by 2 days of fasting a week to keep body toxins free.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" >
                    <div id="dna_video">
                        <div class="youtube-player" data-id="qlMGAs3gN-k" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/qlMGAs3gN-k.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial"  id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Liver Cirrhosis Testimonial" >
                            <h3 class="usrname">Devraj Singh</h3>
                            <p class="desig">Liver Cirrhosis Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                My liver was damaged for the past 7 months and after consulting so many doctors, I was ultimately suggested for a liver transplant. I got to know about Dr. Sharda Ayurveda and thought of consulting them once. With Ayurvedic Treatment followed by dietary modifications, I am now at a recovery stage without getting liver transplant done. I am thankful to Dr. Sharda Ayurveda for their guidance and effective Ayurvedic medicines.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Liver Cirrhosis Testimonial" >
                            <h3 class="usrname">Sukhveer Singh</h3>
                            <p class="desig">Liver Cirrhosis Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Due to excessive alcohol consumption, my liver was slowly getting damaged. I tried quitting alcohol but I could not. Then I consulted Dr. Sharda Ayurveda for their <a class="green-anchor"href="https://www.drshardaayurveda.com/alcohol-de-addiction">alcohol de-addiction</a> and liver treatment. They prescribed me some mandatory dietary changes followed by Ayurvedic treatment. I was able to quit alcohol in just 2 months and was continuing medicines for liver. I am satisfied as I am now at a recovery stage.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" >
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                Does liver cirrhosis damage the brain?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            Yes, liver cirrhosis can even hamper brain functioning. When the liver cells are unable to detoxify the toxins from the body, this may reach the brain and affect its functioning.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                    What are the symptoms of liver cirrhosis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            The common symptoms include:
                            <ul>
                                <li>Loss of appetite</li>
                                <li>Jaundice</li>
                                <li>Dark-colored urine</li>
                                <li>Itching on the skin</li>
                                <li>Nausea</li>
                                <li>Swelling of lower body</li>
                                <li>Yellow discoloration of the body</li>
                                <li>Muscle cramps</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                    How can Ayurveda treat liver cirrhosis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            Ayurvedic medicines are the formulation of long-known herbs which are essentially helpful in treating disease. Treatment for liver cirrhosis in terms of Ayurveda focuses on detoxification of the liver. Papaya seeds can be used as a remedy for relief but it is always advised to consult experts before adding this to your diet.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                What are the risk factors associated with liver cirrhosis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                        The risk factors of liver cirrhosis can be:
                            <ul>
                                <li>Hepatitis B/C</li>
                                <li>Autoimmune liver disease</li>
                                <li>Hemochromatosis</li>
                                <li>Fatty Liver Disease (FLD)</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion2">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    Is liver cirrhosis disease preventable?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion2">
                            <div class="card-body">
                                Yes, it is preventable with early examination and with proper Ayurvedic medication one can slow down further damage of the liver. There are ways one can adopt to minimize the damage to the liver:
                                <ul>
                                    <li>Consume a fiber-rich diet</li>
                                    <li>Maintain healthy body weight.</li>
                                    <li>Avoid intake of alcohol and drugs</li>
                                    <li>Ayurveda is proven to provide the best, safe and <a class="green-anchor"href="https://www.drshardaayurveda.com/fatty-liver">effective treatment for the liver</a> along with dietary modification</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                    What is the difference between <a class="green-anchor"href="https://www.drshardaayurveda.com/fatty-liver">fatty liver</a> and liver cirrhosis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion2">
                        <div class="card-body">
                            These both are diseases linked with the liver but there are some key differences between the two which are stated below:
                            <ul>
                                <li>Fatty liver is the condition of building up of fat in the liver but cirrhosis is scarring of the normal/healthy liver tissue</li>
                                <li>Fatty liver condition is more prevalent than liver cirrhosis.</li>
                                <li>Fatty liver disease takes a long to develop but liver cirrhosis is not preventable and is considered as the warning that the liver is deteriorating</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Does intake of ghee useful for treating liver cirrhosis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion2">
                        <div class="card-body">
                            Yes, ghee can be consumed along with the Ayurvedic medication. As ghee can absorb toxins and can easily remove it from your body. It detoxifies our bodies.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                What is the life expectancy of a patient with liver cirrhosis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion2">
                        <div class="card-body">
                            The life expectancy of an individual depends upon two stages of liver cirrhosis which is categorized as :
                            <ul>
                                <li>Compensated Cirrhosis – People are asymptomatic and can live up to 10 or more years</li>
                                <li>Decompensated Cirrhosis – Already shown symptoms and complications. The life span is greatly reduced and the patient has often suggested liver transplant surgery.</li>
                            </ul>
                            But Ayurveda has given positive results. Ayurvedic Treatment followed by dietary changes can help one to avoid a liver transplant
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                What are the four stages of liver disease?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion2">
                        <div class="card-body">
                            The four stages of liver disease from mild to severe are:
                            <strong> Stage 1 Inflammation</strong> Due to the accumulation of toxins in our body liver cells become inflamed and swelled up. <br />
                            <strong> Stage 2 Fibrosis</strong> Untreated liver inflammation leads to the conversion of a normal cell into scar cells, causing fibrosis or scarring of the liver. Therefore, the scarred liver would hinder the blood flow through the organ. <br />
                            <strong> Stage 3 Cirrhosis</strong> The irreversible and severe condition of scarring of the liver. The symptoms at this stage are visible like loss of appetite, nausea, and fatigue. <br />
                            <strong> Stage 4 Liver Failure</strong> The last stage is where the liver functioning is greatly hampered. And people have often suggested liver transplant surgery. <br />
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Is itchiness common in liver disease?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion2">
                        <div class="card-body">
                            Mostly the itchiness is experienced late evening or at night. The areas such as a limb, soles of the feet, and palms of the hand will tend to have more itchiness.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Liver Cirrhosis",
    "item": "https://www.drshardaayurveda.com/liver-cirrhosis"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "Does liver cirrhosis damage the brain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, liver cirrhosis can even hamper brain functioning. When the liver cells are unable to detoxify the toxins from the body, this may reach the brain and affect its functioning."
    }
  },{
    "@type": "Question",
    "name": "What are the symptoms of liver cirrhosis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The common symptoms include:
• Loss of appetite 
• Jaundice
• Dark-colored urine
• Itching on the skin
• Nausea
• Swelling of lower body
• Yellow discoloration of the body.
• Muscle cramps"
    }
  },{
    "@type": "Question",
    "name": "How can Ayurveda treat liver cirrhosis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurvedic medicines are the formulation of long-known herbs which are essentially helpful in treating disease. Treatment for liver cirrhosis in terms of Ayurveda focuses on detoxification of the liver. Papaya seeds can be used as a remedy for relief but it is always advised to consult experts before adding this to your diet."
    }
  },{
    "@type": "Question",
    "name": "What are the risk factors associated with liver cirrhosis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The risk factors of liver cirrhosis can be:
•   Hepatitis B/C
•   Autoimmune liver disease
•   Hemochromatosis
•   Fatty Liver Disease (FLD)"
    }
  },{
    "@type": "Question",
    "name": "Is liver cirrhosis disease preventable?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it is preventable with early examination and with proper Ayurvedic medication one can slow down further damage of the liver. There are ways one can adopt to minimize the damage to the liver:
•   Consume a fiber-rich diet.
•   Maintain healthy body weight.
•   Avoid intake of alcohol and drugs.
•   Ayurveda is proven to provide the best, safe and effective treatment for the liver along with dietary modification."
    }
  },{
    "@type": "Question",
    "name": "What is the difference between fatty liver and liver cirrhosis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "These both are diseases linked with the liver but there are some key differences between the two which are stated below:
•   Fatty liver is the condition of building up of fat in the liver but cirrhosis is scarring of the normal/healthy liver tissue.
•   Fatty liver condition is more prevalent than liver cirrhosis.
•   Fatty liver disease takes a long to develop but liver cirrhosis is not preventable and is considered as the warning that the liver is deteriorating."
    }
  },{
    "@type": "Question",
    "name": "Does intake of ghee useful for treating liver cirrhosis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, ghee can be consumed along with the Ayurvedic medication. As ghee can absorb toxins and can easily remove it from your body. It detoxifies our bodies."
    }
  },{
    "@type": "Question",
    "name": "What is the life expectancy of a patient with liver cirrhosis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The life expectancy of an individual depends upon two stages of liver cirrhosis which is categorized as :
•   Compensated Cirrhosis – People are asymptomatic and can live up to 10 or more years.
•   Decompensated Cirrhosis – Already shown symptoms and complications. The life span is greatly reduced and the patient has often suggested liver transplant surgery.
But Ayurveda has given positive results. Ayurvedic Treatment followed by dietary changes can help one to avoid a liver transplant."
    }
  },{
    "@type": "Question",
    "name": "What are the four stages of liver disease?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The four stages of liver disease from mild to severe are:
Stage 1 Inflammation
Due to the accumulation of toxins in our body liver cells become inflamed and swelled up.
Stage 2 Fibrosis
Untreated liver inflammation leads to the conversion of a normal cell into scar cells, causing fibrosis or scarring of the liver. Therefore, the scarred liver would hinder the blood flow through the organ. 
Stage 3 Cirrhosis
The irreversible and severe condition of scarring of the liver. The symptoms at this stage are visible like loss of appetite, nausea, and fatigue.
Stage 4 Liver Failure
The last stage is where the liver functioning is greatly hampered. And people have often suggested liver transplant surgery."
    }
  },{
    "@type": "Question",
    "name": "Is itchiness common in liver disease?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Mostly the itchiness is experienced late evening or at night. The areas such as a limb, soles of the feet, and palms of the hand will tend to have more itchiness."
    }
  }]
}
</script>
@stop