<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4">Depression</h3>
                    
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/depression">Depression</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatments">TREATMENTS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="definition">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/Depression.webp') }}" class="why-choose-us img-fluid" alt="Depression Treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Depression</b></h1>
                <p>
                Depression is considered a mood disorder that affects a person’s quality of life. It is a mental health mood disorder and the typical depressive episode is characterized by symptoms like <strong style="font-weight: bold;">unhappiness, loss of interest, hopelessness, slow, self-blame insomnia,</strong> etc. which lasts for at least 2 weeks. Some affect mood and others affect the body. Usually, symptoms come and go. It also varies in men, women, and children. This feeling is expressed differently from person to person. There are two types of depressive disorder i.e. major and persistent. Major depressive disorder is characterized by persistent feelings of sadness, loss of interest, weight loss or gain, slower thinking, fatigue, loss of concentration. The persistent depressive disorder also known as dysthymia is a milder but chronic type of depression that lasts for 2 yrs.  
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                    <p>
                    The lifetime risk of depression in females is 20 to 26% is more than in males 8 to 12 %. Sometimes women develop depression during the ovulation period of mensuration. Others get seasonal depression, which starts in late fall and early winters. This goes away itself during spring and summer. Depression affects health and quality of life. A depressed person feels like carrying a heavy burden on their head. Ayurvedic Medicine for depression is totally safe as it includes no steroids and can be taken at any stage.
                    </p>
                    <div class="btn-banner crd">
                        <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                        </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Depression</b></h2>
                    <p>The causes of Depression are still unknown but some causes are mostly seen</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Genetics
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                            Family history of depression or some past history can also be the cause. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Hormonal Changes
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            Sometimes some hormonal changes in the body can also lead to depression.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Certain Modern Medicine
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                            Some medicines like sleeping pills or blood pressure medication may also cause depression. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Early Childhood Trauma
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                            Early childhood trauma that affects the body to fear and stressful situation.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                Environmental Factors
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                            Some environmental factors like the death of a loved one, poverty, physical or sexual abuse, divorce also puts an individual into depression.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                   <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/depression-symptoms.webp') }}" class="img-fluid"alt="Depression symptoms">
        </div>
        <div class="cvn sym" >
            <h3><span>Symptoms</span> of Depression </h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Loss of Concentration Power</p>
            <p>Difficulty and loss in concentration, difficulty thinking clearly or quickly, feeling distracted, and focusing on some things that seem impossible. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Loss of Interest</p>
            <p>An individual loses his interest in happy moments, remains sad, unhappy, hopeless, worthless and lost, and also distracts them from the crowd.  </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Disturbed Sleep Cycle</p>
            <p>A patient feels trouble sleeping or sometimes oversleeping. It gets hard for them to get off the bed.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Suicidal Tendencies</p>
            <p>Recurrent thoughts of death or suicide as they think we are worthless and a burden on our family.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Random Eating Schedule</p>
            <p>A depressed person will have a loss of appetite i.e. eating a little or overeating i.e. eating too much. Their mood changes after every second. 
            </p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="treatments">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Ayurvedic Treatment for <b>Depression</b></h2>
                <p class="im3"> 
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Depression can be best managed by an Ayurvedic psychological approach like <strong style="font-weight: bold;">Daivavyapashraya, Satvavjaya, and Yuktivyapashraya Chikitsa</strong>.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurvedic detoxification and rejuvenation procedures help to improve mood and mental health.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Daily meditation for depression like Pranayam and other yoga asanas can help to heal mental and emotional disturbances and to relax the mind and body. 
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Following dinacharya (daily health regime) and ritucharya (seasonal health regime) is highly beneficial as it removes toxins and also stimulates the flow of natural energy in the body. 
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                     Depression Ayurvedic Treatment with therapies like shirodhara and marma massage restore the balance of dosha followed with sattvic diet and proper sleep regime helps in overcoming depression.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="3SGdFQJiJaw" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/3SGdFQJiJaw.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                            How does one comes to know that he/she is suffering from depression?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                        When one is unable to cherish the happy moments and remains sad and disappointed all of the time, loss of appetite, <a class="green-anchor"href="https://www.drshardaayurveda.com/insomnia">insomnia</a>, and suicidal thoughts then they are symptoms of depression. If one experiences any of these symptoms consult an <a class="green-anchor"href="https://www.drshardaayurveda.com/dr-mukesh-sharda">Ayurvedic doctor</a> at the earliest. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                    Can depression cause weight gain or loss?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                        Yes, increase in appetite and loss of appetite is main symptoms of depression so it is possible that depressive patient can have sudden weight gain or loss.  
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Why are women more prone to depression than males? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                        A woman goes through the various biological, lifecycle, hormonal, and psychosocial factors that link a woman to higher chances of depression By adolescence as is the peak time of hormonal and bodily changes so however girls become more likely to experience depression than boys.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Is depression is a serious condition or not?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                        Yes, it may be serious as it affects normal daily life as well as leads to other health-related issues like addiction and chemical dependence or even suicide. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion2">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    What are the preventive measures in depression?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion2">
                            <div class="card-body">
                            Get enough sleep, follow a healthy and nutritional diet, avoid alcohol and drugs, exercise regularly, spend quality time with family and friends and cherish every moment of life. 
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                How do I stop being sad?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion2">
                        <div class="card-body">
                        Start taking a back step from disappointment or failure, think positively, think of solutions to a problem, talk to someone for help, and have moral support. Try finding happiness in everything. Listen to music you like, eat your favorite food for a change.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                What are the side effects of overthinking? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion2">
                        <div class="card-body">
                        Difficulty swallowing, dry mouth, fatigue, irritability, headache, and body ache. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Is there a permanent cure for depression in Ayurveda? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion2">
                        <div class="card-body">
                        Yes, it can be permanently cured by Ayurvedic medicines and procedures. The biggest medicine to overcome depression is self-motivation. It depends on the stage of a patient that how long will it take to heal. There is no single answer for every patient about how long recovery could take as every individual has their own body type. <b>Ayurvedic Treatment for Depression</b> helps an individual to overcome depression for lifetime.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                What are the home remedies taken to cure depression?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion2">
                        <div class="card-body">
                        Cow’s ghee to be consumed in milk once a day and also few drops of cow’s ghee in nostrils and practice yoga and meditation daily. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Does depression causes hair loss?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion2">
                        <div class="card-body">
                        Yes, depression causes hair loss, stress and tension directly cause hair fall.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Depression",
    "item": "https://www.drshardaayurveda.com/depression"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "How does one comes to know that he/she is suffering from depression?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "When one is unable to cherish the happy moments and remains sad and disappointed all of the time, loss of appetite, insomnia, and suicidal thoughts then they are symptoms of depression. If one experiences any of these symptoms consult an Ayurvedic doctor at the earliest."
    }
  },{
    "@type": "Question",
    "name": "Can depression cause weight gain or loss?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, increase in appetite and loss of appetite is main symptoms of depression so it is possible that depressive patient can have sudden weight gain or loss."
    }
  },{
    "@type": "Question",
    "name": "Why are women more prone to depression than males?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "A woman goes through the various biological, lifecycle, hormonal, and psychosocial factors that link a woman to higher chances of depression By adolescence as is the peak time of hormonal and bodily changes so however girls become more likely to experience depression than boys."
    }
  },{
    "@type": "Question",
    "name": "Is depression is a serious condition or not?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it may be serious as it affects normal daily life as well as leads to other health-related issues like addiction and chemical dependence or even suicide."
    }
  },{
    "@type": "Question",
    "name": "What are the preventive measures in depression?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Get enough sleep, follow a healthy and nutritional diet, avoid alcohol and drugs, exercise regularly, spend quality time with family and friends and cherish every moment of life."
    }
  },{
    "@type": "Question",
    "name": "How do I stop being sad?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Start taking a back step from disappointment or failure, think positively, think of solutions to a problem, talk to someone for help, and have moral support. Try finding happiness in everything. Listen to music you like, eat your favorite food for a change."
    }
  },{
    "@type": "Question",
    "name": "What are the side effects of overthinking?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Difficulty swallowing, dry mouth, fatigue, irritability, headache, and body ache."
    }
  },{
    "@type": "Question",
    "name": "Is there a permanent cure for depression in Ayurveda?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it can be permanently cured by Ayurvedic medicines and procedures. The biggest medicine to overcome depression is self-motivation. It depends on the stage of a patient that how long will it take to heal. There is no single answer for every patient about how long recovery could take as every individual has their own body type. Ayurvedic Treatment for Depression helps an Individual to overcome depression for lifetime."
    }
  },{
    "@type": "Question",
    "name": "What are the home remedies taken to cure depression?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Cow’s ghee to be consumed in milk once a day and also few drops of cow’s ghee in nostrils and practice yoga and meditation daily."
    }
  },{
    "@type": "Question",
    "name": "Does depression causes hair loss?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, depression causes hair loss, stress and tension directly cause hair fall."
    }
  }]
}
</script>
@stop