<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Flatulence</h3>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/flatulence">Flatulence</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/Flatulence.webp') }}" class="why-choose-us img-fluid" alt="Flatulence treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Flatulence</b> Ayurvedic Treatment</h1>
                <p>
                More than 80 % of people irrespective of sex face the problem of flatulence. Flatulence is termed as passing gas or commonly known as farting, passing wind. Flatulence in medical terms is described as releasing of gas from the digestive system through the anus and passing of gas through the mouth is known as burping. It occurs when excessive gas gets collected inside the digestive system. Passing gas is quite normal and almost every human being does it consciously or unconsciously around 14-18 times a day. Flatulence is the result of the food that we eat. As the digestive system works, it makes gas. Every individual body functions in a different way. So some pass gas with odor (Smell) i.e.- smell comes from the bacteria in the intestine that releases a gas that contains sulfur.
                <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">

                    <ul>
                        Sometimes there is no odor (Smell) in passing gas. Gas gets collected in 2 ways-
                        <li>As the food gets digested in the body some gases like hydrogen, methane, and carbon dioxide get collected</li>
                        <li>While eating food or drinking, some of the gases like Oxygen and Nitrogen sometimes get collected in the digestive tract</li>
                    </ul>
                    <p>Not everyone experiences the gas with the same food products. <strong>Flatulence treatment in Ayurveda </strong>provides everlasting results and recovery</p>
                    <div class="btn-banner crd">
                        <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                        </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Flatulence</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Dietary Habits
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Eating heavy and fatty foods which are hard to digest.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Improper Bowel Functioning
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            If there is no proper bowel functioning, it can also lead to flatulence. 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Swallowing Air
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                            A smaller amount of air gets swallowed automatically when we eat or drink. But while smoking and eating too quickly more air gets accumulated in the body.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Stress
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                            If a person stays more stressed, will be affected with more gastric issues.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                Disturbed Sleep Pattern
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                                Disturbed sleep regime also leads to flatulence.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/flatulence-symptoms.webp') }}" class="img-fluid"alt="Flatulence symptoms">
        </div>
        <div class="cvn sym" >
            <h3><span>Symptoms of</span> Flatulence </h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Heaviness in the stomach</p>
            <p>When a person experiences full, tight heavy stomach and feels like eating nothing.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Bloating</p>
            <p>When the stomach is filled with excessive gas is bloating.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Burping</p>
            <p>An individual burps quite often and sometimes it is quite loud.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Weight loss</p>
            <p>Sometimes unexplained weight loss is also seen. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Joint and Muscle pain</p>
            <p>Gas sometimes gets collected in joints, thus causes pain.</p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Benefits of <b>Ayurvedic Treatment for Flatulence</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurveda focuses on improving diet to treat Flatulence.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Always keep yourself updated with handy natural digestives. They are quite helpful in an emergency.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    It is very important to keep yourself physically active. Never skip a day without exercising. 
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Yoga and meditation will keep your stress level low and your digestion process smooth.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    There are quite easily available effective Ingredients in your kitchen to treat flatulence. Follow home remedies to relieve Flatulence. Gastric treatment in ayurveda stops the issues from reoccurring. 
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="sR-v6exBo2s" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/sR-v6exBo2s.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
    </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Flatulence Testimonial" >
                            <h3 class="usrname">Suraj Singh</h3>
                            <p class="desig">Flatulence Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I chose Dr. Sharda Ayurveda for my gastric and flatulence issues. Their Ayurvedic treatment for flatulence helped me a lot in recovering. I religiously followed their Ayurvedic treatment and diet prescribed by them. I must say, <a class="green-anchor" href="https://www.drshardaayurveda.com/">Dr. Sharda Ayurveda</a> is the best Ayurvedic clinic in Ludhiana for flatulence treatment
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Flatulence Testimonial" >
                            <h3 class="usrname">Saroja Devi</h3>
                            <p class="desig">Gastric Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                For 2 years I was suffering from a gastric issue. Over time my problem increased and started getting worsen. I consulted Dr. Sharda Ayurveda for my treatment as one of my cousins suggested to me. I started with their treatment and within few days I could see a change in myself. I continued their medicines for 3 months as prescribed by doctors and I was relieved from gastric issues and flatulence.  
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Flatulence Testimonial" >
                            <h3 class="usrname">Sukran</h3>
                            <p class="desig">Gastric Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                At the age of 60’s my grandmother used to be upset due to gastric issues. She felt very uncomfortable the whole day long. She felt like eating nothing and was constipated too. We visited Dr. Sharda Ayurveda for her treatment. After their treatment, her constipation was treated within 2 days and she is in a recovery state from gastric issues too. We are very thankful to them. 
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                Why do I fart so much?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                        Excessive flatulence is caused due to swallowing more air than usual while eating and it is also caused due to eating those food items which is difficult to digest. It can also be related to a health problem that affects the digestive system such as recurring indigestion issues or <a class="green-anchor" href="https://www.drshardaayurveda.com/ibs">irritable bowel syndrome</a> (IBS). 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                How can I get rid of my daily passing of gas?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                            <ul>
                                <li>Stop Smoking</li>
                                <li>Chew food properly and slowly</li>
                                <li>Avoid carbonated drinks and Soda as they release carbon dioxide gas.</li>
                                <li>Treat Heartburn timely.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Where does the gas/flatulence originates from?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                            Intestinal gas originates in 2 waysatment.
                            <ul>
                                <li>Swallowed air i.e.- the undigested gas</li>
                                <li>Gas produced by the bacteria in the colon</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Sometimes my fart is smelly. Why does this happen?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                            A minor component of gas contains sulfur, which leads to smelly fart. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                I burp more than other people, why does this happen?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                        This depends mainly on your daily diet i.e. not only what you eat but also how you eat i.e. how you chew your food and also your workout routine. Eat without gulping and avoid drinking carbonated beverages.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    Why do I get more gastric issues after consuming dairy products?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                            It is very common as some people are unable to digest certain carbohydrates. Lactose present in milk causes gastric issues to many and is not easy for everyone to digest it thus causes gastric issues. 
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                What is the best Ayurvedic remedy for gastric?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                        For easy home remedy to relieve gastric issues and flatulence-
                        <ul>
                                <li>Take Ginger juice daily for few days or warm ginger water.</li>
                                <li>Cumin Seeds(Jeera) and fennel seeds both are beneficial herbs which are often used to ease bloating.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Why do I always have more gas at night in comparison to daytime?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                        If one has more gas at night, that is because you might do more snaking before going to bed. One should avoid eating 2-3 hours before going to bed because that leads to excessive production of gas.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                How can I clean my digestive system by adopting Ayurveda?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                        Panchakarma is prescribed for detoxification of the body. It begins with. 
                        <ul>
                            <li>Virechan: In it, cleansing of a body is done using powders, pastes, or heated medicinal plants.</li>
                            <li>Vaman: A person is made to do forced vomit or purging through herbal medicinal treatment.</li>
                            <li>By editing some dietary changes.</li>
                        </ul>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                What is the worst food that causes Flatulence?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                        Avoid eating-
                        <ul>
                            <li>Raw Onions</li>
                            <li>Beans</li>
                            <li>Vegetables like- Broccoli, Cabbage, Asparagus, and Cauliflower causes excessive gas in the body.</li>
                            <li>Processed foods</li>
                        </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Flatulence",
    "item": "https://www.drshardaayurveda.com/flatulence"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "Why do I fart so much?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Excessive flatulence is caused due to swallowing more air than usual while eating and it is also caused due to eating those food items which is difficult to digest. It can also be related to a health problem that affects the digestive system such as recurring Indigestion issues or Irritable bowel syndrome (IBS)."
    }
  },{
    "@type": "Question",
    "name": "How can I get rid of my daily passing of gas?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "•    Stop Smoking.
•   Chew food properly and slowly. 
•   Avoid carbonated drinks and Soda as they release carbon dioxide gas. 
•   Treat Heartburn timely."
    }
  },{
    "@type": "Question",
    "name": "Where does the gas/ flatulence originates from?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Intestinal gas originates in 2 ways:
•   Swallowed air i.e.- the undigested gas
•   Gas produced by the bacteria in the colon."
    }
  },{
    "@type": "Question",
    "name": "Sometimes my fart is smelly. Why does this happen?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "A minor component of gas contains sulfur, which leads to smelly farts."
    }
  },{
    "@type": "Question",
    "name": "I burp more than other people, why does this happen?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "This depends mainly on your daily diet i.e. not only what you eat but also how you eat i.e. how you chew your food and also your workout routine. Eat without gulping and avoid drinking carbonated beverages."
    }
  },{
    "@type": "Question",
    "name": "Why do I get more gastric issues after consuming dairy products?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It is very common as some people are unable to digest certain carbohydrates. 
Lactose present in milk causes gastric issues to many and is not easy for everyone to digest it thus causes gastric issues."
    }
  },{
    "@type": "Question",
    "name": "What is the best Ayurvedic remedy for gastric?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "For easy home remedy to relieve gastric issues and flatulence-
•   Take Ginger juice daily for few days or warm ginger water.
•   Cumin Seeds( Jeera) and Fennel seeds both are beneficial herbs which are often used to ease bloating."
    }
  },{
    "@type": "Question",
    "name": "Why do I always have more gas at night in comparison to daytime?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If one has more gas at night, that is because you might do more snaking before going to bed. One should avoid eating 2-3 hours before going to bed because that leads to excessive production of gas."
    }
  },{
    "@type": "Question",
    "name": "How can I clean my digestive system by adopting Ayurveda?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Panchakarma is prescribed for detoxification of the body. It begins with:
•   Virechan: In it, cleansing of a body is done using powders, pastes, or heated medicinal plants.
•   Vaman: A person is made to do forced vomit or purging through herbal medicinal treatment.
•   By editing some dietary changes."
    }
  },{
    "@type": "Question",
    "name": "What is the worst food that causes Flatulence?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Avoid eating-
•   Raw Onions
•   Beans
•   Vegetables like- Broccoli, Cabbage, Asparagus, and Cauliflower causes excessive gas in the body. 
•   Processed foods"
    }
  }]
}
</script>
@stop