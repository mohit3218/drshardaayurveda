<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4">PCOD (Polycystic Ovarian Disease)</h3>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/pcod-pcos">PCOD</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/pcos-pcod.webp') }}" class="why-choose-us img-fluid" alt="PCOS Treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Polycystic Ovarian Disease(PCOD)</b> Ayurvedic Treatment</h1>
                <p>
                Polycystic Ovarian Disease is a common female endocrine disorder affecting approximately 30% of women as it is found in reproductive age that is 20- 35, and now a days it is a leading cause of female infertility. It is considered as a lifestyle changing disease because in today’s life people are busy in their work schedule and they do not have any time to take care of their diet and body. It affects women particularly ovaries, where egg is released hence affects ovulation. Follicle Stimulating Hormone(FSH) and Luteinizing Hormone (LH) are produced by pituitary gland where FSH stimulate ovaries to produce follicle and LH trigger ovaries to release mature egg. In Polycystic Ovarian Disease many fluid filled sacs grow inside the ovaries and cause cyst there. These cysts are follicles with immature egg. Eggs in PCOD never mature enough to trigger ovulation. 
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                    <p>
                    Even male hormone i.e. androgen get secreted excess in women with PCOD, which also disrupt menstrual cycle. PCOD affects females mentally and physically. These are some features which develop like cyst in ovaries, high levels of male hormone, irregular or skipped periods. PCOD Ayurvedic treatment is safe and effective. 
                    </p>
                    <div class="btn-banner crd">
                        <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                        </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Polycystic Ovarian Disease</b></h2>
                    <p>The causes of PCOD are still unknown but some causes are mostly seen</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Obesity
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                            Overweight leads to decrease d metabolism, hence variation in hormones secretion which leads to Polycystic Ovarian Disease. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Heredity
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            Certain genetic sometimes run in family and its correlation may exist with PCOD in women.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Insulin Resistance
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                            Pancreas secrete insulin, body demand for insulin increases, hence pancreas secretes more insulin these extra insulin trigger ovaries to produce more male hormones. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Eating Unhealthy
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                            Eating more of food that is high in fats, sugar and carbohydrates. Stress-Higher levels of stress also hampers the body functioning and can also cause irregular periods. 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/pcos-symptoms.webp') }}" class="img-fluid"alt="PCOS symptoms">
        </div>
        <div class="cvn sym" >
            <h3><span>Symptoms</span> of Polycystic Ovarian Disease</h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Irregular Periods</p>
            <p>Due to lack of ovulation it leads to fewer periods.  </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Heavy Bleeding</p>
            <p>Due to thick lining of endometrium development, women suffer from heavy periods. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Hair Growth Over Face</p>
            <p>Due to increase in male hormones women develops hair on face and back of chest.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Infertility</p>
            <p>Women with Polycystic Ovarian Disease suffer from infertility because ovulation (in which mature egg released from the ovary) doesn’t occur.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Weight gain</p>
            <p>More than 80 percent of PCOD patient are seen with obesity.</p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Benefits of <b>Ayurvedic Treatment for Polycystic Ovarian Disease</b></h2>
                <p class="im3"> 
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    PCOD in Ayurveda is considered as rasome do dhatuvikara and has to be managed that includes Amaharachikitsa, sodhana and samana therapies with vatakaphadravyas, diet and life style modifications.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    With Ayurvedic medicines, a patient is advised to manage diet and improve lifestyle with some therapies.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Niruhavasti, anuvasnavasti is good for Polycystic Ovarian Disease also Nasya is done to balance kapha. 
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Avoid indulging in stress or overthinking. 
                </p>

                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Liquorice is a natural source of female hormones, should be given twice a day with milk.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="yXlgxwdj7Rc" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/yXlgxwdj7Rc.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Acidity Testimonial" >
                            <h3 class="usrname">Mrs Sunita</h3>
                            <p class="desig">PCOD Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was suffering from PCOD from a very longer time but completely got cured from the Polycystic Ovarian disease treatment in Ayurveda from <a class="green-anchor" href="https://www.drshardaayurveda.com/">Dr. Sharda Ayurveda</a>. Within 2 months of treatment my periods got back to normal which was the biggest satisfaction to me. 
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h2>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                            What is difference between PCOD and PCOS?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                        In Polycystic Ovarian Disease (PCOD) ovaries contain many immature or partially mature eggs and in Polycystic ovary syndrome (PCOS) ovarian produce high amount of androgen which leads to formation of more than 10 follicular cysts in ovary every month.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Is PCOD a serious disease?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                        Yes if it is left untreated then it leads to many diseases like diabetes, infertility, high cholesterol which leads heart disease and obesity. It evens gets harder for a girl to conceive. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                What is the main cause for PCOD?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                        PCOD is caused by hormonal imbalance in the body and excessive weight.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Which food should be avoided to get relief from PCOD?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                        Food items like white sugar, tea, coffee, white rice, muffins and cake. Avoid milk and soya products.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion2">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    How do I check PCOD? 
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion2">
                            <div class="card-body">
                            There is no single test to detect PCOD. There are some symptoms like irregular or heavy periods, facial hair, acne from which one gets to know about PCOD. Ayurvedic treatment for PCOD in Punjab is sure, safe and cost-effective. 
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Can I get pregnant with PCOD naturally? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion2">
                        <div class="card-body">
                        Usually it is difficult as ovulation is irregular. With Ayurvedic treatment of Polycystic Ovarian Disease, a girl can definitely get pregnant. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Can one have PCOD with regular periods? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion2">
                        <div class="card-body">
                        Yes with PCOD a patient can have regular periods. Sometimes a patient has amenorrhoea and other times get heavy bleeding.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Is it normal if periods skip for 3 months?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion2">
                        <div class="card-body">
                        No, women should have regular periods, because if female do not have periods for continuous 3 months, then that condition is called as amenorrhoea. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                What to do if periods are not regular? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion2">
                        <div class="card-body">
                        If periods are not regular, a patient needs to be checked by an Ayurvedic expert with diet modification, exercise. A patient is advised to take Ayurvedic medicines to regulate periods. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Is milk good for PCOD? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion2">
                        <div class="card-body">
                        Milk is rich source of Calcium and protein but in PCOD daily intake of milk products is indicated to reduce intake of carbohydrates in diet.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "PCOD-PCOS",
    "item": "https://www.drshardaayurveda.com/pcod-pcos"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What is difference between PCOD and PCOS?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "In PCOD ovaries contain many immature or partially mature eggs and in PCOS ovarian produce high amount of androgen which leads to formation of more than 10 follicular cysts in ovary every month."
    }
  },{
    "@type": "Question",
    "name": "Is PCOD a serious disease?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes if it is left untreated then it leads to many diseases like DM, infertility, high cholesterol which leads heart disease and obesity. It evens gets harder for a girl to conceive."
    }
  },{
    "@type": "Question",
    "name": "What is the main cause for PCOD?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "PCOD is caused by hormonal imbalance in the body and excessive weight."
    }
  },{
    "@type": "Question",
    "name": "Which food should be avoided to get relief from PCOS?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Food items like white sugar, tea, coffee, white rice, muffins and cake. Avoid milk and soya products."
    }
  },{
    "@type": "Question",
    "name": "How do I check PCOD?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "There is no single test to detect PCOD. There are some symptoms like irregular or heavy periods, facial hair, acne from which one gets to know about PCOD. Ayurvedic treatment for PCOD in Punjab is sure, safe and cost-effective."
    }
  },{
    "@type": "Question",
    "name": "Can I get pregnant with PCOS naturally?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Usually, it is difficult as ovulation is irregular. With Ayurvedic treatment of PCOD / PCOS, a girl can definitely get pregnant."
    }
  },{
    "@type": "Question",
    "name": "Can one have PCOS with regular periods?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes with PCOS a patient can have regular periods. Sometimes a patient has amenorrhoea and other times get heavy bleeding."
    }
  },{
    "@type": "Question",
    "name": "Is it normal if periods skip for 3 months?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "No, women should have regular periods, because if female do not have periods for continuous 3 months, then that condition is called as amenorrhoea."
    }
  },{
    "@type": "Question",
    "name": "What to do if periods are not regular?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If periods are not regular, a patient needs to be checked by an Ayurvedic expert with diet modification, exercise. A patient is advised to take Ayurvedic medicines to regulate periods."
    }
  },{
    "@type": "Question",
    "name": "Is milk good for PCOS?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Milk is rich source of Calcium and protein but in PCOS daily intake of milk products is indicated to reduce intake of carbohydrates in diet."
    }
  }]
}
</script>
@stop