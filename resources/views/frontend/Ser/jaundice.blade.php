<?php
/**
 * Jaundice Page 
 * 
 * @created    23/12/2021
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Jaundice</h3>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/jaundice">Jaundice</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/Jaundice.webp') }}" class="why-choose-us img-fluid" alt="jaundice treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Jaundice</b> Ayurvedic Treatment</h1>
                <p>
                Jaundice is described as yellowing of the skin texture, the whiteness of the eyes, dark urine, followed by itchiness. The term JAUNDICE is derived from the French word “Jaune”, which means yellow. Jaundice may not be categorized as a disease, but it is a visible sign of an underlying disease process that can be serious and chronic. In Ayurveda jaundice is described as Pitta dosha and is called as <b>Kamala</b>. Due to accumulation of Pitta in the body (bilirubin) the body tends to becomes infected. The sole reason for the yellowish shade is the high level of bilirubin in the body. Hemoglobin, part of RBC’S which carries oxygen forms the bilirubin which is broken down as part of the normal process of recycling old and damaged red blood cells. Bilirubin is carried in the bloodstream to the liver, where its role is to bind with bile (the digestive juice produced by the liver). Then it is moved through the bile ducts into the digestive tract so that it can be eliminated from the body which is a must.
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                    <p>
                    Most of the bilirubin is seen to be eliminated through stool, but a small amount is eliminated in urine. If it is not moved out of the body through the liver and bile duct then it gets built up in the blood and gets deposited on the skin resulting in jaundice. Jaundice has many causes i.e. it can be <b>caused due to hepatitis, gallstones, and tumors</b>. An inflamed liver or obstructed bile duct can lead to excessive bilirubin thus leading to the cause of jaundice. Jaundice if frequently occurs indicates a problem with the liver or bile duct. This disease is evaluated by the physical examination of the body.
                    </p>
                    <div class="btn-banner crd"><a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a></div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Jaundice</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Blocked Bile Ducts
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                            Bile ducts are thin tubes whose function is to carry fluid called bile from the liver and gallbladder to the small intestine. Sometimes in rare cases, these tubes get blocked by gallstones, cancer, or liver diseases. If this happens, it causes jaundice.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Hepatitis
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            Hepatitis is usually liver inflammation caused by a virus or due to an autoimmune disorder or the use of certain drugs for a longer period of time. In Hepatitis liver gets damaged, leading to its inability to move bilirubin into the bile ducts. Hepatitis may be acute or chronic
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Alcohol-Related Liver Disease
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                                Drinking alcohol over a longer period of time is automatically going to harm your liver. Mostly 2 diseases i.e., Alcoholic hepatitis and alcoholic cirrhosis harm the liver. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Certain Synthetic Medicines
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                            Overconsumption of some synthetic medicine over a longer period of time affects liver functioning
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Hemolytic Anemia
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                            Hemolytic anemia is considered to be a serious yet treatable blood disorder. This situation occurs when the body starts destroying red blood cells faster than making new ones as Red blood cells are the ones responsible for carrying oxygen to all the body cells.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz px-2.5" >
            <img src="{{ URL::asset('front/images/jaundice-symptoms.webp') }}" class="img-fluid"alt="jaundice symptoms">
        </div>
        <div class="cvn sym" >
            <h3><span>Symptoms of</span> Jaundice</h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Diarrhea and Nausea</p>
            <p>These are the most common symptoms of jaundice. In diarrhea, there is watery bowel movement and in nausea, it is an urge to vomit.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Dark-shaded Pee</p>
            <p>In jaundice, dark-colored urine is the most common symptom. It is caused due to less water intake required for the body.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Skin-itching</p>
            <p>Red-colored spots appear on the skin which is itchy and causes irritation.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Loss of Appetite</p>
            <p>An individual does not feel like eating and due to lack of proper nutrition one comes on the stage of weight loss.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Fatigue and Weakness</p>
            <p>Due to high fever in jaundice, an individual becomes quite weak and almost in a state of tiredness.</p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Benefits of <b>Ayurvedic Treatment for Jaundice</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Diet and lifestyle change play an important to recover from jaundice followed by Ayurvedic treatment.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Jaundice is also caused due to a lack of Vitamin B12 in the body. So include dairy products like milk, and yogurt and also add mushrooms which are the rich sources of Vitamin B12.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurvedic treatment for jaundice has effective and safe results so that jaundice does not happen to appear again and again. Ayurvedic treatment has been successfully used to treat jaundice.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Wheatgrass juice is a marvelous remedy for jaundice as it flushes out excess bilirubin and toxins from the body and also Buttermilk which is a rich source of calcium and iron required to recover from jaundice.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Vitamin C is also suggested to be included in the diet as it cleanses the liver. Some of the rich sources of Vitamin C are Aloe vera, Guava, Kiwi, and Broccoli.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <iframe src="https://www.youtube.com/embed/PGKM-htmG9A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Harpreet Singh</h3>
                            <p class="desig">Jaundice Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was worried about my health as I was getting weak and did not feel like doing anything. So I got myself tested and thereby I got diagnosed with jaundice. I visited Dr. Sharda Ayurveda where I was guided by the experts and after 3-4 months of regular Ayurvedic treatment, I was completely normal and free from Jaundice. I am thankful to Dr. Sharda Ayurveda
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Karana Kashyap</h3>
                            <p class="desig">Jaundice Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                My liver functioning was improper and I used to fall ill very easily. After every two months, I got affected by jaundice. I visited Dr. Sharda Ayurveda for my treatment. Within 2 months of the Ayurvedic treatment my health was improving and I continued the treatment for another 5 months. With their <b>Jaundice Ayurvedic treatment</b>, I recovered and my liver functioning is improving too.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Vikramjeet</h3>
                            <p class="desig">Jaundice Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                As I was more into alcohol consumption, my liver functioning was hampered due to which jaundice often happened to me. I was concerned about my deteriorating health condition. So I visited Dr. Sharda Ayurveda for the treatment of jaundice in Ayurveda where I was well guided by the experts. Within 6 months of regular Ayurvedic treatment followed by all the precautions that were guided to me. I was recovering so I suggest others visit Dr. Sharda Ayurveda for their Ayurvedic treatment.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1"><b>FAQ'S</b></h2>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                            Is liver dysfunction considered to be the sole reason for jaundice?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                        There are several causes for jaundice but yes liver dysfunction is also one of the most common reasons for jaundice.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                What do we mean by infant jaundice?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                        A problem that is affecting the infants prior to their birth. It is seen that babies that are born before the completion of the pregnancy or are under breastfeeding show jaundice symptoms. Its main reason for its occurrence is that the infant's liver is small in size is therefore not able to remove enough bilirubin from the body. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                If a person does not show yellowing of the skin. Will he still be affected by jaundice?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                        Yes, it is possible as sometimes the symptoms appear later. So knowing other symptoms are also essential which includes abdominal pain, discolored urine, fever, and weight loss, etc.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                What should be the ideal diet for jaundice patients?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                        Some of the essential elements that should be included in the diet are:
                        <ul>
                            <li>Vitamin B12 </li>
                            <li>Vitamin C</li>
                            <li>Green vegetables like spinach, broccoli</li>
                            <li>Juices intake such as aloe vera</li>
                            <li>Whole wheat</li>
                        </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                What dietary changes one should follow in jaundice?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                            The things that are suggested to be excluded from the diet include:
                            <ul>
                                <li>Excessive iron intake </li>
                                <li>Salty foods</li>
                                <li>Packaged products</li>
                                <li>Fatty or fried food</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    How Ayurveda is helpful in treating jaundice?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                            Ayurveda has proven to be the best and effective for a long time. Ayurveda provides therapies that help in the detoxification of the body's waste products.
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Can jaundice be treated with Ayurvedic treatment?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                        Yes, jaundice can be treated with Ayurveda as Ayurvedic herbs have inherited anti-bacterial, and anti-inflammatory properties in them. It mainly focuses on removing the toxins from the liver and body. Additionally, relaxes the liver muscles.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                How long does it take an individual to recover from jaundice?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                        It is seen that with proper expert guidance one can easily recover from jaundice within 2-3 months of Ayurvedic treatment followed by dietary and lifestyle changes.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                How can one prevent them from jaundice?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                        Some of the best suggestions one can follow to prevent jaundice are:
                        <br><strong>Restrict alcohol consumption</strong>Excessive alcohol intake directly damages the liver which leads to jaundice. Therefore, restricting alcohol consumption is mandatory to prevent jaundice. Treatment for jaundice in adults has effective results in Ayurveda.
                        <br><strong>Managing body weight</strong>Balancing the weight according to BMI is mandatory to stay disease-free.
                        <br><strong>Wholesome diet</strong>Intake of essential elements that are required by the body for its smooth and healthy functioning is a must
                        <br><strong>Exercising</strong>Regular exercises promote overall health status. To stay fit never miss on your workout routine.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Can a non-alcoholic person be affected by jaundice?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                            Yes, causes for jaundice are many so it is not always that an alcoholic person may only get affected with jaundice.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
@stop