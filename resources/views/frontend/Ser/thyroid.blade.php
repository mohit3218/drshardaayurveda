<?php
/**
 * thyroid Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="3000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4">Thyroid</h3>
                    <p style="font-size: 24px;">Break up with Thyroid through Ayurveda</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/thyroid">Thyroid</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a  href="#types">TYPES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="3000">
                    <img src="{{ URL::asset('front/images/thyroid1.webp') }}" class="why-choose-us img-fluid" alt="Thyroid Treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="3000">
                <h1 class="heading1"><b>Thyroid</b> Ayurvedic Treatment</h1>
                <p>
                    Thyroid occurs due to inadequate production of thyroid hormone. Any structural or functional defect of the thyroid gland leads to a decrease in the secretion of the hormone. Thyroid in Ayurveda cannot be considered as a single disease but is a syndrome, where an increase in pitta dosha leads to hypofunction of thyroid, Agni Dusti, stress contributes to the imbalance of Vata Dosha and therefore weight gain, heaviness, coldness are a sign of Kapha imbalance. In conclusion, we could say that imbalance of Vata, Pitta and Kapha leads to thyroid. The thyroid gland is one of the most important and sensitive endocrine gland.
                </p>
                <div class="btn-banner crd">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                        </div>
            </div>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec" id="types"> 
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 data-aos="fade-up" data-aos-duration="3000" class="aos-init aos-animate">Types of <b>Thyroid</b></h2>
                <p class="text-center">There are 2 types of Thyroid glands which are classified as:</p>
            </div>
        </div>
        <div class="row cut-row">
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="3000">
                <h2>Hypothyroidism</h2>
                <p>
                    It is an Autoimmune disorder with less production of thyroid hormone which causes hypothyroidism. Hypothyroidism is the most common thyroid disorder, and it affects around 6-10% of women. According to Ayurveda hypothyroidism can be considered as a condition that results due to Agni Dushti. 
                </p>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="3000">
                    <img src="{{ URL::asset('front/images/Hypothyroidism.webp') }}" class="why-choose-us img-fluid" alt="Hypothyroidism">
                </div>
            </div>
        </div>
        
        <div class="row cut-row">
            <div class="col-xl-6 col-lg-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="3000">
                    <img src="{{ URL::asset('front/images/Hyperthyroidism.webp') }}" class="why-choose-us img-fluid" alt="Hyperthyroidism">
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="3000">
                <h2>Hyperthyroidism</h2>
                <p>
                    In Ayurveda, it is termed, BHASMAK Rog. Approximately 1% of the population suffers from hyperthyroidism. Hyperthyroidism is an excess secretion of thyroid hormones in the body. It includes many conditions such as GRAVE’S diseases, Goiter and Thyroiditis. In Ayurveda, hyperthyroidism is a condition due to increased Pitta in the body. Due to this increased pitta, the digestive fire also increases further imbalances the metabolism of the body.
                </p>
            </div>
        </div>
<!--         <div class="row">
            <div class="col-md-12">
                <div class="btn-ayurvedic">
                    <a href="#" class="btn-appointment">Find More Treatment</a>
                </div>
            </div>
        </div>
    </div> -->
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="3000">
                    <h2 class="heading1">Causes of <b>Thyroid</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Role of Iodine
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                An imbalance of iodine in the diet can also cause hypothyroidism and in some cases, Goitre.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Stress
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                A major cause of thyroid is stress. From the Ayurvedic perspective, stress contributes to an imbalance of Vata Dosha which is involved in all disease processes. 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion3">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Variation in gland
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                            <div class="card-body">
                                Secretion in variations of thyroid glands is the main cause of thyroid. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Inflammation of Thyroid
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                            <div class="card-body">
                                Swelling is seen in the thyroid gland around the neck.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Pituitary Gland Malfunction
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion3">
                            <div class="card-body">
                                Over and lower secretion of pituitary gland hormone varies. 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>		

<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/Thyroid-Symptoms.webp') }}" class="img-fluid">
        </div>
        <div class="cvn sym px-4" >
            <h3><span>Symptoms</span> of Thyroid </h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Excessive sweating</p>
            <p>Excessive sweating and intolerance to heat is a big symptom.</p>
            <br>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Fatigue, lethargy, and general tiredness</p>
            <p>Restlessness, fatigue becomes part of daily routine. </p>
            <br>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Reduced ability to concentrate</p>
            <p>One loses the ability to concentrate on certain things.</p>
            <br>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Weight loss</p>
            <p>A patient suffers from major weight loss. </p>
            <br>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Feelings of anxiety and nervousness</p>
            <p>An individual suffers from mood swings and gets nervous easily. </p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Advantages of <b>Ayurvedic Treatment For Thyroid</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    In western medicine one will be required to take medications for life once one has been diagnosed with thyroid. In Ayurveda by Ayurvedic treatment, management of diet, lifestyle, and Ayurvedic herbs can stop this disease from progressing further.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurvedic treatment assist with the proper secretion of hormones and at the same time encouraging the proper absorption of nutrients.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Pranayama and Yoga create calm and relaxation and Ayurvedic treatment help relieve and control stress, which is the major contributor 
                    to thyroid dysfunction. 
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    The Ayurvedic treatment boosts the immune system and opens the body channels that control healthy metabolism.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    All the Ayurvedic medicines at Dr. Sharda Ayurveda are nature extracted and works on treating the root cause of disease.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="MWeclWxmnMI" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/MWeclWxmnMI.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="3000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Thyroid Reviews" >
                            <h3 class="usrname">Rajani Devi</h3>
                            <p class="desig">Thyroid Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p><span><i class="fa fa-quote-left" aria-hidden="true"></i>
                                </span>I was suffering from thyroid for 3 years and was continuously taking medicines to get rid of it. Despite taking medicines my thyroid kept on increasing and was getting hard to manage. One of my friends suggested me to visit <a class="green-anchor" href="https://www.drshardaayurveda.com/">Dr. Sharda Ayurveda</a> for the Ayurvedic treatment of my thyroid. I visited them and within 3 months of treatment, my thyroid started decreasing. I am very thankful to them. <span><i class="fa fa-quote-right" aria-hidden="true"></i>
                                </span></p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Patient Reviews" >
                            <h3 class="usrname">Parminder Kaur</h3>
                            <p class="desig">Thyroid Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Due to my Thyroid, my body weight was constantly increasing. I was on medications but was unable to manage them. I visited Dr. Sharda Ayurveda for Ayurvedic thyroid treatment and Dr.Mukesh Sharda prescribed me Ayurvedic medication with dietary changes. I am very happy and satisfied with the results of the medicines. 
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Patient Reviews" >
                            <h3 class="usrname">Kanchan Sharma</h3>
                            <p class="desig">Thyroid Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Dr. Sharda Ayurveda is best for the Ayurvedic treatment of the thyroid. I consulted them for my thyroid treatment. All the doctors are highly knowledgeable and guides in a perfect way. Their Ayurvedic medicines are quite effective and have good results.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="3000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                What are the best exercises for the thyroid?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            All strenuous exercises like walking, swimming, biking, jogging can be adopted, Ujjai Pranayam also helps in treating thyroid.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Which food affects the thyroid?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            Excessive intake of soy rich foods, certain vegetables like cabbage, broccoli, cauliflower, spinach, and fruits like sweet potato, peaches, strawberry, etc. Nuts like millets, peanuts, etc. can cause thyroid.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                What are the early warning signs of thyroid?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            High heart rate, excessive tiredness, anxiety, weight gain or loss, body shakes, and feeling chilly or overheated.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                How can I check thyroid malfunction at home?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                            Thyroid malfunction cannot be checked at home. One can get their blood tests to be done to check the value of t3,t4,tsh.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFive" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <i class="fa" aria-hidden="true"></i>
                                Can Ayurveda cure thyroid? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="card-body">
                            Ayurveda can but along with standard treatment mentioned with dietary changes because Ayurvedic medicines help in treating all complications that occur during hypothyroid.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                How can one reduce weight having a history of hypothyroid?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                        <div class="card-body">
                            By avoiding milk, milk products in the diet, having small and frequent meals, exercise daily, avoid carbs in the diet, take Dr. Sharda herbal powder to cure constipation. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Is coriander water good for the thyroid?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                        <div class="card-body">
                            Often heard that coriander is good in thyroid. Yes, it is quite effective due to its mineral content i.e.- It is rich in Vitamin B1, Vitamin B2, Vitamin B3. Consume it daily in the morning. 

                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                What if I miss my thyroid dose for the day?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                        <div class="card-body">
                            Missing on to your thyroid dose for the day will not harm your body as Ayurveda works on treating the root cause, not just symptom-based treatment.  
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                What is the time duration for thyroid treatment in Ayurveda?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                        <div class="card-body">
                            Ayurveda works differently for every individual. Sometimes if the disease is acute it takes approximately 2-3 months for recovery. A course of treatment depends upon the level of disease it is. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                What are early warning signs of thyroid?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                        <div class="card-body">
                            High heart rate, excessive tiredness, anxiety, weight gain or loss, body shakes, and feeling chilly or overheated are the warning signs of thyroid.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Thyroid",
    "item": "https://www.drshardaayurveda.com/thyroid"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What are the Best Exercises for the Thyroid?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "All strenuous exercises like walking, swimming, biking, jogging can be adopted, Ujjai pranayam also help in treating Thyroid."
    }
  },{
    "@type": "Question",
    "name": "Which food affects the Thyroid?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Excessive intake of Soy rich food, certain vegetables like cabbage, broccoli, cauliflower, spinach, and fruits like sweet potato, peaches, strawberry, etc. nuts like millets, peanuts, etc. can cause Thyroid."
    }
  },{
    "@type": "Question",
    "name": "What are early warning signs of Thyroid?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "High heart rate, excessive tiredness, anxiety, weight gain or loss, body shakes, and feeling chilly or overheated."
    }
  },{
    "@type": "Question",
    "name": "How can I check thyroid malfunction at home?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Thyroid malfunction cannot be checked at home. One can get their blood tests to be done to check the value of t3,t4,tsh."
    }
  },{
    "@type": "Question",
    "name": "Can Ayurveda cure Thyroid?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurveda can but along with standard treatment mentioned with dietary changes because Ayurvedic medicines help in treating all complications that occur during hypothyroid."
    }
  },{
    "@type": "Question",
    "name": "How can one reduce weight having a history of hypothyroid?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "By avoiding Milk, Milk products in the diet, having small and frequent meals, exercise daily, avoid carbs in the diet, take Dr. Sharda herbal powder to cure constipation."
    }
  },{
    "@type": "Question",
    "name": "Often heard that Coriander is good in Thyroid. Is Coriander water good for the Thyroid?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it is quite effective due to its mineral content i.e.- It is rich in Vitamin B1,vit.B2,vit.B3. Consume it daily in the morning."
    }
  },{
    "@type": "Question",
    "name": "What if I miss my Thyroid dose for the day?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Missing on to your Thyroid dose for the day will not harm your body as Ayurveda works on treating the root cause, not just symptom-based treatment."
    }
  },{
    "@type": "Question",
    "name": "What is the time duration for Thyroid treatment in Ayurveda?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurveda works differently for every individual. Sometimes if the disease is acute it takes approximately 2-3 months for recovery. A course of treatment depends upon the level of disease it is."
    }
  },{
    "@type": "Question",
    "name": "What are early warning signs of Thyroid?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "High heart rate, excessive tiredness, anxiety, weight gain or loss, body shakes, and feeling chilly or overheated are the warning signs of Thyroid."
    }
  }]
}
</script>
@stop