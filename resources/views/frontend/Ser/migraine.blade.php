<?php
/**
 * Migraine Page 
 * 
 * @created    03/02/2022
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2022
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4">Migraine</h3>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/migraine">Migraine</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatments">TREATMENTS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose"id="definition">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc">
                    <img src="{{ URL::asset('front/images/migraine.webp') }}" class="why-choose-us img-fluid" alt="migraine treatment">
                </div>
            </div>
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " >
                <h1 class="heading1"><b>Migraine</b> (Suryavartha Soola)</h1>
                <p>Today people are leading a stressful life which is directly impacting their mental and physical health. Migraine is just more than a normal or bad headache. Migraine is a neurological disease that causes shooting pain and pulsing sensations usually at one side of our head. The pain can last for a minute to even long hours but when the condition becomes severe it can last for a few days too. The “AURA” is said to be the alarming symptoms of migraine which are blurred vision, difficulty speaking, and loss of ability to concentrate. These are the reversible symptoms of the nervous system which occur before or during migraine. The triggers of the problem are stress, anxiety, hormonal imbalance, and an unhealthy diet. The complications are sleeplessness, nausea and vomiting, and vertigo. If not treated on time can lead to complications which are enough to make you discomfort.
                <span id="dots">...</span>
                </p>
                <span id="more-content">
                <p>
                In Ayurveda, it is known as <strong style="font-weight: bold;">“Suryavartha soola”</strong> where Surya means Sun and Avartha means a rise in pain which aggravates if not treated. The major cause of migraine is the sedentary lifestyle of the individual. The imbalanced Vata and Pitta doshas are the reason for the problem causing redness and intense pain in the eyes. It can also be associated with the pre-existing health condition of the digestive system. For the balancing of these doshas migraine, Ayurvedic treatment can help you get to recover from the migraine headache in a safe and effective way.
                </p>
                <div class="btn-banner crd">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                        </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate">
                    <h2 class="heading1">Causes of <b>Migraine</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Lack of sleep
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Poor sleeping schedule is one of the most common causes of migraine.
                            </div>
                        </div>
                    </div>                    
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Excessive Traveling
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                Physically and mentally exerting your mind and body is the major cause of migraine.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Stress and Anxiety
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">
                                Stress can directly impact brain functioning. Therefore its maintenance is of utmost importance for keeping yourself away from problems.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Dehydration
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                            <div class="card-body">
                                Dehydrated body can hinder proper body functioning.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Fragrance
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                Some people are allergic to certain kinds of fragrances which can indirectly trigger a migraine attack.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>			


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/migraine-symptoms.webp') }}" class="img-fluid" alt="Migraine Symptoms">
        </div>
        <div class="cvn sym" >
            <h3>Symptoms of<span> Migraine</span></h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Headache</p>
            <p>People experience a severe headache at one side of their head as throbbing or shooting pain.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Nausea and vomiting</p>
            <p>Prevalent abdominal pain and indigestion can be one of the symptoms of migraine.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Neck Stiffness</p>
            <p>Constant pain can hinder neck movement and pain.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Blurred Vision</p>
            <p>Migraine headache can directly affect the eye's vision. People experiencing constant pain often complain of blurred vision.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Inability to Concentrate</p>
            <p>Prevalent headaches can indirectly affect the concentrating ability of the individual.</p>
        </div>
    </div>
</div>
<div class="split"></div>
<section class="dsa-why-choose" id="treatments">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " >
                <h2 class="client-test-title"style="font-size: 24px !important;">Ayurvedic Treatment for <b>Migraine</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Panchkarma therapy for getting relief from migraine is highly recommended as it detoxifies thereby purifying the body. It does so by removing toxins from your body that is the cause of migraine.
                    <li>Shirodhara to relieve stress and anxiety by pouring medicated oil over your forehead</li>
                    <li>Body massage</li>
                    <li>Sweating therapy</li>

                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Yoga is a traditional practice of performing for ages. It helps in relaxing and calming the mind and body which directly reduces stress and anxiety which is the major cause of the problem. Various asana is suggested such as child pose and bridge pose.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurveda recommends a relaxation technique that helps in relaxing the mind, body, and soul thereby reducing stress and tension. Some of the best relaxation techniques are -
                    <li>Deep breathing</li> 
                    <li>Meditation</li>
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    In Ayurveda, the main focus is on understanding the cause of the migraine. The treatment is then given by accordingly understanding the root cause.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="PGKM-htmG9A" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/backpain.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Asthma Testimonial" >
                            <h3 class="usrname">Shivam Sharma</h3>
                            <p class="desig">Migraine Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Due to constant pain at one side of my head, I was not able to perform my day-to-day routine work. I tried various medications but could not find relief. So I consulted Dr. Sharda Ayurveda for the treatment where with 1 month of regular Ayurvedic treatment along with the lifestyle changes as recommended by the doctors I am now at a recovery stage. I am thankful to Dr. Sharda Ayurveda for their best and effective treatment. 
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Asthma Testimonial" >
                            <h3 class="usrname">Sonu Verma</h3>
                            <p class="desig">Migraine Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                My busy schedule and poor sleeping habits caused me severe pain in my head and stiff neck. I tried remedies for the problem but the situation gets more worsen. So I consulted Dr. Sharda Ayurveda for the treatment. Within just 2 months of the treatment followed by changes in my day-to-day routine helped me get relief from the problem. I am thankful to Dr. Sharda Ayurveda for their safe, best, and effective treatment.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                What factors can trigger migraine more often? 
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            The factors that can trigger migraine attack are:
                            <li>Stress</li><li>Poor sleeping schedule</li><li>Medications</li><li>Climate change</li>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                               Can a migraine headache damage my brain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                           No, it will not damage the brain.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                What treatment is best for migraine?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            Ayurvedic treatment is the best and effective treatment for getting relief from the migraine headache. Dr. Sharda Ayurveda treatment for migraine is proven to be safe, and effective with more than thousands of positive results.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                How one can get instant relief from a headache at their comfort? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                            The methods recommended are:
                                <li>Hot compression</li><li>Dim the light</li><li>Avoid chewing</li><li>Hydrate yourself</li><li>Practice meditation</li> But it is always advisable to consult your doctors before following these methods and also to get a permanent cure for the problem.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFive" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <i class="fa" aria-hidden="true"></i>
                                Does stress play a major role in developing migraine? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="card-body">
                            Yes, stress does play a role in emerging the problem. It indirectly hinders brain functioning and thereby severe headaches.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                What can migraine do to your brain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                        <div class="card-body">
                            As soon as the migraine triggers the brain would produce a signal in response to the trigger. These electrical signals would directly affect the blood flow to the brain causing brain nerves to be highly affected and severe pain.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                What is the major difference between migraine and headache?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                        <div class="card-body">
                            Headache last for a short duration of time but in migraine, the headache can last for up to 3 days and the pain become more severe at night.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                How Ayurveda can be beneficial in getting relief from migraine?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                        <div class="card-body">
                            Ayurvedic herbs are long being known to provide marvelous benefits. In Ayurveda first the root cause of the problem is detected and the treatment is then planned accordingly and if treated well can give you the long term cure from the migraine. Ayurveda is the safer, effective, and natural way of treatment with no harmful side effects.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                What are the four stages of a migraine attack?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                        <div class="card-body">
                            The four stages are:
                            <li><b>Prodrome –</b> It’s the warning signal that occurs one or two days before the attack.</li>
                            <li><b>Aura –</b> Occur before or during a migraine attack. These are the reversible response of the nervous system.</li>
                            <li><b>Attack –</b> The migraine attack varies from person to person. It can even last for minutes to hours or even for a few days if not treated timely.</li>   <li><b>Post-drome:</b> The constant pain occurring even after the attack. The persons feel drained out, tired, and restless.</li>  
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                What are the health complications if migraine is not treated timely?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                        <div class="card-body">
                            The complications include:
                            <li>Depression and anxiety</li>
                            <li>Sleeplessness</li>
                            <li>Vertigo</li> 
                            <li>Digestive problem</li>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<style>
    .carousel-inner {position: relative;width: 100%;overflow: hidden;}
    .carousel-item {position: relative;float: left;width: 100%;margin-right: -100%;-webkit-backface-visibility: hidden;backface-visibility: hidden;transition: -webkit-transform .6s ease-in-out;transition: transform .6s ease-in-out;transition: transform .6s ease-in-out,-webkit-transform .6s ease-in-out;}
    .card-body {padding-top: 1.5rem;padding-bottom: 1.5rem;-webkit-border-radius: 0!important;border-radius: 0!important;flex: 1 1 auto;-ms-flex: 1 1 auto;}
    .card .card-body a h3{font-size: 18px;color: #ff9000;line-height: 1.4;}
    .card .card-body h3{color: #373433;font-size: 18px;    line-height: 1.4;}
    .font-weight-bold {font-weight: 700!important;}
    .card-title {margin-bottom: .75rem;}
    .card-img-top {width: 100%;border-top-left-radius: calc(.25rem - 1px);border-top-right-radius: calc(.25rem - 1px);}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            responsiveClass: !0,
            navigation : true,
            autoplay: true,
            loop: true,
            smartSpeed: 2500,
            singleItem : true,
            navigationText: ["<i class='fa fa-angle-left fa-2'></i>", "<i class='fa fa-angle-right fa-2'></i>"],
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                480: {
                    items: 1,
                    nav: !1
                },
                768: {
                    items: 2,
                    nav: !1
                },
                1000: {
                    items: 2,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1024: {
                    items: 3,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1366: {
                    items: 3,
                    nav: !1
                }
            }
        })
    });

</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Migraine",
    "item": "https://www.drshardaayurveda.com/migraine"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What factors can trigger migraine more often?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The factors that can trigger migraine attack are:
•   Stress
•   Poor sleeping schedule 
•   Medications
•   Climate change"
    }
  },{
    "@type": "Question",
    "name": "Can a migraine headache damage my brain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "No, it will not damage the brain."
    }
  },{
    "@type": "Question",
    "name": "What treatment is best for migraine?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurvedic treatment is the best and most effective treatment for getting relief from migraine headaches. Dr. Sharda Ayurveda treatment for migraine is proven to be safe, and effective with more than thousands of positive results."
    }
  },{
    "@type": "Question",
    "name": "How one can get instant relief from a headache at their comfort?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The methods recommended are:
•   Hot compression
•   Dim the light 
•   Avoid chewing 
•   Hydrate yourself 
•   Practice meditation 
But it is always advisable to consult your doctors before following these methods and also to get a permanent cure for the problem."
    }
  },{
    "@type": "Question",
    "name": "Does stress play a major role in developing migraine?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, stress does play a role in emerging the problem. It indirectly hinders brain functioning and thereby severe headaches."
    }
  },{
    "@type": "Question",
    "name": "What can migraine do to your brain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "As soon as the migraine triggers the brain would produce a signal in response to the trigger. These electrical signals would directly affect the blood flow to the brain causing brain nerves to be highly affected and severe pain."
    }
  },{
    "@type": "Question",
    "name": "What is the major difference between migraine and headache?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Headache last for a short duration of time but in migraine, the headache can last for up to 3 days and the pain become more severe at night."
    }
  },{
    "@type": "Question",
    "name": "How Ayurveda can be beneficial in getting relief from migraine?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurvedic herbs are long being known to provide marvelous benefits. In Ayurveda first the root cause of the problem is detected and the treatment is then planned accordingly and if treated well can give you the long term cure from the migraine. Ayurveda is the safer, effective, and natural way of treatment with no harmful side effects."
    }
  },{
    "@type": "Question",
    "name": "What are the four stages of a migraine attack?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The four stages are:
•   Prodrome – It’s the warning signal that occurs one or two days before the attack.
•   Aura – Occur before or during a migraine attack. These are the reversible response of the nervous system.
•   Attack – The migraine attack varies from person to person. It can even last for minutes to hours or even for a few days if not treated timely.
•   Post-drome – The constant pain occurring even after the attack. The persons feel drained out, tired, and restless."
    }
  },{
    "@type": "Question",
    "name": "What are the health complications if migraine is not treated timely?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The complications include:
•   Depression and anxiety
•   Sleeplessness
•   Vertigo 
•   Digestive problem"
    }
  }]
}
</script>
@stop