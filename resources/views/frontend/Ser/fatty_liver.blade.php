<?php
/**
 * Diabetes Page 
 * 
 * @created    21/09/2021
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2021
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Fatty Liver</h3>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/fatty-liver">Fatty Liver</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#types">TYPES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/fatty-liver.webp') }}" class="why-choose-us img-fluid" alt="Fatty Liver treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1">Fatty Liver Ayurvedic Treatment</h1>
                <p>
                Fatty liver disease is affecting about 20 percent of people globally and is associated with diabetes and obesity. Fatty liver is also termed hepatic steatosis. The liver is the second largest organ of the body which helps process nutrients from food and drinks and is also responsible for filtering harmful substances from the blood. So it is very important to take care of the liver. Fatty liver means an increase in the fat amount in cells of the liver. If there is less amount of fat in the liver, it is considered normal but excessive fat can be a serious complication. Too much fat in the liver causes inflammation in the liver which further leads to liver failure. When 5 percent of cells of the liver have these fat-filled cells inside the liver, it is termed as fatty liver. Fatty liver Ayurvedic treatment has the most effective results and is safe to be consumed at any age and stage.
                </p>
                <div class="btn-banner crd"><a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                        </div>
            </div>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec fattyliver-sec" id="types"> 
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="heading1">Types of <b>Fatty Liver</b></h2>
            </div>
        </div>
        <div class="row cust-iba">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                <div class="iba-box box1 fattyliver">
                    <div class="xd">
                        <div class="cont">  
                            <div class="wht-space"></div>
                            <h3>Alcoholic fatty liver</h3>
                            <p>
                                It is caused due to over drinking alcohol. The more alcohol goes in the more you damage your liver. Alcoholic fatty liver disease is the earliest and common stage of alcohol-related liver disease.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                <div class="iba-box box2 fattyliver2">
                    <div class="xd">
                        <div class="cont">
                            <div class="wht-space"></div>
                            <h3>Nonalcoholic fatty liver disease</h3>
                            <p>
                                It is a disease that is not related to heavy alcohol use. It is caused due to accumulation of fat due to dietary and sedentary lifestyles. This condition when turns serious are termed NASH, where inflammation of cells of the liver occurs and fibrosis, scar tissue, and liver cell destruction is seen.
                                </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="btn-ayurvedic">
                    <a href="#" class="btn-appointment">Find More Treatment</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Fatty Liver</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Intake of Extra Carbs
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                            Diabetic and obese patients are seen in habit of taking refined carbs, which leads to extra fat storage in the liver hence fatty liver.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Impaired Gut Health
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            Imbalance in gut bacteria or its function also leads to fatty liver.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Obesity
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                            Being obese or overweight causes fatty liver as sometimes obesity leads to inflammation of cells of the liver due to fat storage. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Insulin Resistance
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                            In type 2 diabetes due to varied metabolism a patient has chances of developing fatty liver. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                Virus Infection
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                            Some types of infections, such as Hepatitis C attacks the liver. 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz px-2.5" >
            <img src="{{ URL::asset('front/images/fatty-liver-symptoms.webp') }}" class="img-fluid"alt="Fatty Liver symptoms">
        </div>
        <div class="cvn sym" >
            <h3><span>Symptoms of</span> Fatty Liver</h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Loss of Appetite</p>
            <p>A patient does not feel like eating and loses his appetite.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Off and On Abdominal Pain</p>
            <p>Pain in the abdominal region sometimes occurs all of sudden which is severe and goes away on its own. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Weakness</p>
            <p>A patients feel restless and lack energy. Weakness is often seen in patients with fatty liver.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Increased Levels of Liver Enzymes</p>
            <p>Elevated liver enzymes indicate inflammation or damage to cells in the liver. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Jaundice</p>
            <p>A patient is seen with yellowish skin and eyes i.e. <a class="green-anchor"href="https://www.drshardaayurveda.com/jaundice">Jaundice</a>.</p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Benefits of <b>Ayurvedic Treatment for Fatty Liver</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurvedic treatment for fatty liver deals with <b>Nidanparivarjan</b> by avoiding causative factors. Also, avoid skipping meals of the day and do not get into fasting as this aggravates the Pitta Dosha.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Maintainance of ideal weight is a must. Exercise is a must for health and also for reducing obesity. Add more fluids to your diet so that toxins can easily flush out.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    It is advised to avoid fermented, pungent, sour, and salty foods. Adopt a fat-free diet for early and healthy recovery.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Panchkarma is quite beneficial according to rog and Prakriti of a patient. Adopt Ayurveda for fatty liver for life-long healing.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Practice yoga, pranayama, and meditation daily. Do not miss doing them. 
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <iframe src="https://www.youtube.com/embed/PGKM-htmG9A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Fatty Liver Testimonial" >
                            <h3 class="usrname">Raj Gupta</h3>
                            <p class="desig">Fatty Liver Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Myself Raj Gupta is a fatty liver patient for 8 months. I am taking Fatty Liver Disease Ayurvedic treatment from Dr. Sharda Ayurveda. Within 1 month of treatment, I can see myself recovering and I am ongoing treatment from them. They provided me a personalized diet chart and I am following the same. Thanks to Dr. Sharda Ayurveda for providing me with the best Ayurvedic treatment.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1"><b>FAQ'S</b></h2>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                            How to remove excess fat from the liver?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                        Physical activity is an effective and simplest way to decrease liver fat. It is often said that exercising daily significantly helps in reducing the amount of extra fat stored in liver cells, regardless of whether weight loss occurs.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Which food items are bad for the liver?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                        Fast and junk food is a poor choice to keep your liver healthy. Do not eat too much food high in saturated fat as it can make it harder for the liver to do its job. And over time it may also lead to inflammation, which can even lead to scarring of the liver, and that condition is known as cirrhosis.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                What are the signs of an unhealthy liver?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                        If you get such symptoms, then you are at a risk of unhealthy liver functioning-
                        <ul>
                            <li>Jaundice</li>
                            <li>Pain in the abdomen and swelling</li>
                            <li>Swelling mostly in the legs and ankles.</li>
                            <li>Itchy skin.</li>
                            <li>Dark urine color</li>
                            <li>Fatigue</li>
                            <li>Nausea or vomiting</li>
                        </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                How can one make their liver strong?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                        For a healthy and strong liver, it is most important to maintain a healthy weight. Eat a balanced diet and exercise regularly. Also, avoid the consumption of alcohol.
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    What are the different stages of fatty liver?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                                <ul>
                                    <li>It begins with</li>
                                    <li>Simple fatty liver</li>
                                    <li>Steatohepatitis i.e. inflammation of liver cells occurs.</li>
                                    <li>Fibrosis i.e. scarring of cell.</li>
                                    <li>Cirrhosis i.e. scarring of the liver is widespread</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Will losing weight help in treating fatty liver? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                        Yes, definitely. Losing weight is itself the best medication for treating fatty liver. For healthy and life-long recovery you can get liver diseases treatment with Ayurveda.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                    Who is more at risk of getting fatty liver?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                        Persons who are into excessive Alcohol consumption are at the highest chances of getting a fatty liver. Also, diabetes, high cholesterol, and obese persons are prone to be affected by it. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                	Is Ashwagandha good for the liver?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion4">
                        <div class="card-body">
                        Yes, Ashwagandha has very good results as it relieves stress, anxiety, liver health condition and even removes toxins from the liver, and prevents liver damage.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Fatty Liver",
    "item": "https://www.drshardaayurveda.com/fatty-liver"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "How to remove excess fat from the liver?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Physical activity is an effective and simplest way to decrease liver fat. It is often said that exercising daily significantly helps in reducing the amount of extra fat stored in liver cells, regardless of whether weight loss occurs."
    }
  },{
    "@type": "Question",
    "name": "Which food items are bad for the liver?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Fast and junk food is a poor choice to keep your liver healthy. Do not eat too much food high in saturated fat as it can make it harder for the liver to do its job. And over time it may also lead to inflammation, which can even lead to scarring of the liver, and that condition is known as cirrhosis."
    }
  },{
    "@type": "Question",
    "name": "What are the signs of an unhealthy liver?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If you get such symptoms, then you are at a risk of unhealthy liver functioning-
1.  Jaundice
2.  Pain in the abdomen and swelling.
3.  Swelling mostly in the legs and ankles.
4.  Itchy skin.
5.  Dark urine color.
6.  Fatigue.
7.  Nausea or vomiting"
    }
  },{
    "@type": "Question",
    "name": "How can one make their liver strong?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "For a healthy and strong liver, it is most important to maintain a healthy weight. Eat a balanced diet and exercise regularly. Also, avoid the consumption of alcohol."
    }
  },{
    "@type": "Question",
    "name": "Is Ashwagandha good for the liver?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, Ashwagandha has very good results as it relieves stress, anxiety, liver health condition and even removes toxins from the liver, and prevents liver damage."
    }
  },{
    "@type": "Question",
    "name": "What are the different stages of fatty liver?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "1.    It begins with- 
2.  Simple fatty liver
3.  Steatohepatitis i.e. inflammation of liver cells occurs.
4.  Fibrosis i.e. scarring of cell
5.  Cirrhosis i.e. scarring of the liver is widespread."
    }
  },{
    "@type": "Question",
    "name": "Will losing weight help in treating fatty liver?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, definitely. Losing weight is itself the best medication for treating fatty liver. For healthy and life-long recovery you can get liver diseases treatment with Ayurveda."
    }
  },{
    "@type": "Question",
    "name": "Who is more at risk of getting fatty liver?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Persons who are into excessive Alcohol consumption are at the highest chances of getting a fatty liver. Also, diabetes, high cholesterol, and obese persons are prone to be affected by it."
    }
  }]
}
</script>
@stop