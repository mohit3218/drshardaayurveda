<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<div class="online-consultation">
    
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Cervical Spondylosis</h3>
                    <p style="font-size: 24px;">Protect Your Joints with Timely Treatment</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/cervical">Cervical Spondylosis</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/Cervical.webp') }}" class="why-choose-us img-fluid" alt="why choose">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Cervical Spondylosis</b> Ayurvedic Treatment</h1>
                <p>
                    It is commonly known as arthritis of the neck and is a Vata disorder. When there is wear and tear of cartilage, disk, ligament, and bones of cervical vertebrae, it is called cervical spondylosis which finally leads to neck pain, stiffness in the neck.
                    All these changes are caused by the normal wear and tear due to aging. As with growing age, the disc of the spine gradually breaks down, starts losing fluid and volume, and also becomes stiff, dried, and cracked and ligaments get thickened, and spur formation occurs in bone. It usually occurs with middle-aged that start at the age of early ’30s and elderly people. According to Ayurveda, it is known as <strong>Grivasandhigatvata.</strong>
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                    <p>
                        These cervical vertebrae are 7 in number which begins at the base of the skull and ends at the thoracic vertebrae. Through vertebrae, spinal cord, and nerve passes carry a message from the brain to other parts of the body i.e. muscles and organs.
                        A sedentary lifestyle, wrong posture, prolonged sitting in the same position makes the problem more complicated and worst. Cervical spondylosis treatment in Ayurveda has the most effective results. 
                    </p>
                    <div class="btn-banner crd">
                        <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                        </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Cervical Spondylosis</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Injury
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                            Any Injury in the body can cause pain but injury near or on the neck, back or arm can cause cervical pain, and also holding the neck in an uncomfortable position for a longer duration causes pain.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Wrong Body Posture
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            If sitting posture is incorrect like watching the screen continuously for hours, more sedentary habits also cause pain. 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Bone Spurs
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                            Bone spurs formation at edges of vertebrae or called osteophytes development causes cervical pain.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Herniated Discs
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                            Herniation of spinal disk which bulge out and starts pressing nearby Tissues or spinal nerve.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                Degenerated Spinal Disc
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                            Degeneration of spinal disks of cervical vertebrae leads to thinning of disk and losing elasticity of soft tissue.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/Cervical-symptoms.webp') }}" class="img-fluid" alt = "Cervical Spondylosis symptoms">
        </div>
        <div class="cvn sym" >
            <h3><span>Symptoms of</span> Cervical Spondylosis </h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Weak Muscles</p>
            <p>Weak muscles make it hard to lift the arms or grasp the objects firmly.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Neck Pain</p>
            <p>Stiffness and pain in the neck or at the back of the neck and near the shoulder blade. . </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Headache</p>
            <p>Headaches due to muscular spasm and stiffness at the back of the neck. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Feeling Loss of Body Balance and Dizziness</p>
            <p>Dizziness on the movement of the neck and one also loses its body balance sometimes. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Grinding Noise</p>
            <p>Sensation of popping and grinding sound on movement or turning of the neck. </p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Benefits of <b>Ayurvedic Treatment for Cervical Spondylosis</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Acharya Sushruta described Cervical which is caused by sleeping during day time, by using pillow wrongly during sleep and gazing constantly in upward direction leads to an aggravation of two Doshas Vata and Kapha.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Combination of Ayurvedic therapies like Grivabasti, Nasya, Ayurvedic medicines has a significant role in the management of cervical spondylosis or Osteoarthritis of the cervical spine.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Exercise your neck every day by slowly stretching your head in side-to-side and then up and down motion.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Keep changing your position often. Never sit or stand in one position for a longer duration.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Never use a neck brace or collar without your Ayurvedic doctor’s approval.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="QX-7BHDo2io" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/QX-7BHDo2io.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Surjeet Kaur</h3>
                            <p class="desig">Cervical Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was living with cervical pain for years. I used to get physiotherapy done and used to get relief from pain for 2 days and then back to chronic pain again. I could not find its cure until I got my treatment done by Dr. Sharda Ayurveda. I consulted them for Ayurvedic treatment for cervical and the results were highly appreciated. I finally recovered from cervical pain
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                            How can one know that it is cervical pain or some other pain?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                        When there is severe pain on and around the neck, stiffness, numbness in the arm, headache, and tingling sensation. If you feel these symptoms then it is cervical pain and you should consult an Ayurvedic expert. Never assume anything on your own and before beginning with any medicine do visit Best Cervical treatment hospitals in India. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Which food items should be avoided in cervical pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                        Avoid eating a diet rich in protein, Bananas, a high carb diet, Alcohol, canned, and processed food. They will lead to an increase in pain. Processed food cause weakness in bones and make them brittle, Alcohol leads to loss of calcium and magnesium in the body and Smoking leads to bone damage
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Which food should be consumed to get relief from cervical pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                        An anti-inflammatory like Turmeric, Ginger should be added to a daily diet. Also, coconut water, wheatgrass, Amla, and green leafy vegetables help in relieving pain.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                I am suffering from chronic pain in the neck due to cervical, would surgery help me to get rid of the pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                        To surgery, it is a big No as surgery is not the treatment to get rid of the pain. Ayurvedic medicine for cervical spondylosis has the best results and Ayurveda has the best herbs to treat cervical
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                What is the most serious complication of cervical
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                        Chronic back pain with stiffness that causes a tingling sensation in the fore and hind limbs.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    What happens if cervical is left untreated?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                            Neck pain increases each passing day and degeneration of bones leads to fusion of bones together creating more muscle stiffness and rigidity, hence causes restricted movement. 
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Does cold weather affects cervical spondylosis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                        Coldwater increases more muscle stiffness and neck pain. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                What is the difference between spondylitis and spondylosis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                        Spondylitis means the inflammatory condition of joints whereas spondylosis is wear and tear of the disk, ligament, bone, and cartilage.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                Can cervical cause paralysis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                        Yes, if C4 vertebrae get hurt, it impairs phrenic nerve function thus causes paralysis. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Do cervical affect eyes?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                        Usually, patients complain of blurred vision, headache, vertigo, nausea, palpitations, and tinnitus in the cervical. So it can impact the eyes too.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Cervical",
    "item": "https://www.drshardaayurveda.com/cervical"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "How can one know that it is cervical pain or some other pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "When there is severe pain on and around the neck, stiffness, numbness in the arm, headache, and tingling sensation. If you feel these symptoms then it is cervical pain and you should consult an Ayurvedic expert. Never assume anything on your own and before beginning with any medicine do visit Best Cervical treatment hospitals in India."
    }
  },{
    "@type": "Question",
    "name": "Which food items should be avoided in cervical pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Avoid eating a diet rich in protein, Bananas, a high carb diet, Alcohol, canned, and processed food. They will lead to an increase in pain. Processed food cause weakness in bones and make them brittle, Alcohol leads to loss of calcium and magnesium in the body and Smoking leads to bone damage."
    }
  },{
    "@type": "Question",
    "name": "Which food should be consumed to get relief from cervical pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "An anti-inflammatory like Turmeric, Ginger should be added to a daily diet. Also, coconut water, wheatgrass, Amla, and green leafy vegetables help in relieving pain."
    }
  },{
    "@type": "Question",
    "name": "I am suffering from chronic pain in the neck due to cervical, would surgery help me to get rid of the pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "To surgery, it is a big No as surgery is not the treatment to get rid of the pain. Ayurvedic medicine for cervical spondylosis has the best results and Ayurveda has the best herbs to treat cervical."
    }
  },{
    "@type": "Question",
    "name": "What is the most serious complication of cervical?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Chronic back pain with stiffness that causes a tingling sensation in the fore and hind limbs."
    }
  },{
    "@type": "Question",
    "name": "What happens if cervical is left untreated?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Neck pain increases each passing day and degeneration of bones leads to fusion of bones together creating more muscle stiffness and rigidity, hence causes restricted movement."
    }
  },{
    "@type": "Question",
    "name": "Does cold weather affects cervical spondylosis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Coldwater increases more muscle stiffness and neck pain."
    }
  },{
    "@type": "Question",
    "name": "What is the difference between spondylitis and spondylosis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Spondylitis means the inflammatory condition of joints whereas spondylosis is wear and tear of the disk, ligament, bone, and cartilage."
    }
  },{
    "@type": "Question",
    "name": "Can cervical cause paralysis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, if C4 vertebrae get hurt, it impairs phrenic nerve function thus causes paralysis."
    }
  },{
    "@type": "Question",
    "name": "Do cervical affect eyes?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Usually, patients complain of blurred vision, headache, vertigo, nausea, palpitations, and tinnitus in the cervical. So it can impact the eyes too."
    }
  }]
}
</script>
@stop