<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Sinusitis</h3>
                    <p style="font-size: 22px;">Breathing Healthy with Ayurveda</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/sinusitis">Sinusitis</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a  href="#types">TYPES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/Sinusitis-treatment.webp') }}" class="why-choose-us img-fluid" alt="Sinusitis treatment" title="Sinusitis treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Sinusitis</b> Ayurvedic Treatment</h1>
                <p>
                Sinusitis is the inflammation of the tissue lining of sinuses. The Ayurvedic treatment for sinusitis is the safest and most authentic approach that treats the disease from its underlying cause, thus minimizing the chances of symptoms recurrence in the future. The ancient method of healing disease is based on stabilizing the aggravated doshas (Vata and Kapha dominance in sinusitis patients) and eliminating the toxins from the body. It is the most common respiratory ailment that has affected almost 1/4th of the population on a worldwide scale. It is also called by the name rhinosinusitis because the symptoms appear to be associated with both nose and sinuses. The infection is mostly triggered by viruses or bacteria, and the discomforting symptoms are recurrent.
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                    </p>
                <span id="more-content">
                <p>The main signs include discolored nasal drainage and nose blockage that persist for more than a week or get worsen with each passing day. The infection affects the nasal cavities within the nose that are four paired and called paranasal sinuses. It has a thin mucus lining, that drains out of the passage of the nose. Many times patients also get diagnosed with nasal polyps, a problem that probably occurs due to improper cleaning of the drainage system. In Ayurveda, it completely resembles Suryavarta and occurs due to the predominance of two Vata and Kapha doshas.
                    </p></span>
                <div class="btn-banner crd"><a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec" id="types"> 
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="heading1">Types of <b>Sinusitis</b></h2>
            </div>
        </div>
        <div class="row cust-iba">
            <div class="owl-carousel owl-theme" >
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/acute-sinusitis.webp') }}" class="card-img-top lazyload" alt="Acute sinusitis" title="Acute sinusitis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Acute sinusitis</h4>
                            <p class="card-text">
                                It specifically causes symptoms including runny, stuffy nose accompanied by facial pain. The discomfort persists for 10-12 days.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/subacute-sinusitis.webp') }}"  alt="Subacute sinusitis" title="Subacute sinusitis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Subacute sinusitis</h4>
                            <p class="card-text">
                                The inflammation of the tissue lining of the sinuses that can cause pain for at least 4 to 7 weeks.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/chronic-sinusitis.webp') }}" alt="Chronic sinusitis" title="Chronic sinusitis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Chronic sinusitis</h4>
                            <p class="card-text">The patient may experience nasal congestion, facial pressure, and nasal blockage that persists for at least more than 7 weeks.</p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/recurrent-sinusitis.webp') }}"  alt="Recurrent sinusitis" title="Recurrent sinusitis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Recurrent sinusitis</h4>
                            <p class="card-text">
                                The repetition of the symptoms, more often.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Sinusitis</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Nasal Polyps
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                            The growth of tissue can block the nasal passages or sinuses.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Respiratory Tract Infection
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            The infection of the respiratory tract can significantly cause inflammation and thickening of the sinus membrane, accompanied by blockage of the cavities.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Sinus due to allergy
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                            The disease can occur due to a compromised immune system, i.e., the defense mechanism's inability to generate an effective response against environmental pollutants, and chemical irritants.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Nasal Septum
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                            The wall between the nostrils blocks the sinus passages, making a person difficult to even breathe properly.  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz px-2.5" >
            <img src="{{ URL::asset('front/images/Sinusitis-symptoms.webp') }}" class="img-fluid"alt="sinusitis symptoms" title="sinusitis symptoms">
        </div>
        <div class="cvn sym" >
            <h3><span>Symptoms of</span> Sinusitis </h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Mucus Discharge</p>
            <p>The runny nose and thick discolored discharge is mostly seen.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Postnasal Drainage</p>
            <p>Drainage from down the back of the throat.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Difficulty Breathing</p>
            <p>Blocked, and congested nose can cause difficult breathing via the nasal passage.</p>
            <ul>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Reduced sense of taste and smell.</p><br>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Swelling </p>
            <p>The severe pain accompanied by inflammation can be observed around the eyes, nose, and forehead.</p>
            <ul>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Benefits of <b>Ayurvedic Treatment for Sinusitis</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Nasya also known as Shirovirechna, is the technique where the herbal formulations in the form of powder, and oil are administered via the nostrils to promote the secretion of mucus lodged in sinuses. Massage of the head and steam inhalation is done to liquify the mucus.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    The procedure called Jal Neti Kriya is used to cleanse the nasal path by removing mucus and opening blockages. If followed for a particular time period, it can effectively reduce the chances of getting affected by sinusitis. 
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Taking nasal steam inhalation is one of the recommended techniques to ease the discomfort. The volatile oil that can be used for this purpose includes tulsi, peppermint, and camphor which are to be added to water and inhaled.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Practice yoga and meditation daily for the best results. The poses suggested for treating respiratory diseases include Bhujangasana and Sarvangasana.
                </p>                
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="xFGNXX3DAhM" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/xFGNXX3DAhM.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">
                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Patient's <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Sinusitis Patient Testimonial" Title=" Rubail Testimonial">
                            <h3 class="usrname">Rubail </h3>
                            <p class="desig">Sinusitis Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Myself Rubail was suffering from sinusitis for the past 2-3 years. I was experiencing constant coughing and sneezing that were more than enough to make my life miserable. Even after consulting specialists, sadly, none of the treatments and medications show positive results. But thanks to Dr. Sharda Ayurveda as the Ayurvedic doctor's guidance just provide relief within just 7 months. I would highly recommend everyone to consult Dr. Sharda Ayurveda. 
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                               What all can trigger sinusitis?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                        The presence of causative factors like viruses, bacteria, chemical irritants, and pollution can potentially trigger the condition.   
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Can a person be diagnosed with sinusitis without facing mucus discharge?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                        Usually, sinusitis occurs due to the drying of the sinus cavity. It can significantly cause severe inflammation of the nasal cavity.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                How turmeric is helpful in getting relief from sinusitis infection?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                        Yes, turmeric is very helpful for combating infections. Take 1/4th tsp. of turmeric powder and add it to boiling water. Drinking this thrice a day will provide effective relief from inflammation, as it is high in anti-inflammatory properties and also opens narrow nasal passages.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Can Ayurveda cure sinusitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                        Yes, Ayurveda offers a holistic and authentic approach to treating sinusitis. The herbal medications along with strictly following certain dietary and lifestyles altogether aim to provide positive and long-term results without any side effects.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                How long does it take for a sinus infection to clear on its own?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                        The best method to get perfect relief includes drinking plenty of fluids that effectively clears the nasal passages.</strong>. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    Is there any diagnostic procedure that may be used to detect sinusitis?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                            The types of diagnostic tests that are used to detect sinusitis include allergy tests, CT scans, endoscopy, mucus culture studies, and X-Ray. 
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                What treatment is usually recommended to treat sinusitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                        <strong><b>Ayurvedic Treatment for sinusitis</b> is the most preferred medical practice as it naturally heals the patients without even suggesting surgeries and prevents long-term dependency on medications. Usually, nasal drops are mostly recommended to help reduce congestion. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                What are the lifestyle changes that one should opt to treat sinusitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                        If you are facing sinusitis issues, adopt these below-mentioned lifestyle modification tips for a positive recovery.
                        <li>Quit smoking.</li>
                        <li>Reduce stress.</li>
                        <li>Wash your hands more often.</li>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                What happens if the sinus is left untreated?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                        Sinus if left untreated, causes unnecessary pain and discomfort. A patient usually complains of a running nose after every alternate day. In rare cases, failure to treat sinusitis can also result in meningitis and also infection of the bone or bone marrow. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                What are the foods that must be avoided if suffering from sinusitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                            Food like dairy products, processed sugar, refined carbohydrates, and Omega-6 fatty acids increases the chances of symptoms appearance and flare-up.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')
@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Sinusitis",
    "item": "https://www.drshardaayurveda.com/sinusitis"  
  }]
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What all can trigger sinusitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The presence of causative factors like viruses, bacteria, chemical irritants, and pollution can potentially trigger the condition."
    }
  },{
    "@type": "Question",
    "name": "Can a person be diagnosed with sinusitis without facing mucus discharge?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Usually, sinusitis occurs due to the drying of the sinus cavity. It can significantly cause severe inflammation of the nasal cavity."
    }
  },{
    "@type": "Question",
    "name": "How turmeric is helpful in getting relief from sinusitis infection?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, turmeric is very helpful for combating infections. Take 1/4th tsp. of turmeric powder and add it to boiling water. Drinking this thrice a day will provide effective relief from inflammation, as it is high in anti-inflammatory properties and also opens narrow nasal passages."
    }
  },{
    "@type": "Question",
    "name": "Can Ayurveda cure sinusitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, Ayurveda offers a holistic and authentic approach to treating sinusitis. The herbal medications along with strictly following certain dietary and lifestyles altogether aim to provide positive and long-term results without any side effects."
    }
  },{
    "@type": "Question",
    "name": "How long does it take for a sinus infection to clear on its own?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The best method to get perfect relief includes drinking plenty of fluids that effectively clears the nasal passages."
    }
  },{
    "@type": "Question",
    "name": "Is there any diagnostic procedure that may be used to detect sinusitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The types of diagnostic tests that are used to detect sinusitis include allergy tests, CT scans, endoscopy, mucus culture studies, and X-Ray."
    }
  },{
    "@type": "Question",
    "name": "What treatment is usually recommended to treat sinusitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurvedic Treatment for sinusitis is the most preferred medical practice as it naturally heals the patients without even suggesting surgeries and prevents long-term dependency on medications. Usually, nasal drops are mostly recommended to help reduce congestion."
    }
  },{
    "@type": "Question",
    "name": "What are the lifestyle changes that one should opt to treat sinusitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If you are facing sinusitis issues, adopt these below-mentioned lifestyle modification tips for a positive recovery. 
1. Quit smoking. 
2. Reduce stress.
3. Wash your hands more often."
    }
  },{
    "@type": "Question",
    "name": "What happens if the sinus is left untreated?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Sinus if left untreated, causes unnecessary pain and discomfort. A patient usually complains of a running nose after every alternate day. In rare cases, failure to treat sinusitis can also result in meningitis and also infection of the bone or bone marrow."
    }
  },{
    "@type": "Question",
    "name": "What are the foods that must be avoided if suffering from sinusitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Food like dairy products, processed sugar, refined carbohydrates, and Omega-6 fatty acids increases the chances of symptoms appearance and flare-up."
    }
  }]
}
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            responsiveClass: !0,
            navigation : true,
            autoplay: true,
            loop: true,
            smartSpeed: 2500,
            singleItem : true,
            navigationText: ["<i class='fa fa-angle-left fa-2'></i>", "<i class='fa fa-angle-right fa-2'></i>"],
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                480: {
                    items: 1,
                    nav: !1
                },
                768: {
                    items: 2,
                    nav: !1
                },
                1000: {
                    items: 2,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1024: {
                    items: 3,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1366: {
                    items: 3,
                    nav: !1
                }
            }
        })
    });

</script>
@stop