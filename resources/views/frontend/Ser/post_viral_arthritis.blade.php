<?php
/**
 * Post-Viral Arthritis Joint Page 
 * 
 * @created    17/01/2022
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2022
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Post-Viral Arthritis</h3>
                    <p style="font-size: 24px;">Protect your joints with timely treatment</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/post-viral-arthritis">Post-Viral Arthritis</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#managements">MANAGEMENTS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc">
                    <img src="{{ URL::asset('front/images/post-viral-arthrits.webp') }}" class="why-choose-us img-fluid" alt="Post- Viral Arthritis">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Post- Viral Arthritis</b></h1>
                <p>
                Arthritis is defined as the inflammation of the joints. If the disease is due to viral infection then it is termed Viral Arthritis. As per the name <strong style="font-weight: bold;">Post Viral Arthritis</strong> means all the pain, swelling, and stiffness of joints that occurs immediately after or during viral infection, which usually diverts indications of symptoms of arthritis. The most prone areas of infection are – <strong style="font-weight: bold;">ankles, toes, hands, wrists, and knees</strong>. Several viruses are triggers of this disease some of which are <strong style="font-weight: bold;">dengue, parvovirus B16, Hepatitis, Rubella, and retrovirus</strong>. It can easily heal if taken into consideration or early treatment but the symptoms are prolonged and can lead to serious health conditions. The symptoms of the disease vary from the severity of the disease. This viral arthritis is usually self-limiting but can even last for several weeks to years. This is associated with the impairment of the day-to-day activity of the person causing severe pain. Women and people who frequently smoke are more likely or at a higher risk of getting Post viral arthritis.
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                    <p>
                    The sole reason for infection to occur is the counter-attack of the body’s immune system. The immune system gets activated as soon as it encounters a foreign antigen and accordingly antibodies are formed. But when antibodies are not made against antigens this is the sign of a weaker immune system. This causes inflammation, and pain in joints. Post viral arthritis treatment in Ayurveda is best and effective. The herbal medications are essentially beneficial for getting relief from the pain caused due to the disease. In Ayurveda, it is termed as the imbalance of Vata Dosha. <a class="green-anchor"href="https://www.drshardaayurveda.com/post-viral-arthritis"><strong style="font-weight: bold;">Viral arthritis treatment in Ayurveda</strong></a> is done with anti-inflammatory and pain-relieving ingredient which relieves joint pains and inflammations.
                    </p>
                    <div class="btn-banner crd">
                        <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                    </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate">
                    <h2 class="heading1">Causes of <b>Post-Viral Arthritis</b></h2>
                    <p>The other viruses that can cause post-viral arthritis are <strong style="font-weight: bold;">Mumps, Retrovirus, Alphavirus, Chikungunya.</strong></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Hepatitis 
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <ul>These are categorized according to their severity i.e.,
                                    <li>Hepatitis A causes severe pain in joints but not arthritis.</li>
                                    <li>Hepatitis B and C can cause pain as well as arthritis.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    HIV 
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                This virus directly affects the immune system of the body thereby causing arthritis in some of the cases.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Flavivirus
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                                The dengue and zika viruses are the prominent members of this family which causes illness, swelling, and severe joint pain.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Enterovirus  
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                                This virus can directly affect the joints causing swelling, and pain on the affected part.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Rubella Virus
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                                It has the potential to cause chronic inflammatory joint diseases. It is more prevalent in women compared to men.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/post-viral-arthrits-symptoms.webp') }}" class="img-fluid"alt="Post-Viral Arthritis symptoms">
        </div>
        <div class="cvn sym" >
            <h2 class="heading1">Symptoms of <span>Post-Viral Arthritis</span></h2>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Fever</p>
            <p>This is the initial symptom that occurs as soon as the virus infects the body.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Swelling and Painful Joints</p>
            <p>The symptoms are caused due to infection caused by certain viruses restricting the joint movement to a greater extend.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Nausea and Vomiting</p>
            <p>People often remain sick and feel dizziness. There is also difficulty in taking proper diet as they are unable to digest food.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Skin Rashes</p>
            <p>Appearance of red, itchy patches all over the affected area causes extreme itchiness. The skin remains dry, and dull all the time.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Redness Around Joints</p>
            <p>Localized warmth and redness around the affected joints can cause severe pain.</p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Ayurvedic Treatment for <b>Post-Viral Arthritis</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        With Panchkarma complete detoxification of the body is done and prevented from any further viral infection attack.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        Post viral arthritis causes pain which is the feature of vitiated Vata dosha and an increase of Rooksha property of Vata. Hence in Ayurveda massaging joints with herbal oils and intake of hot meals helps early recovery of the body.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        Imbalanced Tri-Dosha which mainly occurs in post-viral arthritis is the priority to deal with and if managed properly the patient gets immediate relief from the signs and symptoms of the disease.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        Agni plays an important role in the development of disease. Hence Agni is controlled by using Agni Vardak Yog to heal the pain.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        Turmeric can be consumed to get relief from the pain as it has anti-inflammatory properties. Take 1 tsp. of turmeric in 1 glass of warm water and consume it to get effective results.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div id="dna_video">
                    <div class="youtube-player" data-id="PGKM-htmG9A" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                        <img src="{{ URL::asset('front/images/backpain.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                        <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Sunil Kumar</h3>
                            <p class="desig">Viral Arthritis Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was previously affected with dengue even after taking medications my joints were constantly paining and were swelled too. There were visible red patches all over my knees. So I consulted Dr. Sharda Ayurveda for the treatment and within 2 months of regular treatment followed by dietary modification I am now at a recovery stage. I am thankful to Dr. Sharda Ayurveda.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Jasveer Singh</h3>
                            <p class="desig">Viral Arthritis Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                    There was a constant pain in my left shoulder for the last 5 months. I take medicines but did not get any effective results. So one of my friends suggested visiting Dr. Sharda Ayurveda. I visited there and within 1 month of treatment, I got relief from the problem. The experts here guide you the right way and follow natural treatment i.e., dietary and lifestyle changes. I am thankful to Dr. Sharda Ayurveda for their effective treatment.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Simran Kaur</h3>
                            <p class="desig">Viral Arthritis Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Due to chikungunya, there were prevalent red rashes all over my body. For this, I consulted Dr. Sharda Ayurveda for the treatment. With experts guidance and regular treatment for about 2 weeks, I am now at a recovery stage. I thank Dr. Sharda Ayurveda for their effective treatment. 
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid faqs">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                How long does post-viral arthritis last?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                            Post viral arthritis is self-limiting and can last for a week to months. But if not treated timely the patient can go to a chronic stage where the inflammation and pain in joints are the symptoms. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                How Ayurveda is beneficial for treating post-viral arthritis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                            Ayurvedic treatment for Chikungunya arthritis or post-viral arthritis has proven to provide positive results. The herbal formulations make Ayurveda a natural treatment by following a holistic approach.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Do lifestyle and dietary changes can help in recovery from post-viral arthritis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                            Yes, it is possible by adopting a healthy lifestyle and including a nutrient-rich diet.
                            <ul>
                                <li>Dairy products should be strictly avoided</li>
                                <li>Balance body weight</li>
                                <li>Do regular exercise and meditation.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                 Does turmeric help in recovery from post-viral arthritis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                            Yes, it can be beneficial for getting relief from the joint pain and swelling caused due to post-viral arthritis. This is so as it has anti-inflammatory and antiseptic properties.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                What food items are to be avoided if having post-viral arthritis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                            The food items that should be avoided are:
                            <ul>
                                <li>Dairy products</li>
                                <li>Refined carbohydrates.</li>
                                <li>Sugar</li>
                                <li>Fried and processed food</li>
                                <li>Alcohol and tobacco</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    What are the symptoms of post-viral arthritis?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                                The symptoms of post-viral arthritis are:
                                <ul>
                                    <li>Joint pain and swelling</li>
                                    <li>Nausea</li>
                                    <li>Skin rashes</li>
                                    <li>Fever</li>
                                    <li>Redness around painful joint</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Which viruses are responsible for viral arthritis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                            The major viruses that are responsible for viral arthritis are:
                                <ul>
                                    <li>Rubella </li>
                                    <li>Hepatitis A, B, C </li>
                                    <li>Parvovirus </li>
                                    <li>Alphavirus</li>
                                    <li>Mumps</li>
                                </ul>
                            The other viruses involved are flavivirus and herpes.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Who are at higher risk of developing long-term viral arthritis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                            Adopting an unhealthy lifestyle and dietary changes are the major reason for the development of long-term viral arthritis. But the people who smoke and women are highly prone or at high risk to develop long-term viral arthritis.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                Do viruses have the potential to get settled in your joints?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                            Yes, it does have the potential if not treated on time. The infection spread all over the body through the bloodstream causing infection in joints. The symptoms can be pain, swelling, and body ache.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Which food items are recommended to recover from post-viral arthritis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                            The food items recommended are:
                                <ul>
                                    <li>Ginger and turmeric</li>
                                    <li>Canola and olive oil </li>
                                    <li>Vegetables – carrots, cauliflower, and broccoli</li>
                                    <li>Whole-grain- quinoa </li>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')

<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Post-Viral Arthritis",
    "item": "https://www.drshardaayurveda.com/post-viral-arthritis"  
  }]
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "How long does post-viral arthritis last?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Post viral arthritis is self-limiting and can last for a week to months. But if not treated in timely the patient can go to a chronic stage where the inflammation and pain in joints are the symptoms."
    }
  },{
    "@type": "Question",
    "name": "How Ayurveda is beneficial for treating post-viral arthritis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurvedic treatment for Chikungunya arthritis or post-viral arthritis has proven to provide positive results. The herbal formulations make Ayurveda a natural treatment by following a holistic approach."
    }
  },{
    "@type": "Question",
    "name": "Do lifestyle and dietary changes can help in recovery from post-viral arthritis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it is possible by adopting a healthy lifestyle and including a nutrient-rich diet. 
•   Dairy products should be strictly avoided
•   Balance body weight 
•   Do regular exercise and meditation."
    }
  },{
    "@type": "Question",
    "name": "Does turmeric help in recovery from post-viral arthritis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it can be beneficial for getting relief from the joint pain and swelling caused due to post-viral arthritis. This is so as it has anti-inflammatory and antiseptic properties."
    }
  },{
    "@type": "Question",
    "name": "What food items are to be avoided if having post-viral arthritis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The food items that should be avoided are: 
•   Dairy products
•   Refined carbohydrates.
•   Sugar
•   Fried and processed food
•   Alcohol and tobacco"
    }
  },{
    "@type": "Question",
    "name": "What are the symptoms of post-viral arthritis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The symptoms of post-viral arthritis are :
•   Joint pain and swelling
•   Nausea
•   Skin rashes 
•   Fever 
•   Redness around painful joint"
    }
  },{
    "@type": "Question",
    "name": "Which viruses are responsible for viral arthritis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The major viruses that are responsible for viral arthritis are: 
•   Rubella 
•   Hepatitis A, B, C 
•   Parvovirus 
•   Alphavirus 
•   Mumps 
The other viruses involved are flavivirus and herpes."
    }
  },{
    "@type": "Question",
    "name": "Who are at higher risk of developing long-term viral arthritis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Adopting an unhealthy lifestyle and dietary changes are the major reason for the development of long-term viral arthritis. But the people who smoke and women are highly prone or at high risk to develop long-term viral arthritis."
    }
  },{
    "@type": "Question",
    "name": "Do viruses have the potential to get settled in your joints?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it does have the potential if not treated on time. The infection spread all over the body through the bloodstream causing infection in joints. The symptoms can be pain, swelling, and body ache."
    }
  },{
    "@type": "Question",
    "name": "Which food items are recommended to recover from post-viral arthritis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The food items recommended are: 
•   Ginger and turmeric 
•   Canola and olive oil 
•   Vegetables – carrots, cauliflower, and broccoli
•   Whole-grain- quinoa"
    }
  }]
}
</script>

@stop