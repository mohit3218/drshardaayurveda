<?php
/**
 * Asthma Services Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4">Asthma</h3>
                    <p style="font-size: 22px;">Breathing Healthy With Ayurveda</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/asthma">Asthma</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</section>

<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a  href="#types">TYPES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose"id="treatment">>
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/asthma-treatment.webp') }}" class="why-choose-us img-fluid" alt="asthma treatment"title="Asthma Ayurvedic Treatment">
                </div>
            </div>
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1">Ayurvedic Treatment for <b>Asthma</b></h1>
                <p>
                    Before knowing the Ayurvedic treatment for asthma, the basics of asthma must be understood. In Ayurveda, Asthma is known as ThamakaShvasa. Asthma is a chronic inflammatory disease which directly affects the airways. It is symbolized by episodes of frequent nasal allergies, difficulty in breathing, shortness of breath and wheezing mostly in early morning or at mid night which might be due to respiratory origin. In Asthma there is a spontaneous attack on patient in which one is unable to breath because of airway blockage. The severity of an attack may differ from one attack to the next, from person to person, it may be life threatening and could require immediate emergency medical attention.
                  <span id="dots" data-aos="fade-up" data-aos-duration="3000">...</span>
                </p>
                <span id="more-content">
                <p>
                    Asthma develops when a body is exposed to high & cold temperatures, unhealthy air pollutants, environmental factors and taking high stress. All the impurities carry itself through the body’s channels to the chest and lungs area, where they irritate the lining of airways, also triggering an inflammatory response that harms the epithelial lining of the airways. <strong>Asthma treatment with Ayurveda</strong> has its guaranteed results. We never suggest any of our patients to go for Inhalers. We always treat naturally. Ayurvedic Medicine for Asthma has effective and amazing results.
                </p>
                </span>
                <div class="btn-banner crd">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec" id="types"> 
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="heading1">Types of <b>Asthma</b></h2>
            </div>
        </div>
        <div class="row cust-iba">
            <div class="owl-carousel owl-theme">
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/Acute Asthma.webp') }}" class="card-img-top lazyload" alt="acute asthma" title= "Acute Asthma">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Acute Asthma </h4>
                            <p class="card-text">
                                It is a spontaneous attack caused by an inflammation of the air sacs of the lungs and that further causes the narrowing and contraction of the bronchioles causing a restriction of airflow making breathing difficult for a person.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/Chronic Asthma.webp') }}" alt="Chronic Asthma" title="Chronic Asthma">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Chronic Asthma </h4>
                            <p class="card-text">
                                Chronic asthma is portrayal of frequent asthma attacks which requires medical help to prevent and minimize acute attacks.
                            </p>
                        </div>
                    </div>
                </div>


                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/Allergic Asthma.webp') }}"  alt="Allergic Asthma" title="Allergic Asthma">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Allergic Asthma</h4>
                            <p class="card-text">Allergic Asthma is caused due to exposure to an allergen, for example- dust particles or pollen grains.</p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/Non-allergic Asthma.webp') }}"  alt="Non-allergic Asthma" title="Non-allergic Asthma">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Non-allergic Asthma</h4>
                            <p class="card-text">
                                Non-allergic Asthma can be caused due to various factors such as stress, exercise, illness, extreme weather, irritants in the air.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!--         <div class="row">
            <div class="col-md-12">
                <div class="btn-ayurvedic">
                    <a href="#" class="btn-appointment">Find More Treatment</a>
                </div>
            </div>
        </div> -->
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Asthma</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Excessive intaka of Vata
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Chilled cold items highly taken can cause asthma as it directly affects the lungs tissues.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Environmental Conditions
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                Unhealthy environment conditions can cause asthma.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Exposure To Unhealthy Air
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                Unhealthy dust, smoke and wind can lead to cough.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Genetics
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">
                                Asthma takes it form from heredity too and passes itself.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Exposure To Cold Climate
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                            <div class="card-body">
                                High exposure to cold and icy climate increases the chances of Asthma.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>			


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/Asthma-Symptoms.webp') }}" class="img-fluid" alt="Asthma Symptoms"title="Asthma Symptoms">
        </div>
        <div class="cvn sym" >
            <h3>Symptoms of <span>Asthma</span></h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Shortness of Breath</p>
            <p>One faces problem in breathing as normal breathing process hinders.</p>
            <br>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Pain And Tightness in Chest</p>
            <p>Tightness and pain in chest takes place in body one being affected by asthma</p>
            <br>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Whistling Sound While Breathing</p>
            <p>Some whistling sounds automatically occurs when one is prone to asthma</p>
            <br>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Chest Pain</p>
            <p>A patient also feels pain in chest if experience severe asthma attack</p>
            <br>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Thick Mucus Sputum</p>
            <p>In asthma consistency of sputum (saliva) gets thick</p>
        </div>
    </div>
</div>
<div class="split"></div>
<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title"style="font-size: 24px !important;">Advantages of <b>Ayurvedic Treatment For Asthma</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Effective Ayurvedic Medicines helps to get rid of Inhaler.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurvedic treatment helps in recovering with the help Breathing Exercises.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurvedic treatment promotes Dietary and Lifestyle changes.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurvedic Medicines has no adverse side effects on health.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurveda reduces future Health risks.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="_-4_fagZucU" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/asthma-thumbnail.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Patient <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Asthma Testimonial" title="Aman Kaur Testimonial">
                            <h3 class="usrname">Aman Kaur</h3>
                            <p class="desig">Asthma Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                My grandmother was suffering from Asthma for past 10 years and was dependent on inhaler.  We went to Dr. Sharda Ayurveda for treatment of Asthma. Dr. Mukesh Sharda guided us the best and she immediately started with <a class="green-anchor"href="https://www.drshardaayurveda.com/asthma">Ayurvedic treatment of Asthma</a>. 
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Asthma Testimonial" title="Dilpreet Singh Testimonial" >
                            <h3 class="usrname">Dilpreet Singh</h3>
                            <p class="desig">Asthma Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I used to face breathing issues, sometimes it was quite 
                                difficult for me to breath at night in winters. After Ayurvedic treatment of asthma, I am highly satisfied with the results and I am recovering from my breathing issues.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Asthma Testimonial" title="Ahmad Khan Testimonial">
                            <h3 class="usrname">Ahmad Khan</h3>
                            <p class="desig">Asthma Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                My 17 years old son was totally dependent on Inhaler as he had chronic Asthma. We started with Ayurvedic medicines from Dr. Sharda Ayurveda. I cannot thank Dr. Mukesh Sharda enough for treating my son’s problem and making him healthy. He started breathing healthy without any use of Inhaler.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="dsa-why-choose" id="testimonials">
    <div class="container-fluid">
        <h2 text align="center"style="font-size: 32px !important;">Kind Words of Asthma Patients -<strong>Dr. Sharda Ayurveda</strong></h2>
        <br>    
        
        <div class="row cut-row" >
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div id="dna_video">
                    <div class="youtube-player" data-id="pujSQbxCBK0" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                        <img src="{{ URL::asset('front/images/pujSQbxCBK0.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                        <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div id="dna_video">
                    <div class="youtube-player" data-id="BodQAXFJA5o" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                        <img src="{{ URL::asset('front/images/BodQAXFJA5o.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                        <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                Can one opt for Ayurvedic treatment for asthma? 
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            Ayurveda is the oldest form of medicine that began in India. It uses all the healthy nutritious herbs to formulate medicines. Ayurveda transforms an individual completely. Ayurveda cures a patient naturally with least side-effects. It focuses on restoration of airway passage which is the major cause of Asthma. Choose <b>best Ayurvedic doctor in India</b> Dr. Mukesh Sharda and get relieve from Asthma.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Can Ayurveda permanentely cure asthma ?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            Ayurveda works on all the natural methods. By adopting healthy lifestyle, change in diet and exercising regularly one can definitely see change in their life. Also with it one needs to consult an Ayurvedic doctor and follow all his medications to treat Asthma and get rid of it.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Which all fruits cannot be taken in asthma? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            Banana, Citrus fruits like Oranges and  Maraschino Cherries should be avoided. Always avoid intake of all sour fruits. Rest of the fruits always consume at room temperature.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                How to remove mucus from lungs at home? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                            <b>Honey tea-</b> Honey tea can be made by mixing about 2 teaspoons of honey with warm water or tea.<br> <b>Saltwater gargles-</b> Saltwater helps in reducing mucus and phlegm from your throat. <br><b>Steam-</b> Steam helps in clearing mucus or phlegm out of your lungs. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFive" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <i class="fa" aria-hidden="true"></i>
                                Can Asthma damage lungs? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="card-body">
                            Asthma can cause permanent damage to your lungs if not treated early and in right way. Ayurvedic treatment for Asthma can save you timely and early. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                What causes Asthma?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                        <div class="card-body">
                            There are several reasons that causes asthma. It is usually caused by a mixture of hereditary factor i.e.- inherit from birth and some environmental factors. Certain allergants from house, dust, mites and pets are the common causes. Other allergens, such as pollen can also cause Asthma. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                How long does Asthma attack last? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                        <div class="card-body">
                            An asthma attack can last for several continuous hours if not treated timely. Acute asthma attack last for few minutes but a chronic attack takes several hours and a proper medication is required. Medical treatment is mandatory is case of shortness of breath.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                Does Asthma gets worsen by age?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                        <div class="card-body">
                            Yes, poorly treated Asthma obviously gets worsen by age. The lungs start to function less in comparison to non- asthmatic persons. Modern medications have temporary treatment for Asthma. They provide treatment to relieve the symptoms by giving inhaler. But in Ayurveda treatment for Asthma has sure results and can be treated at any stage of life. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Is Asthma a life-long disease?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                        <div class="card-body">
                            Asthma can become a life-long disease if it is not treated in a healthy way. By Ayurvedic treatment it can be vanished. Course of Ayurvedic treatment followed by dietary changes and exercises one can easily recover from Asthma without any side effects on health. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Do Asthma triggers more in cold weather?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                        <div class="card-body">
                            Yes, to some asthma triggers more in cold weather conditions due to seasonal changes. It is mandatory to carry your medicines with you specially in changing weather and environmental conditions. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Asthma",
    "item": "https://www.drshardaayurveda.com/asthma"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "Can one opt for Ayurvedic treatment for asthma?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurveda is an ancient form of medicine that began in India. It uses all the healthy nutritious herbs to formulate medicines. Ayurveda transforms an Individual completely. Ayurveda cures a patient naturally with least side-effects. It focuses on restoration of airway passage which is the major cause of Asthma. Choose best Ayurvedic Doctor in India Dr. Mukesh Sharda and get relieve from Asthma."
    }
  },{
    "@type": "Question",
    "name": "Can Ayurveda permanently cure asthma?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurveda works on all the natural methods. By adopting healthy lifestyle, change in diet and exercising regularly one can definitely see change in their life. Also with it one needs to consult an Ayurvedic doctor and follow all his medications to treat Asthma and get rid of it."
    }
  },{
    "@type": "Question",
    "name": "Which all fruits cannot be taken in asthma?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Banana, citrus fruits like oranges and  maraschino cherries should be avoided. Always avoid intake of all sour fruits. Rest of the Fruits always consume at room temperature."
    }
  },{
    "@type": "Question",
    "name": "How to remove mucus from lungs at home?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "1. Honey tea- You can make honey tea by mixing about 2 teaspoons of honey with warm water or tea. 2. Saltwater gargles- Saltwater helps reduce mucus and phlegm in your throat. 3. Steam- Steam may help clear mucus or phlegm out of your lungs."
    }
  },{
    "@type": "Question",
    "name": "Can Asthma damage lungs?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Asthma can cause permanent damage to your lungs if not treated early and in right way. Best Ayurvedic treatment for Asthma can save you timely and early."
    }
  }]
}
</script>
<style>
    .carousel-inner {position: relative;width: 100%;overflow: hidden;}
    .carousel-item {position: relative;float: left;width: 100%;margin-right: -100%;-webkit-backface-visibility: hidden;backface-visibility: hidden;transition: -webkit-transform .6s ease-in-out;transition: transform .6s ease-in-out;transition: transform .6s ease-in-out,-webkit-transform .6s ease-in-out;}
    .card-body {padding-top: 1.5rem;padding-bottom: 1.5rem;-webkit-border-radius: 0!important;border-radius: 0!important;flex: 1 1 auto;-ms-flex: 1 1 auto;}
    .card .card-body a h3{font-size: 18px;color: #ff9000;line-height: 1.4;}
    .card .card-body h3{color: #373433;font-size: 18px;    line-height: 1.4;}
    .font-weight-bold {font-weight: 700!important;}
    .card-title {margin-bottom: .75rem;}
    .card-img-top {width: 100%;border-top-left-radius: calc(.25rem - 1px);border-top-right-radius: calc(.25rem - 1px);}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            responsiveClass: !0,
            navigation : true,
            autoplay: true,
            loop: true,
            smartSpeed: 2500,
            singleItem : true,
            navigationText: ["<i class='fa fa-angle-left fa-2'></i>", "<i class='fa fa-angle-right fa-2'></i>"],
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                480: {
                    items: 1,
                    nav: !1
                },
                768: {
                    items: 2,
                    nav: !1
                },
                1000: {
                    items: 2,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1024: {
                    items: 3,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1366: {
                    items: 3,
                    nav: !1
                }
            }
        })
    });

</script>
@stop