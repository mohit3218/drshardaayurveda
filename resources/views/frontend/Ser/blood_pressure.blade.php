<?php
/**
 * Blood Pressure Page 
 * 
 * @created    03/02/2022
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2022
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4">Blood Pressure</h3>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/blood-pressure">Blood Pressure</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a  href="#types">TYPES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatments">TREATMENTS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc">
                    <img src="{{ URL::asset('front/images/blood-pressure.webp') }}" class="why-choose-us img-fluid" alt="Blood Pressure">
                </div>
            </div>
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " >
                <h1 class="heading1">Ayurveda for <b>Blood Pressure</b></h1>
                <p>
                As per Ayurveda blood pressure is known as <strong style="font-weight: bold;">“Vyanabala Vaishamya”</strong>. Blood pressure has nowadays emerged as a common and serious issue that is the result of long term and short-term force of blood against the arterial wall. The pressure on the wall is high and constant which eventually cause interference in the heart functioning, leading to heart diseases. In other words, stated as the condition when the blood pressure is greatly and chronically elevated. The blood pressure of an individual is determined by two factors i.e., <strong style="font-weight: bold;">the amount of blood that the heart pumps and secondly resistance to blood flow in the arteries</strong>. Maintaining normal systolic and diastolic is essential for sustainable and disease-free living. If a mere fluctuation happens during pumping of the blood flow around the circulatory system, thus making no oxygen or nutrients to be passed or delivered through arteries to body tissues and organs thus conferred as the vital process of the body.
                <span id="dots">...</span>
                </p>
                <span id="more-content">
                <p>
                Therefore early detection and later its effective management are important for reducing the risk of future complications which significantly affects the health. Blood pressure according to Ayurveda occurs as a result of an imbalance of <strong style="font-weight: bold;">Pitta Dosha</strong>, which is one of the three energies responsible for disease to occur and flare-up with passing time. Ayurvedic herbs to control your blood pressure are known for ages and always majority of times given positive and effective results. Dr. Sharda Ayurveda for treating the disease through its root cause by the implementation of Ayurvedic medicine for high blood pressure and low blood pressure provides long-term results naturally. 

                </p>
                <div class="btn-banner crd">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                        </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec" id="types"> 
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="heading1">Types of <b>Blood Pressure</b></h2>
                <!-- <p class="text-center">There are two major types of Blood Pressure</p> -->
            </div>
        </div>
        <div class="row cust-iba">
            <div class="owl-carousel owl-theme" >
                <div class="item col-md-12 text-center aos-init aos-animate" >
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/hypertension.webp') }}" class="card-img-top lazyload" alt="Hypertension">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Hypertension</h4>
                            <p class="card-text">
                                It is characterized as when the individual's systolic blood pressure is 140mm Hg or higher and the diastolic blood pressure remains at 90mm Hg or higher. High blood pressure is fatal and dangerous as it makes the heart work harder to pump up blood to the body which significantly contributes to cause hardening of the arteries. If the pumping of the blood is high it results in narrowing of the arteries.
                            </p>
                            <h5 class="text-left">Symptoms</h5>
                            <ul class="text-left" style="padding:10px;">
                                <li>Headache</li>
                                <li>Chest pain</li>
                                <li>Difficulty breathing</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" >
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/hypotension.webp') }}" class="card-img-top lazyload" alt="Hypotension">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Hypotension</h4>
                            <p class="card-text">
                                It states that when the individual's systolic blood pressure is 120mm Hg or less and the diastolic blood pressure remains at 70mm Hg or less. Early treatment is necessary to avoid unwanted life-threatening complications. The condition arises when the heart is not pumped properly and there is no constriction of blood vessels or arteries leading to low blood pressure.
                            </p>
                            <h5 class="text-left">Symptoms</h5>
                            <ul class="text-left" style="padding:10px;">
                                <li>Fainting</li>
                                <li>Nausea</li>
                                <li>Dizziness</li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate">
                    <h2 class="heading1">Causes of <b>Blood Pressure</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="causes">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    High Cholesterol
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                High cholesterol confers blood coagulation. Cholesterol is a type of fatty acid used by the body to produce hormones. The excessive production of cholesterol in the body means sticking of oil to arterial wall.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Genetics
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                Many studies proved that issue of blood pressure arises as an outcome of heredity.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion">
                        <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Mental Health
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                               When under stress body generates hormones in response. These hormones are associated with a temporary increase in blood pressure causing the heart to beat faster and narrowing blood vessels.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Dehydration
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">
                                When the body is dehydrated the blood volume decreases causing blood pressure to low. As a result, the organs do not receive enough oxygen and nutrients for smooth functioning.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Lack of Nutritious Diet
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                            <div class="card-body">
                               Minimal intake of fresh fruits, vegetables, whole grains, and low-fat dairy products are associated with increasing the chances of circulatory diseases.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>			


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/blood-pressure-symptoms.webp') }}" class="img-fluid" alt="Symptoms of Blood Pressure">
        </div>
        <div class="cvn sym" >
            <h3>Symptoms of <span>Blood Pressure</span></h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Headache</p>
            <p>Specifically, during hypertension pressure in the cranium builds as an outcome of increased blood pressure that suddenly spike up to a critical level causing severe headache. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Breathlessness</p>
            <p>Hypertension sometimes may lead to shortness of breath directly impacting heart and lung functioning. Shortness of breath is more noticeable with physical exertion or more exercise.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Dizziness</p>
            <p>A person suffering from blood pressure fluctuation frequently experiences dizziness.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Blurred Vision</p>
            <p>Lack of proper blood flow to the retina leads to blurred vision or sometimes complete loss of sight. People with high blood pressure are at greater risk for developing this condition.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Nausea</p>
            <p>People with blood pressure often experience nausea causing discomfort in the stomach and the sensation of wanting to vomit.</p>
        </div>
    </div>
</div>
<div class="split"></div>
<section class="dsa-why-choose" id="treatments">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " >
                <h2 class="client-test-title"style="font-size: 24px !important;">Ayurvedic Treatment For <b>Blood Pressure</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Panchakarma therapy is applied as part of Ayurvedic medication which involves different procedures to combat the disease efficiently. The therapies are <strong style="font-weight: bold;">Virechana and Basti karma</strong>.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Take a teaspoon of coriander and to it add a pinch of cardamom with thorough mixing with one cup freshly squeezed peach juice. Drink this solution twice or thrice a day to help deal with high blood pressure.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Practicing yoga and meditation daily can be beneficial in giving long-term results. Some of the best asanas for effective management are <strong style="font-weight: bold;">Uttanasana, Pashchimottanasana, Virasana, and Siddhasana</strong>.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurveda stands by the perspective that maintaining a healthy diet keeps diseases at bay. For this add all the nutrition to the diet essential to maintain a prolonged disease-free living.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Exercise regularly can specifically help avoid the occurrence of high blood pressure (hypertension). Some of the best exercises to tackle the disease are <strong style="font-weight: bold;">walking, jogging, and cycling</strong>.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="_eUfC0IR_9I" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/_eUfC0IR_9I.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Asthma Testimonial" >
                            <h3 class="usrname">Simran Kaur</h3>
                            <p class="desig">Blood Pressure Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Extreme fluctuation in blood pressure has affected my living to a greater extent. Even after consulting with the specialists, the issue reoccurs again which significantly affect vision and inability to concentrate on work. After recommendation from a friend, I approached Dr. Sharda Ayurveda for the <a class="green-anchor"href="https://www.drshardaayurveda.com/blood-pressure"><strong style="font-weight: bold;">blood pressure Ayurvedic treatment</strong></a>. It was amazing that within just 2 months medications with special attention to diet and lifestyle helped me recover well from the issue. I am thankful to Dr. Sharda Ayurveda for their best and most effective natural treatment. 
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               
                                <i class="fa" aria-hidden="true"></i>
                                What are all the factors responsible for increased blood pressure?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            The factors responsible for raised blood pressure are stress, having more caffeinated products, which may be due to pre-existing medical health such as kidney disease, and diabetes.
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Which treatment is effective to treat blood pressure?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            Ayurvedic treatment is known to be the best and most effective to manage blood pressure. Ayurveda incorporates natural elements to treat the disease thus giving no side effects, long-term results, and minimal chances of disease symptoms to reoccur.
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Which is the best Ayurvedic clinic in India to approach for the Hypertension treatment?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            There are various recognized Ayurvedic experts all over India. But it is seen that Dr. Sharda Ayurveda is recommended best to manage the blood pressure authentically and naturally. They are recognized for an enormous number of thousands of recovered patients and specialized treatment which is more based on diet and lifestyle changes along with medications.
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Can lack of sleep cause high blood pressure?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                            Studies show that a disturbed sleeping schedule is the root cause of many health issues one of which is blood pressure. Over time, it can lead to swings in hormones which contribute to high blood pressure and other health risks mainly associated with the heart.
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingFive" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <i class="fa" aria-hidden="true"></i>
                               Can mental health instability can be a major cause of circulatory disease?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="card-body">
                            Disturbed mental health is known to be the root cause of many bodily system issues. An increase and decrease in blood pressure is associated with stress. Therefore, to control the levels practice regular yoga and meditation which aids in giving beneficial results.
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                What home remedies can be adapted to control blood pressure?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                        <div class="card-body">
                            Ayurveda suggests one of the best blood pressure natural remedy which can help in the effective management.
                            <li>Cucumber + curd acts as an excellent diuretic that helps keep blood pressure and digestive health in check. </li>
                            <li>Reduce the level of sodium intake in your diet specifically if facing high blood pressure.</li>
                            <li>Have almond milk daily (for low blood pressure) for this take soaked almonds and mix it well with warm milk for better results.</li>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                What health complications are associated with high and low blood pressure?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                        <div class="card-body">
                           The issues related to high blood pressure are:
                           <li>Kidney and heart diseases</li>
                           The concerns regarding low blood pressure are:
                           <li>Diabetes and Parkinson’s disease</li>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Can keeping the body hydrated helps lower blood pressure?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                        <div class="card-body">
                           Majorly if a person has the concern of high blood pressure they need to keep in check diet, and lifestyle and additionally as an important aspect hydrating or drinking plenty of water each day for maintain healthy blood pressure. 
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                Do bananas help lower blood pressure?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                        <div class="card-body">
                            Yes, it does by having banana each day helps keep high blood pressure at bay. As it is rich in potassium content (which is known as a blood pressure-lowering mineral).
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Which fruits are good for high blood pressure?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                        <div class="card-body">
                            The best fruits recommended for lowering blood pressure include citrus fruits, berries, bananas, pomegranates, prunes, and melons.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<style>
    .carousel-inner {position: relative;width: 100%;overflow: hidden;}
    .carousel-item {position: relative;float: left;width: 100%;margin-right: -100%;-webkit-backface-visibility: hidden;backface-visibility: hidden;transition: -webkit-transform .6s ease-in-out;transition: transform .6s ease-in-out;transition: transform .6s ease-in-out,-webkit-transform .6s ease-in-out;}
    .card-body {padding-top: 1.5rem;padding-bottom: 1.5rem;-webkit-border-radius: 0!important;border-radius: 0!important;flex: 1 1 auto;-ms-flex: 1 1 auto;}
    .card .card-body a h3{font-size: 18px;color: #ff9000;line-height: 1.4;}
    .card .card-body h3{color: #373433;font-size: 18px;    line-height: 1.4;}
    .font-weight-bold {font-weight: 700!important;}
    .card-title {margin-bottom: .75rem;}
    .card-img-top {width: 100%;border-top-left-radius: calc(.25rem - 1px);border-top-right-radius: calc(.25rem - 1px);}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            responsiveClass: !0,
            navigation : true,
            autoplay: true,
            loop: true,
            smartSpeed: 2500,
            singleItem : true,
            navigationText: ["<i class='fa fa-angle-left fa-2'></i>", "<i class='fa fa-angle-right fa-2'></i>"],
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                480: {
                    items: 1,
                    nav: !1
                },
                768: {
                    items: 2,
                    nav: !1
                },
                1000: {
                    items: 2,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1024: {
                    items: 2,
                    nav: !1,
                    loop: 0,
                    margin: 20
                },
                1366: {
                    items: 2,
                    nav: !1
                }
            }
        })
    });

</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Blood Pressure",
    "item": "https://www.drshardaayurveda.com/blood-pressure"  
  }]
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What are all the factors responsible for increased blood pressure?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The factors responsible for raised blood pressure are stress, having more caffeinated products, which may be due to pre-existing medical health such as kidney disease, and diabetes."
    }
  },{
    "@type": "Question",
    "name": "Which treatment is effective to treat blood pressure?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurvedic treatment is known to be the best and most effective to manage blood pressure. Ayurveda incorporates natural elements to treat the disease thus giving no side effects, long-term results, and minimal chances of disease symptoms to reoccur."
    }
  },{
    "@type": "Question",
    "name": "Which is the best Ayurvedic clinic in India to approach for the treatment?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "There are various recognized Ayurvedic experts all over India. But it is seen that Dr. Sharda Ayurveda is recommended best to manage the blood pressure authentically and naturally. They are recognized for an enormous number of thousands of recovered patients and specialized treatment which is more based on diet and lifestyle changes along with medications."
    }
  },{
    "@type": "Question",
    "name": "Can lack of sleep cause high blood pressure?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Studies show that a disturbed sleeping schedule is the root cause of many health issues one of which is blood pressure. Over time, it can lead to swings in hormones which contribute to high blood pressure and other health risks mainly associated with the heart."
    }
  },{
    "@type": "Question",
    "name": "Can mental health instability can be a major cause of circulatory disease?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Disturbed mental health is known to be the root cause of many bodily system issues. An increase and decrease in blood pressure is associated with stress. Therefore, to control the levels practice regular yoga and meditation which aids in giving beneficial results."
    }
  },{
    "@type": "Question",
    "name": "What home remedies can be adapted to control blood pressure?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurveda suggests one of the best blood pressure natural remedy which can help in the effective management.
•   Cucumber + curd acts as an excellent diuretic that helps keep blood pressure and digestive health in check. 
•   Reduce the level of sodium intake in your diet specifically if facing high blood pressure. 
•   Have almond milk daily (for low blood pressure) for this take soaked almonds and mix it well with warm milk for better results."
    }
  },{
    "@type": "Question",
    "name": "What health complications are associated with high and low blood pressure?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The issues related to high blood pressure are:
Kidney and heart diseases
The concerns regarding low blood pressure are:
Diabetes and Parkinson’s disease"
    }
  },{
    "@type": "Question",
    "name": "Can keeping the body hydrated helps lower blood pressure?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Majorly if a person has the concern of high blood pressure they need to keep in check diet, and lifestyle and additionally as an important aspect hydrating or drinking plenty of water each day for maintaining healthy blood pressure."
    }
  },{
    "@type": "Question",
    "name": "Do bananas help lower blood pressure?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it does by having banana each day helps keep high blood pressure at bay. As it is rich in potassium content (which is known as a blood pressure-lowering mineral)."
    }
  },{
    "@type": "Question",
    "name": "Which fruits are good for high blood pressure?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The best fruits recommended for lowering blood pressure include citrus fruits, berries, bananas, pomegranates, prunes, and melons."
    }
  }]
}
</script>

@stop