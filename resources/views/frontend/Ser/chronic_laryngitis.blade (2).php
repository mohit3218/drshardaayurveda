<?php
/**
 * Chronic Laryngitis Page 
 * 
 * @created    04/03/2022
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2022
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4">Chronic Laryngitis</h3>
                        <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/chronic-laryngitis">Chronic Laryngitis</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatments">TREATMENTS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="definition">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc">
                    <img src="{{ URL::asset('front/images/chronic-laryngitis.webp') }}" class="why-choose-us img-fluid" alt="Chronic Laryngitis"title="Chronic Laryngitis">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <h1 class="heading1">Ayurvedic Treatment For <b>Chronic Laryngitis</b></h1>
                <p>Before knowing the Ayurvedic treatment for chronic laryngitis, the basics must be understood.
                In Ayurveda chronic laryngitis is described as <strong style="font-weight: bold;">“Swarabhed”</strong> where <strong style="font-weight: bold;">“Sawr” </strong>means voice and <strong style="font-weight: bold;">“Bhed”</strong> means breaking. It is defined as when the laryngeal mucous membrane becomes swollen, congested, and coated with thick and slimy mucus. Chronic conditions arise as the result of repeated episodes or attacks of acute laryngitis. The larynx in literal means <strong style="font-weight: bold;">“Voice Box”,</strong> the vocal cords present in the throat. An individual suffering from laryngitis experiences inflammation of the larynx, which directly affects the proper functioning of the larynx and severely changes the voice. Acute laryngitis lasts for a few days and is temporary but when the condition becomes severe it is categorized as chronic laryngitis which lasts for many weeks and needs serious medical attention. The larynx is also associated with protecting the airways channel and supporting the lungs. The condition if not treated will lead to long-lasting and more uncomfortable symptoms and become the sign of autoimmune disorders.
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">...</span>
                </p>
                <span id="more-content">
                    <p>Chronic laryngitis is respiratory tract problem that in Ayurveda is characterized as the imbalance of Vata Dosha and Pitta Dosha. In Vataj Swarabhed (Vata dosha) is characterized by the symptoms such as  vibrating voice, and torment around the larynx. But when it occurs due to an imbalance of Pitta Dosha the visible signs are thirst, fever, and vibrating sensations in the laryngeal area. Laryngitis treatment with Ayurveda will provide you with the best results. Ayurvedic treatment for chronic laryngitis from Dr. Sharda Ayurveda helps in effective recovery from the disease naturally and holistically. The <a class="green-anchor"href="https://www.drshardaayurveda.com/chronic-laryngitis">Treatment of Chronic Laryngitis</a> with Ayurvedic medicines along with the dietary and lifestyle changes make Dr. Sharda Ayurvedic treatment stand out from others.
                    </p>
                    </span>
                    <div class="btn-banner crd"><a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate">
                    <h2 class="heading1">Causes of <b>Chronic Laryngitis</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Infection
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Sometimes the disease emerges as a result of a weak immune system that is not able to counter-attack infectious agents such as viruses, and bacteria.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Acid Reflux 
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                The acidic pH of the body can majorly affect the vocal cord if not treated timely.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Medications 
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                                Some people are allergic to certain kind of medications which has the potential to significantly interfere with the voice and respiratory channels.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Chronic Sinusitis
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                                Laryngitis is a disease that may emerge as an outcome of pre-existing respiratory diseases that were left untreated.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Excessive Coughing 
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                                Extreme cold and cough which is accompanied by persistent coughing and sneezing is the major sign of laryngitis disease flare-up.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/chronic-laryngitis-symptoms.webp') }}" class="img-fluid"alt="Chronic Laryngitis symptoms"title="Chronic Laryngitis symptoms">
        </div>
        <div class="cvn sym" >
            <h3>Symptoms of <span>Chronic Laryngitis</span></h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Dry and Sore Throat</p>
            <p>The dry and sore throat becomes the ground for allergens to attack and develop infection causing disease emergence.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Losing Voice</p>
            <p>Inability to enunciate or a voice marked by frequent cracking and breaking. Several voice fluctuations during the day may be observed as an alarming sign.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Swelling of lymph nodes in your neck</p>
            <p>Swelling can cause sore and scratchy throat which can cause pain while swallowing and talking.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Irritated Throat</p>
            <p>It is marked by the sensation of a lump in your throat which causes aching and irritated feeling all day.</p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="treatments">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 ">
                <h2 class="client-test-title" style="font-size: 24px !important;">Ayurvedic Treatment for <b>Chronic Laryngitis</b></h2>
                <p class="im3"> 
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Application of essential oil example- Eucalyptus helps release thick mucus from nasal and soothes irritation caused due to coagulated nasal cavity. Add four drops to steam and it should not be ingested in any form.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Avoid having spicy foods as they can majorly cause the production of acid in the stomach which moves to the throat or esophagus. This can severely cause extreme <strong style="font-weight: bold;">itchiness, heartburn, and GERD (Gastro esophageal Reflux Disease)</strong>.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    A diet full of whole grains, fruits, and vegetables marks a person’s good health status. Specifically for treatment for chronic laryngitis, it is essentially recommended to have wholesome Vitamins such as A, E, and C which will keep the mucus membranes lining of the throat healthy. 
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    If experiencing sore and irritated throat more frequently the best treatment for chronic laryngitis as per Ayurveda which prevents discomfort is to gargle throat with warm saltwater every day in the morning and evening.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Yoga and meditation are known to strengthen the vocal cord and prevent hoarseness. Some of the best asanas recommended are salamba sarvangasana, matsyasana, and halasana.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="BMVD2SPPd0Q" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/BMVD2SPPd0Q.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Fungal Infection Testimonial"title="Sukhdev Singh Testimonial">
                            <h3 class="usrname">Sukhdev Singh</h3>
                            <p class="desig">Chronic Laryngitis Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                From the last 1 year on every alternative day, I was experiencing sore and irritated throat. Even after doing remedies at home and consulting many specialists, the results were always disheartening and dissatisfying. With each passing day, my symptoms and discomfort increased even after taking regular medications. Then, as per the recommendation from a friend of mine I consulted Dr. Sharda Ayurveda for the treatment. They did several examinations where I got to know that I was suffering from chronic or later stage laryngitis. But within just 3-4 months of the Ayurvedic medications along with certain dietary changes, I am now recovering well. I, wholeheartedly thank Dr. Sharda Ayurveda for their best Ayurvedic treatment of chronic laryngitis which helped me recover well.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Fungal Infection Testimonial" title="Partibha Testimonial">
                            <h3 class="usrname">Partibha</h3>
                            <p class="desig">Chronic Laryngitis Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                The last 5-6 months were miserable for me as I could not eat properly due to the swollen throat. After consulting, specialists I got to know that I was suffering from chronic laryngitis which is a severe condition of throat infection causing a person extreme discomfort. Even after medications, there was no sign of recovery. Thereby after quite a search, I got aware of Dr. Sharda Ayurveda effective treatment which became a blessing for me. I consulted them and shared my problem. I was amazed to see that with guidance from experts and medications within just 2 months I am now recovering well. Today also I am sticking to the diet and medications for another 1 month to get complete recovery. A mere thanks would not be enough for Dr. Sharda Ayurvedic treatment in comparison to what they have gifted me i.e., healthy and disease-free living.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                How long does it take to treat chronic laryngitis?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            The exact duration of the treatment depends upon the severity of the condition and the underlying cause. If a person takes the right medications and follows guidance from experts the recovery from the disease would be much better and more effective.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                What can you do for chronic laryngitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                        Adopt Ayurveda to heal the condition.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                How is Ayurvedic treatment for chronic laryngitis more effective than other medical treatments?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                        Ayurveda is applied for serving the mankind since long, which majority of time gives beneficial results. Dr. Sharda Ayurveda is known for its best Ayurvedic treatment of Chronic Laryngitis because it removes the root cause and provides a specialized treatment with minimal use of medications and gives more emphasis on diet and lifestyle for long-term recovery from the disease.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                What are the underlying cause of hoarseness?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                           The causes can be many but the main underlying cause that results in hoarseness are:
                            <ul>
                                <li>Gastroesophageal reflux (GERD)</li>
                                <li>Sinusitis</li>
                                <li>Nasal polyps</li>
                                <li>Neurological disorder</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion2">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    Can viral infection cause the emergence of laryngitis?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion2">
                            <div class="card-body">
                                Yes, viral infection may cause laryngitis.
                                Laryngitis is characterized by sore throat, hoarse voice, and extreme cough. It is seen that laryngitis specifically caused by influenza and adenovirus may sometimes be accompanied by fever, and headache. 
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Which home remedies are beneficial for recovering from chronic laryngitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion2">
                        <div class="card-body">
                        Some of the best home remedies effective for recovering from chronic laryngitis are:
                        <ul>
                            <li>Drinking warm water and adding honey to it helps relieve pain and inflammation. Honey is helpful as it acts as a natural cough suppressant.</li>
                            <li>Manage stomach acids. For this avoid having large meals and caffeine-based products.</li>
                            <li>Ginger tea helps reduce inflammation of the throat.</li>
                        </ul>
                        But these home remedies should be followed with guidance from experts.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Which vitamins are effective for treating chronic laryngitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion2">
                        <div class="card-body">
                            The vitamins helpful for treating the disease are:
                            <ul>
                                <li>Vitamin B12</li>
                                <li>Vitamin A</li>
                                <li>Vitamin D</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                How one can prevent the emergence of laryngitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion2">
                        <div class="card-body">
                            If the following measures are taken a person may reduce the risk to develop chronic laryngitis are:
                            <ul>
                                <li>Avoid screaming, or talking loudly, especially when you have a cold or sinus infection.</li>
                                <li>Maintain proper hygiene to prevent viral infections.</li>
                                <li>Maintain a healthy stomach.</li>
                                <li>Have a healthy and balanced diet and a stress-free mind for better immune system functioning. A strong immune system can be a strong defense against viral or bacterial infections.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                Can laryngitis be fatal?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion2">
                        <div class="card-body">
                        Laryngitis is a disease associated with an upper respiratory infection, typically due to viruses and bacteria. Several forms of laryngitis occur in children that if not treated timely can be dangerous and cause complete respiratory blockage.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Can laryngitis interfere with the breathing process?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion2">
                        <div class="card-body">
                            Yes it does affect breathing process.<br>The initial symptoms include hoarse voice and difficulty in speaking. But later it can extremely cause the larynx to swell. It is not common in adults but majorly affects children as they have smaller and narrower windpipes.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Chronic Laryngitis",
    "item": "https://www.drshardaayurveda.com/chronic-laryngitis"  
  }]
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "How long does it take to treat chronic laryngitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The exact duration of treatment depends upon the severity of the condition and the underlying cause. If a person takes the right medications and follows guidance from experts the recovery from the disease would be much better and more effective."
    }
  },{
    "@type": "Question",
    "name": "Which treatment is best for treating chronic laryngitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurvedic treatment is considered best and most effective for recovery from the disease. Ayurveda medicines are wholesome of natural elements which contain enormous properties which help heal the individual from inside. The treatment has no side effects accompanied by long-term results."
    }
  },{
    "@type": "Question",
    "name": "How is Ayurvedic treatment for chronic laryngitis more effective than other medical treatments?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurveda is applied for serving the mankind since long which majority of time gives beneficial results. Dr. Sharda Ayurveda is known for its best Ayurvedic treatment of Chronic Laryngitis because it provides root cause and specialized treatment with minimal use of medications and more emphasis on diet and lifestyle for long-term recovery from the disease."
    }
  },{
    "@type": "Question",
    "name": "What are the underlying cause of hoarseness?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The causes can be many but the main underlying cause that results in hoarseness are:
•   Gastroesophageal reflux (GERD)
•   Sinusitis 
•   Nasal polyps 
•   Neurological disorder"
    }
  },{
    "@type": "Question",
    "name": "Can viral infection cause the emergence of laryngitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, viral infection may cause laryngitis.
Laryngitis is characterized by sore throat, hoarse voice, and extreme cough. It is seen that laryngitis specifically caused by influenza and adenovirus may sometimes be accompanied by fever, and headache."
    }
  },{
    "@type": "Question",
    "name": "Which home remedies are beneficial for recovering from chronic laryngitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Some of the best home remedies effective for recovering from chronic laryngitis are:
•   Drinking warm water and adding honey to it helps relieve pain and inflammation. Honey is helpful as it acts as a natural cough suppressant. 
•   Manage stomach acids to this avoid having large meals and caffeine-based products. 
•   Ginger tea helps reduce inflammation of the throat.
But these home remedies should be followed with guidance from experts."
    }
  },{
    "@type": "Question",
    "name": "Which vitamins are effective for treating chronic laryngitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The vitamins helpful for treating the disease are:
•   Vitamin B12
•   Vitamin A
•   Vitamin D"
    }
  },{
    "@type": "Question",
    "name": "How one can prevent the emergence of laryngitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If the following measures are taken a person may reduce the risk to develop chronic laryngitis are: 
•   Avoid screaming, or talking loudly, especially when you have a cold or sinus infection.
•   Maintain proper hygiene to prevent viral infections.
•   Maintain a healthy stomach. 
•   Have a healthy and balanced diet and a stress-free mind for better immune system functioning. A strong immune system can be a strong defense against viral or bacterial infections."
    }
  },{
    "@type": "Question",
    "name": "Can laryngitis be fatal?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Laryngitis is a disease associated with an upper respiratory infection, typically due to viruses and bacteria. Several forms of laryngitis occur in children that if not treated timely can be dangerous and causes complete respiratory blockage."
    }
  },{
    "@type": "Question",
    "name": "Can laryngitis interfere with the breathing process?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes it does affect breathing process.
The initial symptoms include hoarse voice and difficulty in speaking. But later it can extremely cause the larynx to swell. It is not common in adults but majorly affects children as they have smaller and narrower windpipes."
    }
  }]
}
</script>

@stop