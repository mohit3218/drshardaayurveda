<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Anemia</h3>
                    <!--<p style="font-size: 24px;">Easy Bowel Movements with Ayurveda <br /> Not just symptomatic, Get Root cause treatment for Gout</p>-->
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/anemia">Anemia</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatments">TREATMENTS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="definition">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/Anemia.webp') }}" class="why-choose-us img-fluid" alt="Anemia Treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Anemia</b> Ayurvedic Treatment</h1>
                <p>
                    More than 30% of the population is affected by anemia worldwide. Anemia is a condition in which the color of the Skin becomes pale i.e.- yellowish in color or whitish. It is also characterized by a whitish tint on skin, eyes, and nails. It is termed as deficiency of blood in the body in which a person becomes pale. When the Pitta Dosha gets aggravated and the tissue undergoes inflammation they, therefore, become weak. Pallor is the condition in which paleness of the skin and mucous membrane comes with a result of diminished circulatory blood cells or diminished blood supply. In Ayurveda, it is termed <strong style="font-weight: bold;">Pandu Roga</strong>. In Sanskrit, Pandutya means whitish color. Anemia occurs when there are not enough healthy blood cells in the body to carry oxygen to all the body organs. When an individual develops anemia, they are called anemic. Anemia is not a big problem but if not treated on time it can prove dangerous for life. 
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                    <p>
                          Our body without blood cannot survive for long. It’s the biggest nutrition in our body. We work with all constituents required in our system. For life-long results choose Ayurveda for Anemia treatment. Dr. Sharda Ayurveda provides <strong style="font-weight: bold;">anemia treatment in India</strong> and the results are highly effective. 
                    </p>
                    <div class="btn-banner crd">
                        <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                        </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Anemia </b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Non- Nutritious Diet
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                If the diet is not nutritional and healthy, one is definitely on the verge of being anemic.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Excessive Intake of Alcohol
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                Due to excessive drinking of alcohol, a person can become anemic. 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Lower Iron Level in the Body
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                                The most common cause of anemia is the lower level of Iron in the body. A body requires a certain amount of Iron for the production of hemoglobin. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Blood Loss
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                                A person also becomes anemic due to blood loss in the body due to some injury, gastritis, heavy menstrual bleeding, and hemorrhoids.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Destruction of Red Blood Cells
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                                Destruction of red blood cell can be caused due to excessive intake of antibiotics and drugs. Hypertension also leads to the destruction of RBCs.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/Anemia-symptoms.webp') }}" class="img-fluid"alt="Anemia symptoms">
        </div>
        <div class="cvn sym" >
            <h2 class="heading1">Symptoms of <b>Anemia (Pandu Roga)</b></h2>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Fatigue and Weakness</p>
            <p>A person always feels restless, tired, and weak.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Pale</p>
            <p>A person will appear pale and dull due to a deficiency of blood in the body. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Coldish Body</p>
            <p>A person often complains of a cold body always. The body remains cold in all variations.  </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Unusual Cravings</p>
            <p>A person suffering from Anemia will always crave for eating Clay, Chalk, sand, and Ice. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Shortness of Breath</p>
            <p>A person gets tired easily performing an activity and suffers from shortness of Breath.</p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="treatments">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Ayurvedic Treatment for <b>Anemia </b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Cook the food of anemic patients in iron pots. This will help in retaining and balancing the hemoglobin level in the body.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    There are no artificial supplements given to raise the hemoglobin level. Each procedure is done according to Doshas and with proper management. <strong style="font-weight: bold;"><a class="green-anchor"href="https://www.drshardaayurveda.com/blogs/how-to-overcome-from-anemia-with-ayurveda">Anemia treatment in Ayurveda</a></strong> gives life-long results.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    By regulating Nutrition, one can easily recover up from Anemia.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    We treat the root cause of the disease and hence eradicate it from the beginning point.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    We try to manage all the symptoms like dyspnea, paleness with all herbal medicines which are harmless for the kidney and liver.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <br>
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="dg00tqccfNU" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/dg00tqccfNU.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">
                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Sakshi Testimonial" >
                            <h3 class="usrname">Sakshi</h3>
                            <p class="desig">Anemia Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I never knew lower levels of hemoglobin can cause so many problems in my body. At the age of 22, my hemoglobin declined to 7 and it caused me many health issues. I consulted Dr. Sharda Ayurveda for my treatment and within 20 days of treatment, my hemoglobin started raising with a healthy diet and Ayurvedic medicines.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Vanshika Testimonial" >
                            <h3 class="usrname">Vanshika</h3>
                            <p class="desig">Anemia Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                My hemoglobin level always used to be below the average. Despite taking so many medicines, it never raised and doctors advised me for taking the injections to raise blood. But after taking Ayurvedic medicines from <a class="green-anchor"href="https://www.drshardaayurveda.com/">Dr. Sharda Ayurveda</a>, I started feeling healthy and active, and slowly my hemoglobin levels were balanced.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Shivam Khatri Testimonial" >
                            <h3 class="usrname">Shivam Khatri</h3>
                            <p class="desig">Anemia Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                A big thanks to Dr. Sharda Ayurveda for helping me boosting my immunity as I used to be quite restless and sleepy the whole day long. After proper checkups and investigations, they told me about the lack of hemoglobin in my body. With their prescribed Ayurvedic medicines, I was finally active and healthy.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">              <i class="fa" aria-hidden="true"></i>
                                How can I know that I am anemic?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                            There are several symptoms like breathlessness and whitish or pallor skin. Also, there is a loss in appetite and fatigue. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Does Anemia affects the fertility of women?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                            Yes, it does affect because the menstrual cycle can only be maintained if a woman has good balanced hemoglobin and ferritin level in the Body. Choose Ayurvedic medicine for anemia and live a healthy life.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Is Anemia hereditary?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                            Yes, hereditary do cause anemia. Hereditary anemia sometimes is mild and can sometimes be severe too depending on the genetic disorder. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Which foods to avoid if you are anemic?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                            <ul>
                                <li>Tea and Coffee</li>
                                <li>Foods that contain Oxalic acid e.g.- Peanuts and Chocolates </li>
                                <li>Whole-grain portions of cereal</li>
                                <li>Foods rich in Gluten i.e. products made with Wheat, Barley, Rye, or Oats.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Which is the best food rich in Hemoglobin?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                            A high iron-rich diet that includes <b>green leafy vegetables, nuts, seeds, and breakfast cereals</b>. It is also important to include food in the diet that can improve the Body’s absorption of iron. Also, note to avoid such foods in the diet that interfere with the process.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    What is considered severe Anemia?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                                Hemoglobin level less than 7.0 g/dl is considered to be severe anemia that makes a person left with no energy in the body.
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Does lack Sleep causes Anemia?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                            Short sleep can sometimes lead to a downfall in hemoglobin levels whereas disturbed sleep increases the risk of anemia as the brain does not rest itself and proper functioning is hindered.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                What happens if Anemia is left untreated?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                            Iron- deficiency can cause serious health complications for the body to function smoothly. Untreated anemia also handers all the body parts functioning. The heart is at higher risk to function if there is no proper flow of oxygen in the body.
                            For best results consult Dr. Sharda Ayurveda, the best Ayurvedic clinic for Anemia treatment.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                What if the iron level is back to normal, then should one continue taking supplements?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                            If your hemoglobin is balanced then you should keep the medication on for another 3 months to build iron levels in the bone marrow. Also never stop taking anemia medicines without consulting your doctor. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                What are the signs to know whether my body is digesting iron or not?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                            If there is a sign of constipation or indigestion, this signifies that the body is unable to digest iron and also the body is unable to digest the medicines too.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Anemia",
    "item": "https://www.drshardaayurveda.com/anemia"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "How can I know that I am Anemic?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "There are several symptoms like breathlessness and whitish or pallor skin. Also, there is a loss in appetite and fatigue."
    }
  },{
    "@type": "Question",
    "name": "Does Anemia affects the fertility of women?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it does affect because the menstrual cycle can only be maintained if a woman has good balanced hemoglobin and ferritin level in the Body. Choose Ayurvedic Medicine for Anemia and live a healthy life."
    }
  },{
    "@type": "Question",
    "name": "Is Anemia hereditary?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, hereditary do cause Anemia. Hereditary Anemia sometimes is mild and can sometimes be severe too depending on the genetic disorder."
    }
  },{
    "@type": "Question",
    "name": "Which foods to avoid if you are anemic?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "•    Tea and Coffee
•   Foods that contain Oxalic acid e.g.- Peanuts and Chocolates 
•   Whole-grain portions of cereal. 
•   Foods are rich in Gluten i.e. products made with Wheat, Barley, Rye, or Oats."
    }
  },{
    "@type": "Question",
    "name": "Which is the best food rich in Hemoglobin?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "A high Iron-rich diet that includes Green leafy vegetables, Nuts, Seeds, and Breakfast Cereals. It is also important to include food in the diet that can improve the Body’s absorption of Iron. Also, note to avoid such foods in the diet that interfere with the process."
    }
  },{
    "@type": "Question",
    "name": "What is considered severe Anemia?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Hemoglobin level less than 7.0 g/dl is considered to be severe Anemia that makes a person left with no energy in the body."
    }
  },{
    "@type": "Question",
    "name": "Does lack Sleep causes Anemia?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Short sleep can sometimes lead to a downfall in hemoglobin levels whereas disturbed sleep increases the risk of Anemia as the brain does not rest itself and proper functioning is hindered."
    }
  },{
    "@type": "Question",
    "name": "What happens if Anemia is left untreated?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Iron- deficiency can cause serious health complications for the body to function smoothly. Untreated Anemia also handers all the body parts functioning. The Heart is at higher risk to function if there is no proper flow of oxygen in the body. For best results consult Dr. Sharda Ayurveda, the best Ayurvedic clinic for Anemia treatment."
    }
  },{
    "@type": "Question",
    "name": "What if the Iron level is back to normal, then should one continue taking supplements?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If your hemoglobin is balanced then you should keep the medication on for another 3 months to build iron levels in the bone marrow. Also never stop taking Anemia medicines without consulting your doctor."
    }
  },{
    "@type": "Question",
    "name": "What are the signs to know whether my body is digesting Iron or not?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If there is a sign of Constipation or Indigestion, this signifies that the body is unable to digest Iron and also the body is unable to digest the medicines too."
    }
  }]
}
</script>
@stop