<?php
/**
 * Piles Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Piles</h3>
                    <!--<p style="font-size: 24px;">Easy Bowel Movements with Ayurveda <br /> Not just symptomatic, Get Root cause treatment for Gout</p>-->
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/piles">Piles</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/piles-treatment.webp') }}" class="why-choose-us img-fluid" alt="Piles treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Piles</b> Ayurvedic Treatment</h1>
                <p>
                Piles also called hemorrhoids are the result of the distention and congestion of the internal and external venous plexuses around the anal canal. They are lumps or inflamed tissue inside or outside the anal canal. They contain blood vessels, supportive tissue, muscle, and elastic fibers. It is usually associated with constipation and straining at defecation. Patients with piles either complaints of bright red bleeding after defecation or prolapse of something through rectum which disappear after defecation or some complain that something comes out which has to be pushed back or there may be a tag of skin. Three out of four people suffer from piles but no worries choose the best Ayurvedic treatment for piles in India and get a solution to your problem. 
                <div class="btn-banner crd"><a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                        </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Piles</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Chronic Constipation
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                            Irregular bowel movement is the root cause of piles.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Eating Low Fiber Diet
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            Not taking a rich fiber diet causes a person to strain while bowel movements.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Pregnancy
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                                There is pressure on the enlarged uterus on the rectum and anus.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Sitting or Standing for Longer Period
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                                Sitting or standing for a longer period or in another position for long hours.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                Straining While Passing Stool
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                            Straining on the toilet also increases pressure within blood vessels.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/piles-symptoms.webp') }}" class="img-fluid"alt="Piles symptoms">
        </div>
        <div class="cvn sym" >
            <h3><span>Symptoms of</span> Hemorrhoids (Piles)</h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Burning And Severe Pain in Anal Region</p>
            <p>A hard, sometimes painful lump is felt around the anus.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Itching At Anal Area</p>
            <p>The area around the anus after defecation gets red, itchy, and sore. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Sensation of Incomplete Evacuation</p>
            <p>A person does not feel fresh and still feels bowel is full after a bowel movement. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Discharge Of Blood</p>
            <p>In chronic piles, red blood is seen after a bowel movement. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Pain </p>
            <p>A person feels unbearable pain while passing the stool. </p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Benefits of <b>Ayurvedic Treatment for Piles</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Piles in Ayurveda is called Arsha i.e. disease that tortures like an enemy. Mandagni ie the inability to digest food material is an important cause that causes Ama in the body.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    In this fleshy mass of variable size, shape, and color appear in the anus. External application, internal medicine, and modifications of lifestyle, and improvement in the diet provide relief to piles in Ayurveda.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Treatment adopted in Ayurveda to treat piles is-
                    <ul>
                        <li>Kshar Karma i.e. cauterization with alkalis or caustic</li>
                        <li>Agni Karma i.e. fire cauterization</li>
                        <li>Aushadha i.e. Ayurvedic medicines </li>
                        <li>Dietary changes </li>
                        <li>Essential Workout </li>
                    </ul>
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Seitz bath with Epsom salt is very beneficial in treating piles.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Apply Jatyadi oil or castor oil on the anus to get relief from pain from piles.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="2SZa45R8_FY" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/2SZa45R8_FY.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Piles Testimonial" >
                            <h3 class="usrname">Vijay Kumar</h3>
                            <p class="desig">Haemorrhoids Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was suffering from piles for 13 months. I tried many remedies to get rid of it but it was always just temporary relief. I consulted Dr. Sharda <strong>Ayurveda for piles treatment in India</strong>. They have quite effective and safe medicines to treat piles. Within 14 days of treatment, I got relief with their Ayurvedic medicines but continued it for 3 months to get completely rid of it.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Haemorrhoids Testimonial" >
                            <h3 class="usrname">Sagar</h3>
                            <p class="desig">Haemorrhoids Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Myself Sagar resident of model town Ludhiana visited Dr. Sharda Ayurveda for Ayurvedic hemorrhoid treatment. I was told to get surgery for it but I was escaping from it and was finding an alternative for it. So I consulted here and they treated me without getting any operation done. So anyone looking for a piles treatment without surgery can consult Dr. Sharda Ayurveda
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                            Can prolonged sitting and standing cause piles?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                        Yes, prolonged sitting on a hard mattress or chair or standing continuously for long hours can cause piles. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                I am suffering from chronic piles, would it be better for me to have surgery?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                        “NO,” Ayurveda has a very effective treatment for chronic piles. Before going for surgery you should consult an Ayurvedic physician or visit Dr. Sharda Ayurveda for piles treatment without any surgery.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Which food should be avoided to get relief from piles?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                            Deep-fried, processed food, high sugar, spicy food, and alcohol should be avoided in piles.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Is it safe to take Triphala daily to treat piles?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                        Yes, it is a good supplement that helps to manage blood glucose levels, intestinal infection, and constipation, it can be taken daily. It will not harm the body.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Do piles burst?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                        Yes, it can burst when it gets filled with blood inside it and further it bursts out and blood comes out during passing stool.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    What happens if piles remain untreated?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                            Untreated piles may cause bleeding. This bleeding may lead to anemia if these hemorrhoids remain for a long inactive stage and can also cause several health complications. 
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                How do piles look like?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                        Piles are described as uncomfortable, enlarged, painful mass around or protruded outward in anal is seen when checked. It is usually a red or discolored lump, painful in nature.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Is coffee, tea bad for piles?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                        Yes, tea and coffee are not good in piles as it makes stool dry, hence more pain and straining during defecation.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                At what age can piles occur?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                        Piles can occur at any age but mostly in age between 45-65. This problem is most common in women who are pregnant.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                How long do piles last?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                        Piles improve within 1 week to 12 days without surgery by Ayurvedic treatment. Whether it is internal hemorrhoid treatment it can be treated without surgery too. It may take 2-3weeks for a lump to completely go off.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Piles",
    "item": "https://www.drshardaayurveda.com/piles"  
  }]
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "Can prolonged sitting and standing cause piles?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, prolonged sitting on a hard mattress or chair or standing continuously for long hours can cause piles."
    }
  },{
    "@type": "Question",
    "name": "I am suffering from chronic piles, would it be better for me to have surgery?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "“NO,” Ayurveda has a very effective treatment for chronic piles. Before going for surgery you should consult an Ayurvedic physician or visit Dr. Sharda Ayurveda for piles treatment without any surgery."
    }
  },{
    "@type": "Question",
    "name": "Which food should be avoided to get relief from piles?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Deep-fried, processed food, high sugar, spicy food, and alcohol should be avoided in piles."
    }
  },{
    "@type": "Question",
    "name": "Is it safe to take Triphala daily to treat piles?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it is a good supplement that helps to manage blood glucose levels, intestinal infection, and constipation, it can be taken daily. It will not harm the body."
    }
  },{
    "@type": "Question",
    "name": "Do piles burst?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it can burst when it gets filled with blood inside it and further it bursts out and blood comes out during passing stool."
    }
  },{
    "@type": "Question",
    "name": "What happens if piles remain untreated?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Untreated piles may cause bleeding. This bleeding may lead to anemia if these hemorrhoids remain for a long inactive stage and can also cause several health complications."
    }
  },{
    "@type": "Question",
    "name": "How do piles look like?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Piles are described as uncomfortable, enlarged, painful mass around or protruded outward in anal is seen when checked. It is usually a red or discolored lump, painful in nature."
    }
  },{
    "@type": "Question",
    "name": "Is coffee, tea bad for piles?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, tea and coffee are not good in piles as it makes stool dry, hence more pain and straining during defecation."
    }
  },{
    "@type": "Question",
    "name": "At what age can piles occur?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Piles can occur at any age but mostly in age between 45-65. This problem is most common in women who are pregnant."
    }
  },{
    "@type": "Question",
    "name": "How long do piles last?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Piles improve within 1 week to 12 days without surgery by Ayurvedic treatment. Whether it is internal hemorrhoid treatment it can be treated without surgery too. It may take 2-3weeks for a lump to completely go off."
    }
  }]
}
</script>
@stop