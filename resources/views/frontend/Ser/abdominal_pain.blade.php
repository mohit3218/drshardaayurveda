<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Abdominal Pain</h3>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/abdominal-pain">Abdominal Pain</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/abdominal-pain.webp') }}" class="why-choose-us img-fluid" alt="stomach-pain treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Abdominal Pain</b> Ayurvedic Treatment</h1>
                <p>
                Abdominal Pain Ayurvedic Treatment involves a natural way to free the body from pain and discomfort. Abdominal pain is very common as almost every individual has experienced once this pain in their lifetime. It can be called a stomach ache too. It may not be a major issue but it gives birth to other serious problems. Pain or discomfort in the abdominal area i.e. from rib to pelvic. It may be mild or severe or may be continuous or it may come and go. It varies with every person. Sometimes abdominal pain is associated with fever, bloody stool, nausea, vomiting, or weight loss. Stomach pain Ayurvedic treatment provides life-long results no matter how acute or chronic the problem is. The abdomen consists of many organs i.e. stomach, small and large intestine, liver, gallbladder, pancreas, uterus, kidney, and urinary bladder.
                <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                <p>
                A viral or bacterial infection affects the stomach and intestines and may cause significant pain in the abdomen. Ayurvedic treatment for stomach aches is safe with no side effects on the body.
                </p>
                <div class="btn-ayurvedic">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Abdominal Pain</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Gastric Issue
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                            Abdominal pain mostly arises due to gas in the stomach, or indigestion.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Intestinal Disorders
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            Any infection, inflammation, or blockage in the intestine also leads to pain in the stomach.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Constipation
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                            Due to irregular bowel movement the stomach becomes heavy,  thus, leading to pain.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Kidney Stones
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                            The presence of stones in the kidney also causes abdominal pain which is quite severe.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                Food Poisoning
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                            A person goes through serious stomach pain in food poisoning due to indigestion of food.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz px-2.5" >
            <img src="{{ URL::asset('front/images/Abdominal-pain-symptoms.webp') }}" class="img-fluid"alt="abdominal pain symptoms">
        </div>
        <div class="cvn sym" >
            <h3><span>Symptoms of</span> Abdominal Pain </h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Generalized Pain</p>
            <p>Generalized pain i.e. pain felt in more than half of the abdominal area is seen particularly in indigestion and gas.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Localized Pain</p>
            <p>It is the pain felt in just one area of the abdomen or stomach, or the appendix.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Cramping Pain</p>
            <p>Pain which comes and goes or changes its severity or position particularly seen in gas, and indigestion during periods.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Colicky Pain</p>
            <p>Pain that comes and goes but tends to be severe suddenly, this is seen in kidneys or gall stones.
            </p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Benefits of Ayurvedic <b>Treatment for Abdominal Pain</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurveda can prove beneficial because it detoxifies the system from within and provides permanent irreversibility to the disease by modifying diet and lifestyle. 
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                     Cut down your daily intake of cold drinks and caffeine.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Eat less than your appetite demands.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Just focus on eating while having a meal. Do not overthink for any other reasons.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                     For stomach pain, Ayurvedic home remedies are very beneficial i.e. Decoction of Ajwain is prescribed to get relief from pain.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="G2SXEXmtMqY" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/G2SXEXmtMqY.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Patients <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Abdominal Pain Testimonial" >
                            <h3 class="usrname">Karnail  Singh</h3>
                            <p class="desig">Abdominal Pain Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I used to suffer from stomach pain after every 2-3 days. I got scanning of my stomach done but it was all fine. I consulted Dr. Sharda Ayurveda for it. After my investigations they guided me that pain in my stomach was due to gastritis issues. They started with Ayurvedic medicines for <a class="green-anchor" href="https://www.drshardaayurveda.com/acidity">acidity</a> as well as stomach pain. Within few days I got relieved from stomach pain and slowly I started recovering from acidity too.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Abdominal Pain Testimonial" >
                            <h3 class="usrname">Anmol Singh</h3>
                            <p class="desig">Abdominal Pain Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                A thank you won’t be enough to the doctors of Dr. Sharda Ayurveda.  They helped me a lot and now I am back to my normal daily routine. For the past five months, I was dealing with bloating and severe abdominal pain. No doctor could help me, but ever since I switched to Ayurveda I have had such a relief. Not only the medicines but their diet chart helped me immensely!
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                            When should I worry about abdominal pain? 
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                        If the pain in the stomach goes very sharp, severe, and sudden and is accompanied by pain in the chest, and neck, Further which is followed by vomiting, bloody diarrhea, or black stools.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                What does the pain in the abdomen indicate?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                        Causes of abdominal pain are several but the main causes are bacterial infection, abnormal tissue growth, inflammation, and blockage in the intestines. Throat infection and blood infection can cause bacteria to enter the digestive tract thus, leading to pain in the stomach.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                What are the 3 foods to be avoided in abdominal pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                        Carbonated beverages, spicy food, and garlic are three foods that you should not eat if suffering from abdominal pain. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                How can I lower abdominal pain naturally?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                        Place a hot bottle or heated bag on the abdomen or drink plenty of water. Eat fresh fruits, and drink juices and ajwain water. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Why do I only get lower abdominal pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                        It may be caused due to multiple reasons i.e. infections, abnormal growth, inflammation, and obstruction. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    What type of investigation is necessary for abdominal pain?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                            USG (ultrasonography) is the best test for abdominal pain. It will clear the exact cause and area of pain.
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                How long does abdominal pain last? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                        Normally abdominal pain subsides itself within 2-3 hours but if the reason is serious and related to another problem then it can be long-lasting. Ayurvedic medicine for stomach pain is safe and has sureshot results to get relief from pain.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                How is acute abdominal pain treated?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                        According to Ayurveda, diet is very helpful to heal the disease so for abdominal disorders Ayurveda advises eating less food, and fasting once a week. Vamana and Virechana are suggested to be free from abdominal disorders.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                Is abdominal pain life-threatening?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                        Yes, if the pain is severe then it is an indication of another abdominal problem in which the organ is inflamed or infected. Abdominal pain Ayurvedic treatment provides you relief from pain and discomfort. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                What causes abdominal pain daily?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                        Indigestion, gas, and some heavy load work that pulled muscles can cause abdominal pain in daily routine life.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Abdominal Pain",
    "item": "https://www.drshardaayurveda.com/abdominal-pain"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "When should I worry about abdominal pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If the pain in the stomach goes very sharp, severe, sudden and is accompanied with pain in the chest, neck followed by vomiting, bloody diarrhea, or black stools."
    }
  },{
    "@type": "Question",
    "name": "What does the pain in the abdomen indicate?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Causes of abdominal pain are several but the main causes are bacterial Infection, abnormal growths, inflammation, and blockage in the intestines. Throat infection, blood infection can cause bacteria to enter the digestive tract thus leading to pain in the stomach."
    }
  },{
    "@type": "Question",
    "name": "What are the 3 foods to be avoided in abdominal pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Carbonated beverages, spicy food, Garlic are 3 foods that you should not eat in abdominal pain."
    }
  },{
    "@type": "Question",
    "name": "How can I lower abdominal pain naturally?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Place a hot bottle or heated bag on the abdomen or drink plenty of water like Ajwain water, drink juices and eat fruits."
    }
  },{
    "@type": "Question",
    "name": "Why do I only get lower abdominal pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It may be caused due to multiple reasons i.e. infections, abnormal growth, inflammations, and obstruction."
    }
  },{
    "@type": "Question",
    "name": "What type of investigation is necessary for abdominal pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "USG (ultrasonography) is the best test for abdominal pain. It will clear the exact cause and area of pain."
    }
  },{
    "@type": "Question",
    "name": "How long does abdominal pain last?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Normally abdominal pain subsides itself within 2-3 hours but if the reason is seriously related to another problem then it can be long-lasting. Ayurvedic medicine for stomach pain is safe and has sure results to get relief from pain."
    }
  },{
    "@type": "Question",
    "name": "How is acute abdominal pain treated?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "According to Ayurveda diet is very helpful to cure disease so in abdominal disorders Ayurveda advises eating less food, fasting is advised once a week. Vamana and Virechana are advised to cure abdominal disorders."
    }
  },{
    "@type": "Question",
    "name": "Is abdominal pain life-threatening?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes if the pain is severe then it is an indication of another abdominal problem in which any organ is inflamed or infected. Abdominal pain Ayurvedic treatment does not provide you temporary relief from pain."
    }
  },{
    "@type": "Question",
    "name": "What causes abdominal pain daily?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Indigestion, gas, and some heavy work which pulled muscles can cause abdominal pain in daily routine life."
    }
  }]
}
</script>
@stop