<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Menopause</h3>
                    <p style="font-size: 24px;">Get the Safe & Secure Ayurvedic Treatment for Menopause</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/menopause">Menopause</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/menopause-treatment.webp') }}" class="why-choose-us img-fluid" alt="acidity treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Menopause</b> Ayurvedic Treatment</h1>
                <p>
                An end of the menstrual cycle is termed Menopause. In Ayurveda, the menopausal syndrome is known as Rajonivrutti which means the end of the menstruation cycle i.e. monthly period cycle comes to an end. The natural transition generally occurs at the age of 45-55 years which is a biological process. A woman goes through many physical and emotional changes during this period. When the ovaries stop producing the hormones i.e. there is no longer release of an egg each month and to produce the female sex hormones Oestrogen and progesterone. The fall in the level of these hormones in the bloodstream gives rise to the symptoms of menopause. It is the end of the fertility period. The importance of diet in menopause is highly advised for a healthy body.
                </p>
                <div class="btn-banner crd"><a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                        </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Menopause</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Natural Body Changes
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                            Declining of reproductive hormones and ovaries stop releasing an egg and periods get stopped.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Removal of Ovaries
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            Removal of ovaries i.e. due to oophorectomy or removal of the uterus due to DUB or hormonal disbalance. 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Cancer
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                            Cancer treatment like chemotherapy and radiation causes early menopause. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Aging
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                                Menopause is a part of aging and it mostly happens after the age of 40. But in some cases, women can experience menopause early.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                Surgery
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                            Early menopause can be due to surgery, like if the ovaries are removed.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz px-2.5" >
            <img src="{{ URL::asset('front/images/menopause-symptoms.webp') }}" class="img-fluid"alt="Menopause symptoms">
        </div>
        <div class="cvn sym" >
            <h3><span>Symptoms of</span> Menopause </h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Vaginal Dryness</p>
            <p>A woman experiences vaginal dryness i.e. during sexual intercourse.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Irregular Periods</p>
            <p>Due to a fall in the level of hormones, periods become irregular. <strong>Ayurvedic Medicine for Menopause</strong> can help you relieve </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Mood Swings</p>
            <p>A women go through severe mood swings i.e. one minute they are happy and the other minute they can be seen low.  </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Osteoporosis</p>
            <p>During and after menopause bones of the body get weak and require a healthy nutritious diet.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Thinning of Hair</p>
            <p>During menopause a woman goes through a phase of hair fall where hair gets thin and dryness of skin increases. </p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Benefits of <b>Ayurvedic Treatment for Menopause</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurveda views menopause not as a disease but as a transition period. Menopause is manageable in Ayurveda.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    The main goal of Ayurvedic treatment is to establish a balance of Vata and pitta dosha improve the digestive five and eradicate toxins from the body.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Milk is a rich source of calcium and vitamin D, it should be consumed twice a day.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Liquorice is a natural source of female hormones, should be given twice a day with milk. 
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Rich diet followed by exercises such as jogging, walking, swimming, and cycling is beneficial.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <iframe src="https://www.youtube.com/embed/PGKM-htmG9A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                            What is the right age for menopause?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                        Menopause can happen in your 40 or 50 age but the average age is 48-51.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                What is the most common cause of menopause?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                        The most common cause of menopause is a reduced level of hormone Oestrogen.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                What are the worst menopausal symptoms?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                        The worst symptoms are hot flushes, anxiety, weight gain, night sweat, and abnormal hair growth on the face. Menopause Ayurvedic treatment can bring you relief from all these symptoms.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Which vitamins are good in menopause?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                        Vitamin D and calcium should be taken after menopause regularly to avoid osteoporosis as bones tend to get weak.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Can periods restart after menopause?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                        No, it cannot restart after menopause. Women may face vaginal bleeding due to some pathology developed with aging in the uterus or ovaries. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    Can a woman get pregnant after menopause? 
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                            No, after menopause, no egg is formed hence there is no chance of getting pregnant.
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Can anyone speed up menopause? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                        Yes, it can speed up by external factors like stress, chemotherapy ovarian surgery, smoking, etc.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Why am I still having my periods at 54? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                        It is normal to complete menopause is seen between 50-54 years of age but symptoms start developing after the late 40s. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                How can I reverse menopause and get pregnant?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                        Ovarian rejuvenation procedure is prescribed as it helps to restore the reproductive hormones that are responsible for maturing and bursting of follicles. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                How do I know if I am going into menopause? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                        When these symptoms are seen like irregular periods, hot flashes, vaginal dryness, sleep disturbance, and mood swings started developing.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Menopause",
    "item": "https://www.drshardaayurveda.com/menopause"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What is the right age for menopause?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Menopause can happen in your 40 or 50 age but the average age is 48-51."
    }
  },{
    "@type": "Question",
    "name": "What is the most common cause of menopause?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The most common cause of menopause is a reduced level of hormone Oestrogen."
    }
  },{
    "@type": "Question",
    "name": "What are the worst menopausal symptoms?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The worst symptoms are hot flushes, anxiety, weight gain, night sweat, and abnormal hair growth on the face. Menopause Ayurvedic treatment can bring you relief from all these symptoms."
    }
  },{
    "@type": "Question",
    "name": "Which vitamins are good in menopause?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Vitamin D and calcium should be taken after menopause regularly to avoid osteoporosis as bones tend to get weak."
    }
  },{
    "@type": "Question",
    "name": "Can periods restart after menopause?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "No, it cannot restart after menopause. Women may face vaginal bleeding due to some pathology developed with aging in the uterus or ovaries."
    }
  },{
    "@type": "Question",
    "name": "Can a woman get pregnant after menopause?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "No, after menopause, no egg is formed hence there is no chance of getting pregnant."
    }
  },{
    "@type": "Question",
    "name": "Can anyone speed up menopause?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it can speed up by external factors like stress, chemotherapy ovarian surgery, smoking, etc."
    }
  },{
    "@type": "Question",
    "name": "Why am I still having my periods at 54?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It is normal to complete menopause is seen between 50-54 years of age but symptoms start developing after the late 40s."
    }
  },{
    "@type": "Question",
    "name": "How can I reverse menopause and get pregnant?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ovarian rejuvenation procedure is prescribed as it helps to restore the reproductive hormones that are responsible for maturing and bursting of follicles."
    }
  },{
    "@type": "Question",
    "name": "How do I know if I am going into menopause?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "When these symptoms are seen like irregular periods, hot flashes, vaginal dryness, sleep disturbance, and mood swings started developing."
    }
  }]
}
</script>
@stop