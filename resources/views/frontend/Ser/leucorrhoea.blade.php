<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Leucorrhoea</h3>
                    <p style="font-size: 24px;">Get the Safe & Secure Treatment for Leucorrhoea</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/leucorrhoea">Leucorrhoea</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/leucorrhoea-Ayurvedic-treatment.webp') }}" class="why-choose-us img-fluid" alt="Leucorrhoea treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Leucorrhoea</b> Ayurvedic Treatment</h1>
                <p>
                In Ayurveda leucorrhoea is known as Swetapradara where Sweta means white and pradara means discharge. Leucorrhoea is an excessive discharge of a white, sticky, thick or thin, or sometimes yellowish in color and foul-smelling material from the vagina before periods is called leucorrhoea. Thin, stretchy mucus is considered fertile as it happens around the time when an egg gets released and white thick discharge is considered infertile cervical mucus. This discharge keeps vagina tissue healthy and lubricated. Each woman must have experienced some vaginal discharge in her life span, it might be with some symptoms like feeling of weakness, pain in back and calf, loss of vital fluids, pruritis, etc. Leucorrhoea is of two types physiological and inflammatory. It occurs due to Kapha dosha. 
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                    <p>
                    The increased Kapha dosha impairs the rasa dhatu present in the vaginal tract thus leading to the discharge of white-colored fluid from the vagina. It occurs commonly among weak and anemic women. Just as the degree of sweating differs in various people, and the degree of virginal discharge also differs considerably in various women. <strong>Leucorrhoea treatment in Ayurveda</strong> helps you getting rid of all the bacterial Infections and other causes that give birth to it.
                    </p>
                    <div class="btn-banner crd"><a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                        </div>
                    </span>                   
                    <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
                </span>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Leucorrhoea</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Errors in Diet
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                            Wrong food combinations and excessive intake of sour and spicy food lead to this condition.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Hormonal Irregularities
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            Taking birth control pills changes hormone levels in the body which leads to leucorrhoea.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Sexually Transmitted Diseases
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                            Sexually transmitted diseases like gonorrhea, trichomoniasis infection sometimes cause foul smell discharge with itching, pain in the vagina. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Infection of Female Genital Organs
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                            Candidiasis i.e. yeast infection also causes more thick and white discharge from the vagina.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                Bacterial Vaginosis
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                            Vaginosis due to bacterial infection also causes a fishy smell and greyish white color discharge from the vagina. 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz px-2.5" >
            <img src="{{ URL::asset('front/images/leucorrhoea-symptoms.webp') }}" class="img-fluid"alt="Leucorrhoea symptoms">
        </div>
        <div class="cvn sym" >
            <h3><span>Symptoms of</span> Leucorrhoea </h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Stomach Cramps</p>
            <p>Stomach cramps or painful menstruation. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Itching and Burning Pain</p>
            <p>Pain, burning, and discomfort in and around the vagina.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Vaginal Discharge</p>
            <p>Cottage cheese or frothy textured discharge.  </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Weakness</p>
            <p>Weakness due to regular discharge from the vagina leads to weakness.  </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Abdominal Pain</p>
            <p>Lower abdominal pain or pain in calves and lumbar region.</p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Benefits of <b>Ayurvedic Treatment for Leucorrhoea</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurvedic management includes cleansing accumulated toxin and balance of Kapha dosha, tone up the muscle of reproductive organs with Ayurvedic medicines, and lastly cleansing of the vagina and adopting personal hygiene.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Treatment of sweat pradara is based on the use of drugs with kashaya rasa and Kapha shamak properties. Even Balyachikitsa to provide strength to female reproductive organs also proves good while treating leucorrhoea.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    A warm vaginal douche with Triphala water is recommended.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Rice water wash is advised to maintain hygiene. 
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Panchkarma is also beneficial for leucorrhoea like shirodhara, abhyanga, mardana,pindasweda and shiroabhyanga are done for the treatment.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <iframe src="https://www.youtube.com/embed/PGKM-htmG9A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                            How does one gets to know that it is a leucorrhoea?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                        You will feel severe pain in the lower back, weakness, white discharge, and foul smell from the vagina if u experience any of these symptoms consult an Ayurvedic expert and get <strong>Ayurvedic treatment for leucorrhoea</strong>. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Do leucorrhoea cause weakness?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                        When a patient has excessive white vaginal discharge from long time periods then that infection can cause severe weakness.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Which food items should one take to relieve leucorrhoea?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                        Red spinach, green gram, dates, cow ghee, Amla, and ash gourd should be taken to get relief from leucorrhoea. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Is daily discharge is normal? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                        Yes, some women have discharge every day while other experience it frequently, but if a white discharge is heavily followed with lower back pain and weakness then it is not considered normal. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Does leucorrhoea affects normal periods?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                        No, it does not affect your periods, it is normal to have leucorrhoea, only if it is heavy discharge, back pain, weakness, and foul smell, then it should be taken seriously.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    What do pregnancy discharge look like? 
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                            It is thin, clear, or milky white and does not have any smell.
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Can stress cause leucorrhoea? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                        Yes, stress increases the amount of vaginal discharge. If there is any foul smell from the vagina, it needs to intervene. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Can leucorrhoea leads to infertility? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                        If vaginal discharge is infectious, foul-smelling, it leads to severe complications like PID and infertility.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                Does PCOS cause discharge? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                        Yes, PCOS causes vaginal discharge, some women may experience less vaginal discharge.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                What is the difference between leucorrhoea and normal discharge? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                        Leucorrhoea is another name for normal discharge in medicine. It can be white or yellow, thin or thick, without any smell.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Leucorrhoea",
    "item": "https://www.drshardaayurveda.com/leucorrhoea"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "How does one gets to know that it is a leucorrhoea?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "You will feel severe pain in the lower back, weakness, white discharge, and foul smell from the vagina if u experience any of these symptoms consult an Ayurvedic expert and get Ayurvedic treatment for leucorrhoea."
    }
  },{
    "@type": "Question",
    "name": "Do leucorrhoea cause weakness?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "When a patient has excessive white vaginal discharge from long time periods then that infection can cause severe weakness."
    }
  },{
    "@type": "Question",
    "name": "Which food items should one take to relieve leucorrhoea?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Red spinach, green gram, dates, cow ghee, Amla, and ash gourd should be taken to get relief from leucorrhoea."
    }
  },{
    "@type": "Question",
    "name": "Is daily discharge is normal?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, some women have discharge every day while other experience it frequently, but if a white discharge is heavily followed with lower back pain and weakness then it is not considered normal."
    }
  },{
    "@type": "Question",
    "name": "Does leucorrhoea affects normal periods?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "No, it does not affect your periods, it is normal to have leucorrhoea, only if it is heavy discharge, back pain, weakness, and foul smell, then it should be taken seriously."
    }
  },{
    "@type": "Question",
    "name": "What do pregnancy discharge look like?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It is thin, clear, or milky white and does not have any smell."
    }
  },{
    "@type": "Question",
    "name": "Can stress cause leucorrhoea?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, stress increases the amount of vaginal discharge. If there is any foul smell from the vagina, it needs to intervene."
    }
  },{
    "@type": "Question",
    "name": "Can leucorrhoea leads to infertility?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If vaginal discharge is infectious, foul-smelling, it leads to severe complications like PID and infertility."
    }
  },{
    "@type": "Question",
    "name": "Does PCOS cause discharge?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, PCOS causes vaginal discharge, some women may experience less vaginal discharge."
    }
  },{
    "@type": "Question",
    "name": "What is the difference between leucorrhoea and normal discharge?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Leucorrhoea is another name for normal discharge in medicine. It can be white or yellow, thin or thick, without any smell."
    }
  }]
}
</script>
@stop