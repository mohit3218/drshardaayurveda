<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Acidity</h3>
                    <p style="font-size: 24px;">Get Acid Reflux Root Cause Solution with Ayurveda</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/acidity">Acidity</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/acidity.webp') }}" class="why-choose-us img-fluid" alt="acidity treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Acidity</b> Ayurvedic Treatment</h1>
                <p>
                    Millions of people today suffer from acidity and we often hear people saying I have acidity issues. Acidity occurs when the acid in the stomach gets pushed upwards towards esophagus. The acid secreted by stomach is essential in the digestive process. When there is excessive production of acid in the stomach it further results in the condition known as acidity. Acidity is also termed as acid reflux. It is a condition that is characterized by heartburn which is felt around the lower chest area. In Ayurveda it is categorized as a Pitta origin disease. The stomach secretes hydrochloric acid a digestive juice that breaks down food particles into their smallest to aid digestion. An excessive amount of hydrochloric acid in the stomach causes acidity. Acidity treatment in Ayurveda has sure lifetime results.
                </p>
               <div class="btn-banner crd">
                        <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                        </div>
                </span>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Acidity</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Unhealthy Eating Habits
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Skipping on meals, eating at odd timings, consumption of spicy food causes acidity.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Excessive Consumption of Certain Food Items
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                Consumption of more tea and coffee, junky, and extremely spicy food causes acidity.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Stomach Disorders
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                                Stomach disorders such as gastroesophageal reflux disease, tumors, and peptic ulcers.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Stress
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                                It is always seen a stressed person will always suffer from acidity.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Smoking and Alcohol Consumption
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                                A person who is addicted to smoking and alcohol will suffer from acidity.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="apn">
                    <a href="javascript void(0);" data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</a>
                        </div>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz px-2.5" >
            <img src="{{ URL::asset('front/images/acidity-symptoms.webp') }}" class="img-fluid"alt="acidity symptoms">
        </div>
        <div class="cvn sym" >
            <h3><span>Symptoms of</span> Acidity </h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Heartburn</p>
            <p>Discomfort or burning sensation may move from stomach to abdomen or chest, sometimes till throat too.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Burping</p>
            <p>A person frequently burps for no apparent reason. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Indigestion</p>
            <p>It gets hard for an Individual to digest meals and suffers from indigestion. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Weight Loss</p>
            <p>weight loss for no reason is also seen sometimes with one suffering from prolonged acidity. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Bloating</p>
            <p>Burning sensation and pain in the stomach is always seen in a person suffering from acidity. </p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Benefits of <b>Ayurvedic Treatment for Acidity</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurveda treats Acidity by not harming the internal lining of small and Large Intestines.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Eat smaller meals frequently than having them at once throughout the day and also modify the types of foods you are eating. Follow a low-carb diet.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Stop snacking before 2 hours you go to bed.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Avoid wearing very tight clothes and belts.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Regular exercising, shaking your body also helps relieve acidity.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="KVBvuKRdSO0" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/KVBvuKRdSO0.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Acidity Testimonial" >
                            <h3 class="usrname">Mrs Sunita</h3>
                            <p class="desig">Acidity Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Many people suffering from acidity refer to over medications thinking that it will not harm us. The same was with me.
                                Whenever I felt I used to pop in a tablet. Later I realized how harmful it became. From Dr. Sharda Ayurveda, I started my course of medication and after the treatment, I was finally relieved from acidity.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Acidity Testimonial" >
                            <h3 class="usrname">Harjeet</h3>
                            <p class="desig">Acidity Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I started experiencing symptoms like a burning sensation in my chest, trouble sleeping, and feeling like I lumped in my throat, I was unable to judge what was happening to me. I visited Dr. Sharda Ayurveda for treatment and after check-up Doctors told me that I was suffering from acidity. I started with their treatment and within few days I was feeling comfortable and slowly I recovered.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Acidity Testimonial" >
                            <h3 class="usrname">Ravinder</h3>
                            <p class="desig">Acidity Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I used to burp quite often and sometimes they were out of control. It was quite embarrassing for me. I started with a course of treatment from <strong>Dr. Sharda Ayurveda for acidity</strong>. They made some changes in my diet and prescribed me some Ayurvedic medicines. Within a short period, I recovered from acidity. 
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1"><b>FAQ'S</b></h2>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                How can I treat acidity naturally?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                            The best way to treat acidity at home is to have a shot of Gooseberry (Amla) juice daily and coconut water empty stomach. 
                            Eating healthy, no spicy food helps to treat acidity. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                How to avoid getting acidity?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                            Few lifestyle and dietary changes help to treat acidity.
                            <ul>
                                <li>Quit Smoking</li>
                                <li>Avoid resting for at least 2 hours after you have your meals</li>
                                <li>Eat frequent meals than having heavy at once. </li>
                                <li>Avoid tea and coffee. </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                What are the side- effects of medications for acidity?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                            Daily popping in a tablet for acidity is going to harm your body if not today then for sure in the future. Modern medicines cause constipation, diarrhea, nausea, and other diseases whereas Ayurvedic medicines have no side- effects on health. For best results choose Dr. Sharda Ayurveda best Ayurvedic clinic for acidity treatment</a>.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                What Dietary changes should I make for treating Acidity?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                            <ul>
                                <li>Don’t skip your meals. Eat something healthy after every 2 hours. </li>
                                <li>Food items that are hard to digest should be avoided at night. </li>
                                <li>Avoid consumption of alcohol.</li>
                                <li>Follow workout routine religiously</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                How to treat acidity immediately with a home remedy?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                            <ul>
                                <li>Try sipping apple cider vinegar.</li>
                                <li>A tsp. of baking soda mixed with water also helps in recovering from acidity attack.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    How is Ayurvedic medicine helpful in treating acidity?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                                Ayurvedic medicines work on treating an individual without any side effects on health. Ayurvedic medicine for acidity has life-long results as it does not only works on symptoms, it treats a disease from its root cause. With some dietary changes, one can easily recover from acidity.
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Often heard that Ginger is good in acidity. Is it?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                           Ginger helps in reducing the chances of stomach acid that flows up to the esophagus. Ginger also helps in reducing inflammation and helps to relieves symptoms of acid reflux.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                What happens if my body is too acidic?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                            Too much acidity in the body causes inflammation, heart disease, obesity, diabetes, and other chronic conditions. It can also leads to <a class="green-anchor" href="https://www.drshardaayurveda.com/ibs">IBS</a>.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                How should I sleep when I have acidity?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                            While suffering from acidity, don't sleep on your right side. Sleep on your left side always. It is the best position found to reduce acid reflux from the body.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                How long does acid reflux attack last?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                            Most people suffer from frequent bouts of heartburn, burning pain behind the breastbone that moves up towards the neck. This happens when one usually intake heavy meals. Acid attack sometimes lasts for as long as two hours.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Acidity",
    "item": "https://www.drshardaayurveda.com/acidity"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "How can I treat Acidity naturally?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The best way to treat Acidity at home is to have a shot of Gooseberry (Amla) juice daily and coconut water empty stomach. Eating healthy, no spicy food helps to treat Acidity."
    }
  },{
    "@type": "Question",
    "name": "How to avoid getting Acidity?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Few Lifestyle and Dietary changes help to treat Acidity. 
1. Quit Smoking
2. Avoid resting for at least 2 hours after you have your meals. 
3. Eat frequent meals than having heavy at once. 
4. Avoid tea and coffee."
    }
  },{
    "@type": "Question",
    "name": "What are the side- effects of medications for Acidity?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Daily popping in a tablet for acidity is going to harm your body if not today then for sure in the future. Modern Medicines cause Constipation, diarrhea, nausea, and other diseases whereas Ayurvedic medicines have no side- effects on health. For best results choose Dr. Sharda Ayurveda best Ayurvedic clinic for acidity treatment."
    }
  },{
    "@type": "Question",
    "name": "What Dietary changes should I make for treating Acidity?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "1. Don’t skip your meals. Eat something healthy after every 2 hours. 
2. Food items that are hard to digest should be avoided at night. 
3. Avoid consumption of Alcohol.
4. Follow workout routine religiously."
    }
  },{
    "@type": "Question",
    "name": "How to treat acidity immediately with a home remedy?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "1. Try sipping Apple cider Vinegar.
2. A tsp. of Baking soda mixed with water also helps in recovering from acidity attack."
    }
  },{
    "@type": "Question",
    "name": "How is Ayurvedic Medicine helpful in treating Acidity?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurvedic medicines work on treating an Individual without any side effects on health. Ayurvedic medicine for Acidity has life-long results as it does not only works on symptoms, it treats a disease from its root cause. With some Dietary changes, one can easily recover from Acidity."
    }
  },{
    "@type": "Question",
    "name": "Often heard that Ginger is good in Acidity. Is it?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ginger helps in reducing the chances of stomach acid that flows up to the esophagus. Ginger also helps in reducing inflammation and helps to relieve symptoms of acid reflux."
    }
  },{
    "@type": "Question",
    "name": "What happens if my body is too Acidic?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Too much acidity in the body causes inflammation, heart disease, obesity, diabetes, and other chronic conditions. It can also lead to IBS."
    }
  },{
    "@type": "Question",
    "name": "How should I sleep when I have acidity?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "While suffering from Acidity, don't sleep on your right side. Sleep on your left side always. It is the best position found to reduce acid reflux from the body."
    }
  },{
    "@type": "Question",
    "name": "How long does acid reflux attack last?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Most people suffer from frequent bouts of heartburn, burning pain behind the breastbone that moves up towards the neck. This happens when one usually intake heavy meals. Acid attack sometimes lasts for as long as two hours."
    }
  }]
}
</script>
@stop