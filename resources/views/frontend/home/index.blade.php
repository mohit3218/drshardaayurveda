<?php
/**
 * Home Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<div class="home-page">
    <section class="dsa-banner-sec">
        <div class="container-fluid">
            <div class="row cust-dsa-banner">
                <div class="col-md-6">
                    <div class="dsa-content">
                    
                        <h1 class="dsa-title">India's Best <br>Ayurvedic Clinic for <br>Chronic Diseases</h1>
                        <p>Every day only Ayurveda!</p>
                        <div class="btn-banner">
                            <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6" >
                    <div class="right-img">
                        <img src="{{ URL::asset('front/images/Ayurvedic-doctor-dr-mukesh-sharda.webp') }}" class="doctor img-fluid" alt="Ayurvedic Doctor Dr. Mukesh Sharda">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="dsa-why-choose">
        <div class="container-fluid">
            <div class="row cut-row">
                <div class="col-xl-7 col-lg-12 col-md-12 col-sm-12 col-12 mb-2">
                    <div class="left-img" >
                        <img src="{{ URL::asset('front/images/why-choose-Ayurveda.webp') }}" class="why-choose-us img-fluid" alt="Ayurvedic practices">
                    </div>
                </div>
                <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="adj-height">
                        <h2 class="" style="margin-top: 00px;">Why Choose <b>Ayurveda</b></h2>
                        <p >THE WOUND IS THE PLACE WHERE THE LIGHT ENTERS YOU!</p>
                        <p >
                            In this materialistic world, AYURVEDA is new LIGHT although it is the oldest healing of science. In Sanskrit, Ayurveda means 
                            “THE SCIENCE OF LIFE.” Ayurvedic knowledge was originated more than 5000 years ago in India, and is called as “MOTHER OF HEALINGS.”
                            Ayurveda works on three principles of energies i.e. VATA PITTA and KAPHA. Which are present in all human beings and works to 
                            control the various system of human anatomy and physiology.
                        </p>
                        <ul  style="display: inline-block; margin: 0px auto;">
                            <li>VATA for MOVEMENTS</li>
                            <li>PITTA for DIGESTION</li>
                            <li>KAPHA for NOURISHMENT</li>
                        </ul>
                        <p >
                            Ayurveda gives a holistic approach to treat and cure disease.
                        </p>
                        <span id="dots" ></span>
                        <span id="more-content">
                                In such circumstance, Ayurveda plays an important role in one’s life by:
                            <ul>
                                <li>HEALING</li>
                                <li>DETOXIFYING</li>
                                <li>REJUVENATING NEW CELLS</li>
                            </ul>
                            <p>
                                Natural Plant, Animal and Mineral origin drugs administered to our body is accepted more than synthetic 
                                medicines and with minimum adverse effects.
                            </p>
                            <p>Ayurveda is a system of life, based on the principles of health, knowledge and healing. The clinic is known for its high quality of Ayurvedic, delivered in a convenient way. If you're feeling unwell, Ayurveda is a natural solution for you. It helps support your body's normal functions through diet, exercise and lifestyle changes. Known for its holistic approach to healing, Ayurveda can help alleviate conditions and improve overall health.</p>
                        </span>
                        <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" >Read more</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="dsa-india-ayurvedic-sec"> 
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h2  style="line-height: 30px;">Diseases & <b>Ayurvedic Treatments</b></h2>
                </div>
            </div>
            <div class="row cust-iba">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" >
                    <div class="iba-box box1">
                        <div class="xd">
                            <div class="cont">	
                                <div class="wht-space" ></div>
                                <h3>Asthma</h3>
                                <p>
                                Asthma is categorized as “Shwas Rog” in Ayurveda. A condition in which a person faces regular breathing difficulty. By Ayurvedic treatment we treat a patient by only Ayurvedic medicines without any inhalers.</p>
                                <a href="https://www.drshardaayurveda.com/asthma" class="cont">Read more...</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" >
                    <div class="iba-box box2">
                        <div class="xd">
                            <div class="cont">
                                <div class="wht-space"></div>
                                <h3>Rheumatoid Arthritis</h3>
                                <p>Rheumatoid Arthritis is called as "AMAVATA" in Ayurveda. Ayurveda has the efficiency to treat chronic Rheumatoid Arthritis (RA) at any stage of life.</p>
                                <a href="https://www.drshardaayurveda.com/rheumatoid-arthritis" class="cont">Read more...</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 mt-2 col-sm-12 col-12" >
                    <div class="iba-box box3">
                        <div class="xd">
                            <div class="cont">
                                <div class="wht-space"></div>
                                <h3>Diabetes</h3>
                                <p>Diabetes is called “MADHUMEHA” in Ayurveda. Weak digestion of an individual causes production of sticky toxin and passes that is “AMA” that gets added in the pancreatic Cells and damages production of insulin.</p>
                                <a href="https://www.drshardaayurveda.com/diabetes" class="cont">Read more...</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="btn-ayurvedic">
                        <a href="https://www.drshardaayurveda.com/ayurvedic-treatments" class="btn-appointment">Find More Treatment</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pl-ayurvedic-treatment-sec">
        <div class="container-fluid">
            <div class="row">
                <div class="pl-tp-content" >
                    <h2>Steps to Get Your <b>Personalised Ayurvedic</b> Treatment</h2>
                    <p>
                       Dr. Sharda Ayurveda works on root cause treatment and offers customized treatment
                    </p>
                </div>
            </div>
            <div class="row cst-pl-row">
                <div class="cst-boxies">
                    <div class="treatment-box" >
                        <img src="{{ URL::asset('front/images/Reach-Us.webp') }}" class="treatment-img" alt="How to Reach us">
                        <h3>Reach us</h3>
                        <p>By Call, Or Chat</p>
                    </div>
                    <div class="treatment-box">
                        <img src="{{ URL::asset('front/images/book-appointment.webp') }}" class="treatment-img" alt="Book Appointment">
                        <h3>Book an</h3>
                        <p>Appointment with <br>our Specialist</p>
                    </div>


                    <div class="treatment-box">
                        <img src="{{ URL::asset('front/images/Nadi-Pariksha.webp') }}" class="treatment-img" alt="Nadi Pariksha">
                        <h3>Nadi Pariksha</h3>
                        <p>To get Doshas analyzed</p>
                    </div>

                    <div class="treatment-box">
                        <img src="{{ URL::asset('front/images/Specialized-doctor.webp') }}" class="treatment-img" alt="Specialization">
                        <h3>Specialized</h3>
                        <p>Doctor will assist you</p>
                    </div>

                    <div class="treatment-box">
                        <img src="{{ URL::asset('front/images/Personalized-Ayurvedic-treatment.webp') }}" class="treatment-img" alt="Get Treatment">
                        <h3>Personalized</h3>
                        <p>Ayurvedic treatment <br>with diet plan</p>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <section class="about-us-sec">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="tp-abt-content">
                        <h2><b>Dr. Sharda Ayurveda</b> Ayurvedic Center In India</h2>
                        <p class="tp-abt-lh">Once You Choose Dr. Sharda Ayurvedic clinic in India, Anything is Possible</p>
                    </div>
                </div>
            </div>
            <div class="row custum-row">
                <div class="col-xl-6 col-lg-6 col-md-12 colsm-12 col-12 mb-4" >
                    <div id="dna_video">
                        <div class="youtube-player" data-id="PGKM-htmG9A" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/backpain.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 colsm-12 col-12">
                    <div class="about-right-content" >
                        <p><strong>DR. SHARDA AYURVEDA</strong> is renowned for the treatment of auto-immune diseases over the years in the world.</p>
                        <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">We have immerged as leading centers in Punjab, India and have treated 100000+ of patients worldwide.</p>
                        <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Our team of professional doctors aims to provide traditional and scientific research-based effective treatment.</p>
                        <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Our strong pillars are Care, Compassion and Commitment towards our patients.</p>
                        <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Our sole aim is to provide authentic, traditional Ayurvedic treatment to patients who have lost their hopes to recover.</p>
                        <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">We are dedicated to Hope, Healing and Recovery of our patients.</p>
                        <div class="btn-about">
                            <a href="https://www.drshardaayurveda.com/about-us" class="btn-aboutus">Know More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="happy-world-sec">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="hw-content" >
                        <img src="{{ URL::asset('front/images/Dr-Sharda-white-logo.webp') }}" class="img-fluid wht-logo" alt="drshardaayurveda logo">
                        <h2>लोकाः समस्ताः सुखिनो भवन्तु</h2>
                        <p>Let The Entire World Be Happy</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="dr-mukesh-sharda-sec"> 
        <div class="container-fluid">
            <div class="row">
                <div class="cl-test">
                    <h2 class="gen-title">Our <b> Founder</b></h2>
                </div>
            </div>
            <div class="row cust-dr">
                <div class="col-xl-4 col-md-12 col-sm-12" >
                    <div class="dr-img-left dr-img-center" >
                        <img src="{{ URL::asset('front/images/Dr-Mukesh-Sharda.webp') }}" class="img-fluid" alt="Dr Mukesh Sharda">
                    </div>
                </div>
                <div class="col-xl-8 col-md-12 mt-4 col-sm-12" >
                    <p><b>Dr. Mukesh Sharda (BAMS, Ph.D.)</b> is serving as an <a class="green-anchor"href="https://www.drshardaayurveda.com/dr-mukesh-sharda"><b>Ayurvedic doctor</b></a> for more than 12+ years and in 2013 she started the chain of Dr. Sharda Ayurveda clinic. As the recovery rate of patients increased, it motivated her to spread several branches in various sections of Punjab, India. The main motive of Dr. Sharda Ayurveda is to spread the awareness and importance of Ayurveda in every house. Her life purpose is to treat every sufferer and make them healthy through natural and holistic treatment, i.e., Ayurveda. She is renowned and recognized worldwide for providing effective Ayurvedic treatment. Dr. Mukesh Sharda has been awarded by the honorable chief minister of Punjab respected Capt. Amrinder Singh as well as honorable health minister Mr. Brahm Mohindra and also from honorable central health minister Mr. J.P Nadda.</p>
                    <div class="sign-area">
                        <img src="{{ URL::asset('front/images/signature.webp') }}" class="ceo-sign img-fluid" alt="CEO Sign" >
                        <p>Director (CEO)</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="doctors-sec">
    <div class="container-fluid">
    <div class="head-bx">
        <div class="row">
                <div class="cl-test">
                    <h2 class="gen-title">Meet Our <b>Doctors</b></h2>
                </div>
            </div>
                <p>Dr. Sharda Ayurveda is focusing on enhancing the quality of the treatment that will aim to provide better and improved health.</p>
        </div>
        <div class="row cust-iba">
            <div class="owl-carousel owl-theme">
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                    <img class="card-img-top" src="{{ URL::asset('front/images/DrVinodChauhan.webp') }}" alt="Dr. Vinod Chauhan" title="Dr. Vinod Chauhan">
                        <div class="card-body">
                            <div class="card-body">
                            <h4 class="card-title">Dr. Vinod Chauhan (BAMS, M.Sc.)</h4>
                            <p class="card-text">Dr. Vinod Chauhan holds an experience of 20+ years in treating numerous chronic conditions patients, specifically sexual, skin, and digestive-related issues, all with his knowledge and commitment towards his services. Having a master's degree in clinical research and regulatory affairs, he is being associated with Dr. Sharda Ayurveda for the last 2 years.
                            </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                    <img class="card-img-top" src="{{ URL::asset('front/images/Dr-Jeevan-Jyoti.webp') }}"alt="Dr. Jeevan Jyoti" title="Dr. Jeevan Jyoti">
                        <div class="card-body">
                        <h4 class="card-title">Dr. Jeevan Jyoti (BAMS, MD)</h4> <p class="card-text">Dr. Jeevan Jyoti holds an experience of more than 20+ years of experience and is specialized in treating a range of chronic diseases like skin, gynecology, respiratory, neurological, and many others. She has her post-graduate degree in clinical cosmetology. She is actively performing her duty as a senior Ayurvedic doctor with Dr. Sharda Ayurveda for the last 2 years.</p> 
                        </div>
                    </div>
                </div>
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                    <img class="card-img-top" src="{{ URL::asset('front/images/Dr-Raman.webp') }}" alt="Dr. Ramanjeet Kaur"title="Dr. Ramanjeet Kaur">
                        <div class="card-body">
                        <h4 class="card-title">Dr. Ramanjeet Kaur (BAMS, MD)</h4> <p class="card-text">Dr. Ramanjeet Kaur is an experienced and qualified Ayurvedic doctor having 7+ years of experience and did her master's in AM (Alternative Medicine). She has put forward her education in a way that will effectively heal patients and provide them with lifelong results, especially for chronic disease complainants. She has been doing her services as a senior Ayurvedic doctor at Dr. Sharda Ayurveda for the last 5 years.</p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                    <img class="card-img-top" src="{{ URL::asset('front/images/Dr-Vandana-Sharma.webp') }}" alt="Dr. Vandana Sharma" title="Dr. Vandana Sharma">
                        <div class="card-body">
                        <h4 class="card-title">Dr. Vandana Sharma (BAMS)</h4>
                            <p class="card-text">Dr. Vandana Sharma has successfully treated countless patients, who mainly complain about joint pain. She holds an experience of 4+ years and is serving as an Ayurvedic expert at Dr. Sharda Ayurveda. Her knowledge has effectively treated patients who have lost all hopes in getting recovered and were suggested for surgery as the last choice to get relief.</p>
                        </div>
                    </div>
                </div>

<!--                 <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                    <img class="card-img-top" src="{{ URL::asset('front/images/dr-nikita.webp') }}" alt="Dr. Nikita" title="Dr. Nikita">
                        <div class="card-body">
                        <h4 class="card-title">Dr. Nikita (BAMS)</h4>
                        <p class="card-text">Dr. Nikita is a graduate of Ayurvedic sciences. She is an expert at Dr. Sharda Ayurveda and is genuinely concerned for her patients. With a passion to heal people, she willingly is in the service of mankind. Along with being a great doctor, she is able to communicate effectively with her patients which helps them to heal and recover.</p>
                        </div>
                    </div>
                </div> -->

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                    <img class="card-img-top" src="{{ URL::asset('front/images/dr-preeti.webp') }}" alt="Dr. Preeti Arora" title="Dr. Preeti Arora">
                        <div class="card-body">
                        <h4 class="card-title">Dr. Preeti Arora (BAMS, M.Sc.)</h4>
                        <p class="card-text">Dr. Preeti Arora has an experience of 4+ years and has been associated with Dr. Sharda Ayurveda since 2019. She holds a master’s degree in hospital management and is working selflessly for providing the best of the best care to the patients. Her guidance has successfully treated an enormous number of chronic disease patients mainly skin, gynecology, joint, digestive, and many others.</p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                    <img class="card-img-top" src="{{ URL::asset('front/images/Dr-Sunil-Chabra.webp') }}" alt="Dr. Sunil Chabbra" title="Dr. Sunil Chabbra">
                        <div class="card-body">
                        <h4 class="card-title">Dr. Sunil Chabbra (BAMS)</h4>
                        <p class="card-text">Dr. Sunil Chabbra has a broad experience of 15+ years and is working as a senior Ayurvedic doctor at Dr. Sharda Ayurveda for the last 11+ years. He is expertise in treating chronic diseases with holding the highest success rate of healing sexual, skin, respiratory, endocrine, neurological disorders, and many others.</p>
                        </div>
                    </div>
                </div>

<!--                 <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                    <img class="card-img-top" src="{{ URL::asset('front/images/Dr-Sarabhjot-Singh.webp') }}" alt="Dr. Sarabjot Singh" title="Dr. Sarabjot Singh">
                        <div class="card-body">
                        <h4 class="card-title">Dr. Sarabjot Singh (BAMS)</h4>
                        <p class="card-text" text size=>Dr. Sarabjot Singh is a qualified Ayurveda professional with holding an experience of 8+ years and has currently been associated with Dr. Sharda Ayurveda for a long time, i.e., 5+ years. His skills and knowledge have successfully treated patients that were mainly complainants of joint pain, skin, liver, circulatory, and respiratory.</p>
                        </div>
                    </div>
                </div> -->
                
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                    <img class="card-img-top" src="{{ URL::asset('front/images/dr-dapinder-kaur.webp') }}" alt="Dr. Dapinder Kaur" title="Dr. Dapinder Kaur">
                        <div class="card-body">
                        <h4 class="card-title">Dr. Dapinder Kaur (BAMS)</h4>
                                        <p class="card-text">Dr. Dapinder Kaur has a graduate degree in Ayurvedic sciences. She believes in serving mankind by ending their suffering. With her medical and interpersonal skills, she not only provides Ayurvedic treatment to her patients but also educates them regarding the disorder. She is a great expert at Dr. Sharda Ayurveda and aims to make a difference with her intelligence.</p>
                                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- <section class="our-product" style="display: none;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="prod-tp-content" >
                        <h2>Our <b>Products</b></h2>
                        <p>Once You Choose Dr. Sharda Ayurvedic Hospital In</p>
                    </div>
                </div>
            </div>
            <div class="row cust-product">
                <div class="col-xl-4 col-md-12 mb-4">
                    <div class="prod-box" >
                        <img src="{{ URL::asset('front/images/product.webp') }}" class="img-fluid" alt="Product" >
                        <ul class="price">
                            <li class="brand-name">Dr.sharda </li>
                            <li class="header">Best joint pain oil of ayurveda</li>
                            <li class="rate"><i class="fa fa-inr" aria-hidden="true"></i> 200/-</li>
                            <li class="rating">
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                            </li>
                            <li><a href="#" class="btn-shop-know">Shop Now</a></li>
                        </ul>

                    </div>
                </div>
                <div class="col-xl-4 col-md-12 mb-4">
                    <div class="prod-box" >
                        <img src="{{ URL::asset('front/images/product.webp') }}" class="img-fluid" alt="Product" >
                        <ul class="price">
                            <li class="brand-name">Dr.sharda </li>
                            <li class="header">Best joint pain oil of ayurveda</li>
                            <li class="rate"><i class="fa fa-inr" aria-hidden="true"></i> 200/-</li>
                            <li class="rating">
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                            </li>
                            <li><a href="#" class="btn-shop-know">Shop Now</a></li>
                        </ul>

                    </div>
                </div>
                <div class="col-xl-4 col-md-12 mb-4" >
                    <div class="prod-box">
                        <img src="{{ URL::asset('front/images/product.webp') }}" class="img-fluid" alt="Product" >
                        <ul class="price">
                            <li class="brand-name">Dr.sharda </li>
                            <li class="header">Best joint pain oil of ayurveda</li>
                            <li class="rate"><i class="fa fa-inr" aria-hidden="true"></i> 200/-</li>
                            <li class="rating">
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                            </li>
                            <li><a href="#" class="btn-shop-know">Shop Now</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
 -->   
 <section class="client-testimonial" >
        <div class="container-fluid">
            <div class="row">
                <div class="cl-test">
                    <h2 class="client-test-title">Patient's <b>Testimonial</b></h2><br>
                </div>
            </div>
            <div class="row cust-test">
                <div class="col-md-12">
                    <div id="client-test" class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="testimonial-content">
                                <img src="{{ URL::asset('front/images/Mrs. Seema.webp') }}" class="img-fluid" alt="Rheumatoid Arthritis Testimonial" >
                                <h3 class="usrname">Mrs. Seema</h3>
                                <ul>
                                    <li>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                    </li>
                                </ul>
                                <p>
                                    <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                    I was suffering from <a class="green-anchor"href="https://www.drshardaayurveda.com/rheumatoid-arthritis">Rheumatoid Arthritis (RA)</a> from the past 10 years. 
                                    I took western medicines for more than 8 years but got no relief. Then I got to know about Dr. Sharda Ayurveda and visited their Ludhiana clinic. They got my various blood investigation done and my RA was positive. 
                                    In July 2019 I started with their course of treatment. I did not drop out taking my medicines. Then on 6th August 2020 I got my blood investigation done and my RA turned out to be negative. A big thanks to Dr. Sharda Ayurveda who developed a lost hope of mine to ever recover from RA, and finally I can say I am free from RA.
                                    <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                                </p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial-content">
                                <img src="{{ URL::asset('front/images/Manjindar.webp') }}" class="img-fluid" alt="Asthma Testimonial" >
                                <h3 class="usrname">Manjindar Kaur</h3>
                                <ul>
                                    <li>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                    </li>
                                </ul>
                                <p>
                                    <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                    Myself Manjindar Kaur from Rupnagar. I was suffering from Asthma. I was totally dependent on inhalers and had breathing issues in winters. But after treatment from Dr. Sharda Ayurveda Ludhiana from Dr.  Mukesh Sharda, i was fully satisfied with their treatment. Within 7 months of treatment i was totally relieved from Asthma. I can blindly say she is the best Ayurvedic doctor i have ever met.
                                    <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                                </p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial-content">
                                <img src="{{ URL::asset('front/images/Raj-Kumar.webp') }}" class="img-fluid" alt="Knee Pain Testimonial" >
                                <h3 class="usrname">Raj Kumar</h3>
                                <ul>
                                    <li>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                    </li>
                                </ul>
                                <p>
                                    <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                    I was suffering from Knee Pain for many years. I took treatment from various hospitals but of no use. Then somebody advised me to take treatment from Dr. Sharda Ayurveda clinic and after taking treatment from there I was almost free from <a class="green-anchor" href="https://www.drshardaayurveda.com/knee-pain">knee pain</a> within 6 months.  I have discontinued my medicines and feeling no pain at all and I'm very thankful to Dr. Sharda Ayurveda for the most effective Ayurvedic treatment.
                                    <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                                </p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="testimonial-content">
                                <img src="{{ URL::asset('front/images/arminder-kaur-gil.webp') }}" class="img-fluid" alt="Knee Pain Testimonial" >
                                <h3 class="usrname">Arminder Kaur Gill</h3>
                                <ul>
                                    <li>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                        <span class="fa fa-star checked"></span>
                                    </li>
                                </ul>
                                <p>
                                    <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                    I Arminder Kaur Gill for the last 5-6 years was suffering from diabetes, weather allergy, and unexplained weight gain. Even after consulting various renowned <strong>Ayurvedic Doctors in Toronto</strong>, none of the treatments provided satisfactory results. My health condition was progressively declining and imagining a day without taking medicines and inhalers was impossible as it was difficult for me to breathe. Then, after quite a search, I got to know about Dr. Sharda <strong>Ayurveda-Online consultancy in Canada</strong>. They are known for providing the best online consultation in Canada and around the globe at your comfort. Their Ayurvedic doctors in-depth examined my condition and prescribed the medications and mandatory dietary and lifestyle changes. Their holistic treatment helped resolve all my health issues within just 4 months and blessed me with a disease-free life. As per my experience, I would highly recommend everyone to consult them.
                                    <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="latest-post-sec">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 col-md-12">
                    <div class="lp-content">
                        <h2>Latest <b>Post</b></h2>
                        <p>Blog Posts about the disease and their Ayurvedic treatments</p>
                    </div>
                </div>
            </div>
            <div class="row custom-lp">
                <?php foreach($posts as $post) : $img = $post['post_image']; ?>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 mb-4">
                    <div class="lp-post">
                        <img src="/uploads/posts/{{ $img }}" class="img-fluid" alt="Blogopost" >
                        <div class="post-content">
                            <h3><?= strip_tags(Str::limit($post['post_title'], 55)); ?></h3>
                            <p><?= strip_tags(Str::limit($post['post_contant'], 50)); ?></p>
                            <a href="/blogs/<?=str_replace(array( '(', ')' ), '', str_replace(" ","-", strtolower($post['name']))); ?>" class="ptn-post">Read more...</a>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    @include('../partials/frontend/form')
</div>
<script type='text/javascript'>
/*$(window).bind("load", function() {
    $('#dna_video').prepend('<iframe src="https://www.youtube.com/embed/PGKM-htmG9A?rel=0" width="680" height="383" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
});*/
</script>
<script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "WebSite",
      "name": "Dr Sharda Ayurveda",
      "potentialAction": {
        "@type": "SearchAction",
        "target": "https://www.drshardaayurveda.com/search?q={search_term_string}",
        "query-input": "required name=search_term_string"
      },
      "url": "https://www.drshardaayurveda.com/"
    }
</script>


<script type="text/javascript">
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            responsiveClass: !0,
            navigation : true,
            autoplay: true,
            loop: true,
            smartSpeed: 2500,
            singleItem : true,
            navigationText: ["<i class='fa fa-angle-left fa-2'></i>", "<i class='fa fa-angle-right fa-2'></i>"],
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                480: {
                    items: 1,
                    nav: !1
                },
                768: {
                    items: 2,
                    nav: !1
                },
                1000: {
                    items: 3,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1024: {
                    items: 4,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1366: {
                    items: 4,
                    nav: !1
                }
            }
        })
    });

</script>
@stop