<?php
/**
 * Eczema Skin Page 
 * 
 * @created    11/01/2022
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2022
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Ayurveda For Eczema</h3>
                    <p style="font-size: 18px;">SKIN-VEST-MENT It is going to with you for life time</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/eczema">Eczema</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#types">TYPES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#managements">MANAGEMENTS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="definition">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/Eczema.webp') }}" class="why-choose-us img-fluid" alt="Eczema (Vicharchika)">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Eczema </b>Ayurvedic Treatment</h1>
                <p>Eczema also called dermatitis is termed “Vicharchika” in Ayurveda.  Eczema is an ordinary skin disorder characterized by inflammation of dermal tissues, followed by peeling, itching, dry skin patches, and redness. In rare cases, painful blisters can be seen on the skin.  In ancient times it was not a common disorder but now thousands of people across the nation suffer from various types of eczema. 
                Ayurvedic treatment for eczema involves a purely natural approach that helps heal the symptoms of this skin disorder. Eczema is associated with a group of symptoms like the appearance of inflamed, itchy patches, and lesions on the skin. Eczema makes the skin turn rough thus, sometimes leading to bleeding in some cases. The most commonly affected parts are the elbows, knees, and feet. It is a non-contagious disease i.e., a non-transferable disease. Eczema majorly affects infants and also other age groups. Atopic Dermatitis is a chronic condition that with time tends to spread periodically. The causes for the occurrence of the problem can be many i.e., genetics/hereditary, environment, weak immune system, and many more. Eczema if not treated timely can also lead to other complications like sleep disorders, asthma, and other skin infections. 
                <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                    <p>
                        Ayurvedic treatment is natural and holistic. It focuses on  understanding the root cause, and the recommendations are provided according to the severity of the disease. The unbalancing of the Doshas causes eczema. Ayurvedic treatment for eczema focuses on balancing doshas. Eczema Ayurvedic treatment from Dr. Sharda Ayurveda is the best and most effective. Herbal formulations of Ayurvedic medicines provide an extra advantage over others leading to no side effects and safer treatment. Ayurvedic treatment followed by dietary and lifestyle modification holds equal importance for the best results.
                    </p>
                </span>
                    <div class="btn-ayurvedic">
                        <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                    </div>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec" id="types">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="heading1">Types of <b>Eczema</b></h2>
                <p font align="center">The best option is Ayurveda for eczema and the symptoms as this skin condition requires a natural way which will be helpful in not only reducing the pain but also boosting the immunity.</p>
            </div>
        </div>
        <div class="row cust-iba">
            <div class="owl-carousel owl-theme" >
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <a href="" style="color:black; text-decoration: none;" >
                        <img src="{{ URL::asset('front/images/atopic-dermatitis.webp') }}" class="card-img-top lazyload" alt="Atopic Dermatitis" alt title="Atopic Dermatitis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Atopic Dermatitis</h4>
                            <p >
                                 It is the most common and chronic type of eczema that affects children and adults alike and may continue for years. It mostly gets affected by external irritants and can suddenly trigger an immune response that leads to skin redness, itchiness, and dryness. Eczema usually affects the face, fold of the arms and legs, and neck. Atopic dermatitis treatment in Ayurveda is proven to provide the best results.
                            </p>
                        </div>
                        <p>Read More</p>
                    </div>
                    </a>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <a href="https://www.drshardaayurveda.com/eczema/contact-dermatitis" style="color:black; text-decoration: none;" >
                        <img src="{{ URL::asset('front/images/new-images.webp') }}" class="card-img-top lazyload" alt="Contact Dermatitis" alt title="Contact Dermatitis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Contact Dermatitis</h4>
                            <p class="card-text">
                                In this condition skin cells get damaged by direct contact with skin irritants. Symptoms like redness, irritation on the skin, and dryness are common in this condition. Direct contact with skin irritants such as harsh chemicals in soaps, shampoos, cleansing agents, paints, jewelry, etc. can be the cause. It can affect any part of the body exposed to irritants. Contact Dermatitis treatment if done timely saves it from spreading further to other parts of the body.
                            </p>
                        </div>
                        <p>Read More</p>
                    </div>
                    </a>
                </div>


                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <a href="" style="color:black; text-decoration: none;" >
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/dyshidrotic-eczema.webp') }}"  alt="Dyshidrotic Eczema">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Dyshidrotic Eczema</h4>
                            <p class="card-text">It causes the formation of bubble-like red small blisters on the hands that are quite itchy. Men are fewer sufferers as compared to women. This is chronic as it results in sudden flare-ups when exposed to trigger factors like allergic reactions and weather changes and can also happen due to overexposure to water. It usually affects parts of the hand, fingers, and feet. Dyshidrotic Dermatitis treatment is a must for a timely recovery. </p>
                        </div>
                        <p>Read More</p>
                    </div>
                </a>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <a href="" style="color:black; text-decoration: none;" >
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/neurodermatitis.webp') }}"  alt="Neurodermatitis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Neurodermatitis</h4>
                            <p class="card-text">In this type of eczema there is a constant urge to scratch, which irritates the affected skin.  It causes raised, dry patches of skin that are extremely itchy. It can be caused by an insect bite, nerve damage, or allergic to certain fabrics. The commonly affected parts of the body are the groin area, forearm, and back of the neck. Neuro Dermatitis treatment in Ayurveda is irreversible.
                            </p>
                        </div>
                        <p>Read More</p>
                    </div>
                </a>
                </div>
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <a href="" style="color:black; text-decoration: none;" >
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/nummular-eczema.webp') }}"  alt="Nummular Eczema">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Nummular Eczema</h4>
                            <p class="card-text">It causes oval, round, coin-shaped spots on the skin that sometimes tend to bleed. It is different from other types and is completely treatable. They can be caused by exposure to skin irritants, injury to the skin, or can be triggered by existing eczema. The most commonly affected parts are the legs and arms but can affect other body parts as well. Nummular Dermatitis treatment can be easily done with effective results.</p>
                        </div>
                        <p>Read More</p>
                    </div>
                </a>
                </div>
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <a href="https://www.drshardaayurveda.com/eczema/stasis-dermatitis" style="color:black; text-decoration: none;" >
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/stasis-dermatitis.webp') }}"  alt="Stasis Dermatitis"title="Stasis Dermatitis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Stasis Dermatitis</h4>
                            <p class="card-text">This type of eczema usually affects the lower legs. This happens when deoxygenated blood from the legs is unable to reach the heart owing to the failure of valves in the veins of your legs thus causing pain, redness, swelling, and itchiness. This condition is called venous insufficiency and is the main cause of stasis dermatitis. The main causes are- high blood pressure, kidney failure, and venous insufficiency. Stasis Dermatitis treatment in Ayurveda aims to remove the root cause of the disease.</p>
                        </div>
                        <p>Read More</p>
                    </div>
                </a>
                </div>
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <a href="" style="color:black; text-decoration: none;" >
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/seborrheic-dermatitis.webp') }}"  alt="Seborrheic Dermatitis" title="Seborrheic Dermatitis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Seborrheic Dermatitis</h4>
                            <p class="card-text">A skin condition that causes patches of itchy, red skin and scaly white or yellow flakes. It is commonly found in areas that are prone to have more sebaceous glands. 
                            It is usually affected by hormonal imbalance, weather changes, and exposure to certain cleansing products. The parts of the body usually affected are the scalp, eyebrows, eyelids, T-zone of the face, and chest.</p>
                        </div>
                        <p>Read More</p>
                    </div>
                </a>
                </div>
            </div>
        </div>
        <div class="btn-ayurvedic">
            <a href="https://www.drshardaayurveda.com/eczema/types-of-eczema" class="btn-appointment">Read More</a>
        </div>
    </div>
</section>

<section class="our-product causes" style="background-color:#f3f3f3" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate">
                    <h2 class="heading1">Causes of <b>Eczema</b></h2>
                </div>
                <p font align="center">Eczema Ayurvedic treatment will help with the causes of symptoms as the aim is to remove the root cause of the disease.</p>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="causes">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Genetics
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Genetics of a person is the cause of many skin diseases. It is one of the true causes of eczema as it spreads or can even worsen skin rashes. Environmental factors can trigger genetics indirectly and is responsible for the appearance and persistence of the disease.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Hyperactive immune System
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                It is directly related to the autoimmune responses of the system. Overexpression of the immune system, proteins leads to the spreading of the disease.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Weather and Temperature
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                                <ul>
                                     Hot weather can invite eczema. As the temperature rises the chances of getting itchy, and irritated skin is high. So it is necessary to keep the skin hydrated and moisturized. Cold weather is the invitation for dry, chapped, and cracked skin.
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Stress 
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                                Stress can directly hamper the immune system which in turn can lead to the initiation of the infection.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Environmental Factors 
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                                 It can indirectly influence the emergence of an infection. The common factors are Allergens like pollen and dust. Food substances such as wheat, soya, and dairy products.
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a free Assessment Report of <strong>Eczema Ayurvedic Treatment</strong></p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/eczema-symptoms.webp') }}" class="img-fluid"alt="Eczema symptoms">
        </div>
        <div class="cvn sym" >
            <h3>Symptoms of <span>Eczema</span></h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Red Patches</p>
            <p>The most common symptom is the appearance of red scaly patches all over the body.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Itchiness</p>
            <p>The scaly lesions causes irritation and itchiness. This condition occurs when the problem becomes more severe. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Skin Color Change </p>
            <p>The skin becomes dull, weak, and starts to lose its moisture. This skin becomes rough, darker, and thicker</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Cracks on Skin</p>
            <p>In severe conditions, the cuts and cracks on the skin are common which can even cause pain. It is said to be the breeding ground for bacterial and <a class="green-anchor"href="https://www.drshardaayurveda.com/fungal-infection">fungal infections</a> to grow. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Swollen Skin</p>
            <p>Inflamed or swollen skin is common with the appearance of dark red patches on the skin. The hands are most likely to be prone to this symptom for indicating the eczema infection in adults.</p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="managements">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Ayurvedic Treatment For <b>Eczema</b></h2>

                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurvedic treatment involves consuming various herbs for safe and life-long recovery. Some of the herbs beneficial in treating eczema are Neem, Turmeric, Triphala, and Cardamom.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        Panchkarma is highly recommended for treating eczema. The procedure includes:
                        <ul>
                            <li>Consuming desi ghee for 5 to 8 days.</li>
                            <li>Massaging body with herbal oils.</li>
                        </ul>
                    This treatment helps in removing the toxins and boost immunity.</p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    The foods rich in Vitamin A, C, and Zinc are essentially helpful as they help in reducing the symptoms of disease.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Dietary and lifestyle changes followed by practicing yoga and meditation have proven to provide positive results. As healthy habits improves body functioning thereby making it less prone to disease.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Avoid consumption of processed food, caffeine products, and smoking to get recovery from the pain and itchiness caused due to eczema.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Primrose oil is used for getting relief from eczema and itchiness as it contains omega 6-fatty acids and linolenic acids which provide anti-inflammation property. It can be consumed orally or can also be applied to affected areas.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    They can be added to cooked food, can be consumed in tea, or can be taken as medicine. Make sure to consult your expert first before beginning Ayurvedic treatment for eczema and the early recovery you can opt for Ayurvedic treatment for eczema from Dr. Sharda Ayurveda.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div id="dna_video">
                    <div class="youtube-player" data-id="nHZUQt-N2uw" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                        <img src="{{ URL::asset('front/images/Eczema-thumbnail.jpg') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                        <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Jasveer Kaur</h3>
                            <p class="desig">Eczema Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                For the last 4 months, I was experiencing red patches on my skin which were accompanied by itchiness. I tried various creams to get relief but the results were not satisfactory. So I consulted Dr. Sharda Ayurveda for the treatment. There they provided me with the medicines and recommended a healthy diet and lifestylechanges that benefitted in my recovery. Today, within just a few weeks of Ayurvedic treatment I can feel the change in my skin. I am thankful to Dr. Sharda Ayurveda</a>.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Rubi</h3>
                            <p class="desig">Eczema Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                There were dark-colored itchy patches visible all over my hands. I was getting worried about my condition; therefore, I consulted Dr. Sharda Ayurveda as one of my friends suggested so I visited there. Within 1 month of regular medications, I am now at a recovery stage. All thanks to Dr. Sharda Ayurveda for their safe and effective treatment.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Simranjeet Kaur</h3>
                            <p class="desig">Eczema Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Ever since I gave birth to my child,he used to have skin allergy. After taking eczema ayurvedic treatment in Ludhiana from Dr. Sharda Ayurveda, my child is in such a relief. His skinis perfect now and there are no side effects of Ayurvedic treatment. Thankyou, so much Dr. Mukesh Sharda.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid faqs">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                What are the visible symptoms of the disease?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                            The symptoms can vary from the condition i.e., mild to severe. The symptoms are:
                            <ul>
                                <li>Dry and cracked skin</li>
                                <li>Red, and itchy patches all over the skin.</li>
                                <li>Swollen and inflamed skin</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                    Is eczema a contagious disease?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                            This skin disease is non-contagious and therefore is not transferrable. The major cause of the disease is genetics and environmental factors.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                What are the complications associated with the disease?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                            If the disease is not treated on time it can lead to complications that are difficult to  recover from. The complication includes.
                            <ul>
                                <li>Cataract</li>
                                <li>Hypopigmentation or leukoderma</li>
                                <li>Vitamin D deficiency causes discoloration of the skin.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                    Which type of foods trigger eczema?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                            Several foods like Eggs, Tomatoes, Soya, Broccoli, Wheat, and citrus Fruits can trigger eczema. So it is advised to avoid these food items.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                How is Ayurveda beneficial in treating eczema?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                            Ayurvedic treatment implements a holistic approach. The diet and lifestyle changes can help in early recovery. Ayurvedic treatment from Dr. Sharda Ayurveda for eczema and other skin-related diseases is safe and effective.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    Is eczema rare in India?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                                No, it is not. In ancient times it was rare but our bad eating habits, not maintaining personal hygiene, and lifestyle changes have made eczema common.
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Can poor hygiene be one of the main reasons for eczema?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                            Yes, if the skin is not washed properly. It can become the breeding ground for germs and therefore higher chances of getting eczema.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                What causes eczema's sudden outburst?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                            It can be caused due to triggers which may vary from person to person. The triggering factors can be:
                            <ul>
                                <li>Exposure to chemical-based soaps, detergents, and perfumes.</li>
                                <li>Due to plant pollens</li>
                                <li>Synthetic or woolen clothes</li>
                                <li>Dehydrated or dry skin.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                What is the main root cause of eczema?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                            The root cause of eczema is stated as an overactive or overexpressed immune system in response to exposed triggers. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Does keeping the body hydrated prevent eczema?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                            The individual suffering from eczema has comparatively dry and itchy skin than others. Therefore it is recommended to keep skin and body hydrated to prevent the occurrence of eczema.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<style>
    .carousel-inner {position: relative;width: 100%;overflow: hidden;}
    .carousel-item {position: relative;float: left;width: 100%;margin-right: -100%;-webkit-backface-visibility: hidden;backface-visibility: hidden;transition: -webkit-transform .6s ease-in-out;transition: transform .6s ease-in-out;transition: transform .6s ease-in-out,-webkit-transform .6s ease-in-out;}
    .card-body {padding-top: 1.5rem;padding-bottom: 1.5rem;-webkit-border-radius: 0!important;border-radius: 0!important;flex: 1 1 auto;-ms-flex: 1 1 auto;}
    .card .card-body a h3{font-size: 18px;color: #ff9000;line-height: 1.4;}
    .card .card-body h3{color: #373433;font-size: 18px;    line-height: 1.4;}
    .font-weight-bold {font-weight: 700!important;}
    .card-title {margin-bottom: .75rem;}
    .card-img-top {width: 100%;border-top-left-radius: calc(.25rem - 1px);border-top-right-radius: calc(.25rem - 1px);}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            responsiveClass: !0,
            navigation : true,
            autoplay: true,
            loop: true,
            smartSpeed: 2500,
            singleItem : true,
            navigationText: ["<i class='fa fa-angle-left fa-2'></i>", "<i class='fa fa-angle-right fa-2'></i>"],
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                480: {
                    items: 1,
                    nav: !1
                },
                768: {
                    items: 2,
                    nav: !1
                },
                1000: {
                    items: 2,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1024: {
                    items: 3,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1366: {
                    items: 3,
                    nav: !1
                }
            }
        })
    });

</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Eczema",
    "item": "https://www.drshardaayurveda.com/eczema"  
  }]
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What are the visible symptoms of the disease?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The symptoms can vary from the condition i.e., mild to severe. The symptoms are:
•   Dry and cracked skin.
•   Red, and itchy patches all over the skin.
•   Swollen and inflamed skin."
    }
  },{
    "@type": "Question",
    "name": "Is eczema a contagious disease?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "This skin disease is non-contagious and therefore is not transferrable. The major cause of the disease is genetics and environmental factors."
    }
  },{
    "@type": "Question",
    "name": "What are the complications associated with the disease?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If the disease is not treated on time it can lead to complications that are difficult to recover from. The complication includes.
•   Cataract
•   Hypopigmentation or leukoderma
•   Vitamin D deficiency causes discoloration of the skin."
    }
  },{
    "@type": "Question",
    "name": "What food items should be avoided in eczema?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The foods that must be avoided are: 
•   Citrus fruit
•   Gluten products
•   Dairy products
•   Spices 
•   Processed or packaged food
•   Tomatoes."
    }
  },{
    "@type": "Question",
    "name": "How is Ayurveda beneficial in treating eczema?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurvedic treatment implements a holistic approach. The diet and lifestyle changes can help in early recovery. 
 Ayurvedic treatment from Dr. Sharda Ayurveda for eczema and other skin-related diseases is safe and effective."
    }
  },{
    "@type": "Question",
    "name": "How one can take care of eczema at home?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The recommendations suggested for preventing the spread of eczema at home are 
•   Wash your hands with lukewarm water.
•   Avoid doing wet work or wearing gloves while doing it.
•   Apply mild moisturizer on hands.
•   Do not wear rings as its common place of growth for infection.
•   Disinfect your house daily to avoid the growth of the infection."
    }
  },{
    "@type": "Question",
    "name": "Can poor hygiene be one of the main reasons for eczema?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, if the skin is not washed properly. It can become the breeding ground for germs and therefore higher chances of getting eczema."
    }
  },{
    "@type": "Question",
    "name": "What causes eczema's sudden outburst?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It can be caused due to triggers which may vary from person to person. The triggering factors can be:
•   Exposure to chemical-based soaps, detergents, and perfumes.
•   Due to plant pollens.
•   Synthetic or woolen clothes
•   Dehydrated or dry skin."
    }
  },{
    "@type": "Question",
    "name": "What is the main root cause of eczema?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The root cause of eczema is stated as an overactive or overexpressed immune system in response to exposed triggers."
    }
  },{
    "@type": "Question",
    "name": "Does keeping the body hydrated prevent eczema?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The individual suffering from eczema has comparatively dry and itchy skin than others. Therefore it is recommended to keep skin and body hydrated to prevent the occurrence of eczema."
    }
  }]
}
</script>

@stop