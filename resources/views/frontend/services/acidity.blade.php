<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h1 class="ser-heading mt-4">Ayurvedic Treatment for Acidity</h1>
                    <p style="font-size: 18px;">Get Acid Reflux Root Cause Solution with Ayurveda</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/acidity">Acidity</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#overview">OVERVIEW <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatment">TREATMENT</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="overview">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/acidity.webp') }}" class="why-choose-us img-fluid" alt="women suffering from acidity reflux" title="women suffering from acidity reflux">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h2 class="heading1">What is <b>Acidity?</b></h2>
                <p>
                    Acidity is a common condition characterised by the occurrence of heartburn, typically felt in the lower chest area. In Ayurveda, it is referred to as Amla Pitta, indicating an imbalance in the Pitta Dosha, which is responsible for digestion. The main cause of acidity is the upward movement of stomach acid into the oesophagus, resulting in discomfort and a burning sensation. 
                    <p>Ayurvedic treatment for acidity focuses on restoring balance to the aggravated Pitta Dosha. It involves adopting dietary and lifestyle modifications, along with the use of natural remedies to alleviate symptoms and address the root cause. Ayurveda emphasises the consumption of cooling and soothing foods such as fresh fruits, vegetables, and herbal infusions to reduce acidity.</p>
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">...</span>
                    </p>
                    <span id="more-content">
                    <p>
                    Additionally, Acidity’s Ayurvedic treatment contain ingredients like Amla (Indian gooseberry), Yashtimadhu (licorice), and Shankhpushpi (Convolvulus pluricaulis) may be recommended to support digestion and provide relief from acidity. These herbal remedies are believed to have a calming effect on the digestive system and help reduce the excessive production of stomach acid.
                    Ayurvedic treatment for acidity aims to provide long-lasting relief and promote overall digestive health through a holistic approach.

                    </p>
                    </span>
                    <div class="btn-ayurvedic">
                        <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                    </div>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div> </span>
            </div>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec our-expert" style="background-color:#f3f3f3" id="causes">
    <div class="container-fluid" >
        <div class="row cut-row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes <b>Acidity</b></h2>
                    <li><b>Excessive Stomach Acid Production: </b>When the stomach produces an excess amount of acid, it can lead to acidity. This can happen due to factors such as irregular eating patterns, overeating, or consuming spicy and oily foods.
                    </li>
                    <li><b>Improper Eating Habits: </b>
                    Unhealthy eating habits, like eating too quickly, skipping meals, or eating late at night, can disrupt the digestive process and contribute to acidity.
                    </li>
                    <li><b>Trigger Foods: </b>
                    Certain foods can trigger heartburn and acidity in susceptible individuals. Common culprits include citrus fruits, tomatoes, chocolate, caffeine, carbonated beverages, and fatty or fried foods.
                    </li>
                    <li><b>Stress and Lifestyle Factors: </b>
                     High levels of stress, inadequate sleep, and a sedentary lifestyle can all contribute to acidity by disrupting the normal functioning of the digestive system.
                    </li>
                    <li><b>Obesity: </b>Being overweight or obese increases the risk of experiencing acidity due to increased pressure on the stomach, leading to acid reflux.
                    </li>
                    <p>It's important to identify and avoid these triggers to manage acidity effectively. Making dietary and lifestyle modifications, along with acidity treatment from Dr. Sharada Ayurveda, can help address the root causes and provide relief from acidity symptoms.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>	

<div class="container-strip" style="background: #eeb03f";>
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz px-2.5" >
            <img src="{{ URL::asset('front/images/acidity-symptoms.webp') }}" class="img-fluid"alt="symptoms of acidity" title="symptoms of acidity">
        </div>
        <div class="cvn sym" >
            <h3>Symptoms of <span>Acidity</span></h3>
            <li><b>Heartburn: </b>A burning sensation felt in the chest area, often rising up to the throat, is a common symptom of acidity.
            </li>
            <li><b>Regurgitation: </b>
            The backflow of stomach acid into the throat or mouth can cause a sour or bitter taste.
            </li>
            <li><b>Dyspepsia: </b>
            Also known as indigestion, it refers to discomfort or pain in the upper abdomen, often accompanied by a feeling of fullness or bloating.
            </li>
            <li><b>Nausea: </b>
             Some individuals with acidity may experience a sensation of queasiness or an urge to vomit.
            </li>
            <li><b>Difficulty swallowing: </b>Acidity can cause a sensation of a lump or tightness in the throat, making swallowing challenging.
            </li>
            <li><b>Cough and hoarseness: </b>
             Stomach acid irritating the throat can lead to a persistent cough or hoarseness of voice.
            </li>
            <li><b>Chest pain: </b>In some cases, acidity can cause chest pain that may mimic heart-related conditions. It is essential to rule out any cardiac issues if experiencing chest pain.
            </li>
            <p>Recognizing these symptoms of acidity is crucial for timely intervention and seeking appropriate treatment, including Ayurvedic remedies, to alleviate discomfort and promote digestive health.
            </p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;"><b>Acidity</b> Ayurvedic Treatment</h2>
                 <li><b>Dietary modifications: </b>Ayurvedic treatment for acidity emphasises adopting a balanced and suitable diet. It includes consuming cooling and soothing foods like fresh fruits, vegetables, and herbal infusions to reduce acidity.
                </li>
                <li><b>Lifestyle changes: </b>
                Incorporating healthy lifestyle habits plays a crucial role in managing acidity. This includes practising stress management techniques, maintaining regular meal timings, and avoiding spicy, fried, and processed foods.
                </li>
                <li><b>Herbal remedies: </b>
                Ayurveda offers various herbal formulations and natural remedies for acidity. Ingredients like Amla (Indian gooseberry), Yashtimadhu (licorice), and Shankhpushpi (Convolvulus pluricaulis) are known for their digestive properties and can help reduce the excessive production of stomach acid.
                </li>
                <li><b>Panchakarma: </b>
                 Ayurvedic therapies such as Virechana (therapeutic purgation) or Vamana (therapeutic vomiting) may be recommended by an Ayurvedic practitioner for deep cleansing and balancing of the digestive system.
                </li>
                <li><b>Yoga and Pranayama: </b>Specific yoga asanas (postures) and pranayama (breathing exercises) can help alleviate acidity by improving digestion, reducing stress, and promoting overall well-being.
                </li>
                <br></br>
                <p>Ayurvedic treatment for acidity focuses on addressing the root cause of the condition by restoring the balance of the doshas and improving digestive fire. It aims to provide long-lasting relief and promote optimal digestive health through a holistic approach. It is recommended to consult with Dr. Sharada Ayurveda to receive personalised ayurvedic treatment based on your unique constitution and condition.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="KVBvuKRdSO0" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/KVBvuKRdSO0.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Patient's <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Mrs Sunita acidity patient" title="Mrs Sunita acidity patient" >
                            <h3 class="usrname">Mrs Sunita</h3>
                            <p class="desig">Acidity Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Many people who suffer with acidity turn to over-the-counter pills in the belief that they won't hurt them. With me, it was the same. I used to take a pill whenever I was feeling down. Later, I became aware of its negative effects. I began my medication program from Dr. Sharda Ayurveda, and after the procedure, I was completely free of acidity.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Harjeet acidity patient" title="Harjeet acidity patient">
                            <h3 class="usrname">Harjeet</h3>
                            <p class="desig">Acidity Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I began to feel symptoms like a lump in my throat, a burning sensation in my chest, and sleeping problems; I was unable to understand what was happening to me. I tried counseling with Dr. Sharda Ayurveda, and after a consultation, the medical professionals informed me that I had acidity. I began to receive their treatment, and within a couple weeks I was at calm and made a steady recovery.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Ravinder Acidity Patient" title="Ravinder Acidity Patient">
                            <h3 class="usrname">Ravinder</h3>
                            <p class="desig">Acidity Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                My burps used to be extremely frequent and even out of control. To be honest, I felt rather embarrassed. I begin by receiving treatment from Dr. Sharda Ayurveda for Acidity. My diet got changed, and I was prescribed some Ayurvedic remedies as well. I was able to fully recover from acidity.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1"><b>FAQ'S</b></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                How can I treat acidity naturally?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                            The best way to treat Acidity at home is to have a shot of Gooseberry (Amla) juice daily and coconut water empty stomach. Eating healthy and avoiding spicy food helps in treating Acidity. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                What is the best way to avoid acidity?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                            Few lifestyle and dietary changes help to treat acidity.
                            <ul>
                                <li>Quit Smoking and alcohol.</li>
                                <li>Avoid resting for at least 2 hours after intake of your meals.</li>
                                <li>Eat 2-3 meals than having a single heavy one.</li>
                                <li>Avoid tea and coffee. </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                What are the side- effects of medications for acidity?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                            Daily popping in a tablet for acidity is going to harm your body if not today then for sure in the future. Modern Medicines cause Constipation, diarrhea, nausea, and other diseases whereas Ayurvedicmedicines give no side- effects on health.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                What foods can choose to I eat to reduce my acidity?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                                Water-rich fruits and vegetables are a wise choice for stomach-related problems. Watermelons, apples, pomegranates, bananas, pears, coconut, papaya, kiwis, oranges, grapes, java plum, tomatoes, cucumber, beetroot, zucchini, broccoli, bitter gourd, and lotus stem are considered good for acidity. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                               Can Ayurveda get rid of acidity?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                           To treat acidity and improve digestion, Ayurveda suggests a number of treatments, dietary modifications, and lifestyle changes. To prevent acidity and other digestive problems, Ayurveda also advises against eating and drinking fermented foods and drinks.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    How ayurveda help in treating acidity?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                                Ayurveda work on treating an individual without any side effects on health. Ayurvedic medicine for acidity has life-long results as it does not only works on symptoms, it treats a disease from its root cause. With some dietary changes, one can easily recover from acidity.
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                What foods can increase my acidity?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                            All types of junk, oily, processed, highly spiced, stale, under, or overcooked can result in acid reflux. Apart from this, limit dairy products and non-vegetarian food as they support bacterial growth inside the body.  
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                What happens if acidity increases?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                            Too much acidity in the body causes inflammation, heart diseases, obesity, diabetes, and other chronic conditions. It can also lead to IBS.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                               What is the main problem of acidity?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                            In general, acidity is caused by the consumption of extra spicy foods, coffee, overeating or a low-fibre diet.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                How long does acid reflux attack last?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                            Most people suffer from frequent bouts of heartburn, burning pain behind the breastbone that moves up towards the neck. This happens when one usually consume heavy meals. Acid attack can sometimes lastfor as long as two hours.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Acidity",
    "item": "https://www.drshardaayurveda.com/acidity"  
  }]
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "How can I treat Acidity naturally?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The best way to treat Acidity at home is to have a shot of Gooseberry (Amla) juice daily and coconut water empty stomach. Eating healthy and avoiding spicy food helps in treating Acidity."
    }
  },{
    "@type": "Question",
    "name": "How to avoid getting Acidity?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Following Lifestyle and Dietary changes help to heal Acidity:  Quit Smoking and alcohol. Avoid resting for at least 2 hours after intake of your meals. Eat 2-3 meals than having a single heavy one. Avoid tea and coffee."
    }
  },{
    "@type": "Question",
    "name": "What are the side- effects of medications for Acidity?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Daily popping in a tablet for acidity is going to harm your body if not today then for sure in the future. Modern Medicines cause Constipation, diarrhea, nausea, and other diseases whereas Ayurvedic medicines give no side- effects on health. For best results choose Dr. Sharda Ayurveda best Ayurvedic clinic for acidity treatment."
    }
  },{
    "@type": "Question",
    "name": "What Dietary changes should I make for treating Acidity?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "•    Don’t skip your meals. Eat something healthy after every 2-3 hours. •   Food items that are hard to digest should be avoided at night. Avoid consumption of Alcohol. Follow workout routine religiously."
    }
  },{
    "@type": "Question",
    "name": "How to treat acidity immediately with a home remedy?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "•    Have fennel powder with a glass of lukewarm water. Chewing 1 cardamom pod per day reduces acidity, flatulence, and improves digestion."
    }
  },{
    "@type": "Question",
    "name": "How is Ayurvedic Medicine helpful in treating Acidity?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurvedic medicines work on treating an Individual without any side effects on health. Ayurvedic medicine for Acidity has life-long results as it does not only work on symptoms; it treats a disease from its root cause. With some Dietary changes, one can easily recover from Acidity."
    }
  },{
    "@type": "Question",
    "name": "Often heard that Ginger is good in Acidity. Is it?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ginger helps in reducing the chances of stomach acid that flows up to the esophagus. Ginger also helps in reducing inflammation and helps to relieves symptoms of acid reflux."
    }
  },{
    "@type": "Question",
    "name": "What happens if my body is too acidic?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Too much acidity in the body causes inflammation, heart disease, obesity, diabetes, and other chronic conditions. It can also lead to IBS."
    }
  },{
    "@type": "Question",
    "name": "How should I sleep when I have acidity?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "While suffering from Acidity, don't sleep on your right side. Sleep on your left side always. It is the best position found to reduce acid reflux from the body."
    }
  },{
    "@type": "Question",
    "name": "How long does acid reflux attack last?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Most people suffer from frequent bouts of heartburn, burning pain behind the breastbone that moves up towards the neck. This happens when one usually consume heavy meals. Acid attack can sometimes lastfor as long as two hours."
    }
  }]
}
</script>

@stop