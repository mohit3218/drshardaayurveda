<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Gout</h3>
                    <p style="font-size: 24px;">Protect Your Joints With Timely Treatment</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/gout">Gout</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/Gout.webp') }}" class="why-choose-us img-fluid" alt="Gout Treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Gout</b> Ayurvedic Treatment</h1>
                <p>
                Gout is a type of arthritis when uric acid increases in blood and there is sudden pain and inflammation in one joint. <a class="green-anchor" href="https://www.drshardaayurveda.com/uric-acid">Uric acid</a> is breakdown product of purines which are part of food which we eat. Development of Gout is more common in men in comparison to women. Chances of gout rises even with growing age. Women sometimes after <a class="green-anchor"href="https://www.drshardaayurveda.com/menopause">menopause</a> are affected by this disease. Women levels of uric acid increases in blood termed ashyperuricemia but all hyperuricemia patients will not develop gout. If parents are affected with gout then it maybe 20 percent chances of developing in next generation. There are some risk factors in male which causes gout like obesity, congestive heart failure, hypertension, diabetes andpoor kidney functions. The risk of gout is greater with alcohol intake and with food highlyrich in protein. An attack of gout sometimes occur out of sudden after waking up in the middle of night with a sensation of toes burning and the joint feels like burning, hot and swollen.
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                    <p>
                     Certain diuretics drugs and disease like hypothyroid can elevates levels of uric acid in body. Gout may occur again after several weeks or months if its treatment is not done timely. 
                    </p>
                    <div class="btn-ayurvedic">
                        <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                    </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Gout </b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Excess of uric acid in blood stream
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Urates crystals start forming and they get accumulated in joints causing inflammation and pain.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Over Eating
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                Intake of food before the digestion of the previous meal.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Obesity
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                                Overweight increases the risk of gout because overweight means over production of uric acid in the body.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Genetics
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                                Family history of gout increases the risk of gout in children.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Alcohol
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                                Excessive intake of Alcohol can causes gout.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/Gout-Symptoms.webp') }}" class="img-fluid"alt="Gout Symptoms">
        </div>
        <div class="cvn sym px-4" >
            <h3><span>Symptoms of</span> Gout  </h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Numbness in joint</p>
            <p>Insensibility to touch and numbness in seen in the affected joint.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Burning sensation on the affected part</p>
            <p>The affected joint has sensation of burning and irritation. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Restricted movement of Joint</p>
            <p>Unable to move joint normally hence leads to restricted movement.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Discomfort in Joints</p>
            <p>Long term discomfort in joints that linger over time if not detected timely. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Unbearable pain</p>
            <p>The pain in the joints sometimes becomes intolerable and may last up to 12 hours. </p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Benefits of <b>Ayurvedic Treatment for Gout </b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    A balanced healthy diet helps to reduce weight therefore reducing the burden on joints.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Follow a systematic daily way of life (Dincharya)
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Avoiding activities that cause both mental and physical stress. 
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Vasti is prescribed as one of the best treatment for Vataimbalance and thereby for gout followed by Ayurvedic medicines.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    With the usage of Ayurvedic medicines one can enhance the quality of life and decrease the usage of painkillers and steroids.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="UAT4pZY3Clg&t" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/UAT4pZY3Clg&t.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>    
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Harman Kaur</h3>
                            <p class="desig">Gout Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was diagnosed with gouty arthritis few months ago and started my treatment from Dr. Sharda Ayurveda. I could see the results in few days of treatment. They have best gouty arthritis treatment in Ayurveda
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Ram Kumar</h3>
                            <p class="desig">Gout Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                My uric acid used to be quite high and I got affected with Gouty arthritis. I began with Ayurvedic treatment for uric acid and Gouty arthritis together from Dr. Sharda Ayurveda. I took medicine for approximately 9 months and I was completely fine and healthy. A big thanks to Dr. Sharda for solving m health issues.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Manpreet Kaur</h3>
                            <p class="desig">Gout Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was suffering with gout since 3 years. I consulted many doctors but nothing was working. I got to know about Dr. Sharda Ayurveda from one of my friends who was taking medicines from them. I began with Ayurvedic Medicines for Gout from them. After every 3 months they get my Investigation done and I could see the result.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                What is fastest and easy way to get rid of gout?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                            <ul>
                                <li>Adopt protein restricted diet</li>
                                <li>Avoid non veg and eggs</li>
                                <li>Avoid alcohol</li>
                                <li>Quit Smoking </li>
                                <li>Exercise</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                How long does a gout flare up last?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                            An episode of gout can last for 3 days to 14 days. If not treated timely leads to worsening of pain and inflammation
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Why does gout pain worsens at night?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                            Due to decrease in levels of cortisol at night, lowers body temperature and dehydration, hence pain worsens at night.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Does lemon water help with Gout?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                            Lemon juice is useful remedy to help treat gout along with diet modifications and Ayurvedic treatment for gout. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    What are the causes of gout in Ayurveda?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                                The main cause of Vatarakta (gout) in Ayurveda are:
                                <ul>
                                    <li>Excessiveintake of Lavana (salty)</li>
                                    <li>Amla (Sour)</li>
                                    <li>Katu (pungent)</li>
                                    <li>Tikta (bitter)</li>
                                    <li>Kshara (alkaline)</li>
                                    <li>Snigdha (Too oily)</li>
                                    <li>Ushna (Too hot) foods</li>
                                </ul>
                                eating dried & preserved or spoiled fish/meat, Horse gram, Blackgram,sour curd or buttermilk, incompatible foods like fish with milk products, drinking alcohol, daytime sleep, staying awake in the night, anger etc.
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Does gout medicines have to take lifelong?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                            With Ayurvedic course of medicines followed by diet and Ayurvedic procedures a patient do not have to take medicine for life-long. It is curable in Ayurveda.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Why are men more affected by gout than women?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion4">
                        <div class="card-body">
                            Gout is more common in men than women.It is because estrogen lowersthe blood uric acid level that is why women suffer from gout after menopause.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                What are the risk factors for development of Gout?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                            Obesity, hypertension, diabetes, kidney disease, joint trauma and age are all risk factors for developing gout. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                Can any medication increase the risk of Gout?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                            Excessive intake of steroid based modern medicines increases the chances of Gout.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Can some essential oils help in treating Gout?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                            Yes, certain oil therapies in Ayurveda are known as treatment to cure gout. Consult your <strong>Ayurvedic Doctor</strong> for Gout and under their guidance one can start using them to get relief.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Gout",
    "item": "https://www.drshardaayurveda.com/gout"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What is fastest and easy way to get rid of gout?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "1.   Adopt protein-restricted diet
2.  Avoid non-veg and eggs
3.  Avoid alcohol
4.  Quit Smoking 
5.  Exercise"
    }
  },{
    "@type": "Question",
    "name": "How long does a gout flare up last?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "An episode of gout can last for 3 days to 14 days. If not treated timely leads to worsening of pain and inflammation."
    }
  },{
    "@type": "Question",
    "name": "Why does gout pain worsens at night?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Due to a decrease in levels of cortisol at night, lowers body temperature and dehydration, hence pain worsens at night."
    }
  },{
    "@type": "Question",
    "name": "Does lemon water help with Gout?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Lemon juice is a useful remedy to help treat gout along with diet modifications and Ayurvedic treatment for gout."
    }
  },{
    "@type": "Question",
    "name": "What are the causes of  gout in Ayurveda?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The main cause of Vatarakta (gout) in Ayurveda are
 1. excessive intake of Lavana (salty), 
2. Amla (Sour)
3. Katu (pungent)
4.  Tikta (bitter) 
5. Kshara (alkaline)
6. Snigdha (Too oily)
7. Ushna (Too hot) foods, 
eating dried & preserved or spoiled fish/meat, Horse gram, Blackgram, sour curd or buttermilk, incompatible foods like fish with milk products, drinking alcohol, daytime sleep, staying awake in the night, anger etc."
    }
  },{
    "@type": "Question",
    "name": "Does gout medicines have to take lifelong?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "With an Ayurvedic course of medicines followed by diet and Ayurvedic procedures, a patient does not have to take medicine for life-long. It is curable in Ayurveda."
    }
  },{
    "@type": "Question",
    "name": "Why are men more affected by gout than women?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Gout is more common in men than women. It is because estrogen lowers the blood uric acid level that is why women suffer from gout after menopause."
    }
  },{
    "@type": "Question",
    "name": "What are the risk factors for development of Gout?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Obesity, hypertension, diabetes, kidney disease, joint trauma and age are all risk factors for developing gout."
    }
  },{
    "@type": "Question",
    "name": "Can any medication increase the risk of Gout?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Excessive intake of steroid-based modern medicines increases the chances of Gout."
    }
  },{
    "@type": "Question",
    "name": "Can some essential oils help in treating Gout?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, certain oil therapies in Ayurveda are known as treatments to cure gout. Consult your Ayurvedic Doctor for Gout and under their guidance one can start using them to get relief."
    }
  }]
}
</script>
@stop