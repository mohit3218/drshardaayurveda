<?php
/**
 * rhinitis Page 
 * 
 * @created    09/02/2022
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2022
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4">Ayurvedic Treatment for Rhinitis (Pratishyaya Roga)</h3>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/rhinitis">Rhinitis</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION<span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a  href="#types">TYPES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatment">TREATMENT</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="definition">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc">
                    <img src="{{ URL::asset('front/images/rhinitis.webp') }}" class="why-choose-us img-fluid" alt="Rhinitis treatment" title="Rhinitis treatment">
                </div>
            </div>
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " >
                <h1 class="heading1">What is <b>Allergic Rhinitis?</b></h1>
                <p>
                In Ayurveda rhinitis is called “Pratishyaya Roga”. The disease is categorized under respiratory problem which causes the emergence of discomforting symptoms that are mainly produced by nasal mucous membrane. The word Rhinitis in general means the “inflammation of the nose”. The nasal cavity produces a thin and clear fluid called the mucus which is responsible to keep the dust, debris, and allergens away or out of the lungs by effectively trapping the particles. The symptoms occur as an outcome of a weak immune system reaction that leads to nasal congestion, runny nose, sneezing, and itching. In worse cases may significantly affect the eyes, ears, and throat. Rhinitis is categorized into various types accordingly to the severity of the condition. It is observed and diagnosed that a person who is already got trapped with respiratory diseases like asthma and sinusitis is more prone to develop rhinitis in the future.
                <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                <p>
                Ayurveda states that rhinitis is an outcome of imbalanced Kapha Dosha and also due to hypersensitivity of the nervous system to environmental allergies. The accumulation of ama (toxins) in the body can majorly hamper the defense system's functioning. But worry not as for the rescue Ayurvedic treatment has emerged as the strong and effective solution to get long-term results. The treatment for allergic rhinitis Ayurvedic treatment and non-allergic rhinitis are best available at Dr. Sharda Ayurveda. The treatment aims to cure the underlying cause which provides prevention from recurrence of disease symptoms in the future.
                </span>
                <div class="btn-ayurvedic">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn">Read more</button>
            </div>
        </div>
    </div>
</section>
<section class="dsa-india-ayurvedic-sec" id="types"> 
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="heading1">Types of <b>Rhinitis</b></h2>
                <p font align="center">Rhinitis is categorized into two types which are:</p>
            </div>
        </div>
        <div class="row cust-iba">
            <div class="owl-carousel owl-theme" >
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/allergic-rhinitis.webp') }}" class="card-img-top lazyload" alt="women suffering from allergic rhinitis" title="women suffering from allergic rhinitis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Allergic rhinitis</h4>
                            <p class="card-text">
                                It’s an immunoglobulin E mediated inflammatory chronic disease that affects the nasal mucosa caused by environmental allergens. The visible symptoms are itching, sneezing, and chest congestion.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/non-allergic-rhinitis.webp') }}"  alt="women suffering from Non-allergic rhinitis" title="women suffering from Non-allergic rhinitis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Non-allergic rhinitis</h4>
                            <p class="card-text">
                                The inflammation of the inside layer of the nose. It is not caused by the action of allergens. The main symptoms are nasal blockage, congestion, and runny nose.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

<section class="our-product causes" style="background-color:#f3f3f3" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate">
                    <h2 class="heading1">Causes of <b>Rhinitis</b></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Allergens
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Some people who have a weak immune system are more prone to be infected with allergies as compared to others. The common allergens are molds, dust, and pollens.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Temperature Change
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                The seasonal variations are the self invitation for many allergic respiratory diseases. They can potentially trigger the membrane inside the nose to swell and cause a stuffy runny nose.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Hormonal Change
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                The major reason for the emergence of non-allergic rhinitis is hormonal misbalance. The main phases where there are constant hormonal changes are during pregnancy, menstruation, and thyroid.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Diet
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">
                                Diet plays a crucial role which helps in keeping diseases at bay. Eating spicy foods and consuming alcoholic beverages can be harmful as it causes the membrane inside the nose to swell leading to nasal congestion.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Infection
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                            <div class="card-body">
                                Viruses and bacteria are infectious agents that can lead to the development of non-allergic rhinitis.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>			


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a free Assessment Report of Allergic Rhinitis Ayurvedic Treatment</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="advantages">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/rhinitis-symptoms.webp') }}" class="img-fluid" alt="Rhinitis symptoms" title="Rhinitis symptoms">
        </div>
        <div class="cvn sym">
            <h3>Symptoms of <span>Allergic Rhinitis</span></h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Blocked Nose</p>
            <p>Mainly the blockage of nasal passages can cause other issues like difficulty in breathing.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Runny Nose</p>
            <p>The constant secretion of mucous from the nasal cavity is also a highlighting sign of rhinitis.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Snoring</p>
            <p>It is described as a rattling noise made when a person breathes during sleep. It occurs majorly due to a blocked nose.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Itchy Skin</p>
            <p>The appearance of red and itchy patches on the skin is also a notable symptom of this disorder.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Increased Heart Rate</p>
            <p>Due to difficulty in breathing a person’s heart rate fluctuates and also a person experiences frequent chest pain and tightness.</p>
        </div>
    </div>
</div>
<div class="split"></div>
<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " >
                <h2 class="client-test-title"style="font-size: 24px !important;"><b>Rhinitis</b> Ayurvedic Treatments</h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                     Panchakarma therapy is the most effective solution as it helps provide recovery from the disease in the best and most natural way.
                    <li>In Shirodhara, the medicated oil is poured on your forehead.</li>
                    <li>Nasya, where gentle massage on the forehead and neck with medicated aromatic oil promotes the release of mucous from congested nose followed by steam therapy.</li>
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Practicing yoga and meditation daily aims to provide benefits that help recover well from the disease. Some of the best poses are Ardha chandrasana, Virabhadrasana, and Sarvangasana.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Certain modifications in the diet can definitely benefit in providing early and effective recovery from the disease. Some best dietary changes suggested by Ayurveda are avoiding processed food  & dairy products and consuming more lukewarm water & fresh fruits and vegetables.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Consuming herbal tea such as basil and mint can provide relief from a congested nose.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    If the condition is chronic the procedure Vamana and Virechana as part of Panchakarma therapy can help balance dominating Doshas of the body thus, healing the disease from its underlying cause.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="ob52MQSABk0" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/ob52MQSABk0.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="dsa-why-choose" id="testimonials">
    <div class="container-fluid">
        <h2 text align="center"style="font-size: 32px !important;">Kind Words of Rhinitis Patients -<strong>Dr. Sharda Ayurveda</strong></h2>
        <br>    
        
        <div class="row cut-row" >
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div id="dna_video">
                    <div class="youtube-player" data-id="gYv-IX46VKI" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                        <img src="{{ URL::asset('front/images/gYv-IX46VKI.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                        <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div id="dna_video">
                    <div class="youtube-player" data-id="aVEuOC31FrI" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                        <img src="{{ URL::asset('front/images/aVEuOC31FrI.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                        <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Patient's <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Tushar Testimonial" title="Tushar Testimonial">
                            <h3 class="usrname">Tushar</h3>
                            <p class="desig">Allergic Rhinitis Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                When I get to know that I am suffering from chronic rhinitis I could not breathe properly and was severely facing difficulty while sleeping due to a congested nose. I consulted various specialists but the results were unsatisfactory and with time worried me. There I consulted Dr. Sharda Ayurveda after knowing that they have thousands of effectively treated patients of respiratory diseases. I was astonished to observe that with just 1 month of the medication I could feel the difference in my health. With regular Ayurvedic treatment along with diet and lifestyle changes for about 5 months, I am now at the recovery stage. I am thankful to Dr. Sharda Ayurveda for their effective Ayurvedic treatment.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Gurbhajan Singh Testimonial" title="Gurbhajan Singh Testimonial">
                            <h3 class="usrname">Gurbhajan Singh</h3>
                            <p class="desig">Rhinitis Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was suffering from allergic rhinitis for the past 2 years. Even after consulting many doctors, I could not find any satisfactory results. Therefore as a recommendation from my friend I visited Dr. Sharda Ayurveda where within just 4 months of allergic rhinitis Ayurvedic treatment along with lifestyle and dietary changes suggested by the experts I am recovering well. I would highly recommend Dr. Sharda Ayurveda to everyone.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Vijender Singh Testimonial" title="Vijender Singh Testimonial" >
                            <h3 class="usrname">Vijender Singh</h3>
                            <p class="desig">Rhinitis Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Whenever the season used to change, I was always in a terrible state. The constant sneezing and runny nose used to disturb me a lot. Also, I faced issues while breathing. All other treatments gave me so many side effects. I looked for the best Ayurvedic doctor and consulted Dr. Sharda Ayurveda. Since then my condition improved and now I can breathe normally.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <i class="fa" aria-hidden="true"></i>
                            Can allergic rhinitis cause other health problems?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            Yes, it can cause other health concerns. The disease can cause certain complications including recurrent sore throat, headache, ear and nose infection, and disturbed sleeping patterns. To avoid complications choose rhinitis treatment from Dr. Sharda Ayurveda for effective results.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                What is the best treatment for rhinitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            Ayurvedic treatment is considered the best for treating rhinitis. Ayurveda is a traditional practice whose recommendations are followed today also. If you are suffering from allergic and non-allergic rhinitis then Dr. Sharda's Ayurvedic treatment can be a boon in providing you positive recovery. Along with the herbal medications the experts suggest diet and lifestyle changes that aid in natural recovery.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Is rhinitis a transferable disease?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            Yes, non-allergic rhinitis can be transferred from the affected person to another person who comes in direct contact.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Does stress cause a flare-up of the disease? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                            Stress is the major cause of many diseases one of which is rhinitis. The research believes that stress is associated with allergic rhinitis. It can negatively impact your body. So it is necessary to have good mental health to avoid emergence and flaring-up of chronic disease.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFive" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <i class="fa" aria-hidden="true"></i>
                                Is turmeric good for treating allergic rhinitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="card-body">
                            Turmeric is an essentially important Ayurvedic herb that is greatly known for its anti-inflammatory property. It is seen that the active ingredient present in it is curcumin, which is linked with reducing the symptoms of inflammation-driven diseases and thereby helps in minimizing swelling and irritation caused by allergic rhinitis.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Does Vitamin C help to treat allergies?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                        <div class="card-body">
                            Vitamin C is a potent antioxidant that helps protect your cells from damage thereby reducing the severity of the disease. If consumed essentially during allergy season, it slows down the overreaction of the body to environmental triggers by decreasing the body’s histamine production.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Who are at higher risk of developing rhinitis? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                        <div class="card-body">
                            The person already suffering from any respiratory disease like asthma and skin diseases like eczema. Parental history or genetics is also considered a well-known risk factor.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                What food items are to be avoided to prevent rhinitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                        <div class="card-body">
                            The food items that are to be avoided which are considered as the potential allergens that induce allergic rhinitis are - Rice, citrus fruits, black grams, and bananas.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                What happens if the disease is left untreated?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                        <div class="card-body">
                            If the disease is left untreated, it often becomes chronic and may lead to serious health complications including chronic nasal inflammation and obstruction. This seriously affects your nasal airways which causes shortness of breath and dizziness.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                What lifestyle changes helps in recovery from the disease?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                        <div class="card-body">
                            The lifestyle changes that help in recovery are:
                            <li>Perform regular exercise.</li>
                            <li>Stabilize your mental health.</li>
                            <li>Maintain a better sleeping pattern.</li>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<style>
    .carousel-inner {position: relative;width: 100%;overflow: hidden;}
    .carousel-item {position: relative;float: left;width: 100%;margin-right: -100%;-webkit-backface-visibility: hidden;backface-visibility: hidden;transition: -webkit-transform .6s ease-in-out;transition: transform .6s ease-in-out;transition: transform .6s ease-in-out,-webkit-transform .6s ease-in-out;}
    .card-body {padding-top: 1.5rem;padding-bottom: 1.5rem;-webkit-border-radius: 0!important;border-radius: 0!important;flex: 1 1 auto;-ms-flex: 1 1 auto;}
    .card .card-body a h3{font-size: 18px;color: #ff9000;line-height: 1.4;}
    .card .card-body h3{color: #373433;font-size: 18px;    line-height: 1.4;}
    .font-weight-bold {font-weight: 700!important;}
    .card-title {margin-bottom: .75rem;}
    .card-img-top {width: 100%;border-top-left-radius: calc(.25rem - 1px);border-top-right-radius: calc(.25rem - 1px);}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            responsiveClass: !0,
            navigation : true,
            autoplay: true,
            loop: true,
            smartSpeed: 2500,
            singleItem : true,
            navigationText: ["<i class='fa fa-angle-left fa-2'></i>", "<i class='fa fa-angle-right fa-2'></i>"],
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                480: {
                    items: 1,
                    nav: !1
                },
                768: {
                    items: 2,
                    nav: !1
                },
                1000: {
                    items: 2,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1024: {
                    items: 3,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1366: {
                    items: 3,
                    nav: !1
                }
            }
        })
    });

</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Rhinitis",
    "item": "https://www.drshardaayurveda.com/rhinitis"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "Can allergic rhinitis cause other health problems?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it can cause other health concerns. The disease can cause certain complications including recurrent sore throat, headache, ear and nose infection, and disturbed sleeping patterns. To avoid complications choose rhinitis treatment from Dr. Sharda Ayurveda for effective results."
    }
  },{
    "@type": "Question",
    "name": "What is the best treatment for rhinitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurvedic treatment is considered the best for treating rhinitis. Ayurveda is a traditional practice whose recommendations are followed today also. If you are suffering from allergic and non-allergic rhinitis then Dr. Sharda's Ayurvedic treatment can be a boon in providing you positive recovery. Along with the herbal medications the experts suggest diet and lifestyle changes that aid in natural recovery."
    }
  },{
    "@type": "Question",
    "name": "Is rhinitis a transferrable disease?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, non-allergic rhinitis can be transferred from the affected person to another person who comes in direct contact."
    }
  },{
    "@type": "Question",
    "name": "Does stress cause a flare-up of the disease?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Stress is the major cause of many diseases one of which is rhinitis. The research believes that stress is associated with allergic rhinitis. It can negatively impact your body. So it is necessary to have good mental health to avoid the emergence and flaring-up of chronic disease."
    }
  },{
    "@type": "Question",
    "name": "Is turmeric good for treating allergic rhinitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Turmeric is an essentially important Ayurvedic herb that is greatly known for its anti-inflammatory property. It is seen that the active ingredient present in it is curcumin, which is linked with reducing the symptoms of inflammation-driven diseases and thereby helps in minimizing swelling and irritation caused by allergic rhinitis."
    }
  },{
    "@type": "Question",
    "name": "Does Vitamin C help to treat allergies?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Vitamin C is a potent antioxidant that helps protect your cells from damage thereby reducing the severity of the disease. If consumed essentially during allergy season, it slows down the overreaction of the body to environmental triggers by decreasing the body’s histamine production."
    }
  },{
    "@type": "Question",
    "name": "Who are at higher risk of developing rhinitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The person already suffering from any respiratory disease like asthma and skin diseases like eczema. Parental history or genetics is also considered a well-known risk factor."
    }
  },{
    "@type": "Question",
    "name": "What food items are to be avoided to prevent rhinitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The food items that are to be avoided which are considered as the potential allergens that induce allergic rhinitis are - Rice, citrus fruits, black grams, and bananas."
    }
  },{
    "@type": "Question",
    "name": "What happens if the disease is left untreated?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If the disease is left untreated, it often becomes chronic and may lead to serious health complications including chronic nasal inflammation and obstruction. This seriously affects your nasal airways which causes shortness of breath and dizziness."
    }
  },{
    "@type": "Question",
    "name": "What lifestyle changes helps in recovery from the disease?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The lifestyle changes that help in recovery are:
•   Perform regular exercise. 
•   Stabilize your mental health. 
•   Maintain a better sleeping pattern."
    }
  }]
}
</script>

@stop