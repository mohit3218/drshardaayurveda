<?php
/**
 * DRUG DE-ADDICTION Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Drug De-Addiction</h3>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/drug-de-addiction">Drug De-Addiction</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/Drug-De-Addiction.webp') }}" class="why-choose-us img-fluid" alt="Drug-De-Addiction">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1">Ayurveda For <b>Drug De-Addiction</b></h1>
                <p>
                We are well aware of the ugly side of any kind of addiction whether it is alcohol, smoking, or drug addiction and one of the common and serious forms of addiction is Drug Addiction. Drug addiction has affected a large number of people in society, especially at a young age. In addition to, it affects the health of an individual adversely, addiction also affects the family, society, and nation. The sufferer of drug addiction hunts for drugs they are addicted to everywhere and use them obsessively despite knowing that it is destroying their living and lives. They are very well aware of the fact that it is not only harmful to them but also to those who are around them. <strong>Drug addiction treatment in Ayurveda</strong> varies for every individual depending on their bodily constitution i.e. Vata, Pitta, Kapha. Drug addiction Ayurvedic medicines aim to flush out the toxins from the body, restore internal tissues, and helps in pulling out from the darkness of addiction.
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">...</span>
                </p>
                <span id="more-content">
                    <p>
                    An addicted individual knowingly spends a lot of money, out of their control and limits that sometimes even exceed what they can afford causing financial, physical, and family problems. All the time is just given to drugs and in hunt of them.
                    </p>
                    <div class="btn-ayurvedic">
                        <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                    </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Drug Addiction </b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Mental Stress
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                            A stressed person is prone to addiction to drugs.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Unhappy Lifestyle
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            Unable to live a lifestyle that a person wants, puts them into depression and makes them addicted.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Lack of Family Involvement
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                            When the family does not cooperate and is put under restrictions always, a person follows the wrong path.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Family History of Drugs Addiction
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                            Drug addiction in children is more common as it involves genetic predisposition.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                Environmental Cause
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                            Environmental changes play a vital role in developing drug dependence as the environment influences behavior.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/Drug-De-Addiction-symptoms.webp') }}" class="img-fluid"alt="Drug addiction symptoms">
        </div>
        <div class="cvn sym" >
            <h3><span>Symptoms of</span> Drug Addiction</h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Loss Of Appetite</p>
            <p>A person never feels like eating anything as they only want drugs only in their meals themselves.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Sleep Disorder</p>
            <p>It becomes hard for an individual to have a sound deep sleep. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Poor Concentration</p>
            <p>An Individual is unable to memorize anything as drugs excessive consumption had to weaken their nerves. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Joint Pain</p>
            <p>After a certain limit when all the toxins get deposited it starts with pain in joints. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Fatigue</p>
            <p>A person feels tired, restless, and not feeling like doing anything. </p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Benefits of Ayurvedic <b>Treatment for Drug De-Addiction </b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Diet plays a vital role in the road to recovery from drug addiction. Experts of Dr. Sharda Ayurveda will prepare a special Ayurvedic diet that will help in early recovery.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Panchakarma is highly effective to treat drug de-addiction. Panchakarma helps in detoxifying the body and removing all negative conditions
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurveda is a storehouse of effective herbs that are especially for de-addiction. Ayurvedic treatment is always recommended as it can be given even without letting the patient know about it.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Yoga is itself said to be a <a class="green-anchor"href="https://www.drshardaayurveda.com/blogs/ayurvedic-treatment-for-de-addiction-of-drug-and-alcohol">treatment for drug de-addiction</a> as the chemicals consumed by the drug addicts cause wreak havoc on the brain ongoing the phase of recovery. By adopting yoga and other exercises, an addict can regenerate endorphin chemicals in the brain
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Meditation helps in boosting the body, mind, and soul and makes the body fresh and healthy.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="jWlfQMinrFQ" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/drug-de-addiction-video-thumbnail.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Rajat Kumar</h3>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                My husband was into drug addiction for 4 years. I was very worried about his health and all the finances were being spread on him as he used to stay unwell. I got to know about Ayurvedic treatment for drug addiction from Dr. Sharda Ayurveda. I consulted the expert and just within 12 days of medicines my husband was all comfortable if he did not find drugs around them and that was a big satisfaction for me. I would suggest everyone adopt Ayurveda for drug de-addiction treatment from Dr. Sharda Ayurveda. They won’t break your trust.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                            How does Ayurveda deal with de-addiction?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                        Ayurveda catches the problem to work on the root cause to treat it, not just the symptoms of a person. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                How to deal with drug addict patients?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                        Proper counseling is a must that provides emotional support followed by Ayurvedic medicines that helps to treat the drug addict patient
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Do drug-addicted patients always stay in anger?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                        A person highly dependent on drugs mostly remains hyper excited, irritated, has low memory power, and is also sometimes unable to balance while walking and talking.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                How does dependency on drugs affect personal life?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                        There is a loss in sexual desire and also the high dosage of drugs reduces the capability of thinking and working hence affects the personal life of a patient.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                How quickly one can get addicted to drugs?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                        How quickly one gets addicted to drugs varies from person to person. It depends upon several factors including your biology i.e. genes, age, gender, environmental conditions. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    Is there an effective treatment for drug de-addiction in Ayurveda?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                            Drug addiction can be treated by some behavioral therapies. Treatment varies from person to person depending upon the drugs they are taking and for how long. Treatment is advised according to body type. Ayurveda has the most effective, safe treatment.
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Can drug addiction leads to mental disorders?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                        Yes, it can because a person gets in depression and anxiety attacks if they do not get their needs on time. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Can one get addicted easily to a drug if they have it just for once?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                        Yes, one can depend on a person’s ability. Younger age children intake it just for a taste or to try and when it becomes a habit they get unaware of it. This happens due to the change in a person’s brain each time they consume them.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                My friend is addicted to drugs for 7-8 months. Can Ayurveda help him getting rid of it?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                        Yes, Ayurveda can very easily help him in getting rid of it. The best advantage of Ayurvedic medicine is that you can give it without letting the patient know about it. Visit the nearest drug addiction treatment centers and find a way out for your friend. 

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Drug De-Addiction",
    "item": "https://www.drshardaayurveda.com/drug"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "How does Ayurveda deal with de-addiction?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurveda catches the problem to work on the root cause to treat it, not just the symptoms of a person."
    }
  },{
    "@type": "Question",
    "name": "How to deal with drug addict patients?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Proper counseling is a must that provides emotional support followed by Ayurvedic medicines that helps to treat the drug addict patient."
    }
  },{
    "@type": "Question",
    "name": "Do drug-addicted patients always stay in anger?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "A person highly dependent on drugs mostly remains hyper excited, irritated, has low memory power, and is also sometimes unable to balance while walking and talking."
    }
  },{
    "@type": "Question",
    "name": "How does dependency on drugs affect personal life?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "There is a loss in sexual desire and also the high dosage of drugs reduces the capability of thinking and working hence affects the personal life of a patient."
    }
  },{
    "@type": "Question",
    "name": "How quickly one can get addicted to drugs?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "How quickly one gets addicted to drugs varies from person to person. It depends upon several factors including your biology i.e. genes, age, gender, environmental conditions."
    }
  },{
    "@type": "Question",
    "name": "Is there an effective treatment for drug de-addiction in Ayurveda?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Drug addiction can be treated by some behavioral therapies. Treatment varies from person to person depending upon the drugs they are taking and for how long. Treatment is advised according to body type. Ayurveda has the most effective, safe treatment."
    }
  },{
    "@type": "Question",
    "name": "Can drug addiction leads to mental disorders?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it can because a person gets in depression and anxiety attacks if they do not get their needs on time."
    }
  },{
    "@type": "Question",
    "name": "Can one get addicted easily to a drug if they have it just for once?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, one can depend on a person’s ability. Younger age children intake it just for a taste or to try and when it becomes a habit they get unaware of it. This happens due to the change in a person’s brain each time they consume them."
    }
  },{
    "@type": "Question",
    "name": "My friend is addicted to drugs for 7-8 months. Can Ayurveda help him getting rid of it?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, Ayurveda can very easily help him in getting rid of it. The best advantage of Ayurvedic medicine is that you can give it without letting the patient know about it. 
Visit the nearest drug addiction treatment centers and find a way out for your friend."
    }
  }]
}
</script>
@stop