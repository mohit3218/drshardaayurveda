<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Sinusitis (Pinas)</h3>
                    <p style="font-size: 22px;">Breath Healthy with Ayurveda</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/sinusitis">Sinusitis</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a  href="#types">TYPES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatment">TREATMENT</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="definition">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/Sinusitis-treatment.webp') }}" class="why-choose-us img-fluid" alt="women suffering from sinusitis" title="women suffering from sinusitis">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1">What is <b>Sinusitis?</b></h1>
                <p>
                Sinusitis is known as Pinas in Ayurveda. Sinuses are the hollow cavities that are present in the region of the skull. In this condition, the sinuses become inflamed and are blocked with mucus which causes congestion in the nasal passage. Sinusitis treatment in Ayurveda has a holistic approach to healing. 
                As per Ayurveda, Kapha (water) is accumulated in the sinuses and blocks the flow of Vata (air).  When the Pitta (fire) is imbalanced it results in swelling of the tissues around the sinuses.
                Other factors which contribute are suppressing natural urges, and intake of excess oily, or processed foods.  Ayurvedic treatment for sinus infection aims to remove the root cause of the disorder and provides long-term relief.  
<!--                 <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                <p>
                </p></span> -->
                <div class="btn-ayurvedic">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
                <!-- <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button> -->
            </div>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec" id="types"> 
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="heading1">Types of <b>Sinusitis</b></h2>
                <p font align="center">Sinusitis treatment in Ayurveda follows a purely natural approach. But before understanding how Ayurveda works, let’s discuss the types of sinusitis.</p>
            </div>
        </div>
        <div class="row cust-iba">
            <div class="owl-carousel owl-theme" >
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/acute-sinusitis.webp') }}" class="card-img-top lazyload" alt="women suffering from Acute sinusitis" title="women suffering from Acute sinusitis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Acute sinusitis</h4>
                            <p class="card-text">
                                Acute sinusitis lasts up to around 4 weeks or less and develops rapidly. The main cause can be viral infections or seasonal allergies.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/subacute-sinusitis.webp') }}"  alt="man suffering from Subacute sinusitis" title="man suffering from Subacute sinusitis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Subacute sinusitis</h4>
                            <p class="card-text">
                                Subacute sinusitis is a transition phase from acute to chronic sinusitis. The duration can be in between 4 - 8 weeks. Seasonal allergies or bacterial infections can result in this type.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/chronic-sinusitis.webp') }}" alt="Women suffering from Chronic sinusitis" title="Women suffering from Chronic sinusitis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Chronic sinusitis</h4>
                            <p class="card-text">Chronic sinusitis lasts for more than 12 weeks. This can be due to persistent allergies, bacterial infections,  or deformation of the structure of the nasal region. Three months is a long time, so Ayurveda is recommended for chronic sinusitis treatment.</p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/recurrent-sinusitis.webp') }}"  alt="Man suffering from Recurrent sinusitis" title="Man suffering from Recurrent sinusitis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Recurrent sinusitis</h4>
                            <p class="card-text">
                                In recurrent sinusitis, at least four episodes of sinusitis occur in one year and each episode must last for at least 7 days.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

<section class="our-product causes" style="background-color:#f3f3f3" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Sinusitis</b></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Immunity
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                            Immunity plays a major role as a weak immune system can result in catching colds, and flu easily. Hence, resulting in sinus blockage.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Respiratory Tract Infection
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            Infections present in the respiratory tract can also flare up sinusitis. Asthma often acts as a trigger for sinusitis.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Seasons Allergies
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                            People are often affected by seasonal changes. The cold can lead to sinus congestion, which can linger in the sinuses. As a result, bacterial infections develop.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Nasal Polyps
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                            The extra tissue growth in the nasal region can block the sinuses which can result in sinusitis.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                Deviated Septum
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                            The structural deformation of the septum can impair the drainage of the sinuses. Deviated septum or crooked septum leads to blockage of the sinuses. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                Exposure to smoking
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                            Smoking or exposure to cigarette smoke blocks sinus cavities.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-strip" style="background: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report of Sinusitis Ayurvedic Treatment</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<section class="our-product causes" style="background-color:#f8e6c5" id="symptoms">
    <h2 class="heading1">Symptoms of <b>Sinusitis (Pinas)</b></h2>
    <div class="wrp pt-5 mt-5 pb-5" style="padding-top: 0rem!important";>
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/Sinusitis-symptoms.webp') }}" class="img-fluid"alt="sinusitis symptoms" title="sinusitis symptoms">
        </div>
        <div class="cvn sym" >
            <h3>Symptoms of <span>Sinusitis</span></h3>
            <li>Runny nose with thick, discolored discharge.</li>
            <li>Blocked nose causing difficulty in breathing.</li>
            <li>Reduced sense of taste and smell.</li>
            <li>Postnasal drip is the drainage of mucus down the throat.</li>
            <li>Inflammation around eyes, nose, and forehead.</li>
            <li>Feeling facial pressure and pain in the facial region.</li>
            <li>Episodes of headache.</li>
            <li>Halitosis, i.e., bad breath or taste in the mouth</li>
            <li>Extreme fatigue</li>
            <li>Sore throat</li>        
        </div>
    </div>
</div>
<div class="split"></div>
</section>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;"><b>Sinusitis</b> Treatment in Ayurveda</h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Panchakarma Therapy is considered the best sinus ayurvedic treatment as ancient practices are involved which provide immense relief to patients suffering from sinusitis. Ayurvedic herbs derived from nature act in favor of the body and give no side effects as they are plant-based. 
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Adopting a Satvic diet which includes fresh fruits, vegetables, and raw food instead of nonvegetarian and processed food does wonders for the digestive system.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    An active lifestyle that includes walking, jogging, and other physical activities helps to increase the oxygen level in the body which improves the functions of the respiratory system.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Yoga asanas rejuvenate the body, mind, and soul. Additionally, physical and mental stress is released which results in a soothing effect.
                </p>                
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Meditation is encouraged to boost the activity of the brain. At the same time, stress is relieved from the nerve channels of the body.
                </p>                
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="xFGNXX3DAhM" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/xFGNXX3DAhM.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">
                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Sinusitis Patient's <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Rubail Testimonial" Title=" Rubail Testimonial">
                            <h3 class="usrname">Rubail </h3>
                            <p class="desig">Sinusitis Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Myself Rubail was suffering from sinusitis for the past 2-3 years. I was experiencing constant coughing and sneezing that were more than enough to make my life miserable. Even after consulting specialists, sadly, none of the treatments and medications show positive results. But thanks to Dr. Sharda Ayurveda as the Ayurvedic doctor's guidance just provide relief within just 7 months. I would highly recommend everyone to consult Dr. Sharda Ayurveda. 
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/gurwinder-kaur-testimonial.webp') }}" class="img-fluid" alt="Gurwinder Kaur Testimonial" >
                            <h3 class="usrname">Gurwinder Kaur</h3>
                            <p class="desig">Sinusitis Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was suffering from Sinusitis. Despite of taking several medicines nothing was working for me. I was in terrible state, but ever since I started with sinusitis ayurvedic treatment from Dr. Sharda Ayurveda I am in such a relief. With the guidance and suggestions of the experts my sinusitis healed magically! I am glad finally I am back to my normal life.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt=" Sinusitis Testimonial" >
                            <h3 class="usrname">Mrs. Rajvinder Kaur</h3>
                            <p class="desig">Sinusitis Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Even after getting treatment from various doctors for my Sinus problem, I wasn't getting any satisfactory results. So I consulted Dr. Sharda Ayurveda where I was guided well by the experts and within 2 months of the ayurvedic treatment of sinusitis, I was completely cured of the problem. I am very thankful to Dr.Sharda Ayurveda for their best treatment.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                               What triggers sinusitis?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                        The presence of causative factors like viruses, bacteria, chemical irritants, or pollution, can trigger conditions. 
                        A history of seasonal allergies, pollen, pet dander, nasal polyps, and deviated nasal septum also leads to sinusitis.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                How is turmeric useful for the sinuses?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                        Turmeric has anti-inflammatory properties that help in decreasing the inflammation which is responsible for the narrowing of the nasal passage. 
                        <li>1 tsp. of turmeric can be added to the milk and consumed for getting rid of the symptoms.</li> 

                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Can Ayurveda cure sinusitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                        Ayurveda offers holistic ways to cure sinusitis. Its natural remedies followed by Ayurvedic medicines not only relieve the symptoms of sinusitis but offers life-long benefits that too without any side effects.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                How long does it take for a sinus infection to clear on its own?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                        Usually, the viral infection goes away on its own within 10-14 days if the proper sinusitis Ayurvedic medicines are consumed.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                What treatment is usually recommended to treat sinusitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                        Ayurvedic Treatment for sinusitis is the best way to heal sinusitis. As in Ayurvedic treatment, surgery is never recommended.
                        Nose drops or oral medicines are recommended to help reduce congestion and other    discomforting signs.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    What happens if the sinus is left untreated?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                            Sinusitis if left untreated causes unnecessary pain and discomfort. A patient usually complains of a runny nose after every alternate day. 
                            Bacterial and viral infections when persisting for long can result in congestion. In severe cases, it may lead to meningitis.
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                 What are the foods to be avoided in Sinus?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                        Food like dairy products, processed sugar, refined carbohydrates, and cold/stale food have the chance of increasing sinusitis symptoms.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                What is the fastest way to cure sinusitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                        An effective way to heal from the symptoms of sinusitis is to take steam. Add tulsi leaves to the boiling water. Steam will clear the blockage and help in draining the excess mucus.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                How do I permanently stop sinusitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                        Ayurvedic treatment for sinus infection removes the root cause of the disease by natural means. It gives permanent relief and gives no side effects.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Which part of the body is affected by sinusitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                        Sinuses are hollow cavities present around the cheekbones, forehead, in between the eyes, and behind the nose. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')
@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Sinusitis",
    "item": "https://www.drshardaayurveda.com/sinusitis"  
  }]
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What triggers sinusitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The presence of causative factors like viruses, bacteria, chemical irritants, or pollution, can trigger conditions. A history of seasonal allergies, pollen, pet dander, nasal polyps, and deviated nasal septum also leads to sinusitis."
    }
  },{
    "@type": "Question",
    "name": "How is turmeric useful for the sinuses?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Turmeric has anti-inflammatory properties that help in decreasing the inflammation which is responsible for the narrowing of the nasal passage. 1 tsp. of turmeric can be added to the milk and consumed for getting rid of the symptoms."
    }
  },{
    "@type": "Question",
    "name": "Can Ayurveda cure sinusitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurveda offers holistic ways to cure sinusitis. Its natural remedies followed by Ayurvedic medicines not only relieve the symptoms of sinusitis but offers life-long benefits that too without any side effects."
    }
  },{
    "@type": "Question",
    "name": "How long does it take for a sinus infection to clear on its own?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Usually, the viral infection goes away on its own within 10-14 days if the proper sinusitis Ayurvedic medicines are consumed."
    }
  },{
    "@type": "Question",
    "name": "What treatment is usually recommended to treat sinusitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurvedic Treatment for sinusitis is the best way to heal sinusitis. As in Ayurvedic treatment, surgery is never recommended. Nose drops or oral medicines are recommended to help reduce congestion and other discomforting signs."
    }
  },{
    "@type": "Question",
    "name": "What happens if the sinus is left untreated?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Sinusitis if left untreated causes unnecessary pain and discomfort. A patient usually complains of a runny nose after every alternate day. Bacterial and viral infections when persisting for long can result in congestion. In severe cases, it may lead to meningitis."
    }
  },{
    "@type": "Question",
    "name": "What are the foods to be avoided in Sinus?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Food like dairy products, processed sugar, refined carbohydrates, and cold/stale food have the chance of increasing sinusitis symptoms."
    }
  },{
    "@type": "Question",
    "name": "What is the fastest way to cure sinusitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "An effective way to heal from the symptoms of sinusitis is to take steam. Add tulsi leaves to the boiling water. Steam will clear the blockage and help in draining the excess mucus."
    }
  },{
    "@type": "Question",
    "name": "How do I permanently stop sinusitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurvedic treatment for sinus infection removes the root cause of the disease by natural means. It gives permanent relief and gives no side effects."
    }
  },{
    "@type": "Question",
    "name": "Which part of the body is affected by sinusitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Sinuses are hollow cavities present around the cheekbones, forehead, in between the eyes, and behind the nose."
    }
  }]
}
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            responsiveClass: !0,
            navigation : true,
            autoplay: true,
            loop: true,
            smartSpeed: 2500,
            singleItem : true,
            navigationText: ["<i class='fa fa-angle-left fa-2'></i>", "<i class='fa fa-angle-right fa-2'></i>"],
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                480: {
                    items: 1,
                    nav: !1
                },
                768: {
                    items: 2,
                    nav: !1
                },
                1000: {
                    items: 2,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1024: {
                    items: 3,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1366: {
                    items: 3,
                    nav: !1
                }
            }
        })
    });

</script>
@stop