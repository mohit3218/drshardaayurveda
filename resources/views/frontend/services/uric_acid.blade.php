<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Uric Acid</h3>
                   <p style="font-size: 24px;">Protect Your Joints with Timely Treatment</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/uric-acid">Uric Acid</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/uric-acid-treatment.webp') }}" class="why-choose-us img-fluid" alt="uric acid treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>URIC ACID </b></h1>
                <p>
               The uric acid crystal accumulation in the joints can cause serious complications and result in Hyperuricemia. The crystals are formed when the kidneys are unable to filter the food or there is no efficient elimination or flushing out of the waste appropriately. It is the waste product of the digestion process that contains purines. It can result in extreme pain and swelling of the joints if not treated well on time. The condition affects males more compared to women. It mostly affects the big toes and furthermore the midfoot, ankle, and knees. The disease is estimated to affect as many as 1/3rd of populations worldwide. 

                <span id="dots" data-aos="fade-up" data-aos-duration="3000">...</span>
                </p>
                <span id="more-content">
                    <p>Ayurveda, the ancient medical science, is recognized for having an effective solution to resolve a variety of health issues. The Ayurvedic medicines are grounded in providing the perfect balance of the vitiated doshas by suggesting adopting a healthy diet and lifestyle along with remaining strict with taking regular medications. High uric acid is caused due to aggravation of Vata dosha which is mainly contributed by having a poor diet and sedentary lifestyle. Understanding the signs and treating the disease early by choosing Ayurvedic treatment from Dr. Sharda Ayurveda can give positive and long-term results.   </p>
                    <div class="btn-ayurvedic">
                        <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                    </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>




<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Uric Acid</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Renal insufficiency
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                            When the kidneys are unable to filter the waste appropriately. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Purine-rich diet
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                           It is highly recommended to reduce the protein content from the diet to save yourself from developing ailments like uric acid. Example - meat, potatoes, and mushrooms. 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                               Pre-existing medical condition
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                             The health conditions like blood pressure and thyroid (specifically hyperthyroidism) can lead to the production and accumulation of uric acid. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Stress
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                         Many studies proved that high levels of stress and anxiety are associated with the increased production of uric acid.

                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                               Sedentary lifestyle
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                            The continuous following of an unhealthy daily regime increases the chances of making you more prone to get affected by the uric acid disease. 


                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
</section>



<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/uric-acid-symptoms.webp') }}" class="img-fluid"alt="uric-acid-symptoms">
        </div>
        <div class="cvn sym" >
            <h3><span>Symptoms of</span> Uric Acid</h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Pain in joints</p>
            <p> The person may experience severe pain where uric acid accumulation is high.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Swelling</p>
            <p>The extreme swelling along with redness can be observed in the affected joints.  </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Limited range of motion</p>
            <p>Untreated gout can restrict joint mobility or movement. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Kidney stones</p>
            <p>Excessive accumulation of uric acid may potentially increase the chances of stones formation. 
 </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Irritated skin </p>
            <p>The skin becomes dry and chapped. </p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h1 class="client-test-title" style="font-size: 24px !important;">BENEFITS OF  <b>AYURVEDIC TREATMENT FOR URIC ACID</b></h1>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    The Ayurvedic treatment of uric acid is based on balancing the aggravated doshas mostly the Vata doshas by prescribing herbal medications that treat the disease from its root cause.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Exclude having a purine-rich diet, which is the main cause of excessive uric acid production and accumulation in the joints.

                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    The incorporation of anti-inflammatory herbs including turmeric, giloy, and Triphala in the diet will help reduce the pain and swelling of the joints.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    A gentle massage with herbal medicated oils like sesame oil on the affected joint areas will help relieve the severe pain.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Remaining physically active is the best key to preventing issues of joint pain and lost mobility. 
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                
                    <iframe src="https://www.youtube.com/embed/UAT4pZY3Clg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</section>

    

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Harcharan Kaur</h3>
                            <p class="desig">Uric Acid patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was diagnosed with increased uric acid levels in the joints around 3 years back. With passing time, the joint pain and swelling were becoming miserable and unbearable. Slowly and gradually I was getting dependent on painkillers to suppress the pain. Then one of my friends recommended the name of Dr. Sharda Ayurveda, who are known for its authentic Ayurvedic treatment for uric acid and its successful recovery rate. I am glad to share that within just 5-6 months of treatment I am fully recovered, all thanks to Dr. Sharda Ayurveda. 

                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Ram Kishan</h3>
                            <p class="desig">Uric Acid Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                               In January 2021 my mother, Harbans Kaur, was diagnosed with high levels of uric acid which leads to extreme joint pain along with severe swelling. Even after taking a lot of medications and treatments, still, the sign of recovery was far away. Then I got to know about Dr. Sharda's Ayurvedic treatment, which proved to be a blessing for my mother. The Ayurvedic doctor's guidance and prescribed medications provided relief from prolonged pain within just 3 months. I would highly recommend their name.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                 
                </div>
            </div>
        </div>
    </div>
</section>





<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>



<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                            What is the normal level of uric acid in the body?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                       The range can vary with the gender. 
For females, the normal range should be between 2.4-6.0 mg/dL. But in males, the uric acid level range is between 3.4-6.0 mg/dL.
  </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                What do high uric acid levels lead to?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                        The early signs are directed toward arthritis (chronic joint inflammatory disease). If the condition remains untreated, it can eventually lead to permanent bone, joint, and tissue damage. In severe cases, it can also become a reason for the emergence of kidney and heart disease.            </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                How can a person examine whether the uric acid levels are in the normal range?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                        If a person experiences persistent pain and swelling in the joints, they should not neglect it and immediately consult an expert to get well-guided treatment. The levels can be diagnosed by blood or urine test.        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                How the food items that are rich in purines can worsen the symptoms? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                       A low purine diet is often prescribed to patients diagnosed with hyperuricemia. It is noted that high purine production and accumulation with time can lead to chronic condition gout or initiate the formation of kidney stones. As purines in our food break down into uric acid, thus leading to its accumulation.           </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                What are the risk factors associated with high uric acid levels? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                        The risk factors associated with the condition are:
                        
                        <ul>
                                <li>Poor diet</li>
                                <li>Pre-existing medical condition.</li>
                                <li>Side-effects of certain medications.</li>
                                <li>Family history </li>
                            </ul>

 </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                   Does the high cholesterol and uric acid levels co-related? 
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                                
                                
                            Yes, these both are co-related if several studies have to be believed. If a person already suffering from cholesterol and are under medications, it makes them prone to get affected by issues associated with high uric acid levels, i.e., joint pain, swelling, and stiffness. 

                          
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Can babies be affected by uric acid? 

                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                       The low levels of uric acid (hypouricemia) in the bloodstream occur due to starvation or as the negative effect of certain medication. Rarely, children can get affected by gout, but if so, then it can severely cause painful inflammation due to the deposition of uric acid crystals in the synovial fluid. 
   </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                What is the best treatment to choose if a person gets diagnosed with a uric acid-associated disease? 

                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                       The Ayurvedic treatment is the best to opt to get positive and lifelong results. The authentic and holistic Ayurvedic treatment from Dr. Sharda Ayurveda is considered the safest and most reliable option that has successfully healed thousands of patients by suggesting adopting a healthy diet and lifestyle and taking regular herbal medications. 
    </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                Can the use of medicated essential oils can resolve the issue of high uric acid? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                        Yes, the oils that are rich in gamma-linoleic acid (GLA) are highly recommended that help alleviate the pain and swelling associated with the disease. Example - primrose and borage oil. 

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "uric acid",
    "item": "https://www.drshardaayurveda.com/uric-acid"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What is the normal level of uric acid in the body?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "For Females, the normal range of uric acid levels in the body should be from 2.4-6.0 mg/dL.
For Males, the normal range of uric acid levels in the body should be from 3.4-6.0 mg/dL. 
An increase in uric acid above these levels is called hyperuricemia. It varied sometimes according to the different laboratory investigations."
    }
  },{
    "@type": "Question",
    "name": "What do high uric acid levels lead to?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Higher levels of uric acid in the body cause Gout which is a type of arthritis and causes severe pain and inflammation in joints. Uric acid treatment in Ayurveda treats a person within so that there are least chances of its reversal."
    }
  },{
    "@type": "Question",
    "name": "How will I get to know that my uric acid is normal in range or is increased?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If you feel a lot of pain and weakness in joints and on foot, tiredness, and restless then there can be chances of an increase of uric acid in the body. It is also a symptom of arthritis. So you should get your blood investigation done to know the normal range of uric acid."
    }
  },{
    "@type": "Question",
    "name": "Why are food items rich in purines bad for uric acid?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Whenever one is diagnosed with uric acid, 1st thing told to them is to avoid eating food that is rich in purines. This is said so because purines are a compound when metabolized turns into uric acid in the body. By avoiding a purine diet, one reduces the risk of high uric acid."
    }
  },{
    "@type": "Question",
    "name": "What is the biggest cause of uric acid?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "When the kidneys are unable to eliminate all the waste product from the body then it leads to hyperuricemia."
    }
  },{
    "@type": "Question",
    "name": "How to prevent higher uric acid levels?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If you are obese, start reducing your weight there and then. 
•   Exercise regularly, eat fresh and hygiene food. 
•   Reducing sugar intake.
•   Avoid drinking soft drinks and Alcohol.
•   Avoid food rich in purine."
    }
  },{
    "@type": "Question",
    "name": "Is cholesterol and uric acid co-related?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, because a person suffering from cholesterol takes medications that can increase the risk for uric acid as sometimes the mechanism of the medicines affects the working of kidneys and their metabolic process. Ayurvedic treatment for uric acid has sure results with effectiveness."
    }
  },{
    "@type": "Question",
    "name": "Can babies be affected by uric acid?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Higher uric acid can affect any age group. There are no certain age criteria.
Get uric acid levels checked by the specialist and get the best Ayurvedic medicine for uric acid from Dr. Sharda Ayurveda."
    }
  },{
    "@type": "Question",
    "name": "How does obesity causes high uric levels in the body?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "You will always find an obese person will be diagnosed with higher uric levels in the body. It is so because excessive weight gain leads to the deposition of uric acid being deposited into fat tissues."
    }
  }]
}
</script>
@stop