<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" > Back Pain Ayurvedic Treatment </h3>
                    <p style="font-size: 24px;">Protect Your Joints with Timely Treatment</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/back-pain">Back Pain</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/Back-Pain.webp') }}" class="why-choose-us img-fluid" alt="back pain treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"> Ayurvedic Treatment for Back Pain Relief </h1>
                <p>
                    Dr. Sharda Ayurveda, the best ayurvedic treatment approach on back pain relief. We provide a complete set of ayurvedic back pain remedies to get rid of all kinds of back problem issues. </p><p>Each person in a family is suffering with back pain that can be caused due to injury, activity or some medical ill health. Back pain affects people of every age group but with advancing age lower back pain problem increases. Sciatica pain men and women are equally affected and mostly occurs between age 30-50 due to aging process and sedentary life styles with no exercise. The low back supports the weight of the upper body and provide mobility for everyday motion such as bending. So its functioning should be easy and comfortable always. Lower back ache can be related to <a class="green-anchor" href="https://www.drshardaayurveda.com/lumbar-spondylosis">lumbar spine disorders</a>, disc between vertebrae ligament around spine and disc, spinal cord and nerves, muscles of lower back, abdominal and pelvic internal organs lower back. Pain observed in upper back may be due to problem in aorta, chest and spine inflammation.
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">...</span>
                </p>
                <span id="more-content">
                    <p>
                        Back pain begins with light or acute pain and becomes chronic if not treated or medications are not proper. Acute pain starts suddenly and can last up to 6 weeks whereas chronic pain develop over longer period i.e. last for 3 months. Back pain may be sudden onset or can be a chronic pain. It can be constant or intermittent, stay in one place or radiate to other areas. It may be a dull ache, or sharp or piercing or burning sensation. People generally in back pain starts doing home remedies with home remedies patient got relief and after sometime problem occur again and again due to growing age the bones, discs and ligaments in the spine naturally becomes weak and pain starts and that condition is termed as spondylosis and degenerative changes in the spine are called osteoarthritis of spine. Whereas <strong>Ayurvedic treatment for back pain</strong> has life-long results will least chance of back pain to occur itself. 
                    </p>
                    <div class="btn-ayurvedic">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Back Pain </b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Muscle Strain
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                This is frequent cause of back pain. Strain occurs due to muscle spasms, muscle tension, damaged disks, fractures or injuries. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Poor Posture
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                Poor posture overtime will cause the changes in the spine.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Bulging Discs
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                                Bulging of disc can result in more pressure on a nerve ( nerve press).
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Sciatica
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                                Presence of herniated discs causes pressure on nerve hence radiating sharp and shooting pain to lower extremities.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Kidney Problem
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                                Kidney issues like calculi also cause pain in back laterally.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Back Pain <b>Symptoms</b></h2>
                </div>
            </div>
        </div>
        <div class="kmoz cvn sym" >
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Ache or pain anywhere in back</p>
            <p>Pain occurs anytime in lower back and sometimes get hard to even bear it.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Inflammation or swelling on back</p>
            <p>In some cases swelling is also seen due to chronic back pain.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Pain radiating to lower leg</p>
            <p>Pain from back passes to lower leg too causing more complications.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Numbness in legs, buttocks and around genitals</p>
            <p>When there is severe back pain, sometimes numbness occurs in various parts of body.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Weakness</p>
            <p>Weakness in commonly seen in everyone suffering with back pain.</p>
        </div>
        <div class="cvn sym" >
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Ache or pain anywhere in back</p>
            <p>Pain occurs anytime in lower back and sometimes get hard to even bear it.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Inflammation or swelling on back</p>
            <p>In some cases swelling is also seen due to chronic back pain.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Pain radiating to lower leg</p>
            <p>Pain from back passes to lower leg too causing more complications.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Numbness in legs, buttocks and around genitals</p>
            <p>When there is severe back pain, sometimes numbness occurs in various parts of body.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Weakness</p>
            <p>Weakness in commonly seen in everyone suffering with back pain.</p>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Get Ayurveda Benefits<b> for Back Pain</b></h2>
                </div>
            </div>
        </div>
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
<!--                 <h2 class="client-test-title" style="font-size: 24px !important;">Get Ayurveda Benefits <b> for Back Pain </b></h2> -->
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                   With the help of Ayurveda we treat the root cause of back pain and result in long term relief and prevent re-occurrence.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    With the help of Ayurveda we balance Vata by keeping to a routine getting enough sleep avoiding exertion and with light regular exercise like postural and strengthening exercise and nature walk.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Panchkarma is beneficial in lower back pain with the help of Panchkarma. Detoxification is done and Snehana, Swedena and Basti are beneficial for Vata disorders
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    With the help of Ayurvedic medicine we improve the quality of life and reduce the stress pain and other risk factors.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Dietary modification i.e. food rich in calcium, Vitamin D is essential for bones health. Neck exercise and asanas heals the stiffness present in back. 
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="ox-AqxY0FaY" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/ox-AqxY0FaY.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Harjot Kaur</h3>
                            <p class="desig">Back Pain Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was suffering with lower back pain since 4 years. I used to take painkiller after an alternate day to get relief. So much dependency on painkillers started harming my body in other ways. So I decided to take Ayurvedic treatment and started medicines from Dr. Sharda Ayurveda. Within few months I got relief from pain and I recovered completely within 7 months. 
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Kulwant Singh</h3>
                            <p class="desig">Back Pain Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was going through severe back pain since 2 years. I consulted many specialists but almost everyone suggested me for surgery as they said condition is critical. But from Ayurvedic medicines from Dr. Sharda Ayurveda I recovered without any need of operation. Dr. Sharda Ayurveda is the best for back pain treatment in India. 
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    
                   
                </div>
            </div>
        </div>
    </div>
</section>


<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                Is Ayurvedic treatment effective for back pain?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                            Yes, Dr Sharda, has effective range of medicines and one recovers easily. If a patient follows workout routine and  hot fermentation, it provides excellent result oriented treatment for back pain. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Is Ajwain good for back pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                            Yes, having Ajwain with lukewarm help to treat back and abdominal pain.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Which drinks are beneficial for treating back pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                            Cherry juice can relieve muscle pain hence is benificial in treating back pain. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Is drinking water good for lower back pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                            Yes, drinking water replenish the discs and reduce the likelihood of developing back pain
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Is walking good for lower back pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                            Walking is good as aerobic exercise has reduced incidence of low back pain.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    How can I cure my back pain?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                                <ul>
                                    <li>Avoid more bed rest</li>
                                    <li>Keep right posture</li>
                                    <li>Don’t miss out walking</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                What is main reason for back pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                            Heavy lifting of goods or sudden awkward movement can cause back pain.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                What type of food is advised in back pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                            Avoid eating heavy and fried food. Always eat light food rich in calcium. Obesity causes back pain so try to balance your weight so as to avoid back pain
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                My mother is suffering from back pain should she limit her mobility?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                            Yes definitely as in back pain one should avoid forward bending because in exertion a patient might complaint of numbness in feet. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                I am Obese, do I have chance of back pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                            Yes, overweight people are more prone to back pain because extra weight of body put stress on spine therefore causes health issues.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Back Pain",
    "item": "https://www.drshardaayurveda.com/back-pain"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "Is Ayurvedic treatment effective for back pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, Dr. Sharda, has an effective range of medicines and one recovers easily. If a patient follows a workout routine and hot fermentation, it provides excellent result-oriented solution for back pain."
    }
  },{
    "@type": "Question",
    "name": "Is Ajwain good for back pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, having Ajwain with lukewarm helps to treat back and abdominal pain."
    }
  },{
    "@type": "Question",
    "name": "Which drinks are beneficial for treating back pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Cherry juice can relieve muscle pain hence is beneficial in treating back pain."
    }
  },{
    "@type": "Question",
    "name": "Is drinking water good for lower back pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, drinking water replenishes the discs and reduce the likelihood of developing back pain."
    }
  },{
    "@type": "Question",
    "name": "Is walking good for lower back pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Walking is good as aerobic exercise has reduced the incidence of low back pain."
    }
  },{
    "@type": "Question",
    "name": "How can I cure my back pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "•    Avoid more bed rest 
•   Keep right posture 
•   Don’t miss out on walking"
    }
  },{
    "@type": "Question",
    "name": "What is main reason for back pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Heavy lifting of goods or sudden awkward movement can cause back pain."
    }
  },{
    "@type": "Question",
    "name": "What type of food is advised in back pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Avoid eating heavy and fried food. Always eat light food rich in calcium. Obesity causes back pain so try to balance your weight so as to avoid back pain."
    }
  },{
    "@type": "Question",
    "name": "My mother is suffering from back pain should she limit her mobility?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes definitely as in back pain one should avoid forward bending because in exertion a patient might complain of numbness in the feet."
    }
  },{
    "@type": "Question",
    "name": "I am Obese, do I have chance of back pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, overweight people are more prone to back pain because extra weight of body put stress on spine therefore causes health issues."
    }
  }]
}
</script>
@stop