<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Alcohol De-Addiction</h3>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/alcohol-de-addiction">Alcohol De-Addiction</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/Alcohol-De-Addiction.webp') }}" class="why-choose-us img-fluid" alt="Alcohol Addiction Treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1">Alcohol De-Addiction Ayurvedic Treatment</h1>
                <p>
                Excess of everything is bad so is the consumption of excessive Alcohol going to harm you one day. Alcohol consumption is injurious to health. A person starts drinking for fun and later it becomes a bad addictive habit of them. Addiction to Alcohol is also the major cause of destruction. It disturbs the mind, harms the body and relationships. Alcohol addiction has been ruing families for generations and over the past years, it has become a hot growing concern. Almost one person in a family is addicted to alcohol. It affects people of all age groups mostly after 18. Due to excessive drinking, a person sometimes loses control of their senses and does not realize what they are up to. It gets hard for them to control their actions. It is always advisable to opt for <strong style="font-weight: bold;">Ayurvedic treatment for Alcohol de Addiction</strong> before it gets too late
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">...</span>
                </p>
                <span id="more-content">
                    <p>
                    The severity of addiction i.e. how often someone drinks, the amount of alcohol they consume varies from every Individual. Some of them begin in the morning itself like it is their bed tea and continue drinking heavily all day long while some at once start drinking and continue it for several hours. A person who is heavily dependent on alcohol and cannot stay without it for even a few hours, sober and normal should get Ayurvedic treatment for alcoholism for best results. Alcohol addiction impairs the functioning of kidneys and liver and also affects sexual life and muddles up life physically, emotionally, and personally. 
                    </p>
                    <div class="btn-ayurvedic">
                        <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                    </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Alcohol Addiction</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Stress
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                            We often hear a drinker saying I drink to release my daily stress. Stress can be caused due to several reasons like work stress or family stress.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Unhappy Lifestyle
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            A person who has complaints and is not satisfied with life leads to the direction of alcohol addiction.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Insomnia
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                            Some people due to many reasons are unable to sleep at night. So to get the sleep they start drinking and it becomes their habit.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Drinking History
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                            An individual’s drinking history impacts heavily the likelihood of developing alcoholism.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                Lack of Family Supervision
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                            Lack or avoidance of family towards their actions and habits and no significant levels of nurturing puts an individual into odd ways.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/Alcohol-Addiction-symptoms.webp') }}" class="img-fluid"alt="Alcohol Addiction symptoms">
        </div>
        <div class="cvn sym" >
            <h3><span>Symptoms of</span> Alcohol Addiction </h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Impaired Body Functioning</p>
            <p>An individual develops several health complications like damage to the kidney and liver.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Violence</p>
            <p>Due to alcohol addiction a person when gets high and loses control becomes violent too because of aggression. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Avoidance from Loved Ones</p>
            <p>Avoiding and ignoring loved ones because they stop them from drinking. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Emotional Issues</p>
            <p>Restlessness, depression, anxiety, frustration, and increased lethargy are quite common. . </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Inappropriate Timings</p>
            <p>An addicted person starts drinking anywhere anytime. For some drinking alcohol becomes the morning clock. They are quite addicted that they even want alcohol in inappropriate places. . </p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Benefits of <b>Ayurvedic Treatment for Alcohol De-Addiction</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurveda is famous for its old age remedial treatments and Ayurvedic herbs that help one in recovering from alcohol addiction through all the natural ways with no harm to the body. Ayurveda offers several Ayurvedic medicines that help in cutting the craving for alcohol naturally. Alcohol withdrawal treatment is safe and effective in Ayurveda.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurveda will treat a patient within and make them healthy so that the habit of drinking does not begin again.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Most Important Ayurvedic medicines can be given to patients even without knowing them.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    The treatment varies for every patient as in Ayurveda it is believed that no 2 individuals are alike. They have different body types and depending on the constitution of Vata, Pitta, and Kapha medicines are prescribed that aims to flush out all the toxins from the body and restore the internal tissues.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Yoga and Pranayama followed with daily meditation helps to release all the worries, stress and makes an Individual healthy and fit.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Engage in activities that enhance your mood.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <br>
                        <div class="youtube-player" data-id="Anz1gnNtUT0" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/Anz1gnNtUT0.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Alcohol Addiction Testimonial" >
                            <h3 class="usrname">Dheer Singh</h3>
                            <p class="desig">Alcohol Addiction Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                My son got addicted to alcohol at an early age. We caught him red-handed and were quite worried so that we can help him get out of his bad habit. We thought of getting treatment from <b>Dr. Sharda Ayurveda</b> as we heard a lot about their de-addiction Ayurvedic treatment. We consulted the specialist and got the medicines for my son. Even without letting him know we started giving him medicines added in food. Within just 20 days we saw a change in him and his behavior. We checked again after few months he had already left drinking. We are very thankful to Dr. Sharda Ayurveda
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Alcohol Addiction Testimonial" >
                            <h3 class="usrname">Jeet Singh</h3>
                            <p class="desig">Alcohol Addiction Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was addicted to alcohol for 7 years now. I knew my family was disturbed due to my addiction but I tried still could not stop myself from drinking. It was my daily task as I was unable to sleep without drinking. I tried several medicines too but nothing worked. I started my treatment with Dr. Sharda Ayurveda. Their 3 months Ayurvedic treatment helped me in getting rid of my habit and finally I could control myself from drinking. 
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1"><b>FAQ'S</b></h2>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                What is Alcoholism?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                            Alcoholism is described as the dependency on alcohol that includes some symptoms such as-
                            <ul>
                                <li><b>Craving-</b> Strong urge to drink.</li>
                                <li><b>Loss of control-</b> One is unable to control or stop drinking once drinking has begun.</li>
                                <li><b>Tolerance-</b> To get high the need increases in greater amounts of alcohol.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Can alcoholism be treated?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                        Yes, alcoholism can be treated through Ayurvedic medicines. Ayurvedic treatment for de-addiction of alcohol involves counseling and Ayurvedic medications to help a person stop drinking. <b>Ayurvedic Treatment for alcohol de-addiction</b> has helped many around the globe to stop drinking
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Is there any specific age group of people more likely to have problems?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                        Alcohol de-addiction can begin at any age. It begins with fun and becomes a habit in a short time. No matter male or female can both get addicted to any point in life. Alcohol addiction is found highest among younger adults i.e. 18 to 35 and lowest among the adults aged between 65 and older.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                How soon a person quits drinking after taking Ayurvedic medicines?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                        Within 20 days of treatment, you will yourself get to see a different change in the person. It may take some days to help him quit drinking. Ayurvedic medicines stop the craving for alcohol by working on the root cause so that a person does not begin again drinking
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                What are the harmful effects of drinking alcohol every day?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                        Drinking alcohol daily whether in less quantity is going to hamper body functions. It will harm Liver and Kidney functioning and will impact the eyes. Liver and kidney functioning are mandatory for a healthy body.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    How long does alcohol take to leave the body?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                            Alcohol detection tests help in measuring alcohol in the blood for up to 6 hours, on the breath for 12 to 24 hours, urine, saliva for 12 to 24 hours. 
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                What are the signs of liver damage from alcohol?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                        Alcohol addiction leads to liver damage. It begins with swelling of the liver, which leads to discomfort in the upper right side of the abdomen, fatigue, unexplained weight loss, loss of appetite, nausea, and vomiting. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Do drinking alcohol shorten life expectancy?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                        Yes, alcohol addiction harms the body in several ways and minimizes the chances of living a healthier life. It directly leads to liver damage and kidney disorders.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                What is the quickest way to get alcohol out of the body system?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                        Opt for detoxification of the body that helps in the removal of all toxins. Intake more juice, soup, and coconut water. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Can one take antidepressant medication while being treated for alcohol de-addiction Ayurvedic treatment?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                        Yes, one can take it but under the supervision of an Ayurvedic specialist. Never take medicines without the doctor's prescription. But it is always advised sleeping sound sleep, exercises, and meditation help one in getting out of depression.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Alcohol De-Addiction",
    "item": "https://www.drshardaayurveda.com/alcohol-de-addiction"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What is Alcoholism?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Alcoholism is described as the dependency on alcohol that includes some symptoms such as-
•   Craving- Strong urge to drink.
•   Loss of control- One is unable to control or stop drinking once drinking has begun.
•   Tolerance- To get high the need increases in greater amounts of alcohol."
    }
  },{
    "@type": "Question",
    "name": "Can alcoholism be treated?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, alcoholism can be treated through Ayurvedic medicines. Ayurvedic treatment for de-addiction of alcohol involves counseling and Ayurvedic medications to help a person stop drinking. Ayurvedic Treatment for alcohol de-addiction has helped many around the globe to stop drinking."
    }
  },{
    "@type": "Question",
    "name": "Is there any specific age group of people more likely to have problems?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Alcohol de-addiction can begin at any age. It begins with fun and becomes a habit in a short time. No matter male or female can both get addicted to any point in life. Alcohol addiction is found highest among younger adults i.e. 18 to 35 and lowest among the adults aged between 65 and older."
    }
  },{
    "@type": "Question",
    "name": "How soon a person quits drinking after taking Ayurvedic medicines?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Within 20 days of treatment, you will yourself get to see a different change in the person. It may take some days to help him quit drinking. Ayurvedic medicines stop the craving for alcohol by working on the root cause so that a person does not begin again drinking."
    }
  },{
    "@type": "Question",
    "name": "What are the harmful effects of drinking alcohol every day?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Drinking alcohol daily whether in less quantity is going to hamper body functions. It will harm Liver and Kidney functioning and will impact the eyes. Liver and kidney functioning are mandatory for a healthy body."
    }
  },{
    "@type": "Question",
    "name": "How long does alcohol take to leave the body?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Alcohol detection tests help in measuring alcohol in the blood for up to 6 hours, on the breath for 12 to 24 hours, urine, saliva for 12 to 24 hours."
    }
  },{
    "@type": "Question",
    "name": "What are the signs of liver damage from alcohol?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Alcohol addiction leads to liver damage. It begins with swelling of the liver, which leads to discomfort in the upper right side of the abdomen, fatigue, unexplained weight loss, loss of appetite, nausea, and vomiting."
    }
  },{
    "@type": "Question",
    "name": "Do drinking alcohol shorten life expectancy?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, alcohol addiction harms the body in several ways and minimizes the chances of living a healthier life. It directly leads to liver damage and kidney disorders."
    }
  },{
    "@type": "Question",
    "name": "What is the quickest way to get alcohol out of the body system?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Opt for detoxification of the body that helps in the removal of all toxins. 
Intake more juice, soup, and coconut water."
    }
  },{
    "@type": "Question",
    "name": "Can one take antidepressant medication while being treated for alcohol de-addiction Ayurvedic treatment?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, one can take it but under the supervision of an Ayurvedic specialist. Never take medicines without the doctor's prescription. But it is always advised sleeping sound sleep, exercises, and meditation help one in getting out of depression."
    }
  }]
}
</script>
@stop