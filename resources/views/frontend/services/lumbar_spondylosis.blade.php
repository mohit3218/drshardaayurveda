<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Lumbar Spondylosis</h3>
                    <p style="font-size: 24px;">Protect Your Joints with Timely Treatment</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/lumbar-spondylosis">Lumbar Spondylosis</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/lumbar-spondylosis.webp') }}" class="why-choose-us img-fluid" alt="Lumbar Spondylosis Treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Lumbar Spondylosis</b> Ayurvedic Treatment</h1>
                <p>Lumbar spondylosis Ayurvedic treatment is beneficial to the body and it not only involves the Ayurvedic medicines but also the herbs, dietary/lifestyle changes, and yoga. Lumbar spondylosis is a term that simply refers to some degeneration in the spine, lumbar vertebrae, cartilage, muscles, and bones involved in the lumbar area. It is commonly referred to as spine arthritis. It is a chronic and inflammatory disease and is progressive with age and irreversible in elderly patients. As the bodies age, the intervertebral disc starts losing fluid that leads to collapse in disc height and other degenerative changes like ligaments becoming thickened and stiff. The location of degenerative changes is facet joints, intervertebral disc, and sacroiliac joint, nerve, and muscles around Lumbar Vertebrae. It occurs as a result of new bone formation in an area where the annular ligament is stressed and growth of new bone in form of Osteophytes is seen anteriorly or literally. 
                <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                    <p>
                    These Osteophytes cause back pain which increases with the age of a person. This pain is seen in 15-45% of the total population. Some bear acute lower back pain and others face chronic low back pain, pain that persists beyond 3 months. Some people develop lumbar spondylosis even at an earlier age, due to certain risk factors like history of trauma, history of smoking, genetic predisposition, or occupation factor of hard labor. Ayurvedic medicines for lumbar spondylosis have effective results that too without any surgery performed of the spine.
                    </p>
                    <div class="btn-ayurvedic">
                        <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                    </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Lumbar Spondylosis</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Age Factor
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                As we age, the bones get degenerated thus leading to <a class="green-anchor" href="https://www.drshardaayurveda.com/back-pain">back pain</a>.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Activity and Occupation
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            Activities and  work like bending, lifting, twisting, and sustaining non-neutral postures, driving for a long time increases the chances of getting degenerative changes in bones, ligaments, and cartilages due to load on spine.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Hereditary
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                            Genetic factors likely influence the formation of Osteophytes and disc degeneration.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Herniated or Bulging Disc
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                                Disk acts as a cushion in between the vertebrae in the spine. Due to wear and tear, the soft cushion gets shrunken thus causing pain.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Arthritis of The Spine
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                                Degeneration of bones causes lumbar spondylosis</a>.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/lumbar-spondylosis-symptoms.webp') }}" class="img-fluid"alt="Lumbar Spondylosis Symptoms">
        </div>
        <div class="cvn sym" >
            <h3><span>Symptoms of</span> Lumbar Spondylosis </h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Pain in the axial spine</p>
            <p>It is confined to the lower back area or region.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Numbness in the affected area when standing or walking</p>
            <p>This is due to nerve compression. Lack of blood circulation in the body causes numbness. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Joint or muscular stiffness upon awakening</p>
            <p>Stiffness that typically gets worse in the morning or at night is the common symptom that patient experiences. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Muscle weakness or tingling in the back, buttocks, and legs</p>
            <p>Chronic stress can lead to muscle weakness.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Tenderness in the area of nerve compression </p>
            <p>Due to herniated disc, osteoarthritis, or some Injury, tenderness may occur.</p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Benefits of <b>Ayurvedic Treatment for Lumbar Spondylosis</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Lumbar spondylosis is considered as Vata Vyadhi in Ayurveda.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    <strong>Ayurvedic treatment for lumbar spondylosis</strong> involves detoxification and elimination of toxins, Kati Basti, Patra Pinda Sweda.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    An Ayurvedic medicine for balancing VataDosha not only heals the problem but also relieves the pain. Lumbar spondylosis treatment in Ayurveda works on treating the root cause and heals the body within. 
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurvedic medicines and therapies relax the muscles; relieve inflammation ofthe nerves and surrounding tissue.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurvedic treatment for lumbar spondylosis focuses on treating back pain and neck pain that comes with the inflammation.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="TEkKfY2aTi8" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/TEkKfY2aTi8.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                            Is there any Ayurvedic treatment for Lumbar spondylosis?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                        Yes, Ayurvedic medicines and Panchkarma are preferred that heals muscles, nerves, and surrounding tissue of affected areas. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Is walking good for Lumbar Spondylosis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                        Yes, walking is advised as it increases flexibility by creating antagonist movement of muscles, regulating ligaments action in back, legs and hip.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                What is the main cause of Lumbar Spondylosis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                        Aging and Osteoarthritis are the main cause, where aging leads to wear and tear of ligament, cartilage, and bones.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                What happens if Lumbar Spondylosis is not treated?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                        If lumbar spondylosis is left untreated then extra bones start developing in between vertebrae and finally get fused which leads to restricted movement of spine thus causing pain.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Can one lift weight if diagnosed with lumbar spondylosis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                        No, one should always avoid heavy weight lifting because it might aggravate back pain which may last longer than usual.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    Is massage with oil good in lumbar spondylosis?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                            Massage is good for relieving lumbar spondylosis pain. It also helps to cure muscles spasm and to improve the movement of affected areas. 
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Is cycling advised for patients of lumbar spondylosis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                        Some exercises are especially for it but one should never practice them without a doctor’s prescription. Cycling is a good exercise as it improves blood circulation and heals stiffness of nearby nerves and muscles.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Does lumbar spondylosis affect every age group?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                        Due to lifestyle changes and not eating a healthy nutritious diet, more than 80% of men and women aged 40 and above are more likely to show the symptoms of spine degeneration.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                Which food items are good to treat lumbar spondylosis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                        <ul>
                            <li>All the seasonal fruits and vegetables are high in antioxidants</li>
                            <li>Add more fluids to your diet. Fresh fruit juices, vegetable juices, and soups.</li>
                            <li>Plant based Omega-3 fatty acids are highly beneficial.</li>
                            <li>Omega-3 fatty acids are highly beneficial.</li>
                            <li>Avoid eating food high in fat and cholesterol.</li>
                        </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                    What is the difference between spondylolisthesis and spondylosis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                        Spondylolisthesis is termed as a movement of one vertebra over the one below it (Slippage) and Spondylosis is termed as a crack or stress fracture in a specific part of the vertebra which is mostly due to wear and tear.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Lumbar Spondylosis",
    "item": "https://www.drshardaayurveda.com/lumbar-spondylosis"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "Is there any Ayurvedic treatment for Lumbar spondylosis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, Ayurvedic medicines and Panchkarma are preferred that heals muscles, nerves, and surrounding tissue of affected areas."
    }
  },{
    "@type": "Question",
    "name": "Is walking good for Lumbar Spondylosis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, walking is advised as it increases flexibility by creating antagonist movement of muscles, regulating ligaments action in back, legs and hip."
    }
  },{
    "@type": "Question",
    "name": "What is the main cause of Lumbar Spondylosis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Aging and Osteoarthritis are the main cause, where aging leads to wear and tear of ligament, cartilage, and bones and osteoarthritis develop due to degeneration of bones."
    }
  },{
    "@type": "Question",
    "name": "What happens if Lumbar Spondylosis is not treated?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If lumbar spondylosis is left untreated then extra bones start developing in between vertebrae and finally get fused which leads to restricted movement thus causing pain."
    }
  },{
    "@type": "Question",
    "name": "Can one lift weight with lumbar spondylosis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "No, one should always avoid heavy weight lifting because it might aggravate back pain and may last longer."
    }
  },{
    "@type": "Question",
    "name": "Is massage with oil good in lumbar spondylosis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Massage is good for releasing lumbar spondylosis pain. It also helps to cure muscles spasm and to improve the movement of affected areas."
    }
  },{
    "@type": "Question",
    "name": "Is cycling advised for patients of lumbar spondylosis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Some exercises are specially for it but one should never practice them without a doctor’s prescription. Cycling is a good exercise as it improves blood circulation and heals stiffness of nearby nerves and muscles."
    }
  },{
    "@type": "Question",
    "name": "Does lumbar spondylosis affect every age group?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Due to lifestyle changes and not eating a healthy nutritious diet, more than 80% of men and women aged 40 and above are more likely to show the symptoms of spine degeneration."
    }
  },{
    "@type": "Question",
    "name": "Which food items are good to treat lumbar spondylosis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "•    All the seasonal fruits and vegetables are high in antioxidants. 
•   Add more fluids to your diet. Fresh fruit juices, vegetable juices, and soups. 
•   Omega-3 fatty acids are highly beneficial.
•   Avoid eating food high in fat and high-cholesterol foods."
    }
  },{
    "@type": "Question",
    "name": "What is the difference between spondylolisthesis and spondylosis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Spondylolisthesis is termed as a movement of one vertebra over the one below and 
Spondylolysis is termed as a crack or stress fracture in a specific part of the vertebra."
    }
  }]
}
</script>
@stop