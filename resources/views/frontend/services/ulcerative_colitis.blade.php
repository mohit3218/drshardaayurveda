<?php
/**
 * Ulcerative Colitis Page 
 * 
 * @created    03/02/2022
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2022
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h1 class="ser-heading mt-4">Ayurvedic Treatment for Ulcerative Colitis</h1>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/ulcerative-colitis">Ulcerative Colitis</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION<span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a  href="#types">TYPES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatment">TREATMENT</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="definition">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc">
                    <img src="{{ URL::asset('front/images/ulcerative-colitis.webp') }}" class="why-choose-us img-fluid" alt="ulcerative colitis">
                </div>
            </div>
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " >
                <h1 class="heading1">What is <b>Ulcerative Colitis</b></h1>
                <p>
                In Ayurveda ulcerative colitis is known as “Pittaj Grahani”. Ulcerative colitis is the chronic disease of the large intestine which is categorized under inflammatory bowel disease (IBD) causing irritation, inflammation, and ulcers in your digestive tract. It affects the innermost lining of the large intestine i.e., colon and rectum. If not treated timely can sometimes lead to life-threatening complications. The major cause of the disease is our improper immune functioning. Normally when a foreign intruder attacks our body the immune system generates responses to protect us. But if a person is suffering from ulcerative colitis the immune system considers the food, gut bacteria, and healthy cells in the lining of the colon as the intruder and thus generate a response against them. Thereby the white blood cells affect the colon lining, causing inflammation, and ulcers. The disease begins in the rectal area and may involve large intestine over time. Because the disease is chronic, the symptoms do not develop suddenly but over time. 
                <span id="dots">...</span>
                </p>
                <span id="more-content">
                <p>
                Ayurveda believes that disease emerges due to an imbalance of the person's consciousness and thereby ulcerative colitis is associated with the imbalance of Pitta and Vata Doshas. Ayurveda is a natural and effective way of healing and maintaining body health. When the digestive system fails to function properly, the body becomes congested, and there is accumulation of toxins in the body. <strong>Ayurvedic treatment for ulcerative colitis</strong> from Dr. Sharda Ayurveda helps in the healthy functioning of the digestive system by opting holistic approach.
                </p>
                <div class="btn-ayurvedic">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn">Read more</button>
            </div>
        </div>
    </div>
</section>
<section class="dsa-india-ayurvedic-sec" id="types"> 
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="heading1">Types of <b>Ulcerative Colitis</b></h2>
            </div>
        </div>
        <div class="row cust-iba">
            <div class="owl-carousel owl-theme" >
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/ulcerative-proctitis.webp') }}" class="card-img-top lazyload" alt="Ulcerative Proctitis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Ulcerative Proctitis</h4>
                            <p class="card-text">
                                It causes bowel inflammation which majorly happens in the rectum area, the last part of the large intestine. This type is linked with the higher risk of developing cancer. The symptoms are pain, and bleeding in rectum.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/left-sided-colitis.webp') }}" alt="Left-Sided Colitis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Left-Sided Colitis</h4>
                            <p class="card-text">
                                Affects the rectum, and all the way to the splenic flexure of the colon. It symptoms include loss of appetite, weight gain, and diarrhea.
                            </p>
                        </div>
                    </div>
                </div>


                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/extensive-colitis.webp') }}"  alt="Extensive Colitis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Extensive Colitis</h4>
                            <p class="card-text">The type which affects the entire colon and causes continuous inflammation that begins from the rectum and extends beyond splenic flexure. The symptoms of this type are loss of appetite, abdominal pain, and weight loss.</p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/proctosigmoiditis.webp') }}"  alt="Proctosigmoiditis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Proctosigmoiditis</h4>
                            <p class="card-text">
                                This form involves the area of the rectum and the lower area of the colon. The symptoms are bloody diarrhea, abdominal cramps, and pain.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/fulminant-colitis.webp') }}"  alt="Fulminant Colitis">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Fulminant Colitis</h4>
                            <p class="card-text">This is the severe and life-threatening form of colitis that affects the entire colon causing pain, and diarrhea.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate">
                    <h2 class="heading1">Causes of <b>Ulcerative Colitis</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Abnormal Immune System
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Improper immune system functioning is the important cause of the development of ulcerative colitis. It leads to severe inflammation of the large intestine of the body.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Microbiome
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                These are the bacteria, viruses, and fungi present in the person’s digestive tract which aids in healthy digestion. If the immune system is not functioning, the white blood cells kill the healthy gut bacteria which causes ulcerative colitis.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Genetic
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">
                                It is believed that digestive diseases run in the family genes. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Environment
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                            <div class="card-body">
                                Certain environmental factors may cause the onset of ulcerative colitis which is categorized as unhealthy diet and sedentary lifestyle. The symptoms are <a class="green-anchor"href="https://www.drshardaayurveda.com/abdominal-pain">abdominal pain</a> and loss of appetite.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>			


<div class="container-strip" style="background: #eeb03f";>
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/ulcerative-colitis-symptoms.webp') }}" class="img-fluid" alt="Ulcerative Colitis symptoms">
        </div>
        <div class="cvn sym" >
            <h3>Symptoms of<span> Ulcerative Colitis</span></h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Abdominal Pain with Constipation</p>
            <p>Due to internal body inflammation, the person may experience constant abdominal pain which in severe condition is unbearable.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Bloody and Mucous Stools</p>
            <p>The common symptom of the disease is diarrhea where the person would have pain with bloody stools.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Inflammation</p>
            <p>As result of improper immune system functioning which directly affects the colon lining causing inflammation. In severe condition pain and inflammation of joints is also observed.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Skin Rashes</p>
            <p>The red, and itchy rashes all over the skin are the observable symptom of ulcerative colitis.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Weight Fluctuations</p>
            <p>Individuals affected with colitis may experience weight fluctuations even after maintaining a healthy diet.</p>
        </div>
    </div>
</div>
<div class="split"></div>
<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " >
                <h2 class="client-test-title"style="font-size: 24px !important;"><b>Ulcerative Colitis</b> Ayurvedic Treatment</h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ulcerative colitis treatment in Ayurveda can be done by following Panchakarma therapy which helps in detoxification of the body. It includes herbal oil massage, diet and lifestyle regulations that help in eliminating toxins, reducing inflammation, and stabilizing the immune system.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Practicing yoga and meditation helps relaxing mind and body, thereby, reducing the chances of accumulation of toxins in our body.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurvedic herbs are useful in managing inflammation and easing the symptoms of ulcerative colitis. Some of the best herbs recommended are Boswellia, and Curcuma longa which have strong anti-inflammatory properties.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Maintaining a proper diet rich in Omega-3 fatty acids helps in releasing the toxins from the body and reducing the symptoms of ulcerative colitis.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurvedic treatment along with adopting healthy dietary and lifestyle changes helps in detoxification of the body effectively thus reducing the chances of emergence of disease.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="G2SXEXmtMqY" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/G2SXEXmtMqY.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Patient <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Asthma Testimonial" >
                            <h3 class="usrname">Arjun Kumar</h3>
                            <p class="desig">Ulcerative Colitis Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                My problem of ulcerative colitis started 2 years back. At first, I neglected the problem but with time I could experience constant abdominal pain, and blood stools which made me worried. I consulted many specialists but the result was always unsatisfactory. At last, I was suggested to undergo surgery. Therefore, I consulted Dr. Sharda Ayurveda for ulcerative colitis treatment. I was told to follow the specialized diet chart along with the Ayurvedic medicines. I was surprised to see that within just 1 month of the treatment I could feel the change. I continued with the treatment for another 6-8 months and I am now recovering well. I could not thank enough to Dr. Sharda Ayurveda whose treatment not only helped me effectively recovering from the disease but also avoided unwanted surgeries. I would recommend everyone to visit Dr. Sharda Ayurveda for effective Ayurvedic treatment.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Asthma Testimonial" >
                            <h3 class="usrname">Sukhdeep Singh</h3>
                            <p class="desig">Ulcerative Colitis Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I am thankful to Dr. Sharda Ayurveda as I am no longer suffering from ulcerative colitis after taking their medicines for about 4 months from now. Their best and most effective Ayurvedic treatment along with the recommended diet charts and lifestyle modifications helped me recover within just a few months of the treatment. I would recommend everyone to consult Dr. Sharda Ayurveda for the best and effective Ayurvedic treatment.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                               What are the risk factors of ulcerative colitis? 
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            The risk factors associated with ulcerative colitis are:
                            <li>Age</li>
                            <li>Gender</li>
                            <li>Unhealthy diet</li>
                            <li>Sedentary lifestyle</li>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Which Ayurvedic herbs help reduce inflammations?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            The best and most effective herbs helpful in reducing body inflammations are turmeric and ginger. Additionally, they also relieve chronic pain, reduce nausea, and improve immune system functioning.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Which is the best treatment for getting recovery from ulcerative colitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            The best and most effective treatment for ulcerative colitis is Ayurveda. The Ayurveda is the traditional practice which with its long known herbal medications helps in long-term recovery. The ulcerative colitis best treatment in India is available at Dr. Sharda Ayurveda. With the guidance from the experts along with recommended dietary modification provides the safe and effective treatment.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                What is the difference between ulcerative colitis and Crohn's disease?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                            Both of these diseases are associated with the intestines of the body. But the major difference is that ulcerative colitis is caused due to improper immune functioning but in Crohn's disease there is blockage of the intestine and ulceration in the intestinal tracts.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFive" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <i class="fa" aria-hidden="true"></i>
                                How does stress play role in developing ulcerative colitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="card-body">
                            If you are already suffering from ulcerative colitis stress can significantly flare up the disease. People with UC are more likely to diagnose with mental disorders such as stress, and anxiety.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                How Ayurveda can help to treat ulcerative colitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                        <div class="card-body">
                            Ayurveda is a traditional practice whose applications are used till now to treat various life-threatening chronic diseases and through its application one can even avoid unwanted surgeries. If you are searching for the best ulcerative colitis Ayurveda consultants without a second thought consult Dr. Sharda Ayurveda for effective Ayurvedic treatment.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                How can exercise beneficial for ulcerative colitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                        <div class="card-body">
                            The benefits of exercise can be many that help in effective recovery from ulcerative colitis are:
                            <li>Strengthens your bones</li> 
                            <li>Lower stress</li> 
                            <li>Maintains muscle functioning </li> 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                What are the best vitamins for ulcerative colitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                        <div class="card-body">
                            The vitamins which are best to recover from ulcerative colitis are vitamin B12, D, A, and K.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Is ulcerative colitis an autoimmune disease?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                        <div class="card-body">
                            Yes, it’s an autoimmune disease where the immune system mistakenly recognizes the healthy cells of the body as the intruder or foreign antigen and generate response against them.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                What are the consequence if ulcerative colitis is left untreated?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                        <div class="card-body">
                            If ulcerative colitis is left untreated the inflammation would spread throughout the colon thereby severely damaging the lining of the colon. But in children, if the disease is not treated timely can limit their growth and may interfere with their overall development.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<style>
    .carousel-inner {position: relative;width: 100%;overflow: hidden;}
    .carousel-item {position: relative;float: left;width: 100%;margin-right: -100%;-webkit-backface-visibility: hidden;backface-visibility: hidden;transition: -webkit-transform .6s ease-in-out;transition: transform .6s ease-in-out;transition: transform .6s ease-in-out,-webkit-transform .6s ease-in-out;}
    .card-body {padding-top: 1.5rem;padding-bottom: 1.5rem;-webkit-border-radius: 0!important;border-radius: 0!important;flex: 1 1 auto;-ms-flex: 1 1 auto;}
    .card .card-body a h3{font-size: 18px;color: #ff9000;line-height: 1.4;}
    .card .card-body h3{color: #373433;font-size: 18px;    line-height: 1.4;}
    .font-weight-bold {font-weight: 700!important;}
    .card-title {margin-bottom: .75rem;}
    .card-img-top {width: 100%;border-top-left-radius: calc(.25rem - 1px);border-top-right-radius: calc(.25rem - 1px);}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            responsiveClass: !0,
            navigation : true,
            autoplay: true,
            loop: true,
            smartSpeed: 2500,
            singleItem : true,
            navigationText: ["<i class='fa fa-angle-left fa-2'></i>", "<i class='fa fa-angle-right fa-2'></i>"],
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                480: {
                    items: 1,
                    nav: !1
                },
                768: {
                    items: 2,
                    nav: !1
                },
                1000: {
                    items: 2,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1024: {
                    items: 3,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1366: {
                    items: 3,
                    nav: !1
                }
            }
        })
    });

</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Ulcerative Colitis",
    "item": "https://www.drshardaayurveda.com/ulcerative-colitis"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What are the risk factors of ulcerative colitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The risk factors associated with ulcerative colitis are:
•   Age 
•   Gender
•   Unhealthy diet 
•   Sedentary lifestyle"
    }
  },{
    "@type": "Question",
    "name": "Which Ayurvedic herbs help reduce inflammations?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The best and most effective herbs helpful in reducing body inflammations are turmeric and ginger. Additionally, they also relieve chronic pain, reduce nausea, and improve immune system functioning."
    }
  },{
    "@type": "Question",
    "name": "Which is the best treatment for getting recovery from ulcerative colitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The best and most effective treatment for ulcerative colitis is Ayurveda. The Ayurveda is the traditional practice which with its long known herbal medications helps in long-term recovery. The ulcerative colitis best treatment in India is available at Dr. Sharda Ayurveda. With the guidance from the experts along with recommended dietary modification provides the safe and best treatment."
    }
  },{
    "@type": "Question",
    "name": "What is the difference between ulcerative colitis and Crohn's disease?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Both of these diseases are associated with the intestines of the body. But the major difference is that ulcerative colitis is caused due to improper immune functioning but in Crohn's disease there is blockage of the intestine and ulceration in the intestinal tracts."
    }
  },{
    "@type": "Question",
    "name": "How does stress play role in developing ulcerative colitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If you are already suffering from ulcerative colitis stress can significantly flare up the disease. People with UC are more likely to diagnose with mental disorders such as stress, and anxiety."
    }
  },{
    "@type": "Question",
    "name": "How Ayurveda can help to treat ulcerative colitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurveda is a traditional practice whose applications are used till now to treat various life-threatening chronic diseases and through its application one can even avoid unwanted surgeries. If you are searching for the best ulcerative colitis Ayurveda consultants without a second thought consult Dr. Sharda Ayurveda for effective Ayurvedic treatment."
    }
  },{
    "@type": "Question",
    "name": "How can exercise beneficial for ulcerative colitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The benefits of exercise can be many that help in effective recovery from ulcerative colitis are:
•   Strengthens your bones 
•   Lower stress
•   Maintains muscle functioning"
    }
  },{
    "@type": "Question",
    "name": "What are the best vitamins for ulcerative colitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The vitamins which are best to recover from ulcerative colitis are vitamin B12, D, A, and K."
    }
  },{
    "@type": "Question",
    "name": "Is ulcerative colitis an autoimmune disease?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it’s an autoimmune disease where the immune system mistakenly recognizes the healthy cells of the body as the intruder or foreign antigen and generates a response against them."
    }
  },{
    "@type": "Question",
    "name": "What are the consequence if ulcerative colitis is left untreated?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If ulcerative colitis is left untreated the inflammation would spread throughout the colon thereby severely damaging the lining of the colon. But in children, if the disease is not treated timely can limit their growth and may interfere with their overall development."
    }
  }]
}
</script>

@stop