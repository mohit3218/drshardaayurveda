<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h1 class="ser-heading mt-4" >Ayurvedic Treatment for Flatulence</h1>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/flatulence">Flatulence</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION<span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatment">TREATMENT</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="definition">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/Flatulence.webp') }}" class="why-choose-us img-fluid" alt="men suffering from flatulence" title="men suffering from flatulence">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1">What is <b>Flatulence?</b></h1>
                <p>
                According to Ayurveda, flatulence is termed  Aadhmana. In this condition, there is stiffness in the stomach region and the person also suffers from constipation. Primarily, the Ayurvedic dosha that is responsible for flatulence is  Vata Dosha. 
                The Pitta Dosha is responsible for the Agni, i.e., the digestive fire. The imbalance of Pitta Dosha decreases the functions of the digestive system and forms gas. Hence, ama (toxins) accumulation     aggravates the Vata Dosha.
                Flatulence is when the person is facing excessive stomach/intestinal gas which further causes a lot of discomforts. The gas is released which contains the components which are hydrogen, methane, hydrogen, oxygen, and carbon dioxide. 
                <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                    So some pass gas with odor (Smell) i.e.- smell comes from the bacteria in the intestine that releases a gas that contains sulfur. Sometimes there is no odor (Smell) in passing gas. 
                    Gas gets collected in 2 ways-
                    <ul><li>As the food gets digested in the body some gases like hydrogen, methane, and carbon dioxide get collected.</li> 
                    <li>While eating food or drinking, some gases like Oxygen and Nitrogen get accumulated in the digestive tract.</li></ul>
                    Not everyone experiences the gas with the same food products. Flatulence treatment in Ayurveda provides everlasting results and recovery.
                    </p>
                    <div class="btn-ayurvedic">
                        <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                    </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec our-expert" style="background-color:#f3f3f3" id="causes">
    <div class="container-fluid" >
        <div class="row cut-row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Flatulence (Aadhmana)</b></h2>
                    <p font align="center" style="line-height:2">Farting too much can happen if you eat foods that are hard to digest. It can also be caused by a digestive problem like indigestion or irritable bowel syndrome (IBS). As per Ayurveda, Flatulence is linked to:</p>
                    <li>Eating heavy and fatty foods which are hard to digest. Processed food and carbonated drinks also cause flatulence. Plus, if you chew food at a fast pace then also the digestive system may suffer from gas.</li>
                    <li>If there is no proper bowel functioning, it can also lead to Flatulence. The improper bowel movements can be due to gallbladder disease, food allergies, peptic ulcers, or gallstones.</li>
                    <li>If a person stays more stressed, it will affect the digestive organs and cause gastric issues. Not only stress but other emotions like grief, and anxiety may also result in flatulence. </li>
                    <li>Disturbed sleep regime also leads to Flatulence. If not sleeping properly or dealing with insomnia, then also it can hamper the functions of the digestive system.</li>
                    <li>Sedentary and inactive lifestyle disturbs the metabolism of the body which leads to a bloated feeling. </li>
                    <li>Hormonal changes in women can cause them to feel bloated. During periods, pregnancy or menopause imbalance in hormones hinders the functioning of the digestive organs.</li>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container-strip" style="background: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<section class="our-product causes" style="background-color:#f8e6c5" id="symptoms">
    <h2 class="heading1"><b>Aadhmana or Flatulence</b> Experience the following Symptoms:</h2>
    <div class="wrp pt-5 mt-5 pb-5" style="padding-top: 0rem!important";>
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/flatulence-symptoms.webp') }}" class="img-fluid"alt="symptoms of flatulence">
        </div>
        <div class="cvn sym" >
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Heaviness in the stomach</p> <p>A person experiences a full, tight, and heavy stomach.</p>

            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Pain in the abdomen</p>
            <p>As the gas accumulates, pain is also experienced.</p>

            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Loss of appetite</p>
            <p>Due to heaviness, one often does not feel like eating anything.</p>

            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Bloating</p>
            <p>When the stomach is filled with excessive gas it is called Bloating.</p>

            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Burping</p>
            <p>An Individual burps quite often because of the accumulation of gas and sometimes the belching is quite loud and embarrassing.</p>

            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Indigestion and passing gas</p>
            <p>As the functioning is improper it leads to indigestion and one often releases the gas/farts.</p>

            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Joint and Muscle pain</p>
            <p>Gas sometimes gets collected in joints, thus causing pain.</p>
        </div>
    </div>
    </div>
    <div class="split"></div>
</section>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;"><b>Flatulence</b> Ayurvedic Treatment</h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurvedic medicines for gas and bloating are derived from plant-based products   which help in giving immense relief and no side effects.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                     Ayurveda focuses on improving diet to treat Flatulence. A healthy diet does wonders for the body. Ayurveda promotes a Satvic diet which helps in healing the body and at the same time, boosts immunity. 
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    <strong style="font-weight: bold;">Lifestyle-</strong> the way life is lived daily impacts the body too. Ayurveda believes in an active routine that helps in being physically as well as mentally fit. Along with medicines and decoctions exercise greatly helps in stomach gas treatment. 
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Flatulence ayurvedic treatment also involves yoga and meditation. The yoga asanas help in relieving the excess gas. 
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    There are also easily available effective ingredients in your kitchen to treat Flatulence. Follow simple home remedies to get relief from Flatulence.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="sR-v6exBo2s" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/sR-v6exBo2s.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
    </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Patient's <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="suraj singh flatulence patient's testimonial" title="suraj singh flatulence patient's testimonial" >
                            <h3 class="usrname">Suraj Singh</h3>
                            <p class="desig">Flatulence Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                For my stomach problems and farts, I went with Dr. Sharda Ayurveda. I recovered much more quickly thanks to their Ayurvedic therapy for flatulence. I followed completely to their Ayurvedic treatment and nutrition plan. I must state it is Ludhiana's top Ayurvedic centre for treating flatulence.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="saroja devi flatulence patient's testimonial" title="saroja devi flatulence patient's testimonial">
                            <h3 class="usrname">Saroja Devi</h3>
                            <p class="desig">Gastric Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I've used to have a stomach problem for two years. My issue grew over time and started to grow terrible. As recommended by one of my relatives, I sought the advice of Dr. Sharda Ayurveda for my healthcare issues. As soon as I began receiving their treatment, I noticed a difference in myself. I took their medications as prescribed by the doctors for three- months, and my stomach problems and farts disappeared.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="sukran flatulence patient's testimonial" title="sukran flatulence patient's testimonial">
                            <h3 class="usrname">Sukran</h3>
                            <p class="desig">Gastric Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Due to stomach problems, my grandma used to get upset. All throughout the day, she had intense difficulty. She was also constipated and didn't feel like eating. To receive her therapy, we went to see Dr. Sharda Ayurveda. Her constipation was resolved after their therapy with just couple of days, and she is currently recovering from her digestion problems as well. They deserve my special thanks.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                What is considered too much flatulence?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                        <li>Excessive intestinal gas</li> <li>Belching or flatulence more than 20 times a day — sometimes indicates a disorder such as: Celiac disease. Colon cancer.</li>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                How can I get rid of my daily passing of gas?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                            <ul>
                                <li>Chew food properly and slowly. Have a healthy diet that includes fresh fruits and vegetables.</li>
                                <li>Avoid carbonated drinks and Soda as they release carbon dioxide gas.</li>
                                <li>Go out for a walk/jog. Plan a mild workout routine.</li>
                                <li>Stop Smoking.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                What is the main causes of flatulence?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                            Excessive flatulence can be caused by swallowing more air than usual or eating food that's difficult to digest.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Sometimes my fart is smelly. Why does this happen?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                           A minor component of gas contains sulfur, which leads to smelly farts.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFive" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <i class="fa" aria-hidden="true"></i>
                                I burp more than other people, why does this happen?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion3">
                        <div class="card-body">
                        This depends mainly on your daily diet i.e. not only what you eat but also how you eat i.e. how you chew your food. Eat without gulping and slowly. 
                        Also, avoid drinking carbonated beverages and food that vitiate Vata Dosha. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion2">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingSix" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                    <i class="fa" aria-hidden="true"></i>
                                   Why do I get more gastric issues after consuming dairy products?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion2">
                            <div class="card-body">
                                It is very common as some people are unable to digest certain carbohydrates. Lactose present in milk causes gastric issues to many and is not easy for everyone to digest, thus, causing gastric issues.
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                What is the best Ayurvedic remedy for gastric?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion2">
                        <div class="card-body">
                            The easy home remedies to relieve gastric issues and flatulence are:
                            <li>Take Ginger juice daily or drink warm water with powdered ginger.</li>
                            <li>Cumin Seeds( Jeera) and Fennel seeds both are beneficial herbs that are often used to ease bloating.</li>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Why do I always have more gas at night in comparison to daytime?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion2">
                        <div class="card-body">
                            Having heavy meals at night causes flatulence. The gas is built upin the body due to the meal intake. Avoid having heavy food products at night. Also, do not sleep immediately after having dinner. Maintain a gap of 2-3 hours.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                How can I clean my digestive system by adopting Ayurveda?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion2">
                        <div class="card-body">
                           Panchakarma Therapy is used for the detoxification of the body. It begins with: 
                           <li>Virechana: In it, cleansing of a body is done using powders, pastes, or heated medicinal plants. This purgation is induced through medicinal herbs.</li> <li>Vaman: A person is made to do forced vomit or purging through herbal medicinal treatment which eliminates the toxins from the body.</li>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                What is the worst food that causes Flatulence?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion2">
                        <div class="card-body">
                            Avoid eating-
                            <li>Raw Onions, Beans</li>
                            <li>Carbonated drinks</li>
                            <li>Broccoli, Cabbage, Asparagus, and Cauliflower</li>
                            <li>Processed, Spicy & Junk foods</li>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Flatulence",
    "item": "https://www.drshardaayurveda.com/flatulence"  
  }]
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What is considered too much flatulence?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Excessive intestinal gas — belching or flatulence more than 20 times a day — sometimes indicates a disorder such as: Celiac disease. Colon cancer."
    }
  },{
    "@type": "Question",
    "name": "How can I get rid of my daily passing of gas?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Chew food properly and slowly. Have a healthy diet that includes fresh fruits and vegetables. Avoid carbonated drinks and Soda as they release carbon dioxide gas. Go out for a walk/jog. Plan a mild workout routine. Stop Smoking."
    }
  },{
    "@type": "Question",
    "name": "What is the main causes of flatulence?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Excessive flatulence can be caused by swallowing more air than usual or eating food that's difficult to digest."
    }
  },{
    "@type": "Question",
    "name": "Sometimes my fart is smelly. Why does this happen?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "A minor component of gas contains sulfur, which leads to smelly farts."
    }
  },{
    "@type": "Question",
    "name": "I burp more than other people, why does this happen?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "This depends mainly on your daily diet i.e. not only what you eat but also how you eat i.e. how you chew your food. Eat without gulping and slowly. Also, avoid drinking carbonated beverages and food that vitiate Vata Dosha."
    }
  },{
    "@type": "Question",
    "name": "Why do I get more gastric issues after consuming dairy products?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It is very common as some people are unable to digest certain carbohydrates. Lactose present in milk causes gastric issues to many and is not easy for everyone to digest, thus, causing gastric issues."
    }
  },{
    "@type": "Question",
    "name": "What is the best Ayurvedic remedy for gastric?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The easy home remedies to relieve gastric issues and flatulence are:Take Ginger juice daily or drink warm water with powdered ginger. Cumin Seeds( Jeera) and Fennel seeds both are beneficial herbs that are often used to ease bloating."
    }
  },{
    "@type": "Question",
    "name": "Why do I always have more gas at night in comparison to the daytime?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Having heavy meals at night causes flatulence. The gas is built in the body due to the meal intake. Avoid having heavy food products at night. Also, do not sleep immediately after having dinner. Maintain a gap of 2-3 hours."
    }
  },{
    "@type": "Question",
    "name": "How can I clean my digestive system by adopting Ayurveda?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Panchakarma Therapy is used for the detoxification of the body. It begins with: Virechana: In it, cleansing of a body is done using powders, pastes, or heated medicinal plants. This purgation is induced through medicinal herbs. Vamana: A person is made to do forced vomit or purging through herbal medicinal treatment which eliminates the toxins from the body."
    }
  },{
    "@type": "Question",
    "name": "What is the worst food that causes Flatulence?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Raw Onions, Beans Carbonated  drinks Broccoli, Cabbage, Asparagus, and Cauliflower Processed, Spicy  & Junk foods"
    }
  }]
}
</script>

@stop