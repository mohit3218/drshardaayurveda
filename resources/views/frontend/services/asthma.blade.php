<?php
/**
 * Asthma Services Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4">Ayurvedic Treatmet for Asthma (Tamaka Shwasa)</h3>
                    <p style="font-size: 22px;">Breath Healthy With Ayurveda</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/asthma">Asthma</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</section>

<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a  href="#types">TYPES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose"id="treatment">>
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/asthma-treatment.webp') }}" class="why-choose-us img-fluid" alt="asthma treatment"title="Asthma Ayurvedic Treatment">
                </div>
            </div>
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1">What is <b>Asthma?</b></h1>
                <p>
                    Asthma is a disorder of the lungs, in which an individual’s airways become inflamed, narrow, swelled, and produce extra mucus, which makes breathing difficult.
                    According to Ayurveda, Asthma is termed"Swasa Roga.”It is a respiratory disorder caused by an imbalance of the Vata dosha (air element) and Kapha dosha (water and earth element) in the body.Asthma is caused by the accumulation of toxins (Ama) in the body, which disrupts the balance of the doshas and blocks the flow of prana (life force) in the respiratory system.
                    Ayurvedic Treatment for Asthma plays a vital role in the healing process of asthma.  ‘Pranavaha Sroto Vikara’ is the term used for asthma in Ayurvedic text that causes regular breathing difficulties. 
                  <span id="dots" data-aos="fade-up" data-aos-duration="3000"></span>
                </p>
                <div class="btn-ayurvedic">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec" id="types"> 
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="heading1">Types of <b>Asthma</b></h2>
                <p font align="center" style="line-height:2">These are the main types of Asthma. Moreover, the Treatment of Asthma in Ayurveda has amazing healing power.</p>
            </div>
        </div>
        <div class="row cust-iba">
            <div class="owl-carousel owl-theme">
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/Allergic Asthma.webp') }}"  alt="Allergic Asthma" title="Allergic Asthma">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Allergic Asthma</h4>
                            <p class="card-text">It is a breathing condition where the airways get tightened because of an allergic reaction. It happens because of various common factors like pollen, mold spores, dust mites, pet dander, and cockroach debris. This type of asthma is fairly seen in children and adults.</p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/Non-allergic Asthma.webp') }}"  alt="Non-allergic Asthma" title="Non-allergic Asthma">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Non-allergic Asthma</h4>
                            <p class="card-text">
                                Non-allergic asthma is a condition that tends to affect a significant portion of individuals after they reach middle age. It does not take place due to an allergic reaction but by factors such as cold air, infections, or stress.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/Cough-Variant-Asthma.webp') }}" class="card-img-top lazyload" alt="Cough Variant Asthma" title= "Cough Variant Asthma">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Cough-Variant Asthma</h4>
                            <p class="card-text">
                                The specific feature is a dry, nonproductive cough. There can be no traditional asthma symptoms, such as shortness of breath or wheezing. The only symptom which persists is a continued cough.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/Occupational-asthma.webp') }}" alt="Occupational asthma" title="Occupational asthma">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Occupational asthma</h4>
                            <p class="card-text">
                                 The lungs and airways become easily inflamed when exposed to certain triggers (flare-ups). Such triggers include catching a cold and inhaling pollen. Childhood asthma can be a cause that interferes with the sports, play, studies, school, and sleep schedules of children. 
                            </p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/Childhood-asthma.webp') }}"  alt="Childhood asthma" title="Childhood asthma">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Childhood asthma</h4>
                            <p class="card-text">The lungs and airways become easily inflamed when exposed to certain triggers (flare-ups). Such triggers include catching a cold and inhaling pollen. Childhood asthma can be a cause that interferes with the sports, play, studies, school, and sleep schedules of children.</p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/Adult-onset-asthma.webp') }}"  alt="Adult-onset asthma" title="Adult-onset asthma">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Adult-onset asthma</h4>
                            <p class="card-text">When asthma symptoms appear and are diagnosed in adults older than 20 years of age, it is usually known as adult-onset asthma. It is different from childhood asthma, which starts in early childhood. Adult-onset asthma can occur in people who had never experienced asthma symptoms before or in those who had asthma as children but had a period of remission.</p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/Exercise-induced-asthma.webp') }}"  alt="Exercise induced asthma" title="Exercise induced asthma">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Exercise-induced asthma</h4>
                            <p class="card-text">The medical term for this condition is exercise-induced bronchoconstriction. When the airways squeeze or narrow during intense physical activity. It causes shortness of breath, wheezing, coughing, and other symptoms during or after exercise. It occurs frequently in dry and cold air. Eventually, the airway narrows for 5 to 20 minutes after the beginning of physical activity, making it difficult to breathe.</p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/Night-time-_Nocturnal_-Asthma.webp') }}"  alt="Night time (Nocturnal) Asthma" title="Night time (Nocturnal) Asthma">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Night time (Nocturnal) Asthma</h4>
                            <p class="card-text">In the night, asthma symptoms are much higher during sleep because asthma is powerfully influenced by the sleep-wake cycle (circadian rhythms). Your asthma symptoms of coughing, wheezing, and difficulty in breathing are common and dangerous, particularly at night.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!--         <div class="row">
            <div class="col-md-12">
                <div class="btn-ayurvedic">
                    <a href="#" class="btn-appointment">Find More Treatment</a>
                </div>
            </div>
        </div> -->
    </div>
</section>

<section class="our-product causes" style="background-color:#f3f3f3" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Asthma</b></h2>
                    <p font align="center">Here are the causes of asthma, which are as follows:</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Poor digestion
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Poor digestion is one of the primary causes of asthma. It leads to the formation of toxins (ama) in the body, which, ultimately aggravates the Kapha Dosha and leads to asthma.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Excessive consumption of cold and heavy foods
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                Foods that are cold, heavy, and oily, like dairy products, fried foods, and ice cream, can aggravate the Kapha Dosha which leads to difficulty in breathing.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Exposure to allergens
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                Allergens in the atmosphere such as dust, pollen, and animal dander, can aggravate the Kapha Dosha.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Emotional stress
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">
                                It is also an important factor that can lead to anxiety and it is hazardous for health.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Improper lifestyle habits
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                            <div class="card-body">
                                Lack of exercise and improper irregular sleeping patterns and consumption of alcohol, smoking, and other drugs, can contribute to the development of asthma.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>			

<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/Asthma-Symptoms.webp') }}" class="img-fluid" alt="Asthma Symptoms"title="Asthma Symptoms">
        </div>
        <div class="cvn sym" >
            <h3>What are symptoms of <span>Asthma</span></h3>
            <li>The main symptoms of asthma in adults is difficulty in breathing or shortness of breath. The individual feels as if his chest is tight, and he is unable to take deep breaths.</li>
            <li>Wheezing is a whistling sound that is heard when an individual exhales. It is caused by the narrowing of the airways.</li>
            <li>A persistent cough that worsens at night is one of the symptoms of asthma. The cough might be dry or with phlegm.</li>
            <li>An individual can feel a tightness or pressure in the chest, making it difficult to breathe.</li>
            <li>Asthma can cause fatigue and weakness due to the increased effort required to breathe.</li>
            <li>Asthma attacks can cause anxiety and restlessness in an individual, making it difficult for them to stay calm.</li>
        </div>
    </div>
<div class="split"></div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title"style="font-size: 24px !important;">Ayurvedic Treatment For <b>Asthma</b></h2>
                <p>Ayurvedic treatment for asthma strengthens the lungs. Ayurvedic herbs have miraculous power to heal the symptoms of Asthma with ease.</p>
                <li><b>Panchakarma Therapy:</b> Panchakarma therapies that can be useful in managing and healing asthma are Vamana(Vomiting procedure), Virechana (purgation therapy), Nasya (Administration of medicated oils through the nose), Abhyanga (Massage of the body), Swedana (Herbal fomentation).</li>
                <li><b>Natural herbs:</b> Ayurvedic treatment for asthma use natural herbs, and medicated oils, to manage the symptoms of asthma.</li>
                <li><b>Lifestyle changes:</b> Dietary regimes and lifestyle changes, such as yoga and meditation, and Pranayama(breathing exercises), can improve the overall health and well-being of the person. These lifestyle changes can also help in reducing stress, which is a common flare-up of asthma.</li>
                <li><b>Holistic approach:</b> Ayurveda follows the holistic approach that focuses on balancing the body, mind, and soul, and helps reduce the frequency and severity of asthmatic attacks.</li>
                <li><b>Stops recurrence:</b> Ayurveda aims to treat the root cause of the disease, which can provide long-term benefits as well as stops the recurrence of asthma.</li>
                Significantly, Ayurveda has the treasure of natural herbs, yoga, meditation, and breathing exercises (Pranayama), to manage and prevent the recurrence of asthma symptoms.

            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="_-4_fagZucU" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/asthma-thumbnail.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Patient <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Asthma Testimonial" title="Aman Kaur Testimonial">
                            <h3 class="usrname">Aman Kaur</h3>
                            <p class="desig">Asthma Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                My grandmother has been suffering from Asthma for the past 10 years and was dependent on Inhaler. We went to Dr. Sharda Ayurveda for treatment of Asthma. Dr. Mukesh Sharda mam guided us nicely and started with Ayurvedic treatment for Asthma. Therefore, my grandmother recovered from Asthma totally from its root cause as well as there is no recurrence. Thus, this was the best treatment for asthma in Ayurveda which my grandmother experienced.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Asthma Testimonial" title="Dilpreet Singh Testimonial" >
                            <h3 class="usrname">Dilpreet Singh</h3>
                            <p class="desig">Asthma Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Dr. Sharda Ayurveda is best for the Ayurvedic treatment of Asthma. I used to face breathing issues, sometimes it was quite difficult to breathe at night in the winter season. After Ayurvedic treatment for Asthma, I am admiringly happy with the results and I am recovering from my breathing issues.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Asthma Testimonial" title="Ahmad Khan Testimonial">
                            <h3 class="usrname">Ahmad Khan</h3>
                            <p class="desig">Asthma Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                My 17-year-old son was completely dependent on Inhaler as he had chronic Asthma. We started with Ayurvedic medicines from Dr. Sharda Ayurveda. I wholeheartedly want to thank Dr. Mukesh Sharda for treating my son’s situation and making him healthy. He started breathing healthily and naturally without any use of an Inhaler.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="dsa-why-choose" id="testimonials">
    <div class="container-fluid">
        <h2 text align="center"style="font-size: 32px !important;">Kind Words of Asthma Patients -<strong>Dr. Sharda Ayurveda</strong></h2>
        <br>    
        
        <div class="row cut-row" >
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div id="dna_video">
                    <div class="youtube-player" data-id="pujSQbxCBK0" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                        <img src="{{ URL::asset('front/images/pujSQbxCBK0.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                        <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div id="dna_video">
                    <div class="youtube-player" data-id="BodQAXFJA5o" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                        <img src="{{ URL::asset('front/images/BodQAXFJA5o.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                        <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                Can Ayurveda permanently cure Asthma? 
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            Ayurvedic treatment for asthma aims to treat the root cause of the disease and prevent the recurrence of asthma attacks. It is important to adopt a healthy lifestyle, changes in diet, and exercise regularly for the permanent cure of Asthma. Additionally, consulting an Ayurvedic expert practitioner will eventually help in getting rid of Asthma permanently.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                How to get rid of mucus and cough at home?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            You can make a decoction (Kadha) of Ginger, Basil, Cloves, and Cinnamon and consume it lukewarmly twice a day.
                            <li>Honey tea: Honey tea can be made by mixing about 2 teaspoons of honey with warm water or tea.</li>
                            <li>Saltwater gargles: Saltwater helps in reducing mucus and phlegm from your throat.</li>
                            <li>Steam inhalation: Steam helps in clearing mucus or phlegm from the nasal passage and out of your lungs.</li>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                What causes Asthma?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            There are several reasons that lead to asthma. It is usually caused by a mixture of hereditary factors i.e.- inherit from birth and some environmental factors. Certain allergens from houses, dust, mites, and pets are the common causes. Other allergens, such as pollen can also cause Asthma.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                What drinks to avoid if you have asthma?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                            Alcoholic drinks containing higher amounts of histamine and sulfites should be avoided at a high priority. Apart from this, beverages like coffee and tea should also be avoided.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFive" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <i class="fa" aria-hidden="true"></i>
                                Which breathing exercises are best for asthma?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="card-body">
                            The most useful breathing exercises (Pranayama) are as follows: 
                            Nadi Shodhana, which is also known as Anuloma Viloma. It clears up the blocked energy channels in your body and helps it heal. In addition, Bhastrika pranayama is done with Kapal Bhati to cleanse the airways of the body. These three breathing exercises are best for the patients who are suffering from asthma.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                What drink is good for asthma?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                        <div class="card-body">
                            Drinking warm drinks gives benefits for conditions like Asthma. Such as drinking herbal tea or decoction (Kadha) made up of herbs such as ginger, licorice, and basil has soothing properties and can promote respiratory health.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                What drinks to avoid if you have asthma?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                        <div class="card-body">
                            Cold juices, cold drinks, and cold shakes should be excluded from the diet as they can aggravate the symptoms of Asthma.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                Do Asthma triggers more in cold weather?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                        <div class="card-body">
                            Yes, to some people, asthma triggers more in cold weather conditions due to seasonal changes. For such patients, it is mandatory to carry your medicines with you, especially in changing weather and environmental conditions.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                How long does an Asthma attack last?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                        <div class="card-body">
                            An asthma attack can last for several continuous hours if not treated timely. Acute asthma attacks last for a few minutes but a chronic attack takes several hours and proper medication is required. Medical treatment is mandatory in the case of shortness of breath.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Can Asthma damage the lungs?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                        <div class="card-body">
                            Asthma can cause hazardous damage to your lungs if not treated early and in the right way. The finest Ayurvedic treatment for Asthma can protect you timely and early. 
                            The inflammation in the airways can cause damage to the bronchial tubes and the lungs over time, leading to chronic obstructive pulmonary disease (COPD).
                            Repeated episodes of asthma attacks can cause scarring and thickening of the airway walls, which can permanently reduce the airway diameter and obstruct the airflow. This condition is known as airway remodeling, and it can lead to irreversible damage to the lungs.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Asthma",
    "item": "https://www.drshardaayurveda.com/asthma"  
  }]
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "Can Ayurveda permanently cure Asthma?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurvedic treatment for asthma aims to treat the root cause of the disease and prevent the recurrence of asthma attacks. It is important to adopt a healthy lifestyle, changes in diet, and exercise regularly for the permanent cure of Asthma. Additionally, consulting an Ayurvedic expert practitioner will eventually help in getting rid of Asthma permanently."
    }
  },{
    "@type": "Question",
    "name": "How to get rid of mucus and cough at home?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "You can make a decoction (Kadha) of Ginger, Basil, Cloves, and Cinnamon and consume it lukewarmly twice a day.Honey tea: Honey tea can be made by mixing about 2 teaspoons of honey with warm water or tea. Saltwater gargles: Saltwater helps in reducing mucus and phlegm from your throat. Steam inhalation: Steam helps in clearing mucus or phlegm from the nasal passage and out of your lungs."
    }
  },{
    "@type": "Question",
    "name": "What causes Asthma?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "There are several reasons that lead to asthma. It is usually caused by a mixture of hereditary factors i.e.- inherit from birth and some environmental factors. Certain allergens from houses, dust, mites, and pets are the common causes. Other allergens, such as pollen can also cause Asthma."
    }
  },{
    "@type": "Question",
    "name": "What drinks to avoid if you have asthma?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Alcoholic drinks containing higher amounts of histamine and sulfites should be avoided at a high priority. Apart from this, beverages like coffee and tea should also be avoided."
    }
  },{
    "@type": "Question",
    "name": "Which breathing exercises are best for asthma?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The most useful breathing exercises (Pranayama) are as follows: Nadi Shodhana, which is also known as Anuloma Viloma. It clears up the blocked energy channels in your body and helps it heal. In addition, Bhastrika pranayama is done with Kapal Bhati to cleanse the airways of the body. These three breathing exercises are best for the patients who are suffering from asthma."
    }
  },{
    "@type": "Question",
    "name": "What drink is good for asthma?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Drinking warm drinks gives benefits for conditions like Asthma. Such as drinking herbal tea or decoction (Kadha) made up of herbs such as ginger, licorice, and basil has soothing properties and can promote respiratory health."
    }
  },{
    "@type": "Question",
    "name": "What drinks to avoid if you have asthma?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Cold juices, cold drinks, and cold shakes should be excluded from the diet as they can aggravate the symptoms of Asthma."
    }
  },{
    "@type": "Question",
    "name": "Do Asthma triggers more in cold weather?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, to some people, asthma triggers more in cold weather conditions due to seasonal changes. For such patients, it is mandatory to carry your medicines with you, especially in changing weather and environmental conditions."
    }
  },{
    "@type": "Question",
    "name": "How long does an Asthma attack last?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "An asthma attack can last for several continuous hours if not treated timely. Acute asthma attacks last for a few minutes but a chronic attack takes several hours and proper medication is required. Medical treatment is mandatory in the case of shortness of breath."
    }
  },{
    "@type": "Question",
    "name": "Can Asthma damage the lungs?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Asthma can cause hazardous damage to your lungs if not treated early and in the right way. The finest Ayurvedic treatment for Asthma can protect you timely and early. The inflammation in the airways can cause damage to the bronchial tubes and the lungs over time, leading to chronic obstructive pulmonary disease (COPD). Repeated episodes of asthma attacks can cause scarring and thickening of the airway walls, which can permanently reduce the airway diameter and obstruct the airflow. This condition is known as airway remodeling, and it can lead to irreversible damage to the lungs."
    }
  }]
}
</script>

<style>
    .carousel-inner {position: relative;width: 100%;overflow: hidden;}
    .carousel-item {position: relative;float: left;width: 100%;margin-right: -100%;-webkit-backface-visibility: hidden;backface-visibility: hidden;transition: -webkit-transform .6s ease-in-out;transition: transform .6s ease-in-out;transition: transform .6s ease-in-out,-webkit-transform .6s ease-in-out;}
    .card-body {padding-top: 1.5rem;padding-bottom: 1.5rem;-webkit-border-radius: 0!important;border-radius: 0!important;flex: 1 1 auto;-ms-flex: 1 1 auto;}
    .card .card-body a h3{font-size: 18px;color: #ff9000;line-height: 1.4;}
    .card .card-body h3{color: #373433;font-size: 18px;    line-height: 1.4;}
    .font-weight-bold {font-weight: 700!important;}
    .card-title {margin-bottom: .75rem;}
    .card-img-top {width: 100%;border-top-left-radius: calc(.25rem - 1px);border-top-right-radius: calc(.25rem - 1px);}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            responsiveClass: !0,
            navigation : true,
            autoplay: true,
            loop: true,
            smartSpeed: 2500,
            singleItem : true,
            navigationText: ["<i class='fa fa-angle-left fa-2'></i>", "<i class='fa fa-angle-right fa-2'></i>"],
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                480: {
                    items: 1,
                    nav: !1
                },
                768: {
                    items: 2,
                    nav: !1
                },
                1000: {
                    items: 2,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1024: {
                    items: 3,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1366: {
                    items: 3,
                    nav: !1
                }
            }
        })
    });

</script>
@stop