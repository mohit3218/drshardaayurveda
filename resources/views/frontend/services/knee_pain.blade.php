<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Knee Pain</h3>
                    <p style="font-size: 24px;">Protect your joints with timely treatment</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/knee-pain">Knee Pain</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/knee-pain.webp') }}" class="why-choose-us img-fluid" alt="Knee pain treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Knee Pain</b> Ayurvedic Treatment</h1>
                <p>
                Knee pain is referred to as a pain in the knee or around the knee. It is symptoms caused due to sudden or overuse injury or degenerative changes or arthritis in the knee. Pain in the knee or around the knee indicates a condition affecting the knee joint itself or the soft tissue around the knee. Knee pain may begin as mild discomfort that slowly gets worse. The knee joint is a weight-bearing joint of the body which bends and straightens as per requirement. It is a simple hinged joint that also can twist and rotate. Knee pain can affect people of all ages but it is a common complaint among adults associated with wear and tear like walking, bending, standing, and lifting. It can get aggravated by physical exercise or activity or can be caused by aging, injury or repeated stress on the knee. Location of knee pain varies according to cause and according to the structure involved.
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                    <p>
                    The knee might be seen swollen with a torn meniscus, fracture of a bone, backer cyst, or with other pathophysiology. The severity of the knee also varies from person to person. Ayurvedic treatment for knee pain requires no surgery to get rid of the pain. Those who have been suggested for knee surgery or knee transplant can get their Ayurvedic treatment done from the best <strong>Ayurvedic hospital for knee pain treatment</strong> and can see the most effective results within few days of treatment.
                    </p>
                    <div class="btn-ayurvedic">
                        <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                    </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Knee Pain</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Acute Injury to Knee
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                            Due to injuries such as fracture, ligament tear, or meniscal tear.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Gout
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                An increased value of uric acid in the blood may also cause knee pain
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Osteoarthritis
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                            It is due to aging and wear and tear on the joint hence causing knee pain. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Torn Meniscus
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                            Cartilage present in the knee act as shock absorbers between shin bone and thigh bone get torn leads to knee pain.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Obesity
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                            Obesity also causes knee pain because excess weight increases stress on the knee joint which further leads to pain.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz px-2.5" >
            <img src="{{ URL::asset('front/images/knee-pain-symptoms.webp') }}" class="img-fluid"alt="Knee pain symptoms">
        </div>
        <div class="cvn sym" >
            <h3><span>Symptoms of</span> Knee Pain </h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Difficulty to Bear Weight</p>
            <p>Difficulty in walking and weight-bearing due to instability of the knee.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Difficulty in Walking</p>
            <p>Pain walking up or down due to ligament damage.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Locking of Knee</p>
            <p>It gets hard to bend the knee and move. Movement gets restricted. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Redness</p>
            <p>Swelling, stiffness, and redness can be seen on touching. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Inability to Extend Knee</p>
            <p>Unable to completely straighten the knee.</p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Benefits of <b>Ayurvedic Treatment for Knee Pain</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    According to Ayurveda accumulation of Ama leads to joint disorder. Sandhivata is a term given to <a class="green-anchor" href="https://www.drshardaayurveda.com/osteoarthritis">osteoarthritis</a> of the knee
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    JanuVasti is one of the effective treatments in chronic knee pain in which pooling warm oil over the knee is done supported by gram flour mix.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Massage with Ayurvedic medicated oil will increases blood circulation and provides relief from pain. Knee pain treatment in Ayurveda never recommends knee surgery or transplant as Ayurvedic herbs potential them to heal it naturally. 
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    The basic line of treatment involves balancing of vitiated factors such as normalizing Agni, Ama, Vata. 
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Herbs that possess analgesics and anti-inflammatory properties are used commonly in the management of disease in Ayurveda which completely heals knee pain. 
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="99Kq9z0Hwjw" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/99Kq9z0Hwjw.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>    
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Arshdeep Kaur</h3>
                            <p class="desig">Knee Pain Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                My grandmother was suffering from knee pain for 4 years. She took painkillers and got relief for some time. In the end doctors just suggested her to get her knees replaced. She was very scared to get it done. We got to know about Dr. Sharda Ayurveda knee pain Ayurvedic treatment. We consulted them. As the condition of my grandmother’s knees was not okay they told us earlier that she will be all fine but will take some time. We began with treatment and within just 20 days she was relieved from pain. But for her complete recovery we continued the complete course. We are very thankful to them
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Kulwant Singh Dhillon</h3>
                            <p class="desig">Knee Pain Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I consulted Dr. Sharda Ayurveda for my knee pain treatment. I was suffering with knee pain from very long. I consulted many doctors but got no relief. I thought of getting Ayurvedic medicine and began my treatment from <a class="green-anchor" href="https://www.drshardaayurveda.com/">Dr. Sharda Ayurveda</a>. I religiously followed the diet prescribed by them and followed all the precautions with Ayurvedic medicines. Within just few days I was relieved from pain.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1"><b>FAQ'S</b></h2>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                            Is walking good for knee pain?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                        Yes, walking is good because it improves blood circulation to the joint and also serves the nutrients necessary for knee joint activity. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                How to keep knees healthy?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                        Weight control is essential for healthy knees. Regular exercise, healthy eating, and maintaining an active lifestyle are a must for healthy knees.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                What are the symptoms of cartilage damage in the knee?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                        Joint pain, swelling, stiffness, and joint locking are the common symptoms of cartilage damage.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Can yoga help in knee pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                        Yes, yoga helps in knee pain because yoga is a low-impact exercise that increases the heart rate and minimizes the amount of stress on the joints. It removes pain and increases the quality of life.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Does painkillers are safe for knee pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                        Non-steroidal anti-inflammatory drugs are strictly avoided because it reduces the blood flow to the kidney. High doses and long-term use of painkillers may even harm healthy kidneys. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    Is knee replacement is a good option or not?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                            We never recommend knee surgery or transplant. It is an unsafe, unsure and expensive procedure that provides temporary hope for living. It can provide you relief for a certain time period but the situation gets more worse in the coming years. <strong>Knee pain treatment clinics in India</strong> have sure results.
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                What is better for knee pain heat or cold fermentation? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                        In a sprain, tear, tendonitis, swelling usually ice packs relieve pain but when this pain is chronic, hot fermentations relieves symptoms. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                What sleeping posture is advised at the time of sleeping?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                            <ul>
                                <li>Place a pillow under your knee for support.</li>
                                <li>Lying on one side, keep the knee in a flexed position to minimize pain.</li>
                                <li>Never cross legs while sleeping.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                How can you treat inner knee pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                        Rest is most important as the knee joint gets injured due to exercise or after running. Use of Ice pack three to four times to reduce inflammation and pain. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                How long does Ayurvedic treatment takes to get rid of knee pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                        Results of Ayurvedic treatment vary from individual to individual as everyone has a different body type and the condition varies.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Knee Pain",
    "item": "https://www.drshardaayurveda.com/knee-pain"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "Is walking good for knee pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, walking is good because it improves blood circulation to the joint and also serves the nutrients necessary for knee joint activity."
    }
  },{
    "@type": "Question",
    "name": "How to keep knees healthy?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Weight control is essential for healthy knees. Regular exercise, healthy eating, and maintaining an active lifestyle are a must for healthy knees."
    }
  },{
    "@type": "Question",
    "name": "What are the symptoms of cartilage damage in the knee?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Joint pain, swelling, stiffness, and joint locking are the common symptoms of cartilage damage."
    }
  },{
    "@type": "Question",
    "name": "Can yoga help in knee pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, yoga helps in knee pain because yoga is a low-impact exercise that increases the heart rate and minimizes the amount of stress on the joints. It removes pain and increases the quality of life."
    }
  },{
    "@type": "Question",
    "name": "Does painkillers are safe for knee pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Non-steroidal anti-inflammatory drugs are strictly avoided because it reduces the blood flow to the kidney. High doses and long-term use of painkillers may even harm healthy kidneys."
    }
  },{
    "@type": "Question",
    "name": "Is knee replacement is a good option or not?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "We never recommend knee surgery or transplant. It is an unsafe, unsure and expensive procedure that provides temporary hope for living. It can provide you relief for a certain time period but the situation gets more worse in the coming years. Knee pain treatment clinics in India have sure results."
    }
  },{
    "@type": "Question",
    "name": "What is better for knee pain heat or cold fermentation?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "In a sprain, tear, tendonitis, swelling usually ice packs relieve pain but when this pain is chronic, hot fermentations relieves symptoms."
    }
  },{
    "@type": "Question",
    "name": "What sleeping posture is advised at the time of sleeping?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "1.   Place a pillow under your knee for support. 
2.  Lying on one side, keep the knee in a flexed position to minimize pain. 
3.  Never cross legs while sleeping."
    }
  },{
    "@type": "Question",
    "name": "How can you treat inner knee pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Rest is most important as the knee joint gets injured due to exercise or after running. 
Use of Ice pack three to four times to reduce inflammation and pain."
    }
  },{
    "@type": "Question",
    "name": "How long does Ayurvedic treatment takes to get rid of knee pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Results of Ayurvedic treatment vary from Individual to Individual as everyone has a different body type and the condition varies."
    }
  }]
}
</script>
@stop