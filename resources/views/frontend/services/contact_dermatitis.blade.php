<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')

<section class="types-of-eczema mt-5" id="typesOfEczema">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-3 col-sm-12 mb-2 eczemaCategories">
                <ul>
                    <li><a href="{{ 'contact-dermatitis' }}">Contact Dermatitis</a></li>
                    <li><a href="{{ 'types-of-eczema' }}">Types of Eczema</a></li>
                    <li><a href="{{ 'stasis-dermatitis' }}">Stasis Dermatitis</a></li>
                </ul>
            </div>
            <div class="col-xl-8">
                <h1 class="heading1">What is Contact dermatitis?</h1>
                <p> 
                    Contact dermatitis is a type of skin inflammation that occurs when the skin comes into direct contact with a substance that triggers an allergic reaction or irritates the skin. It is a common condition characterized by redness, itching, and skin irritation in the area of contact. There are two types of contact dermatitis
                </p>
                <h2 class="heading1">Types of Contact Dermatitis</h2>
                <p>There are two types of contact dermatitis:</p>
                   <h3 class="heading1">Allergic contact dermatitis</h3>
                        <p>Allergic contact dermatitis is an inflammation of the skin that arises when the immune system responds to the presence of a specific allergen upon contact. It only affects individuals who are sensitized to a particular allergen.</p>
                    <h3 class="heading1">Irritant contact dermatitis</h3>
                    <p>Irritant Contact dermatitis arises when the skin is directly irritated by a substance, such as harsh chemicals, detergents, solvents, or prolonged exposure to water. It does not involve an immune system response and can affect anyone exposed to the irritant substance.</p>
                    <p>In Ayurveda, Udarda is a similar condition to Contact dermatitis described by Madhava Acharya. Ayurvedic Treatment of Contact Dermatitis is excellent and heals the condition from its root cause with herbs and changes in diet and lifestyle.</p>

                <h2 class="heading1">Who is most at risk for contact dermatitis?</h2>
                    <p>People with atopic dermatitis have a higher risk of developing contact dermatitis due to weakened skin barrier function. This makes them more vulnerable to irritants and allergens.</p>

                    <p>Professionals from the food industry, healthcare workers, beauticians, and hairdressers who have prolonged exposure to irritants are at high risk of contact dermatitis.</p>

                    <p>Any condition that compromises the skin's natural protective barrier, such as cuts, burns, or skin diseases, can make the skin more susceptible to contact dermatitis.
                    Women tend to be more affected by certain types of contact dermatitis, such as allergic reactions to cosmetics or jewellery containing nickel as they wear cosmetics often and wear jewelry on a regular basis.</p>

                   
                <h2 class="heading1">Symptoms of Contact Dermatitis </h2>
                    <p>Symptoms of Contact dermatitis usually show up near where touched. The severity and appearance vary in every individual. Here are the symptoms of Contact dermatitis:</p>
                    <ul class="li-format">
                        <li>The most common symptom is a red, itchy rash that appears in the area of the skin that came into contact with the irritant or allergen. The rash can be localized or spread to nearby areas.</li>
                        <li>Intense itching is a common symptom of contact dermatitis. The urge to scratch can worsen the condition and potentially lead to skin damage or infection.
                        Swelling along with redness can occur often.</li>
                        <li>Sometimes, blisters can also take place which can be fluid-filled blisters that may ooze and develop crust over it.</li>
                        <li>Over time, skin can become dry, and scaly, or even develop cracks or fissures. 
                        A few individuals can experience a burning or stinging sensation in the affected area.</li>
                        <li>The symptoms of contact dermatitis can cause mild pain, and tenderness particularly if the skin is scratched or irritated.
                        </li></ul>

                <h2 class="heading1">Causes of Contact Dermatitis</h2>
                    <p>There are various irritants and allergens that come in contact with the skin and cause contact dermatitis which are as follows:
                        <ul class="li-format">
                        <li><b>Harsh chemicals:</b> Exposure to chemicals like detergents, soaps,  acids, alkalis, solvents, and cleaning products can irritate the skin.</li>
                        <li><b>Repeated friction:</b> Friction on the skin by wearing certain clothes such as synthetic clothes or wearing ill-fitted shoes.</li>
                        <li><b>Metals in ornaments:</b> Metals like Nickel, cobalt, and chromium found in jewelry can flare up contact dermatitis.</li>
                        <li><b>Strong Fragrances or perfumes:</b> Numerous scents, perfumes, or products with strong odors can trigger contact dermatitis.</li>
                        <li><b>Medications:</b> Topical Ointments which are anti-fungal, anti-bacterial or corticosteroids can also cause contact dermatitis.</li>
                        <li><b>Contact with Allergenic Plants:</b> Coming in contact with plants such as poison sumac, poison ivy, poison oak, and other allergenic plants can lead to allergic contact dermatitis.
                        </li></ul>
                    </p>
                    
                <h2 class="heading1">Contact Dermatitis Ayurvedic Treatment</h2>
                <p>Contact Dermatitis Treatment consists of various Ayurvedic Herbs, dietary changes, Lifestyle modifications, and yoga asanas which are as follows:</p>

                <h3 class="newh3">Ayurvedic herbs</h3>
                    <p>Naturally obtained herbs have wonderful and rejuvenating effects on the skin and nourish the skin as well. These herbs have anti-inflammatory, anti-fungal, and anti-bacterial properties which relieve the symptoms of contact dermatitis from its root cause. In addition, these herbs have cooling and soothing effect on the skin.</p>

                <h3 class="newh3">Dietary Changes</h3>
                    <p>The role of diet in controlling inflammation in the body is pivotal. Adopting an anti-inflammatory diet can reduce inflammation in the skin and relieve symptoms associated with contact dermatitis, such as redness, swelling, and itching.<br> 
                    Changes in diet can support digestion and elimination of toxins (Ama). This detoxification process may contribute to improved skin health and reduce the severity of contact dermatitis symptoms.</p>

                <h3 class="newh3">Lifestyle modifications</h3>
                    <p>Lifestyle modifications can play a significant role in managing contact dermatitis. Maintaining good skin hygiene is essential for managing contact dermatitis.<br>
                    Adopting certain lifestyle changes, can potentially reduce triggers, promote skin health, and support overall well-being. A balanced routine (Dincharya) includes regular sleep patterns, waking up and going to bed at consistent times, taking fresh air in the morning, walking and jogging should be included and following a structured daily schedule. A balanced routine promotes stability and helps reduce stress, which can be beneficial for managing contact dermatitis.</p>

                <h3 class="newh3">Yoga asanas</h3>
                    <p>Yoga asanas have several potential benefits for individuals with contact dermatitis. Contact dermatitis can be aggravated by stress and emotional factors. Practicing Yoga asanas stimulates relaxation, reduces stress levels, and helps calm the mind.<br>
                    Yoga asanas often involve gentle stretching, twisting, and bending movements that stimulate blood circulation. Enhanced blood flow can nourish the skin cells, promote healing, and support overall skin health.<br>
                    Moreover, Regular practice of Yoga asanas can strengthen the immune system, which plays a crucial role in managing skin conditions like contact dermatitis.</p>

                <h3 class="newh3">Home Remedies</h3>
                    <p>Home remedies play a valuable role in the management and treatment of contact dermatitis. Home remedies often involve the use of natural ingredients that are readily available in most households. They can be convenient, cost-effective alternatives, and chemical free. <br>
                    Many home remedies focus on soothing and moisturizing the skin, which is beneficial for contact dermatitis. Some home remedies can provide a cooling effect on the skin, which can help ameliorate discomfort and reduce inflammation. </p>

                    <p>Hence, In this way, with the help of Ayurvedic treatment and home remedies, the symptoms of Contact dermatitis can be treated and managed in a natural manner. Ayurveda not only treats current symptoms of contact dermatitis but also helps to equip an individual with the knowledge and tools to prevent future flare-ups and maintain optimal skin well-being.</p>

                <h2 class="heading1">FAQ’s</h2>
                <ol>
                    <li><b>Is coconut oil good for Contact dermatitis?</b></li>
                    <p>Coconut oil is a natural moisturizer that nourishes skin in a natural way. It can improve symptoms such as itchiness, and dry skin. It can soothe skin and moisturize the skin. Additionally, it is free from chemicals and has no side effects.</p>

                    <li><b>Which are the best oils for Contact dermatitis?</b></li>
                    <p>There are various oils obtained naturally for the treatment of contact dermatitis such as Coconut oil, Neem Oil, and Brahmi Oil. These oils have warming, rejuvenating, and stimulating properties. They improve blood circulation and can be beneficial for certain types of dermatitis, especially when the skin is dry and itchy.</p>

                    <li><b>What to avoid if you are suffering from Contact dermatitis?</b></li>
                    <p>Avoid allergens, irritants like soaps, fragrances, and chemicals if you are suffering from contact dermatitis. Choose soft cotton clothes and avoid wearing synthetic clothes, manage stress, avoid excessive heat and sweating, refrain from scratching, and use lukewarm water.</p>

                    <li><b>How common is Contact dermatitis?</b></li>
                    <p>It affects about 15-20% of the general population at some point in their lives. It is more common in certain professions where individuals are exposed to irritants or allergens on a regular basis, such as healthcare workers, hairdressers, and construction workers. Additionally, certain populations, such as those with atopic dermatitis (eczema), can be more prone to developing contact dermatitis.</p>
                </ol>
            </div>
        </div>
    </div>
</section>

@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')

@stop