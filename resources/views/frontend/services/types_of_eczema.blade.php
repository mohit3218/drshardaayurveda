<?php
/**
 * Diabetes Page 
 * 
 * @created    10/06/2023
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2023
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')

<section class="types-of-eczema mt-5" id="typesOfEczema">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-3 col-sm-12 mb-2 eczemaCategories">
                <ul>
                    <li><a href="{{ 'types-of-eczema' }}">Types of Eczema</a></li>
                    <li><a href="{{ 'stasis-dermatitis' }}">Stasis Dermatitis</a></li>
                    <li><a href="{{ 'contact-dermatitis' }}">Contact Dermatitis</a></li>
                </ul>
            </div>
            <div class="col-xl-8">
                <h1 class="heading1">Eczema Uncovered: A Comprehensive Guide to Understanding the Types</h1>
                <p> 
                    Eczema is an inflammatory skin condition that affects millions of people around the globe. It is a pesky skin condition that can cause immense  discomfort and frustration. It's like having an uninvited guest that just won't leave! <br> 
                    With millions of people affected by eczema worldwide, There are various types of eczema, each with its unique symptoms and triggers.<br>
                    From the atopic dermatitis often seen in infants and children, to the irritating hand eczema that can make daily tasks a pain, this condition is frustrating and irritating to experience. And let's not forget about the sneaky contact dermatitis, which can be triggered by anything from skincare products to household cleaners.<br>
                    Living with eczema can be challenging, but there are ways to manage and control it. And what can be the best and most effective treatment path than Ayurveda which provides a natural, holistic, and effective treatment option for curing Eczema. 
                </p>
                <h2 class="heading1">OVERVIEW</h2>
                <p> 
                    It is a chronic inflammatory disease that causes skin rashes, itchiness, and cause inflammation followed by other discomforting and irritating symptoms such as:-
                </p>
                <ul class="li-format">
                    <li>Dry, itchy, and flaky skin</li>
                    <li>Thick, cracked, scaly, and inflamed skin</li>
                    <li>Red, brownish patches</li>
                    <li>Blisters, peel or weep on skin</li>
                    <li>Small bumps, sensitive and swollen skin</li>
                    <li>Swelling </li>
                    <li>Skin rash </li>
                    <li>Sweat</li>
                </ul>
                <p>
                    The most common places to experience these symptoms are the hands, neck, elbows, lips, feet, knees, and ankles. Also, These symptoms get flare up when countered by these triggers:- 
                </p>
                <ul class="li-format">
                    <li>Dry weather</li>
                    <li>Fabrics or a specific piece of clothing</li>
                    <li>Makeup and skincare products</li>
                    <li>Soaps and detergents </li>
                    <li>A particular touch that one is allergic to</li>
                    <li>Stress or anxiety </li>
                    <li>Foods such as peanuts, dairy products</li>
                    <li>Sweat</li>
                    <li>Smoking</li>
                    <li>Pollen</li>
                    <li>Pet dander</li>
                    <li>Cockroach Debris</li>
                </ul>
                <p>
                    The following triggers should be avoided to be in contact with so that they don’t get instigated and result in eczema. 
                </p>
                <h2 class="heading1">There are different types of eczema, each with unique symptoms and triggers.</h2>
                <ul class="li-format">
                    <li>Atopic Dermatitis</li>
                    <li>Contact Dermatitis</li>
                    <li>Dyshidrotic Eczema</li>
                    <li>Neurodermatitis </li>
                    <li>Nummular Eczema</li>
                    <li>Seborrheic Dermatitis</li>
                    <li>Stasis Dermatitis</li>
                </ul>
                <h3 class="newh3">Atopic Dermatitis</h3>
                <p>Atopic dermatitis, also known as childhood eczema, is a common form of skin inflammation that often begins in children's early years. Atopic dermatitis causes the skin to get affected with highly irritated and itchy skin and skin rashes. 
                The symptoms of atopic dermatitis can last longer, usually, this disease is a life-long condition that can lower when you grow up but its symptoms can flare up frequently. </p>

                <h3 class="newh3">Contact Dermatitis</h3>
                <p>This type of eczema is caused by exposure to irritants or allergens. This type of eczema can be affected by environmental triggers. Substances like rough fabric, rubber slippers, soap, paints, perfume, or other skin care products are responsible for contact dermatitis. 
                Further classifications include irritant contact dermatitis and allergic contact dermatitis. 
                Irritant contact dermatitis occurs when you get itchy rashes, burning skin, and blisters as a result of a sudden response to any irritating substance like soap, cleansers, or fragrances.  
                Allergic dermatitis occurs when you have an allergic reaction due to any past allergy and common allergens or causes include fragrances, preservatives, nail paints, and more.</p>

                <h3 class="newh3">Dyshidrotic Eczema</h3>
                <p>A painful and irritating condition, dyshidrotic dermatitis implies having extremely itchy blisters on the palms, hands, feet, fingers, and toes. 
                It is most common in young adults between 20 to 40 years of age. Around 50% of the total population, gets affected as a sudden reaction when they touch an allergen. Its common triggers include:- 
                Stress, sweat, rising temperature, allergies, and immune system.</p>

                <h3 class="newh3">Neurodermatitis</h3>
                <p>Neurodermatitis is a painful and irritating condition wherein a patient experiences itchiness, dryness, and other symptoms including discoloration, rough and inflamed skin, itchy and scaly skin, open pores, and thick patches.
                The factors or risk factors of neurodermatitis can be environmental factors such as tight clothing, bug bites, excessively dry skin, poor blood flow, injured nerves, and so forth.
                The symptoms can lead to severe scratching in the affected skin, which leads to bacterial infections, scars, and discoloration.</p>

                <h3 class="newh3">Seborrheic Dermatitis</h3>
                <p>Seborrheic Dermatitis most commonly known as scalp eczema or dandruff, affects the oil-producing areas such as the scalp region, and yellow patches can be seen on the skin with flaky, dry skin, red itchy patches and greasy skin in yellow patches. 
                In infants, this condition is also known as cradle scalp.</p>

                <h3 class="newh3">Stasis Dermatitis</h3>
                <p>Venous eczema, or stasis dermatitis, is a skin disorder that develops when there is inadequate blood flow in the lower legs, causing fluid buildup and skin damage.
                Red skin, itching, and swelling on the lower legs, ankles, and feet are symptoms of stasis dermatitis. The skin may thicken, discolor, and scald over time. 
                In severe situations, open sores or ulcers on the skin are possible.
                Poor circulation, which can be brought on by several conditions such as obesity, pregnancy, extended standing or sitting, and a history of blood clots, is the primary cause of stasis dermatitis.</p>
                <h3 class="newh3">Papular Eczema</h3>
                <p>Papular Eczema includes small, round papules or bumps that make the skin rash, irritated, and itchy. 
                The symptoms of papular eczema begin to show at a very young age. Its triggers include environmental factors such as clothing, and metals along with other triggers such as:-
                Pollen, smoke, dyes, scents, dry air, and detergents.       
                With the right set of medications and preventions, papular eczema can be cured effectively.</p>

                <h2 class="heading1">AYURVEDIC PERSPECTIVE</h2>
                <p>If you're also struggling with which treatment to ace the best treatment path for treating eczema, Ayurveda is the answer and solution to all the problems! With effective herbs, changes in diet and lifestyle alleviate the symptoms of Eczema.</p>
                <p>
                <b>Experts at Dr. Sharda Ayurveda suggest treatment options to heal your Eczema effectively.</b></p>

                <h3 class="newh3">Herbal Remedies For Eczema</h3>
                    <p>Some herbs are super-effective in treating your condition:- </p>
                    <h4 class="newh3">Aloe Vera</h4>
                    <p>The antibacterial and anti-inflammatory properties soothe inflammation and help in treating various skin diseases. Apply fresh aloe vera gel directly to the affected areas of the skin and leave it on for at least 20 minutes before rinsing it off with water.</p>
                    <h4 class="newh3">Guduchi</h4>
                    <p>A tri dosha balancing herb that possesses skin-renewing properties. Gudduchi helps in healing inflammation and eases the symptoms of Eczema.</p>
                    <h4 class="newh3">Ashwagandha</h4>
                    <p>Ashwagandha exhibits soothing properties and soothes dry, flaky, irritated skin. With its hydrating properties, it hydrates the skin well and prevents skin dryness.</p>
                    <h4 class="newh3">Neem</h4>
                    <p>Neem has anti-inflammatory and anti-microbial properties that can help reduce inflammation and prevent infection. Apply neem oil to the affected areas of the skin.</p>
                    <h4 class="newh3">Turmeric</h4>
                    <p>Turmeric has anti-inflammatory properties that can help reduce inflammation and soothe eczema symptoms. Mix turmeric powder with water or coconut oil to make a paste and apply it to the affected areas of the skin.</p>
                    <h4 class="newh3">Tea tree oil</h4>
                    <p>Tea tree oil hydrates the skin and with its antibacterial properties, it helps prevents infection.<br>
                    For application, simply apply it on your hands and fingers to treat eczema. 
                    Tea tree possesses these beneficial properties- antibacterial, antifungal, anti-inflammatory, antioxidant, and antiseptic properties that soothe and help in soothing inflamed and irritated skin.</p>

                <h3 class="newh3">Changes in Diet & Lifestyle</h3>
                    <p>There are some modifications in diet and lifestyle that can treat eczema effectively and naturally</p>
                 
                        <li>Identify your trigger foods, fabrics, or any irritant which can exacerbate eczema symptoms. Avoid these triggers to heal eczema.</li>
                        <li>Moisturize your skin using coconut oil as it will hydrate the skin well and prevent skin dryness.</li>
                        <li>Keep your body hydrated as well, and drink plenty of water to keep your body and skin hydrated.</li>
                        <li>A balanced diet is essential to prevent and treat eczema effectively. Your diet should include plenty of fresh fruits, vegetables, and whole grains. Avoid processed foods and sugary drinks, which can flare up inflammation.</li>
                        <li>Mild exercise will help you keep your body healthy and improve overall health. </li>
                        <li>Include spices as per the ayurvedic suggested diet as it will reduce symptoms of eczema such as inflammation, skin dryness, and itchiness. Add spices such as turmeric, ginger, cardamom, and cinnamon to your diet. </li>
                    <br>
                    <p>These are some of the diet and lifestyle modifications that will help in alleviating the symptoms of eczema and will heal this condition naturally. </p>
                

                <h2 class="heading1">FAQ’s</h2>
                <ol>
                <li><b>What is the most severe type of eczema?</b></li>
                <p>Although the symptoms of all types of eczema are painful and irritating, dyshidrotic and stasis eczema are even worse.
                In dyshidrotic, itchy blisters on the palms, hands, open sores, and ulcers on the skin affect the individual poorly. 
                Veins in the legs when weakened cause blood to leak and pool out in the lower legs leading to rashes and itchy skin in the case of stasis dermatitis making the condition extremely painful and miserable. 
                </p>
                <li><b>How many types of eczema are there?</b></li>
                <p>From atopic dermatitis to stasis dermatitis, there are 8 different types of eczema including Atopic Dermatitis, Contact Dermatitis, Dyshidrotic Eczema, Neurodermatitis, Nummular Eczema, Seborrheic Dermatitis, Stasis Dermatitis and Papular Eczema.  
                All types of eczema can be painful, irritating, and worse to experience. 
                </p>
                <li><b>Which eczema can cause bumps all over the body?</b></li>
                <p>Eczema is a skin condition that can cause extreme discomfort and frustration. One of the types of eczema, Papular eczema causes itchy bumps, rashes, scaly rash, and skin infections. </p>
                <li><b>What is the difference between eczema and dermatitis?</b></li>
                <p>Although the terms are overlapped in the case of their uses, causes, symptoms, and their treatments but there is a significant difference between these two respective terms.Eczema refers to a general term for a group of skin conditions or itchy, rashed, and dry skin whereas dermatitis is a more extensive term than eczema rashes alone. It can be caused by a wide range of factors, including irritants, allergens, infections, and autoimmune diseases.
                </p>
                <li><b>Which is the most popular (common) eczema type?</b></li>
                <p>The most common form of eczema is atopic dermatitis. 
                It is a skin condition that causes itchy, dry, and scaly patches of skin. It can occur in both children and adults depending on the skin type. 
                </p>
                </ol>
            </div>
        </div>
    </div>
</section>


@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')

@stop