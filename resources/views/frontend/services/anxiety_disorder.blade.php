<?php
/**
 * Anxiety Disorder Page 
 * 
 * @created    03/02/2022
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2022
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4">Anxiety Disorder</h3>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/insomnia">Anxiety Disorder</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a  href="#types">TYPES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatment">TREATMENT</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc">
                    <img src="{{ URL::asset('front/images/anxiety-disorder.webp') }}" class="why-choose-us img-fluid" alt="Anxiety Disorder">
                </div>
            </div>
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " >
                <h1 class="heading1"><b>Anxiety Disorder</b> (Chittodvege)</h1>
                <p>
                In Ayurveda anxiety is named <strong style="font-weight: bold;">“Chittodvege”and “Manoavsad"</strong>. Anxiety in general is termed the common emotion of a person when dealing with stress. But in no time this problem can become more severe if left untreated. The term describes the common emotion a person faces in his/her day-to-day life when dealing with stress. People often hesitate to share their emotions with anyone which can adversely affect their mind to a great extent. This leads to a situation where a person doesn’t hold on the emotions i.e., always irritated, panicked, and constant nervousness. It also refers to as a low mood that may last for a longer duration of time that can negatively impact a person’s mind. The main triggers are genetic, psychological, and environmental factors.
                <span id="dots">...</span>
                </p>
                <span id="more-content">
                <p>
                Ayurveda states, anxiety disorder occur as an outcome of imbalanced Vata Dosha. The Ayurvedic treatment for anxiety disorder from Dr. Sharda Ayurveda will surely provide positive and effective results by following a natural and holistic approach. The treatment helps stabilize mental health by prescribing natural medications and by adopting a healthy lifestyle.
                </p>
                <div class="btn-ayurvedic">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec" id="types"> 
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="heading1">Types of <b>Anxiety</b></h2>
                <p class="text-center">There are 3 major types of anxiety</p>
            </div>
        </div>
        <div class="row cust-iba">
            <div class="owl-carousel owl-theme" >
                <div class="item col-md-12 text-center aos-init aos-animate" >
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/train-anxiety.webp') }}" class="card-img-top lazyload" alt="Train Anxiety">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Train Anxiety</h4>
                            <p class="card-text">
                            This type is categorized as the lifelong pattern of anxiety which includes temperament, apart from an individual’s personality.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" >
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/state-anxiety.webp') }}" alt="State Anxiety">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">State Anxiety</h4>
                            <p class="card-text">
                            This is an acute stage of the anxiety which is described as the situation which does not persist beyond the provoking situation.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="item col-md-12 text-center aos-init aos-animate" >
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="{{ URL::asset('front/images/free-floating-anxiety.webp') }}" alt="Free floating Anxiety">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">Free floating Anxiety</h4>
                            <p class="card-text">
                            The type usually develops mainly due to constant mental disturbance.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate">
                    <h2 class="heading1">Causes of <b>Anxiety Disorder</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="causes">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Stress
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Stress can affect your mental health to a wider extent which is stated as the main reason for anxiety.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Medical Condition
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                The heart and lungs related disease has somewhat similar symptoms of anxiety disorder. So a full-body examination is a must to keep up the good health.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Hormone Level
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                Puberty accounts for constant changes in hormonal levels i.e., estrogen and progesterone which may increase the risk of developing anxiety.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Trauma
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">
                                Past traumatic incidence may become one of the major reasons that increase the chances or risk of developing anxiety symptoms in the future. In the most extreme cases the sufferer may experience panic attacks.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Pain
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                            <div class="card-body">
                                People who are emotional and overthink more regarding a situation more are more likely to undergo physical chronic pain compare to others. Therefore, it becomes the main reason for developing anxiety disorder.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>			


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/anxiety-disorder-symptoms.webp') }}" class="img-fluid" alt="Symptoms of Anxiety">
        </div>
        <div class="cvn sym" >
            <h3>Symptoms of <span>Anxiety Disorder</span></h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Headache</p>
            <p>Headache is the outcome of frequent panic attacks. Timely treatment may help reduce unbearable pain efficiently.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Increased Heart Rate</p>
            <p>A tense person suffering from mental disorders may experience fluctuated or increased heart rate.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Isolation</p>
            <p>Isolation is the main symptom of anxiety i.e., people tend to keep themselves away from social interactions and always prefer to remain separate or detached from others.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Chest Tightness</p>
            <p>Panic attacks can severely cause chest pain and tightening.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Over Thinking</p>
            <p>People who overthink more are most likely to be physically and mentally emotional.</p>
        </div>
    </div>
</div>
<div class="split"></div>
<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " >
                <h2 class="client-test-title"style="font-size: 24px !important;">Ayurvedic Treatment For <b>Anxiety Disorder</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Panchakarma therapy is an effective way to overcome mental diseases in the best way. The Abhyangam oil massaging with specific medicated herbal oils (chamomile, lavender, and sandalwood) will help reduce the levels of stress in mind to a considerable extent. Also, Shirodhara plays a pivotal role in treating patients suffering from an anxiety disorder.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurvedic treatment is a natural way to enhance the cognitive ability of an individual. It supports healthy memory, boosted concentration, and improved sleeping patterns.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Practicing yoga and meditation regularly will help release stress, maintain healthy hormone levels, and eases the mind.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Consuming herbal tea daily has shown to provide positive results in relieving worries from the mind. Some best herbal tea recommended by Ayurveda is chamomile and valerian.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Having a well-maintained balanced diet that must be excluded from high sugar, processed food, and preservatives.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="3SGdFQJiJaw" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/3SGdFQJiJaw.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Asthma Testimonial" >
                            <h3 class="usrname">Punkaj Kumar</h3>
                            <p class="desig">Anxiety Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                After observing symptoms including sleepless nights, and inability to concentrate I without wasting time consulted various doctors to resolve the issue but none of the treatments provided satisfactory results. Then, as per a suggestion from a friend decided to visit Dr. Sharda Ayurveda. There the expert guided me well about the whole treatment which includes intake of herbal medications along with strictly following a healthy diet and lifestyle. The treatment showed marvelous results and within just 4 months I am fully recovered from the problem. I am thankful to Dr. Sharda Ayurveda for their effective treatment and highly recommend everyone to visit their clinic if suffering from a similar problem.</a>. 
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               
                                <i class="fa" aria-hidden="true"></i>
                                What are the risk factors of anxiety disorder? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            The risk factors of anxiety are: 
                            <li>Genetics</li>
                            <li>Trauma</li>
                            <li>Constant stress</li>
                            <li>Physical illness</li>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                How one can overcome anxiety disorder?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            The tips that can help you overcome mental disorders naturally are:
                            <li>Practice regular meditation to relax your mind.</li>
                            <li>Check on your diet.</li>
                            <li>Maintain a healthy lifestyle</li>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                What is the permanent and best treatment to recover from anxiety? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            The Ayurvedic treatment is considered to be the most reliable and safe option to effectively overcome mental disorders. <a class="green-anchor"href="https://www.drshardaayurveda.com/anxiety-disorder">Anxiety treatment in Ayurveda</a> helps an individual to conquer anxiety effectively without any worry of getting side effects and complications.
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                How to know that you are suffering from anxiety? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                            The common notable signs include increased heart rate, sweating, headache, and difficulty concentrating. If these symptoms are persistent it is recommended to consult your experts immediately.
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingFive" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <i class="fa" aria-hidden="true"></i>
                                How Ayurvedic treatment helps to overcome anxiety? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="card-body">
                            Ayurveda is a traditional practice whereby incorporating healthy choices in life and by the intake of herbal medications one can get recovery early. Dr. Sharda's Ayurveda anxiety Ayurvedic treatment has so far proven to provide the best results. With proper guidance from experts, an individual can recover naturally.
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                What are the natural remedies one can adopt to overcome anxiety?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                        <div class="card-body">
                            The natural remedies which, if adopted can help in recovery are:
                            <li>Protein-rich breakfast</li>
                            <li>Regular exercise</li>
                            <li>Oil massage with medicated oils. Example- lavender.</li>
                            <li>Include a diet that should be rich in Vitamin B12 and Omega 3.</li>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                What are the natural Ayurvedic herbs that can help you to recover from anxiety?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                        <div class="card-body">
                           The list of Ayurvedic herbs is listed below.
                           <li>Ashwagandha</li>
                           <li>Brahmi</li>
                           <li>Bhringraj</li> 
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                What lifestyle modifications will help manage your anxiety?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                        <div class="card-body">
                            To reduce anxiety in Ayurveda it is suggested to incorporate certain lifestyle changes for long-term results. These lifestyle suggestions are:
                            <li>Prioritize sleep</li>
                            <li>Stay active</li>
                            <li>Keep yourself hydrated</li>
                            <li>Focus on a nutritious diet</li>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                What happens if an anxiety disorder is left untreated?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                        <div class="card-body">
                            If the condition remains untreated, it can be the major cause of other severe mental disorders one of which is depression. People with depression are at higher risk of developing suicidal thoughts or may have self-harm behaviors.
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Which vitamins are helpful to manage anxiety?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                        <div class="card-body">
                            The vitamins useful to manage anxiety are:
                            <li>Vitamin B5 supports the adrenal gland that helps in reducing stress and anxiety.</li>
                            <li>Vitamin B12 balances mood swings.</li>
                            <li>Vitamin B6 with magnesium balances anxiety that is more associated with premenstrual syndrome.</li>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<style>
    .carousel-inner {position: relative;width: 100%;overflow: hidden;}
    .carousel-item {position: relative;float: left;width: 100%;margin-right: -100%;-webkit-backface-visibility: hidden;backface-visibility: hidden;transition: -webkit-transform .6s ease-in-out;transition: transform .6s ease-in-out;transition: transform .6s ease-in-out,-webkit-transform .6s ease-in-out;}
    .card-body {padding-top: 1.5rem;padding-bottom: 1.5rem;-webkit-border-radius: 0!important;border-radius: 0!important;flex: 1 1 auto;-ms-flex: 1 1 auto;}
    .card .card-body a h3{font-size: 18px;color: #ff9000;line-height: 1.4;}
    .card .card-body h3{color: #373433;font-size: 18px;    line-height: 1.4;}
    .font-weight-bold {font-weight: 700!important;}
    .card-title {margin-bottom: .75rem;}
    .card-img-top {width: 100%;border-top-left-radius: calc(.25rem - 1px);border-top-right-radius: calc(.25rem - 1px);}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            responsiveClass: !0,
            navigation : true,
            autoplay: true,
            loop: true,
            smartSpeed: 2500,
            singleItem : true,
            navigationText: ["<i class='fa fa-angle-left fa-2'></i>", "<i class='fa fa-angle-right fa-2'></i>"],
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                480: {
                    items: 1,
                    nav: !1
                },
                768: {
                    items: 2,
                    nav: !1
                },
                1000: {
                    items: 2,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1024: {
                    items: 3,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1366: {
                    items: 3,
                    nav: !1
                }
            }
        })
    });

</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Anxiety Disorder",
    "item": "https://www.drshardaayurveda.com/anxiety-disorder"  
  }]
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What are the risk factors for anxiety disorder?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The risk factors of anxiety are: 
•   Genetics 
•   Trauma 
•   Stress 
•   Physical illness"
    }
  },{
    "@type": "Question",
    "name": "How one can overcome anxiety disorder?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The tips that can help you overcome mental disorders naturally are:
•   Practice regular meditation to relax your mind.
•   Check on your diet.
•   Maintain a healthy lifestyle"
    }
  },{
    "@type": "Question",
    "name": "What is the permanent and the best treatment to recover from anxiety?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The Ayurvedic treatment is considered to be the most reliable and safe option to effectively overcome mental disorders. Anxiety treatment in Ayurveda helps an individual to conquer anxiety effectively without any worry of getting side effects and complications."
    }
  },{
    "@type": "Question",
    "name": "How to know that you are suffering from anxiety?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The common notable signs include increased heart rate, sweating, headache, and difficulty concentrating. If these symptoms are persistent it is recommended to consult your experts immediately."
    }
  },{
    "@type": "Question",
    "name": "How Ayurvedic treatment helps to overcome anxiety?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurveda is a traditional practice whereby incorporating healthy choices in life and by the intake of herbal medications one can get recovery early. Dr. Sharda's Ayurveda anxiety Ayurvedic treatment has so far proven to provide the best results. With proper guidance from experts, an individual can recover naturally."
    }
  },{
    "@type": "Question",
    "name": "What are the natural remedies that one can adopt to overcome anxiety?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The natural remedies which, if adopted can help in recovery are:
•   Protein-rich breakfast 
•   Regular exercise 
•   Oil massage with medicated oils. Example- lavender.
•   Include a diet that should be rich in Vitamin B12 and Omega 3."
    }
  },{
    "@type": "Question",
    "name": "What all Ayurvedic herbs can provide effective results to conquer anxiety?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The list of Ayurvedic herbs is listed below. 
•   Ashwagandha
•   Brahmi 
•   Bhringraj"
    }
  },{
    "@type": "Question",
    "name": "What lifestyle modifications will help manage your anxiety?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "To reduce anxiety in Ayurveda it is suggested to incorporate certain lifestyle changes for long-term results. These lifestyle suggestions are:
•   Prioritize sleep
•   Stay active 
•   Keep yourself hydrated
•   Focus on a nutrition-rich diet"
    }
  },{
    "@type": "Question",
    "name": "What happens if the anxiety remains left untreated?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If the condition remains untreated, it can be the major cause of other severe mental disorders one of which is depression. People with depression are at higher risk of developing suicidal thoughts or may have self-harm behaviors."
    }
  },{
    "@type": "Question",
    "name": "Which vitamins are helpful to manage anxiety?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The vitamins useful to manage anxiety are:
•   Vitamin B5 supports the adrenal gland that helps in reducing stress and anxiety. 
•   Vitamin B12 balances mood swings. 
•   Vitamin B6 with magnesium balances anxiety that is more associated with premenstrual syndrome."
    }
  }]
}
</script>
@stop