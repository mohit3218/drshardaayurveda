<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4">Kidney Stones</h3>
                    <p style="font-size: 24px;">Get Healthy Kidneys with Ayurveda</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/kidney-stones">Kidney Stones</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/kidney-Stones.webp') }}" class="why-choose-us img-fluid" alt="Kidney Stones treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Kidney Stones</b> Ayurvedic Treatment</h1>
                <p>
                Urine is an excretory part excreted from the kidney which has many minerals and salts dissolved in it. When the composition of these solvents increases in urine they form calculi in the kidney. Hence kidney stones can be small and grows larger. These calculi formed in the urinary tract can get struck in its path hence blocking urine output and causes pain. The kidney, ureters, bladder are parts of the urinary tract. Here kidneys make urine from water and body waste. The urine formed move from kidney to bladder through the ureter. From the urinary bladder, it leaves our body through the urethra. Hence when atone is formed in the kidney it passes through the ureter then to the urinary bladder and urethra and finally outside the body.
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">...</span>
                </p>
                <span id="more-content">
                    <p>
                    The area like the ureter and urethra are narrow tubes where it might stick and causes pain. Kidney stone formed varies in color and types. Calcium stones are the most common type of stone and uric acid stones are the least seen. Other types are struvite stones i.e. stones caused due to <strong>chronic urinary tract infection</strong>. About 1 percent of stones are of cystine stone where it is an amino acid present in some food.
                </p>
                    <div class="btn-ayurvedic">
                        <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                    </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Kidney Stones</b></h2>
                    
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Dehydration
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                            Low urine volume and not enough intake of water is a major risk factor for kidney stone formation. Both these condition leads to concentrated urine means there is less fluid to keep salts dissolved.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Protein Rich Diet
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            Intake of excess calcium leads to an increase in calcium inside the kidney. Calcium is necessary for bone strength, so it is advised to lower salt intake instead of calcium because it helps in keeping calcium from being reabsorbed from urine.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Bowel Movement
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                            Indigestion or chronic diarrhea also raises the risk of forming kidney stones because it lowers the urine volume.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Obesity
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                            It increases acid levels in urine which leads to more stone formation.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                Modern Medicines
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                            Some calcium supplements, vitamin C, and antacids increase the risk of forming stones in kidneys. 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/kidney-Stones-symptoms.webp') }}" class="img-fluid"alt="Kidney Stones symptoms">
        </div>
        <div class="cvn sym" >
            <h3><span>Symptoms</span> of Kidney Stones </h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Pain While Urinating</p>
            <p>Sometimes a patient goes through severe pain while urinating.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Cramping Pain on Your Back</p>
            <p>A patient feels a sharp pain in the back and lower abdomen.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Nausea and Vomiting</p>
            <p>Sudden urge to vomit and nausea feeling.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Blood in Urine</p>
            <p>When the kidney stone is larger, a patient complains of blood in the urine.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Burning Sensation While Urinating</p>
            <p>A lot of people suffering from kidney stones have a burning sensation while urinating.</p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Benefits of <b>Ayurvedic Treatment for Kidney Stones</b></h2>
                <p class="im3"> 
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    	Panchkarma is quite effective as its cleansing process helps in the removal of toxins and also restores the healing capacity of the body.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurvedic medicines are indicated that helps to break stones into smaller pieces that can pass from urine itself. Diuretic and alkali therapy is mentioned in Ayurveda to get rid of kidney stones.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Nidan Parivarjan is advised as it removes the causative agent of kidney stones. 
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Some mandatory dietary changes i.e. to avoid eating Brinjal, Beans, ladyfinger, capsicum, tomato, cucumber, and spinach.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Avoid dairy products like ghee and also avoid eggs and, non-vegetarian foods.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <br>
                    <br>
                    <iframe src="https://www.youtube.com/embed/YF16t9Lg_CI" title="Reasons behind the recurrence of your Kidney Stones - Ayurvedic treatment" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Kidney Stone patient Testimonial" >
                            <h3 class="usrname">Harbhajan Kaur</h3>
                            <p class="desig">Kidney Stone patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Ayurvedic treatment for kidney stones from Dr. Sharda Ayurveda is quite effective. I am ongoing treatment as I was suffering from kidney stones for 6 months. Within few days of treatment followed with Ayurvedic diet for kidney stones prescribed by them is helping me to get rid of kidney stones. I would really appreciate Dr. Sharda Ayurveda as the best Kidney stone treatment hospital.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1"><b>FAQ'S</b></h2>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                            What happens if a kidney stone does not pass? 
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                        Kidney stone if does not pass causes pain in the abdominal. It can also lead to acute kidney disorder if kidney stones are ignored.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                What dissolves kidney stones fast?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                        A good intake of water, Lemon juice, Basil juice, Celery Juice, and Pomegranate juice.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Which food should be avoided for kidney stones?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                        Nuts, peanuts are high in oxalate so they should be avoided. Spinach, wheat bran should also be avoided in kidney stones. Chocolate, tea, should also be avoided.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Can kidney stones be cured without surgery in Ayurveda? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                        Yes, with Ayurvedic treatment, one can easily get rid of kidney stones without surgery. Ninety percent of stones pass by themselves within three to six weeks by kidney stone Ayurvedic treatment. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion2">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    How can a kidney stone pass by itself at home?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion2">
                            <div class="card-body">
                            The best remedy to encourage the kidney stone to pass by itself at home is to add on drinking a lot of fluids, especially plain water and citrus juices. The extra fluid will cause more urination which helps the stone move and keeps it from growing.
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                How long does it take to pass a kidney stone?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion2">
                        <div class="card-body">
                        The size of the kidney stone varies from individual to individual. So the period varies. A stone that is smaller than 4 mm may pass within a time gap of 10-14 days. A stone that is larger than 4 mm takes more than 15 days to pass. Ayurvedic treatment followed with some kidney stone home remedies prescribed by the experts helps passing stones easily.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Is lemon water good for kidney stones?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion2">
                        <div class="card-body">
                        Yes, it is good as it helps in preventing calcium from building up and forming stones in the kidney.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Can the Kidney repair itself?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion2">
                        <div class="card-body">
                        Yes, kidneys are regenerating in nature and can repair themselves by Ayurvedic treatment throughout life.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                Can kidney stone leads to renal failure?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion2">
                        <div class="card-body">
                        Kidney stones might lead to <a class="green-anchor" href="https://www.drshardaayurveda.com/kidney-failure">acute kidney failure</a> when calculi are blocked in the path and hence kidneys get damaged by puss cells and infections caused.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                How is kidney stone pain like?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion2">
                        <div class="card-body">
                        It is sharp, cramping pain in the back and side sometimes it moves to the lower abdomen or groins. It starts suddenly and comes in waves.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Kidney Stones",
    "item": "https://www.drshardaayurveda.com/kidney-stones"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What happens if a kidney stone does not pass?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Kidney stone if does not pass causes pain in the abdominal. It can also lead to acute kidney disorder if kidney stones are ignored."
    }
  },{
    "@type": "Question",
    "name": "What dissolves kidney stones fast?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "A good intake of water, Lemon juice, Basil juice, Celery Juice, and Pomegranate juice."
    }
  },{
    "@type": "Question",
    "name": "Which food should be avoided for kidney stones?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Nuts, peanuts are high in oxalate so they should be avoided. Spinach, Wheat bran should also be avoided in kidney stones. Chocolate, tea, should also be avoided."
    }
  },{
    "@type": "Question",
    "name": "Can kidney stones be cured without surgery in Ayurveda?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, with Ayurvedic treatment, one can easily get rid of kidney stones without surgery. Ninety percent of stones pass by themselves within three to six weeks by kidney stone Ayurvedic treatment."
    }
  },{
    "@type": "Question",
    "name": "How can a kidney stone pass by itself at home?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The best remedy to encourage the kidney stone to pass by itself at home is to add on drinking a lot of fluids, especially plain water and citrus juices. The extra fluid will cause more urination which helps the stone move and keeps it from growing."
    }
  },{
    "@type": "Question",
    "name": "How long does it take to pass a kidney stone?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The size of the kidney stone varies from individual to individual. So the period varies. A stone that is smaller than 4 mm may pass within a time gap of 10-14 days. A stone that is larger than 4 mm takes more than 15 days to pass. Ayurvedic treatment followed with some kidney stone home remedies prescribed by the experts helps passing stones easily."
    }
  },{
    "@type": "Question",
    "name": "Is lemon water good for kidney stones?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it is good as it helps in preventing calcium from building up and forming stones in the kidney."
    }
  },{
    "@type": "Question",
    "name": "Can the Kidney repair itself?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, kidneys are regenerating in nature and can repair themselves by Ayurvedic treatment throughout life."
    }
  },{
    "@type": "Question",
    "name": "Can kidney stone leads to renal failure?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Kidney stones might lead to acute kidney failure when calculi are blocked in the path and hence kidneys get damaged by puss cells and infections caused."
    }
  },{
    "@type": "Question",
    "name": "How is kidney stone pain like?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It is sharp, cramping pain in the back and side sometimes it moves to the lower abdomen or groins. It starts suddenly and comes in waves."
    }
  }]
}
</script>
@stop