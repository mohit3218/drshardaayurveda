<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4">Oligospermia</h3>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/oligospermia">Oligospermia</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/Oligospermia.webp') }}" class="why-choose-us img-fluid" alt="Oligospermia Treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1">Oligospermia Ayurvedic Treatment</h1>
                <p>
                A low sperm count is called Oligospermia and the absence of sperms i.e. no production of sperms is termed as Azoospermia. Whenever one thinks of expanding their family the number and quality of sperms matters the most to conceive a baby. Oligospermia is a medical condition found in men which are characterized by a low sperm count in their semen and the sperms count is considered lower than normal if a male has less than 15 million sperm per millimeter. This has become a very common problem in men and the main cause is their lifestyle. Any problem related to sperm i.e. whether it is low sperm count or the quality of sperm is found in most males and the couples struggle to get pregnant. It is a health issue in males that lowers the chance of his female getting pregnant. Male infertility treatment in Ayurveda has the most effective results and helps in the natural production of sperms. <br />
                Oligospermia is also classified into 4 categories i.e.-
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">...</span>
                </p>
                <span id="more-content">
                    <p>
                        <li><strong>Mild Oligospermia—</strong> Sperm count varies between 10-15 million sperm per milliliter.</li>
                        <li><strong>Moderate Oligospermia—</strong> Sperm count lies between 5-10 million sperm per milliliter. </li>
                        <li><strong>Severe Oligospermia—</strong> Sperm count is between 0 to 5 million sperm per milliliter. Low sperm count Ayurvedic treatment is safe and sure and helps the production of sperms naturally with Ayurvedic medicines and a healthy diet prescribed by the expert.</li>
                        <li><strong>Nil Sperm count-</strong> There is no production of sperms and is termed as Azoospermia.</li> 
                    </p>
                    <div class="btn-ayurvedic">
                        <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                    </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Oligospermia</b></h2>
                    <p>The causes of Oligospermia are still unknown but some causes are mostly seen</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Obesity
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Obesity, overweight directly impairs fertility in several ways.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Excessive Alcohol Consumption
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            Excessive intake of alcohol, smoking, drugs impacts the production of sperms. 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Trauma
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                            Surgeries like vasectomy, hernia repair surgery, prostate surgery can reduce sperm count. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Certain Medications
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                            Medicines like antifungal, antibiotics, ulcers medicines, chemotherapy drugs reduce the production of sperms. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Klinefelter Syndrome
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                            It is a genetic disorder that causes abnormal development in the male reproductive organs.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/oligospermia-symptoms.webp') }}" class="img-fluid"alt="Oligospermia symptoms">
        </div>
        <div class="cvn sym" >
            <h3><span>Symptoms</span> of Oligospermia </h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Infertility</p>
            <p>to conceive a baby due to low sperm count. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Erectile Dysfunction</p>
            <p>It gets hard for a man to maintain his erection during sexual intercourse. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Reduced Body Hair</p>
            <p>Less facial and body hair growth due to hormonal imbalance. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Swelling Around Testicles</p>
            <p>Swelling, pain, and a lump in the testis is caused due to some pathophysiology that develops hindrance in the passage of sperm.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Swollen Veins</p>
            <p>Enlarged and swollen veins in the scrotum.</p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Benefits of <b>Ayurvedic Treatment for Oligospermia</b></h2>
                <p class="im3"> 
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    As per Ayurveda Oligospermia can be correlated with Shukrakshaya. Ayurveda uses the principles of nature to maintain the health of a person by keeping the individual’s body, mind, and soul in perfect equilibrium with nature.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    <a class="green-anchor" href="https://www.drshardaayurveda.com/blogs/ayurvedic-treatment-for-oligospermia-or-low-sperm-count">Oligospermia Ayurvedic treatment</a> comprises therapies like rasayanas and vajikaradravyas and Ayurvedic medicines which are given internally followed with dietary changes. 
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Panchkarma and Vajikarana are beneficial for detoxification and rejuvenation along with lifestyle modification. 
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Get enough Vitamin D
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/sexual-disorder-common.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">
                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images') }}') no-repeat;">
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Oligospermia Testimonial" >
                            <h3 class="usrname">Harjeet Singh</h3>
                            <p class="desig">Oligospermia</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I took treatment for my sexual weakness. I had a low sperm count due to which we were unable to conceive a baby. Within few days of treatment, I could feel the change and after 4 months of treatment, it helped us conceive a baby. Dr. Sharda Ayurveda provides the best Ayurvedic treatment for male infertility. The diet prescribed by them helped me a lot and after dropping my medicines too I am healthy. 
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                            How can Oligospermia be treated naturally?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                        By getting enough sleep, exercise, avoiding smoking and alcohol, reducing stress adding more antioxidant-rich food. For early and safe recovery you can begin with Ayurvedic medicines. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Is watery sperm an indication of oligospermia? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                        Yes, it is a sign of low sperm count that may be a temporary condition but can be permanent too that will vary according to disease. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                What are the signs of unhealthy sperm?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                        The problem in sexual activity, low sexual desire, pain and swelling in the testicular area, facial and body hair loss due to chromosomal abnormality. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion2">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    At what age do men stop producing sperm?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion2">
                            <div class="card-body">
                            Men older than 40 years have fewer healthy sperm than young men. Even motility also starts decreasing as age advances.
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Which food items produce sperms fast and healthy?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion2">
                        <div class="card-body">
                        Food items like Walnuts, spinach, eggs, Asparagus boost the sperm count. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Can Azoospermia be treated in Ayurveda? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion2">
                        <div class="card-body">
                        Yes, it is treatable. With the most effective Ayurvedic herbs and healthy nutritious diet, some lifestyle changes, following a workout routine one can recover from azoospermia too. It may take a little extra time but will be recovered.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "oligospermia",
    "item": "https://www.drshardaayurveda.com/oligospermia"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "How can Oligospermia be treated naturally?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "By getting enough sleep, exercise, avoiding smoking and alcohol, reducing stress adding more antioxidant-rich food. For early and safe recovery you can begin with Ayurvedic medicines."
    }
  },{
    "@type": "Question",
    "name": "Is watery sperm an indication of oligospermia?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it is a sign of low sperm count that may be a temporary condition but can be permanent too that will vary according to disease."
    }
  },{
    "@type": "Question",
    "name": "What are the signs of unhealthy sperm?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The problem in sexual activity, low sexual desire, pain and swelling in the testicular area, facial and body hair loss due to chromosomal abnormality."
    }
  },{
    "@type": "Question",
    "name": "At what age do men stop producing sperm?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Men older than 40 years have fewer healthy sperm than young men. Even motility also starts decreasing as age advances."
    }
  },{
    "@type": "Question",
    "name": "Which food items produce sperms fast and healthy?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Food items like Walnuts, spinach, eggs, Asparagus boost the sperm count."
    }
  },{
    "@type": "Question",
    "name": "Can Azoospermia be treated in Ayurveda?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it is treatable. With the most effective Ayurvedic herbs and healthy nutritious diet, some lifestyle changes, following a workout routine one can recover from azoospermia too. It may take a little extra time but will be recovered."
    }
  }]
}
</script>
@stop