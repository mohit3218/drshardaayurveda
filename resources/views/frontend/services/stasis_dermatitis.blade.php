<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')

<section class="types-of-eczema mt-5" id="typesOfEczema">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-3 col-sm-12 mb-2 eczemaCategories">
                <ul>
                    <li><a href="{{ 'stasis-dermatitis' }}">Stasis Dermatitis</a></li>
                    <li><a href="{{ 'types-of-eczema' }}">Types of Eczema</a></li>
                    <li><a href="{{ 'contact-dermatitis' }}">Contact Dermatitis</a></li>
                </ul>
            </div>
            <div class="col-xl-8">
                <h1 class="heading1">What is Stasis dermatitis in Ayurveda?</h1>
                <p> 
                    In Ayurveda, Stasis dermatitis is known as "Vataja Twak Vikara" or "Vataja Charmaroga."
                    According to Ayurveda, Stasis dermatitis is primarily caused by an imbalance of  Vata Dosha, Kapha Dosha, and Pitta Dosha. These Doshas control the movement and circulation in the body. When these are imbalanced, it can lead to dryness, poor circulation, and accumulation of toxins (Ama).

                    It is characterized by dry, itchy, inflamed, and discolored skin. It is generally associated with poor blood circulation in the lower extremities, especially the legs. The stagnant blood and impaired circulation can result from factors such as a sedentary lifestyle, prolonged standing or sitting, obesity, and certain diseases like varicose veins.

                    Stasis Dermatitis Ayurvedic Treatment is effective and chemical-free. It includes lifestyle changes, dietary recommendations, and exercise routines (Yoga asanas, Pranayama, Walking), aimed at improving circulation and promoting long-term healing.
                </p>
                <h2 class="heading1">Symptoms of Stasis dermatitis</h2>
                        <li>The first sign is swelling around the ankles. It might get better during sleep and then come back during the day when an individual is active again.</li> <br>
                        <li>Heaviness in the legs can be noticed while standing or walking.</li><br>
                        <li>The color of the skin around or above the ankles appears reddish, yellowish, or brownish.</li><br>
                        <li>Varicose veins, which can be distinguished by a twisted, bulging appearance and a dark purple or blue coloration.</li><br>
                        <li>Itching.</li><br>
                        <li>Leg Pain.</li><br>
                        <li>Sores that ooze, crust, or look scaly.</li><br>
                        <li>Thickened skin around your ankles.</li><br>
                        <li>Tired legs.</li><br>
                        <li>Restless legs.</li><br>
                        <li>Wounds or ulcers on the skin.</li><br>
                <h2 class="heading1">Causes and Risk Factors of Stasis Dermatitis</h2>
                <p>Stasis dermatitis, also known as Venous Stasis dermatitis. It is primarily associated with impaired blood flow and circulation in the lower extremities. Several causes of stasis dermatitis and risk factors can contribute to its development, including:</p>
                    <li><b>Venous insufficiency:</b> It is characterized by a diminished ability of the veins to effectively transport blood back to the heart, resulting in the accumulation of blood in the legs and the potential development of stasis dermatitis.</li><br>
                    <li><b>Varicose veins:</b> These are large, twisted veins that can emerge when the valves in the veins that help blood flow back to the heart are impaired. This can cause blood to collect in the legs, leading to stasis dermatitis.</li><br>
                    <li><b>Age:</b> It is most typical in people above the age of 50 years or older individuals, as the veins and blood vessels naturally lose elasticity and become less efficient at circulating blood.</li><br>
                    <li><b>Blood clots:</b> If there is a Blood clot in the leg of an individual. It can block blood flow and cause Stasis dermatitis.</li><br>
                    <li><b>Obesity:</b> It is a major cause and raises the tension on the veins and leads to stasis dermatitis.</li><br>
                    <li><b>Sitting or standing for long hours:</b> You also may be more prone to have it, if you usually stand or sit for long periods of time or you don't get much exercise.</li><br>
                    <li><b>Pregnancy:</b> Stasis dermatitis can occur as a result of the enlargement of the uterus during multiple pregnancies, which exerts pressure on the veins.</li><br>
                    <li><b>Deep Vein Thrombosis (DVT):</b> It is a blood clot that forms in the deep veins of the legs. Individuals with a history of DVT have an increased risk of developing venous insufficiency and subsequent stasis dermatitis.</li><br>
                    <li><b>Prior Leg Trauma or Surgery:</b> Injury or trauma to the legs or previous leg surgery can damage veins and disrupt the normal flow of blood. This can lead to venous insufficiency and increase the risk of stasis dermatitis.</li><br>
                   
                    <h2 class="heading1">Can Ayurveda cure dermatitis?</h2>
                    <p>Yes, Ayurveda manages stasis dermatitis very well with the help of the following Ayurvedic practices:</p>
                    <h3 class="heading1">Diet and lifestyle modifications</h3>
                    <p>Ayurveda gives importance to a balanced and nutritious diet. Specific dietary changes are made to reduce inflammation and support healthy skin according to the “Prakriti” of the patient. Lifestyle transformations such as stress reduction techniques, regular exercise, and maintaining proper hygiene can also be beneficial.</p>

                    <h3 class="heading1">Ayurvedic Herbs</h3>
                    <p>Ayurveda utilizes a fabulous treasure of herbs for the cure and management of stasis dermatitis. Herbs reduce inflammation and soothe the skin. Some commonly used herbs for dermatitis include Neem, Turmeric, Aloe vera, and licorice.</p>

                    <h3 class="heading1">Yoga and Meditation</h3>
                    <p>Ayurveda recognizes the mind-body connection and practices like yoga and meditation to promote the overall health of an individual. These practices can help reduce stress, improve circulation, and support healthy skin.</p>
                    <p>In conclusion, with the holistic and individualized approach of Ayurveda, a skin condition like Stasis dermatitis can be managed effectively. Ayurvedic treatment methodology focuses on detoxification, encouraging healthy circulation, and balancing the Doshas. Moreover, with the glory of herbs, lifestyle changes, and yoga asana, the symptoms of stasis dermatitis can be relieved. Thus, healing can take place in the affected area of the skin. Adopt Ayurveda as a means to stay healthy, fit, and vibrant.</p>


                    <h2 class="heading1">FAQ’s</h2>
                <ol>

                    <li><b>What is the best treatment for Stasis dermatitis?</b></li>
                    <p>Ayurveda treats the individual as a whole and not only the disease. The root cause of the disease is treated without any side effects in the most natural Ayurvedic way. Hence, it is the most promising treatment for managing various skin conditions, including Stasis dermatitis. 
                    Ayurvedic treatments for stasis dermatitis aim to address the imbalance of the dosha in the body and focuses on personalized treatment plans according to the individual's unique Prakriti and Vikriti.</p>

                    <li><b>What is the root cause of Stasis dermatitis?</b></li>
                    <p>According to Ayurveda, the root cause of Stasis dermatitis lies in the imbalance of the dosha, tridosha Vata Dosha, Kapha Dosha, and Pitta Dosha. When there is an excess or aggravation of these doshas, it disrupts the normal functioning of the circulatory system, leading to poor blood circulation and lymphatic flow in the legs. This stagnant blood and lymph then cause swelling, inflammation, and tissue damage, resulting in stasis dermatitis.
                    Ayurveda also considers impaired digestion and metabolism (Agni) as a contributing factor to the development of stasis dermatitis. When Agni is weakened, it leads to the accumulation of toxins (Ama) in the body, which can be seen in Stasis dermatitis.</p>

                    <li><b>Is turmeric effective in alleviating dermatitis symptoms?</b></li>
                    <p>While turmeric (Curcuma longa) has so many beneficial properties for the skin, including its anti-inflammatory, antioxidant, and anti-microbial effects, it is important to consult with an Ayurvedic physician for the proper line of treatment according to the Prakriti (constitution) of the patient.</p>

                    <li><b>Which herbs are beneficial for treating Stasis dermatitis?</b></li>
                    <p>Ayurveda has several excellent herbs that are known for their beneficial properties in managing stasis dermatitis such as Neem (Azadirachta indica), Turmeric (Curcuma longa), Manjistha (Rubia cordifolia), Aloe Vera (Aloe barbadensis), Guduchi (Tinospora cordifolia) and Licorice (Glycyrrhiza glabra).</p>

                    <li><b>Is stasis dermatitis permanent?</b></li>
                    <p>With proper Ayurvedic treatment and management through lifestyle changes, it is possible to significantly improve the symptoms and reduce the frequency and intensity of stasis dermatitis episodes.</p>

                    <li><b>How serious is Stasis dermatitis?</b></li>
                    <p>Stasis dermatitis is a chronic and progressive condition that can be life-threatening. If left untreated or poorly managed, the condition can progress, leading to more severe symptoms and complications.
                    Complications can include skin ulcers, open sores, skin infections, and cellulitis. These complications can cause pain, discomfort, and difficulty in healing, and can potentially lead to further severity of symptoms if not properly managed.</p>

                    <li><b>Is Stasis dermatitis contagious or can spread from one person to another?</b></li>
                    <p>Stasis dermatitis itself is not contagious and cannot spread from person to person. It is a localized skin condition where venous insufficiency in the lower legs or ankles takes place. If it worsens or if it is left untreated, it can lead to the development of venous ulcers.</p>
                </ol>
            </div>
        </div>
    </div>
</section>

@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')

@stop