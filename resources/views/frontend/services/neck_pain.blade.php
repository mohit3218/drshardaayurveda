<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Neck Pain</h3>
                    <p style="font-size: 24px;">Protect Your Neck Joints With Timely Treatment</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/neck-pain">Neck Pain</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/Neck-Pain.webp') }}" class="why-choose-us img-fluid" alt="Neck Pain">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Neck Pain</b> Ayurvedic Treatment</h1>
                <p>
                    Pain in neck area at back due to poor posture, medical diseases like rheumatoid arthritis, aging or degenerative disease, and injury. The neck covers the cervical spine area which is a network of nerves, bones, joints, and muscles. It is the neck that provides support to the head. Slight disturbance to this area causes pain in the shoulder, head, arm, and hands. Neck pain usually goes away within a few days or weeks or can persist for a month. It can be classified as acute, when pain lasts less than 4weeks, acute pain when pain lasts 4-12 weeks, and chronic when pain lasts 3 or more months. Neck pain may be sharp and located in one spot or might spread across the area i.e. sometimes it accompanies headaches, shoulder pain, and numbness in the forearm. Sometimes the pain is like a tingling sensation radiating down into the shoulder, arm, or hand. Neck pain is common among adults. It can develop suddenly in form of injury due to poor posture.
                    <!-- <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span> -->
                </p>
                <!-- <span id="more-content"> -->
                    <p>
                     
                    </p>
                    <div class="btn-ayurvedic">
                        <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                    </div>
                </span>
                <!-- <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button> -->
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Neck Pain</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Muscle Strains
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                wrong posture and continuous sitting at the computer and reading in bed or gritting teeth triggers muscle strains.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Worn Joint
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                with age, due to degenerative changes in bones and due to deterioration of cushion between the bone in osteoarthritis and bone spurs formation causes pain in the neck.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Nerve Compression 
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                                bone spurs and herniated disk sometimes cause nerve compression of the spinal cord, which leads to numbness in the hands.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Injuries
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                                whiplash injury caused due to auto collision where the head is pushed backward and forward forcefully creating strain to soft tissues and neck. 
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Disease
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                                In <a class="green-anchor" href="https://www.drshardaayurveda.com/rheumatoid-arthritis">Rheumatoid Arthritis</a>, meningitis, etc presence of neck pain is seen.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz px-2" >
            <img src="{{ URL::asset('front/images/Neck-pain-Symstoms.webp') }}" class="img-fluid"alt="Neck Pain Symstoms">
        </div>
        <div class="cvn sym px-4" >
            <h3><span>Symptoms of</span> Neck pain </h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Neck Pain</p>
            <p>Severe neck pain is the main symptom of it.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Fatigue</p>
            <p>Due to pain, one feels tired and restless.</p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Advantage of <b>Ayurvedic treatment for neck pain </b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    In Ayurveda, neck pain is described as Manyastambha or Vatavyadi which causes muscle rigidity, immobility, and neck pain.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    In treatment by Ayurvedic methods, the first cause of neck pain is detected and then Ayurvedic therapies are decided.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    It provides a permanent cure to neck pain. External treatments like GrivaBasti, Abhyangam, etc are also seen as beneficial.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Internal medicines and guidance of neck posture and yogic exercise completely heal the problem.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="h6nMxB8wO7U" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/h6nMxB8wO7U.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>    
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Priya Sharma</h3>
                            <p class="desig">Neck Pain Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was suffering from neck and back pain for a long. I consulted Dr. Sharda Ayurveda for my treatment and was relieved from pain within 4 months of treatment.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                What is Ayurvedic treatment for neck pain? 
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                            Ayurvedic treatment includes external treatments like Abhyanga, Basti, and Nasya. Internal treatment with Ayurvedic medicines- Correction of posture adopting Asana and Yogic activities.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Which dosha causes neck pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                            Vatadosha aggravation causes neck pain. This Dosha causes muscle rigidity, immobility, and pain in the neck.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Which Vitamin is good for neck pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                            Vitamin D is essential for bones, hence its deficiency should always be ruled out.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Is ginger good for neck pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                            Yes, it is good because of its anti-inflammatory properties it can be used in treating neck pain.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                What is a quick remedy to neck pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                            <ul>
                                <li>Press the sore spot with your fingers.</li>
                                <li>Turn head slightly in the direction opposite the cramp and bend it diagonally.</li>
                                <li>Repeat steps 2-3 times.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    How should I sleep with a stiff neck?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                                <ul>
                                    <li>Sleep on the back with a slight reclined position.</li>
                                    <li>Sleep on the back with a pillow under your knees.</li>
                                    <li>Sleep with a pillow between your knees.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                How many pillows do you need for neck pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                            Back with 2 pillows, the pillow for back sleepers should have a medium loaf or should not take any pillow during pain. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Is it safe to massage a stiff neck?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                            Yes, it is safe but it gives temporary relief to pain and stiffness.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                Why can't I move my neck without it hurting?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                            When neck pain occurs neck muscles get contracted and stiffness occurs leads to extreme pain.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Can neck problems affect your brain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                            It depends upon the severity of the cause, if the brain stem is affected people develop a sense of vibration, pain temperatures and muscle weakness, dizziness, and impaired vision.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Neck Pain",
    "item": "https://www.drshardaayurveda.com/neck-pain"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What is Ayurvedic treatment for neck pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurvedic treatment includes external treatments like Abhyanga, Basti, and Nasya. 
Internal treatment with Ayurvedic medicines- Correction of posture adopting Asana and Yogic activities."
    }
  },{
    "@type": "Question",
    "name": "Which dosha causes neck pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Vata dosha aggravation causes neck pain. This Dosha causes muscle rigidity, immobility, and pain in the neck."
    }
  },{
    "@type": "Question",
    "name": "Which Vitamin is good for neck pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Vitamin D is essential for bones, hence its deficiency should always be ruled out."
    }
  },{
    "@type": "Question",
    "name": "Is ginger good for neck pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it is good because of its anti-inflammatory properties it can be used in treating neck pain."
    }
  },{
    "@type": "Question",
    "name": "What is a quick remedy to neck pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "•    Press the sore spot with your fingers. 
•   Turn head slightly in the direction opposite the cramp and bend it diagonally. 
•   Repeat steps 2-3 times."
    }
  },{
    "@type": "Question",
    "name": "How should I sleep with a stiff neck?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "•    Sleep on the back with a slightly reclined position
•   Sleep on the back with a pillow under your knees. 
•   Sleep with a pillow between your knees."
    }
  },{
    "@type": "Question",
    "name": "How many pillows do you need for neck pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Back with 2 pillows, the pillow for back sleepers should have a medium loaf or should not take any pillow during pain."
    }
  },{
    "@type": "Question",
    "name": "Is it safe to massage a stiff neck?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it is safe but it gives temporary relief to pain and stiffness."
    }
  },{
    "@type": "Question",
    "name": "Why can't I move my neck without it hurting?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "When neck pain occurs neck muscles get contracted and stiffness occurs leads to extreme pain."
    }
  },{
    "@type": "Question",
    "name": "Can neck problems affect your brain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It depends upon the severity of the cause, if the brain stem is affected people develop a sense of vibration, pain temperatures and muscle weakness, dizziness, and impaired vision."
    }
  }]
}
</script>
@stop