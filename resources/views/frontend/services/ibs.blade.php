<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h1 class="ser-heading mt-4" >Ayurvedic Treatment for Irritable Bowel Syndrome (IBS)</h1>
                    <p style="font-size: 18px;">Not just symptomatic, Get Root cause treatment for IBS</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/ibs">IBS</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatment">TREATMENT</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="definition">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/IBS.webp') }}" class="why-choose-us img-fluid" alt="man holding his stomach in pain experience irritable bowel syndrome" title="man holding his stomach in pain experience irritable bowel syndrome">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h2 class="heading1">What is <b>Irritable Bowel Syndrome(IBS)?</b></h2>
                <p>
                    Irritable Bowel Syndrome (IBS) is a common disorder caused by a collection of symptoms like cramping, abdominal pain, bloating, gas, and constipation. It is also known as gastrointestinal tract, and it is a very dangerous chronic condition. Irritable Colon, Spastic Colon, Spastic Colon, and Mucous Collitis are further terms for IBS. It is unrelated to other bowel diseases and is a distinct disorder from inflammatory bowel disease. Ayurvedic treatment for irritable bowel syndrome, which involves the use of herbal remedies, dietary changes, and lifestyle modifications, has shown promise in alleviating the symptoms of this common gastrointestinal disorder.<br>
                    According to an overview from the <a class="green-anchor"href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5453305/" rel=nofollow> 2018 Study Published</a>, medical experts search for symptoms that have shown at least three days each month for the previous three months in order to identify it.<br>
                    In certain circumstances, IBS can harm the digestive system. This however, is uncommon. IBS doesn't enhance your risk of gastrointestinal malignancies, according to a <a class="green-anchor"href="https://pubmed.ncbi.nlm.nih.gov/35130187/" rel="nofollow">2022 research by Trusted Source</a> Nonetheless, it may still have a big impact on your life.
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                    <p>
                        <strong style="font-weight: bold;">What does Ayurveda say about IBS ?</strong><br>
                    According to the Ayurveda Irritable Bowel Syndrome (IBS) is referred to as Grahani, which is described as an Agni Adhiṣṭhāna (Digestive Fire), which aids in charge of converting food into energy and nutrients. Aahaar’s ingestion, digestion, absorption, and assimilation are all controlled by Grahani. If a person has difficulty in digesting food, this suggests that their Agni (Digestive System), or fire element—is weak.</p>
                        <p>Based on Bowel Patterns, it  divided into four Primary types:
                        <li>Vataja (Major Constipation Grahani (IBS))</li>
                        <li>Pittaja (Predominant Diarrhoea Grahani (IBS)) </li>
                        <li>Kaphaja (Predominance of Dysentery Grahani(IBS)) </li>
                        <li>Tridoṣaja(When all of the mentioned symptoms are present together)</li>
                    </p>
                    <div class="btn-ayurvedic">
                        <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                    </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec our-expert" style="background-color:#f3f3f3" id="causes">
    <div class="container-fluid" >
        <div class="row cut-row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">What Causes <b>Irritable Bowel Syndrome (IBS)?</b></h2>
                    <p font align="center" style="line-height:2">Grahani (IBS) is mostly caused by insufficient mandagni (digestive fire)  and ama (toxin generation). As per Ayurveda, IBS is linked to:</p>
                    <li>Bad diet and nutrition, such as eating a lot of fried food, eating too much, eating foods that are unhealthy for your gut, drinking too much, and eating cold meals.</li>
                    <li>Wrong timing of eating or skipping meals while you're hungry.</li>
                    <li>Many underlying disorders include a neural system imbalance, aberrant gastrointestinal motor and sensory activity, etc.</li>
                    <li>Accumulation of ama (toxins) in the tissues used for circulation.</li>
                    <li>Stress of any kind, both physical and emotional, weakens the body's defences and internal fortitude.</li>
                    <li>A disturbed and irregular natural biological rhythm.</li>
                </div>
            </div>
        </div>
    </div>
</section>	

<div class="container-strip" style="background: #eeb03f";>
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/IBS-Symptoms.webp') }}" class="img-fluid"alt="infographic showing symptoms of irritable bowel syndrome" title="infographic showing symptoms of irritable bowel syndrome">
        </div>
        <div class="cvn sym" >
            <h3><b>IBS Symptoms:</b> Understanding Grahani</h3>
                <p>Individuals suffering from Irritable Bowel Syndrome (IBS) frequently experience a variety of symptoms, which may impact some people more severely than others. These symptoms of IBS might develop during stressful times or after eating specific foods. After bowel motions, you may feel better.</p>
            <li>Undergoing irregular bowel movements, including alternate bouts of diarrhoea and constipation, poor Agni (Digestion), and the movement of partially digested meals.</li>
            <li>Urge to move constantly just after eating.</li>
            <li>Discomfort in the lower abdomen</li>
            <li>Expansion or dilation</li>
            <li>Belly Bloating and Nausea</li>
            <li>A feeling of incomplete egress coupled with a need to move</li>
            <li>Dysphagia, chest discomfort, and heartburn.</li>
            <li>The Physical and Mental Symptoms of Grahani (IBS), such as fatigue, irritability, headache, anxiety, sadness, and even urologic dysfunction and gynecologic problems, are just a few more symptoms that can occur.</li>
        </div>
    </div>
</div>

<div class="split"></div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <h2 class="client-test-title"style="font-size: 24px!important;" text-al>Effective Ayurvedic Treatment for <b>Irritable Bowel Syndrome (IBS)</b></h2>
            <p style="text-align:center;">Ayurvedic treatment for IBS can help manage IBS symptoms such as bloating, constipation, diarrhoea, and abdominal pain, regardless of subtype (IBS-M, IBS-D, IBS-U, or IBS-C). Consult with an Ayurvedic practitioner for personalised treatment for IBS. Irritable bowel syndrome treatment with Ayurveda by calming disturbed internal energies, recovering the functioning of the Agni (digestive system), and removing built-up ama (toxins). Considering that stress is usually a major contributing factor to the disease, lifestyle modification and herbs that enhance the neurological and mental systems are effective therapeutic modalities. Here are several treatments that may be useful.</p>
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <p class="im3">
                    <li><b style="font-weight: bold;">Agnidīpana:</b> The management of the Agni (Digestive Fire) is a part of this process. Ama (undigested toxins) are removed from the digestive tract to cure this. Digestive herbs and other strong ayurvedic substances can help decrease ama.</li>
                </p>
                <p class="im3">
                    <li><b style="font-weight: bold;">Vātānulomana:</b> Based on how much Vata is unbalanced, numerous health issues arise, such as chapped lips, dry skin, weakness, constipation, etc. Vātānulomana is a procedure that is used to balance and correct the Vata Dosha.</li>
                </p>
                <p class="im3">
                    <li><b style="font-weight: bold;">Manonukulata:</b> Grahani or Irritable Bowel Syndrome (IBS) is frequently reported to worsen when a person goes through a stressful environment or is dealing with mental problems. Both relaxation and treatment of these underlying problems are part of this approach.</li>
                </p>
                <p class="im3">
                    <li><b style="font-weight: bold;">Shodhan:</b> A purification or cleansing procedure is called Shodhan. It includes transforming any dangerous substance into beneficial, neutral, nonpoisonous constituents.</li>
                </p>
                <p class="im3">
                    <li><b style="font-weight: bold;">Shamana:</b> Shamana revives the body by addressing imbalances and removing any leftover pollutants during cleansing. In Ayurveda, it is a natural procedure. Following the completion of the detoxification procedure, this treatment tries to revive and restore the body's balance.</li>
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="G2SXEXmtMqY" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/G2SXEXmtMqY.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
    </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Patient's <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="harbhajan kaur IBS patient's testimonial" title="harbhajan kaur IBS patient's testimonial">
                            <h3 class="usrname">Harbhajan Kaur</h3>
                            <p class="desig">IBS Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Irritable bowel syndrome has always been a problem for my younger sister. Her IBS symptoms would frequently fluctuate between constipation and diarrhoea. All of the medications, doctors, and natural cures she tried were failed. She has experienced significant improvements in her health once we started our therapy at Dr. Sharda Ayurveda clinic. She has stopped experiencing any IBS symptoms by taking her normal medicines and eating the right foods.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="ram kumar IBS patient's testimonial" title="ram kumar IBS patient's testimonial" >
                            <h3 class="usrname">Ram Kumar</h3>
                            <p class="desig">IBS Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I've had irritable bowel syndrome for the past two years. I've encountered lot of doctors and through a lot of therapies, but nothing improved. I moved from allopathic to ayurvedic treatment because I believed it would cure my disease. After getting ayurvedic therapy from Dr. Sharda Ayurveda for four months, I am now able to start my normal life. I would recommend everyone dealing with IBS to consider getting treatment with Ayurveda. 
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="manjeet kaur IBS patient's testimonial" title="manjeet kaur IBS patient's testimonial">
                            <h3 class="usrname">Manjinder Kaur</h3>
                            <p class="desig">IBS Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Luckily, after taking Ayurvedic treatment from Dr. Sharda Ayurveda, I no longer experience any Irritable Bowel Syndrome and my health is steady. I took therapies for almost three months, and while I no longer do so, I continue to follow the doctors' recommended diet. We are thankful to Dr. Sharda Ayurveda for the excellent treatments and recommendations.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1"><b>FAQ'S</b></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                What type of treatment is available in IBS?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            Ayurvedic Medicines with dietary changes followed by Panchakarma therapy. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                How to treat abdominal pain in IBS with Home Remedy?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            Take a teaspoon of Ajwain along with warm water twice a day to get relief from abdominal pain. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Can IBS be genetic?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            Relatives of an individual with IBS are two to three times as likely to have IBS, with both genders being affected. The estimated genetic liability ranges between 1–20%, with heritability estimates ranging between 0–57%.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                What is the main trigger of IBS?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                            Some common IBS triggers include diet, stress, infection and medications. Many people with IBS notice that some foods make their symptoms worse, but these 'trigger foods' differ from one person to the next.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion2">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    How can I help myself with IBS?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion2">
                            <div class="card-body">
                                Eating small meals several times per day to reduce bloating. Trying probiotics, kefir, or aloe vera juice to promote healthy digestion. Drinking plenty of water to counteract IBS constipation or diarrhea
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Do Spicy foods affect IBS? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion2">
                        <div class="card-body">
                            There is no such specification that Spicy food affects the symptoms of IBS. But if they create any problem or provoke the symptoms then one should avoid taking them.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                What can untreated IBS lead to?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion2">
                        <div class="card-body">
                            Untreated IBS leads to long-term abdominal pain and can also lead to anxiety or depression. It may lead to pain and discomfort in the abdominal region.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Is IBS permanent?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion2">
                        <div class="card-body">
                            IBS does not cause damage to the digestive system. There is a sure cure for IBS in Ayurveda. Timely treatment of symptoms of IBS can stop the disease from getting chronic.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                How to distinguish between IBS and IBD?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion2">
                        <div class="card-body">
                            IBS and IBD are both chronic conditions that cause abdominal pains, sometimes cramping, and urgent bowel movements. IBS affects the small and large intestines with the colon whereas IBD causes inflammation in the intestines. IBS can be genetic too.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                               Is Ayurveda able to treat IBS?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion2">
                        <div class="card-body">
                            Yes, IBS is completely curable through Ayurveda with the right medication and diet.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>

@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "IBS",
    "item": "https://www.drshardaayurveda.com/ibs"  
  }]
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What type of treatment is available in IBS?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurvedic Medicines with dietary changes followed by Panchakarma therapy."
    }
  },{
    "@type": "Question",
    "name": "How to treat abdominal pain in IBS with Home Remedy?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Take a teaspoon of Ajwain along with warm water twice a day to get relief from abdominal pain."
    }
  },{
    "@type": "Question",
    "name": "Can IBS be genetic?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Relatives of an individual with IBS are two to three times as likely to have IBS, with both genders being affected. The estimated genetic liability ranges between 1–20%, with heritability estimates ranging between 0–57%."
    }
  },{
    "@type": "Question",
    "name": "What is the main trigger of IBS?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Some common IBS triggers include diet, stress, infection and medications. Many people with IBS notice that some foods make their symptoms worse, but these 'trigger foods' differ from one person to the next."
    }
  },{
    "@type": "Question",
    "name": "How can I help myself with IBS?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Eating small meals several times per day to reduce bloating. Trying probiotics, kefir, or aloe vera juice to promote healthy digestion. Drinking plenty of water to counteract IBS constipation or diarrhea."
    }
  },{
    "@type": "Question",
    "name": "Do Spicy foods affect IBS?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "There is no such specification that Spicy food affects the symptoms of IBS. But if they create any problem or provoke the symptoms then one should avoid taking them."
    }
  },{
    "@type": "Question",
    "name": "What can untreated IBS lead to?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Untreated IBS leads to long-term abdominal pain and can also lead to anxiety or depression. It may lead to pain and discomfort in the abdominal region."
    }
  },{
    "@type": "Question",
    "name": "Is IBS permanent?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "IBS does not cause damage to the digestive system. There is a sure cure for IBS in Ayurveda. Timely treatment of symptoms of IBS can stop the disease from getting chronic."
    }
  },{
    "@type": "Question",
    "name": "How to distinguish between IBS and IBD?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "IBS and IBD are both chronic conditions that cause abdominal pains, sometimes cramping, and urgent bowel movements. IBS affects the small and large intestines with the colon whereas IBD causes inflammation in the intestines. IBD can be genetic too."
    }
  },{
    "@type": "Question",
    "name": "Is Ayurveda able to treat IBS?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, IBS is completely curable through Ayurveda with the right medication and diet."
    }
  }]
}
</script>

@stop