<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Sciatica Pain</h3>
                    <p style="font-size: 18px;">Protect Your Joints with Timely Treatment</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/sciatica-pain">Sciatica Pain</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/sciatica-pain.webp') }}" class="why-choose-us img-fluid" alt="Sciatica pain treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Sciatica Pain</b> Ayurvedic Treatment</h1>
                <p>
                Sciatica pain is characterized by low back pain that radiate along with the sciatica nerve with inflammation of sciatica nerve that occur due to irritation of root or nerve anywhere in the spinal canal in the pelvic or buttocks. It is the largest nerve in the body that starts from the back of the hip and goes to the big toe of the foot. This nerve is thick in nature and any injury or infection causes irritation and pain in whole leg from back to hip. It may also cause muscle weakness in leg and foot, numbness in leg and unpleasant tingling needles sensation. It is also known as sciatica neuritis, the main cause of sciatica is when herniated disc or bony spur in the spine put pressure on the spine nerve. According to Ayurveda aggravation of Vata dosha causes pain.
                <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                    <p>
                     It is common in people of occupation with physically strenuous positions as machine operators or truck drivers. Sciatica pain stays usually for 4-6 weeks and if not treated then it affects the neurological functioning. <strong>Sciatica pain treatment in Ayurveda</strong> will save you from surgery and provides best effective results.
                    </p>
                    <div class="btn-ayurvedic">
                        <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                    </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Sciatica Pain</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Spinal Injury
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                            Any accidental cause can harm spine thus causing sciatica pain.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Herniated Lumbar Disc
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            Formation of osteothytes bone spur causes pain in muscles near lumbar vertebrae which irritates the spinal cord causing pain.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Spinal Tumor
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                            Any growth in spinal cord leads to pressure on spinal vertebrae hence causing sciatica pain.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Lumbar Spinal Stenosis
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                            Here stenosis means decrease in narrowing of spinal canal hence pressure is exerted over spinal cord by muscles, bones and causing pain.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                Prolonged Sitiing
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                            Prolonged sitting and standing can also damage disk.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/sciatica-pain-symptoms.webp') }}" class="img-fluid"alt="Sciatica Pain Symstoms">
        </div>
        <div class="cvn sym" >
            <h3>Symptoms of <span>Sciatica Pain</span></h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Lower Back Pain</p>
            <p>The shooting pain or burning sensation from lower back pain radiating down to front or back of thigh and leg or foot.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Numbness</p>
            <p>Sciatica pain is accompanied by numbness in lower limbs or partially in fingers. Tingling sensation or weakness may also be present.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Posture Induced Symptoms</p>
            <p>A person complaints of worse pain while sitting, trying to stand up, bending spine forward, twisting the spine, lying down, or while coughing. A person gets relief on walking or applying hot compression over pelvic region.</p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Benefits of <b>Ayurveda for Sciatica Pain</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurvedic massage therapies calm the muscles and nerves and also relive tension.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Exercise and Yoga asana also help in relieving pain and discomfort. Gently stretch your joints and soft tissues by Yoga.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Avoid same activity for prolonged period i.e. sitting or standing.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Sip on Anti-inflammatory drinks like Turmeric milk, Ginger tea and green tea.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurvedic Medicines gives best results to treat sciatica pain without any surgery. Consult with sciatica pain treatment specialist at Dr. Sharda Ayurveda and get the best effective Ayurvedic treatment.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="K4SbexpmF6E" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/K4SbexpmF6E.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>


<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                            How long does Ayurveda take to treat sciatica pain?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                        Ayurveda helps in relieving pain quickly that too without surgery. Ayurveda is more beneficial and is without any side effects as other medicines do have in modern pathy. <strong>Ayurvedic treatment for sciatica pain</strong> is advised no matter how acute or worst the condition is.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                When sciatica pain is left untreated does it cause any complication?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                        Yes, if it is left untreated then it might lead to complete loss of sensation in area of sciatic nerve.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Is sciatic nerve on right or left side of the body?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                        One each side right and left and sciatic nerve runs through hips, buttocks and down a leg ending just below the knees.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                What are 4 types of sciatica?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                        Acute, chronic, alternating, bilateral sciatica.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                How should I lay with sciatica pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                        Always lie flat on back, keep heels and buttocks in contact with bed and bend on knees slightly towards ceiling and slide a pillow between bed and knees for support.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    Can prolonged sitting causes sciatica?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                            Yes, prolonged sitting causes sciatica as it exerts pressure over back and lumbar vertebrae.
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                What are the sign that shows nerve is damaged?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                        Pain on back radiates to the leg and then goes to toes is the sign for nerve damage. Some of the signs are- Pain on one side of body, difficulty sitting and walking, numbness and tingling sensation.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                What is the diet for sciatica patients?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                        The diet includes- Green vegetables, Grains including wheat, millets, pulses, Oats, cereals and barley. Add plenty of fluids in diet.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                Is it good to use heat and ice pads for sciatica pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                        If a patient suffers with inflammation then heat pads will increase the pain and inflammation. Try to change the diet and follow exercises. Heat fermentation will help decreasing the pain but in case of inflammation it worsens the condition.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                What can a patient do immediately after sciatica?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                        It is advised to take rest at the first preference, then patient should  apply  cold pad on effected areas that is lower back then apply any medicated oil.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEleven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Do heredity is also a cause of sciatica pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingEleven" data-parent="#accordion4">
                        <div class="card-body">
                        No, sciatica pain has no connection with heredity because it is a lifestyle born disease like wrong posture, sitting or standing, any injury or trauma to spine.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Sciatica Pain",
    "item": "https://www.drshardaayurveda.com/sciatica-pain"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "How long does Ayurveda take to treat sciatica pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurveda helps in relieving pain quickly that too without Surgery. Ayurveda is more beneficial and is without any side effects as other medicines do have in modern pathy. Ayurvedic treatment for sciatica pain is advised no matter how acute or worst the condition is."
    }
  },{
    "@type": "Question",
    "name": "When sciatica pain is left untreated does it cause any complication?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, if it is left untreated then it might lead to complete loss of sensation in area of sciatic nerve."
    }
  },{
    "@type": "Question",
    "name": "Is sciatic nerve on right or left side of the body?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "One each side right and left and sciatic nerve runs through hips, buttocks and down a leg ending just below the knees."
    }
  },{
    "@type": "Question",
    "name": "What are 4 types of sciatica?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Acute, chronic, alternating, bilateral sciatica."
    }
  },{
    "@type": "Question",
    "name": "How should I lay with sciatica pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Always lie flat on back, keep heels and buttocks in contact with bed and bend on knees slightly towards ceiling and slide a pillow between bed and knees for support."
    }
  },{
    "@type": "Question",
    "name": "Can prolonged sitting causes sciatica?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, prolonged sitting causes sciatica as it exerts pressure over back and lumbar vertebrae."
    }
  },{
    "@type": "Question",
    "name": "What are the sign that shows nerve is damaged?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Pain on back radiates to the leg and then goes to toes is the sign for nerve damage. Some of the signs are- Pain on one side of body, difficulty sitting and walking, numbness and tingling sensation."
    }
  },{
    "@type": "Question",
    "name": "What is the diet for sciatica patients?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The diet includes- Green vegetables, Grains including wheat, millets, pulses, Oats, cereals and barley. Add plenty of fluids in diet."
    }
  },{
    "@type": "Question",
    "name": "Is it good to use heat and ice pads for sciatica pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If a patient suffers with inflammation then heat pads will increase the pain and inflammation. Try to change the diet and follow exercises. Heat fermentation will help decreasing the pain but in case of inflammation it worsens the condition."
    }
  },{
    "@type": "Question",
    "name": "What can a patient do immediately after sciatica?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It is advised to take rest at the first preference, then patient should  apply  cold pad on effected areas that is lower back  then apply any medicated oil."
    }
  }]
}
</script>
@stop