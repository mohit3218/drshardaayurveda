<?php
/**
 * Premature Ejaculation Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Premature Ejaculation</h3>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/premature-ejaculation">Premature Ejaculation</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/Premature-ejaculation.webp') }}" class="why-choose-us img-fluid" alt="Premature Ejaculation Treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Premature Ejaculation</b> Ayurvedic Treatment</h1>
                <p>
                Premature ejaculation has become a very common problem affecting more than 40% of men. Premature ejaculation is also termed rapid ejaculation or early ejaculation. Ejaculation is described as when ejaculation occurs sooner than a man or his partner would like during sexual intercourse. It is the expulsion of semen from the body. Ejaculation is controlled by the nervous system. Out of fear or stress or any other reasons, a man is unable to maintain the sexual intercourse and ejaculates earlier as he loses his control. Sometimes it can be frustrating and even embarrassing. It has become a common sexual complaint and out of 10, four men suffer from it. In some cases, premature ejaculation is associated with erectile dysfunction that is when the penis does not remain firm enough for sexual activity. 
                <span id="dots" data-aos="fade-up" data-aos-duration="3000">...</span>
                </p>
                <span id="more-content">
                    <p>
                    Premature ejaculation if not treated timely and effectively with Ayurvedic medicines makes it hard to start a family because the sperms are unable to reach an egg for fertilization. It can also be caused due to depression which may be due to premature ejaculation and that affects sex drive or performance. It is not something to be depressed or worry about as Ayurvedic medicine for pre-mature ejaculation has the most effective results. To get free from all the male and female sexual disorders, consult the <strong>best sexologists in India</strong>. 
                    </p>
                    <div class="btn-ayurvedic">
                        <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                    </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Premature Ejaculation</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Body Factor
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                            Physical, emotional, and psychological factors cause premature ejaculation.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Depression
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            Depression, stress, anxiety affects the sex hormones due to which premature ejaculation occurs.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Serotonin
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                            Serotonin is a natural substance formed by nerves in the body. Low serotonin is chemical in the brain that is involved in sexual desire and shortens the time of ejaculation.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Lack of Confidence
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                            Lack of confidence and anxiety may lead to hesitation and fear which decreases the level of serotonin hence premature ejaculation occurs.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                Diabetes
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                                A person suffering from diabetes for a longer time complains of <a class="green-anchor" href="https://www.drshardaayurveda.com/erectile-dysfunction">Erectile Dysfunction</a> which further leads to pre-mature ejaculation.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<!-- <div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
    <div class="kmoz" >
        <img src="{{ URL::asset('front/images/symptoms-of-asthma.webp') }}" class="img-fluid"alt="Premature Ejaculation symptoms">
    </div>
    <div class="cvn sym" >
        <h3><span>Symptoms of</span> Premature Ejaculation </h3>
        <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">There is only a symptom of premature ejaculation that is the disease itself. Premature ejaculation is the inability to delay ejaculation for more than a few seconds after penetration</p>
    </div>
    </div>
</div> -->
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Benefits of <b>Ayurvedic Treatment for Premature Ejaculation</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Premature ejaculation is compared with Shukra Avrit Vata i.e. When Vata Dosh gets aggravated and disturbs the action of sperm.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    In Ayurveda, many Shukra Stambak Yog is mentioned in Vajikarna. This yoga increases the duration of the sexual act. This yoga poses Vaishya, Balya, Medhya, and Shukra Stambaka properties.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Vata i.e. anxiety and stress as a trigger factor in premature ejaculation can be handled by proper counseling and medhya drugs mentioned in Ayurveda.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Special diet is recommended to the patient to provide proper nourishment to males to enhance dhatudorbalya.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Special Yoga and meditation relieve Seritonil levels in the brain. 
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/sexual-disorder-common.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">
                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images') }}') no-repeat;">
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Premature-ejaculation Testimonial" >
                            <h3 class="usrname">Mr Ramandeep</h3>
                            <p class="desig">Sexual Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was very worried due to my weakness. My relation with my partner was getting disturbed slowly. I consulted Dr. Sharda Ayurveda for Premature ejaculation Ayurvedic treatment and the results were excellent. They advised me for Ayurvedic treatment for 3 months to complete the course and I followed them. I will forever be thankful to Dr. Sharda Ayurveda.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                            What is the timing for premature ejaculation?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                        There is no set time when a man should ejaculate during sexual intercourse but if he loses erection immediately after insertion is known as premature ejaculation. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                What happens if men release sperm daily?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                        Men with healthy normal sperm count should not worry about regular effects of regular ejaculation but with some pathophysiology, they may complain of weakness or inability to do sex.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Does Ayurvedic medicine have harmful effects on premature ejaculation?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                        No Ayurvedic medicine target both in physical and mental healthiness of male as it improves and nourish dhatu of body and without giving any adverse effect on the body and resolves premature ejaculation.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                How can I cure PME?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                        Behavioral treatment along with Ayurvedic medicinal treatment shows effective results in PME.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                What causes quick ejaculation?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                        Abnormal hormone level, infection of prostate or urethra, Diabetes mellitus, Abnormal levels of brain neurotransmitter leads to PME.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    Can PME be a cause of infertilitys?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                            If a man has PME and ejaculation never reaches the women’s vagina he may not be able to get his partner pregnant and extend his family. Adopt Ayurveda for premature ejaculation. 
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Is PME is same as Erectile dysfunction?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                        Erectile dysfunction is a situation where the man cannot have an erection whereas in PME man has an erection but ejaculates very soon within few seconds
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Which exercise is best for PME?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                        Kegal exercise is beneficial in PME and special methods are taught to stop immediate ejaculation by withdrawing during coitus, squeezing the penis when a male is about to ejaculate.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                Which diet helps in solving the issue of PME?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                        Diet rich in protein, minerals, omega fatty acids, and amino acids. Ayurvedic medicines are beneficial for the nourishment of dhatu which leads to proper erection and provides stamina to undergo PME
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Is stress the cause for PME?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                        Yes, serotonin levels affect a person’s mental state and a man firstly does not have an erection. If fortunately, he manages to have an erection with a loss of interest towards his partner patient is unable to hold his erection hence ejaculation occurs immediately within a second.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Premature Ejaculation",
    "item": "https://www.drshardaayurveda.com/premature-ejaculation"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What is the timing for premature ejaculation?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "There is no set time when a man should ejaculate during sexual intercourse but if he loses erection immediately after insertion is known as premature ejaculation."
    }
  },{
    "@type": "Question",
    "name": "What happens if men release sperm daily?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Men with healthy normal sperm count should not worry about regular effects of regular ejaculation but with some pathophysiology, they may complain of weakness or inability to do sex."
    }
  },{
    "@type": "Question",
    "name": "Does Ayurvedic medicine have harmful effects on premature ejaculation?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "No Ayurvedic medicine target both in physical and mental healthiness of male as it improves and nourish dhatu of body and without giving any adverse effect on the body and resolves premature ejaculation."
    }
  },{
    "@type": "Question",
    "name": "How can I cure PME?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Behavioral treatment along with Ayurvedic medicinal treatment shows effective results in PME."
    }
  },{
    "@type": "Question",
    "name": "What causes quick ejaculation?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Abnormal hormone level, infection of prostate or urethra, Diabetes mellitus, Abnormal levels of brain neurotransmitter leads to PME."
    }
  },{
    "@type": "Question",
    "name": "Can PME be a cause of infertility?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If a man has PME and ejaculation never reaches the women’s vagina he may not be able to get his partner pregnant and extend his family. Adopt Ayurveda for premature ejaculation."
    }
  },{
    "@type": "Question",
    "name": "Is PME is same as Erectile dysfunction?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Erectile dysfunction is a situation where the man cannot have an erection whereas in PME man has an erection but ejaculates very soon within few seconds."
    }
  },{
    "@type": "Question",
    "name": "Which exercise is best for PME?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Kegal exercise is beneficial in PME and special methods are taught to stop immediate ejaculation by withdrawing during coitus, squeezing the penis when a male is about to ejaculate."
    }
  },{
    "@type": "Question",
    "name": "Which diet helps in solving the issue of PME?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Diet rich in protein, minerals, omega fatty acids, and amino acids. Ayurvedic medicines are beneficial for the nourishment of dhatu which leads to proper erection and provides stamina to undergo PME."
    }
  },{
    "@type": "Question",
    "name": "Is stress the cause for PME?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, serotonin levels affect a person’s mental state and a man firstly does not have an erection. If fortunately, he manages to have an erection with a loss of interest towards his partner patient is unable to hold his erection hence ejaculation occurs immediately within a second."
    }
  }]
}
</script>
@stop