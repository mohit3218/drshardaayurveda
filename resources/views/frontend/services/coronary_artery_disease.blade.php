<?php
/**
 * Coronary Artery Disease Page 
 * 
 * @created    03/02/2022
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2022
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4">Coronary Artery Disease</h3>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/coronary-artery-disease">Coronary Artery Disease</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a  href="#types">TYPES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatment">TREATMENT</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc">
                    <img src="{{ URL::asset('front/images/coronary-artery-disease.webp') }}" class="why-choose-us img-fluid" alt="Coronary Artery Disease">
                </div>
            </div>
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " >
                <h1 class="heading1"><b>Coronary Artery Disease</b></h1>
                <p>
                Coronary artery diseases as per Ayurveda stated as <strong style="font-weight: bold;">“Krimija Hrida Roga”</strong> which is defined as the interference supply of oxygen-rich blood through arteries to the heart. This whole is the outcome of plaque formation that leads to narrowing and blockage of the arteries, which are the main suppliers of oxygen. As a result, there is an emergence of condition like a heart attack. In medical terms categorized as heart disease which involves atherosclerotic plaque formation in the vessel lumen. This directly can lead to impairment in blood flow, thus causing oxygen to be delivered to the myocardium (muscle layer of the heart). The prevalence of coronary artery disease (CAD) has increased in the past 2-3 decades on a global scale, but alone India accounts for 20% of deaths due to disease <strong style="font-style: italic;">(Trend in the prevalence of coronary artery disease and risk factors over two decades in rural Punjab (<a class="green-anchor"href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5818047/">nih.gov</a>))</strong>. The reason, according to Ayurveda is changing lifestyle. For heart disease treatment, Ayurveda is considered to be the best and safest option.
                <span id="dots">...</span>
                </p>
                <span id="more-content">
                <p>
                <strong style="font-weight: bold;">Ayurveda for heart disease</strong> has an altogether different approach to treatment compared to other medical practices. Ayurveda follows a systematic and root cause method which gives long-term recovery and reduces the chances of discomforting symptoms to reoccur. Dr. Sharda Ayurveda through its authentic and holistic Ayurvedic treatment provides the best and long-term result.
                </p>
                <div class="btn-ayurvedic">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn">Read more</button>
            </div>
        </div>
    </div>
</section>


<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate">
                    <h2 class="heading1">Causes of <b>Coronary Artery Disease</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="causes">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    High Cholesterol
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Deposition of fat in the blood vessels blocks the arteries, which directly hampers the blood flow to other parts of the body.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    High Blood Pressure
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                               Due to high blood pressure, the lining of the arteries gets damaged, thus increasing chances of deposition of fat and subsequently hindrance in blood flow. As a result, the heart works hard for a sufficient supply of oxygen-rich blood to the body.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion">
                        <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Lack of Physical Activity
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                Being inactive for too long can lead to the deposition of fatty material in the arteries. This directly causes damage and coagulation of arteries and therefore the process of blood flow is hampered.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Narrow Arteries
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">
                                Narrowed arteries can majorly cause chest pain. Due to blockage, insufficient blood flow to the heart muscle and rest of the body.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Smoking 
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                            <div class="card-body">
                                Smoking can significantly increase the formation of plaques in the blood vessels which makes difficult for arteries to carry blood to heart muscles efficiently. The presence of chemicals in cigarettes causes the thickening of blood and forms clots inside veins and arteries.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>			


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/coronary-artery-disease-symptoms.webp') }}" class="img-fluid" alt="Symptoms of Coronary Artery Disease">
        </div>
        <div class="cvn sym" >
            <h3>Symptoms of <span>Coronary Artery Disease</span></h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Chest Pain</p>
            <p>The pain is often referred to as <strong style="font-weight: bold;">“angina”</strong> which occurs in the middle or left side of the chest resulting in less supply of oxygen-rich blood to the heart through arteries.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Shortness of Breath </p>
            <p>When the heart is not supplied with enough blood as a result person may experience shortness of breath or extreme fatigue sometimes.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Dizziness</p>
            <p>A person suffering from coronary artery disease frequently experiences intermittent lightheadedness. This is accompanied by physical exertion at the same time.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Silent Heart Attack</p>
            <p>Type of heart attack that causes no major noticeable symptoms. These are characterized by constant chest pain and sometimes breathlessness.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Palpitations</p>
            <p>Fluctuated or irregular heartbeat is experienced as thumping or quivering sensations.</p>
        </div>
    </div>
</div>
<div class="split"></div>
<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " >
                <h2 class="client-test-title"style="font-size: 24px !important;">Ayurvedic Treatment for <b>Coronary Artery Disease</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        Panchakarma therapy is the basic Ayurvedic treatment that employs different procedures that helps in detoxification and purification of the bodily system. It is seen that specifically for CAD, Snehana (oleation) and Swedana (sudation or perspiration) therapies are applied.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        With just a few lifestyle modifications, one can get effectively recover from the disease which includes a daily walk for 30 minutes that directly improves heart function and reduces blood cholesterol levels.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        This simple yet effective Ayurvedic home remedy gives positive results, i.e., mix ½ tsp of ginger juice and ½ tsp of garlic juice in lukewarm water. Drink this twice a day for maintaining a healthy heart.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        Performing regular yoga and meditations helps build strong core by regulating the systolic blood pressure, total cholesterol, triglycerides, and LDL cholesterol levels in the body. The best poses recommended are <strong style="font-weight: bold;">Utkatasana, Bhujangasana, and Makarasana</strong>.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        Incorporate more fruits and raw vegetable salads in the diet. Additionally, it is recommended to avoid saturated fats, refined and processed food. 
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc">
                    <div id="dna_video">
                        <br>
                            <div class="youtube-player" data-id="PGKM-htmG9A" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/backpain.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                            </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Asthma Testimonial" >
                            <h3 class="usrname">Surjeet Gill</h3>
                            <p class="desig">Coronary Artery Disease Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                For the last 2 years, I was experiencing constant chest pain and breathlessness which significantly hampered me from even performing basic daily routine work. After consulting with doctors get to know that I have <a class="green-anchor"href="https://www.drshardaayurveda.com/high-cholesterol">high cholesterol levels</a> in the blood which is directly interfering with the functioning of heart. As guided started taking medicines for 6 months but there was no sign of improvement in my health. Therefore, visited Dr. Sharda Ayurveda and after consulting with their experts started with medications and strictly followed dietary and lifestyle suggestions told by them. And with just 3 months of treatment, I am fully recovered from the disease. I am thankful to Dr. Sharda Ayurveda for their effective <strong>Ayurvedic Treatment of Coronary Artery Disease</strong>.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Flatulence Testimonial" >
                            <h3 class="usrname">Hakam Singh</h3>
                            <p class="desig">Coronary Artery Disease Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                From last 6-7 months was facing the problem of increased palpitation quite often. After consulting doctors get to know that I am suffering from coronary artery disease. As soon as I got know consulted Dr. Sharda Ayurveda for the treatment where I was guided with a specialized diet chart and lifestyle recommendation along with herbal medications. I could not thank enough Dr. Sharda Ayurveda for their effective treatment which helped me recover from the disease within just 2 months.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               
                                <i class="fa" aria-hidden="true"></i>
                              What happens when coronary artery disease is left untreated?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            The detection of early symptoms and its effective management timely can help one to recover from coronary artery disease. But if timely treatment is not taken then it can be the reason for the development of certain serious life-threatening concerns including cardiogenic shock. These concerns have aftereffects i.e., severe injury to heart muscles, thus disrupting its functioning and irregular heart rhythm.
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                What risk factors are associated with coronary artery disease?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            The risks factors that are associated with CAD are:
                            <ul>
                                <li>Family history</li>
                                <li>Smoking</li>
                                <li>Circulatory diseases</li>
                                <li>Endocrine diseases</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Which treatment is best for treating coronary artery diseases (CAD)?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            <b>Ayurvedic Treatment of Coronary Artery Disease</b> is best for long-term and effective recovery. The treatment helps to maintain the elasticity of the arteries and prevents its blockage and hardening. This altogether reduces the risks of heart failure and other heart-associated diseases. As the treatment is natural and authentic therefore there are no chances of side effects.
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Do heart diseases emerge genetically?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                            Studies show that genetics do play some role in the development of diseases like high blood pressure, coronary artery disease, and other associated conditions. But, it is true that a person with a family history of heart diseases shares a common environment and other factors which may increase the risk of occurrence.
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingFive" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <i class="fa" aria-hidden="true"></i>
                               How heart disease can affect the lungs?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="card-body">
                            If the heart has lost the potential to pump blood effectively it is also associated with improper functioning of the lungs. As a result, blood backs up and raises the pressure in the veins inside the lungs thus pushing fluid into the air sacs. With time liquid builds up and it gets harder to even breathe properly.
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                How one can prevent the development of heart diseases?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                        <div class="card-body">
                            The precautionary measures are:
                            <ul>
                                <li>Eat a healthy diet.</li>
                                <li>Avoid having refined and processed food.</li>
                                <li>Do regular exercise.</li>
                                <li>Balance your mental stability.</li>
                                <li>Perform yoga and meditation daily.</li>
                                <li>Make sure to get enough sleep.</li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Can stress be a major cause of occurrence of heart diseases?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                        <div class="card-body">
                            Yes, stress can be a major reason for the emergence of heart diseases.
                            It is seen that stress can lead to high blood pressure, which can directly pose risk for heart attack and stroke. It can contribute to the development of serious cardiovascular diseases which are associated with risks including smoking, overeating, and lack of physical activity.
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Which vitamins are best that help recover from coronary artery disease?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                        <div class="card-body">
                           The best vitamins are:
                           <ul>
                               <li><b>Vitamin E –</b> It lessen the risk of heart attacks.</li>
                               <li><b>Vitamin C –</b> It improves the effective functioning of the arteries.</li>
                           </ul>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                               Which yoga poses are effective to recover from CAD?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                        <div class="card-body">
                            The yoga poses that are suggested best to recover from coronary artery diseases are <b>Nishpanda Bhava, Sthitaprarthanasana, and Anulom Vilom Pranayama</b>.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Are bananas good for maintaining a healthy heart?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                        <div class="card-body">
                            Yes, they are effective for maintaining healthy heart because they contain fiber, potassium, and folate. Additionally, a high fiber-rich diet lowers the risk of cardiovascular disease.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<style>
    .carousel-inner {position: relative;width: 100%;overflow: hidden;}
    .carousel-item {position: relative;float: left;width: 100%;margin-right: -100%;-webkit-backface-visibility: hidden;backface-visibility: hidden;transition: -webkit-transform .6s ease-in-out;transition: transform .6s ease-in-out;transition: transform .6s ease-in-out,-webkit-transform .6s ease-in-out;}
    .card-body {padding-top: 1.5rem;padding-bottom: 1.5rem;-webkit-border-radius: 0!important;border-radius: 0!important;flex: 1 1 auto;-ms-flex: 1 1 auto;}
    .card .card-body a h3{font-size: 18px;color: #ff9000;line-height: 1.4;}
    .card .card-body h3{color: #373433;font-size: 18px;    line-height: 1.4;}
    .font-weight-bold {font-weight: 700!important;}
    .card-title {margin-bottom: .75rem;}
    .card-img-top {width: 100%;border-top-left-radius: calc(.25rem - 1px);border-top-right-radius: calc(.25rem - 1px);}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            responsiveClass: !0,
            navigation : true,
            autoplay: true,
            loop: true,
            smartSpeed: 2500,
            singleItem : true,
            navigationText: ["<i class='fa fa-angle-left fa-2'></i>", "<i class='fa fa-angle-right fa-2'></i>"],
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                480: {
                    items: 1,
                    nav: !1
                },
                768: {
                    items: 2,
                    nav: !1
                },
                1000: {
                    items: 2,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1024: {
                    items: 2,
                    nav: !1,
                    loop: 0,
                    margin: 20
                },
                1366: {
                    items: 2,
                    nav: !1
                }
            }
        })
    });

</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Coronary Artery Disease",
    "item": "https://www.drshardaayurveda.com/coronary-artery-disease"  
  }]
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What happens when coronary artery disease is left untreated?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The detection of early symptoms and its effective management timely can help one to recover from coronary artery disease. But if timely treatment is not taken then it can be the reason for the development of certain serious life-threatening concerns including cardiogenic shock. These concerns have aftereffects i.e., severe injury to heart muscles, thus disrupting its functioning and irregular heart rhythm."
    }
  },{
    "@type": "Question",
    "name": "What risk factors are associated with coronary artery disease?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The risks factors that are associated with CAD are:
•   Family history 
•   Smoking 
•   Circulatory diseases 
•   Endocrine diseases"
    }
  },{
    "@type": "Question",
    "name": "Which treatment is best for treating coronary artery diseases (CAD)?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurvedic Treatment of Coronary Artery Disease is best for long-term and effective recovery. The treatment helps to maintain the elasticity of the arteries and prevents its blockage and hardening. This altogether reduces the risks of heart failure and other heart-associated diseases. As the treatment is natural and authentic therefore there are no chances of side effects."
    }
  },{
    "@type": "Question",
    "name": "Do heart diseases emerge genetically?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Studies show that genetics do play some role in the development of diseases like high blood pressure, coronary artery disease, and other associated conditions. But, it is true that a person with a family history of heart diseases shares a common environment and other factors which may increase the risk of occurrence."
    }
  },{
    "@type": "Question",
    "name": "How heart disease can affect the lungs?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If the heart has lost the potential to pump blood effectively it is also associated with improper functioning of the lungs. As a result, blood backs up and raises the pressure in the veins inside the lungs thus pushing fluid into the air sacs. With time liquid builds up and it gets harder to even breathe properly."
    }
  },{
    "@type": "Question",
    "name": "How one can prevent the development of heart diseases?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The precautionary measures are:
•   Eat a healthy diet.
•   Avoid having refined and processed food. 
•   Do regular exercise. 
•   Balance your mental stability. 
•   Perform yoga and meditation daily. 
•   Make sure to get enough sleep."
    }
  },{
    "@type": "Question",
    "name": "Can stress be a major cause of occurrence of heart diseases?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, stress can be a major reason for the emergence of heart diseases. It is seen that stress can lead to high blood pressure, which can directly pose risk for heart attack and stroke. It can contribute to the development of serious cardiovascular diseases which are associated with risks including smoking, overeating, and lack of physical activity."
    }
  },{
    "@type": "Question",
    "name": "Which vitamins are best that help recover from coronary artery disease?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The best vitamins are:
•   Vitamin E – It lessen the risk of heart attacks. 
•   Vitamin C – It improves the effective functioning of the arteries."
    }
  },{
    "@type": "Question",
    "name": "Which yoga poses are effective to recover from CAD?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The yoga poses that are suggested best to recover from coronary artery diseases are Nishpanda Bhava, Sthitaprarthanasana, and Anulom Vilom Pranayama."
    }
  },{
    "@type": "Question",
    "name": "Are bananas good for maintaining a healthy heart?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, they are effective for maintaining healthy heart because they contain fiber, potassium, and folate. Additionally, a high fiber-rich diet lowers the risk of cardiovascular disease."
    }
  }]
}
</script>
@stop