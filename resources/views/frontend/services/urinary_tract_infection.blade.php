<?php
/**
 * Urinary Tract Infection Page 
 * 
 * @created    23/12/2021
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Urinary Tract Infection (UTI)</h3>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/urinary-tract-infection">Urinary Tract Infection</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatment">TREATMENT</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="definition">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/urinary-tract-infection.webp') }}" class="why-choose-us img-fluid" alt="Urinary Tract Infection">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1">What is <b>Urinary Tract Infection (UTI)?</b></h1>
                <p>
                Urinary tract infection is becoming one of the most serious and common infections affecting millions worldwide. Urinary tract infection in Ayurveda is described as the imbalance of the Pitta Dosha and subsequently. UTI is the infection of the urinary system which includes the kidney, bladder, urethra, and ureters but the most affected parts are the lower tracts. Urinary tract infections typically occur when bacteria enter the urinary tract through the urethra and begin to multiply themselves in the bladder which causes trouble while peeing. This infection is more common in women than men due to anatomical differences which is the smaller size of the urethra in females making it easier for bacteria to invade the bladder. 
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                    <p>
                    There are mainly two types of UTI known one of which is <a class="green-anchor"href="https://www.drshardaayurveda.com/cystitis">cystitis</a> and other urethritis which are associated with the bladder and urethra respectively. Doctors try treating urinary tract infections with antibiotics which can easily be treated in Ayurveda. Ayurvedic treatment for UTI from Dr. Sharda Ayurveda treats you naturally. By increasing the water intake, adding more fluids in diet, maintaining personal hygiene, adopting a healthy lifestyle one easily recovers followed by Ayurvedic treatment. Within just a few days of treatment, one can feel the difference.
                    </p>
                    <div class="btn-ayurvedic">
                        <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                    </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" style="background-color:#f3f3f3" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Urinary Tract Infection</b></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Infection in the Bladder
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                            The reason for infection in the bladder can be having diabetes, and unhealthy sexual intercourse. It is seen to happen in pregnant women commonly due to hormonal changes leading to relaxing of the bladder muscles and thus delay in a clearing.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Constipation
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            Due to constipation people face difficulty in emptying their stomach, therefore, making way for bacteria to grow and cause infection. Also uncontrolled feces i.e., loose stools which may have bacteria can easily be able to make their way to the vagina.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Holding Urine for Longer
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                            Most people get UTI if they control urine for a longer period of hours which gives way to bacteria already present in the bladder to overgrow and thus causing infection.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Dehydration
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                            Keeping your body dehydrated for a longer time would lead to less urination thereby infections in the body are not flushed out.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                Lack of Personal Hygiene
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                                Not maintaining the vaginal flora or using the feminine products inappropriately or without guidance can majorly cause the urinary tract infection.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-strip" style="background: #eeb03f";>
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a free Assessment Report For  <strong>UTI Ayurvedic Treatment</strong></p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz px-2.5" >
            <img src="{{ URL::asset('front/images/uti-symptoms.webp') }}" class="img-fluid"alt="Urinary Tract Infection symptoms infographic" title="Urinary Tract Infection symptoms infographic">
        </div>
        <div class="cvn sym" >
            <h3>Symptoms of <span>Urinary Tract Infection</span></h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Irritating and Painful Urination</p>
            <p>The condition is called dysuria where the persons experience a burning sensation, and itchiness while passing urine.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Pelvic Pain</p>
            <p>Pelvic is the lower abdominal area of the women which also includes the sexual organs. Pelvic is the house of bladder, womb, and ovaries therefore pain in any of these areas can be a possible symptom of the urinary tract infection.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Strong Odor Urine</p>
            <p>Urine contains water and a small concentration of the waste or byproducts of our body. It is seen urine with foul, fishy, and sweet smells can be an indicator of UTI.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Rectal Area</p>
            <p>Swelling and inflammation of the prostate gland is called prostatitis. This will cause pain between the scrotum and anus or the lower back, penis, or testes.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Nausea</p>
            <p>It means that bacteria that are responsible for the cause of UTI have made their path to reach the kidneys. Due to which kidneys get inflamed and which is known as pyelonephritis. This infection is serious because it directly affects and damages the kidneys.</p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;"><b>URINARY TRACT INFECTION (UTI)</b> Ayurvedic Treatment</h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    It is suggested to avoid caffeine-based products in UTI and switch to <b>Ayurvedic herbal tea and juices like cranberry and blueberry</b>.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    It is believed that Garlic is one the most common herb that exhibits anti-bacterial property thus keeping one away from the infection.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Practicing yoga and meditation followed with Ayurvedic treatment has shown effective results in treating urine infection.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurveda is known to be the natural and simpler way of restoring the body’s bacterial balance. This is so by aiding the healthy bacteria and destroying the harmful ones.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Ayurvedic herbal medicine for urinary tract infections is known to have <b>phytoextracts (plant extracts)</b> showing favorable results in treating urinary disorders. This could be an alternative to the pathogens which are resistant to antibiotics for treating urinary tract infection.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="PGKM-htmG9A" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/backpain.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Patient's <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Harleen Kaur UTI Patient" title="Harleen Kaur UTI Patient">
                            <h3 class="usrname">Harleen Kaur</h3>
                            <p class="desig">UTI Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Earlier this year I got Urinary Tract Infection which I got treated for and was cured. But again after 2 months, I got the same problem so I thought of switching towards Ayurveda as it works on the root cause treatment of the disease. I consulted Dr. Sharda Ayurveda. With their guidance and Ayurvedic treatment, I got treated within a week. I am thankful to Dr. Sharda Ayurveda.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Dapinder Kaur UTI Patient" title="Dapinder Kaur UTI Patient">
                            <h3 class="usrname">Dapinder Kaur</h3>
                            <p class="desig">UTI Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Despite maintaining personal hygiene and care than also after every month I used to get infected with urinary tract infection. So for this, I consulted Dr. Sharda Ayurveda where I got the Ayurvedic treatment and within two weeks of the treatment, I did not observe any more symptoms of UTI. I thank Dr. Sharda Ayurveda for getting the positive results.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1"><b>FAQ'S</b></h2>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                            Why UTI is most common in women compared to men?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                        The possible reason for this relies on the anatomical difference i.e., women have thin, narrower urethra compared to men thus allowing bacteria to invade the urinary tract. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                What are some of the risk factors associated with UTI?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                        The reasons are:
                        <li>Consuming birth control pills like spermicide.</li>
                        <li>Not maintaining personal hygiene.</li>
                        <li>People with diabetes are prone due to high sugar levels.</li>
                        <li>Due to kidney stones there is often difficulty in urinating.</li>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Why are STI and UIT terms confused with one another?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                        This is because of the common overlapping symptoms between the two i.e., burning or pain while urinating, foul odor of urine, lower abdominal pain, and unusual discharge.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Does UTIs and bladder infection differ from each other?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                        Yes, they do differ from each other. UTI is the common term used for referring to the infection of the urinary tract. But bladder infection which is also known as <a class="green-anchor" href="https://www.drshardaayurveda.com/cystitis">cystitis</a> causes inflammation in the bladder.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                How one can prevent UTI?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                        <li>Keep yourself hydrated.</li>
                        <li>Add Vitamin C to your diet as it promotes acidic urine thereby killing bacteria.</li>
                        <li>Avoid birth control pills.</li>
                        <li>Maintain personal hygiene before and after sexual intercourse.</li>
                        <li>Add herbal juices to diet.</li>
                        <li>Avoid consuming caffeine-based products.</li>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    How UTI can be cured with Ayurvedic treatment?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                            Ayurvedic treatment has long been suggested for treating various serious diseases. Ayurvedic herbs bearberry leaves, garlic, and juices are highly recommended. Ayurvedic treatment at Dr. Sharda Ayurveda is proven to be effective with positive results.
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                            What are the notable signs through which a person can detect UTI?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                        The signs and symptoms of urinary tract infection are:
                        <li>Itchiness and pain while urinating.</li>
                        <li>Pain in the lower abdominal area.</li>
                        <li>Frequent urination.</li>
                        <li>Strong odor urine.</li>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                What would happen if the urinary tract infection is left untreated?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                        Following complications would happen if the infection remains untreated:
                        <li>Pyelonephritis, condition of kidney failure and damage.</li>
                        <li>Bacteria of UTI may also infect the bloodstreams which can cause death. The visible symptoms include dizziness and increased heart rate.</li>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                How long UTI takes to get away?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                        Natural treatment has shown visible results. It is known that in women it takes 2-3 days to get away but in men, it takes around 7-9 days. Ayurvedic medication is always preferred for treating the infection as they are free from side effects and natural.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                When should one consult with the doctors for treating UTI?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                            It is suggested that if a person suspects certain serious symptoms related to urinary tract infection they should immediately consult with the doctors before it's too late.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')

<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Urinary Tract Infection",
    "item": "https://www.drshardaayurveda.com/urinary-tract-infection"  
  }]
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "Why UTI is most common in women compared to men?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The possible reason for this relies on the anatomical difference i.e., women have thin, narrower urethra compared to men thus allowing bacteria to invade the urinary tract."
    }
  },{
    "@type": "Question",
    "name": "What are some of the risk factors associated with UTI?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The reasons are: • Consuming birth control pills like spermicide. • Not maintaining personal hygiene. • People with diabetes are prone due to high sugar levels. • Due to kidney stones there is often difficulty in urinating."
    }
  },{
    "@type": "Question",
    "name": "Why are STI and UIT terms confused with one another?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "This is because of the common overlapping symptoms between the two i.e., burning or pain while urinating, foul odor of urine, lower abdominal pain, and unusual discharge."
    }
  },{
    "@type": "Question",
    "name": "Does UTIs and bladder infection differ from each other?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, they do differ from each other. UTI is the common term used for referring to the infection of the urinary tract. But bladder infection which is also known as cystitis causes inflammation in the bladder."
    }
  },{
    "@type": "Question",
    "name": "How one can prevent UTI?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "• Keep yourself hydrated. • Add Vitamin C to your diet as it promotes acidic urine thereby killing bacteria. • Avoid birth control pills. • Maintain personal hygiene before and after sexual intercourse. • Add herbal juices to diet. • Avoid consuming caffeine-based products."
    }
  },{
    "@type": "Question",
    "name": "How UTI can be cured with Ayurvedic treatment?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurvedic treatment has long been suggested for treating various serious diseases. Ayurvedic herbs bearberry leaves, garlic, and juices are highly recommended. Ayurvedic treatment at Dr. Sharda Ayurveda is proven to be effective with positive results."
    }
  },{
    "@type": "Question",
    "name": "What are the notable signs through which a person can detect UTI?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The signs and symptoms of urinary tract infection are: • Itchiness and pain while urinating. • Pain in the lower abdominal area. • Frequent urination. • Strong odor urine."
    }
  },{
    "@type": "Question",
    "name": "What would happen if the urinary tract infection is left untreated?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Following complications would happen if the infection remains untreated: • Pyelonephritis, condition of kidney failure and damage. • Bacteria of UTI may also infect the bloodstreams which can cause death. The visible symptoms include dizziness and increased heart rate."
    }
  },{
    "@type": "Question",
    "name": "How long UTI takes to getaway?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Natural treatment has shown visible results. It is known that in women it takes 2-3 days to get away but in men, it takes around 7-9 days. Ayurvedic medication is always preferred for treating the infection as they are free from side effects and natural."
    }
  },{
    "@type": "Question",
    "name": "When should one consult with the doctors for treating UTI?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It is suggested that if a person suspects certain serious symptoms related to urinary tract infection they should immediately consult with the doctors before it's too late."
    }
  }]
}
</script>

@stop