<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Osteoporosis</h3>
                    <p style="font-size: 24px;">Protect Your Joints with Timely Treatment</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/osteoporosis">Osteoporosis</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/osteoporosis.webp') }}" class="why-choose-us img-fluid" alt="Osteoporosis Treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Osteoporosis</b> Ayurvedic Treatment</h1>
                <p>
                The term Osteo means Bone and porosis means porous or thinning, therefore osteoporosis is defined as thinning of the bone (bone density) due to reduction in the bone mass ( all components of bone, not only calcium). It is a condition defined as when the bones become weak and brittle and thus leads to fracture even on a sudden fall or mild stress on the bone. Bone is made up of living tissue that gets broken and replaced constantly and it is a condition where new bone tissues are not formed. 
                All the bones have some small spaces and when osteoporosis occurs, therefore, it increases the size of these spaces i.e. more of a gap thus causes the bone to lose its strength and density. It can occur at any age but mostly in older adults. Women are 4 times more prone to osteoporosis, especially after menopause.

                <span id="dots" data-aos="fade-up" data-aos-duration="3000">...</span>
                </p>
                <span id="more-content">
                    <p>
                    Osteoporosis mainly affects the hips, ribs, wrist, and spine. There are several modern medicines that cause side effects to the bone which leads to osteoporosis. Bones are made up of protein collagen band calcium when bone mass decreases after the age of 35 calcium started decreasing hence no new bone mass is formed leads to the spongy bone. Ayurvedic medicines, a healthy nutrient-rich diet, and weight-bearing exercises prescribed by the Ayurvedic doctor can help prevent bone loss and strengthen weak bones. <strong>Ayurveda for Osteoarthritis treatment</strong> has the most effective and life-long results.
                    </p>
                    <div class="btn-ayurvedic">
                        <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                    </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Osteoporosis</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Less Calcium Intake
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Low calcium intake leads to diminished bone density, hence more risk of fracture.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Auto-Immune Disease
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                Patient having a history of <a class="green-anchor" href="https://www.drshardaayurveda.com/rheumatoid-arthritis">rheumatoid arthritis</a> is more at risk of osteoporosis.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Age
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                            Loss of bone mass with age. Due to aging bones start getting breakdown and are more fragile thus are more prone to breakage.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Sedentary Lifestyle and Obesity
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                                When the body gets no physical workout it starts degenerating itself and more prone to diseases.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Thin and Small Body Frame
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                                Men and women who have small body frames are at a higher risk because they might have less bone mass to draw from as they age.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="{{ URL::asset('front/images/osteoporosis-symptoms.webp') }}" class="img-fluid"alt="Osteoporosis symptoms">
        </div>
        <div class="cvn sym" >
            <h3><span>Symptoms of</span> Osteoporosis </h3>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Weak Nails</p>
            <p>People suffering from osteoporosis have weak and brittle nails.</p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Weak and Thin Bones</p>
            <p>People suffering from osteoporosis lose their grip strength due to weak and thin bone. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Stooped Posture</p>
            <p>Stopped posture or bending forward is also due to weak and thin bones. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Bone Fracture</p>
            <p>Bone that breaks much more early than expected. </p>
            <p class="sub"><img src="{{ URL::asset('front/images/2.png') }}" alt="2.png">Shortness of Breath </p>
            <p>Shortness of breath due to decreased lung capacity. </p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Benefits of <b>Ayurvedic Treatment for Osteoporosis</b></h2>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    In Ayurveda 5th Dhatu is bone ie Asthi. It is said that bones are formed from Medha Dhatu thought Asthivaha channels, vayu, agni, and prithavi mahabutas. If digestive fire is affected it leads to a lack of nutrition to bones causing osteoporosis or Asthikshaya.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Treatments like Abhyanga, Vasti karma are highly recommended to maintain bone health.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Improper dietary and lifestyle habits cause vitiation of Vata and production of Ama in the body. This leads to pain in the bones.
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    Hence while treatment in Ayurveda Agnimandya is treated which treats calcium metabolism i.e. regulates digestion and absorption of calcium and phosphate
                </p>
                <p class="im3">
                    <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                    <strong>Osteoporosis treatment in Ayurveda</strong> is prescribed followed by rejuvenation external therapies which help in increasing the blood circulation, nourishes the muscles.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/PGKM-htmG9A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                            How can one get to know that they have osteoporosis?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                        If one feels tired all the time and gets injured very easily on bone and also some important blood Investigations are usually performed to diagnose osteoporosis that as bone densitometry,dual-energy and x-ray. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                What is the prevention for osteoporosis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                        A high calcium-rich diet along with Vitamin- D supplements, lifestyle modifications should be done. Avoid prolonged sitting and standing and stop smoking and alcohol
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                What lifestyle changes should be done to avoid osteoporosis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                        Follow a systematic daily way of living, avoid excess mental and physical stress. Do not suppress the natural urge. Indulge in light walking and swimming.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                What is the main cause of osteoporosis in women?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                        It is caused mainly due to decrease in estrogen levels which help to build and maintain bony structure, hence after menopause osteoporosis occurs.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Is osteoporosis a serious disease?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                        Yes, it is a serious disease as it leads to a decrease in height, also affects or limits the mobility of bones due to pain in bones. Opt for <strong>Osteoporosis Ayurvedic treatment in India</strong>, and see the change within your health and get those strong bones back.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    What are the different stages of Osteoporosis?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                            <ul>
                                <li>1st stage when new bone remodeling >rate of breakdown</li>
                                <li>2nd stage is when the rate of bone breakdown outweighs the rate of bone deposition</li>
                                <li>3rd stage is when the breakdown of bone faster due to estrogen hormone depletion</li>
                                <li>4th stage is when significant bone loss is visible</li>
                            </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Is sitting for longer hours bad for osteoporosis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                        Sitting causes pressure into the front of the spine, which increases the risk of compression fracture. If it occurs it can also trigger acascade of fractures
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Is osteoporosis normal with aging?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                        Yes, osteoporosis is a part of aging in old age bone loses mass and people may experience painful broken bones
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                Which joints are more likely to be affected by osteoporosis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                        The hip joint, wrist joint, and spine are affected in osteoporosis when the spine is affected then the patient's posture is called stopped posture
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Is walking beneficial for osteoporosis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                        Walking little a day can help in building bone health, so it is advised to the patient to go for a walk for 30 minutes daily.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('../partials/frontend/blogs_post_section')

@include('../partials/frontend/form')
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Osteoporosis",
    "item": "https://www.drshardaayurveda.com/osteoporosis"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "How can one get to know that they have osteoporosis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If one feels tired all the time and gets injured very easily on bone and also some important blood Investigations are usually performed to diagnose osteoporosis that as bone densitometry, dual-energy and x-ray."
    }
  },{
    "@type": "Question",
    "name": "What is the prevention for osteoporosis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "A high calcium-rich diet along with Vitamin- D supplements, lifestyle modifications should be done. Avoid prolonged sitting and standing and stop smoking and alcohol."
    }
  },{
    "@type": "Question",
    "name": "What lifestyle changes should be done to avoid osteoporosis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Follow a systematic daily way of living, avoid excess mental and physical stress. Do not suppress the natural urge. Indulge in light walking and swimming."
    }
  },{
    "@type": "Question",
    "name": "What is the main cause of osteoporosis in women?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It is caused mainly due to decrease in estrogen levels which help to build and maintain bony structure, hence after menopause osteoporosis occurs."
    }
  },{
    "@type": "Question",
    "name": "Is osteoporosis a serious disease?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it is a serious disease as it leads to a decrease in height, also affects or limits the mobility of bones due to pain in bones. Opt for Osteoporosis Ayurvedic treatment in India, and see the change within your health and get those strong bones back."
    }
  },{
    "@type": "Question",
    "name": "What are the different stages of Osteoporosis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "1st stage when new bone remodeling >rate of breakdown
2nd stage is when the rate of bone breakdown outweighs the rate of bone deposition. 
3rd stage is when the breakdown of bone faster due to estrogen hormone depletion. 
4th stage is when significant bone loss is visible."
    }
  },{
    "@type": "Question",
    "name": "Is sitting for longer hours bad for osteoporosis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Sitting causes pressure into the front of the spine, which increases the risk of compression fracture. If it occurs it can also trigger a cascade of fractures."
    }
  },{
    "@type": "Question",
    "name": "Is osteoporosis normal with aging?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, osteoporosis is a part of aging in old age bone loses mass and people may experience painful broken bones."
    }
  },{
    "@type": "Question",
    "name": "Which joints are more likely to be affected by osteoporosis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The hip joint, wrist joint, and spine are affected in osteoporosis when the spine is affected then the patient's posture is called stopped posture."
    }
  },{
    "@type": "Question",
    "name": "Is walking beneficial for osteoporosis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Walking little a day can help in building bone health, so it is advised to the patient to go for a walk for 30 minutes daily."
    }
  }]
}
</script>
@stop