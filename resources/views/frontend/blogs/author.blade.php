<?php
/**
 * Author Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<?php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$routeName = \Route::currentRouteName();
?>
<section class="section-wrap blog-standard pb-50" id="treatment" style="padding: 35px 0px;">
    <ul class="breadcrumb-item text-left" style="margin-left: 3%;">
        <li class="breadcrumb-li active" aria-current="page"><a href="/blogs/">Blogs</a>&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
        <li class="breadcrumb-li active" aria-current="page">Author&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
        <li class="breadcrumb-li"><a href="/author/<?= $routeName; ?>"><?= $author_name; ?></a></li>
    </ul>
    <div class="container-fluid">
        <div class="row cut-row">
        <div class="col-md-8 col-xs-12">
            <?php foreach($records as $record) : ?>
            
                <article class="entry-item" style="margin-bottom: 0 !important;">
                    <div class="entry-wrap" style="padding-bottom: 0 !important;">
                        
                        <h1 class="entry-title"><?= $record['post_title']; ?></h1>
                        <ul class="breadcrumb-item text-left">
                            <li class="breadcrumb-li active" aria-current="page">Last updated on <?= date('F jS, Y' , strtotime($record['created_at'])); ?> by <?= $author_name; ?></li>
                        </ul>
                        <div class="entry-content">
                            <div class="row">
                                <div class="col-md-3">
                                    <img  src="/uploads/posts/{{ $record['post_image'] }}" />
                                </div>
                                <div class="col-md-9">
                                    <p>
                                        <?= strip_tags(Str::limit($record['post_contant'], 200)); ?> <a href="/blogs/<?=str_replace(array( '(', ')' ), '', str_replace(" ","-", strtolower($record['name']))); ?>">Read More »</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            
            <hr>
            <?php endforeach; ?>
            </div>

            <div class="col-md-4 col-xs-12 sidebar">
                <div class="col-md-12 contact-us">
                    <h3 class="widget-title heading relative bottom-line left-align">GET IN TOUCH</h3>
					<form id="form_contact" action="https://www.drshardaayurveda.com/submit_lp_page.php" method="POST">
						<input type='hidden' name="subject" value="<?= $routeName; ?>">
                        <input type='hidden' name="action" value="user_mail">
                        <input type='hidden' name="return_url" value="https://www.drshardaayurveda.com/thanks.html">
                        <input type='hidden' name="page_url" value="<?= $actual_link; ?>">
						<div class="contact-fname">
							<div class="error form_error form-error-fname" id="form-error-fname"></div>
							<input name="name" id="fname" type="text" placeholder="Name*">
						</div>
						<div class="contact-email">
							<div class="error form_error form-error-email" id="form-error-email"></div>
							<input name="email" id="email" type="email" placeholder="E-mail*">
						</div>
						<div class="contact-mobile">
							<div class="error form_error form-error-mobile" id="form-error-mobile"></div>
							<input name="mobile" id="mobile" type="text" placeholder="Mobile">
						</div>
						<div class="error form_error form-error-comment" id="form-error-comment"></div>
						<textarea rows="2" name="description" id="comment" placeholder="Message" rows="9"></textarea>
						<input type="submit" class="btn btn-lg btn-color" value="Submit">
						<div id="msg" class="message"></div>
					</form>
                </div>
            </div>
        </div>
    </div>
</section>
@stop