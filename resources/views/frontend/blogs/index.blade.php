<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<?php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$routeName = \Route::currentRouteName();
$category_title = str_replace("-"," ", strtolower(request()->segment(3)));
?>
<style>#errmsgMobile,#errmsgEmail,.error-form{color: red;} .success-form{color: green;} .display-none{display: none;}</style>
<section class="blog-list blog-standard pb-50" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-md-3 col-xs-12 sidebar" data-aos="fade-up">
                <div class="sidebar">
                    <div class="ctgry-bx box hide-mobile">
                        <div class="inner">
                            <h3 class="widget-title heading relative bottom-line left-align">CATEGORY</h3>
                            <ul>
                                <?php  foreach (StaticArrays::$post_categories as $id => $name) : ?>
                                    <li <?php if(ucwords($category_title) == $name) : ?> class="highlighted" <?php endif; ?>><a href="/blogs/category/<?=str_replace(array( '(', ')' ), '', str_replace(" ","-", strtolower($name))); ?>"> <?= $name; ?> </a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 contact-us">
                    <h3 class="widget-title heading relative bottom-line left-align" style="color:#d49925;">GET IN TOUCH</h3>
                    
                    <span class="success-form display-none">Thank you for submitting your details, Our patient advisor will contact you soon.</span>
                    <?php if (isset($_GET['error']) && $_GET['error'] != "") { ?>
                        <span class="error-form"><?php echo $_GET['error']; ?>&#8230;.</span> <br/>
                    <?php } ?>
                    <form id="form_contact" action="https://www.drshardaayurveda.com/submit_lp_page_google_captcha.php" method="POST">
                        <input type='hidden' name="subject" value="<?= $routeName; ?>">
                        <input type='hidden' name="action" value="user_mail">
                        <input type='hidden' name="return_url" value="https://www.drshardaayurveda.com/thanks.html">
                        <input type='hidden' name="page_url" value="<?= $actual_link; ?>">
                        <div class="contact-fname">
                            <div class="error form_error form-error-fname" id="form-error-fname"></div>
                            <input name="name" id="fname" type="text" placeholder="Name*" required>
                        </div>
                        <div class="contact-email">
                            <div class="error form_error form-error-email" id="form-error-email"></div>
                            <input name="email" id="email" type="email" placeholder="E-mail*">
                            <span id="errmsgEmail"></span><br>
                        </div>
                        <div class="contact-mobile">
                            <div class="error form_error form-error-mobile" id="form-error-mobile"></div>
                            <input min="0" class="mobile" name="mobile" id="mobile" type="number" placeholder="Mobile" pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}" required>
                            <span id="errmsgMobile"></span><br>
                        </div>
                        <div class="error form_error form-error-comment" id="form-error-comment"></div>
                        <textarea rows="2" name="description" id="comment" placeholder="Message" rows="9"></textarea>
                        <!--Local-->
                        <!--<button type="submit" class="btn btn-lg btn-color g-recaptcha" data-sitekey="6LfEsOclAAAAAJsozhE_palqbg2un0C6HBMAtacd" data-callback='onSubmit' data-action='submit' value="Submit">Submit</button>-->
                        <!--Live-->
                        <button type="submit" class="btn btn-lg btn-color g-recaptcha" data-sitekey="6LcdxeglAAAAAI4u0cH7jiY5S3y-tEG6LqocURj2" data-callback='onSubmit' data-action='submit' value="Submit">Submit</button>
                        <!--<input type="submit" class="btn btn-lg btn-color" value="Submit">-->
                        <div id="msg" class="message"></div>
                    </form>
                </div>
            </div>
           
            <div class="col-md-9 col-xs-12">
                <div class="right-cont">
                    <div class="listings">
                        <?php foreach($records as $record) : ?>
                        <div class="list">
                            <a href="/blogs/<?=str_replace(array( '(', ')' ), '', str_replace(" ","-", strtolower($record->name))); ?>"></a>
                            <div class="image">
                                <a href="/blogs/<?=str_replace(array( '(', ')' ), '', str_replace(" ","-", strtolower($record->name))); ?>">
                                    <img src="/uploads/posts/{{ $record->post_image }}" alt="{{ $record->post_title }}" title="{{ $record->post_title }}">
                                </a>
                                <h4 class="img-head">
                                    <a href="/blogs/category/<?=str_replace(array( '(', ')' ), '', str_replace(" ","-", strtolower(StaticArrays::$post_categories[$record->category]))); ?>">{{ StaticArrays::$post_categories[$record->category] }}</a>
                                </h4>
                            </div>

                            <div class="desc">
                                <h3><a href="/blogs/<?=str_replace(array( '(', ')' ), '', str_replace(" ","-", strtolower($record->name))); ?>">{{ $record->post_title }}</a></h3>
                                <div class="summry">
                                    <!-- <p class="admin">By <?= $users[$record->created_by] ?></p> -->
                                    <p class="admin">Publish Date</p>
                                    <p class="date"><?= date('d-M' , strtotime($record->created_at)); ?>, <?= date('Y' , strtotime($record->created_at)); ?></p>
                                </div>
                            </div>

                            <div class="btn_outer">
                                <div class="know-btn orange">
                                <a href="/blogs/<?=str_replace(array( '(', ')' ), '', str_replace(" ","-", strtolower($record->name))); ?>"> <span>Read More</span></a> </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@stop