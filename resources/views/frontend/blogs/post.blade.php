<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<?php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$routeName = \Route::currentRouteName();
?>
<style>#errmsgMobile,#errmsgEmail,.error-form{color: red;} .success-form{color: green;} .display-none{display: none;}</style>
<section class="section-wrap blog-standard pb-50 blogs-style" id="treatment" style="padding: 35px 0px;">
    <ul class="breadcrumb-item text-center" style="margin-left: 3%;">
        <li class="breadcrumb-li active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
        <li class="breadcrumb-li"><a href="/blogs/<?=str_replace(array( '(', ')' ), '', str_replace(" ","-", strtolower($records['name']))); ?>"><?= $records['post_title']; ?></a></li>
    </ul>
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-md-8 col-xs-12" data-aos="fade-up">
                <article class="entry-item">
                    <div class="entry-wrap">
                        <ul class="entry-meta">
                            <li class="entry-date"> By <?= $users[$records['created_by']] ?>, <?= date('d-M' , strtotime($records['created_at'])); ?>, <?= date('Y' , strtotime($records['created_at'])); ?></li>
                            <li class="entry-category">
                                <a href="<?= $actual_link; ?>"><?= StaticArrays::$post_categories[$records['category']]; ?></a>												
                            </li>
                            <li>
                                <div class="custom-logo-sec blog-tp-social-icon">
                                    {!! $shareComponent !!}
                                </div>
                            </li>
                        </ul>
                        
                        <!-- <ul class="breadcrumb-item">
                            <li class="breadcrumb-li active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                            <li class="breadcrumb-li"><a href="/blogs/<?=str_replace(array( '(', ')' ), '', str_replace(" ","-", strtolower($records['post_title']))); ?>"><?= $records['post_title']; ?></a></li>
                        </ul> -->
                        <h1 class="entry-title"><?= $records['post_title']; ?></h1>
                        <div class="entry-content">
                            <p>
                                <?= $records['post_contant']; ?>
                            </p>
                            <div class="content-single-author-profile-wrap">
                                <div class="row">
                                    <div class="col-md-12 entry-title-wrap">
                                        <h4>Article By:</h4>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-2 avatar-warp">
                                                <?php if($records['created_by'] == 1) { ?> 
                                                <img alt="" src="{{ URL::asset('front/images/blog-logo-135.webp') }}" class="avatar avatar-135 photo" height="135" width="135" loading="lazy">                
                                                <?php } else if($records['created_by'] == 3) { ?>
                                                    <img alt="" src="{{ URL::asset('front/images/dr-mukesh-sahrda-135.webp') }}" class="avatar avatar-135 photo" height="135" width="135" loading="lazy"> <?php } ?>
                                            </div>
                                            <div class="col-md-10 author-content-wrap">
                                                <h4 class="author-name"><?= $users[$records['created_by']] ?></h4>
                                                <p class="author-nickname">
                                                    <?= $records['article_by']; ?>                   
                                                </p>
                                                <p class="author-about"></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div id="disqus_thread"></div> -->
                        </div>
                    </div>
                </article>
            </div>
            <div class="col-md-4 col-xs-12 sidebar" data-aos="fade-up">
                <div class="col-md-12 contact-us">
                    <h3 class="widget-title heading relative bottom-line left-align">GET IN TOUCH</h3>
                    <?php if (isset($_GET['error']) && $_GET['error'] != "") { ?>
                        <span class="error-form"><?php echo $_GET['error']; ?>&#8230;.</span> <br/>
                    <?php } ?>
					<form id="form_contact" action="https://www.drshardaayurveda.com/submit_lp_page_google_captcha.php" method="POST">
						<input type='hidden' name="subject" value="<?= $routeName; ?>">
                                                <input type='hidden' name="action" value="user_mail">
                                                <input type='hidden' name="return_url" value="https://www.drshardaayurveda.com/thanks.html">
                                                <input type='hidden' name="page_url" value="<?= $actual_link; ?>">
						<div class="contact-fname">
							<div class="error form_error form-error-fname" id="form-error-fname"></div>
							<input name="name" id="fname" type="text" placeholder="Name*" required>
						</div>
						<div class="contact-email">
							<div class="error form_error form-error-email" id="form-error-email"></div>
							<input name="email" id="email" type="email" placeholder="E-mail*">
						</div>
						<div class="contact-mobile">
							<div class="error form_error form-error-mobile" id="form-error-mobile"></div>
							<!--<input name="mobile" id="mobile" type="text" placeholder="Mobile" required>-->
                                                        <input min="0" class="mobile" name="mobile" id="mobile" type="number" placeholder="Mobile" pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}" required>
                                                        <span id="errmsgMobile"></span><br>
						</div>
						<div class="error form_error form-error-comment" id="form-error-comment"></div>
						<textarea rows="2" name="description" id="comment" placeholder="Message" rows="9"></textarea>
                                                
                                                <!--Local-->
                                                <!--<button type="submit" class="btn btn-lg btn-color g-recaptcha" data-sitekey="6LfEsOclAAAAAJsozhE_palqbg2un0C6HBMAtacd" data-callback='onSubmit' data-action='submit' value="Submit">Submit</button>-->
                                                <!--Live-->
                                                <button type="submit" class="btn btn-lg btn-color g-recaptcha" data-sitekey="6LcdxeglAAAAAI4u0cH7jiY5S3y-tEG6LqocURj2" data-callback='onSubmit' data-action='submit' value="Submit">Submit</button>
						<!--<input type="submit" class="btn btn-lg btn-color" value="Submit">-->
						<div id="msg" class="message"></div>
					</form>

                    <h3 class="widget-title heading relative bottom-line left-align">Recent Articles</h3>
                    <div class="entry-list w-thumbs">
                        <ul class="posts-list">
                            <?php foreach($posts as $post) : ?>
                            <li class="entry-li">
                                <article class="post-small clearfix">
                                    <div class="entry">
                                        <h3 class="entry-title">
                                            <a href="/blogs/<?=str_replace(array( '(', ')' ), '', str_replace(" ","-", strtolower($post['name']))); ?>"><?= $post['post_title']; ?></a>
                                        </h3>
                                        <ul class="entry-meta list-inline">
                                            <li class="entry-date"><?= date('d-M' , strtotime($post['created_at'])); ?>, <?= date('Y' , strtotime($post['created_at'])); ?></li>
                                        </ul>
                                    </div>
                                </article>
                            </li>
                            <?php endforeach; ?>
                        <ul>
                    </div>

                    <h3 class="widget-title heading relative bottom-line left-align">Releated Articles</h3>
                    <div class="entry-list w-thumbs">
                        <ul class="posts-list">
                            <?php foreach($related_posts as $post) : ?>
                            <li class="entry-li">
                                <article class="post-small clearfix">
                                    <div class="entry">
                                        <h3 class="entry-title">
                                            <a href="/blogs/<?=str_replace(array( '(', ')' ), '', str_replace(" ","-", strtolower($post['name']))); ?>"><?= $post['post_title']; ?></a>
                                        </h3>
                                        <ul class="entry-meta list-inline">
                                            <li class="entry-date"><?= date('d-M' , strtotime($post['created_at'])); ?>, <?= date('Y' , strtotime($post['created_at'])); ?></li>
                                        </ul>
                                    </div>
                                </article>
                            </li>
                            <?php endforeach; ?>
                        <ul>
                    </div>
				</div>
                
            </div>
        </div>
    </div>
</section>
<?= $records['post_schema']; ?>
<script>
    /**
    *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
    *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables    */
    /*
    var disqus_config = function () {
    this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
    this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };
    */
    (function() { // DON'T EDIT BELOW THIS LINE
    var d = document, s = d.createElement('script');
    s.src = 'https://dr-sharda-ayurveda.disqus.com/embed.js';
    s.setAttribute('data-timestamp', +new Date());
    (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
@stop