<?php
/**
 * Diabetes Page 
 * 
 * @created    21/09/2021
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2021
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')


<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-12 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1>Privacy Policy</h1>
                <p>
                    Dr. Sharda Ayurveda is committed to protect all your personal information. Privacy is our utmost priority. 
                    You can avail of our service without any hesitation. We need to provide comfort and satisfaction to our patients
                </p>
                <p>
                    Our company has some principles and we are addicted to them- 
                </p>
                <div id="hgte2" class="col-xl-12 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                    <p class="im3">
                        <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        We do not leak any personal information i.e. name, contact details, e-mail address, credit card details, transaction history, 
                        disease shared to us by the patient will never be shared with any third party for their marketing purposes without 
                        your permission and consent.
                    </p>
                    <p class="im3">
                        <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        All your personal information is confidential and will not be disclosed. We opt for higher levels of security to 
                        keep all the information confidential.
                    </p>
                    <p class="im3">
                        <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        All your information is stored and processed in our computers that are protected by physical and superior technological 
                        security devices.
                    </p>
                    <p class="im3">
                        <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        If your confidential information gets leak out by any third party who is out of our control like a courier agency 
                        and other intermediaries that are linked to us through an order processing system. We are not liable for it.
                    </p>
                    <p class="im3">
                        <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        The recovery series of our happy patients will be shared on digital media or print media or for advertisement only 
                        after taking your consent in writing. No information will be uploaded or processed without seeking your permission.
                    </p>
                    <p class="im3">
                        <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        For any complaints, query or your concern you can contact us by writing on info@drshardaayurveda.com. 
                        Or you can contact us on 98760-35500.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

@stop