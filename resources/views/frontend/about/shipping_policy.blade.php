<?php
/**
 * Shipping Policy Page 
 * 
 * @created    13/08/2022
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2022
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-12 col-md-12 col-sm-12">
                <h1>Shipping Policy</h1>
                <p>
                    We are working dedicatedly to deliver your orders correctly, with the main emphasis on good condition and timely delivery.
                </p>
                <p>
                Kindly go through the shipment policy listed down below:- 
                </p>
                <div id="hgte2" class="col-xl-12 col-md-12 col-sm-12 ">
                    <p class="im3">
                        <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        We are currently providing shipment to PAN India (including all major cities). The delivery is also provided to patients residing in major foreign countries.
                    </p>
                    <p class="im3">
                        <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        To ensure quality delivery and in the shortest time possible, we use reputed courier services. The delivery is shipped through two modes i.e., Post Office and Desk to Desk Courier & Cargo (DTDC).
                    </p>
                    <p class="im3">
                        <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        Each order shall be shipped to only a single destination address. If wish to get the order at different locations, please do confer for the full address details to the team.
                    </p>
                    <p class="im3">
                        <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        If residing in Punjab you can choose between cash on delivery and pre-payment option. But other cities and foreign customers have to make payments in advance for medicine orders. 
                    </p>
                    <p class="im3">
                        <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        We are putting efforts to provide timely service (super fast) without causing you any inconvenience.
                    </p>
                    <p class="im3">
                        <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        Immediately after consultation with doctors, the medicines are packed and shipped through the above-mentioned services to reach your destination within 10-12 days (Monday-Saturday). 
                    </p>
                    <p class="im3">
                        <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        If the items delivered are found not in perfect condition, you can refuse to take the order and we will try our best to compensate for damage to the earliest and provide you with good condition packed products.
                    </p>
                    <p class="im3">
                        <img src="{{ URL::asset('front/images/2.png') }}" alt="3.png">
                        You can also track your order through a specific tracking I’d which will be given to you by our team.  
                        <ul class="ul-refine">
                            <li>For post office orders, check the order status by clicking on this link: Track Consignment <a target="_blank" class="green-anchor" href="https://www.indiapost.gov.in/_layouts/15/dop.portal.tracking/trackconsignment.aspx" rel="nofollow">(indiapost.gov.in)</a></li>
                            <li>For DTDC orders, check the order status by clicking on this link: DTDC Tracking <a target="_blank" class="green-anchor" href="https://trackcourier.io/dtdc-tracking" rel="nofollow"> (trackcourier.io)</a></li>
                        </ul>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

@stop