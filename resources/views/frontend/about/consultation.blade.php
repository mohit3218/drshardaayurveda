<?php
/**
 * Consultation Page 
 * 
 * @created    22/06/2021
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2021
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<div class="online-consultation">

<section class="breadcrumb-sec">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                  <ul class="bread text-center" style="color:#333">
                     <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                     <li ><a href="https://www.drshardaayurveda.com/ayurvedic-online-consultation">Ayurvedic Online Consultation</a></li>
                  </ul>
            </div>
        </div>
    </div>
</section>

<section class="inner-banner-sec" style="background-image:url(https://www.drshardaayurveda.com/front/images/online-consultation-banner.webp)"alt="Banner of Online ayurvedic doctor consultation" title="Banner of Online ayurvedic doctor consultation">
   <div class="container">
   		<div class="content-inner">
		 	<h1 style="color:#000000; text-transform: capitalize; font-weight:500; font-size:38px;  line-height: 45px;">Address your health concerns. Book <br>Ayurvedic Online Consultation <br>with our ayurvedic doctors.</h1>
		 	<p style="color:#000000; margin-top:0px; line-height: 40px;">Consult our experienced doctors at your comfort</p>
         <div class="btn-ayurvedic">
            <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment-common">BOOK AN APPOINTMENT</button>
         </div>
	  	</div>
	  	<div class="bottom-video-banner">
		 	<ul>
				<li style="border:none; padding-right:0px;"><img src="{{ URL::asset('front/images/depth-disease-examination.webp') }}" alt="In-depth disease examination"> In-depth disease examination </li>
				<li><img src="{{ URL::asset('front/images/personalized-treatment.webp') }}" alt="personalized"> Personalized treatment </li>
				<li><img src="{{ URL::asset('front/images/safe-effective-treatment.webp') }}" alt="Safe and effective treatment"> Safe and effective treatment</li>
		 	</ul>
	  	</div>
	</div>
</section>

<section class="disease-treat">
   <div class="container">
      <div class="head-bx">
         <h2>Diseases We Specialized In Treating</h2>
      </div>
      <div class="disease-inner">
         <div class="row">
            <div class="col">
               <div class="inner">
                  <div class="disbx">
                     <div class="icon">
                        <img class="treatment-image" src="{{ URL::asset('front/images/Joint-Pain-icon.webp') }}"alt="Joint Pain icon" title="Joint pain icon">
                     </div>
                     <h4>Joint Problems</h4>
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="inner">
                  <div class="disbx">
                     <div class="icon">
                        <img class="treatment-image" src="{{ URL::asset('front/images/respiratory-icon.webp') }}"alt="Respiratory icon" title="Respiratory icon">
                     </div>
                     <h4>Respiratory Diseases</h4>
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="inner">
                  <div class="disbx">
                     <div class="icon">
                        <img class="treatment-image" src="{{ URL::asset('front/images/digestive-disorders-icon.webp') }}"alt="Digestive Disorders icon"title="Digestive Disorders icon">
                     </div>
                     <h4>Digestive Disorders</h4>
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="inner">
                  <div class="disbx">
                     <div class="icon">
                        <img class="treatment-image" src="{{ URL::asset('front/images/skin-diseases-icon.webp') }}" alt="Skin Diseases icon"title="Skin Diseases icon">
                     </div>
                     <h4>Skin Diseases</h4>
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="inner">
                  <div class="disbx">
                     <div class="icon">
                        <img class="treatment-image" src="{{ URL::asset('front/images/endocrine-disorders-icon.webp') }}"alt="Endocrine Disorder icon" title="Endocrine Disorder icon">
                     </div>
                     <h4>Endocrine Disorders</h4>
                  </div>
               </div>
            </div>
         </div>

         <div class="row">
         <div class="col">
               <div class="inner">
                  <div class="disbx">
                     <div class="icon">
                        <img class="treatment-image" src="{{ URL::asset('front/images/liver-icon.webp') }}"alt="Liver icon" title="Liver icon">
                     </div>
                     <h4>Liver Diseases</h4>
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="inner">
                  <div class="disbx">
                     <div class="icon">
                       <img class="treatment-image" src="{{ URL::asset('front/images/sexual-disorders-icon.webp') }}"alt="Sexual Disorders icon" title="Sexual Disorders icon">
                     </div>
                     <h4>Sexual Disorders</h4>
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="inner">
                  <div class="disbx">
                     <div class="icon">
                        <img class="treatment-image" src="{{ URL::asset('front/images/gynae-icon.webp') }}" alt="Gynae icon"title="Gynae icon">
                     </div>
                     <h4>Gynae</h4>
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="inner">
                  <div class="disbx">
                     <div class="icon">
                        <img class="treatment-image" src="{{ URL::asset('front/images/kidney-icon.webp') }}"alt="Kidney icon" title="Kidney icon">
                     </div>
                     <h4>Kidney Disorders</h4>
                  </div>
               </div>
            </div>
            <div class="col">
               <div class="inner">
                  <div class="disbx">
                     <div class="icon">
                        <img class="treatment-image" src="{{ URL::asset('front/images/urinary-icon.webp') }}"alt="Urinary icon"title="Urinary icon">
                     </div>
                     <h4>Urinary Diseases</h4>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>


<section class="why-drshardha">
   <div class="banner"></div>
   <div class="container">
      <div class="btm-drshardha-why">
         <div class="head-bx">
            <h2>What makes Dr. Sharda's Ayurvedic Treatment Different From Others?</h2>
            <p>The Ayurvedic doctor's team holds uncountable experience and are specialized in treating chronic diseases naturally by deep analyzing the root cause. Their aim is to assure improved health and lifelong results.</p>
         </div>
         <div class="inner-btm text-center">
            <div class="row">
               <div class="col-md-3 col-sm-3 col-xs-6">
                  <div class="inner">
                     <h2>8L+</h2>
                     <p>successfully online patients consulted</p>
                  </div>
               </div>
               <div class="col-md-3 col-sm-3 col-xs-6">
                  <div class="inner">
                     <h2>12+ </h2>
                     <p>Years of experience with assured results</p>
                  </div>
               </div>
               <div class="col-md-3 col-sm-3 col-xs-6">
                  <div class="inner">
                     <h2>97%</h2>
                     <p>positive response from patients</p>
                  </div>
               </div>
               <div class="col-md-3 col-sm-3 col-xs-6">
                  <div class="inner">
                     <h2>10+</h2>
                     <p>experienced team of Ayurvedic doctors</p>
                  </div>
               </div>
            </div>
            <div class="btm-line">
               <p>WE ASSURE TO PROVIDE SAFE AND FINEST TREATMENT</p>
                <div class="btn-ayurvedic">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
         </div>
      </div>
   </div>
</section>

<section class="treat-yrself-sec gradient-custom">
   <div class="container">
      <div class="head-bx">
         <h2>Treat yourself the natural way!</h2>
         <p>We at Dr. Sharda Ayurveda believe in treating the disease by inculcating the knowledge of ancient science combined with modern treatment methods.</p>
      </div>
         <div class="inner-treatyrself">
            <div class="col-md-12 col-sm-12">
               <div class="treatyrself-video-group">
                 <div class="youtube-player" data-id="PGKM-htmG9A" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:380px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/backpain.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 100%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
               </div>
            </div>
         </div>
   </div>
</section>

<section class="why-drshardha">
    <!--<div class="banner"></div>-->
    <br><br><br><br><br><br>
   <div class="container">
      <div class="btm-drshardha-why">
         <div class="head-bx text-center">
            <h2>How Ayurvedic Online Consultation Works</h2>
         </div>
         <div class="inner-btm">
            <div class="row">
               <div class="col-md-3 col-sm-3 col-xs-6">
                  <div class="inner-image">
                     <img class="treatment-image" width="79px" height="76px"src="{{ URL::asset('front/images/con-book-appointment.webp') }}"alt="Schedule online ayurvedic doctor consultation" title="Schedule online ayurvedic doctor consultation">
                     <h2>Book Appointment</h2>
                     <p style="font-weight:450;">Consultation Physical mode Or Audio mode is your Choice!<br></p><p>You may fill out the form on our website or make a call directly at our clinic.</p>
                  </div>
               </div>
               <div class="col-md-3 col-sm-3 col-xs-6">
                  <div class="inner-image">
                     <img class="treatment-image" width="79px" height="76px"src="{{ URL::asset('front/images/con-call.webp') }}"alt="Get a call back" title="Get a call back">
                     <h2>Contact Back</h2>
                     <p>The team will contact and fix a consultation with ayurvedic doctors according to your convenience. It is your choice of physical visit or online consultation</p>
                  </div>
               </div>
               <div class="col-md-3 col-sm-3 col-xs-6">
                  <div class="inner-image">
                     <img class="treatment-image" width="79px" height="76px"src="{{ URL::asset('front/images/con-12.webp') }}"alt="Icon For Doctors Consultation" title="Icon For Doctors Consultation">
                     <h2>Doctor Consultation</h2>
                     <p>As per the arranged schedule, you may speak to our doctor online and share your health concerns. After listening to you and getting to know your past & present history medicines will be prescribed accordingly.</p>
                  </div>
               </div>
               <div class="col-md-3 col-sm-3 col-xs-6">
                  <div class="inner-image">
                     <img class="treatment-image" width="79px" height="76px"src="{{ URL::asset('front/images/con-medicine-delivery.webp') }}" alt="Medicine Delivery" title="Medicine Delivery">
                     <h2>Medicine Delivery</h2>
                     <p>For patient convenience, our delivery partners are engaged in providing express doorstep medicines shipment after Ayurvedic doctor online consultation.</p>
                  </div>
               </div>
            </div>
            <div class="btm-line">
<!--                <p>*Non-OHIP cardholders can also receive care at no charge as long as they are in Ontario.</p> -->
                <div class="btn-ayurvedic">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
         </div>
      </div>
   </div>
</section>

<section class="offer-sec">
   <div class="container">
      <div class="head-bx">
         <h2>What do we aim to offer?</h2>
         </div>
      
      <div class="inner-offer">
         <div class="row">
            <div class="col-md-3 col-sm-6">
               <div class="inner">
                  <div class="content">
                     <h4>Physical Consultation</h4>
                     <p>You can directly visit the hospital and get answers to all your health concerns.</p>
                  </div>
                  <img src="{{ URL::asset('front/images/physical-consultation .webp') }}" alt="Physical Consultation" title="Physical Consultation">
               </div>
            </div>
            <div class="col-md-3 col-sm-6">
               <div class="inner">
                  <div class="content">
                     <h4>Video Consultation</h4>
                     <p>Book an ayurvedic doctor consultation and share issues regarding health with our patient advisor.</p>
                  </div>
                  <img src="{{ URL::asset('front/images/video-consultation.webp') }}"alt="Video Consultation"title="Video Consultation">
               </div>
            </div>
            <div class="col-md-3 col-sm-6">
               <div class="inner">
                  <div class="content">
                     <h4>Telephonic Consultation</h4>
                     <p>Book an appointment online and patient support team will arrange your call with Ayurvedic doctors.</p>
                  </div>
                  <img src="{{ URL::asset('front/images/telephone-consultation.webp') }}" alt="Telephone Consultation" title="Telephone Consultation">
               </div>
            </div>
            <div class="col-md-3 col-sm-6">
               <div class="inner">
                  <div class="content">
                     <h4>Doorstep  Delivery</h4>
                     <p>For providing extra convenience, the medicines are well-packed and safely delivered to you.</p>
                  </div>
                  <img src="{{ URL::asset('front/images/doorstep-delivery.webp') }}" alt="Doorstep Delivery"title="Doorstep Delivery">
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

<section class="map raw01 mt-10">
<!--    <div class="how-bg"> -->
   <div class="container">
      <div class="head-bx">
         <h2>Providing Ayurvedic Treatment & Consultation Globally</h2>
      </div>
      <div class="disease-inner">
         <div class="row">
            <div class="col-md-2 col-sm-3 col-xs-6">
                  <div class="disbx text-center">
                     <div class="icon">
                        <img class="treatment-image" src="{{ URL::asset('front/images/canada.webp') }}"alt="Canada Flag Icon" title="Canada Flag Icon">
                     </div>
                     <h4>Canada</h4>
                  </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6">
                  <div class="disbx text-center">
                     <div class="icon">
                        <img class="treatment-image" src="{{ URL::asset('front/images/usa.webp') }}"alt="USA Flag Icon" title="USA Flag Icon">
                     </div>
                     <h4>United States</h4>
                  </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6">
                  <div class="disbx text-center">
                     <div class="icon">
                        <img class="treatment-image" src="{{ URL::asset('front/images/uk.webp') }}"alt="Uk Flag Icon" title="Uk Flag Icon">
                     </div>
                     <h4>United Kingdom</h4>
                  </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6">
                  <div class="disbx text-center">
                     <div class="icon">
                        <img class="treatment-image" src="{{ URL::asset('front/images/australia.webp') }}"alt="Australia Flag Icon" title="Australia Flag Icon">
                     </div>
                     <h4>Australia</h4>
                  </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6">
                  <div class="disbx text-center">
                     <div class="icon">
                        <img class="treatment-image" src="{{ URL::asset('front/images/uae.webp') }}"alt="UAE Flag Icon" title="UAE Flag Icon">
                     </div>
                     <h4>UAE</h4>
                  </div>
            </div>
            <div class="col-md-2 col-sm-3 col-xs-6">
                  <div class="disbx text-center">
                     <div class="icon">
                        <img class="treatment-image" src="{{ URL::asset('front/images/italy.webp') }}" alt="Italy Flag Icon" title="Italy Flag Icon">
                     </div>
                     <h4>Italy</h4>
                  </div>
            </div>
      </div>
   </div>
</section>

<section class="doctors-sec">
    <div class="container-fluid">
    <div class="head-bx">
                <h2 class="heading1">Meet Our Doctors</h2>
                <p>Dr. Sharda Ayurveda is focusing on enhancing the quality of the treatment that aim to provide better and improved health.</p>
        </div>
        <div class="row cust-iba">
            <div class="owl-carousel owl-theme">
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                    <img class="card-img-top" src="{{ URL::asset('front/images/Dr-Raman.webp') }}" alt="Dr. Ramanjeet Kaur"title="Dr. Ramanjeet Kaur">
                        <div class="card-body">
                        <h4 class="card-title">Dr. Ramanjeet Kaur (BAMS, MD)</h4> <p class="card-text">Dr. Ramanjeet Kaur is an experienced and qualified Ayurvedic doctor having 7+ years of experience and did her master's in AM (Alternative Medicine). She has put forward her education in a way that will effectively heal patients and provide them with lifelong results, especially for chronic disease complainants. She has been doing her services as a senior Ayurvedic doctor at Dr. Sharda Ayurveda for the last 5 years.</p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                    <img class="card-img-top" src="{{ URL::asset('front/images/Dr-Jeevan-Jyoti.webp') }}"alt="Dr. Jeevan Jyoti" title="Dr. Jeevan Jyoti">
                        <div class="card-body">
                        <h4 class="card-title">Dr. Jeevan Jyoti (BAMS, MD)</h4> <p class="card-text">Dr. Jeevan Jyoti holds an experience of more than 20+ years of experience and is specialized in treating a range of chronic diseases like skin, gynecology, respiratory, neurological, and many others. She has her post-graduate degree in clinical cosmetology. She is actively performing her duty as a senior Ayurvedic doctor with Dr. Sharda Ayurveda for the last 2 years.</p> 
                        </div>
                    </div>
                </div>


                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                    <img class="card-img-top" src="{{ URL::asset('front/images/Dr-Vandana-Sharma.webp') }}" alt="Dr. Vandana Sharma" title="Dr. Vandana Sharma">
                        <div class="card-body">
                        <h4 class="card-title">Dr. Vandana Sharma (BAMS)</h4>
										<p class="card-text">Dr. Vandana Sharma has successfully treated countless patients, who mainly complain about joint pain. She holds an experience of 4+ years and is serving as an Ayurvedic expert at Dr. Sharda Ayurveda. Her knowledge has effectively treated patients who have lost all hopes in getting recovered and were suggested for surgery as the last choice to get relief.</p>
                        </div>
                    </div>
                </div>

                  <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                    <img class="card-img-top" src="{{ URL::asset('front/images/DrVinodChauhan.webp') }}" alt="Dr. Vinod Chauhan" title="Dr. Vinod Chauhan">
                        <div class="card-body">
									<div class="card-body">
										<h4 class="card-title">Dr. Vinod Chauhan (BAMS, M.Sc.)</h4>
										<p class="card-text">Dr. Vinod Chauhan holds an experience of 20+ years in treating numerous chronic conditions patients, specifically sexual, skin, and digestive-related issues, all with his knowledge and commitment towards his services. Having a master's degree in clinical research and regulatory affairs, he is being associated with Dr. Sharda Ayurveda for the last 2 years.</p>
									</div>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                    <img class="card-img-top" src="{{ URL::asset('front/images/dr-preeti.webp') }}" alt="Dr. Preeti Arora" title="Dr. Preeti Arora">
                        <div class="card-body">
                        <h4 class="card-title">Dr. Preeti Arora (BAMS, M.Sc.)</h4>
										<p class="card-text">Dr. Preeti Arora has an experience of 4+ years and has been associated with Dr. Sharda Ayurveda since 2019. She holds a master’s degree in hospital management and is working selflessly for providing the best of the best care to the patients. Her guidance has successfully treated an enormous number of chronic disease patients mainly skin, gynecology, joint, digestive, and many others.</p>
                        </div>
                    </div>
                </div>

                <!-- <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                    <img class="card-img-top" src="{{ URL::asset('front/images/dr-rashmeet-kaur.webp') }}" alt="Dr. Rashneet Kaur" title="Dr. Rashneet Kaur">
                        <div class="card-body">
                        <h4 class="card-title">Dr. Rashneet Kaur (BAMS)</h4>
										<p class="card-text">Dr. Rashneet Kaur is a young professional with determination towards her work. A graduate of Ayurveda studies, she is serving as an Ayurvedic practitioner at Dr. Sharda Ayurveda for the last 4 months. She is specialized in treating diseases related to skin, respiratory, gynecology, and joints.</p>
                        </div>
                    </div>
                </div> -->

<!--                 <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                    <img class="card-img-top" src="{{ URL::asset('front/images/dr-nikita.webp') }}" alt="Dr. Nikita" title="Dr. Nikita">
                        <div class="card-body">
                        <h4 class="card-title">Dr. Nikita (BAMS)</h4>
                                        <p class="card-text">Dr. Nikita is a graduate of Ayurvedic sciences. She is an expert at Dr. Sharda Ayurveda and is genuinely concerned for her patients. With a passion to heal people, she willingly is in the service of mankind. Along with being a great doctor, she is able to communicate effectively with her patients which helps them to heal and recover.</p>
                        </div>
                    </div>
                </div> -->

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                    <img class="card-img-top" src="{{ URL::asset('front/images/Dr-Sunil-Chabra.webp') }}" alt="Dr. Sunil Chabbra" title="Dr. Sunil Chabbra">
                        <div class="card-body">
                        <h4 class="card-title">Dr. Sunil Chabbra (BAMS)</h4>
										<p class="card-text">Dr. Sunil Chabbra has a broad experience of 15+ years and is working as a senior Ayurvedic doctor at Dr. Sharda Ayurveda for the last 11+ years. He is expertise in treating chronic diseases with holding the highest success rate of healing sexual, skin, respiratory, endocrine, neurological disorders, and many others.</p>
                        </div>
                    </div>
                </div>

<!--               <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                    <img class="card-img-top" src="{{ URL::asset('front/images/Dr-Sarabhjot-Singh.webp') }}" alt="Dr. Sarabjot Singh" title="Dr. Sarabjot Singh">
                        <div class="card-body">
                        <h4 class="card-title">Dr. Sarabjot Singh (BAMS)</h4>
										<p class="card-text" text size=>Dr. Sarabjot Singh is a qualified Ayurveda professional with holding an experience of 8+ years and has currently been associated with Dr. Sharda Ayurveda for a long time, i.e., 5+ years. His skills and knowledge have successfully treated patients that were mainly complainants of joint pain, skin, liver, circulatory, and respiratory.</p>
                        </div>
                    </div>
               </div> -->

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                    <img class="card-img-top" src="{{ URL::asset('front/images/dr-dapinder-kaur.webp') }}" alt="Dr. Dapinder Kaur" title="Dr. Dapinder Kaur">
                        <div class="card-body">
                        <h4 class="card-title">Dr. Dapinder Kaur (BAMS)</h4>
                                        <p class="card-text">Dr. Dapinder Kaur has a graduate degree in Ayurvedic sciences. She believes in serving mankind by ending their suffering. With her medical and interpersonal skills, she not only provides Ayurvedic treatment to her patients but also educates them regarding the disorder. She is a great expert at Dr. Sharda Ayurveda and aims to make a difference with her intelligence.</p>
                                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="gradient-custom">
  <div class="container my-5 py-5">
    <div class="row d-flex justify-content-center">
      <div class="col-md-12">
         <h2 style= text align="center">Patient Testimonials on Dr. Sharda Ayurveda</h2>
         <p style= text align="center"> Get to see how the holistic and authentic Ayurvedic treatment from Dr. Sharda Ayurveda is healing patients worldwide.</p>
        <div class="text-center mb-4 pb-2">
          <i class="fa fa-quote-left fa-3x text-white"></i>
        </div>

        <div class="card">
          <div class="card-body px-4 py-5">
            <!-- Carousel wrapper -->
            <div id="carouselDarkVariant" class="carousel slide carousel-dark" data-ride="carousel">
              <!-- Indicators -->
              <div class="carousel-indicators mb-0">
               <button data-target="#carouselDarkVariant" data-slide-to="0" class="active" aria-current="true" aria-label="Slide1"></button>
                <button data-target="#carouselDarkVariant" data-slide-to="1" aria-label="Slide 1"></button>
                <button data-target="#carouselDarkVariant" data-slide-to="2" aria-label="Slide 1"></button>
              </div>

              <!-- Inner -->
              <div class="carousel-inner pb-5">
                <!-- Single item -->
                <div class="carousel-item active">
                  <div class="row d-flex justify-content-center">
                    <div class="col-lg-10 col-xl-8">
                      <div class="row">
                        <div class="col-lg-4 d-flex justify-content-center">
                          <img
                            src="{{ URL::asset('front/images/Gyaninder-Kumar-Sharma.webp') }}"
                            class="rounded-circle shadow-1 mb-4 mb-lg-0"
                            alt="Patient Gyaninder Kumar's Image" title="Patient Gyaninder Kumar's Image"
                            width="150"
                            height="150"
                          />
                        </div>
                        <div class="col-9 col-md-9 col-lg-7 col-xl-8 text-center text-lg-start mx-auto mx-lg-0">
                          <h4 class="mb-4">Gyaninder Kumar Sharma</h4>
                          <p class="mb-0 pb-3">
                            Myself Gyaninder Kumar Sharma was diagnosed with Rheumatoid arthritis 6 years back. I was experiencing persistent joint pain that was getting worse with passing time. Even after consulting various renowned specialists still, none of the treatments provided positive recovery. Then, as per a recommendation from a friend consulted Dr. Sharda Ayurveda. Their treatment provided effective recovery within just 6 months. I am very much thankful to them. 
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <!-- Single item -->
                <div class="carousel-item">
                  <div class="row d-flex justify-content-center">
                    <div class="col-lg-10 col-xl-8">
                      <div class="row">
                        <div class="col-lg-4 d-flex justify-content-center">
                          <img
                            src="{{ URL::asset('front/images/Ravinder-Singh.webp') }}"
                            class="rounded-circle shadow-1 mb-4 mb-lg-0"
                            alt="Patient Ravinder Singh's Image" title="Patient Ravinder Singh's Image"
                            width="150"
                            height="150"
                          />
                        </div>
                        <div class="col-9 col-md-9 col-lg-7 col-xl-8 text-center text-lg-start mx-auto mx-lg-0">
                          <h4 class="mb-4">Ravinder Singh</h4>
                          <p class="mb-0 pb-3">
                             23 ਸਾਲ ਪਹਿਲੇ ਇੱਕ ਵੱਡੇ ਹਾਦਸੇ ਕਾਰਨ ਮੇਰੀ L5 ਰੀੜ੍ਹ ਦੀ ਹੱਡੀ ਟੁੱਟ ਗਈ ਸੀ ਓਹਦੇ ਕਾਰਣ ਮੈਨੂੰ Spine ਤੇ Back Pain ਦੀ ਦਿਕਤ ਹੋ ਗਈ ਸੀ | ਇੱਥੋਂ ਤੱਕ ਕਿ ਮੇਰਾ ਬੈਠਣਾ ਅਤੇ ਚਲਣਾ ਵੀ ਮੁਸ਼ਕਲ ਹੋ ਗਿਆ ਸੀ | ਵੱਖ-ਵੱਖ Doctor ਨਾਲ ਸਲਾਹ ਕਰਕੇ ਵੀ ਮੇਰੀ Problem ਦਾ ਹਲ ਨਹੀਂ ਹੋਇਆ | ਫਿਰ ਮੈਨੂੰ Dr. Sharda Ayurveda ਬਾਰੇ ਅਖਬਾਰ ਤੋਂ ਪਤਾ ਲੱਗਾ ਤੇ ਹੁਣ 4 ਮਹੀਨੇ ਦਵਾਈ ਨਾਲ ਮੇਰੀ 80% Pain ਖਤਮ ਹੋ ਗਈ ਹੈ | ਮੈਂ ਓਹਨਾ ਦਾ ਬਹੁਤ ਧੰਨਵਾਦੀ ਹਾਂ |
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="carousel-item">
                  <div class="row d-flex justify-content-center">
                    <div class="col-lg-10 col-xl-8">
                      <div class="row">
                        <div class="col-lg-4 d-flex justify-content-center">
                          <img
                            src="{{ URL::asset('front/images/vijender-singh.webp') }}"
                            class="rounded-circle shadow-1 mb-4 mb-lg-0"
                            alt="Patient Vijender Singh's Image" title="Patient Vijender Singh's Image"
                            width="150"
                            height="150"
                          />
                        </div>
                        <div class="col-9 col-md-9 col-lg-7 col-xl-8 text-center text-lg-start mx-auto mx-lg-0">
                          <h4 class="mb-4">Vijender Singh</h4>
                          <p class="mb-0 pb-3">
                             पिछले कुछ सालों से सांस लेने में तकलीफ और सीने में जकड़न की समस्या से परेशान था | डॉक्टरों से सलाह लेने के बाद पता चला कि मैं एलर्जिक अस्थमा से पीड़ित हूं। मेरे एक मित्र ने डॉ. शारदा आयुर्वेद के पास जाने का सुझाव दिया | उनके आयुर्वेदिक डॉक्टर की सलाह और Medicines से सिर्फ 6 महीनो में मुझे आराम आ गया | 
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="carousel-item">
                  <div class="row d-flex justify-content-center">
                    <div class="col-lg-10 col-xl-8">
                      <div class="row">
                        <div class="col-lg-4 d-flex justify-content-center">
                          <img
                            src="{{ URL::asset('front/images/Lalit-Kumar.webp') }}"
                            class="rounded-circle shadow-1 mb-4 mb-lg-0"
                            alt="Patient lalit Kumar's Image" title="Patient lalit Kumar's Image"
                            width="150"
                            height="150"
                          />
                        </div>
                        <div class="col-9 col-md-9 col-lg-7 col-xl-8 text-center text-lg-start mx-auto mx-lg-0">
                          <h4 class="mb-4">Lalit Kumar</h4>
                          <p class="mb-0 pb-3">
                            I Lalit Kumar Mehra from Delhi, was experiencing extreme and persistent joint pain for the last few years. Along with the pain, there was swelling and stiffness of the joints. Even after taking proper medications prescribed by the doctors still, there was not even a bit of health improvement. Then, after quite a search, I got to know about Dr. Sharda's Ayurveda joint pain treatment. Their experts advised Ayurvedic treatment effectively provided full recovery within just 3-4 months.
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Inner -->

              <!-- Controls -->
              <!-- <button class="carousel-control-prev" type="button" data-target="#carouselDarkVariant" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
              </button>
              <button class="carousel-control-next" type="button" data-target="#carouselDarkVariant" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
              </button> -->
            </div>
            <!-- Carousel wrapper -->
          </div>
        </div>

        <div class="text-center mt-4 pt-2">
          <i class="fa fa-quote-right fa-3x text-white"></i>
        </div>
      </div>
    </div>
  </div>
</section>


<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQs of Ayurvedic Online Consultation</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                What do you charge for an online ayurvedic doctor consultation?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                           The charges for the consultation can vary with the location. 
                           <li>For Indian residents, the charges are 250/-.</li>
                           <li>For foreign residing patients, the charges are 1000/-.</li>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                How can one make the payment? Is the payment secured? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            You can make the payment through:
                            <li>Cash</li> <li>Debit/Credit Card</li><li>UPI</li> <li>Online bank transfer</li>Yes, the payment is safe.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                After consultation, what is the expected time for the medicines to be delivered?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            The delivery time differentiates as per the location. 
                            <ul>
                                <li>For PAN India, the estimated time of delivery is 3-7 days.</li>
                                <li>For overseas residents, the medicines will be delivered within 2 weeks.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Can I share my previous reports while consulting with the Ayurvedic doctor?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                            Yes,  you can share your reports and all your medical history of your health concern.  Accordingly, the medications along with specialized diet charts and lifestyle changes are recommended to the patients.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion2">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    How does the online ayurvedic doctor consultation works?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion2">
                            <div class="card-body">
                               <b>For physical consultation</b>
                                 <li>Visit the hospital</li>
                                 <li>Pay the consultation charges</li>
                                 <li>Consult the experts</li>
                                 <li>Take medicines.</li>
                               <b>For online consultation</b>
                                 <li>Contact patient advisor</li>
                                 <li>Pay the consultation charges</li>
                                 <li>Consult the experts (for Indian patients, the consultation is done between 1-2 days but for foreign patients, it takes around 1 week)</li> 
                                 <li>Then the medicines will be accordingly delivered.</li>
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                After consultation, for what time the patient can further ask their query from the doctor? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion2">
                        <div class="card-body">
                            After consultation, the patients can ask the further query from the doctors within the time frame of 7 days. But the patient's advisors are always active to resolve your issues. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Can allopathic medicines be taken along with Ayurvedic medicines?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion2">
                        <div class="card-body">
                            Yes, you can take allopathic medicine along with Ayurvedic medicine.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Will the Ayurvedic doctor guide well in the online consultation and help treat even the serious condition?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion2">
                        <div class="card-body">
                            Yes, you will be answered regarding all the queries. The experienced and qualified team of professionals are specialized in various fields to provide you with effective treatment and guidelines associated with diet and lifestyle changes for better recovery.  
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                What I can expect from an <a class="green-anchor"href="https://www.drshardaayurveda.com/ayurvedic-online-consultation">online Ayurvedic doctor consultation</a>?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion2">
                        <div class="card-body">
                            The Ayurvedic practitioner will offer:
                            <li>Patient history analysis</li>
                            <li>Report study (if applicable)</li>
                            <li>Prescribe medications</li>
                            <li>Give details about diet and lifestyle changes.</li>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Do I need to have a vegetarian diet, if taking Ayurvedic medicines, for better results?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion2">
                        <div class="card-body">
                            No, it is not necessarily important to be vegetarian along with taking herbal medicines. But in some cases, the patients are suggested to remain strict with the plant-based diet to get early and long-term results. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('../partials/frontend/form')
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            responsiveClass: !0,
            navigation : true,
            autoplay: true,
            loop: true,
            smartSpeed: 2500,
            singleItem : true,
            navigationText: ["<i class='fa fa-angle-left fa-2'></i>", "<i class='fa fa-angle-right fa-2'></i>"],
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                480: {
                    items: 1,
                    nav: !1
                },
                768: {
                    items: 2,
                    nav: !1
                },
                1000: {
                    items: 3,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1024: {
                    items: 4,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1366: {
                    items: 4,
                    nav: !1
                }
            }
        })
    });

</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What do you charge for an online ayurvedic doctor consultation?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The charges of the consultation vary with the location. For Indian residents, the charges are 250/- per session For foreign residing patients, the charges are 1000/- per session."
    }
  },{
    "@type": "Question",
    "name": "How can one make the payment? Is the payment secured?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "There are three modes of payment, i.e., Cash, UPI, and Bank transfer. Yes, the payment is safe."
    }
  },{
    "@type": "Question",
    "name": "After consultation, what is the expected time for the medicines to be delivered?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The delivery time differentiates as per the location. For PAN India, the estimated time of delivery is 3-7 days. For overseas residents, the medicines will be delivered within 2 weeks."
    }
  },{
    "@type": "Question",
    "name": "Can I share my previous reports while consulting with the Ayurvedic doctor?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, you can share your reports and provide the doctor with full details regarding your health concerns. Accordingly, the medications along with specialized diet charts and lifestyle changes are recommended to the patients."
    }
  },{
    "@type": "Question",
    "name": "How does the online ayurvedic doctor consultation system work?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The two ways include:For physical consultation Visit the hospital - Pay the consultation charges - Consult the experts - Take medicines.For online consultation Contact patient advisor - Pay the consultation charges - Consult the experts (for Indian patients, the consultation is done between 1-2 days but for foreign patients, it takes around 1 week) - Then the medicines will be accordingly delivered."
    }
  },{
    "@type": "Question",
    "name": "After consultation, for what time the patient can further ask their query from the doctor?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "After consultation, the patients can ask the further query from the doctors within the time frame of 7 days. But the patient's advisors are always active to resolve your issues."
    }
  },{
    "@type": "Question",
    "name": "Can allopathic medicines be taken along with Ayurvedic medicines?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, you can take the allopathic medicine along with Ayurvedic medicine, but priorly consult the experts and get the proper guidance."
    }
  },{
    "@type": "Question",
    "name": "Will the Ayurvedic doctor guide well in the online consultation and help treat even the serious condition?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, you will be answered regarding all the queries. The experienced and qualified team of professionals are specialized in various fields to provide you with effective treatment and guidelines associated with diet and lifestyle changes for better recovery."
    }
  },{
    "@type": "Question",
    "name": "What I can expect from an online Ayurvedic doctor consultation?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The Ayurvedic practitioner will offer: Full body examination Report study (if applicable) Prescribe medications Give details about diet and lifestyle changes."
    }
  },{
    "@type": "Question",
    "name": "Do I need to have a vegetarian diet, if taking Ayurvedic medicines, for better results?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "No, it is not necessarily important to be vegetarian along with taking herbal medicines. But in some cases, the patients are suggested to remain strict with the plant-based diet to get early and long-term results."
    }
  }]
}
</script>

@stop