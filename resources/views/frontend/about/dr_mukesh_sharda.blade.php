<?php
/**
 * Dr Mukesh Sharda Page 
 * 
 * @created    17/01/2022
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2022
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')

<section class="section-wrap dsa-why-choose blog-standard pb-50" style="padding: 35px 0px;">
    <ul class="breadcrumb-item text-center" style="margin-left: 7%;">
        <li class="breadcrumb-li active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
        <li class="breadcrumb-li"><a href="/dr-mukesh-sharda">Dr Mukesh Sharda</a></li>
    </ul>
    <div class="container">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc pb-50">
                    <h2 class="heading1 text-center"><b>Dr. Mukesh Sharda</b></h2>
                    <img src="{{ URL::asset('front/images/Ayurveidc-doctor-dr-mukesh-sharda.webp') }}" class="why-choose-us img-fluid" alt="Dr Mukesh Sharda">
                </div>
            </div>
        <div class="col-xl-6 col-md-12 col-sm-12"data-aos="fade-up" data-aos-duration="4000">
            <h1 class="heading1 text-left">Best Ayurvedic Doctor in India</h1>
                <p>
                    An Ayurvedic doctor is the one who works on treating disease by natural Ayurvedic herbs gifted to us by nature and beneficially utilizing them for a safe, secure and healthy life. Dr. Mukesh Sharda founder of Dr. Sharda Ayurveda has worked tirelessly to create and implement the standards for the practice in Ayurveda. Being from the beauty of hills, The Himachal Pradesh; she was always attracted and bound to the Beauty of nature. Her keen interest in plants and their research motivated her to become an Ayurvedic doctor.<br> She has been practicing Ayurveda for more than 15+ years. Her specialization stands in understanding disease from an Ayurvedic perspective, sufficient to co-relate it with harmful effects of western medical pathology. It makes her happy to analyze the amazing health effects of the Ayurvedic medicines for treating the affected people. Along with the medicines, she encourages the use of natural herbs and dietary changes providing a holistic approach. As the recovery of patients was successfully growing, this motivated her to establish the chain of clinics. Today Dr. Sharda Ayurveda run by Dr. Mukesh Sharda has spread its branches all over Punjab and is healing mankind across the globe with Ayurvedic treatment.
                </p>
            </div>
        </div>
        <div class="row cut-row">
        <div id="gallery" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="row">
                        <div class="col">
                            <img class="img-fluid" src="{{ URL::asset('front/images/hindustantimes.webp') }}" alt="Hindustan Times logo"/>
                            </a>
                        </div>
<!--                         <div class="col">
                            <img class="img-fluid" src="{{ URL::asset('front/images/thehansindia.webp') }}" alt="The Hans India logo"/>
                            </a>
                        </div> -->
                        <div class="col">
                            <img class="img-fluid" src="{{ URL::asset('front/images/the-week-news.webp') }}" alt="the week logo"/>
                            </a>
                        </div>

                        <div class="col">
                           <img class="img-fluid" src="{{ URL::asset('front/images/outlook-article.webp') }}" alt="Outlook logo"/>
                            </a>
                        </div>

                        <div class="col">
                           <img class="img-fluid" src="{{ URL::asset('front/images/times-of-india-article.webp') }}" alt="times of india logo"/>
                            </a>
                        </div>

                    </div>
                </div>

                <div class="carousel-item">
                    <div class="row">
                        <div class="col">
                           <img class="img-fluid" src="{{ URL::asset('front/images/mid-day-article.webp') }}" alt="Mid Day Logo"/>
                            </a>
                        </div>
<!--                         <div class="col">
                            <img class="img-fluid" src="{{ URL::asset('front/images/telengana-news.webp') }}" alt="telengana news logo"/>
                            </a>
                        </div>
 -->
                        <div class="col">
                            <img class="img-fluid" src="{{ URL::asset('front/images/the-eastyern-herald-article.webp') }}" alt="easternherald logo"/>
                            </a>
                        </div>

                        <div class="col">
                           <img class="img-fluid" src="{{ URL::asset('front/images/the-pioneer-article.webp') }}" alt="The news Pioneer logo"/>
                            </a>
                        </div>

                        <div class="col">
                            <img class="img-fluid" src="{{ URL::asset('front/images/health-shot-article.webp') }}" alt="health shot article logo"/>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <a class="carousel-control-prev" href="#gallery" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>

            <a class="carousel-control-next" href="#gallery" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec our-expert" id="types" style="background-color: #f9f9f9;"> 
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Awards & <b>Recognition</b></h2>
            </div>
        </div>
         <div class="row cust-iba">
            <div class="owl-carousel owl-theme" >
                <div class="item col-md-12 text-center">
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/award6.webp') }}" class="card-img-top lazyload" alt="awards">
                        <div class="card-body">
                            <h6 class="text-left">Best Ayurvedic Doctor in Punjab- 2019</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12">
                                    <p class="text-left">Dr. Mukesh Sharda has also been awarded by -FYI Media Group Surrey, BC, Canada</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item col-md-12 text-center">
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/award7.webp') }}" class="card-img-top lazyload" alt="awards">
                        <div class="card-body">
                            <h6 class="text-left">Guest of Honour</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12">
                                    <p class="text-left">Guest of Honour Khalsa Library Society Surrey, BC Canada - 2019</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
                
                <div class="item col-md-12 text-center">
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/award9.jpg') }}" class="card-img-top lazyload" alt="awards">
                        <div class="card-body">
                            <h6 class="text-left">Best Ayurvedic Doctor in India– 2018</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12">
                                    <p class="text-left">Best Ayurvedic Doctor Awarded By Sh. Brahm Mohindra (Minister of Health & Family Welfare GOVT. of Punjab)</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="item col-md-12 text-center">
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/award3.webp') }}" class="card-img-top lazyload" alt="awards">
                        <div class="card-body">
                            <h6 class="text-left">Best Ayurvedic Clinic Awarded-2018</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12">
                                    <p class="text-left">Best Ayurvedic Clinic Awarded by Chief minister of Punjab Captain Amrinder Singh – 2018</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item col-md-12 text-center">
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/award8.webp') }}" class="card-img-top lazyload" alt="awards">
                        <div class="card-body">
                            <h6 class="text-left">Excellence in the field of Ayurveda-2018</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12">
                                    <p class="text-left">Excellence in the field of Ayurveda Honored by the Group of Dainik Bhaskar </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item col-md-12 text-center">
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/award1.webp') }}" class="card-img-top lazyload" alt="Dr Sarabhjot Singh (BAMS)">
                        <div class="card-body">
                            <h6 class="text-left">Excellence in the field of Ayurvedic Treatment-2018</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12">
                                    <p class="text-left">Excellence in the field of Ayurvedic Treatment by Sh. Shripad Naik Ministry of AYUSH, Government of India</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="item col-md-12 text-center">
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/award5.webp') }}" class="card-img-top lazyload" alt="Dr. Urja Sachdeva (BAMS)">
                        <div class="card-body">
                            <h6 class="text-left">Award of Honour by ICSI</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12">
                                    <p class="text-left">International Conference on Swarm Intelligence – 2019</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>               
            </div>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec our-expert"> 
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-md-6 col-sm-12">
                <div class="cl-test">
                    <h2 class="client-test-title">Events & <b>Media</b></h2>
                    <p class="text-center">Dr. Sharda Ayurveda for its excellence received various awards from recognized personalities</p>
                </div>

                <div class="slider slider-2">
                    <div class="photos">
                        <img src="{{ URL::asset('front/images/events2.webp') }}" class="shown" alt="">
                        <img src="{{ URL::asset('front/images/events1.webp') }}" alt="">
                        <img src="{{ URL::asset('front/images/events3.webp') }}" alt="">
                        <img src="{{ URL::asset('front/images/events4.webp') }}" alt="">
                    </div>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="col-xl-6 col-md-6 col-sm-12">
                <div class="cl-test">
                    <h2 class="client-test-title">NEWS & <b>Interview</b></h2>
                    <p class="text-center">Recognized in News/Media and greatly active through published articles and news interviews</p>
                </div>

                <div class="slider slider-3">
                    <div class="photos">
                        <img src="{{ URL::asset('front/images/news-outlook.webp') }}" class="shown" alt="">
                        <img src="{{ URL::asset('front/images/news-mid-day.webp') }}" alt="">
                        <img src="{{ URL::asset('front/images/news-pioneer.webp') }}" alt="">
                        <img src="{{ URL::asset('front/images/news-health-shots.webp') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</section>

<section class="dsa-india-ayurvedic-sec"> 
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-md-12 col-sm-12">
                <div class="cl-test">
                    <h2 class="client-test-title">Dr. Mukesh Sharda -<b>Ayurvedic Doctor in India</b></h2>
                    <p class="text-center bottom-line">Once You Choose best ayurvedic doctor in Ludhiana, Punjab- India, Anything is Possible</p>
                    <div id="dna_video">
                        <div class="youtube-player" data-id="PGKM-htmG9A" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:78%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; padding-bottom:38.25%;overflow:hidden;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/backpain.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 100%;height:auto; right: 0; top: -100%;">

                            <div class='icon-video' style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec"> 
    <div class="container">
        <div class="row cut-row">
            <div class="col-xl-12 col-md-12 col-sm-12">
                <div class="cl-test">
                    <h2 class="client-test-title">Latest <b>Videos</b></h2>
                    <p class="text-center bottom-line">Know benefits of Ayurveda for treating diseases from Dr. Mukesh Sharda through our newest updates</p>
                </div>
            </div>
        </div>

        <div class="row list_stores ">
            <div class="col-md-4">
                <div id="dna_video">
                    <div class="youtube-player" data-id="ckxCOzzNMVw" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                        <img src="{{ URL::asset('front/images/ckxCOzzNMVw.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 100%;height:auto; right: 0; top: -100%;">
                        <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=ckxCOzzNMVw">Best Food For Rheumatoid Arthritis</a>

                        <div style="height: 72px; width: 72px; left: 50%; top: 30%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div id="dna_video">
                    <div class="youtube-player" data-id="w8UaP6U1H18" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                        <img src="{{ URL::asset('front/images/w8UaP6U1H18.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 100%;height:auto; right: 0; top: -100%;">
                        <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=nb4GuoRBuo8">How to Treat Rheumatoid Arthritis - Best Ayurvedic Treatment</a>

                        <div style="height: 72px; width: 72px; left: 50%; top: 30%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div id="dna_video">
                    <div class="youtube-player" data-id="uJyW3j2vyts" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                        <img src="{{ URL::asset('front/images/uJyW3j2vyts.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 100%;height:auto; right: 0; top: -100%;">
                        <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=nb4GuoRBuo8">Ayurvedic treatment for Rheumatoid Arthritis</a>

                        <div style="height: 72px; width: 72px; left: 50%; top: 30%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@include('../partials/frontend/form')

<script type="text/javascript">

window.onload = function() {
    var slider2 = new Slider({
        images: '.slider-2 img',
        btnPrev: '.slider-2 .buttons .prev',
        btnNext: '.slider-2 .buttons .next',
        auto: true,
        rate: 2000
    });

    var slider3 = new Slider({
        images: '.slider-3 img',
        btnPrev: '.slider-3 .buttons .prev',
        btnNext: '.slider-3 .buttons .next',
        auto: true,
        rate: 2000
    });
}

function Slider(obj) {

    this.images = document.querySelectorAll(obj.images);
    console.log(this.images);
    this.auto = obj.auto;
    this.btnPrev = obj.btnPrev;
    this.btnNext = obj.btnNext;
    this.rate = obj.rate || 1000;

    var i = 0;
    var slider = this;

    this.prev = function () {
        slider.images[i].classList.remove('shown');
        i--;

        if (i < 0) {
            i = slider.images.length - 1;
        }

        slider.images[i].classList.add('shown');
    }

    this.next = function () {
        slider.images[i].classList.remove('shown');
        i++;

        if (i >= slider.images.length) {
            i = 0;
        }

        slider.images[i].classList.add('shown');
    }

    // document.querySelector(slider.btnPrev).onclick = slider.prev;
    // document.querySelector(slider.btnNext).onclick = slider.next;

    if (slider.auto)  {
        setInterval(slider.next, slider.rate);
    }
};

    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            responsiveClass: !0,
            navigation : true,
            autoplay: true,
            loop: true,
            smartSpeed: 2500,
            singleItem : true,
            navigationText: ["<i class='fa fa-angle-left fa-2'></i>", "<i class='fa fa-angle-right fa-2'></i>"],
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                480: {
                    items: 1,
                    nav: !1
                },
                768: {
                    items: 3,
                    nav: !1
                },
                1000: {
                    items: 4,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1024: {
                    items: 4,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1366: {
                    items: 4,
                    nav: !1
                }
            }
        })
    });

</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Dr Mukesh Sharda",
    "item": "https://www.drshardaayurveda.com/dr-mukesh-sharda"  
  }]
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org/",
  "@type": "Person",
  "name": "Dr. Mukesh Sharda",
  "url": "https://www.drshardaayurveda.com/dr-mukesh-sharda",
  "image": "https://www.drshardaayurveda.com/front/images/Ayurveidc-doctor-dr-mukesh-sharda.webp",
  "sameAs": [
    "https://www.facebook.com/DrShardaAyurveda/",
    "https://www.drshardaayurveda.com/dr-mukesh-sharda",
    "https://twitter.com/shardaayurveda",
    "https://www.instagram.com/DrShardaAyurveda/",
    "https://www.youtube.com/c/DrShardaAyurveda/",
    "https://linkedin.com/company/dr-sharda-ayurveda"
  ],
  "jobTitle": "Ayurvedic Doctor",
  "worksFor": {
    "@type": "Organization",
    "name": "Dr. Sharda Ayurveda"
  }  
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "Who is Dr. Mukesh Sharda?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Dr. Mukesh Sharda (B.A.M.S, Ph.D.) is a renowned Ayurvedic doctor who is recognized worldwide for treating chronic disease patients through natural and authentic Ayurvedic treatment. She is the founder of Dr. Sharda Ayurveda and is serving mankind since 2013 through its various widespread branches in India."
    }
  },{
    "@type": "Question",
    "name": "What all treatments are available in Dr. Sharda Ayurveda?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Dr. Sharda Ayurveda through its various widespread branches in Punjab, India is serving patients dedicatedly through effective Ayurvedic treatment with greater emphasis on dietary and lifestyle changes along with herbal medications. They are providing treatment for joint, respiratory, skin, endocrine, digestive, sexual, and many other diseases."
    }
  },{
    "@type": "Question",
    "name": "Is Dr. Sharda Ayurveda providing good treatment for Rheumatoid arthritis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, they do provide the best and most effective treatment for Rheumatoid arthritis. Dr. Mukesh Sharda founder Dr. Sharda Ayurveda is herself a renowned Ayurvedic doctor who alone holds the success rate of treating thousands of RA patients through the holistic approach of Ayurveda. They are not only confined to India but are providing Ayurvedic treatment worldwide through telephonic and video consultations."
    }
  }]
}
</script>
@stop