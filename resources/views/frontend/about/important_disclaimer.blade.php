<?php
/**
 * important disclaimer Page 
 * 
 * @created    09/05/2022
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2022
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                     <h1 class="ser-heading mt-4" >Important Disclaimer</h1>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/about-us/important-disclaimer">Important Disclaimer</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-12 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <!-- <h1>Important Disclaimer</h1> -->
                <p>
                Our products are natural or conventional dietary supplements and not 'programmed' pharmaceutical drugs. They are free of controlled substances. Under the law, they are not aimed at diagnosing, treating, curing, or preventing a disease. All products are vegetarian, gluten-free, always thoroughly tested for heavy metals and bacteria, and use only the highest quality fresh herbs, combined in a very clean and modern system. 
                All information on our part is provided for educational and inspirational purposes only and should not be taken as a substitute for your doctor's advice or interpreted as medical advice. We do not recommend ending the use of prescription medications without consulting your doctor. Any videos of people discussing their experiences and outcomes are their personal opinions, and the outcomes may differ <strong style="font-weight: bold;">(i.e. depending on quantity, duration, body type, and discipline with recommended diet, lifestyle, and many factors)  </strong>. Our goal is to simply spread knowledge about the ancient art and science of Ayurveda with the aim of improving your health, as it has for thousands of people around the world for thousands of years.</p>
                <p>
                We hope you enjoy your time with us and that you learn a lot about <a class="green-anchor"href="https://www.drshardaayurveda.com/">Dr. Sharda Ayurveda</a>.
                </p>
                </div>
            </div>
        </div>
    </div>
</section>

@include('../partials/frontend/form')

</script>
@stop