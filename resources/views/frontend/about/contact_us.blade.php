<?php
/**
 * Contact Page 
 * 
 * @created    15/11/2021
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2021
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<?php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$routeName = \Route::currentRouteName();
?>
<div class='contact-us'>
		<div class="container">
			<div class="row heading-row">
				<div class="col-lg-12 col-md-12 text-center">
					<h1 class="heading uppercase bottom-line"><small>Locate Us</small></h1>
				</div>
			</div>
		</div>


	<section class="dsa-india-ayurvedic-sec abt-us" id="types"> 
		<div class="container">
			<div class="row list_stores ">
				<div class="col-md-4">
					<div class="store_address_box">
						<div class="store_title">Ludhiana (H.O.)</div>
						<div class="store_address">
						562-L, Opposite Suman Hospital, Model Town, Ludhiana, Punjab-141002
						</div>
						<div class="store_phone">Tel: +91-98760-35500</div>
						<div class="view_on_map" data-code="<iframe src=&quot;https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3423.8098235598463!2d75.84080461446577!3d30.89198068499253!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x391a83ad6d88e7d3%3A0x72c134b600c1597b!2sDr.Sharda%20-%20Best%20Ayurvedic%20Clinic%20Ludhiana!5e0!3m2!1sen!2sin!4v1637139215788!5m2!1sen!2sin width=&quot;100%&quot; height=&quot;450&quot; frameborder=&quot;0&quot; style=&quot;border:0;&quot; allowfullscreen=&quot;&quot;></iframe>"
							>View on map
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="store_address_box">
						<div class="store_title">Mohali</div>
						<div class="store_address">
							Opposite Army Canteen, Sco 105, Phase 10, Sector 64, Sahibzada Ajit Singh Nagar, Punjab-160062
						</div>
						<div class="store_phone">Tel: +91-98720-70926</div>
						<div class="view_on_map" data-code="<iframe src=&quot;https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3431.2105875748803!2d76.73987191446025!3d30.68435049501518!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390fedcddcde8bf9%3A0x4ecb4163fc94fd5a!2sDr.Sharda%20-%20Best%20Ayurvedic%20Clinic%20Mohali!5e0!3m2!1sen!2sin!4v1637139400164!5m2!1sen!2sin width=&quot;100%&quot; height=&quot;450&quot; frameborder=&quot;0&quot; style=&quot;border:0;&quot; allowfullscreen=&quot;&quot;></iframe>"
							>View on map
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="store_address_box">
						<div class="store_title">Bathinda</div>
						<div class="store_address">
							Opposite Sepal Hotel, Gate No.1, Near Tinkoni Chowk Bathinda, Punjab-151001
						</div>
						<div class="store_phone">Tel: +91-98726-20926</div>
						<div class="view_on_map" data-code="<iframe src=&quot;https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13789.80959696786!2d74.937764!3d30.224173!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x2625f0b094415ff2!2sDr.Sharda%20-%20Best%20Ayurvedic%20Clinic%20Bathinda!5e0!3m2!1sen!2sin!4v1637138652324!5m2!1sen!2sin width=&quot;100%&quot; height=&quot;450&quot; frameborder=&quot;0&quot; style=&quot;border:0;&quot; allowfullscreen=&quot;&quot;></iframe>"
							>View on map
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="store_address_box">
						<div class="store_title">Sri Muktsar Sahib</div>
						<div class="store_address">
							Abohar Road, Near Bus Stand, Sri Muktsar Sahib, Punjab-152026
						</div>
						<div class="store_phone">Tel: +91-98767-50926</div>
						<div class="view_on_map" data-code="<iframe src=&quot;https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3438.9995796073304!2d74.51161191445435!3d30.464450005565208!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x391769279f3b69c5%3A0xff5387c9197143da!2sDr.Sharda%20-%20Best%20Ayurvedic%20Clinic%20Muktsar%20Sahib!5e0!3m2!1sen!2sin!4v1637139495187!5m2!1sen!2sin width=&quot;100%&quot; height=&quot;450&quot; frameborder=&quot;0&quot; style=&quot;border:0;&quot; allowfullscreen=&quot;&quot;></iframe>"
							>View on map
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div id="map_iframe"></div>
				</div>
			</div>
		</div>
	</section>

	<section class="section-wrap new-arrivals pt-0 pb-20">
		<div class="container">
			<div class="row heading-row">
				<div class="col-md-12 text-center">
					<h2 class="heading uppercase bottom-line"><small>Get in touch</small></h2>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6 mb-40">
					<div class="contact-item">
						<div class="contact-icon">
							<i class="fa fa-map-marker" aria-hidden="true"></i>
						</div>
						<p><strong>Head Office</strong><br>
							562-L, Opposite Suman,<br>
							Hospital, Model Town,<br>
							Ludhiana,<br>
							Punjab 141002<br>
							Tel: <b>+91-98760-35500</b>
						</p>
					</div>
<!-- 					<div class="contact-item">
						<div class="contact-icon">
							<i class="fa fa-mobile fa-lg" aria-hidden="true"></i>
						</div>
						<span>India <a href="tel:919876035500">+91-98760-35500</a></span>
					</div> -->
					<div class="contact-item">
						<div class="contact-icon">
						<i class="fa fa-envelope" aria-hidden="true"></i>
						</div>
						<!-- <a href="mailto:info@drshardaayurveda.com" class="sliding-link"> -->info@drshardaayurveda.com<!-- </a> -->
					</div> <!-- end email -->
					<h5 class="uppercase mt-40 mb-20">Follow us on Social</h5>
					<div class="social-icons">
						<a href="https://www.facebook.com/DrShardaAyurveda" target="_blank"><i class="fa fa-facebook"></i></a>
						<a href="https://www.instagram.com/DrShardaAyurveda/" target="_blank"><i class="fa fa-instagram"></i></a>
						<a href="https://twitter.com/shardaayurveda" target="_blank"><i class="fa fa-twitter"></i></a>
						<a href="https://www.youtube.com/c/DrShardaAyurveda" target="_blank"><i class="fa fa-youtube"></i></a>
					</div>
				</div>

				<div class="col-md-6">
					<form id="form_contact" action="https://www.drshardaayurveda.com/submit_lp_page.php" method='POST'>
						<input type='hidden' name="subject" value="<?= $routeName; ?>">
                        <input type='hidden' name="action" value="user_mail">
                        <input type='hidden' name="return_url" value="https://www.drshardaayurveda.com/thanks.html">
                        <input type='hidden' name="page_url" value="<?= $actual_link; ?>">
						<div class="contact-fname">
							<div class="error form_error form-error-fname" id="form-error-fname"></div>
							<input name="name" id="fname" type="text" placeholder="Name*">
						</div>
						<div class="contact-email">
							<div class="error form_error form-error-email" id="form-error-email"></div>
							<input name="email" id="email" type="email" placeholder="E-mail*">
						</div>
						<div class="contact-mobile">
							<div class="error form_error form-error-mobile" id="form-error-mobile"></div>
							<input name="mobile" id="mobile" type="text" placeholder="Mobile">
						</div>
						<div class="error form_error form-error-comment" id="form-error-comment"></div>
						<textarea name="description" id="comment" placeholder="Message" rows="9"></textarea>
						<input type="submit" class="btn btn-lg btn-color" value="Submit">
						<div id="msg" class="message"></div>
					</form>
				</div> <!-- end col -->
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$('.view_on_map').on('click', function(){       
        var iframe_code = $(this).data('code');        
        $('#map_iframe').html(iframe_code);

        $('html, body').animate({
          scrollTop: $("#map_iframe").offset().top
        }, 1000)
    });
</script>
@stop