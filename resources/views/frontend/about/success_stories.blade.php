<?php
/**
 * Success Stories Page 
 * 
 * @created    19/11/2021
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2021
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<?php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$routeName = \Route::currentRouteName();
?>
<div class='success-stories'>
    <div class="container">
        <div class="row heading-row">
            <div class="col-lg-12 col-md-12 text-center">
                <h1 class="heading uppercase bottom-line"><small>Patient Testimonials</small></h1>
            </div>
        </div>
    </div>

	<section class="successtest" id="types"> 
		<div class="container">
			<div class="row list_stores ">
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="ASUxMhW10_Q" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/ASUxMhW10_Q.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=ASUxMhW10_Q">How I Treated Rheumatoid Arthritis with Ayurveda</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="gpWdbqJx9LE" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/gpWdbqJx9LE.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=gpWdbqJx9LE">Without Surgery Chronic Back Pain Treated with Ayurvedic Treatment</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="3wDomKFVnhA" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/3wDomKFVnhA.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=3wDomKFVnhA">How I Cure My Rheumatoid Arthritis (RA) with Ayurvedic Treatment in 6 Months</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="wmcX2ogrWCQ" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/wmcX2ogrWCQ.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=wmcX2ogrWCQ">Chronic Back Pain Treated with Ayurveda - Patient Testimonial</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="BgjC4_Ou8xY" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/BgjC4_Ou8xY.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=BgjC4_Ou8xY">Joint Pain (Arthritis) Recovered Patient Testimonial</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="BU4RYw3h6fY" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/BU4RYw3h6fY.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=BU4RYw3h6fY">The Back Pain Treated within 4 Months | Patient testimonial</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="itlz_3A9zhI" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/itlz_3A9zhI.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=itlz_3A9zhI">Spinal Disorder Treated with Ayurvedic Treatment</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="JF6ZgpQDLxk" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/JF6ZgpQDLxk.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=JF6ZgpQDLxk">Cyst in Uterus treated with Ayurvedic Treatment</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="vsiQzNU1E2A" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/vsiQzNU1E2A.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=vsiQzNU1E2A">Got Relief from Back Pain Through Ayurveda</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="62ftPz2IN2o" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/62ftPz2IN2o.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=62ftPz2IN2o">Rheumatoid Arthritis Healed Successfully with Ayurveda- Patient Testimonial</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="Tep6lbO3yWc" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/Tep6lbO3yWc.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=Tep6lbO3yWc">Back Pain Patient Testimonial | Ayurveda Helped Treated my Back Pain</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="anz0fxwAQVU" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/anz0fxwAQVU.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=anz0fxwAQVU">Recovered from Allergic Asthma - Patient testimonial</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="KTpPxeeRyHE" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/KTpPxeeRyHE.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=xFGNXX3DAhM">Rheumatoid Arthritis (RA) गठिया Patient Testimonial</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="xFGNXX3DAhM" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/xFGNXX3DAhM.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=xFGNXX3DAhM">पुराने नजला और जुकाम (Sinusitis) से मिली राहत with Dr. Sharda Ayurvedic Treatment</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="QX-7BHDo2io" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/01QX-7BHDo2io.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=QX-7BHDo2io">Neck Pain (Cervical spondylosis) Patient Testimonial | Dr. Sharda Ayurveda</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="cfGLEk0s_hY" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/cfGLEk0s_hY.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=cfGLEk0s_hY">Ayurveda Healed my Cysts and Fibroids - Patient Testimonial | Dr Sharda Ayurveda</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="bO6K2dm5AN0" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/bO6K2dm5AN0.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=bO6K2dm5AN0">Got Relied From Back Pain within Month | Dr Sharda Ayurveda</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="pujSQbxCBK0" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/pujSQbxCBK0.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=pujSQbxCBK0">Inhaler Dependency Prevented - Asthma Patient Testimonial | Dr Sharda Ayurveda</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="CElaFefer2w" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/CElaFefer2w.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=CElaFefer2w">Severe Cervical & Neck Pain Patient Testimonial | Dr Sharda Ayurveda</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="BodQAXFJA5o" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/BodQAXFJA5o.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=BodQAXFJA5o">Dr. Sharda Ayurveda Treated my Asthma - Patient Testimonial | Dr. Sharda Ayurveda</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="WTmIbQmS8E4" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/WTmIbQmS8E4.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=WTmIbQmS8E4">Joint Pain Patient Testimonial | Dr Sharda Ayurveda</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="CAaJzK9dJso" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/CAaJzK9dJso.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=CAaJzK9dJso">Patient Testimonial- 6 years long Rheumatoid Arthritis treated | Dr. Sharda Ayurveda</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="UXCocc4CYJE" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/UXCocc4CYJE.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=UXCocc4CYJE">Got relief from Chronic Back Pain in 3 months | Dr. Sharda Ayurveda</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="_FnGLQWfTdQ" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/_FnGLQWfTdQ.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=_FnGLQWfTdQ">Got Relief From Rheumatoid Arthritis (RA) in 2 Months | Dr. Sharda Ayurveda</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="jCAQgJ3JtpE" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/jCAQgJ3JtpE.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=jCAQgJ3JtpE">Chronic fungal infection treated within a month | Dr Sharda Ayurveda</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="CKThTWLTaoI" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/CKThTWLTaoI.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=CKThTWLTaoI">Chronic Psoriasis was treated within just 1 month | Dr. Sharda Ayurveda.</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="aZ5fBizY8X4" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/aZ5fBizY8X4.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=aZ5fBizY8X4">Get Relief from Knee and Hip Joint with Dr. Sharda Ayurvedic Treatment</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="2Lyu2WoYxaQ" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/2Lyu2WoYxaQ.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=2Lyu2WoYxaQ">Get Relief From Skin Allergy Patient Testimonial</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="dw_7g1Ia734" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/dw_7g1Ia734.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=dw_7g1Ia734">Got relief from Rheumatoid Arthritis गठिया - Patient Testimonial</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="nb4GuoRBuo8" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/nb4GuoRBuo8.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">
                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=nb4GuoRBuo8">Recovery of Rheumatoid Arthritis patient in just 1 year of treatment</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="pz_8zLXlQTs" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/pz_8zLXlQTs.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
                    <div class="card-body ">
                        <h2 class="card-title fs-1pt2 mb-0 fw-600">
                            <a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=pz_8zLXlQTs">Chronic Asthma Recovered in 2 months Patient Testimonial</a>
                        </h2>
                    </div>
                </div>
                <div class="col-md-4 mb-10">
                                        <div id="dna_video">
                        <div class="youtube-player" data-id="dthEvJuw6EA" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/dthEvJuw6EA.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
					<div class="card-body ">
						<h2 class="card-title fs-1pt2 mb-0 fw-600">
							<a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=dthEvJuw6EA">Patient Testimonial - Chronic Back Pain Cured with Dr. Sharda Ayurvedic treatment</a>
						</h2>
					</div>
				</div>
                <div class="col-md-4 mb-10">
                                        <div id="dna_video">
                        <div class="youtube-player" data-id="Pc8NMMIh-gs" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/Pc8NMMIh-gs.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
					<div class="card-body ">
						<h2 class="card-title fs-1pt2 mb-0 fw-600">
							<a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=Pc8NMMIh-gs">Joint Pain Cured in 4 Month | Patient Testimonial | Dr. Sharda Ayurveda</a>
						</h2>
					</div>
				</div>
                <div class="col-md-4 mb-10">
                                        <div id="dna_video">
                        <div class="youtube-player" data-id="TTUFRY46940" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/TTUFRY46940.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
					<div class="card-body ">
						<h2 class="card-title fs-1pt2 mb-0 fw-600">
							<a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=TTUFRY46940">Spine & Back Pain Patient Testimonial cured in 2 months | Dr. Sharda Ayurveda</a>
						</h2>
					</div>
				</div>
                <div class="col-md-4 mb-10">
                                        <div id="dna_video">
                        <div class="youtube-player" data-id="JDkdEcEVXx8" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/JDkdEcEVXx8.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
					<div class="card-body ">
						<h2 class="card-title fs-1pt2 mb-0 fw-600">
							<a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=JDkdEcEVXx8">Cured Chronic Spine Problem with Dr. Sharda Ayurvedic Treatment</a>
						</h2>
					</div>
				</div>
                <div class="col-md-4 mb-10">
                                        <div id="dna_video">
                        <div class="youtube-player" data-id="awL9YlSU3-Q" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/awL9YlSU3-Q.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
					<div class="card-body ">
						<h2 class="card-title fs-1pt2 mb-0 fw-600">
							<a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=awL9YlSU3-Q">Knee pain From 1 Year Got Recovered within 3 Months Patient Testimonial | Dr. Sharda Ayurveda</a>
						</h2>
					</div>
				</div>
                <div class="col-md-4 mb-10">
                                        <div id="dna_video">
                        <div class="youtube-player" data-id="gYv-IX46VKI" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/gYv-IX46VKI.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
					<div class="card-body ">
						<h2 class="card-title fs-1pt2 mb-0 fw-600">
							<a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=awL9YlSU3-Q">Allergic Rhinitis Patient Testimonial - Dr. Sharda Ayurveda</a>
						</h2>
					</div>
				</div>
                <div class="col-md-4 mb-10">
                                        <div id="dna_video">
                        <div class="youtube-player" data-id="Qar8gq3l_XM" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/Qar8gq3l_XM.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
					<div class="card-body ">
						<h2 class="card-title fs-1pt2 mb-0 fw-600">
							<a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=Qar8gq3l_XM">Cured Back Pain (कमर दर्द) in 5 Months Patient Testimonial</a>
						</h2>
					</div>
				</div>
                <div class="col-md-4 mb-10">
                                        <div id="dna_video">
                        <div class="youtube-player" data-id="OPjhs7F1U_Q" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/OPjhs7F1U_Q.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
					<div class="card-body ">
						<h2 class="card-title fs-1pt2 mb-0 fw-600">
							<a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=OPjhs7F1U_Q">Joint Pain Patient Testimonial | Dr. Sharda Ayurveda</a>
						</h2>
					</div>
				</div>
                <div class="col-md-4 mb-10">
                                        <div id="dna_video">
                        <div class="youtube-player" data-id="1xN6UVZ-6p8" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/1xN6UVZ-6p8.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
					<div class="card-body ">
						<h2 class="card-title fs-1pt2 mb-0 fw-600">
							<a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=1xN6UVZ-6p8">Rheumatoid Arthritis Patient Mrs. Renu Testimonial | Dr. Sharda Ayurveda</a>
						</h2>
					</div>
				</div>
                <div class="col-md-4 mb-10">
                                        <div id="dna_video">
                        <div class="youtube-player" data-id="UAIIVsXq5Mo" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/UAIIVsXq5Mo.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
					<div class="card-body ">
						<h2 class="card-title fs-1pt2 mb-0 fw-600">
							<a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=UAIIVsXq5Mo">How I Cured Psoriasis (सोरायसिस) in 6 Months - Dr. Sharda Ayurveda</a>
						</h2>
					</div>
				</div>
                <div class="col-md-4 mb-10">
                                        <div id="dna_video">
                        <div class="youtube-player" data-id="5BFWBcPUuCw" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/5BFWBcPUuCw.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
					<div class="card-body ">
						<h2 class="card-title fs-1pt2 mb-0 fw-600">
							<a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=5BFWBcPUuCw">Knee Pain Patient Testimonial - Dr. Sharda Ayurveda</a>
						</h2>
					</div>
				</div>
                <div class="col-md-4 mb-10">
                                        <div id="dna_video">
                        <div class="youtube-player" data-id="nltm4fE9Hzw" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/nltm4fE9Hzw.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
					<div class="card-body ">
						<h2 class="card-title fs-1pt2 mb-0 fw-600">
							<a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=nltm4fE9Hzw">Asthma allergy Patient Testimonial | Dr. Sharda Ayurveda</a>
						</h2>
					</div>
				</div>
                <div class="col-md-4 mb-10">
                                        <div id="dna_video">
                        <div class="youtube-player" data-id="fa5a8gnSyck" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/fa5a8gnSyck.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
					<div class="card-body ">
						<h2 class="card-title fs-1pt2 mb-0 fw-600">
							<a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=fa5a8gnSyck">Knee Pain Patient Testimonial - Dr. Sharda Ayurveda</a>
						</h2>
					</div>
				</div>
                <div class="col-md-4 mb-10">
                                        <div id="dna_video">
                        <div class="youtube-player" data-id="dIrNj1W32EQ" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/dIrNj1W32EQ.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
					<div class="card-body ">
						<h2 class="card-title fs-1pt2 mb-0 fw-600">
							<a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=dIrNj1W32EQ">Joint Pain Patient Testimonial | Dr. Sharda Ayurveda</a>
						</h2>
					</div>
				</div>
                <div class="col-md-4 mb-10">
                                        <div id="dna_video">
                        <div class="youtube-player" data-id="ZObDS45o-xg" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/ZObDS45o-xg.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div>  
					<div class="card-body ">
						<h2 class="card-title fs-1pt2 mb-0 fw-600">
							<a class="text-dark youtube-popup" href="https://www.youtube.com/watch?v=ZObDS45o-xg">Rheumatoid arthritis Patient S. Manveer Singh Testimonial | Dr. Sharda Ayurveda</a>
						</h2>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="section-wrap new-arrivals pt-0 pb-20">
		<div class="container">
			<div class="row heading-row">
				<div class="col-md-12 text-center">
					<h2 class="heading uppercase bottom-line"><small>Get in touch</small></h2>
				</div>
			</div>

			<div class="row">
				<div class="col-md-6 mb-40">
					<div class="contact-item">
						<div class="contact-icon">
							<i class="fa fa-map-marker" aria-hidden="true"></i>
						</div>
						<p><strong>Head Office</strong><br>
							562-L, Opposite Suman,<br>
							Hospital, Model Town,<br>
							Ludhiana,<br>
							Punjab 141002<br>
							Tel: <b>+91-98760-35500</b>
						</p>
					</div>
<!-- 					<div class="contact-item">
						<div class="contact-icon">
							<i class="fa fa-mobile fa-lg" aria-hidden="true"></i>
						</div>
						<span>India <a href="tel:919876035500">+91-98760-35500</a></span>
					</div> -->
					<div class="contact-item">
						<div class="contact-icon">
						<i class="fa fa-envelope" aria-hidden="true"></i>
						</div>
						<!-- <a href="mailto:info@drshardaayurveda.com" class="sliding-link"> -->info@drshardaayurveda.com<!-- </a> -->
					</div> <!-- end email -->
					<h5 class="uppercase mt-40 mb-20">Follow us on Social</h5>
					<div class="social-icons">
						<a href="https://www.facebook.com/DrShardaAyurveda" target="_blank"><i class="fa fa-facebook"></i></a>
						<a href="https://www.instagram.com/DrShardaAyurveda/" target="_blank"><i class="fa fa-instagram"></i></a>
						<a href="https://twitter.com/shardaayurveda" target="_blank"><i class="fa fa-twitter"></i></a>
						<a href="https://www.youtube.com/c/DrShardaAyurveda" target="_blank"><i class="fa fa-youtube"></i></a>
					</div>
				</div>

				<div class="col-md-6">
					<form id="form_contact" action="https://www.drshardaayurveda.com/submit_lp_page.php" method='POST'>
						<input type='hidden' name="subject" value="<?= $routeName; ?>">
                        <input type='hidden' name="action" value="user_mail">
                        <input type='hidden' name="return_url" value="https://www.drshardaayurveda.com/thanks.html">
                        <input type='hidden' name="page_url" value="<?= $actual_link; ?>">
						<div class="contact-fname">
							<div class="error form_error form-error-fname" id="form-error-fname"></div>
							<input name="name" id="fname" type="text" placeholder="Name*">
						</div>
						<div class="contact-email">
							<div class="error form_error form-error-email" id="form-error-email"></div>
							<input name="email" id="email" type="email" placeholder="E-mail*">
						</div>
						<div class="contact-mobile">
							<div class="error form_error form-error-mobile" id="form-error-mobile"></div>
							<input name="mobile" id="mobile" type="text" placeholder="Mobile">
						</div>
						<div class="error form_error form-error-comment" id="form-error-comment"></div>
						<textarea name="description" id="comment" placeholder="Message" rows="9"></textarea>
						<input type="submit" class="btn btn-lg btn-color" value="Submit">
						<div id="msg" class="message"></div>
					</form>
				</div> <!-- end col -->
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$('.view_on_map').on('click', function(){       
        var iframe_code = $(this).data('code');        
        $('#map_iframe').html(iframe_code);

        $('html, body').animate({
          scrollTop: $("#map_iframe").offset().top
        }, 1000)
    });
</script>
@stop