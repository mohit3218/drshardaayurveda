<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')

<section class="dsa-why-choose" id="definition">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/IBS.webp') }}" class="why-choose-us img-fluid" alt="man holding his stomach in pain experience irritable bowel syndrome" title="man holding his stomach in pain experience irritable bowel syndrome">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h2 class="heading1">What is <b>Irritable Bowel Syndrome(IBS)?</b></h2>
                <p>
                    Irritable Bowel Syndrome (IBS) is a common disorder caused by a collection of symptoms like cramping, abdominal pain, bloating, gas, and constipation. It is also known as gastrointestinal tract, and it is a very dangerous chronic condition. Irritable Colon, Spastic Colon, Spastic Colon, and Mucous Collitis are further terms for IBS. It is unrelated to other bowel diseases and is a distinct disorder from inflammatory bowel disease. Ayurvedic treatment for irritable bowel syndrome, which involves the use of herbal remedies, dietary changes, and lifestyle modifications, has shown promise in alleviating the symptoms of this common gastrointestinal disorder.<br>
                    According to an overview from the <a class="green-anchor"href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5453305/" rel=nofollow> 2018 Study Published</a>, medical experts search for symptoms that have shown at least three days each month for the previous three months in order to identify it.<br>
                    In certain circumstances, IBS can harm the digestive system. This however, is uncommon. IBS doesn't enhance your risk of gastrointestinal malignancies, according to a <a class="green-anchor"href="https://pubmed.ncbi.nlm.nih.gov/35130187/" rel="nofollow">2022 research by Trusted Source</a> Nonetheless, it may still have a big impact on your life.
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                    <p>
                        <strong style="font-weight: bold;">What does Ayurveda say about IBS ?</strong><br>
                    According to the Ayurveda Irritable Bowel Syndrome (IBS) is referred to as Grahani, which is described as an Agni Adhiṣṭhāna (Digestive Fire), which aids in charge of converting food into energy and nutrients. Aahaar’s ingestion, digestion, absorption, and assimilation are all controlled by Grahani. If a person has difficulty in digesting food, this suggests that their Agni (Digestive System), or fire element—is weak.</p>
                        <p>Based on Bowel Patterns, it  divided into four Primary types:
                        <li>Vataja (Major Constipation Grahani (IBS))</li>
                        <li>Pittaja (Predominant Diarrhoea Grahani (IBS)) </li>
                        <li>Kaphaja (Predominance of Dysentery Grahani(IBS)) </li>
                        <li>Tridoṣaja(When all of the mentioned symptoms are present together)</li>
                    </p>
                    <div class="btn-ayurvedic">
                        <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                    </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec our-expert" style="background-color:#f3f3f3" id="causes">
    <div class="container-fluid" >
        <div class="row cut-row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">What Causes <b>Irritable Bowel Syndrome (IBS)?</b></h2>
                    <p font align="center" style="line-height:2">Grahani (IBS) is mostly caused by insufficient mandagni (digestive fire)  and ama (toxin generation). As per Ayurveda, IBS is linked to:</p>
                    <li>Bad diet and nutrition, such as eating a lot of fried food, eating too much, eating foods that are unhealthy for your gut, drinking too much, and eating cold meals.</li>
                    <li>Wrong timing of eating or skipping meals while you're hungry.</li>
                    <li>Many underlying disorders include a neural system imbalance, aberrant gastrointestinal motor and sensory activity, etc.</li>
                    <li>Accumulation of ama (toxins) in the tissues used for circulation.</li>
                    <li>Stress of any kind, both physical and emotional, weakens the body's defences and internal fortitude.</li>
                    <li>A disturbed and irregular natural biological rhythm.</li>
                </div>
            </div>
        </div>
    </div>
</section>	


<section class="dsa-india-ayurvedic-sec our-expert" style="background-color:#f3f3f3" id="causes">
    <div class="container-fluid" >
        <div class="row cut-row">
            <div class="col-md-6">
                <form action="{{ route('razorpay-payment') }}" method="POST" >
                    @csrf 
                    <input type="tex" name="name" class="form-control" placeholder="Name" required/>
                    <input type="tex" name="email" class="form-control" placeholder="E-Mail" />
                    <script src="https://checkout.razorpay.com/v1/checkout.js"
                            data-key="{{ env('RAZORPAY_KEY') }}"
                            data-amount="100"
                            data-buttontext="Pay 1 INR"
                            data-name="Dr Sharda Ayurveda"
                            data-description="Razorpay payment"
                            data-image="/images/logo-icon.png"
                            data-prefill.name="ABC"
                            data-prefill.email="abc@gmail.com"
                            data-theme.color="#ff7529">
                    </script>
                </form>
            </div>
        </div>
    </div>
</section>	

@stop