<?php
/**
 * Bathinda Page 
 * 
 * @created    31/10/2021
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2021
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Dr. Sharda Ayurveda - Bathinda</h3>
              <!--       <p style="font-size: 24px;">Protect Your Joints with Timely Treatment</p> -->
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/locations/bathinda">Bathinda</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-md-12">
                <!-- <h1 class="heading1">DR. SHARDA AYURVEDA- Ayurvedic clinic in Bathinda</h1> -->
                <p>
                Ayurveda is the best medical system for overall health and well-being. Ayurveda is a blend of a proper healthy diet, lifestyle, exercises, natural remedies, detoxification therapy, and Ayurvedic medicines to promote a healthy disease-free life. The biggest advantage of Ayurvedic medicines is that they do not harm the body and have no side effects. If you are looking for the best <strong style="font-weight: bold;">Ayurvedic clinic in Bathinda </strong> to get disease-free and live a healthy long life then you are at the right place.<strong style="font-weight: bold;"> Dr. Sharda Ayurveda  is the best Ayurvedic clinic in Bathinda</strong>. Dr. Sharda Ayurveda is the pioneer of providing the best Ayurvedic medicines and treatments.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/bathinda-clinic.webp') }}" class="why-choose-us img-fluid" alt="Bathinda Ayurvedic clinic">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1">Ayurvedic Clinic in <b>Bathinda</b></h1>
                <p>
                Dr. Sharda Ayurveda- Ayurvedic clinic in Bathinda is the most preferred Ayurvedic hospital for treating all chronic diseases with just Ayurvedic medicines. We have a team of professional, experienced, and highly educated doctors in their specialized field who aims to make a patient disease and pain-free. Treatment for all the diseases at the clinic in Bathinda is cost-effective so that everyone can avail of the services. Ayurvedic medicines are safe to consume without any harmful effects on the body.
                </p>
                <div class="btn-ayurvedic">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-md-12">
                <h6 class="heading1">How is Dr. Sharda Ayurveda Bathinda clinic different and best from others?</h6>
                <ul>
                    <li>For the convenience of patients we have access to an easy online booking facility. </li>
                    <li>All the Ayurvedic medicines are manufactured, packed under our guidance, and are steroids free which are completely safe to be taken.</li>
                    <li>Any query of our patients gets easily cleared by our patient advisor telephonically. </li>
                    <li>Ayurvedic clinic in Bathinda is located in a neat and clean locality with a team of helping, caring staff members. </li>
                </ul>
            </div>
        </div>
    </div>
</section>


<section class="dsa-india-ayurvedic-sec our-expert" id="types" style="margin-top: 3%;"> 
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="cl-test">
                <h2 class="client-test-title">Ayurvedic doctors <b>in Bathinda</b></h2>
                <p>
                The team of doctors in Bathinda is well-experienced and highly qualified and they are motivated towards only Ayurvedic treatment without recommending any surgery for their ailment. Our team of Ayurvedic doctors nearest you perform Ayurvedic therapies followed by Ayurvedic medicines to treat the health problems. Dr. Sharda Ayurveda assures you that you are at a safe place and hands for your treatment. The doctors always treat the patient with full patience and making them comfortable too.
                <br />
            </div>
        </div>
         <div class="row cust-iba">
            <div class="owl-carousel owl-theme" >
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/dr-preeti.webp') }}" class="card-img-top lazyload" alt="Dr. Vandana sharma (BAMS)">
                        <div class="card-body">
                            <h6 class="text-center">Dr. Preeti Arora (BAMS)</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12 text-center">
                                    <div class="tp-social-icon">
                                        <a href="https://www.facebook.com/DrShardaAyurveda" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="https://twitter.com/shardaayurveda" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="https://www.youtube.com/c/DrShardaAyurveda" target="_blank"><i class="fa fa-youtube"></i></a>
                                        <a href="https://www.linkedin.com/company/dr-sharda-ayurveda" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        <a href="https://www.instagram.com/DrShardaAyurveda/" target="_blank"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/dr-rashmeet-kaur.webp') }}" class="card-img-top lazyload" alt="Dr. Jeevan Jyoti (BAMS)">
                        <div class="card-body">
                            <h6 class="text-center">Dr. Rashneet Kaur (BAMS)</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12 text-center">
                                    <div class="tp-social-icon">
                                        <a href="https://www.facebook.com/DrShardaAyurveda" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="https://twitter.com/shardaayurveda" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="https://www.youtube.com/c/DrShardaAyurveda" target="_blank"><i class="fa fa-youtube"></i></a>
                                        <a href="https://www.linkedin.com/company/dr-sharda-ayurveda" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        <a href="https://www.instagram.com/DrShardaAyurveda/" target="_blank"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 

<!--                 <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/Dr-Urja-Sachdeva.webp') }}" class="card-img-top lazyload" alt="Dr. Urja Sachdeva">
                        <div class="card-body">
                            <h6 class="text-center">Dr. Urja Sachdeva (BAMS)</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12 text-center">
                                    <div class="tp-social-icon">
                                        <a href="https://www.facebook.com/DrShardaAyurveda" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="https://twitter.com/shardaayurveda" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="https://www.youtube.com/c/DrShardaAyurveda" target="_blank"><i class="fa fa-youtube"></i></a>
                                        <a href="https://www.linkedin.com/company/dr-sharda-ayurveda" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        <a href="https://www.instagram.com/DrShardaAyurveda/" target="_blank"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>        -->        
            </div>
        </div>
    </div>
</section>
<section class="dsa-why-choose" id="testimonials">
    <div class="container-fluid">
        <h2 text align="center">Dr. Sharda Ayurveda Bathinda - <strong>Patient Testimonial</strong></h2>
        <div class="row cut-row" >
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div id="dna_video">
                        <div class="youtube-player" data-id="CElaFefer2w" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/CElaFefer2w.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div> 
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div id="dna_video">
                        <div class="youtube-player" data-id="_FnGLQWfTdQ" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/_FnGLQWfTdQ.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div> 
            </div>
        </div>
    </div>
</section>
<section>
    <h2 style="font-size: 24px;" text align="center">Locate Dr. Sharda Ayurveda Clinic in Bathinda</h2>
                <iframe width="100%" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13789.80959696786!2d74.937764!3d30.224173!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x2625f0b094415ff2!2sDr.Sharda%20-%20Best%20Ayurvedic%20Clinic%20Bathinda!5e0!3m2!1sen!2sin!4v1640000622651!5m2!1sen!2sin" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</section>
@include('../partials/frontend/form')

<script type="text/javascript">
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            responsiveClass: !0,
            navigation : true,
            autoplay: true,
            loop: false,
            smartSpeed: 2500,
            singleItem : true,
            navigationText: ["<i class='fa fa-angle-left fa-2'></i>", "<i class='fa fa-angle-right fa-2'></i>"],
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                480: {
                    items: 1,
                    nav: !1
                },
                768: {
                    items: 3,
                    nav: !1
                },
                1000: {
                    items: 4,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1024: {
                    items: 4,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1366: {
                    items: 4,
                    nav: !1
                }
            }
        })
    });

</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "LocalBusiness",
  "name": "DR. Sharda Ayurveda - Best Ayurvedic Clinic Bathinda",
  "image": "https://www.drshardaayurveda.com/front/images/dr-sharda-logo.webp",
  "@id": "",
  "url": "https://www.drshardaayurveda.com/locations/bathinda",
  "telephone": "+91 9876035500",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "Gate No.1, near, Tinkoni Chowk, opp. Sepal Hotel, Parjapat Colony, Mata Jivi Nagar,",
    "addressLocality": "Bathinda",
    "postalCode": "151001",
    "addressCountry": "IN"
  },
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": 30.216872,
    "longitude": 74.8955329
  },
  "openingHoursSpecification": {
    "@type": "OpeningHoursSpecification",
    "dayOfWeek": [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
      "Sunday"
    ],
    "opens": "09:00",
    "closes": "19:00"
  },
  "sameAs": [
    "https://www.facebook.com/DrShardaAyurveda/",
    "https://twitter.com/shardaayurveda",
    "https://www.instagram.com/DrShardaAyurveda/",
    "https://www.youtube.com/c/DrShardaAyurveda/",
    "https://linkedin.com/company/dr-sharda-ayurveda",
    "https://in.pinterest.com/DrShardaAyurveda/",
    "https://www.drshardaayurveda.com/"
  ] 
}
</script>

@stop