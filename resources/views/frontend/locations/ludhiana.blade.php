<?php
/**
 * Ludhiana Page 
 * 
 * @created    31/10/2021
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2021
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Dr. Sharda Ayurveda - Ludhiana</h3>
              <!--       <p style="font-size: 24px;">Protect Your Joints with Timely Treatment</p> -->
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/locations/ludhiana">Ludhiana</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-md-12">
                <p>
                    The advantage of Ayurveda is to protect the health of the healthy and to cure the disorders of the diseased. Ayurveda provides marvelous results for every chronic disease too. It is very important to spread the Importance of Ayurveda to every home. Treating more than 5,00,000 patients worldwide motivated us to spread our branches. Dr. Sharda Ayurveda-Ayurvedic clinic in Ludhiana is the pioneer of providing the best Ayurvedic medicines in Ludhiana, Punjab.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/dr-sharda-ayurveda-about-us.webp') }}" class="why-choose-us img-fluid" alt="About-us">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1">Ayurvedic Clinic in <b>Ludhiana</b></h1>
                <p>
                    Dr. Sharda Ayurveda- Ayurvedic clinic in Ludhiana is the most preferred Ayurvedic clinic for treating all chronic diseases with just Ayurvedic medicines. We have a team of professional, experienced, and highly educated doctors in their specialized field who aims to make a patient disease and pain-free. Treatment for all the diseases at the Ayurvedic clinic in Ludhiana is cost-effective so that everyone can avail the services. Ayurvedic medicines are safe to consume without any harmful effects on the body.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-md-12">
                <h6 class="heading1">HOW IS DR. SHARDA AYURVEDA LUDHIANA CLINIC DIFFERENT AND BEST FROM OTHERS?</h6>
                <ul>
                    <li>We believe in patient convenience so we have access to an easy online booking facility.</li>
                    <li>All the Ayurvedic medicines are manufactured under our guidance and are steroids free which are completely safe to be taken and do not harm the body.</li>
                    <li>Patients get full assistance from our patient advisor telephonically.</li>
                    <li>Ayurvedic clinic in Ludhiana is located in the heart of the city and the environment is all clean and hygienic. If searching for best Ayurvedic doctor near me, then you should visit our Ludhiana branch. </li>
                </ul>
            </div>
        </div>
    </div>
</section>


<section class="doctors-sec">
    <div class="container-fluid">
    <div class="head-bx">
                <h2 class="heading1">Meet Our <b>Ayurvedic Doctors</b></h2>
                <p>Dr. Sharda Ayurveda is focusing on enhancing the quality of the treatment that will aim to provide better and improved health.</p>
        </div>
        <div class="row cust-iba">
            <div class="owl-carousel owl-theme">
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                    <img class="card-img-top" src="{{ URL::asset('front/images/Dr-Jeevan-Jyoti.webp') }}"alt="Dr. Jeevan Jyoti" title="Dr. Jeevan Jyoti">
                        <div class="card-body">
                        <h4 class="card-title">Dr. Jeevan Jyoti (BAMS, MD)</h4> <p class="card-text">Dr. Jeevan Jyoti holds an experience of more than 20+ years of experience and is specialized in treating a range of chronic diseases like skin, gynecology, respiratory, neurological, and many others. She has her post-graduate degree in clinical cosmetology. She is actively performing her duty as a senior Ayurvedic doctor with Dr. Sharda Ayurveda for the last 2 years.</p> 
                        </div>
                    </div>
                </div>


                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                    <img class="card-img-top" src="{{ URL::asset('front/images/Dr-Vandana-Sharma.webp') }}" alt="Dr. Vandana Sharma" title="Dr. Vandana Sharma">
                        <div class="card-body">
                        <h4 class="card-title">Dr. Vandana Sharma (BAMS)</h4>
                                        <p class="card-text">Dr. Vandana Sharma has successfully treated countless patients, who mainly complain about joint pain. She holds an experience of 4+ years and is serving as an Ayurvedic expert at Dr. Sharda Ayurveda. Her knowledge has effectively treated patients who have lost all hopes in getting recovered and were suggested for surgery as the last choice to get relief.</p>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                    <img class="card-img-top" src="{{ URL::asset('front/images/dr-dapinder-kaur.webp') }}" alt="Dr. Dapinder Kaur" title="Dr. Dapinder Kaur">
                        <div class="card-body">
                        <h4 class="card-title">Dr. Dapinder Kaur (BAMS)</h4>
                                        <p class="card-text">Dr. Dapinder Kaur has a graduate degree in Ayurvedic sciences. She believes in serving mankind by ending their suffering. With her medical and interpersonal skills, she not only provides Ayurvedic treatment to her patients but also educates them regarding the disorder. She is a great expert at Dr. Sharda Ayurveda and aims to make a difference with her intelligence.</p>
                                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="dsa-why-choose" id="testimonials">
    <div class="container-fluid">
        <h2 text align="center">Dr. Sharda Ayurveda Ludhiana- <strong>Patient Testimonial</strong></h2>
        <div class="row cut-row" >
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div id="dna_video">
                        <div class="youtube-player" data-id="OPjhs7F1U_Q" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/OPjhs7F1U_Q.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div> 
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div id="dna_video">
                        <div class="youtube-player" data-id="gYv-IX46VKI" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="{{ URL::asset('front/images/gYv-IX46VKI.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                            </div>
                        </div>
                    </div> 
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Patient's <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-female.webp') }}" class="img-fluid" alt="Harbhajan Kaur Testimonial" Title="Harbhajan Kaur Testimonial">
                            <h3 class="usrname">Aman Kaur</h3>
                            <p class="desig">Asthma Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was having asthma and was completely dependent on Inhalers. But after taking treatment from Dr. Sharda Ayurveda, Ludhiana, there is no need for me to use inhaler anymore. Thankyou doctors for curing my asthma. 
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Abhi Niket RA Patient Testimonial" title="Abhi Niket RA Patient Testimonial">
                            <h3 class="usrname">Abhi Niket</h3>
                            <p class="desig">Rheumatoid Arthritis Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Ayurvedic treatment of Rheumatoid arthritis from Dr. Sharda Ayurveda helped my father so much. He was in a very terrible state. Thankyou, Thankyou so much doctors!
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="{{ URL::asset('front/images/patient-testimonial-male.webp') }}" class="img-fluid" alt="Harsh Dhaliwal Testimonial" title="Harsh Dhaliwal Testimonial">
                            <h3 class="usrname">Harsh Dhaliwal</h3>
                            <p class="desig">Back Pain Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                My back pain was so severe that I was bedridden. I tried multiple doctors, but got no relief. Now, after consulting the doctors of Dr. Sharda Ayurveda, now I am all okay! Thanks.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test"><br>
                <h3 class="client-test-title" text align="center">Locate <b>Dr. Sharda Ayurveda Clinic in Ludhiana</b></h3>
            </div>
        </div>
                <br>
                <iframe width="100%" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13695.239949935843!2d75.8429933!3d30.8919761!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x72c134b600c1597b!2sDr.Sharda%20-%20Best%20Ayurvedic%20Clinic%20Ludhiana!5e0!3m2!1sen!2sin!4v1639999682799!5m2!1sen!2sin" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </div>
</section>

@include('../partials/frontend/form')

<script type="text/javascript">
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            responsiveClass: !0,
            navigation : true,
            autoplay: true,
            loop: false,
            smartSpeed: 2500,
            singleItem : true,
            navigationText: ["<i class='fa fa-angle-left fa-2'></i>", "<i class='fa fa-angle-right fa-2'></i>"],
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                480: {
                    items: 1,
                    nav: !1
                },
                768: {
                    items: 3,
                    nav: !1
                },
                1000: {
                    items: 4,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1024: {
                    items: 4,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1366: {
                    items: 3,
                    nav: !1
                }
            }
        })
    });

</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "LocalBusiness",
  "name": "DR. Sharda Ayurveda - Best Ayurvedic Clinic Ludhiana",
  "image": "https://www.drshardaayurveda.com/front/images/dr-sharda-logo.webp",
  "@id": "",
  "url": "https://www.drshardaayurveda.com/locations/ludhiana",
  "telephone": "+919876035500",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "opp. Suman Hospital, 562/L, Model Town",
    "addressLocality": "Ludhiana",
    "postalCode": "141002",
    "addressCountry": "IN"
  },
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": 30.8919761,
    "longitude": 75.8429933
  },
  "openingHoursSpecification": {
    "@type": "OpeningHoursSpecification",
    "dayOfWeek": [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
      "Sunday"
    ],
    "opens": "09:00",
    "closes": "18:00"
  },
  "sameAs": [
    "https://www.facebook.com/DrShardaAyurveda/",
    "https://twitter.com/shardaayurveda",
    "https://www.instagram.com/DrShardaAyurveda/",
    "https://www.youtube.com/c/DrShardaAyurveda/",
    "https://linkedin.com/company/dr-sharda-ayurveda",
    "https://in.pinterest.com/DrShardaAyurveda/",
    "https://www.drshardaayurveda.com/"
  ] 
}
</script>

@stop