<?php
/**
 * Ludhiana Page 
 * 
 * @created    31/10/2021
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2021
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Dr. Sharda Ayurveda -Ayurvedic Doctor/Clinic in Sri Muktsar Sahib</h3>
              <!--       <p style="font-size: 24px;">Protect Your Joints with Timely Treatment</p> -->
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/locations/muktsar">Sri Muktsar Sahib</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-md-12">
                <p>
                Are you searching for the best Ayurvedic clinic in Sri Muktsar Sahib for your treatment? Well, your search ends here as Dr. Sharda Ayurveda -Ayurvedic clinic in Sri Muktsar Sahib provides personalized Ayurvedic treatment so are you ready to experience the Importance of Ayurvedic treatment and how it can change your life and make you healthy. Then you should begin with the Ayurvedic treatment from Dr. Sharda Ayurveda- Ayurvedic hospital in Sri Muktsar Sahib and get your assessment done by our Ayurvedic doctors. This gives an Ayurvedic expert a detailed insight into your physical and mental health so they can create a personalized treatment just for your well-being.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/dr-sharda-ayurveda-muktsar.webp') }}" class="why-choose-us img-fluid" alt="About-us">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1">Ayurvedic Clinic in <b>Sri Muktsar Sahib</b></h1>
                <p>
                Dr. Sharda Ayurveda- Ayurvedic clinic in Sri Muktsar Sahib is the most preferred Ayurvedic clinic for treating all chronic diseases with just Ayurvedic medicines. We have a team of professional, experienced, and highly educated doctors in their specialized field who aims to make a patient disease and pain-free. Treatment for all the diseases at the Ayurvedic clinic in Sri Muktsar Sahib is cost-effective so that everyone can avail of the services. Ayurvedic medicines are safe to consume without any harmful effects on the body.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-md-12">
                <h6 class="heading1">How is Dr. Sharda Ayurveda Sri Muktsar Sahib clinic different and best from others?</h6>
                <ul>
                    <li>We believe in patient convenience so we have access to an easy online booking facility.</li>
                    <li>All the Ayurvedic medicines are manufactured under our guidance and are steroids free which are completely safe to be taken and do not harm the body.</li>
                    <li>Patients get full assistance from our patient advisor telephonically.</li>
                    <li>Ayurvedic clinic in Sri Muktsar Sahib is located in the heart of the city and the environment is all clean and hygienic.</li>
                </ul>
            </div>
        </div>
    </div>
</section>


<section class="dsa-india-ayurvedic-sec our-expert" id="types" style="margin-top: 3%;"> 
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="cl-test">
                <h2 class="client-test-title">Ayurvedic doctors in <b>Sri Muktsar Sahib</b></h2>
                <p>
                Dr. Sharda Ayurveda has the best team of top-performing Ayurvedic doctors near you in Sri Muktsar Sahib who are well-experienced. They have an excellent career history and are capable of treating critical stages of any health condition with Ayurvedic medicines. Doctors are dedicated to spending quality time with patients to understand and guide them. 
                <br />
            </div>
        </div>
         <div class="row cust-iba">
            <div class="owl-carousel owl-theme" >
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/Dr-Sunil-Chabra.webp') }}" class="card-img-top lazyload" alt="Dr. Sunil Chabbra (BAMS)">
                        <div class="card-body">
                            <h6 class="text-center">Dr. Sunil Chabbra (BAMS)</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12 text-center">
                                    <div class="tp-social-icon">
                                        <a href="https://www.facebook.com/DrShardaAyurveda" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="https://twitter.com/shardaayurveda" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="https://www.youtube.com/c/DrShardaAyurveda" target="_blank"><i class="fa fa-youtube"></i></a>
                                        <a href="https://www.linkedin.com/company/dr-sharda-ayurveda" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        <a href="https://www.instagram.com/DrShardaAyurveda/" target="_blank"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/Dr-Sarabhjot-Singh.webp') }}" class="card-img-top lazyload" alt="Dr Sarabhjot Singh (BAMS)">
                        <div class="card-body">
                            <h6 class="text-center">Dr. Sarabjot Singh (BAMS)</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12 text-center">
                                    <div class="tp-social-icon">
                                        <a href="https://www.facebook.com/DrShardaAyurveda" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="https://twitter.com/shardaayurveda" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="https://www.youtube.com/c/DrShardaAyurveda" target="_blank"><i class="fa fa-youtube"></i></a>
                                        <a href="https://www.linkedin.com/company/dr-sharda-ayurveda" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        <a href="https://www.instagram.com/DrShardaAyurveda/" target="_blank"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
<!--                 <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/Dr-Jeevan-Jyoti.webp') }}" class="card-img-top lazyload" alt="Dr. Jeevan Jyoti (BAMS)">
                        <div class="card-body">
                            <h6 class="text-center">Dr. Jeevan Jyoti (BAMS)</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12 text-center">
                                    <div class="tp-social-icon">
                                        <a href="https://www.facebook.com/DrShardaAyurveda" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="https://twitter.com/shardaayurveda" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="https://www.youtube.com/c/DrShardaAyurveda" target="_blank"><i class="fa fa-youtube"></i></a>
                                        <a href="https://www.linkedin.com/company/dr-sharda-ayurveda" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        <a href="https://www.instagram.com/DrShardaAyurveda/" target="_blank"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 -->
            </div>
        </div>
    </div>
</section>
<section>
    <h2 style="font-size: 24px;" text align="center">Locate Dr. Sharda Ayurveda Clinic in Sri Muktsar Sahib</h2>
                <iframe width="100%" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13755.998968835225!2d74.5138006!3d30.4644454!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xff5387c9197143da!2sDr.Sharda%20-%20Best%20Ayurvedic%20Clinic%20Muktsar%20Sahib!5e0!3m2!1sen!2sin!4v1640000727184!5m2!1sen!2sin" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</section>
@include('../partials/frontend/form')

<script type="text/javascript">
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            responsiveClass: !0,
            navigation : true,
            autoplay: true,
            loop: false,
            smartSpeed: 2500,
            singleItem : true,
            navigationText: ["<i class='fa fa-angle-left fa-2'></i>", "<i class='fa fa-angle-right fa-2'></i>"],
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                480: {
                    items: 1,
                    nav: !1
                },
                768: {
                    items: 3,
                    nav: !1
                },
                1000: {
                    items: 4,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1024: {
                    items: 4,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1366: {
                    items: 4,
                    nav: !1
                }
            }
        })
    });

</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "LocalBusiness",
  "name": "DR. Sharda Ayurveda - Best Ayurvedic Clinic Muktsar Sahib",
  "image": "https://www.drshardaayurveda.com/front/images/dr-sharda-logo.webp",
  "@id": "",
  "url": "https://www.drshardaayurveda.com/locations/muktsar",
  "telephone": "+91 9876035500",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "Ranjit Avenue, Sri Muktsar Sahib,",
    "addressLocality": "Muktsar",
    "postalCode": "152026",
    "addressCountry": "IN"
  },
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": 30.4644454,
    "longitude": 74.5138006
  },
  "openingHoursSpecification": {
    "@type": "OpeningHoursSpecification",
    "dayOfWeek": [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
      "Sunday"
    ],
    "opens": "09:00",
    "closes": "19:00"
  },
  "sameAs": [
    "https://www.facebook.com/DrShardaAyurveda/",
    "https://twitter.com/shardaayurveda",
    "https://www.instagram.com/DrShardaAyurveda/",
    "https://www.youtube.com/c/DrShardaAyurveda/",
    "https://linkedin.com/company/dr-sharda-ayurveda",
    "https://in.pinterest.com/DrShardaAyurveda/",
    "https://www.drshardaayurveda.com/"
  ] 
}
</script>

@stop