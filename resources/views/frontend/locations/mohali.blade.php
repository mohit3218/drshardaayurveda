<?php
/**
 * Mohali Page 
 * 
 * @created    31/10/2021
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2021
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>
@extends('layouts.main')
@section('content')
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Dr. Sharda Ayurveda - Mohali</h3>
              <!--       <p style="font-size: 24px;">Protect Your Joints with Timely Treatment</p> -->
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/locations/mohali">Mohali</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-md-12">
                <p>
                Ayurveda is a perfect blend of our modern lifestyle and our health-oriented habits with the ancient wisdom of using natural Herbs and Medicines that helps to lead a happy, healthy, and disease-free life. Ayurveda is the storehouse of herbs to treat all acute and chronic diseases. 13+ years of experience in Ayurveda and having a good success rate of treating patients worldwide motivated Dr. Mukesh Sharda to spread Ayurvedic clinics in different sections. Dr. Sharda Ayurveda- <strong style="font-weight: bold;">Ayurvedic clinic in Mohali </strong> is the pioneer of providing the best Ayurvedic medicines in Mohali. 
                </p>
            </div>
        </div>
    </div>
</section>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="{{ URL::asset('front/images/mohali-clinic.webp') }}" class="why-choose-us img-fluid" alt="Mohali Clinic">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1">Ayurvedic Clinic in <b>Mohali</b></h1>
                <p>
                Dr. Sharda Ayurveda- Ayurvedic clinic in Mohali is the most preferred Ayurvedic clinic for treating all acute and chronic diseases with just Ayurvedic medicines. A team of professional, experienced, and highly educated doctors whose motive is to make a patient disease and pain-free. Treatment for all the diseases at the Ayurvedic center in Mohali is cost-effective so that every patient can afford it. Ayurvedic medicines are safe to consume without any harmful effects on the body.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-md-12">
                <h6 class="heading1">How is Dr. Sharda Ayurveda Mohali clinic different and best from others?</h6>
                <ul>
                    <li>We believe in patient convenience so we have access to an easy online booking facility.</li>
                    <li>All the Ayurvedic medicines are manufactured under our guidance and are steroids free which are completely safe to be taken and do not harm the body. </li>
                    <li>Patients get full assistance from our patient advisor telephonically. </li>
                    <li>Ayurvedic clinic in Mohali is located in a neat and clean locality and has a team of helping staff members.</li>
                </ul>
            </div>
        </div>
    </div>
</section>


<section class="dsa-india-ayurvedic-sec our-expert" id="types" style="margin-top: 0%;"> 
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="cl-test">
                <h2 class="client-test-title">Approach Ayurvedic doctor in Mohali to get recovery from chronic disease naturally.</h2>
                <p>
                Ayurvedic team of doctors nearest you in Mohali tries to make the impossible possible by treating the most chronic diseases with just Ayurvedic medicines and therapy. The team of doctors is well qualified and highly experienced.<br> 
                To get rid of serious diseases book an appointment with the doctors of Dr. Sharda Ayurveda Mohali and get the best solution to your problems.</p>
                <br />
            </div>
        </div>
         <div class="row cust-iba">
            <div class="owl-carousel owl-theme" >
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/Dr-Raman.webp') }}" class="card-img-top lazyload" alt="Dr. Ramanjeet kaur (BAMS)">
                        <div class="card-body">
                            <h6 class="text-center">Dr. Ramanjeet Kaur (BAMS)</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12 text-center">
                                    <div class="tp-social-icon">
                                        <a href="https://www.facebook.com/DrShardaAyurveda" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="https://twitter.com/shardaayurveda" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="https://www.youtube.com/c/DrShardaAyurveda" target="_blank"><i class="fa fa-youtube"></i></a>
                                        <a href="https://www.linkedin.com/company/dr-sharda-ayurveda" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        <a href="https://www.instagram.com/DrShardaAyurveda/" target="_blank"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/DrVinodChauhan.webp') }}" class="card-img-top lazyload" alt="Dr Vinod Chauhan (BAMS)">
                        <div class="card-body">
                            <h6 class="text-center">Dr Vinod Chauhan (BAMS)</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12 text-center">
                                    <div class="tp-social-icon">
                                        <a href="https://www.facebook.com/DrShardaAyurveda" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="https://twitter.com/shardaayurveda" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="https://www.youtube.com/c/DrShardaAyurveda" target="_blank"><i class="fa fa-youtube"></i></a>
                                        <a href="https://www.linkedin.com/company/dr-sharda-ayurveda" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        <a href="https://www.instagram.com/DrShardaAyurveda/" target="_blank"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="{{ URL::asset('front/images/dr-nikita.webp') }}" class="card-img-top lazyload" alt="Dr. Nikita (BAMS)">
                        <div class="card-body">
                            <h6 class="text-center">Dr. Nikita (BAMS)</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12 text-center">
                                    <div class="tp-social-icon">
                                        <a href="https://www.facebook.com/DrShardaAyurveda" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="https://twitter.com/shardaayurveda" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="https://www.youtube.com/c/DrShardaAyurveda" target="_blank"><i class="fa fa-youtube"></i></a>
                                        <a href="https://www.linkedin.com/company/dr-sharda-ayurveda" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        <a href="https://www.instagram.com/DrShardaAyurveda/" target="_blank"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</section>
<section class="dsa-why-choose" id="testimonials">
    <div class="container-fluid">
        <h2 text align="center">Dr. Sharda Ayurveda Mohali - <strong>Patient Testimonial</strong></h2>
        <div class="row cut-row" >
           <!--  <div class="col-xl-6 col-md-12 col-sm-12">
                <div id="dna_video">
                    <div class="youtube-player" data-id="UAIIVsXq5Mo" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                        <img src="{{ URL::asset('front/images/UAIIVsXq5Mo.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                        <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div id="dna_video">
                    <div class="youtube-player" data-id="nltm4fE9Hzw" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                        <img src="{{ URL::asset('front/images/nltm4fE9Hzw.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                        <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div id="dna_video">
                    <div class="youtube-player" data-id="fa5a8gnSyck" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                        <img src="{{ URL::asset('front/images/fa5a8gnSyck.webp') }}" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                        <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('{{ URL::asset('front/images/video-icon.png') }}') no-repeat;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <br>
    <h2 style="font-size: 24px;" text align="center">Locate Dr. Sharda Ayurveda Clinic in Mohali</h2>
                <iframe width="100%" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13724.843003444832!2d76.7420606!3d30.6843459!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4ecb4163fc94fd5a!2sDr.Sharda%20-%20Best%20Ayurvedic%20Clinic%20Mohali!5e0!3m2!1sen!2sin!4v1640000477214!5m2!1sen!2sin" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</section>
@include('../partials/frontend/form')

<script type="text/javascript">
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            responsiveClass: !0,
            navigation : true,
            autoplay: true,
            loop: false,
            smartSpeed: 2500,
            singleItem : true,
            navigationText: ["<i class='fa fa-angle-left fa-2'></i>", "<i class='fa fa-angle-right fa-2'></i>"],
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                480: {
                    items: 1,
                    nav: !1
                },
                768: {
                    items: 3,
                    nav: !1
                },
                1000: {
                    items: 4,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1024: {
                    items: 4,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1366: {
                    items: 4,
                    nav: !1
                }
            }
        })
    });

</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "LocalBusiness",
  "name": "DR. Sharda Ayurveda- Best Ayurvedic Clinic Mohali",
  "image": "https://www.drshardaayurveda.com/front/images/dr-sharda-logo.webp",
  "@id": "",
  "url": "https://www.drshardaayurveda.com/locations/mohali",
  "telephone": "09876235500",
  "address": {
    "@type": "PostalAddress",
    "streetAddress": "Infront of army canteen, Sco 105, Phase 10, Sector 64, Sahibzada Ajit Singh Nagar,",
    "addressLocality": "Mohali",
    "postalCode": "160062",
    "addressCountry": "IN"
  },
  "geo": {
    "@type": "GeoCoordinates",
    "latitude": 30.6843459,
    "longitude": 76.7420606
  },
  "openingHoursSpecification": {
    "@type": "OpeningHoursSpecification",
    "dayOfWeek": [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
      "Sunday"
    ],
    "opens": "09:00",
    "closes": "19:00"
  },
  "sameAs": [
    "https://www.facebook.com/DrShardaAyurveda/",
    "https://twitter.com/shardaayurveda",
    "https://www.instagram.com/DrShardaAyurveda/",
    "https://www.youtube.com/c/DrShardaAyurveda/",
    "https://linkedin.com/company/dr-sharda-ayurveda",
    "https://in.pinterest.com/DrShardaAyurveda/",
    "https://www.drshardaayurveda.com/"
  ] 
}
</script>

@stop