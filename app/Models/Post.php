<?php

/**
* Post model
* 
* @created    29/11/2021
* @package    Dr Sharda Ayurveda
* @copyright  Copyright (C) 2021
* @license    Proprietary
* @author     Mohit Thakur
*/
namespace App\Models;

use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends AppModel {

    use Sortable,SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'post_image', 'post_title' , 'category' ,'post_contant' ,'meta_title', 'meta_keywords', 'meta_description', 'og_title', 'og_description', 'og_image', 'canonical_url', 'post_schema' , 'is_active', 'created_by'];

    /**
     * The attributes that should be mutated to deleted_at.
     *
     * @var array
     */
    protected $deleted_at = ['deleted_at'];

}
