<?php

/**
* Home Controller
* 
* @created    08/04/2020
* @package    Dr Sharda Ayurveda
* @copyright  Copyright (C) 2020
* @license    Proprietary
* @author     Mohit Thakur
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response;
use App\Models\Home;
use App\Models\Post;
use App\Models\User;
use App\Models\Payment;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;
use Artesaos\SEOTools\Facades\SEOTools;
use PHPMailer\PHPMailer\PHPMailer;  
use PHPMailer\PHPMailer\Exception;
use Mail;
use DB;
use Razorpay\Api\Api;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
    
     /*
    * Meta Data Set Dynamic
    */
    function paging_meta_data($meta_data)
    {
        SEOMeta::setTitle($meta_data['meta_title']);
        SEOMeta::setDescription($meta_data['meta_description']);
        if($meta_data['meta_keywords'] != '')
        {
            SEOMeta::setKeywords($meta_data['meta_keywords']);
        }
        SEOMeta::setCanonical($meta_data['canonical_url']);
        
        $meta_data['og_url'] = request()->fullUrl() . '/';
        
        if($meta_data['og_title']  != '' && $meta_data['og_description']  != '' && $meta_data['og_image']  != '')
        {
            OpenGraph::addProperty('article:publisher', 'https://www.facebook.com/DrShardaAyurveda/');
            OpenGraph::addProperty('type', 'articles');
            OpenGraph::setSiteName("drshardaayurveda.com");
            OpenGraph::setTitle($meta_data['og_title']);
            OpenGraph::setDescription($meta_data['og_description']);
            OpenGraph::setUrl($meta_data['og_url']);
            OpenGraph::addImage($meta_data['og_image']);


            TwitterCard::addValue("card", "summary_large_image");
            TwitterCard::setUrl($meta_data['og_url']);
            TwitterCard::setSite('drshardaayurveda.com');
            TwitterCard::setTitle($meta_data['og_title']);
            TwitterCard::setDescription($meta_data['og_description']);
            TwitterCard::addValue("image",$meta_data['og_image']);
            TwitterCard::addValue("creator","@drshardaayurveda");
        }
        return TRUE;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $home_page = Home::findOrFail(HOME_PAGE)->toArray();
        $this->paging_meta_data($home_page);
        $posts = Post::orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend.home.index', ['posts' => $posts]);
    }
    
    /**
     * Show the application about us page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function about_us()
    {
        $about_us = Home::findOrFail(ABOUT_US_PAGE)->toArray();
        $this->paging_meta_data($about_us);
        return view('frontend/about/index');
    }
    /**
     * Show the application Dr Mukesh Sharda page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dr_mukesh_sharda()
    {
        $dr_mukesh_sharda = Home::findOrFail(DR_MUKESH_SHARDA)->toArray();
        $this->paging_meta_data($dr_mukesh_sharda);
        return view('frontend/about/dr_mukesh_sharda');
    }
    /**
     * Show the application Dr Arjun Sharda page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dr_arjun_sharda()
    {
        $dr_arjun_sharda = Home::findOrFail(DR_ARJUN_SHARDA)->toArray();
        $this->paging_meta_data($dr_arjun_sharda);
        return view('frontend/about/dr_arjun_sharda');
    }
    /**
     * Show the application Contact us page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function contact_us()
    {
        $contact_us = Home::findOrFail(CONTACT_US)->toArray();
        $this->paging_meta_data($contact_us);
        return view('frontend/about/contact_us');
    }
    /**
     * Show the application Contact us page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function services()
    {
        $services = Home::findOrFail(SERVICES)->toArray();
        $this->paging_meta_data($services);
        return view('frontend/services/index');
    }
    /**
     * Show the application obesity page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function obesity()
    {
        $obesity = Home::findOrFail(OBESITY)->toArray();
        $this->paging_meta_data($obesity);
        $posts = Post::where("category", 1)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/obesity', ['posts' => $posts]);
    }
    /**
     * Show the application asthma page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function asthma()
    {
        $asthma = Home::findOrFail(ASTHMA)->toArray();
        $this->paging_meta_data($asthma);
        $posts = Post::where("category", 5)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/asthma', ['posts' => $posts]);
    }
    /**
     * Show the application diabetes page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function diabetes()
    {
        $diabetes = Home::findOrFail(DIABETES)->toArray();
        $this->paging_meta_data($diabetes);
        $posts = Post::where("category", 1)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/diabetes', ['posts' => $posts]);
    }
    /**
     * Show the application ibs page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function ibs()
    {
        $ibs = Home::findOrFail(IBS)->toArray();
        $this->paging_meta_data($ibs);
        $posts = Post::where("category", 2)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/ibs', ['posts' => $posts]);
    }
    /**
     * Show the application thyroid page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function thyroid()
    {
        $thyroid = Home::findOrFail(THYROID)->toArray();
        $this->paging_meta_data($thyroid);
        $posts = Post::where("category", 1)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/thyroid', ['posts' => $posts]);
    }
    /**
     * Show the application psoriasis page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function psoriasis()
    {
        $psoriasis = Home::findOrFail(PSORIASIS)->toArray();
        $this->paging_meta_data($psoriasis);
        $posts = Post::where("category", 3)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/psoriasis', ['posts' => $posts]);
    }
    /**
     * Show the application psoriasis page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function constipation()
    {
        $constipation = Home::findOrFail(CONSTIPATION)->toArray();
        $this->paging_meta_data($constipation);
        $posts = Post::where("category", 2)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/constipation', ['posts' => $posts]);
    }
    /**
     * Show the application rheumatoid_arthritis page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function rheumatoid_arthritis()
    {
        $rheumatoid_arthritis = Home::findOrFail(RHEUMATOID_ARTHRITIS)->toArray();
        $this->paging_meta_data($rheumatoid_arthritis);
        $posts = Post::where("category", 4)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/rheumatoid_arthritis', ['posts' => $posts]);
    }
    /**
     * Show the application gout page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function gout()
    {
        $gout = Home::findOrFail(GOUT)->toArray();
        $this->paging_meta_data($gout);
        $posts = Post::where("category", 4)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/gout', ['posts' => $posts]);
    }
    
    /**
     * Show the application osteoarthritis page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function osteoarthritis()
    {
        $osteoarthritis = Home::findOrFail(OSTEOARTHRITIS)->toArray();
        $this->paging_meta_data($osteoarthritis);
        $posts = Post::where("category", 4)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/osteoarthritis', ['posts' => $posts]);
    }
    
    /**
     * Show the application psoriatic_arthritis page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function psoriatic_arthritis()
    {
        $psoriatic_arthritis = Home::findOrFail(PSORIATIC_ARTHRITIS)->toArray();
        $this->paging_meta_data($psoriatic_arthritis);
        $posts = Post::where("category", 4)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/psoriatic_arthritis', ['posts' => $posts]);
    }
    /**
     * Show the application chronic_fatigue_syndrome page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function chronic_fatigue_syndrome()
    {
        $chronic_fatigue_syndrome = Home::findOrFail(CHRONIC_FATIGUE_SYNDROME)->toArray();
        $this->paging_meta_data($chronic_fatigue_syndrome);
        $posts = Post::where("category", 2)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/chronic_fatigue_syndrome', ['posts' => $posts]);
    }
    /**
     * Show the application reactive_arthritis page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function reactive_arthritis()
    {
        $reactive_arthritis = Home::findOrFail(REACTIVE_ARTHRITIS)->toArray();
        $this->paging_meta_data($reactive_arthritis);
        $posts = Post::where("category", 4)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/reactive_arthritis', ['posts' => $posts]);
    }
    /**
     * Show the application ankylosing_spondylitis page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function ankylosing_spondylitis()
    {
        $ankylosing_spondylitis = Home::findOrFail(ANKYLOSING_SPONDYLITIS)->toArray();
        $this->paging_meta_data($ankylosing_spondylitis);
        $posts = Post::where("category", 4)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/ankylosing_spondylitis', ['posts' => $posts]);
    }
    /**
     * Show the application erectile_dysfunction page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function erectile_dysfunction()
    {
        $erectile_dysfunction = Home::findOrFail(ERECTILE_DYSFUNCTION)->toArray();
        $this->paging_meta_data($erectile_dysfunction);
        $posts = Post::where("category", 6)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/erectile_dysfunction', ['posts' => $posts]);
    }
    /**
     * Show the application neck_pain page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function neck_pain()
    {
        $neck_pain = Home::findOrFail(NECK_PAIN)->toArray();
        $this->paging_meta_data($neck_pain);
        $posts = Post::where("category", 4)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/neck_pain', ['posts' => $posts]);
    }
    /**
     * Show the application acidity page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function acidity()
    {
        $acidity = Home::findOrFail(ACIDITY)->toArray();
        $this->paging_meta_data($acidity);
        $posts = Post::where("category", 2)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/acidity', ['posts' => $posts]);
    }
    /**
     * Show the application anemia page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function anemia()
    {
        $anemia = Home::findOrFail(ANEMIA)->toArray();
        $this->paging_meta_data($anemia);
        $posts = Post::where("category", 11)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/anemia', ['posts' => $posts]);
    }
    /**
     * Show the application back_pain page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function back_pain()
    {
        $back_pain = Home::findOrFail(BACK_PAIN)->toArray();
        $this->paging_meta_data($back_pain);
        $posts = Post::where("category", 4)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/back_pain', ['posts' => $posts]);
    }
    /**
     * Show the application back_pain page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function cervical_sponylosis()
    {
        $cervical_sponylosis = Home::findOrFail(CERVICAL_SPONYLOSIS)->toArray();
        $this->paging_meta_data($cervical_sponylosis);
        $posts = Post::where("category", 4)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/cervical_sponylosis', ['posts' => $posts]);
    }
    /**
     * Show the application de_aaddiction_alcohol page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function de_aaddiction_alcohol()
    {
        $de_aaddiction_alcohol = Home::findOrFail(DE_AADDICTION_ALCOHOL)->toArray();
        $this->paging_meta_data($de_aaddiction_alcohol);
        $posts = Post::where("category", 7)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/de_aaddiction_alcohol', ['posts' => $posts]);
    }
    /**
     * Show the application drug_de_addiction page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function drug_de_addiction()
    {
        $drug_de_addiction = Home::findOrFail(DRUG_DE_ADDICTION)->toArray();
        $this->paging_meta_data($drug_de_addiction);
        $posts = Post::where("category", 7)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/drug_de_addiction', ['posts' => $posts]);
    }
    /**
     * Show the application fibro myalagia page.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function fibro_myalagia()
    {
        $fibro_myalagia = Home::findOrFail(FIBRO_MYALAGIA)->toArray();
        $this->paging_meta_data($fibro_myalagia);
        $posts = Post::where("category", 4)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/fibro_myalagia', ['posts' => $posts]);
    }
    /**
    * Show the application flatulence page.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function flatulence()
    {
        $flatulence = Home::findOrFail(FLATULENCE)->toArray();
        $this->paging_meta_data($flatulence);
        $posts = Post::where("category", 2)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/flatulence', ['posts' => $posts]);
    }
    /**
    * Show the application flatulence page.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function hip_joint_pain()
    {
        $hip_joint_pain = Home::findOrFail(HIP_JOINT_PAIN)->toArray();
        $this->paging_meta_data($hip_joint_pain);
        $posts = Post::where("category", 4)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/hip_joint_pain', ['posts' => $posts]);
    }
    
    /**
    * Show the application kidney failure page.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function kidney_failure()
    {
        $kidney_failure = Home::findOrFail(KIDNEY_FAILURE)->toArray();
        $this->paging_meta_data($kidney_failure);
        $posts = Post::where("category", 8)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/kidney_failure', ['posts' => $posts]);
    }
    
    /**
    * Show the application lumbar_spondylosis failure page.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function lumbar_spondylosis()
    {
        $lumbar_spondylosis = Home::findOrFail(LUMBAR_SPONDYLOSIS)->toArray();
        $this->paging_meta_data($lumbar_spondylosis);
        $posts = Post::where("category", 4)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/lumbar_spondylosis', ['posts' => $posts]);
    }
    
    /**
    * Show the application osteoporosis failure page.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function osteoporosis()
    {
        $osteoporosis = Home::findOrFail(OSTEOPOROSIS)->toArray();
        $this->paging_meta_data($osteoporosis);
        $posts = Post::where("category", 4)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/osteoporosis', ['posts' => $posts]);
    }
    
    /**
    * Show the application osteoporosis failure page.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function piles()
    {
        $piles = Home::findOrFail(PILES)->toArray();
        $this->paging_meta_data($piles);
        $posts = Post::where("category", 2)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/piles', ['posts' => $posts]);
    }
    
    /**
    * Show the application pre_mature_ejaculation failure page.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function pre_mature_ejaculation()
    {
        $pre_mature_ejaculation = Home::findOrFail(PRE_MATURE_EJACULATION)->toArray();
        $this->paging_meta_data($pre_mature_ejaculation);
        $posts = Post::where("category", 6)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/pre_mature_ejaculation', ['posts' => $posts]);
    }
    
    /**
    * Show the application sciatica pain failure page.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function sciatica_pain()
    {
        $sciatica_pain = Home::findOrFail(SCIATICA_PAIN)->toArray();
        $this->paging_meta_data($sciatica_pain);
        $posts = Post::where("category", 9)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/sciatica_pain', ['posts' => $posts]);
    }
    
    /**
    * Show the application uric_acid failure page.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function uric_acid()
    {
        $uric_acid = Home::findOrFail(URIC_ACID)->toArray();
        $this->paging_meta_data($uric_acid);
        $posts = Post::where("category", 4)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/uric_acid', ['posts' => $posts]);
    }
    /**
    * Show the application knee_pain failure page.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function knee_pain()
    {
        $knee_pain = Home::findOrFail(KNEE_PAIN)->toArray();
        $this->paging_meta_data($knee_pain);
        $posts = Post::where("category", 4)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/knee_pain', ['posts' => $posts]);
    }
    
    /**
    * Show the application abdominal_pain failure page.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function abdominal_pain()
    {
        $abdominal_pain = Home::findOrFail(ABDOMINAL_PAIN)->toArray();
        $this->paging_meta_data($abdominal_pain);
        $posts = Post::where("category", 2)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/abdominal_pain', ['posts' => $posts]);
    }
    /**
    * Show the application menopause failure page.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function menopause()
    {
        $menopause = Home::findOrFail(MENOPAUSE)->toArray();
        $this->paging_meta_data($menopause);
        $posts = Post::where("category", 10)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/menopause', ['posts' => $posts]);
    }
    /**
    * Show the application leucorrhoea failure page.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function leucorrhoea()
    {
        $menopause = Home::findOrFail(LEUCORRHOEA)->toArray();
        $this->paging_meta_data($menopause);
        $posts = Post::where("category", 10)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/leucorrhoea', ['posts' => $posts]);
    }
    /**
    * Show the application sinusitis failure page.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function sinusitis()
    {
        $sinusitis = Home::findOrFail(SINUSITIS)->toArray();
        $this->paging_meta_data($sinusitis);
        $posts = Post::where("category", 5)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/sinusitis', ['posts' => $posts]);
    }
    /**
    * Show the application kidney_stones failure page.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function kidney_stones()
    {
        $kidney_stones = Home::findOrFail(KIDNEY_STONES)->toArray();
        $this->paging_meta_data($kidney_stones);
        $posts = Post::where("category", 8)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/kidney_stones', ['posts' => $posts]);
    }
    /**
    * Show the application depression failure page.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function depression()
    {
        $depression = Home::findOrFail(DEPRESSION)->toArray();
        $this->paging_meta_data($depression);
        $posts = Post::where("category", 9)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/depression', ['posts' => $posts]);
    }
    /**
    * Show the application pcod_pcos failure page.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function pcod_pcos()
    {
        $pcod_pcos = Home::findOrFail(PCOD_PCOS)->toArray();
        $this->paging_meta_data($pcod_pcos);
        $posts = Post::where("category", 10)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/pcod_pcos', ['posts' => $posts]);
    }
    /**
    * Show the application oligospermia failure page.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function oligospermia()
    {
        $oligospermia = Home::findOrFail(OLIGOSPERMIA)->toArray();
        $this->paging_meta_data($oligospermia);
        $posts = Post::where("category", 6)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/oligospermia', ['posts' => $posts]);
    }
    /**
    * Show the application fatty_liver failure page.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function fatty_liver()
    {
        $fatty_liver = Home::findOrFail(FATTY_LIVER)->toArray();
        $this->paging_meta_data($fatty_liver);
        $posts = Post::where("category", 12)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/fatty_liver', ['posts' => $posts]);
    }
    /**
    * Show the application privacy_policy failure page.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function privacy_policy()
    {
        $privacy_policy = Home::findOrFail(PRIVACY_POLICY)->toArray();
        $this->paging_meta_data($privacy_policy);
        return view('frontend/about/privacy_policy');
    }

    public function refreshCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }

    public function lead_submission(Request $request){
        $data = $request->all();
        //dd($data);
        $subject = $data['msubject'] . ' Lead Enquiry';
        $page_url = $data['mpage_url'];
        $name = $data['mname'];
        $mobile = $data['mmobile'];
        $email = (isset($data['memail']) && $data['memail'] != NULL ) ? $data['memail'] : '';
        $description = (isset($data['mdescription']) && $data['mdescription'] != NULL) ? $data['mdescription'] : '';
        //$captcha = $data['captcha'];

        $mail = new PHPMailer;
        $mail1 = new PHPMailer;

        if($name == ''){
            return array('message' => 'Name is required!!', 'status' => 'failed');
        }if($mobile == ''){
            return array('message' => 'mobile is required!!', 'status' => 'failed');
        }/*if($captcha == ''){
           // return array('message' => 'captcha is required!!', 'status' => 'failed');
        }

        $rules = ['captcha' => 'required|captcha'];
        $validator = validator()->make(request()->all(), $rules);
        if ($validator->fails()) {
            return array('message' => 'captcha mismatch!!', 'status' => 'failed');
        }*/

        /*
        $mail1->isSMTP(); 
        $mail1->Host        = 'smtp.gmail.com'; 
        $mail1->SMTPAuth = true; 
        $mail1->Username = 'dev032692@gmail.com'; 
        $mail1->Password = '987hellojack'; 
        $mail1->SMTPSecure = 'tls'; 
        $mail1->Port        = 587;
        */
        /*$mail->isSMTP(); 
        $mail->Host        = 'mail.drshardaayurveda.com'; 
        $mail->SMTPAuth = true; 
        $mail->Username = 'info@drshardaayurveda.com'; 
        $mail->Password = 'pEy}ppBaA2zQ'; 
        $mail->SMTPSecure = 'ssl'; 
        $mail->Port        = 465;*/

        //Server SMTP Access
        $mail->isMail(); 
        $mail->Host        = 'smtp.gmail.com';
        $mail->SMTPAuth = true; 
        $mail->Username = 'digital.shardamedilifeayurveda@gmail.com'; 
        $mail->Password = '@987hellojack'; 
        $mail->SMTPSecure = 'tls'; 
        $mail->Port        = 587;

        $from_email = 'digital.shardamedilifeayurveda@gmail.com';
        $toemail = 'digital.shardamedilifeayurveda@gmail.com';
        //$from_email = 'dev032692@gmail.com';

        // Sender info  
        $mail->setFrom($from_email, 'Dr Sharda Ayurveda'); 
        
        // Add a recipient  
        $mail->addAddress($toemail);  
        
        // Email subject  
        $mail->Subject = $subject;  
        
        // Set email format to HTML  
        $mail->isHTML(true);  
        
        // Email body content  
        $mailContent = "
            <html>
                <head>
                    <title>Booking Enquiry</title>
                </head>
                <body>
                    <p>
                        Thanks for Showing Interest in our Services. One of Executive will contact you ASAP.
                         Please feel free to call us on +91- 9876035500.
                    </p>
                    <table>
                        <tr>
                            <th>Name</th>
                            <td>" . $name . "</td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>" . $email . "</td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <td>" . $mobile . "</td>
                        </tr>
                        <tr>
                            <th>Lead Page</th>
                            <td>" . $page_url . "</td>
                        </tr>
                        <tr>
                            <th>Description</th>
                            <td>" . $description . "</td>
                        </tr>
                    </table>
                </body>
            </html>"; 

        $mail->Body = $mailContent;  
        
        // Send email  
        if(!$mail->send()){  
            return array('message' => 'Message could not be sent. Mailer Error: '.$mail->ErrorInfo, 'status' => 'failed');
        }else{  
            return array('message' => 'Your Booking Enquiry Successfully Submit. We will contact you shortly' , 'status' => 'success');
        }
    }

    /**
    * Show the application Ludhiana failure page.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function ludhiana()
    {
        $ludhiana = Home::findOrFail(LUDHIANA)->toArray();
        $this->paging_meta_data($ludhiana);
        return view('frontend/locations/ludhiana');
    }

     /**
    * Show the application mohali failure page.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function mohali()
    {
        $mohali = Home::findOrFail(MOHALI)->toArray();
        $this->paging_meta_data($mohali);
        return view('frontend/locations/mohali');
    }

     /**
    * Show the application muktsar failure page.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function muktsar()
    {
        $muktsar = Home::findOrFail(MUKTSAR)->toArray();
        $this->paging_meta_data($muktsar);
        return view('frontend/locations/muktsar');
    }


    /**
    * Show the application bathinda failure page.
    *
    * @return \Illuminate\Contracts\Support\Renderable
    */
    public function bathinda()
    {
        $bathinda = Home::findOrFail(BATHINDA)->toArray();
        $this->paging_meta_data($bathinda);
        return view('frontend/locations/bathinda');
    }

    public function success_stories(){
        $success_stories = Home::findOrFail(SUCCESS_STORIES)->toArray();
        $this->paging_meta_data($success_stories);
        return view('frontend/about/success_stories');
    }

    public function blogs(){
        $blogs = Home::findOrFail(BLOGS)->toArray();
        $posts = Post::sortable()->paginate(5);
        $users = User::all()->pluck("name", "id")->toArray();
        $records = Post::orderBy('id', 'desc')->get();
        $this->paging_meta_data($blogs);
        return view('frontend.blogs.index', ['records' => $records, 'posts' => $posts, 'users' => $users]);
    }

    public function category_blogs(){
        $category_title = str_replace("-"," ", strtolower(request()->segment(3)));
        $categories = array_flip(\StaticArrays::$post_categories);
        $category_id = $categories[ucwords("$category_title")];
        $records = Post::select("*")->where("category", $category_id)->orderBy('id', 'desc')->get();
        $users = User::all()->pluck("name", "id")->toArray();
        $posts = Post::sortable()->paginate(5);
        return view('frontend.blogs.index', ['records' => $records, 'posts' => $posts, 'users' => $users]);
    }

    public function post(){
        $post_title = str_replace("-"," ", strtolower(request()->segment(2)));
        $records = Post::where([['name', 'like', '%' . $post_title . '%']])->first()->toArray();
        $this->paging_meta_data($records);
        $users = User::all()->pluck("name", "id")->toArray();
        $posts = Post::orderBy('id', 'desc')->skip(0)->take(5)->get()->toArray();

        $shareComponent = \Share::page(
            url()->current(),
        )
        ->facebook()
        ->twitter()
        ->linkedin()
        ->telegram()
        ->whatsapp()        
        ->reddit();

        $related_posts = Post::where("category", $records['category'])->orderBy('id', 'desc')->skip(0)->take(5)->get()->toArray();
        //dd($records);
        return view('frontend.blogs.post', ['records' => $records, 'posts' => $posts, 'users' => $users, 'related_posts' => $related_posts, 'shareComponent' => $shareComponent]);
    }

    public function smoking(){
        $smoking = Home::findOrFail(SMOKING)->toArray();
        $this->paging_meta_data($smoking);
        $posts = Post::where("category", 7)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/smoking', ['posts' => $posts]);
    }

    public function jaundice(){
        $jaundice = Home::findOrFail(JAUNDICE)->toArray();
        $this->paging_meta_data($jaundice);
        $posts = Post::where("category", 12)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/jaundice', ['posts' => $posts]);
    }

    public function urinary_tract_infection(){
        $urinary_tract_infection = Home::findOrFail(URINARY_TRACT_INFECTION)->toArray();
        $this->paging_meta_data($urinary_tract_infection);
        $posts = Post::where("category", 13)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/urinary_tract_infection', ['posts' => $posts]);
    }

    public function fungal_infection(){
        $fungal_infection = Home::findOrFail(FUNGAL_INFECTION)->toArray();
        $this->paging_meta_data($fungal_infection);
        $posts = Post::where("category", 3)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/fungal_infection', ['posts' => $posts]);
    }

    public function liver_cirrhosis(){
        $liver_cirrhosis = Home::findOrFail(LIVER_CIRRHOSIS)->toArray();
        $this->paging_meta_data($liver_cirrhosis);
        $posts = Post::where("category", 12)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/liver_cirrhosis', ['posts' => $posts]);
    }

    //Skin
    public function eczema(){
        $eczema = Home::findOrFail(ECZEMA)->toArray();
        $this->paging_meta_data($eczema);
        $posts = Post::where("category", 3)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/eczema', ['posts' => $posts]);
    }

    public function post_viral_arthritis(){
        $post_viral_arthritis = Home::findOrFail(POST_VIRAL_ARTHRITIS)->toArray();
        $this->paging_meta_data($post_viral_arthritis);
        $posts = Post::where("category", 4)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/post_viral_arthritis', ['posts' => $posts]);
    }

    public function migraine(){
        $migraine = Home::findOrFail(MIGRAINE)->toArray();
        $this->paging_meta_data($migraine);
        $posts = Post::where("category", 9)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/migraine', ['posts' => $posts]);
    }

    public function insomnia(){
        $insomnia = Home::findOrFail(INSOMNIA)->toArray();
        $this->paging_meta_data($insomnia);
        $posts = Post::where("category", 9)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/insomnia', ['posts' => $posts]);
    }

    public function anxiety_disorder(){
        $anxiety_disorder = Home::findOrFail(ANXIETY_DISORDER)->toArray();
        $this->paging_meta_data($anxiety_disorder);
        $posts = Post::where("category", 9)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/anxiety_disorder', ['posts' => $posts]);
    }

    public function ulcerative_colitis(){
        $ulcerative_colitis = Home::findOrFail(ULCERATIVE_COLITIS)->toArray();
        $this->paging_meta_data($ulcerative_colitis);
        $posts = Post::where("category", 2)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/ulcerative_colitis', ['posts' => $posts]);
    }

    public function rhinitis(){
        $rhinitis = Home::findOrFail(RHINITIS)->toArray();
        $this->paging_meta_data($rhinitis);
        $posts = Post::where("category", 5)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/rhinitis', ['posts' => $posts]);
    }

    public function chronic_laryngitis(){
        $chronic_laryngitis = Home::findOrFail(CHRONIC_LARYNGITIS)->toArray();
        $this->paging_meta_data($chronic_laryngitis);
        $posts = Post::where("category", 5)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/chronic_laryngitis', ['posts' => $posts]);
    }

    public function blood_pressure(){
        $blood_pressure = Home::findOrFail(BLOOD_PRESSURE)->toArray();
        $this->paging_meta_data($blood_pressure);
        $posts = Post::where("category", 11)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/blood_pressure', ['posts' => $posts]);
    }

    public function high_cholestrol(){
        $high_cholestrol = Home::findOrFail(HIGH_CHOLESTEROL)->toArray();
        $this->paging_meta_data($high_cholestrol);
        $posts = Post::where("category", 11)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/high_cholestrol', ['posts' => $posts]);
    }

    public function dr_mukesh_sharda_author(){

        $dr_mukesh_sharda_author = Home::findOrFail(DR_MUKESH_SHARDA_AUTHOR)->toArray();
        $this->paging_meta_data($dr_mukesh_sharda_author);
        $users = User::all()->pluck("name", "id")->toArray();
        $records = Post::where([['created_by', 3]])->get()->toArray();
        return view('frontend.blogs.author', ['records' => $records, 'users' => $users, 'author_name' => 'Dr. Mukesh Sharda']);
    }

    public function dr_sharda_author(){
        
        $dr_sharda_author = Home::findOrFail(DR_SHARDA_AUTHOR)->toArray();
        $this->paging_meta_data($dr_sharda_author);

        $users = User::all()->pluck("name", "id")->toArray();
        $records = Post::where([['created_by', 1]])->get()->toArray();
        return view('frontend.blogs.author ', ['records' => $records, 'users' => $users, 'author_name' => 'Dr. Sharda Ayurveda']);

    }

    public function coronary_artery_disease(){
        $coronary_artery_disease = Home::findOrFail(CORONARY_ARTERY_DISEASE)->toArray();
        $this->paging_meta_data($coronary_artery_disease);
        $posts = Post::where("category", 11)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/coronary_artery_disease', ['posts' => $posts]);
    }

    public function cystitis(){
        $cystitis = Home::findOrFail(CYSTITIS)->toArray();
        $this->paging_meta_data($cystitis);
        $posts = Post::where("category", 13)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/cystitis', ['posts' => $posts]);
    }

    public function important_disclaimer(){
        $important_disclaimer = Home::findOrFail(IMPORTANT_DISCLAIMER)->toArray();
        $this->paging_meta_data($important_disclaimer);
        $posts = Post::where("category", 11)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/about/important_disclaimer', ['posts' => $posts]);
    }

    public function consultation(){
        $consultation = Home::findOrFail(ONLINE_CONSULTATION)->toArray();
        $this->paging_meta_data($consultation);
        //$posts = Post::where("category", 11)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/about/consultation');
    }

    public function shipping_policy()
    {
        $shipping_policy = Home::findOrFail(SHIPPING_POLICY)->toArray();
        $this->paging_meta_data($shipping_policy);
        return view('frontend/about/shipping_policy');
    }

    public function search(Request $request){
        // Get the search value from the request
        $search = $request->input('search');
    
        // Search in the title and body columns from the posts table
        $records = Post::query()
            ->where('name', 'LIKE', "%{$search}%")
            ->get();
            
           // $users = User::all()->pluck("name", "id")->toArray();
    
        // Return the search view with the resluts compacted
        //return view('frontend/blogs/index', compact('posts'));


        //$blogs = Home::findOrFail(BLOGS)->toArray();
        $posts = Post::sortable()->paginate(5);
        $users = User::all()->pluck("name", "id")->toArray();
        //$records = Post::orderBy('id', 'desc')->get();
       // $this->paging_meta_data($blogs);
        return view('frontend.blogs.index', ['records' => $records, 'posts' => $posts, 'users' => $users]);
    }
    
    public function wellness_program()
    {
        $wellness_program = Home::findOrFail(WELLNESS_PROGRAM)->toArray();
        $this->paging_meta_data($wellness_program);
        return view('frontend/wellness_program');
    }
    
    public function razorpay_payment(Request $request)
    {
        $input = $request->all();
        $api = new Api (env('RAZORPAY_KEY'), env('RAZORPAY_SECRET'));
        $payment = $api->payment->fetch($input['razorpay_payment_id']);
        if(count($input) && !empty($input['razorpay_payment_id'])) {
            try {
                $response = $api->payment->fetch($input['razorpay_payment_id'])->capture(array('amount' => $payment['amount']));
                $payment = Payment::create([
                    'r_payment_id' => $response['id'],
                    'method' => $response['method'],
                    'currency' => $response['currency'],
                    'user_email' => $response['email'],
                    'amount' => $response['amount']/100,
                    'json_response' => json_encode((array)$response)
                ]);
            } catch(Exceptio $e) {
                return $e->getMessage();
                \Session::put('error',$e->getMessage());
                return redirect()->back();
            }
        }
        \Session::put('success',('Payment Successful'));
        return redirect()->back();
    }

    public function types_of_eczema()
    {
        //TYPES_OF_ECZEMA
        $types_of_eczema = Home::findOrFail(CONTACT_US)->toArray();
        $this->paging_meta_data($types_of_eczema);
        $posts = Post::where("category", 5)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/types_of_eczema', ['posts' => $posts]);
    }

    public function inverse_psoriasis_treatment()
    {
        //INVERSE_PSORIASIS_TREATMENT
        $inverse_psoriasis_treatment = Home::findOrFail(CONTACT_US)->toArray();
        $this->paging_meta_data($inverse_psoriasis_treatment);
        $posts = Post::where("category", 5)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/inverse_psoriasis_treatment', ['posts' => $posts]);
    }

    public function stasis_dermatitis()
    {
        //STASIS_DERMATITIS
        $stasis_dermatitis = Home::findOrFail(CONTACT_US)->toArray();
        $this->paging_meta_data($stasis_dermatitis);
        $posts = Post::where("category", 5)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/stasis_dermatitis', ['posts' => $posts]);
    }

    public function contact_dermatitis()
    {
        //CONTACT_DERMATITIS
        $contact_dermatitis = Home::findOrFail(CONTACT_US)->toArray();
        $this->paging_meta_data($contact_dermatitis);
        $posts = Post::where("category", 5)->orderBy('id', 'desc')->skip(0)->take(3)->get()->toArray();
        return view('frontend/services/contact_dermatitis', ['posts' => $posts]);
    }
}