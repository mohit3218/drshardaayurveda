<?php

/**
* Post Controller
* 
* @created    29/11/2021
* @package    Dr Sharda Ayurveda
* @copyright  Copyright (C) 2021
* @license    Proprietary
* @author     Mohit Thakur
*/

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Request;
use DB;
use Validator;

class PostController extends AppController {

    // Set for public Model name
    public $modelName = "Post";
    //Define validation rules for the request
    public $rules = array(
        'name' => 'required'
    );

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $conditions = $this->getSearchConditions([
            ["view_field" => "name", "type" => "string"],
        ]);

        if ($conditions) {
            //Paginate page according to search filter
            $records = Post::sortable()->whereRaw($conditions)->paginate(PAGINATION_LIMIT);
        } else {
            //Paginate Post without search filter
            $records = Post::All();
        }

        return view('admin.posts.index', ['records' => $records]);
        //return view('admin.page.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        
        $users = User::get();
        //Return Page Add form
        return view('admin.posts.create', ['users' => $users]);
    }

    /**
     * Adds new record
     * @return type
     */
    public function store($route = NULL) {
        //rote set for redirect the after save posts summary

        $data = request()->all();
        $graphic_rules = array(
			'post_image' => 'image|required|mimes:jpeg,png,jpg,gif,svg,webp'
		);
        $route = "admin/posts";

        //Use laravel Validator helper and $this->rule is represented the which input field you have to mandatory and numaric
        $validator = Validator::make(request()->all(), $graphic_rules);

        if ($validator->passes())
		{
			try 
			{
                $post = new Post();
                $post->name = $data['name'];
                $post->meta_title = $data['meta_title'];
                $post->meta_keywords = $data['meta_keywords'];
                $post->meta_description = $data['meta_description'];
                $post->post_title = $data['post_title'];
                $post->category = $data['category'];
                $post->post_contant = $data['post_contant'];
                $post->og_title = $data['og_title'];
                $post->og_description = $data['og_description'];
                $post->og_image = $data['og_image'];
                $post->canonical_url = $data['canonical_url'];
                $post->post_schema = isset($data['post_schema']) ? $data['post_schema'] : '';
                $post->is_active = 1;
                $post->created_by = $data['created_by'];
                
				//get all post file data
				$uploadImg = request()->file('post_image');
				//Get a file name
				$imagefilename  = $uploadImg->getClientOriginalName();
				//call a commonn fuction for save and resize image
				$post->post_image = $this->resizeAndUploadImage(request(), 'post_image', GRAPHIC_IMAGES . '/', $imagefilename, 350, 350);
			}
			catch(NotReadableException $e)
			{
				return back()->withFailure("$this->modelName Failed to save record")->withErrors($validator)->withInput(request()->all());
			}

            if ($post->save())
            {
                return redirect($route)->withSuccess("$this->modelName Record saved successfully");
            }
		}
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //Get a records in page table
        $post = Post::findOrFail($id);
        //Set id and records to view
        $users = User::get();
        return view('admin.posts.create', compact("id", "post", 'users'));
    }

    /**
     * Update the specified resource in storage.
     * @param Slug $slug
     * @return type
     */
    public function update(Post $post, $route = NULL) {
        $route = "admin/posts";

        $data = request()->all();
        $image = '';
        if(request()->file('post_image') != null){
            $image = ['post_image' => 'image|required|mimes:jpeg,png,jpg,gif,svg,webp'];
        }
        $graphic_rules = array(
			$image
		);

        //Use laravel Validator helper and $this->rule is represented the which input field you have to mandatory and numaric
        $validator = Validator::make(request()->all(), $graphic_rules);
        
        if ($validator->passes())
		{
			try 
			{
                $post->name = $data['name'];
                $post->meta_title = $data['meta_title'];
                $post->meta_keywords = $data['meta_keywords'];
                $post->meta_description = $data['meta_description'];
                $post->post_title = $data['post_title'];
                $post->category = $data['category'];
                $post->post_contant = $data['post_contant'];
                $post->og_title = $data['og_title'];
                $post->og_description = $data['og_description'];
                $post->og_image = $data['og_image'];
                $post->canonical_url = $data['canonical_url'];
                $post->post_schema = isset($data['post_schema']) ? $data['post_schema'] : '';
                $post->is_active = 1;
                $post->created_by = $data['created_by'];
                
                if(request()->file('post_image') != null){
                    //get all post file data
                    $uploadImg = request()->file('post_image');
                    
                    //Get a file name
                    $imagefilename  = $uploadImg->getClientOriginalName();
                    //call a commonn fuction for save and resize image
                    $post->post_image = $this->resizeAndUploadImage(request(), 'post_image', GRAPHIC_IMAGES . '/', $imagefilename, 350, 350);
                }else{
                    $post->post_image = $post->post_image;
                }
				
			}
			catch(NotReadableException $e)
			{
                dd($e);
				return back()->withFailure("$this->modelName Failed to save record")->withErrors($validator)->withInput(request()->all());
			}

            if ($post->save())
            {
                return redirect($route)->withSuccess("$this->modelName Record updated successfully");
            }
		}
    }

    /**
     * Deletes record
     * @param Contact $contact
     * @return type
     */
    public function destroy(Post $post) {
        //Destroy function call the show method
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post) {
        //rote set for redirect the after save pages summary
        $route = "admin/posts";

        //Call a destroy action in main controller to delete a record soft delete
        return parent::delete_record($post, $route);
    }

    /**
     * Status update Existing Record
     * @param type $id , $status
     */
    public function admin_toggleStatus(Post $post, $is_active) {
        //rote set for redirect the after save pages summary
        $route = "admin/pages";

        //Call a destroy action in main controller to delete a record soft delete
        return parent::toggleStatus($post, $is_active, $route);
    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        if($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
        
            $request->file('upload')->move(public_path('images'), $fileName);
   
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('images/'.$fileName); 
            $msg = 'Image uploaded successfully'; 
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
               
            @header('Content-type: text/html; charset=utf-8'); 
            echo $response;
        }
    }

}
