<?php
/**
 * Page Add/Edit Form 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>

<?php $__env->startSection('content'); ?>

<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Page Form</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Page Form</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<?php echo $__env->make('../partials/message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Summary</h3>
                    </div>
                    <!-- /.card-header -->

                    <div class="card-body">
                        <table class="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th> SR No. </th>
                                    <th> Name </th>
                                    <th> Meta Title </th>
                                    <th> Meta Keyword </th>
                                    <th> Meta Description </th>
                                    <th> OG Title </th>
                                    <th> OG Description </th>
                                    <th> OG Image </th>
                                    <th> Canonical URL </th>
                                    <th> Is Active </th>
                                    <th> Actions </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                <?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr class="odd gradeX">
                                    <td> <?php echo e($record->id); ?> </td>
                                    <td> <?php echo e($record->name); ?> </td>
                                    <td> <?php echo e($record->meta_title); ?> </td>
                                    <td> <?php echo e($record->meta_keywords); ?> </td>
                                    <td> <?php echo e(mb_strimwidth($record->meta_description, 0, 100, "...")); ?> </td>
                                    <td> <?php echo e($record->og_title); ?> </td>
                                    <td> <?php echo e(mb_strimwidth($record->og_description, 0, 100, "...")); ?> </td>
                                    <td> <?php echo e($record->og_image); ?> </td>
                                    <td> <?php echo e($record->canonical_url); ?> </td>
                                    <td> <?php echo e($record->is_active); ?> </td>
                                    <td>
                                        <a href="<?php echo e(route('pages.edit', $record->id)); ?>" title="Edit" class="icon currency-color">
                                            <i class=" fa fa-edit"></i>
                                        </a>

                                        <a href="<?php echo e(route('pages.destroy', $record->id)); ?>" title="Delete" class="icon lightBlue-color">
                                            <i class=" fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                            <tfoot>
                            <th> SR No. </th>
                            <th> Name </th>
                            <th> Meta Title </th>
                            <th> Meta Keyword </th>
                            <th> Meta Description </th>
                            <th> OG Title </th>
                            <th> OG Description </th>
                            <th> OG Image </th>
                            <th> Canonical URL </th>
                            <th> Is Active </th>
                            <th> Actions </th>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <!-- /.card -->
            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\drshardaayurveda\resources\views/admin/page/index.blade.php ENDPATH**/ ?>