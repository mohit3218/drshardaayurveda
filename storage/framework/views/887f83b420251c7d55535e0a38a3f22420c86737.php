<?php
/**
 * Constipation Services Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>

<?php $__env->startSection('content'); ?>
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h1 class="ser-heading mt-4">Ayurvedic Treatment for Constipation</h1>
                    <p style="font-size: 18px;">Easy Bowel Movements with Ayurveda</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/constipation">Constipation</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatments">TREATMENT</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="definition">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="<?php echo e(URL::asset('front/images/constipation-diagram.webp')); ?>" class="why-choose-us img-fluid" alt="women suffering from Constipation" title="women suffering from Constipation">
                </div>
            </div>
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="heading1">What is <b>Constipation?</b></h2>
                <p>
                    Understanding constipation, a frequent gastrointestinal problem, is the first step towards better digestive health. Constipation is defined as infrequent and difficult bowel motions that are frequently accompanied by pain and a sense of incomplete evacuation. Constipation has become a serious disorder and is one of the most common health problems these days. Women are more prone to Constipation in comparison to men. An individual faces problems passing stool, which is frustrating and also affects a person’s quality of life. Constipation refers to passing stools that are extremely hard and dry. Constipation is not just a state of trouble; sometimes it can be very painful too. Severe episodes of Constipation can lead to problems like Anal fissures or the growth of Hemorrhoids
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                    <p>Constipation is termed Vibandh in Ayurveda. According to Ayurveda, Constipation is considered an imbalance in the Vata dosha, which leads to impaired digestive fire (Agni) and incorrect evacuation. After eating incompatible foods, there is an increase in Vata and Pitta doshas in the body, which causes Constipation. Elderly people are seen more as sufferers of Constipation due to changes in diet, medication, and decreased mobility.
                    </p>
                    <p>If you choose Dr. Sharda's Ayurvedic Treatment for Constipation, you can eliminate the symptoms of constipation and find relief from it. We have a proficient team of Ayurvedic doctors in India who specialize in treating constipation.</strong>. 
                </p>
                <div class="btn-ayurvedic">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec our-expert" style="background-color:#f3f3f3" id="causes">
    <div class="container-fluid" >
        <div class="row cut-row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Constipation</b></h2>
                    <p font align="center" style="line-height:2">Constipation, a common digestive discomfort, occurs due to difficulty passing stools and can have various underlying causes. Each individual's experience is unique, necessitating a compassionate and holistic approach to effective constipation treatment.</p>

                    <li><b>Low-Fiber Diet:</b> In today's fast-paced world, where processed foods abound, many people unknowingly consume low-fiber diets. It becomes difficult to evacuate stools due to this shortage.
                    </li>
                    <li><b>Dehydration:</b> In our busy lives, it is easy to forget to drink enough water. Lack of water causes the body to create dry, firm stools that are challenging to pass.
                    </li>
                    <li><b>Sedentary Lifestyle:</b> As technology has developed, many people's lives have become less focused on physical exercise. Constipation may result from sluggish bowel movements brought on by infrequent movements.
                    </li>
                    <li><b>Impact of Stress:</b> Contemporary life stressors can greatly impact our health, specifically our digestive system. Stress can have a significant impact on constipation, potentially causing abnormalities and making the issue worse.
                    </li>
                    <li><b>Other Reasons:</b> When there is a lack of regular bowel motions as a result of a nerve injury, This is a significant cause of constipation. Another cause of chronic constipation is a disease, surgery, or medication side effects.
                    </li>
                </div>
            </div>
        </div>
    </div>
</section> 

<div class="container-strip" style="background: #eeb03f";>
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms"background-color="bisque">
    <div class="container-fluid">
            <h2 class="heading1">Symptoms of <b>Constipation</b></h2>
            <p font align="center" style="line-height:2;, text-align: left;">Identifying the existence of constipation requires an understanding of the various signs that our bodies give us. Examine the main signs and symptoms of constipation in order to identify the indications for timely Ayurvedic treatment for constipation.
            </p>
        <div class="kmoz" style="margin-top: 50px;">
            <img src="<?php echo e(URL::asset('front/images/Constipation_Symtoms.webp')); ?>" class="img-fluid"alt="image showing symptoms of Constipation" title="image showing symptoms of Constipation">
        </div>
        <div class="cvn sym px-4" style="margin-top: 50px;">

             <li><b>Infrequent Bowel Movements:</b> Constipation manifests through infrequent bowel movements, occurring less than three times a week.
            </li>
            <li><b>Discomfort with Stools:</b> Passing hard, lumpy stools can be uncomfortable and is a common symptom of constipation.
            </li>
            <li><b>Changes in Bowel Habits:</b> Constipation can disrupt regular bowel habits, leading to changes in the usual pattern of bowel movements.
            </li>
            <li><b>Feeling of Incomplete Evacuation:</b> Another sign of constipation is the feeling that the intestines are not completely empty after passing stool.
            </li>
            <li><b>Abdominal Discomfort:</b> Bloating and pain in the abdomen can result from constipation. Gas and bloating may worsen the discomfort caused by stool buildup in the colon.
            </li>
            <li><b>Nausea and Loss of Appetite:</b> Constipation sometimes goes hand in hand with nausea and a loss of appetite. The function of the digestive system as a whole can be impacted by trash buildup in the intestines.
            </li>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="treatments">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;"><b>       Constipation</b> Ayurvedic Treatment</h2>
                <p font align="center" style="line-height:1.5rem; text-align: left;">When you choose Ayurvedic treatment for constipation, you are opening yourself up to a world of holistic care that values your uniqueness as an individual. This approach to healing incorporates various elements and draws on ancient wisdom, all under the guidance of a compassionate team of experts.
            </p>

             <li>
                Dietary advice, where a fiber-rich diet bursting with fruits, vegetables, whole grains, and legumes takes center stage and balances the Vata Dosha, is one of the cornerstones of Constipation Ayurvedic treatment. Warm water helps with digestion and sets the mood for the day when drunk first thing in the morning.
            </li>
            <br>
            <li>
                In ayurvedic treatment, lifestyle changes are given great importance. This includes incorporating regular exercise and practising yoga for medicinal purposes, along with following a healthy diet. These techniques help improve bowel movements and aid digestion.
            </li>
            <br>
            <li>
                Keeping a healthy water intake is crucial for maintaining soft stools and easier bowel movements. Ayurveda acknowledges the significant impact of stress on digestion; therefore, reducing stress through methods like deep breathing and meditation is an essential part of the healing process.
            </li>
            <br>
            <li>
                If you're suffering from constipation, you may find relief with ayurvedic home remedies. One such remedy is Triphala, a blend made from three fruits: Amalaki, Bibhitaki, and Haritaki. These fruits are known for their ability to regulate bowel movements and improve digestive health, and they have been used as a natural cure for constipation for centuries.
            </li>
            <br>
            <p>
                At Dr. Sharda Ayurveda, we believe that everyone's path to good health is unique. Our compassionate medical experts assess your body type and develop a personalised treatment plan that caters to your specific needs. We embark on a revolutionary journey to achieve optimal digestive health, embracing the purest and most nourishing form of Ayurvedic knowledge.
            </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="sR-v6exBo2s" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="<?php echo e(URL::asset('front/images/sR-v6exBo2s.webp')); ?>" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('<?php echo e(URL::asset('front/images/video-icon.png')); ?>') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Patient's <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="<?php echo e(URL::asset('front/images/patient-testimonial-male.webp')); ?>" class="img-fluid" alt="Mahipal Singh Constipation Patient's Testimonial" title="Mahipal Singh Constipation Patient's Testimonial">
                            <h3 class="usrname">Mahipal Singh</h3>
                            <p class="desig">Constipation Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                When constipation hit at age 67, I was in extreme pain. I had to take a medicine every day to help me pass stools, which was difficult. I tried ayurveda after seeing multiple doctors but was still unable to get better, so I went to see Dr. Sharda Ayurveda after learning about them on social media. I began feeling better after a few weeks, and now I'm happy to be free from all the pain and can feel refreshed without any difficulty.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="<?php echo e(URL::asset('front/images/patient-testimonial-female.webp')); ?>" class="img-fluid" alt="Nikita Constipation Patient's Testimonial" title="Nikita Constipation Patient's Testimonial">
                            <h3 class="usrname">Nikita</h3>
                            <p class="desig">Constipation Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I'm Nikita from Ludhiana, and I've been experiencing constipation for a few months. I experience no relief after taking several medicines. I chose ayurveda since it has no side effects considering my sickness doesn't become worse. In order to receive treatment, I went to Dr. Sharda Ayurveda, and I'm extremely grateful of their remarkable care. I no longer require any additional medicines and have good health.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="<?php echo e(URL::asset('front/images/patient-testimonial-male.webp')); ?>" class="img-fluid" alt="Constipation Testimonial" title="Rajesh Kumar Constipation Patient's Testimonial">
                            <h3 class="usrname">Rajesh Kumar</h3>
                            <p class="desig">Constipation Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                As soon as I began receiving Dr. Sharda Ayurveda's Ayurvedic therapy for constipation, my condition of chronic constipation almost instantly disappeared. I found that by making little dietary modifications, I could end my chronic constipation.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                How long does it take to clear out constipation by Ayurvedic treatment?
                                </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            Ayurvedic treatment is a natural way to heal disorders. Ayurveda relies on Prakriti and every person has their own Prakriti. So, depending upon the condition and duration treatment is planned and so does healing occurs.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                What things make constipation worse?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            Eating a lot of high-fat meats, dairy products and eggs, sweets, or processed foods may cause constipation. Not enough fluids.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                How can I treat Constipation at home?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                            <li>Drinking adequate water</li><li>Eating fruits & salads</li> <li>Having a fiber-rich diet.</li> 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFive" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <i class="fa" aria-hidden="true"></i>
                                Which Dosha causes constipation?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="card-body">
                            Ayurveda categorizes constipation as a type of Vata Dosha disorder.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Does Ayurveda has permanent treatment for constipation?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                        <div class="card-body">
                            Yes, at Dr. Sharda Ayurveda we have many natural formulations of Ayurvedic medicines which have tremendous results in constipation. They give no side- effects on health.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                How often should I move my bowels?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                        <div class="card-body">
                            There is no “normal” number of bowel movements. Many healthcare providers agree that healthy bowel movement frequency can range from three times a day to three times a week.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                How to cure constipation with Ayurvedic treatment?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                        <div class="card-body">
                            One of the best known Ayurvedic laxatives is Triphala, a medicinal blend of dried fruits from the plants Emblica officinalis (Amalaki or the Indian gooseberry), Terminalia bellerica (Bibhitaki), and Terminalia chebula (Haritaki).
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Which Yoga poses help aid Constipation?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            Some of the Yoga poses that help to relieve constipation are:
                            <li>Sarvangasana</li>
                            <li>Vajrasana</li>
                            <li>Trikonasana</li>
                            <li>Janusirsasana</li>
                            <li>Bhujangasana</li>
                            <li>Balasana</li>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                               What precautions should be taken to avoid Constipation?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                        <div class="card-body">
                            <ul>
                                <li>Avoid cold and soft drinks. </li>
                                <li>Avoid eating a non-vegetarian diet as it is hard to digest.</li>
                                <li>Avoid eating Red meat as it is hard to digest.</li>
                                <li>Promoting healthy lifestyle with correct diet and being in harmony with nature helps preventing constipation. </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Is drinking warm water good for constipation?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                        <div class="card-body">
                           Drinking lukewarm water early in the morning for an easy bowel movement will help with constipation. The warm water helps with the metabolism. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $__env->make('../partials/frontend/blogs_post_section', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php echo $__env->make('../partials/frontend/form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Constipation",
    "item": "https://www.drshardaayurveda.com/constipation"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "How long does it take to clear out constipation by Ayurvedic treatment?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Often it will go away on its own within a few days of Ayurvedic treatment followed by some dietary changes and following a workout routine."
    }
  },{
    "@type": "Question",
    "name": "Why do I feel constipated after pooping?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It is due to incomplete evacuation of stool which is clinical symptom of constipation. In state of chronic constipation one feels constipated after passing stool too because of incomplete bowel movement."
    }
  },{
    "@type": "Question",
    "name": "How can I treat Constipation at home?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "By drinking adequate water, eating Fruits, Salad and fiber promoting food in diet."
    }
  },{
    "@type": "Question",
    "name": "Which Dosha causes constipation?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurveda categorize constipation as a type of Vata disorder."
    }
  },{
    "@type": "Question",
    "name": "Do Ayurveda has permanent treatment of constipation?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, at Dr. Sharda Ayurveda we have many natural formulation of Ayurvedic medicines which have tremendous result in constipation which have no Side effects on health."
    }
  },{
    "@type": "Question",
    "name": "How often should one poo?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "This is the most asked question. The frequency of Bowel movement varies from 1 person to another depending upon their Diet and workout pattern."
    }
  },{
    "@type": "Question",
    "name": "What is the cost of treatment for constipation in Ayurveda?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Cost of treatment for constipation depends on the type of Dosha and then treatment is suggested by our expert. Treatment in Ayurveda for constipation is not quite expensive and is very effective. Constipation treatment in Ayurveda from Dr. Sharda has highly effective results."
    }
  },{
    "@type": "Question",
    "name": "Which Yoga poses helps in aid Constipation?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Some of the Yoga poses that helps relieve constipation are:
Sarvangasana, Vajrasana, Trikonasana, Janusirsasana, Bhujangasana and Balasana"
    }
  },{
    "@type": "Question",
    "name": "What precautions should be taken to avoid Constipation?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "1- Avoid cold and soft drinks. 2- Regular exercising promotes healthy digestion that helps in easy bowel movements. 3- Avoid eating Red meat as it is hard to digest. 4- Promoting healthy lifestyle with correct diet and being in harmony with nature helps preventing constipation."
    }
  },{
    "@type": "Question",
    "name": "Is drinking warm water good for constipation?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Drinking hot water early in the morning for easy bowel movements and then drinking it usually helps to break down food faster which do not causes constipation."
    }
  }]
}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\drshardaayurveda\resources\views/frontend/services/constipation.blade.php ENDPATH**/ ?>