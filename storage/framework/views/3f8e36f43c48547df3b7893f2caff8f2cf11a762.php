<!DOCTYPE html>
<html lang="en">
    <!-- BEGIN HEAD -->
    <head>
        <?php echo $__env->make('../partials/head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </head>
    <!-- END HEAD -->
    <body class="hold-transition sidebar-mini">
        <div class="wrapper">
            <!-- BEGIN HEADER -->
            <?php echo $__env->make('../partials/header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <!-- END HEADER -->
            <!-- Main Sidebar Container -->
            <?php echo $__env->make('../partials/sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <!-- End Sidebar Container -->
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <?php echo $__env->yieldContent('content'); ?>
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <?php echo $__env->make('../partials/footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <!-- END FOOTER -->
        </div>
        <!-- jQuery -->
        <script src="<?php echo e(URL::asset('admin/plugins/jquery/jquery.min.js')); ?>" type="text/javascript"></script>
        <!-- Bootstrap 4 -->
        <script src="<?php echo e(URL::asset('admin/plugins/bootstrap/js/bootstrap.bundle.min.js')); ?>" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo e(URL::asset('admin/dist/js/adminlte.min.js')); ?>" type="text/javascript"></script>
        <!-- DataTables  & Plugins -->
        <script src="<?php echo e(URL::asset('admin/plugins/datatables/jquery.dataTables.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(URL::asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(URL::asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js')); ?>" type="text/javascript"></script>
        <script src="<?php echo e(URL::asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')); ?>" type="text/javascript"></script>
        <!-- <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script> -->
        <script src="//cdn.ckeditor.com/4.20.2/full/ckeditor.js"></script>
        <!-- <script src="<?php echo e(url('path/ckeditor.js')); ?>"></script> -->
        <script type="text/javascript">
            CKEDITOR.replace('post_contant', {
                filebrowserUploadUrl: "<?php echo e(route('posts.upload', ['_token' => csrf_token() ])); ?>",
                filebrowserUploadMethod: 'form',
                filebrowserBrowseUrl     : "<?php echo e(route('ckfinder_browser')); ?>",
                //filebrowserImageBrowseUrl: "<?php echo e(route('ckfinder_browser')); ?>?type=Images&token=123",
                //filebrowserFlashBrowseUrl: "<?php echo e(route('ckfinder_browser')); ?>?type=Flash&token=123",
                //filebrowserImageUploadUrl: "<?php echo e(route('ckfinder_connector')); ?>?command=QuickUpload&type=Images",
                //filebrowserFlashUploadUrl: "<?php echo e(route('ckfinder_connector')); ?>?command=QuickUpload&type=Flash",
            });
        </script>
        <?php echo $__env->make('ckfinder::setup', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <script>
            $(function () {
                $('.example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                });
            });
        </script>
    </body>
</html><?php /**PATH D:\xampp\htdocs\drshardaayurveda\resources\views/layouts/admin.blade.php ENDPATH**/ ?>