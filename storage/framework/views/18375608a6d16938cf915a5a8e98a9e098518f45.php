<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>

<?php $__env->startSection('content'); ?>
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Fibromyalgia (FM)</h3>
                    <p style="font-size: 24px;">Protect your joints with timely treatment</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/fibromyalgia">Fibromyalgia</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#managements">MANAGEMENTS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="<?php echo e(URL::asset('front/images/fibromyalagia.webp')); ?>" class="why-choose-us img-fluid" alt="fibromyalagia treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"><b>Fibromyalgia</b> Ayurvedic Treatment</h1>
                <p>
                Fibromyalgia has become a trending disorder now. It is a condition in which a patient complains of soft tissue pain, widespread musculoskeletal pain, stiffness, followed by fatigue, memory loss, and mood swings quite often. There are widely disturbed bilateral symmetrical tender joints. Middle-aged women or above 50 years and above are more prone to develop fibromyalgia. Sleep disorder has been implicated as a factor in pathogenesis. Nonrestorative sleep or awakening non fresh has been observed in most patients of fibromyalgia and sleep encephalograms show disruption of the normal stage. It is also associated with tension, headache, IBS, anxiety, and depression. There is no blood investigation to detect fibromyalgia. Physical examination is required for tenderness especially at the back of the neck.
                                      </p>
               <p>
                     18 tender points are examined. 11 out of 18 if seen tender patient is diagnosed with fibromyalgia. <strong>Fibromyalgia treatment in Ayurveda</strong> has the most effective sure results.
                       </p>
                    <div class="btn-ayurvedic">
                        <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                    </div>
            </div>
        </div>
    </div>
</section>



<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Fibromyalgia</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Microcrystal Deposits
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                            Abnormal increase in the level of chemicals in the brain which leads to nerve stimulation and development of memory of pain in the brain.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Genetics
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            This disease tends to run in families.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Infections
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                            Some illness trigger and aggravates fibromyalgia, presence of RA or SLE an autoimmune disease also cause pain in the body.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Trauma
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                            Psychological stress may lead to fibromyalgia. Sometimes some accidental history can also trigger this pain.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                Disturbed Digestion
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                            Poor nourishment and disturbed digestion is also the cause of fibromyalgia.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="<?php echo e(URL::asset('front/images/fibromalagia-symptoms.webp')); ?>" class="img-fluid"alt="Fibromyalgia symptoms">
        </div>
        <div class="cvn sym" >
            <h3><span>Symptoms of</span> Fibromyalgia </h3>
            <p class="sub"><img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="2.png">Widespread Pain</p>
            <p>Dull constant ache that can even last for 3 months radiating to lower back towards the toe.</p>
            <p class="sub"><img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="2.png">Lost Concentration</p>
            <p>Difficulties with memory and concentration fibro fog i.e. inability to focus, pay attention, concentrate on mental tasks. </p>
            <p class="sub"><img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="2.png">Tiredness</p>
            <p>Fatigue, restlessness, mood swings are the most common symptoms.</p>
            <p class="sub"><img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="2.png">Emotional And Physical Stress</p>
            <p>An Individual stays often under stress. </p>
            <p class="sub"><img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="2.png">Irregular Sleep Patterns</p>
            <p>It gets hard for a patient to have a sound sleep at proper timings. They get disturbed sleep often. </p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Ayurvedic management of <b>Fibromyalgia</b></h2>
                <p class="im3">
                    <img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="3.png">
                    Fibromyalgia syndrome is compared to Vata Vyadi in Ayurveda.
                </p>
                <p class="im3">
                    <img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="3.png">
                    Vitation of Mamravaha, Asthivaha, Majjavaha Strotas occurs in this disease
                </p>
                <p class="im3">
                    <img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="3.png">
                    In Ayurveda treatment of fibromyalgia, the focus is on both Vata Samshamana i.e. rebalance, and Samshodhana ie purification which becomes deranged due to the accumulation of Ama
                </p>
                <p class="im3">
                    <img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="3.png">
                    Treatment includes Panchkarma, herbal steam therapy, internal medicines, strict diet, and lifestyle modifications, and stress management. Yoga and Meditation are advised to follow up daily
                </p>
                <p class="im3">
                    <img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="3.png">
                    Warm Cardamom milk is advised to drink for better recovery
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="GY7cawNN1Ho" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="<?php echo e(URL::asset('front/images/GY7cawNN1Ho.webp')); ?>" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('<?php echo e(URL::asset('front/images/video-icon.png')); ?>') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Client <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="<?php echo e(URL::asset('front/images/patient-testimonial-female.webp')); ?>" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Paramjit Kaur</h3>
                            <p class="desig">Fibromyalgia Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                             I was suffering from Fibromyalgia for the last six months. I was hopeless, but then I switched my treatment to Ayurveda. Since then, I have had such a relief. The Ayurvedic treatment for fibromyalgia treatment has helped me immensely. The experts and staff of Dr. Sharda Ayurveda truly care about the well-being of their patients. I would recommend them as they are the best. 
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
 <div class="item">
                        <div class="testimonial-content">
                            <img src="<?php echo e(URL::asset('front/images/patient-testimonial-female.webp')); ?>" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Parkash Kaur</h3>
                            <p class="desig">Fibromyalgia Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                               I could not believe my eyes when I saw the results of the Ayurvedic treatment from Dr. Sharda Ayurveda. I was at my lowest when I started with treatment. They helped me to improve my conditions in every possible way. The personalized treatment is just fabulous! Thank you, Dr. Sharda Ayurveda!
                               <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="<?php echo e(URL::asset('front/images/patient-testimonial-male.webp')); ?>" class="img-fluid" alt="Testimonial" >
                            <h3 class="usrname">Kulwinder Singh</h3>
                            <p class="desig">Fibromyalgia Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                               Dr. Sharda Ayurveda has given my life a new meaning. I was diagnosed with chronic body pain and the Fibromyalgia Ayurvedic Treatment from here is outstanding. What sets them apart is their excellent services. I have been taking their natural medicines and not only, did I get successful results, but also there were no side effects of the treatment. 
                               <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                What are the foods to be avoided in fibromyalgia?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                        White flour, pasta, rice, dairy, gluten, sugar, Processed foods, gluten, alcohol, red meat, caffeine, refined carbohydrates are the food items that one should avoid eating in fibromyalgia. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                How can one get to know that it is fibromyalgia?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                        If you feel severe pain in the body, stiffness, tingling sensation, cramp, numbness in arm, feverish feeling then fine the fibromyalgia specialist near you and  consult an Ayurvedic expert.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                How does a person get fibromyalgia?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                        It is triggered by physical and emotional stress, injury, or some viral infections.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                What other diseases can be mistaken for fibromyalgia?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                        Lypus, RA, Diabetes, Thyroid, Axial spondyloarthritis, Anemia
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                How does fibromyalgia skin rashes look like?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                        Rashes are red, raised, or bumpy, it may cause a crawling sensation on the skin
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    What are the worst symptoms of fibromyalgia?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                            Fatigue is the worst symptom, fibrofog i.e. difficulty to think is also considered as a significant symptom. 
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Is fibromyalgia a mental illness?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                        It is not considered a mental illness but with body pains all over the body, the patient experiences depression or anxiety. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                How drinking water helps in treating fibromyalgia patients?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                        Hydration is the very first thing one should opt to decrease fibromyalgia fatigue.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                What is the most effective treatment for fibromyalgia?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                        Ayurvedic treatment for fibromyalgia is best as it works on healing both physical, mental issues of the body by changing diet, lifestyle modifications. Fibromyalgia cure in Ayurveda has the most effective sure results. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $__env->make('../partials/frontend/blogs_post_section', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php echo $__env->make('../partials/frontend/form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Fibromyalgia",
    "item": "https://www.drshardaayurveda.com/fibromyalgia"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What are the foods to be avoided in fibromyalgia?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "White flour, pasta, rice, dairy, gluten, sugar, Processed foods, gluten, alcohol, red meat, caffeine, refined carbohydrates are the food items that one should avoid eating in fibromyalgia."
    }
  },{
    "@type": "Question",
    "name": "How can one get to know that it is fibromyalgia?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If you feel severe pain in the body, stiffness, tingling sensation, cramp, numbness in the arm, feverish feeling then fine the fibromyalgia specialist near you and consult an Ayurvedic expert."
    }
  },{
    "@type": "Question",
    "name": "How does a person get fibromyalgia?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It is triggered by physical and emotional stress, injury, or some viral infections."
    }
  },{
    "@type": "Question",
    "name": "What other diseases can be mistaken for fibromyalgia?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Lypus, RA, Diabetes, Thyroid, Axial spondyloarthritis, Anemia."
    }
  },{
    "@type": "Question",
    "name": "How does fibromyalgia skin rashes look like?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Rashes are red, raised, or bumpy, it may cause a crawling sensation on the skin."
    }
  },{
    "@type": "Question",
    "name": "What are the worst symptoms of fibromyalgia?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Fatigue is the worst symptom, fibrofog i.e. difficulty to think is also considered as a significant symptom."
    }
  },{
    "@type": "Question",
    "name": "Is fibromyalgia a mental illness?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It is not considered a mental illness but with body pains all over the body, the patient experiences depression or anxiety."
    }
  },{
    "@type": "Question",
    "name": "How drinking water helps in treating fibromyalgia patients?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Hydration is the very first thing one should opt to decrease fibromyalgia fatigue."
    }
  },{
    "@type": "Question",
    "name": "What is the most effective treatment for fibromyalgia?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurvedic treatment for fibromyalgia is best as it works on healing both physical, mental issues of the body by changing diet, lifestyle modifications. Fibromyalgia cure in Ayurveda has the most effective sure results."
    }
  }]
}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\drshardaayurveda\resources\views/frontend/services/fibro_myalagia.blade.php ENDPATH**/ ?>