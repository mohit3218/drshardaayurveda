<?php if(!empty($posts)) : ?>
<section class="latest-post-sec">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 col-md-12">
                    <div class="lp-content">
                        <h2>Latest <b>Post</b></h2>
                        <p>Blog Posts about the disease and their Ayurvedic treatments</p>
                    </div>
                </div>
            </div>
            <div class="row custom-lp">
                <?php foreach($posts as $post) : $img = $post['post_image']; ?>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 mb-4">
                    <div class="lp-post">
                        <img src="/uploads/posts/<?php echo e($img); ?>" class="img-fluid" alt="Blogopost" >
                        <div class="post-content">
                            <h3><?= strip_tags(Str::limit($post['post_title'], 55)); ?></h3>
                            <p><?= strip_tags(Str::limit($post['post_contant'], 50)); ?></p>
                            <a href="/blogs/<?=str_replace(array( ',', ')' ), '', str_replace(" ","-", strtolower($post['name']))); ?>" class="ptn-post">Read more...</a>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif; ?><?php /**PATH D:\xampp\htdocs\drshardaayurveda\resources\views////partials/frontend/blogs_post_section.blade.php ENDPATH**/ ?>