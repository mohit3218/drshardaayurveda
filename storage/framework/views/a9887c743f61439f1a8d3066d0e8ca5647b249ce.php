<?php
/**
 * Contact Page 
 * also disappear!
 * @created    18/11/2021
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2021
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>

<?php $__env->startSection('content'); ?>
<?php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$routeName = \Route::currentRouteName();
?>
 
<style>.main-sec{width:100% !important;margin:0;}</style>
<div class='contact-us'>
    <div class="container-fluidd">
        <div class="row">
            <div class="col-lg-12 col-md-12 text-center">
                <a data-toggle="modal" data-target=".bd-example-modal-xl">
                    <img src="<?php echo e(URL::asset('front/images/treatment-banner.webp')); ?>" class="doctor img-fluid" alt="Ayurvedic Doctor Dr. Mukesh Sharda">
                </a>
            </div>
        </div>
    </div>
<section class="dsa-why-choose" id="definition">
    <div class="container-fluid">
        <div class="row cut-row">
		<div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1">Ayurvedic Treatments at <b>Dr. Sharda Ayurveda</b></h1>
                <p>The Ayurvedic treatment internally purifies the body and eliminates the toxins balancing the body's energies known as tridoshas. The sattvic diet, herbal remedies, lifestyle modifications, yoga, and meditation help to prevent the buildup of toxins in the future. The experts of Dr. Sharda Ayurveda provide the best Ayurvedic treatment in India as they analyze the medical history along with the present medical conditions and accordingly plan the treatment.<br>
				Ayurveda believes to restore the balance between body, mind, and soul to maintain a healthy lifestyle. By using the holistic system, which has its roots in India, Ayurveda aims to eradicate the disease by providing a personalized approach.<br>
				The doctors aim to end suffering with a comprehensive approach and heal mankind in a purely natural way giving no side effects and long-term relief. 
                </p>
            </div>
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
				<div class="type_row row  mb-5">
					<div class="col-md-4 col-sm-6 col-12 treatment text-center">
						<a href="<?php echo e(route('rheumatoid-arthritis')); ?>">
						<div class="icon_box">
							<div class="icon">
								<img class="treatment-image" src="<?php echo e(URL::asset('front/images/rheumatoid-arthritis-icon.webp')); ?>">
							</div>
						</div>
						<div class="clr"></div>
						<div class="icon-name">
							<span>Rheumatoid Arthritis</span>
						</div>
						 </a>
					</div>

					<div class="col-md-4 col-sm-6 col-12 treatment text-center">
						<a href="<?php echo e(route('back-pain')); ?>">
							<div class="icon_box">
								<div class="icon">
									<img src="<?php echo e(URL::asset('front/images/back-pain-icon.webp')); ?>">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Back Pain</span>
							</div>
						</a>
					</div>

					<div class="col-md-4 col-sm-6 col-12 treatment text-center">
						<a href="<?php echo e(route('asthma')); ?>">
							<div class="icon_box">
								<div class="icon">
									<img class="treatment-image" src="<?php echo e(URL::asset('front/images/asthma-icon.webp')); ?>">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Asthma</span>
							</div>
						</a>
					</div>

					<div class="clr"></div>
				</div>
				<div class="type_row row">
					<div class="col-md-4 col-sm-6 col-12 treatment text-center">
						<a href="<?php echo e(route('psoriasis')); ?>">
						<div class="icon_box">
							<div class="icon">
								<img class="treatment-image" src="<?php echo e(URL::asset('front/images/psoriasis-icon.webp')); ?>">
							</div>
						</div>
						<div class="clr"></div>
						<div class="icon-name">
							<span>Psoriasis</span>
						</div>
						 </a>
					</div>

					<div class="col-md-4 col-sm-6 col-12 treatment text-center">
						<a href="<?php echo e(route('gout')); ?>">
							<div class="icon_box">
								<div class="icon">
									<img src="<?php echo e(URL::asset('front/images/gout-icon.webp')); ?>">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Gout</span>
							</div>
						</a>
					</div>

					<div class="col-md-4 col-sm-6 col-12 treatment text-center">
						<a href="<?php echo e(route('hip-joint-pain')); ?>">
							<div class="icon_box">
								<div class="icon">
									<img class="treatment-image" src="<?php echo e(URL::asset('front/images/hip-joint-icon.webp')); ?>">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Hip Joint Pain</span>
							</div>
						</a>
					</div>
					<div class="clr"></div>
				</div>
            </div>
        </div>
    </div>
</section>
<section class="review_section" style="padding-bottom: 0 !important;">
        <div class="container">
			<div class="row">
          		<div class="review_box">
					<p class="review">
						<i>
							"The advantage of Ayurvedic treatment is that it yields side benefits, not side effects."
						</i>
					</p>
					<p class="name">- Dr. Mukesh Sharda</p>
				</div>
			</div>
        </div>
    </section>

<section class="our_treatment_section bg-texture">
        <div class="container">
          	<div class="our_treatment">
				<div class="text-center">
					<h2 class="f-size2em"><b>Best Ayurvedic Treatments in India BY DR SHARDA AYURVEDA</b></h2>
				</div>
            	<div class="our_treatment_title">Expertise in treatment of all chronic diseases and more than 95% of patients relieved their condition within just 50 days of Ayurvedic treatment. <br>Suffering from any disease? Want a proven solution for it?</div>
				<div class="" style="text-align: center"><h4>Here you go and get the best-personalized treatment.</h4></div>
				<div class="type_row row">
					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="<?php echo e(route('rheumatoid-arthritis')); ?>">
							<div class="icon_box">
								<div class="icon">
									<img class="treatment-image" src="<?php echo e(URL::asset('front/images/rheumatoid-arthritis-icon.webp')); ?>">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Rheumatoid Arthritis</span>
							</div>
						</a>
					</div>

					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="<?php echo e(route('ankylosing-spondylitis')); ?>">
							<div class="icon_box">
								<div class="icon">
									<img class="treatment-image" src="<?php echo e(URL::asset('front/images/ankylosing_spondylitis-icon.webp')); ?>">
								</div>
								<div class="clr"></div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Ankylosing Spondylitis</span>
							</div>
						</a>
					</div>

					<div class="col-md-3 col-sm-6 col-12 treatment">
	                    <a href="<?php echo e(route('back-pain')); ?>">
    						<div class="icon_box">
    							<div class="icon">
    								<img src="<?php echo e(URL::asset('front/images/back-pain-icon.webp')); ?>">
    							</div>
    						</div>
    						<div class="clr"></div>
    						<div class="icon-name">
    							<span>Back Pain</span>
    						</div>
	                    </a>
					</div>

					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="<?php echo e(route('cervical')); ?>">
							<div class="icon_box">
								<div class="icon">
									<img class="treatment-image" src="<?php echo e(URL::asset('front/images/cervical-icon.webp')); ?>">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Cervical Spondylosis</span>
							</div>
						</a>
					</div>
					<div class="clr"></div>
				</div>

				<div class="type_row row">
					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="<?php echo e(route('fibromyalgia')); ?>">
						<div class="icon_box">
							<div class="icon">
								<img class="treatment-image" src="<?php echo e(URL::asset('front/images/fibromyalgia-icon.webp')); ?>">
							</div>
						</div>
						<div class="clr"></div>
						<div class="icon-name">
							<span>Fibromyalgia</span>
						</div>
						 </a>
					</div>

					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="<?php echo e(route('gout')); ?>">
							<div class="icon_box">
								<div class="icon">
									<img src="<?php echo e(URL::asset('front/images/gout-icon.webp')); ?>">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Gout</span>
							</div>
						</a>
					</div>

					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="<?php echo e(route('hip-joint-pain')); ?>">
							<div class="icon_box">
								<div class="icon">
									<img class="treatment-image" src="<?php echo e(URL::asset('front/images/hip-joint-icon.webp')); ?>">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Hip Joint Pain</span>
							</div>
						</a>
					</div>

					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="<?php echo e(route('knee-pain')); ?>">
							<div class="icon_box">
								<div class="icon">
									<img class="treatment-image" src="<?php echo e(URL::asset('front/images/knee-pain-icon.webp')); ?>">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Knee Pain</span>
							</div>
						</a>
					</div>
					<div class="clr"></div>
				</div>

				<div class="type_row row">
					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="<?php echo e(route('lumbar-spondylosis')); ?>">
							<div class="icon_box">
								<div class="icon">
									<img class="treatment-image" src="<?php echo e(URL::asset('front/images/lumbar-spondylosis-icon.webp')); ?>">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Lumbar Spondylosis</span>
							</div>
						</a>
					</div>

					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="<?php echo e(route('neck-pain')); ?>">
							<div class="icon_box">
								<div class="icon">
									<img class="treatment-image" src="<?php echo e(URL::asset('front/images/neck pain-icon.webp')); ?>">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Neck Pain</span>
							</div>
						</a>
					</div>

					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="<?php echo e(route('osteoarthritis')); ?>">
						<div class="icon_box">
							<div class="icon">
								<img class="treatment-image" src="<?php echo e(URL::asset('front/images/osteoarthritis-icon.webp')); ?>">
							</div>
						</div>
						<div class="clr"></div>
						<div class="icon-name">
							<span>Osteoarthritis</span>
						</div>
						</a>
					</div>

					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="<?php echo e(route('osteoporosis')); ?>">
							<div class="icon_box">
								<div class="icon">
									<img class="treatment-image" src="<?php echo e(URL::asset('front/images/osteoporosis-icon.webp')); ?>">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Osteoporosis</span>
							</div>
						</a>
					</div>
					<div class="clr"></div>
				</div>

				<div class="type_row row">
					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="<?php echo e(route('post-viral-arthritis')); ?>">
							<div class="icon_box">
								<div class="icon">
									<img class="treatment-image" src="<?php echo e(URL::asset('front/images/post-viral-arthritis-icon.webp')); ?>">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Post-Viral Arthritis</span>
							</div>
						</a>
					</div>

					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="<?php echo e(route('psoriatic-arthritis')); ?>">
							<div class="icon_box">
								<div class="icon">
									<img src="<?php echo e(URL::asset('front/images/psoriatic-arthritis-icon.webp')); ?>">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Psoriatic Arthritis</span>
							</div>
						</a>
					</div>

					<div class="col-md-3 col-sm-6 col-12 treatment">
                        <a href="<?php echo e(route('reactive-arthritis')); ?>">
						<div class="icon_box">
							<div class="icon">
								<img class="treatment-image" src="<?php echo e(URL::asset('front/images/reactive-arthritis-icon.webp')); ?>">
							</div>
						</div>
						<div class="clr"></div>
						<div class="icon-name">
							<span>Reactive Arthritis</span>
						</div>
						</a>
					</div>

					<div class="col-md-3 col-sm-6 col-12 treatment">
						<a href="<?php echo e(route('uric-acid')); ?>">
							<div class="icon_box">
								<div class="icon">
									<img src="<?php echo e(URL::asset('front/images/uric-acid-icon.webp')); ?>">
								</div>
							</div>
							<div class="clr"></div>
							<div class="icon-name">
								<span>Uric Acid</span>
							</div>
						</a>
					</div>
					<div class="clr"></div>
				</div>

				<div class="type_row row">
					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('anemia')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/anemia-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Anemia</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('blood-pressure')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/blood-pressure-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Blood Pressure</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('high-cholesterol')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/high-cholesterol-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>High Cholesterol</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('coronary-artery-disease')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/coronary-artery-disease-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Coronary Artery Disease</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('alcohol-de-addiction')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/alcohol-de-addiction-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Alcohol De-Addiction</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('drug-de-addiction')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/drug-deaddiction-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Drug De-Addiction</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('smoking-de-addiction')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/smoking-deaddiction-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Smoking De-addiction</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('abdominal-pain')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/abdominal-pain-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Abdominal Pain</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('acidity')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/acidity-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Acidity</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('chronic-fatigue-syndrome')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/chronic-fatigue-syndrome-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Chronic Fatigue Syndrome</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('constipation')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/constipation-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Constipation</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('flatulence')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/flatulance-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Flatulence</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('ibs')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/ibs-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Irritable Bowel Syndrome (IBS)</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('piles')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/piles-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Piles</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('ulcerative-colitis')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/ulcerative-colitis-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Ulcerative Colitis</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('diabetes')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/diabetes-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Diabetes</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('obesity')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/obesity-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Obesity</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('thyroid')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/thyroid-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Thyroid</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('leucorrhoea')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/leucorrhoea-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Leucorrhoea</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('menopause')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/menopause-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Menopause</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('pcod-pcos')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/pcod-pcos-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Polycystic Ovarian Disease</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('kidney-failure')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/kidney-failure-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Kidney Failure</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('kidney-stones')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/kidney-stone-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Kidney Stones</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('fatty-liver')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/fatty liver-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Fatty Liver</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('jaundice')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/jaundie-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Jaundice</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('liver-cirrhosis')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/liver-cirrhosis-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Liver Cirrhosis</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('anxiety-disorder')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/anxiety-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Anxiety Disorder</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('depression')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/depression-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Depression</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('insomnia')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/insomnia-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Insomnia</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('migraine')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/migraine-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Migraine</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('sciatica-pain')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/sciatica-pain-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Sciatica Pain</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('asthma')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/asthma-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Asthma</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('sinusitis')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/sinusitis-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Sinusitis</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('rhinitis')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/rhinitis-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Rhinitis</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('chronic-laryngitis')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/chronic-laryngitis-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Chronic Laryngitis</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('erectile-dysfunction')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/erectile disfunction-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Erectile Dysfunction</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('oligospermia')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/oligospermia-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Oligospermia</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('premature-ejaculation')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/premature-ejaculation-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Premature Ejaculation</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('eczema')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/eczema-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Eczema</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('fungal-infection')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/fungal infection-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Fungal Infection</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('psoriasis')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/psoriasis-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Psoriasis</span>
							  </div>
						  </a>
					  </div>

					  <div class="col-md-3 col-sm-6 col-12 treatment">
						  <a href="<?php echo e(route('urinary-tract-infection')); ?>">
							  <div class="icon_box">
								  <div class="icon">
									  <img class="treatment-image" src="<?php echo e(URL::asset('front/images/urinary-tract-infection-icon.webp')); ?>">
								  </div>
							  </div>
							  <div class="clr"></div>
							  <div class="icon-name">
								  <span>Urinary Tract Infection (UTI)</span>
							  </div>
						  </a>
					  </div>
					  <div class="clr"></div>
				</div>
			</div>
		</div>
</section>

<section class="why-drshardha">
   <div class="banner"></div>
   <div class="container">
      <div class="btm-drshardha-why">
         <div class="head-bx">
         	<h2 class="hs-line-1 font-alt text-center mb-xs-10 mt-5"><b>What makes Dr. Sharda Ayurveda the reliable and secured option?</b></h2>
            <p>The Ayurvedic doctor's team holds uncountable experience and are specialized in treating chronic diseases naturally by deep analyzing the root cause of the disorder. Their main aim is to assure improved health and lifelong results.</p>
         </div>
         <div class="inner-btm text-center">
            <div class="row">
               <div class="col-md-3 col-sm-3 col-xs-6">
                  <div class="inner">
                     <h2>8L+</h2>
                     <p>successfully patients consulted</p>
                  </div>
               </div>
               <div class="col-md-3 col-sm-3 col-xs-6">
                  <div class="inner">
                     <h2>12+ </h2>
                     <p>Years of experience with cure</p>
                  </div>
               </div>
               <div class="col-md-3 col-sm-3 col-xs-6">
                  <div class="inner">
                     <h2>97%</h2>
                     <p>positive response from patients</p>
                  </div>
               </div>
               <div class="col-md-3 col-sm-3 col-xs-6">
                  <div class="inner">
                     <h2>10+</h2>
                     <p>experienced team of Ayurvedic doctors</p>
                  </div>
               </div>
            </div>
            <div class="btm-line">
               <p>WE ASSURE TO PROVIDE SAFE AND FINEST TREATMENT</p>
                <div class="btn-ayurvedic">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
         </div>
      </div>
   </div>
</section>


<section class="our_treatment_section">
    <div class="container">
		<div class="row mb-5">
			<div class="col-md-12">
				<h2 class="hs-line-1 font-alt text-center mb-xs-10 mt-5"><b>What is Ayurveda?</b></h2>
			</div>
			<p font align="center">
					Ayurveda is an ancient science that helps in healing the body, mind, and soul. It works for the overall development of the body. It releases muscle tension, rejuvenates the mind, and paves the path for spiritual awakening.
					Ayurveda has been believed to heal people for ages. With the purely natural approach which is non-invasive, it aims to give no side effects.</p>
		</div>
		<div class="row">
			<div class="col-md-6 ">
				<h3><span>Ayurvedic Treatment Ideology</span></h3>
				<p>
					The base of Ayurveda revolves around the five elements present in nature; space, air, water, earth, and fire. These five elements combine to form the three Doshas, referred to as tridoshas.
				</p>
				<p>
					The Doshas are basically the energies that are formed with the combination of the elements of nature. There are three Doshas:
				</p>
			
				<p> 
					<h4><span>Vata Dosha:</span></h4>
					The Vata Dosha is formed by combining air and space elements. The meaning of this Dosha is to move like the wind. The main function of Vata Dosha is to control the movement of the body and mind. The main managed activities are breathing blood flow, eliminating waste substances, and thoughts.
				</p>
				<h5><span>The characteristics/ qualities of Vata Dosha:</span></h5> 
					<li>Movement</li>
					<li>Roughness</li>
					<li>Coldness</li>
				<br>

				<h4><span>Pitta Dosha</span></h4>
				<p>
					This Dosha is dominated by the fire element and is combined with fire & water.  The heat is required for the proper functioning of the body, its regulation is done by this Dosha. The conversion of food into nutrition and digestion of food is done with this energy.
				</p>
				<h5><span>The characteristics/ qualities of Pitta Dosha:</span></h5> 
				<li>Hot</li>
				<li>Intense</li>
				<li>Flowing</li>
				<br>

				<h4><span>Kapha Dosha</span></h4>
				<p>This Dosha is formed by the combination of water and earth.  The Kapha Dosha is the binding unit of the body and is responsible for providing strength and immunity to the body.
				</p>
				<h5><span>The characteristics/ qualities of Kapha Dosha:</span></h5>
				<li>Cold</li>
				<li>Wet</li>
				<li>Heavy</li>
				<br>
			</div>

			<div class="col-md-6" >
				<h3><span>Ayurvedic Treatments</span></h3>
				<p>Ayurvedic treatment involves purely natural methods. The practices like the consumption of Ayurvedic herbs, spices, adopting a sattvic diet, modifying the lifestyle, and practicing yoga/meditation.
				</p>

				<h3><span> Ayurvedic Herbs</span></h3>
				<p>Ayurvedic herbs are produced from plants. The herbs are formulated into medicines. The parts of herbs that are used are leaves, nuts, seeds, bark, and roots. They are tested clinically and are best for overall development. They are highly nutritious and have immense healing powers.
				</p>
				<li>The herbs also act as preventive medicine as they positively affect overall health and boost immunity</li><br>
				<li>They are non-toxic and give no side effects to the body. Hence, no harm is done to the body.</li><br>
				<li>By rejuvenating the body, mind, and soul, herbs are useful in the overall well-being of the body.</li><br>
				<li>Ayurvedic herbs have a unique aroma & flavor which helps in maintaining an equilibrium between mind, body, and soul.</li><br>

				<h3><span>Satvic Diet</span></h3>
				<p> 
					Having a healthy and balanced diet will enhance the functions of the body. All the fruits and vegetables will instill the body with the required amount of nutrition content and other elements which are responsible for the growth & healing of the individual. 
				</p><br>

				<h3><span>Modification in lifestyle</span></h3>
				<p> 
					The lifestyle we live affects the body and mind which eventually influences the soul. A sedentary life must be ditched as inactive practices make the body more susceptible to various diseases whereas an active lifestyle will keep the body fit and healthy.
				</p><br>

				<h3><span>Yoga & Meditation</span></h3>
				<p> 
					Yoga and Meditation are ancient practices that help the body on a spiritual level. If you are looking for something which will calm the nerves and will soothe the activity of the brain then adopt them. Along with physical tensions, mental stress will also disappear!
				</p><br>
			</div>
		</div>
    </div>
</section>
	

<section class="book_appointment_form" >
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 basic_info">
				<div class="review_box">
					<p class="review"></p>
					<p class="review"> <a class="green-anchor"href="https://www.drshardaayurveda.com/dr-mukesh-sharda"></a></p>
				</div>
				<hr>
				<button type="button" class="btn btn-primary book-button" data-toggle="modal" data-target=".bd-example-modal-xl" style="background-color:#d49925 !important">
					Book a consultation with Dr Sharda Ayurveda
				</button>
			</div>
		</div>
	</div>
</section>

<section class="small-section bg-texture">
        <div class="container relative">
			<div class="row">
                <div class="col-md-12">
				   <h2 class="hs-line-1 font-alt text-center mb-xs-10 mt-5"><b>AYURVEDIC TREATMENTS</b></h2>
                </div>
            </div>
            
            <div class="row multi-columns-row">
				<div class="col-sm-6 col-md-4 col-lg-4">
					<div class="alt-service-item setheight">
						<div class="alt-service-icon">
							<img src="<?php echo e(URL::asset('front/images/rheumatoid-arthritis-icon.webp')); ?>">
						</div>

						<h3 class="alt-services-title font-alt">Rheumatoid Arthritis</h3>

						<div class="alt-services-desc">It is a  type of arthritis is an auto-immune disorder that mainly affects the joints present in the hands. The joints are affected severely....</div>

						<div class="alt-services-more">
							<a href="<?php echo e(route('rheumatoid-arthritis')); ?>">
							Read More</a>
						</div>
				</div>
			</div>
			
			<div class="col-sm-6 col-md-4 col-lg-4">
				<div class="alt-service-item setheight">
					<div class="alt-service-icon">
						<img src="<?php echo e(URL::asset('front/images/back-pain-icon.webp')); ?>">
					</div>

					<h3 class="alt-services-title font-alt">Back Pain</h3>

					<div class="alt-services-desc">Spinal pain is mainly a lifestyle disorder that involves a huge amount of discomfort and pain. The wear and tear may result in serious vertebral issues....</div>

					<div class="alt-services-more">
						<a href="<?php echo e(route('back-pain')); ?>">Read More</a>
					</div>
				</div>
			</div>
                                    
			<div class="col-sm-6 col-md-4 col-lg-4">
				<div class="alt-service-item setheight">
					<div class="alt-service-icon">
						<img src="<?php echo e(URL::asset('front/images/cervical-icon.webp')); ?>">
					</div>

					<h3 class="alt-services-title font-alt">Cervical</h3>

					<div class="alt-services-desc">It is the first region of the vertebral column and cervical pain starts in the neck region. Pain, stiffness, and numbness are common symptoms of the cervical....</div>

					<div class="alt-services-more">
						<a href="<?php echo e(route('cervical')); ?>">Read More</a>
					</div>
				</div>
			</div>
                                    
			<div class="col-sm-6 col-md-4 col-lg-4">
				<div class="alt-service-item setheight">
					<div class="alt-service-icon">
						<img src="<?php echo e(URL::asset('front/images/gout-icon.webp')); ?>">
					</div>

					<h3 class="alt-services-title font-alt">Gout</h3>

					<div class="alt-services-desc">When the uric acid starts forming crystals in the toes of the feet. The accumulation of uric acid results in extreme joint pain which usually affects the toe of the thumb....</div>

					<div class="alt-services-more">
						<a href="<?php echo e(route('gout')); ?>">Read More</a>
					</div>
				</div>
			</div>
                                   
			<div class="col-sm-6 col-md-4 col-lg-4">
				<div class="alt-service-item setheight">
					<div class="alt-service-icon">
						<img src="<?php echo e(URL::asset('front/images/asthma-icon.webp')); ?>">
					</div>

					<h3 class="alt-services-title font-alt">Asthma</h3>

					<div class="alt-services-desc">The respiratory disorder which causes chest pain, difficulty in breathing, and wheezing cough. Normal breathing becomes quite difficult for patients suffering from asthma....</div>

					<div class="alt-services-more">
						<a href="<?php echo e(route('asthma')); ?>">Read More</a>
					</div>
				</div>
			</div>
                                    
			<div class="col-sm-6 col-md-4 col-lg-4">
				<div class="alt-service-item setheight">
					<div class="alt-service-icon">
						<img src="<?php echo e(URL::asset('front/images/psoriasis-icon.webp')); ?>">
					</div>

					<h3 class="alt-services-title font-alt">Psoriasis</h3>

					<div class="alt-services-desc">It is an auto-immune skin disorder that adversely affects the skin. The silvery patches, plaques, redness, and inflammation lead the patient toward a difficult life....</div>

					<div class="alt-services-more">
						<a href="<?php echo e(route('psoriasis')); ?>">Read More</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>`
   
<section class="dsa-india-ayurvedic-sec our-expert" id="types" style="margin-top: 3%;"> 
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="cl-test">
            	<h2 class="hs-line-1 font-alt text-center mb-xs-10 mt-5"><b>What makes Dr. Sharda Ayurveda the best ayurvedic treatment center in India?</b></h2>

                <p>So, how is the best Ayurvedic treatment provided by the experts of Dr. Sharda Ayurveda?

				<li>The ayurvedic treatment aims to remove the root cause of the disease, and does not provide any side effects as the practices are based on a purely natural approach.</li><br>

				<li>The natural methods used in ayurvedic treatment focus to provide long-term results and also work against the reversibility of the disorders.
				</li><br>

				<li>The ayurvedic treatment which is provided by the experts depends upon the requirement of your body and the intensity of your disease. The diagnosis is done thoroughly on the root cause of the disease and then a customised ayurvedic treatment is provided by the professionals.
				</li><br>

				<li>Throughout the treatment journey, the whole team of Dr. Sharda Ayurveda is there to assist you. All your health queries will be answered by the patient advisors as well as Ayurvedic doctors. Your health progress will be noted and each & every detail will be shared with you. 
				</li><br>

				<li>Ayurvedic treatment from Dr. Sharda Ayurveda will keep your treatment confidential to others and transparent to you.
				</li><br>
				
				<li>The diet charts and customised diet and food products will be listed on a diet schedule given to you. The meal plan will be based on your existing condition and according to your body’s nature ( Prakriti).
				</li><br/>
				</p>

				<h2 class="hs-line-1 font-alt text-center mb-xs-10 mt-5"><b>Why we are considered to provide the best Ayurvedic Treatments in India?</b></h2>
				<p>

				<li>Autoimmune disorders are related to the malfunctioning of the immune system. We have a team of professional doctors who deal with such types of diseases.
				</li><br>
				
				<li>Rheumatoid arthritis, Ankylosing spondylitis, Lumbar Spondylosis, osteoarthritis,  and other joint-related disorders like gout, osteoporosis, and spinal issues are resolved with the help of a purely natural approach.
				</li><br>
				
				<li>Furthermore, the disorders related to the respiratory system like asthma, bronchitis, seasonal allergies, and skin problems such as eczema, psoriasis & fungal infections are healed in an effective manner.
				</li><br>
				
				<li>Ayurvedic treatment is provided by professional doctors. With their experience, expertise, and knowledge they help the patient and end their suffering, pain, and discomfort.
				</li><br>
				
				<li>Along with this, we also provide online Ayurvedic doctor consultations which are really useful for people who are unable to visit the clinic due to any reason.
				</li><br>
				
				<li>We have four branches in Punjab, Ludhiana, Mohali, Bathinda, and Sri Mukhatasar Sahib. Ludhiana has our head branch. You can visit or call us for a consultation.
				</li><br>
				
				<li>Let Ayurveda and our Ayurvedic professionals heal you in the best possible way!
				</li><br>
				
				<li>Pick up your phone and call us!
				</li><br>
				</p>
            </div>
        </div>
    </div>
</section>

<section class="sign_up_newsletter">
        <div class="container">
          	<div class="row">
			  	<div class="col-md-5 col-sm-5 col-xs-12">
					<div class="img_box">
						<img src="<?php echo e(URL::asset('front/images/nadi-parikshan-2.webp')); ?>" class="why-choose-us img-fluid" alt="nadi parikshan">
					</div>
				</div>
				<div class="col-md-7 col-sm-7 col-xs-12 ">
					<div class="newsletter_box">
						<div class="title">Analyze Your Dosha</div>
						<div class="form_box">
							<form id="form_contact" action="https://www.drshardaayurveda.com/submit_lp_page.php" method='POST'>
								<input type='hidden' name="subject" value="<?= $routeName; ?>">
								<input type='hidden' name="action" value="user_mail">
								<input type='hidden' name="return_url" value="https://www.drshardaayurveda.com/thanks.html">
								<input type='hidden' name="page_url" value="<?= $actual_link; ?>">
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-12">
										<div class="">
											<label for="fname">Name*
												<abbr class="required" title="required">*</abbr>
											</label>
											<div class="error form_error" id="form-error-fname"></div>
											<input type="text" class="input-text" placeholder="name" name="name" id="fname" required="">
										</div>
									</div>

									<div class="col-md-6 col-sm-6 col-xs-12">
										<div class="form-row-first">
											<label for="email">E-mail*
												<abbr class="required" title="required">*</abbr>
											</label>
										<div class="error form_error" id="form-error-email"></div>
										<input type="email" class="input-text" placeholder="email" name="email" id="email" required="">
									</div>
								</div>

							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="">
									<label for="mobile">Mobile*
										<abbr class="required" title="required">*</abbr>
									</label>
									<div class="error form_error" id="form-error-mobile"></div>
									<input type="number" class="input-text" placeholder="Mobile" name="mobile" id="mobile" required="">
								</div>
							</div>

							<div class="col-md-6 col-sm-6 col-xs-12">
								<div class="">
									<label for="mobile">Description*
										<abbr class="required" title="required">*</abbr>
									</label>
									<div class="error form_error" id="form-error-mobile"></div>
									<textarea rows="1" cols="50" name="description" id="comment" placeholder="Message" rows="9"></textarea>
								</div>
							</div>

							<div class="clr"></div>

							<div class="col-md-6 col-sm-6 col-xs-12" style="text-align:center">
								<div class="form-row">
									<input type="submit"  class="btn btn-lg btn-color Register __web-inspector-hide-shortcut__" id="Register" value="Submit">
								</div>
							</div>
						</div>
								<div class="clr"></div>
							</form>
						</div>
            		</div>
          		</div>
			</div>
        </div>
	</section>
</div>
</div>


<script>
  jQuery(document).ready(function($) {
  	var alterClass = function() {
		var ww = document.body.clientWidth;
		if (ww < 768) {
			console.log('here');
			$('#dynamic-cls').removeClass('row row-cols-2');
		} else if (ww >= 769) {
			$('#dynamic-cls').addClass('row row-cols-2');
		};
	};
	$(window).resize(function(){
		alterClass();
	});
  	//Fire it when the page first loads:
  	alterClass();
});
</script>
   <section class="book_appointment_form" >
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 basic_info">
                    <div class="review_box">
                        <p class="review">For the past 9 years, Dr. Sharda Ayurveda is healing mankind worldwide with ancient effective Ayurvedic herbs. The highly professional experienced team of doctors are pioneers in pulse reading and experienced in treating a patient with Ayurvedic medicines that are formulated and manufactured under the guidance of experts after years of research.</p>
                        <p class="review">Dr. Mukesh Sharda, founder of Dr. Sharda Ayurveda and renowned <a class="green-anchor"href="https://www.drshardaayurveda.com/dr-mukesh-sharda">Ayurvedic Doctor in India</a> is crossing the bars daily by treating more and more people with Ayurveda. Successfully treated more than 10,00,000+ patients of several diseases related to joints, circulatory, kidney, skin, gynae, digestive, sexual, liver, endocrine, neurological, respiratory, urinary and de-addiction and many other acute and chronic diseases.</p>
                    </div>
                    <hr>
                    <button type="button" class="btn btn-primary book-button" data-toggle="modal" data-target=".bd-example-modal-xl" style="background-color:#d49925 !important">
                      Book a consultation with Dr Sharda Ayurveda
                    </button>
                </div>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\drshardaayurveda\resources\views/frontend/services/index.blade.php ENDPATH**/ ?>