<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>

<?php $__env->startSection('content'); ?>
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h1 class="ser-heading mt-4">Ayurvedic Treatmet for Chronic Fatigue Syndrome</h1>
                    <p style="font-size:18px;">Overcome Your Fatigue with Ayurveda</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">
                            Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/chronic-fatigue-syndrome">Chronic Fatigue Syndrome</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#TREATMENTS">treatments</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="definition">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="<?php echo e(URL::asset('front/images/chronic-fatigue-syndrome.webp')); ?>" class="why-choose-us img-fluid" alt="Women Suffering From Chronic Fatigue Syndrome" title="Women Suffering From Chronic Fatigue Syndrome">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h2 class="heading1">What is <b>Chronic Fatigue Syndrome?</b></h2>
                <p>
                    Chronic Fatigue Syndrome is also called Myalgic Encephalomyelitis, a chronic condition where patients complain of extreme fatigue and tiredness during his/ her lifespan. It is a debilitating disease associated with excessive exertion accompanied by many variable signs and symptoms. It is more commonly seen in women. This may be due to environmental or genetic factors. The fatigue can last till 6 months too.
                    <br> 
                    It gets worsens with any activity and does not improve with rest. Chronic fatigue affects the physical and mental health of an individual. It is described as a long-term illness that affects the whole body's functioning. It even gets difficult to perform daily activities and a person becomes dependent.
                    <br>
                    In Ayurveda, it can be compared with Vata imbalance. This aggravation generates a negative impact all over the body.  This condition makes the patient feel irritated, dry, and lacking energy. This aggravation of Vata leads to a deficiency of Rasa Dhatu and Ojas in the body further causing the disease to mark its place. Ayurveda, the ancient Indian system of medicine, offers a holistic approach to the treatment of Chronic Fatigue Syndrome. 
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                    <p>Ayurvedic treatment for CFS focuses on rebalancing the body's energies, strengthening the immune system, and improving vitality through herbal remedies, dietary changes, stress management techniques, and rejuvenating therapies.
                    </p>
                    <div class="btn-ayurvedic">
                        <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                    </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec our-expert" style="background-color:#f3f3f3" id="causes">
    <div class="container-fluid" >
        <div class="row cut-row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Main Causes of <b>Chronic Fatigue Syndrome </b></h2>
                    <li>
                        <b>Viral Infections-</b> Sometimes to some Individuals chronic fatigue syndrome develops after a viral infection. So some viruses can trigger the disorder.
                    </li>
                    <li>
                        <b>Weak Immune System-</b> When the functioning of the Immune system gets impaired and Immunity is low it can cause fatigue syndrome. 
                    </li>
                    <li>
                        <b>Traumatic Event-</b> A person may have experienced an Injury, Surgery, or have gone through physical or emotional trauma. 
                    </li>
                    <li>
                        <b>Stress-</b> High levels of stress for any reason can disturb the brain and digestive functioning causing fatigue syndrome.
                    </li>
                    <li>
                        <b>Hormonal Imbalance-</b> Hormonal changes and disturbances in the body cause fatigue and tiredness. 
                    </li>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="<?php echo e(URL::asset('front/images/chronic-fatigue-Syndrom-Symptoms.webp')); ?>" class="img-fluid"alt="Image Showing Symptoms of Chronic Fatigue Syndrome" title="Image Showing Symptoms of Chronic Fatigue Syndrome">
        </div>
        <div class="cvn sym" >
            <h2 class="client-test-title"style="font-size: 24px!important; color: #244236;">
                Understanding <b>Symptoms of Chronic Fatigue Syndrome </b>
            </h2>
            <li><b style="font-weight: bold;">Fatigue-</b> 
                The most common symptom is that a patient will always be in a state of fatigue, tiredness, and restlessness despite taking rest for several hours.
            </li>
            <li><b style="font-weight: bold;">Loss of Memory and Concentration-</b> 
                 It gets hard for an Individual to remember things easily and loses the ability to concentrate. 
            </li>
            <li><b style="font-weight: bold;">Headache-</b> 
                A patient experiences headaches that may continue for several days despite taking rest the whole day long.
            </li> 
            <li><b style="font-weight: bold;">Disturbed Sleep-</b>
                It gets hard for an Individual to have a proper sleep pattern and faces decreased sleep.
            </li>
            <li><b style="font-weight: bold;">Joint Pain-</b>
                Muscle, joint pain, and numbness in hands and feet.
            </li>
        </div>
    </div>
</div>

<div class="split"></div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <h2 class="client-test-title"style="font-size: 24px!important;" text-al>
            <b>Chronic Fatigue Syndrome</b> Ayurvedic Treatment
        </h2>
            <p style="text-align:center;">
                    Treatment of Chronic Fatigue Syndrome can be challenging, but Ayurvedic treatment options can help manage its symptoms. These may include a combination of lifestyle modifications, and herbal medicines aimed at reducing fatigue, improving sleep, and enhancing overall well-being.
                </p>
                <p style="text-align:center;">
                    Panchakarma, an Ayurvedic detoxification and rejuvenation therapy, can play a significant role in CFS treatment. This specialized Ayurvedic treatment for Chronic Fatigue syndrome aims to remove accumulated toxins, restore energy balance, and improve overall vitality, offering relief from fatigue and enhancing overall well-being.   
                </p> 
            <div class="row cut-row" >
                <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                    <ul>
                        The following are the methods of Panchakarma:-
                        <li><b style="font-weight: bold;">Abhyanga-</b> 
                            Oil massage to relieve stress and aggravated Vata.
                        </li>
                        <li><b style="font-weight: bold;">Shirodhara-</b> 
                             Pouring of medicated oil or buttermilk or medicated milk overhead to relieve Vata. 
                        </li>
                    </ul>
                    <p class="im3">
                        According to Ayurveda, fatigue is caused due to low gastric fire and weakness of the liver. Low or weak gastric fire leads to Ama formation and Ama is the root cause of all diseases in the body.
                        A healthy diet that contains a variety of fresh, nutrient-rich foods which helps reduce feelings of fatigue. You can include foods like kale, oats, and watermelon.
                    </p>
                    <p class="im3">
                        In Ayurveda, specific herbs and formulations are used to address CFS symptoms and are considered a potent treatment for Chronic Fatigue Syndrome. Ayurvedic Herbs are known for their adaptogenic properties, which help combat fatigue, enhance energy levels, and support overall well-being.
                    </p>
                    <p class="im3">
                        Making certain lifestyle modifications can be beneficial in managing CFS. This may include following a regular sleep schedule, incorporating gentle exercises such as yoga or walking, practicing stress reduction techniques like meditation or deep breathing, pranayama, and maintaining a balanced diet to support optimal energy levels.
                    </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="G2SXEXmtMqY" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="<?php echo e(URL::asset('front/images/G2SXEXmtMqY.webp')); ?>" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('<?php echo e(URL::asset('front/images/video-icon.png')); ?>') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
    </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Patient's <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                      <div class="item">
                        <div class="testimonial-content">
                            <img src="<?php echo e(URL::asset('front/images/patient-testimonial-female.webp')); ?>" class="img-fluid" alt="Komal Mishra CFS Patient" >
                            <h3 class="usrname">Komal Mishra</h3>
                            <p class="desig">CFS Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was in a very bad state. I couldn’t understand what was happening to me. Despite several tests, nobody could make the diagnosis. I was helpless. At last, I thought of adopting Ayurveda. When I first came to Dr. Sharda Ayurveda, they did the tests and I was diagnosed with chronic fatigue syndrome. I started with the Ayurvedic treatment and with the modification in diet & lifestyle, I got recovered in around 8 months. I am finally fine now. I have resumed my normal life. Thank you so much, Dr. Sharda Ayurveda.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
    <div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                               Is chronic fatigue syndrome a lifelong illness?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                            Yes, it may last for months or years if timely Ayurvedic treatment for Chronic fatigue syndrome is not chosen.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                               What else does chronic fatigue syndrome lead to?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                            It can even lead to liver failure, anemia, several infections, kidney disease, and neurodegeneration.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                How does chronic fatigue syndrome affect daily life?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                            Extreme tiredness and other physical symptoms can make it hard to carry out everyday activities. You may have to make some major lifestyle changes. ME/CFS can also affect your mental and emotional health, and have a negative effect on your self-esteem.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                              Does Ayurveda have treatment for chronic fatigue syndrome?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                            Yes, Ayurveda has many herbs and Ayurvedic medicines that have tremendous results in chronic fatigue syndrome. The treatment has no side effects and is considered to be safe. Also, Ayurvedic remedies for Chronic Fatigue Syndrome works wonders for the affected people.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                 Does weather affect chronic fatigue syndrome?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                            Yes for many patients suffering from Chronic Fatigue Syndrome, the winter season is the most difficult of the year as lack of sunlight, i.e., Vitamin D results in fatigue. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    Which Vitamins are best for chronic fatigue syndrome?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                                Folic Acid, Vitamin C, and Vitamin D are the best vitamins to treat chronic fatigue syndrome.
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                               Can chronic fatigue syndrome affect children?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                           Yes, there is no specific age group that gets affected by chronic fatigue syndrome
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Do all the patients have the same symptoms?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                            No, symptoms always vary from patient to patient at different stages as no two individuals are alike. Every human has a different body type and the affected condition may vary accordingly. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                How is chronic fatigue syndrome diagnosed?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                            There are no blood investigations for detecting this syndrome. A patient should consult a doctor and share in deep what symptoms he/she is going through. Ayurvedic medicine for chronic fatigue syndrome has life-long and effective results.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                How to regain lost energy?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                            <ul>
                                <li>Quit smoking and consumption of alcohol.</li>
                                <li>Meditate for managing stress and frustration.</li>
                                <li>Have a nutritious diet. Include more nuts and fruits in your meals.</li>
                                <li>Follow a regular sleep schedule.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<?php echo $__env->make('../partials/frontend/blogs_post_section', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php echo $__env->make('../partials/frontend/form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Chronic Fatigue Syndrom",
    "item": "https://www.drshardaayurveda.com/chronic-fatigue-syndrom"  
  }]
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "Is chronic fatigue syndrome a lifelong illness?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it may last for months or years if timely Ayurvedic treatment for Chronic fatigue syndrome is not chosen."
    }
  },{
    "@type": "Question",
    "name": "What else does chronic fatigue syndrome lead to?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It can even lead to liver failure, anemia, several infections, kidney disease, and neurodegeneration."
    }
  },{
    "@type": "Question",
    "name": "How does chronic fatigue syndrome affect daily life?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Extreme tiredness and other physical symptoms can make it hard to carry out everyday activities. You may have to make some major lifestyle changes. ME/CFS can also affect your mental and emotional health, and have a negative effect on your self-esteem."
    }
  },{
    "@type": "Question",
    "name": "Does Ayurveda have treatment for chronic fatigue syndrome?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, Ayurveda has many herbs and Ayurvedic medicines that have tremendous results in chronic fatigue syndrome. The treatment has no side effects and is considered to be safe. Also, Ayurvedic remedies for Chronic Fatigue Syndrome works wonders for the affected people."
    }
  },{
    "@type": "Question",
    "name": "Does weather affect chronic fatigue syndrome?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes for many patients suffering from Chronic Fatigue Syndrome, the winter season is the most difficult of the year as lack of sunlight, i.e., Vitamin D results in fatigue."
    }
  },{
    "@type": "Question",
    "name": "Which Vitamins are best for chronic fatigue syndrome?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Folic Acid, Vitamin C, and Vitamin D are the best vitamins to treat chronic fatigue syndrome."
    }
  },{
    "@type": "Question",
    "name": "Can chronic fatigue syndrome affect children?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, there is no specific age group that gets affected by chronic fatigue syndrome."
    }
  },{
    "@type": "Question",
    "name": "Do all the patients have the same symptoms?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "No, symptoms always vary from patient to patient at different stages as no two individuals are alike. Every human has a different body type and the affected condition may vary accordingly."
    }
  },{
    "@type": "Question",
    "name": "How is chronic fatigue syndrome diagnosed?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "There are no blood investigations for detecting this syndrome. A patient should consult a doctor and share in deep what symptoms he/she is going through. Ayurvedic medicine for chronic fatigue syndrome has life-long and effective results."
    }
  },{
    "@type": "Question",
    "name": "How to regain lost energy?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Quit smoking and consumption of alcohol. Meditate for managing stress and frustration. Have a nutritious diet. Include more nuts and fruits in your meals. Follow a regular sleep schedule."
    }
  }]
}
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\drshardaayurveda\resources\views/frontend/services/chronic_fatigue_syndrome.blade.php ENDPATH**/ ?>