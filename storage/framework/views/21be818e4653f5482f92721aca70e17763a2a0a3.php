<header>
    <div class="top-bar-sec">
        <div class="container-fluid">
            <div class="row cust-new">
                <div class="col-md-12">
                    <div class="top-bar">
                        <p><marquee>Authentic effective Ayurvedic Medicines- Dr. Sharda Ayurveda
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Book an appointment call us on: +91-98760-35500
                        </marquee></p>
                    </div>
                </div>
            </div> 
        </div>
    </div>
    <div class="logo-sec">
        <div class="container-fluid">
            <div class="row custom-logo-sec">
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12" data-aos="zoom-in" data-aos-duration="3000">
                    <span itemscope itemtype="http://schema.org/Organization">
                        <link itemprop="url" href="https://www.drshardaayurveda.com/">
                    <div class="tp-social-icon">
                        <a id="fb"itemprop="sameAs"href="https://www.facebook.com/DrShardaAyurveda" target="_blank"rel="noopener"><i class="fa fa-facebook"></i></a>
                        <a id="twitter"itemprop="sameAs"href="https://twitter.com/shardaayurveda" target="_blank"rel="noopener"><i class="fa fa-twitter"></i></a>
                        <a id="YouTube"itemprop="sameAs"href="https://www.youtube.com/c/DrShardaAyurveda" target="_blank"rel="noopener"><i class="fa fa-youtube"></i></a>
                        <a id="linkedIn"itemprop="sameAs"href="https://www.linkedin.com/company/dr-sharda-ayurveda" target="_blank"rel="noopener"><i class="fa fa-linkedin"></i></a>
                        <a id="Instagram"itemprop="sameAs"href="https://www.instagram.com/DrShardaAyurveda/" target="_blank"rel="noopener"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12" data-aos="zoom-in" data-aos-duration="1000">
                    <div class="logo-img">
                        <a href="/"><img src="<?php echo e(URL::asset('front/images/dr-sharda-logo.webp')); ?>" class="cp-logo" alt="dr sharda ayurveda clinic logo"></a>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12 px-0">
                    <div class="search-container" data-aos="zoom-out-down" data-aos-duration="3000">
                        <form action="<?php echo e(route('search')); ?>">
                            <input type="text" placeholder="Search..." name="search">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="navbar-menu">
        <div class="container-fluid">
            <div class="row cut-nav-row">
                <div class="col-md-12 px-0">
                    <nav class="navbar navbar-expand-lg navbar-light ">

                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav scrollable-nav-menu">
                                <li class="nav-item active">
                                    <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="<?php echo e(route('about-us')); ?>">About Us</a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="<?php echo e(route('about-us')); ?>">About Us</a></li>
                                        <li><a class="dropdown-item" href="<?php echo e(route('dr-mukesh-sharda')); ?>">Dr Mukesh Sharda</a></li>
                                        <!-- <li><a class="dropdown-item" href="<?php echo e(route('ayurvedic-online-consultation')); ?>">Online Consultation</a></li> -->
                                    </ul>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Treatments</a>
                                    <ul class="dropdown-menu">
                                    <li>
                                            <a class="dropdown-item" href="#"> Joint Pain &raquo </a>
                                            <ul class="submenu dropdown-menu scrollable-menu">
                                                <li><a class="dropdown-item" href="<?php echo e(route('rheumatoid-arthritis')); ?>">Rheumatoid Arthritis</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('ankylosing-spondylitis')); ?>">Ankylosing Spondylitis</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('back-pain')); ?>">Back Pain</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('cervical')); ?>">Cervical</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('fibromyalgia')); ?>">Fibromyalgia</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('gout')); ?>">Gout</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('hip-joint-pain')); ?>">Hip Joint Pain</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('knee-pain')); ?>">Knee Pain</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('lumbar-spondylosis')); ?>">Lumbar Spondylosis</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('neck-pain')); ?>">Neck Pain</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('osteoarthritis')); ?>">Osteoarthritis</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('osteoporosis')); ?>">Osteoporosis</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('post-viral-arthritis')); ?>">Post-Viral Arthritis</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('psoriatic-arthritis')); ?>">Psoriatic Arthritis</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('reactive-arthritis')); ?>">Reactive Arthritis</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('uric-acid')); ?>">Uric Acid</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="#"> Circulatory</a>
                                            <ul class="submenu dropdown-menu">
                                                <li><a class="dropdown-item" href="<?php echo e(route('anemia')); ?>">Anemia</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('blood-pressure')); ?>">Blood Pressure</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('coronary-artery-disease')); ?>">Coronary Artery Disease</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('high-cholesterol')); ?>">High Cholesterol</a></li>                                                
                                            </ul>
                                        </li>

                                        <li>
                                            <a class="dropdown-item" href="#"> De Addiction &raquo </a>
                                            <ul class="submenu dropdown-menu">
                                                <li><a class="dropdown-item" href="<?php echo e(route('alcohol-de-addiction')); ?>">Alcohol De Addiction</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('drug-de-addiction')); ?>">Drug De Addiction</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('smoking-de-addiction')); ?>">Smoking De Addiction</a></li>
                                            </ul>
                                        </li>

                                        <li>
                                            <a class="dropdown-item" href="#"> Digestive &raquo </a>
                                            <ul class="submenu dropdown-menu">
                                                <li><a class="dropdown-item" href="<?php echo e(route('abdominal-pain')); ?>">Abdominal Pain</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('acidity')); ?>">Acidity</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('chronic-fatigue-syndrome')); ?>">Chronic Fatigue Syndrome</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('constipation')); ?>">Constipation</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('flatulence')); ?>">Flatulence</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('ibs')); ?>">Irritable Bowel Syndrome</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('piles')); ?>">Piles</a></li>   
                                                <li><a class="dropdown-item" href="<?php echo e(route('ulcerative-colitis')); ?>">Ulcerative Colitis</a></li>                              
                                            </ul>
                                        </li>

                                        <li>
                                            <a class="dropdown-item" href="#">Endocrine &raquo</a>
                                            <ul class="submenu dropdown-menu">
                                                <li><a class="dropdown-item" href="<?php echo e(route('diabetes')); ?>">Diabetes</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('obesity')); ?>">Obesity</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('thyroid')); ?>">Thyroid</a></li>
                                            </ul>
                                        </li>

                                        <li>
                                            <a class="dropdown-item" href="#"> Eczema </a>
                                            <ul class="submenu dropdown-menu">
                                                <li><a class="dropdown-item" href="<?php echo e(route('types-of-eczema')); ?>">Types of Eczema</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('inverse-psoriasis-treatment')); ?>">Inverse psoriasis</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('stasis-dermatitis')); ?>">Stasis Dermatitis</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('contact-dermatitis')); ?>">Contact Dermatitis</a></li>
                                            </ul>
                                        </li>

                                        <li>
                                            <a class="dropdown-item" href="#"> Gynae &raquo </a>
                                            <ul class="submenu dropdown-menu">
                                                <li><a class="dropdown-item" href="<?php echo e(route('leucorrhoea')); ?>">Leucorrhoea</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('menopause')); ?>">Menopause</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('pcod-pcos')); ?>">PCOD-PCOS</a></li>
                                            </ul>
                                        </li>

                                        <li>
                                            <a class="dropdown-item" href="#"> Kidney &raquo </a>
                                            <ul class="submenu dropdown-menu">
                                                <li><a class="dropdown-item" href="<?php echo e(route('kidney-failure')); ?>">Kidney Failure</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('kidney-stones')); ?>">Kidney Stones</a></li>
                                            </ul>
                                        </li>

                                        <li>
                                            <a class="dropdown-item" href="#"> Liver &raquo </a>
                                            <ul class="submenu dropdown-menu">
                                                <li><a class="dropdown-item" href="<?php echo e(route('fatty-liver')); ?>">Fatty Liver</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('jaundice')); ?>">Jaundice</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('liver-cirrhosis')); ?>">Liver Cirrhosis</a></li>
                                            </ul>
                                        </li>

                                        <li>
                                            <a class="dropdown-item" href="#"> Neurological &raquo </a>
                                            <ul class="submenu dropdown-menu">
                                                <li><a class="dropdown-item" href="<?php echo e(route('anxiety-disorder')); ?>">Anxiety Disorder</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('depression')); ?>">Depression</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('insomnia')); ?>">Insomnia</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('migraine')); ?>">Migraine</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('sciatica-pain')); ?>">Sciatica Pain</a></li>
                                            </ul>
                                        </li>

                                        <li>
                                            <a class="dropdown-item" href="#"> Respiratory &raquo </a>
                                            <ul class="submenu dropdown-menu">
                                                <li><a class="dropdown-item" href="<?php echo e(route('asthma')); ?>">Asthma</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('chronic-laryngitis')); ?>">Laryngitis</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('sinusitis')); ?>">Sinusitis</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('rhinitis')); ?>">Rhinitis</a></li>
                                            </ul>
                                        </li>

                                        <li>
                                            <a class="dropdown-item" href="#"> Sexual Disorders &raquo </a>
                                            <ul class="submenu dropdown-menu">
                                                <li><a class="dropdown-item" href="<?php echo e(route('erectile-dysfunction')); ?>">Erectile Dysfunction</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('oligospermia')); ?>">Oligospermia</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('premature-ejaculation')); ?>">Premature Ejaculation</a></li>
                                            </ul>
                                        </li>

                                        <li>
                                            <a class="dropdown-item" href="#"> Skin &raquo </a>
                                            <ul class="submenu dropdown-menu">
                                                <li><a class="dropdown-item" href="<?php echo e(route('eczema')); ?>">Eczema</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('fungal-infection')); ?>">Fungal Infection</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('psoriasis')); ?>">Psoriasis</a></li>                                              
                                            </ul>
                                        </li>
                                                                                                                                   
                                        <li>
                                            <a class="dropdown-item" href="#"> Urinary &raquo </a>
                                            <ul class="submenu dropdown-menu">
                                                <li><a class="dropdown-item" href="<?php echo e(route('cystitis')); ?>">Cystitis</a></li>
                                                <li><a class="dropdown-item" href="<?php echo e(route('urinary-tract-infection')); ?>">Urinary Tract Infection</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Locations</a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="<?php echo e(route('bathinda')); ?>">Bathinda</a>
                                        <a class="dropdown-item" href="<?php echo e(route('ludhiana')); ?>">Ludhiana</a>
                                        <a class="dropdown-item" href="<?php echo e(route('mohali')); ?>">Mohali</a>
                                        <a class="dropdown-item" href="<?php echo e(route('muktsar')); ?>">Muktsar</a>
                                    </div>
                                </li>
                                <!-- <li class="nav-item">
                                    <a class="nav-link" href="<?php echo e(route('contact-us')); ?>">Contact Us</a>
                                </li> -->
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="<?php echo e(route('contact-us')); ?>">Contact Us</a>
                                    <ul class="dropdown-menu">
                                        <li><a class="dropdown-item" href="<?php echo e(route('contact-us')); ?>">Contact Us</a></li>
                                        <li><a class="dropdown-item" href="<?php echo e(route('ayurvedic-online-consultation')); ?>">Online Consultation</a></li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo e(route('testimonial')); ?>">Testimonials</a>
                                </li>
                                <!--<li class="nav-item">
                                    <a class="nav-link" href="javascript:void(0);">Gallery</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="javascript:void(0);">Treatment</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="javascript:void(0);">Store</a>
                                </li>-->
                                <li class="nav-item">
                                    <a class="nav-link" href="<?php echo e(route('blogs')); ?>">Blogs</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <div class="dsa-phone">
                        <a href="tel:+919876035500" class="phone-btn"> <i class="fa fa-phone" aria-hidden="true"></i> +91-9876035500</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header><?php /**PATH D:\xampp\htdocs\drshardaayurveda\resources\views////partials/frontend/main-header.blade.php ENDPATH**/ ?>