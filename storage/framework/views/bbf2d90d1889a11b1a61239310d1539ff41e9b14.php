<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>

<?php $__env->startSection('content'); ?>
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >HIP JOINT PAIN</h3>
                    <p style="font-size: 24px;">Protect your joints with timely treatment</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/hip-joint-pain">Hip Joint Pain</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#treatment">TREATMENT <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#advantages">ADVANTAGES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="<?php echo e(URL::asset('front/images/hip-joint-pain-treatment.webp')); ?>" class="why-choose-us img-fluid" alt="why choose">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1">Hip Joint Pain Ayurvedic Treatment</h1>
                <p>
                The hip joint is a very stable and strong joint of the body and pain in the hip joint occurs due to different pathology developed in the body. It can be due to the wear and tear of soft tissues. It is also known as the ball and socket joint that performs different types of movement of legs and allows the fluid to move in a joint. It is called so because the top of the thigh bone is shaped like a ball. This joint supports legs and upper body parts. The hip joint is held together by a covering of muscle which is known as tendons. These muscles and tendons form a capsule around the joint and support movements, inside the capsule there is synovium which lubricates the joints and keeps cartilage healthy.
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">.....</span>
                </p>
                <span id="more-content">
                    <p>
                    Any problem to muscles cartilage, bone, ligaments disease leads to hip joint pain. Cartilage with age, wear and tear causes difficulty and pain in walking. According to Ayurveda hip, joint pain is caused due to increase in Vata in the body. It is common in both sex, but more common in women. <strong>Hip joint pain Ayurvedic treatment</strong> has the most effective and sure life-long results.
                    </p>
                    <div class="btn-ayurvedic">
                        <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                    </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Hip Joint Pain</b></h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product" style="background-color:#f3f3f3" id="symptoms">
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Arthritis
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                Like osteoarthritis, there is inflammation of the hip joint and breakdown of cartilage that cushions your hip bones.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Hip Fracture
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                            Due to fall or with age bones become brittle, weak that leads to fracture of hip easily.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Age Factor
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                                As we grow older degeneration occurs in joints this degeneration change can cause hip joint pain.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Tendinitis
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                                Inflammation of tendons that causes pain in muscles or tendons during movement.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                Obesity
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                            If the person suffering from hip pain is obese, the weight and stress are more on the affected joint, and over activity thus gets affected.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="<?php echo e(URL::asset('front/images/hip-joint-pain-symptoms.webp')); ?>" class="img-fluid"alt="hip joint pain symptoms">
        </div>
        <div class="cvn sym" >
            <h3><span>Symptoms of</span> Hip Joint Pain </h3>
            <p class="sub"><img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="2.png">Pain in Hip Joint</p>
            <p>Discomfort or pain in the thigh, inside hip joint, groin, outside of hip joint, buttocks is felt.</p>
            <p class="sub"><img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="2.png">Difficulty in Sleeping</p>
            <p>Difficulty sleeping on the hip due to increased pain. </p>
            <p class="sub"><img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="2.png">Warmth over Hip</p>
            <p>Warmness in joints due to any infection. </p>
            <p class="sub"><img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="2.png">Loss of Motion of Hip</p>
            <p>It is the reduced or restricted movement of the hip joint. </p>
            <p class="sub"><img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="2.png">Tenderness</p>
            <p>Tenderness of the hip in which the patient is unable to walk properly. </p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="advantages">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Benefits of Ayurvedic <b>Treatment for Hip Joint Pain</b></h2>
                <p class="im3">
                    <img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="3.png">
                    According to Ayurveda, hip pain can be a symptom of Kati shool, caused by vitiation of Vata dosha.
                </p>
                <p class="im3">
                    <img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="3.png">
                    To treat hip pain patient is advised to avoid strenuous activities and massage of lower back and lower limb
                </p>
                <p class="im3">
                    <img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="3.png">
                    A person should correct their poor posture and should avoid prolonged standing, kneeling, and squatting.
                </p>
                <p class="im3">
                    <img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="3.png">
                    It is advised to take 1 string of garlic daily empty stomach
                </p>
                <p class="im3">
                    <img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="3.png">
                    Ayurvedic medicines help to cure the physical as well as a mental state of an Individual to maintain the wellness of mind, body, and soul.
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="3heIPzMB2Zw" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="<?php echo e(URL::asset('front/images/3heIPzMB2Zw.webp')); ?>" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('<?php echo e(URL::asset('front/images/video-icon.png')); ?>') no-repeat;">
                            </div>
                        </div>
                    </div>    
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                How do I know if my hip pain is serious?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                        In hip joint pain you will be unable to move your leg or hip, unable to bear the weight of the affected leg, you may have fever due to an infection on the affected part. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Is walking advised in hip pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                            Walking is good for hip pain but it should be slow. Running and jumping are strictly avoided in hip pain
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                Why does sleeping on the side hurts in hip joint pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                        Sleeping on the affected side will hurt the hip joint because it will put pressure on the affected part due to which pain increases and the patient is unable to sleep.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                What are the symptoms of hip arthritis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                        Hip arthritis is affected by inflammatory arthritis which is painful and stiff. Pain is worse in the morning or after sitting or resting for a while
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                What type of exercise is beneficial in hip joint pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                        Exercise is advised daily to strengthen the hip muscles, you can do knee lift, external hip rotation, and lower backstretch. Exercise is itself a treatment for hip joint pain
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    What type of food should be avoided in hip joint pain?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                            Processed food like chips, baked goods, fast food, omega 6 fatty acids are strictly avoided in hip joint pain. 
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                What are the risk factors of hip replacement?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                        The most common risk of hip replacement is 4clot and infection. A clot that mostly occurs is called DVT (deep vein thrombosis) 1in which a blood clot develops in a vein deep within the body. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Can Ayurveda cure joint pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                        Ayurveda provides the most effective treatment for healing the whole body suffering from joint pain. By changing dietary habits, lifestyle modifications, yoga any joint disease can be cured. Ayurvedic medicine for hip joint pain works wonders without any surgery.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                What are the worst foods to eat if you have arthritis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                        Red meat, high-fat dairy and cheese, salt, sugary Sweetened beverage, fried and canned food. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                How can I increase my hip mobility?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                        Lie down on your back and bring your legs up then using a strap or band, keep one leg straight up while slowly lowering the other to the floor. Repeat 5 times on each leg once a day in the morning.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $__env->make('../partials/frontend/blogs_post_section', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php echo $__env->make('../partials/frontend/form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Hip Joint Pain",
    "item": "https://www.drshardaayurveda.com/hip-joint-pain"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "How do I know if my hip pain is serious?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "In hip joint pain you will be unable to move your leg or hip, unable to bear the weight of the affected leg, you may have fever due to an infection on the affected part."
    }
  },{
    "@type": "Question",
    "name": "Is walking advised in hip pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Walking is good for hip pain but it should be slow. Running and jumping are strictly avoided in hip pain."
    }
  },{
    "@type": "Question",
    "name": "Why does sleeping on the side hurts in hip joint pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Sleeping on the affected side will hurt the hip joint because it will put pressure on the affected part due to which pain increases and the patient is unable to sleep."
    }
  },{
    "@type": "Question",
    "name": "What are the symptoms of hip arthritis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Hip arthritis is affected by inflammatory arthritis which is painful and stiff. Pain is worse in the morning or after sitting or resting for a while."
    }
  },{
    "@type": "Question",
    "name": "What type of exercise is beneficial in hip joint pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Exercise is advised daily to strengthen the hip muscles, you can do knee lift, external hip rotation, and lower backstretch. Exercise is itself a treatment for hip joint pain."
    }
  },{
    "@type": "Question",
    "name": "What type of food should be avoided in hip joint pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Processed food like chips, baked goods, fast food, omega 6 fatty acids are strictly avoided in hip joint pain."
    }
  },{
    "@type": "Question",
    "name": "What are the risk factors of hip replacement?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The most common risk of hip replacement is 4clot and infection. A clot that mostly occurs is called DVT (deep vein thrombosis) 1in which a blood clot develops in a vein deep within the body."
    }
  },{
    "@type": "Question",
    "name": "Can Ayurveda cure joint pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ayurveda provides the most effective treatment for healing the whole body suffering from joint pain. By changing dietary habits, lifestyle modifications, yoga any joint disease can be cured. Ayurvedic medicine for hip joint pain works wonders without any surgery."
    }
  },{
    "@type": "Question",
    "name": "What are the worst foods to eat if you have arthritis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Red meat, high-fat dairy and cheese, salt, sugary Sweetened beverage, fried and canned food."
    }
  },{
    "@type": "Question",
    "name": "How can I increase my hip mobility?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Lie down on your back and bring your legs up then using a strap or band, keep one leg straight up while slowly lowering the other to the floor. Repeat 5 times on each leg once a day in the morning."
    }
  }]
}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\drshardaayurveda\resources\views/frontend/services/hip_joint_pain.blade.php ENDPATH**/ ?>