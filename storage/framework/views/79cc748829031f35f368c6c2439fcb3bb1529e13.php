<?php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$routeName = \Route::currentRouteName();
?>
<style>#errmsgMobile,#errmsgEmail,.error-form{color: red;} .success-form{color: green;} .display-none{display: none;}</style>
<section class="get-in-touch-sec">
    <div class="container-fluid">
        <div class="row custom-get">
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-4" data-aos="zoom-in" data-aos-duration="3000">
                <h2 class="">Get in Touch</h2>
                <div class="phone-consult">
                    <span><a href="tel:+919876035500" class="consult-ico"><img src="<?php echo e(URL::asset('front/images/6.png')); ?>"></span>Phone Consultation</a>
                </div>
                <div class="phone-consult">
                    <span> <a href="https://www.drshardaayurveda.com/ayurvedic-online-consultation" class="consult-ico"><img src="<?php echo e(URL::asset('front/images/7.png')); ?>"></span>Online Consultation</a>
                </div>
                <div class="phone-consult">
                    <span><a href="tel:+919876035500" class="consult-ico"><img src="<?php echo e(URL::asset('front/images/8.png')); ?>"></span>Clinic Consultation</a>
                </div>
                <div class="email crsem">
                  <span><a href="mailto:info@drshardaayurveda.com" class="email-icon"><i class="fa fa-envelope consult-ico" aria-hidden="true"></i></span>info@drshardaayurveda.com</a>
                </div>
                <div class="phone">
                   <span><a href="tel:+919876035500" class="phone-icon consult-ico"><i class="fa fa-phone" aria-hidden="true"></i></span>+91 9876035500</a>
                </div>
                <br>
                <p>Overseas visitors can contact us on:<br>   <a class="green-anchor"href="tel: +919878680926" class="phone-btn"> <i class="fa fa-phone" aria-hidden="true"></i>  +91-9878680926</a></p>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="get-form" data-aos="zoom-in" data-aos-duration="3000">
                    <h3>Book an Appointment</h3>
                    <span class="error-form display-none">Failed !! please check required fields&#8230;.</span> <br/>
                    <span class="success-form display-none">Thank you for submitting your details, Our patient advisor will contact you soon.</span>
                    <form  id="form_contact" action="https://www.drshardaayurveda.com/submit_lp_page_google_captcha.php" method='POST'>
                        <input type='hidden' name="subject" value="<?= $routeName; ?>">
                        <input type='hidden' name="action" value="user_mail">
                        <input type='hidden' name="return_url" value="https://www.drshardaayurveda.com/thanks.html">
                        <input type='hidden' name="page_url" value="<?= $actual_link; ?>">

                        <input type="text" id="name" name="name" placeholder="Name" required><br>
                        <input type="number" min="0" class="mobile" id="phone" name="mobile" placeholder="Phone" pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}" required>
                        <span id="errmsgMobile"></span><br>
                        <input type="email" id="email" name="email" placeholder="Email"><span id="errmsgEmail"></span><br>
                        <textarea id="story" name="description" rows="5" cols="33" placeholder="Message"></textarea><br>
                        <div class="captcha">
<!--                            <span><?php echo captcha_img(); ?></span>
                            <button type="button" class="btn btn-success btn-refresh"><i class="fa fa-refresh"></i></button>
                        </div><br>
                         <input id="gcaptcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha"><br> -->
                        <!--<input type="submit" value="Submit" name="submit" class="submit">-->
                        <!--Local-->
                        <!--<button type="submit" class="btn btn-lg btn-color g-recaptcha" data-sitekey="6LfEsOclAAAAAJsozhE_palqbg2un0C6HBMAtacd" data-callback='onSubmit' data-action='submit' value="Submit">Submit</button>-->
                        <!--Live-->
                        <button type="submit" class="btn btn-lg btn-color g-recaptcha" data-sitekey="6LcdxeglAAAAAI4u0cH7jiY5S3y-tEG6LqocURj2" data-callback='onSubmit' data-action='submit' value="Submit">Submit</button>
                    </form> 
                </div>
            </div>
        </div>
    </div>
</section><?php /**PATH D:\xampp\htdocs\drshardaayurveda\resources\views////partials/frontend/form.blade.php ENDPATH**/ ?>