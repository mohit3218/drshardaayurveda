<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Dr Sharda Ayurveda">
<?php echo SEOMeta::generate(); ?>

<?php echo OpenGraph::generate(); ?>

<?php echo Twitter::generate(); ?>

<meta name="theme-color" content="#d49925"/>
<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('front/css/style.css')); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('front/css/prefix.css')); ?>">

<link rel="stylesheet" type="text/css" href="<?php echo e(URL::asset('front/css/bootstrap.min.css')); ?>" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

<!--<link href="<?php echo e(URL::asset('front/css/aos.css')); ?>" rel="stylesheet">-->
<script src="<?php echo e(URL::asset('front/js/aos.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('front/js/jquery-3.5.1.min.js')); ?>" ></script>

<script type='text/javascript' src="<?php echo e(URL::asset('front/js/popper.min.js')); ?>" defer></script>
<script type='text/javascript' src="<?php echo e(URL::asset('front/js/bootstrap.min.js')); ?>" defer ></script>



<link rel="stylesheet" href="<?php echo e(URL::asset('front/css/owl.carousel.min.css')); ?>"/>
<script src="<?php echo e(URL::asset('front/js/owl.carousel.min.js')); ?>" ></script>
<link rel="icon" href="<?php echo e(URL::asset('front/images/fav-ak.png')); ?>" sizes="32x32" />
<!-- Global site tag (gtag.js) - Google Analytics -->
<?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false): ?>
<script defer src="https://www.googletagmanager.com/gtag/js?id=G-260M9QC9MV"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-260M9QC9MV');
</script>
<?php endif; ?>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "MedicalOrganization",
  "name": "Dr. Sharda Ayurveda",
  "url": "https://www.drshardaayurveda.com/",
  "logo": "https://www.drshardaayurveda.com/front/images/dr-sharda-logo.webp",
  "contactPoint": {
    "@type": "ContactPoint",
    "telephone": "+91-9876035500 ",
    "contactType": "customer service"
  },
  "sameAs": [
    "https://www.facebook.com/DrShardaAyurveda/",
    "https://twitter.com/shardaayurveda",
    "https://www.instagram.com/DrShardaAyurveda/",
    "https://www.youtube.com/c/DrShardaAyurveda/",
    "https://in.linkedin.com/company/dr-sharda-ayurveda",
    "https://in.pinterest.com/DrShardaAyurveda/",
    "https://www.drshardaayurveda.com/"
  ]
}
</script>
<!-- Meta Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '774260749910695');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=774260749910695&ev=PageView&noscript=1"
/></noscript>
<!-- End Meta Pixel Code -->
<!-- Clarity tracking code for https://www.drshardaayurveda.com/ --><script>    (function(c,l,a,r,i,t,y){        c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};        t=l.createElement(r);t.async=1;t.src="https://www.clarity.ms/tag/"+i+"?ref=bwt";        y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);    })(window, document, "clarity", "script", "8rlmnswuee");</script>

 <script src="https://www.google.com/recaptcha/api.js"></script><?php /**PATH D:\xampp\htdocs\drshardaayurveda\resources\views////partials/frontend/main-head.blade.php ENDPATH**/ ?>