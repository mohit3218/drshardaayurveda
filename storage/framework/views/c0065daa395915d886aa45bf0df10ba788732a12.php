<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>

<?php $__env->startSection('content'); ?>
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4">ABOUT US</h3>
                    <p style="font-size: 14px;">Dr.Sharda Ayurveda was established in 2013 by Dr.Mukesh Sharda. Providing best and safe Ayurvedic treatment for chronic diseases worldwide. <br>It is an ISO 9001:2008 certified company.</p>             
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/about-us">About Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="dsa-why-choose">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="<?php echo e(URL::asset('front/images/Ayurveidc-doctor-dr-mukesh-sharda.webp')); ?>" class="why-choose-us img-fluid" alt="About-us">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1">ABOUT DR. SHARDA AYURVEDA</h1>
                <p>
                    <strong style="font-weight: bold;"> Dr. Sharda Ayurveda </strong>is India’s leading Ayurvedic clinic founded in 2013 with a Modern approach to traditional Ayurvedic medicines that help in restoring the mind, body, and soul. We provide Ayurvedic treatments to patients from all over the world. Dr. Sharda Ayurveda has several branches located in Punjab and is one of the Best Ayurvedic Clinics in India offering a new approach to holistic living.  
                    <br><br>
                    <strong style="font-weight: bold;">Dr. Mukesh Sharda CEO and Founder</strong> of Dr. Sharda Ayurveda has a team of several experienced Ayurvedic doctors that aims to enable patients to live a healthy and better life through authentic Ayurveda treatment. Traditional Ayurvedic herbs combined with high modern technology blend the best of old and new Ayurvedic medicines making Dr. Sharda Ayurveda one of the top Ayurvedic clinics in Punjab.
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                    <p>
                        We are specialized in the treatment of all chronic diseases by only effective Ayurvedic treatment. We provide completely individualized treatment according to Doshas with utmost care after proper examination to support your holistic living. Our primary concern is to ensure that your health receives special care, comfort, and attention by Ayurvedic treatment so that you get the best of results for healthy speedy healing.
                    </p>
                     <div class="btn-ayurvedic">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                    </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<div class="wrp pt-5 pb-5">
    <div class="cvn" style="background-color: #eeb03f; color: #fff; padding: 34px;">
        <h2 style="margin-top: 4%; margin-left: 8%;"><span>Our </span><strong>Vision</strong></h2>
        <p style="margin-left: 4%; margin-bottom: 5%;">
            Making each and everyone happy and healthy on this planet by Ayurveda is the vision of Dr. Sharda Ayurveda. <br>To heal the world with Ayurveda is the vision of Dr. Sharda Ayurveda.
        </p>
        <h2 style="margin-top: 4%; margin-left: 8%;"><span>Our </span><strong>Mission</strong></h2>
        <p  style="margin-left: 4%;">
            Our Mission stays constant to make world a healthy place to live in by adopting all the ancient Ayurvedic techniques and practices in modern Ayurvedic treatment. 
        </p>
    </div>
    <div class="kmoz" >
        <div class="left-img crc" data-aos="zoom-in" data-aos-duration="3500">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="PGKM-htmG9A" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:380px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="<?php echo e(URL::asset('front/images/backpain.webp')); ?>" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 100%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('<?php echo e(URL::asset('front/images/video-icon.png')); ?>') no-repeat;">
                            </div>
                        </div>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-india-ayurvedic-sec our-expert" id="types" style="margin-top: 10%; background-color: #FAF9F6;"> 
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Our <b>Experts</b></h2>
            </div>
        </div>
         <div class="row cust-iba">
            <div class="owl-carousel owl-theme" >
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="<?php echo e(URL::asset('front/images/Dr-Raman.webp')); ?>" class="card-img-top lazyload" alt="Dr. Ramanjeet Kaur (BAMS, MD)">
                        <div class="card-body">
                            <h6 class="text-center">Dr. Ramanjeet Kaur (BAMS, MD)</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12 text-center">
                                    <div class="tp-social-icon">
                                        <a href="https://www.facebook.com/DrShardaAyurveda" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="https://twitter.com/shardaayurveda" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="https://www.youtube.com/c/DrShardaAyurveda" target="_blank"><i class="fa fa-youtube"></i></a>
                                        <a href="https://www.linkedin.com/company/dr-sharda-ayurveda" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        <a href="https://www.instagram.com/DrShardaAyurveda/" target="_blank"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="<?php echo e(URL::asset('front/images/DrVinodChauhan.webp')); ?>" class="card-img-top lazyload" alt="Dr Vinod Chauhan (BAMS, M.Sc.)">
                        <div class="card-body">
                            <h6 class="text-center">Dr. Vinod Chauhan (BAMS, M.Sc.)</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12 text-center">
                                    <div class="tp-social-icon">
                                        <a href="https://www.facebook.com/DrShardaAyurveda" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="https://twitter.com/shardaayurveda" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="https://www.youtube.com/c/DrShardaAyurveda" target="_blank"><i class="fa fa-youtube"></i></a>
                                        <a href="https://www.linkedin.com/company/dr-sharda-ayurveda" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        <a href="https://www.instagram.com/DrShardaAyurveda/" target="_blank"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
    
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="<?php echo e(URL::asset('front/images/Dr-Jeevan-Jyoti.webp')); ?>" class="card-img-top lazyload" alt="Dr. Jeevan Jyoti (BAMS, MD)">
                        <div class="card-body">
                            <h6 class="text-center">Dr. Jeevan Jyoti (BAMS, MD)</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12 text-center">
                                    <div class="tp-social-icon">
                                        <a href="https://www.facebook.com/DrShardaAyurveda" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="https://twitter.com/shardaayurveda" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="https://www.youtube.com/c/DrShardaAyurveda" target="_blank"><i class="fa fa-youtube"></i></a>
                                        <a href="https://www.linkedin.com/company/dr-sharda-ayurveda" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        <a href="https://www.instagram.com/DrShardaAyurveda/" target="_blank"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="<?php echo e(URL::asset('front/images/Dr-Vandana-Sharma.webp')); ?>" class="card-img-top lazyload" alt="Dr. Vandana Sharma (BAMS)">
                        <div class="card-body">
                            <h6 class="text-center">Dr. Vandana Sharma (BAMS)</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12 text-center">
                                    <div class="tp-social-icon">
                                        <a href="https://www.facebook.com/DrShardaAyurveda" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="https://twitter.com/shardaayurveda" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="https://www.youtube.com/c/DrShardaAyurveda" target="_blank"><i class="fa fa-youtube"></i></a>
                                        <a href="https://www.linkedin.com/company/dr-sharda-ayurveda" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        <a href="https://www.instagram.com/DrShardaAyurveda/" target="_blank"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="<?php echo e(URL::asset('front/images/Dr-Sunil-Chabra.webp')); ?>" class="card-img-top lazyload" alt="Dr. Sunil Chabbra (BAMS)">
                        <div class="card-body">
                            <h6 class="text-center">Dr. Sunil Chabbra (BAMS)</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12 text-center">
                                    <div class="tp-social-icon">
                                        <a href="https://www.facebook.com/DrShardaAyurveda" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="https://twitter.com/shardaayurveda" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="https://www.youtube.com/c/DrShardaAyurveda" target="_blank"><i class="fa fa-youtube"></i></a>
                                        <a href="https://www.linkedin.com/company/dr-sharda-ayurveda" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        <a href="https://www.instagram.com/DrShardaAyurveda/" target="_blank"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="<?php echo e(URL::asset('front/images/Dr-Sarabhjot-Singh.webp')); ?>" class="card-img-top lazyload" alt="Dr Sarabhjot Singh (BAMS)">
                        <div class="card-body">
                            <h6 class="text-center">Dr. Sarabjot Singh (BAMS)</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12 text-center">
                                    <div class="tp-social-icon">
                                        <a href="https://www.facebook.com/DrShardaAyurveda" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="https://twitter.com/shardaayurveda" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="https://www.youtube.com/c/DrShardaAyurveda" target="_blank"><i class="fa fa-youtube"></i></a>
                                        <a href="https://www.linkedin.com/company/dr-sharda-ayurveda" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        <a href="https://www.instagram.com/DrShardaAyurveda/" target="_blank"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="<?php echo e(URL::asset('front/images/dr-preeti.webp')); ?>" class="card-img-top lazyload" alt="Dr. Preeti Arora (BAMS, M.Sc.)">
                        <div class="card-body">
                            <h6 class="text-center">Dr. Preeti Arora (BAMS, M.Sc.)</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12 text-center">
                                    <div class="tp-social-icon">
                                        <a href="https://www.facebook.com/DrShardaAyurveda" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="https://twitter.com/shardaayurveda" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="https://www.youtube.com/c/DrShardaAyurveda" target="_blank"><i class="fa fa-youtube"></i></a>
                                        <a href="https://www.linkedin.com/company/dr-sharda-ayurveda" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        <a href="https://www.instagram.com/DrShardaAyurveda/" target="_blank"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="<?php echo e(URL::asset('front/images/dr-nikita.webp')); ?>" class="card-img-top lazyload" alt="Dr. Nikita (BAMS)">
                        <div class="card-body">
                            <h6 class="text-center">Dr. Nikita (BAMS)</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12 text-center">
                                    <div class="tp-social-icon">
                                        <a href="https://www.facebook.com/DrShardaAyurveda" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="https://twitter.com/shardaayurveda" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="https://www.youtube.com/c/DrShardaAyurveda" target="_blank"><i class="fa fa-youtube"></i></a>
                                        <a href="https://www.linkedin.com/company/dr-sharda-ayurveda" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        <a href="https://www.instagram.com/DrShardaAyurveda/" target="_blank"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2">
                        <img src="<?php echo e(URL::asset('front/images/dr-dapinder-kaur.webp')); ?>" class="card-img-top lazyload" alt="Dr. Dapinder Kaur (BAMS)">
                        <div class="card-body">
                            <h6 class="text-center">Dr. Dapinder Kaur (BAMS)</h6>
                            <div class="row custom-logo-sec">
                                <div class="col-12 text-center">
                                    <div class="tp-social-icon">
                                        <a href="https://www.facebook.com/DrShardaAyurveda" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="https://twitter.com/shardaayurveda" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="https://www.youtube.com/c/DrShardaAyurveda" target="_blank"><i class="fa fa-youtube"></i></a>
                                        <a href="https://www.linkedin.com/company/dr-sharda-ayurveda" target="_blank"><i class="fa fa-linkedin"></i></a>
                                        <a href="https://www.instagram.com/DrShardaAyurveda/" target="_blank"><i class="fa fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>     
            </div>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec abt-us"> 
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="btn-ayurvedic">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h2>Why <strong>Dr. Sharda Ayurveda </strong>Products <br>Are Best in Quality And Are Safe To Consume?</h2>
            </div>
        </div>
        <div class="row cust-iba">
            <div class="col-xl-3 col-md-3 col-sm-12 aos-init aos-animate" data-aos="fade-up" data-aos-duration="3000">
                <div class="iba-box box1 psor1">
                    <div class="xd">
                        <div class="cont">	
                            <div class="wht-space"><img src="<?php echo e(URL::asset('front/images/quality-tested-icon.png')); ?>" class="card-img-top lazyload" alt="Quality Tested Icon"></div>
                            <h4 class="text-center">QUALITY TESTED</h4>
                            <p class="text-center">
                                All the Ayurvedic Medicines are made with authentic natural herbs with high-end Technology. After a medicine is produced it is tested before consumption.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-3 col-sm-12 aos-init aos-animate" data-aos="fade-up" data-aos-duration="3000">
                <div class="iba-box box2 psor2">
                    <div class="xd">
                        <div class="cont">
                            <div class="wht-space"><img src="<?php echo e(URL::asset('front/images/no-harmfull-effects-icon.png')); ?>" class="card-img-top lazyload" alt="No Harmful Effects Icon"></div>
                            <h4 class="text-center">NO HARMFUL EFFECTS</h4>
                            <p class="text-center">
                                Ayurvedic Medicines have no side- effects on health rather are loaded with several side benefits. Ayurvedic Medicines never harm your body and do not accompany any other diseases with them.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-3 mt-2 col-sm-12 aos-init aos-animate" data-aos="fade-up" data-aos-duration="3000">
                <div class="iba-box box3 psor3">
                    <div class="xd">
                        <div class="cont">
                            <div class="wht-space"><img src="<?php echo e(URL::asset('front/images/clinically-approved-icon.png')); ?>" class="card-img-top lazyload" alt="Clinically Approved icon"></div>
                            <h4 class="text-center">CLINICALLY APPROVED</h4>
                            <p class="text-center">
                                All the Ayurvedic Medicines at Dr. Sharda Ayurveda are clinically approved and are tested timely.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-3 mt-2 col-sm-12 aos-init aos-animate" data-aos="fade-up" data-aos-duration="3000">
                <div class="iba-box box3 psor3">
                    <div class="xd">
                        <div class="cont">
                            <div class="wht-space"><img src="<?php echo e(URL::asset('front/images/door-step-delivery-icon.png')); ?>" class="card-img-top lazyload" alt="Door step delivery)"></div>
                            <h4 class="text-center">DOOR STEP DELIVERY</h4>
                            <p class="text-center">
                                For your convenience, we provide door step delivery of medicines around the globe. All of the medicines are packed with full safety so that they reach you in a proper manner without any destruction.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Patient's <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="<?php echo e(URL::asset('front/images/patient-testimonial-male.webp')); ?>" class="img-fluid" alt="Kulwant Singh Dhillon Testimonial" title="Kulwant Singh Dhillon Review">
                            <h3 class="usrname">Kulwant Singh Dhillon</h3>
                            <p class="desig">Knee Pain Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I got <a class="green-anchor" href="https://www.drshardaayurveda.com/ayurvedic-online-consultation">ayurvedic online consultation</a> from Dr. Sharda Ayurveda for my knee pain treatment. I was suffering with knee pain from very long. I consulted many doctors but got no relief. I thought of getting Ayurvedic medicine and began my treatment from <a class="green-anchor" href="https://www.drshardaayurveda.com/">Dr. Sharda Ayurveda</a>. I religiously followed the diet prescribed by them and followed all the precautions with Ayurvedic medicines. Within just few days I was relieved from pain.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="<?php echo e(URL::asset('front/images/patient-testimonial-female.webp')); ?>" class="img-fluid" alt="Manjinder Kaur Review" title="Manjinder Kaur Review">
                            <h3 class="usrname">Manjinder Kaur</h3>
                            <p class="desig">Rheumatoid Arthritis Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was suffering from Rheumatoid Arthritis for the past 9-10 years. Randomly one day while scrolling my Facebook I got to know about Dr. Sharda Ayurveda clinic. I ate medicines for approximately 8 months from their clinic and I am fully satisfied with their results and my rheumatoid arthritis is negative now. I would not hesitate to tell anyone that one should visit Dr. Sharda Ayurveda for your chronic disease treatment.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>

                    <div class="item">
                        <div class="testimonial-content">
                            <img src="<?php echo e(URL::asset('front/images/patient-testimonial-male.webp')); ?>" class="img-fluid" alt="Dilpreet Singh Review" title="Dilpreet Singh Review">
                            <h3 class="usrname">Dilpreet Singh</h3>
                            <p class="desig">Asthma Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I used to face breathing issues, sometimes it was quite difficult for me to breath at night in winters. After Ayurvedic treatment for asthma, I am highly satisfied with the results and I am recovering from my breathing issues. 
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo $__env->make('../partials/frontend/form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<script type="text/javascript">
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            responsiveClass: !0,
            navigation : true,
            autoplay: true,
            loop: true,
            smartSpeed: 2500,
            singleItem : true,
            navigationText: ["<i class='fa fa-angle-left fa-2'></i>", "<i class='fa fa-angle-right fa-2'></i>"],
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                480: {
                    items: 1,
                    nav: !1
                },
                768: {
                    items: 3,
                    nav: !1
                },
                1000: {
                    items: 4,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1024: {
                    items: 4,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1366: {
                    items: 4,
                    nav: !1
                }
            }
        })
    });

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\drshardaayurveda\resources\views/frontend/about/index.blade.php ENDPATH**/ ?>