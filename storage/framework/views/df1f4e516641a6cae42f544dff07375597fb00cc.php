<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>

<?php $__env->startSection('content'); ?>
<section class="types-of-eczema mt-5" id="typesOfEczema">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-3 col-sm-12 mb-2 eczemaCategories">
                <ul>
                </ul>
            </div>
            <div class="col-xl-8">
                <h1 class="heading1">INVERSE PSORIASIS</h1>
                <p> 
                    Inverse psoriasis is an auto-immune skin condition that majorly affects the skin folds. It causes itchy, shiny, and smooth lesions at the places where the skin rubs together like the armpits, groin, and under the breast.  It is a rare type of psoriasis that commonly affects overweight or people with deep skin folds and obese people. Skin folds are called flexures so Inverse psoriasis is also called flexural psoriasis or intertriginous psoriasis. Inverse psoriasis treatment in Ayurveda helps reduce rashes and treat the disease.
                </p>
                <h2 class="heading1">What is Inverse psoriasis and how is it related to Psoriasis?</h2>
                   <p>Inverse psoriasis affects 20-30% of people with psoriasis. Thick, discolored patches of skin with white or silvery scales are the symptoms of psoriasis. Plaques are big, scaly spots. Inverse psoriasis affects moist parts of your body, inverse psoriasis doesn't have the thick, scaly plaques that other varieties of psoriasis do.<br>
                    The triggers for inverse psoriasis also include friction and moisture and are linked to sweating. <br>
                    Due to its clinical resemblance to other skin conditions affecting the folds, inverse psoriasis is a clinical variation of psoriasis that can be challenging to identify.<br>
                    Inverse psoriasis typically differs from other forms of psoriasis due to the placement and appearance of these lesions. People may discover lesions in skin folds on their bodies, such as the groin, armpits, under the breasts, and the abdomen, among others.<br>
                    Middle-aged people or seniors are more likely to develop flexural psoriasis than younger people.</p>

                <h2 class="heading1">CAUSES OF INVERSE PSORIASIS</h2>

                <p>
                <ul class="li-format">
                    <li><b>Obesity-</b>  Excess weight of a person may result in larger skin folds.</li>
                    <li><b>Stress-</b> Psoriasis is a stress-dependent disorder. Higher stress leads to trigger psoriasis.</li> 
                    <li><b>Vitamin D deficiency-</b> Lower levels of Vitamin D may trigger the immune system and could be a cause of psoriasis.</li> 
                    <li><b>Medications-</b> Several medicines can affect the immune system functioning and thus can be a cause of inverse psoriasis.</li> 
                    <li><b>Sweating-</b> Sweat aggravates the symptoms of psoriasis. So keeping your body dry and free from moisture is very important.</li></ul>
                </p>
                   
                <h2 class="heading1">SYMPTOMS OF INVERSE PSORIASIS </h2>
                <p><ul class="li-format">
                    <li><b>Skin rashes-</b> Shiny, smooth, discolored (brown, pink, purple, or red) rashes can be seen on skin folds.</li></ul>
                </p>

                <h2 class="heading1">Body Parts Affected By Inverse Psoriasis</h2>
                <p>Inverse psoriasis usually affects the skin folds. So the most affected parts are-
                    <ul class="li-format">
                    <li>Armpits</li>
                    <li>Groin</li> 
                    <li>Under your breasts</li>
                    <li>Skin folds around your genitals</li>
                    <li>Between the buttocks</li></ul>
                </p>

                    <h2 class="heading1">How do you stop inverse psoriasis from spreading?</h2>
                    <p><ul class="li-format">
                        <li><b>Eating a nutrient-rich diet-</b> A healthy diet helps maintain weight and overall health and reduces the severity of the symptoms.</li>
                        <li><b>Keep the affected area clean-</b> It is very important to keep your body especially the affected parts clean and dry as moisture aggravates the symptoms of psoriasis. It is advised to pat dry your body with a soft towel.</li>
                        <li><b>Avoid wearing tight-fitting clothes-</b> Proper tight-fit clothes rubs against the skin and worsen the symptoms of inverse psoriasis. It is advised to wear loose-fit clothes so that body can breathe easily and air can circulate around the affected area.</li> 
                        <li><b>Managing Stress-</b> Stress triggers psoriasis flare-ups. So it is very important to manage stress levels. Yoga, meditation, and deep breathing can help relieve stress.</li> 
                        <li><b>Importance of Sleep-</b> Sleep has a great role to play in treating any disorder. It is advised to sleep on time. Sound sleep and sleeping for proper hours help recover from the disease.</li></ul>
                    </p>

                    <h2 class="heading1">What food should psoriasis patients avoid?</h2>
                    <p>Psoriasis patients are seen to benefit by avoiding certain foods mentioned below that can trigger inflammation and worsen the symptoms. Not all psoriasis patients will be affected by the same foods.  Triggers will always vary for every individual. Here is a list of that psoriasis patients should avoid in their diet-
                    <ul class="li-format">    
                        <li><b>Dairy products-</b> Dairy products are heavy in nature, hard to digest, and can also cause inflammation and worsen the symptoms of some people. So it is advised in psoriasis to stop having them.  Also, some patients with psoriasis may be sensitive to dairy products like milk and cheese.</li> 

                        <li><b>Non-vegetarian food-</b> Non-vegetarian food especially red meat and processed meats like bacon, and sausage triggers inflammation and can worsen the symptoms.</li> 

                        <li><b>Stop drinking Alcohol-</b> Alcohol triggers inflammation. It is always advised that a psoriasis patient should avoid drinking alcohol for early and safe recovery.</li> 

                        <li><b>Gluten-</b> Gluten and products made out of it can trigger the symptoms of psoriasis as it is seen that patients with psoriasis have shown improvement in their symptoms after adopting a gluten-free diet and the recovery rate is higher.</li> </ul>
                    </p>

                <h2 class="heading1">What food should psoriasis patients avoid?</h2>
                    <p>Ayurveda is an ancient system of medicine that offers a holistic approach to the treatment of several diseases. Ayurvedic treatment is different from modern medicine as Ayurveda views the body as a whole, with each individual being unique thus a personalized treatment followed by some mandatory diet and lifestyle change is a must. Therefore there is no better treatment than Ayurveda for inverse psoriasis. Ayurvedic treatment for inverse psoriasis has proven to show the best results.</p>
                    
                    <p><b>Ayurveda can be helpful in managing inverse psoriasis by-</b></p>

                    <h3 class="newh3">DIETARY CHANGES</h3> 
                    <p>Diet plays a crucial role in treating inverse psoriasis. Patients suffering from inverse psoriasis are advised to follow a diet rich in fruits, vegetables, and whole grains. It is advised to avoid processed food, dairy products, and non-vegetarian food.</p>

                    <h3 class="newh3">LIFESTYLE MODIFICATION</h3>
                    <p>Ayurveda emphasizes the importance of lifestyle. It is essential to alter your lifestyle for early healing. Your routine should include exercising regularly, adequate rest, proper sleep, and meditation.</p>

                    <h3 class="newh3">HERBAL REMEDIES</h3> 
                    <p>Herbal remedies have a great role to play in treating inverse psoriasis.
                    The use of Ayurvedic herbal remedies helps reduce inflammation and relieve symptoms. 
                    Remedies vary for every individual according to their body type and how acute or chronic the disease is. So it is advised to consult an Ayurvedic doctor before beginning remedies.</p>

                    <h3 class="newh3">PANCHAKARMA</h3>
                    <p>Panchakarma involves detoxification and cleansing treatments to purify the blood. Through this process, toxins are removed from the body to improve health.</p>

                <h2 class="heading1">FAQ’s</h2>
                <ol><ul class="li-format">

                    <p><li><b>Does inverse psoriasis go away?</b></li>
                    Inverse psoriasis can be managed by Ayurvedic treatment. 
                    Flare-ups and rashes may come if you do not follow the diet and other changes recommended by your expert and goes away if you follow a course of Ayurvedic treatment for inverse psoriasis with diet and lifestyle changes. It is essential to maintain personal hygiene.</p>

                    <p><li><b>How rare is inverse psoriasis?</b></li>
                    It is one of the rare types of psoriasis as it hardly affects 3%-7% of patients suffering from psoriasis.</p>

                    <p><li><b> What foods permanently cure psoriasis?</b></li>
                    The diet prescribed by your Ayurvedic expert will help manage symptoms of psoriasis with the best results. Eating more fresh fruits and vegetables helps lessen the severity of the disease. Ayurveda believes that every individual is different so the diet varies according to their body type.</p>
                </ul></ol>
            </div>
        </div>
    </div>
</section>

<?php echo $__env->make('../partials/frontend/blogs_post_section', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php echo $__env->make('../partials/frontend/form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\drshardaayurveda\resources\views/frontend/services/inverse_psoriasis_treatment.blade.php ENDPATH**/ ?>