<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>

<?php $__env->startSection('content'); ?>
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h1 class="ser-heading mt-4" >Ayurvedic Treatment for Abdominal Pain</h1>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/abdominal-pain">Abdominal Pain</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatments">TREATMENTS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="definition">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="<?php echo e(URL::asset('front/images/abdominal-pain.webp')); ?>" class="why-choose-us img-fluid" alt="Woman suffering from abdominal pain" title="Woman suffering from abdominal pain">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h2 class="heading1">What is <b>Abdominal Pain?</b></h2>
                <p>
                Abdominal pain...ugh, that pesky discomfort or pain experienced between your chest and pelvis. It can be a real buzzkill, ranging from mild annoyance to full-blown agony. But hey, in the world of Ayurveda, we've got some insights for you. We see abdominal pain as a sneaky manifestation of an imbalance in the doshas. You know, those energetic forces called Vata, Pitta, and Kapha.
                <br>
                So, how do we tackle this tummy trouble? Well, Ayurvedic treatment for abdominal pain takes a holistic approach. We're talking about digging deep to find the root cause and bringing those doshas back into balance. It's all about making some dietary and lifestyle modifications, embracing herbal remedies, and even indulging in therapeutic procedures.
                <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                <p>
                    As part of your treatment plan, we might suggest switching to easily digestible foods and ditching those spicy, fried, and processed goodies that can wreak havoc on your digestive system. And oh, let's not forget the magic of herbal formulations infused with ginger, fennel, and peppermint. They can work wonders in soothing your digestive tract and dialling down the pain.
                </p>
                <div class="btn-ayurvedic">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec our-expert" style="background-color:#f3f3f3" id="causes">
    <div class="container-fluid" >
        <div class="row cut-row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">What Causes <b>of Abdominal Pain?</b></h2>
                    <p font align="center" style="line-height:2">
                        Alright, let's dive into the nitty-gritty of what causes that abdominal pain of yours. Brace yourself—there's a whole buffet of factors that can trigger it. Here are some common culprits:
                    </p>
                    <li>
                        <b>Gastrointestinal Issues:</b>  We're talking about gastritis, acid reflux, irritable bowel syndrome (IBS), and constipation. Yep, these troublemakers can give you a hard time in the tummy department.
                    </li>
                    <li>
                        <b>Infections:</b> Oh, those nasty bacterial or viral infections targeting your precious digestive system. Think gastroenteritis or urinary tract infections. Yep, they can leave you doubled over in pain. 
                    </li>
                    <li>
                        <b>Kidney Stones:</b>Picture this—tiny, hard mineral deposits forming in your kidneys. Ouch! As these little devils travel through your urinary tract, they can cause some sharp, intense pain in your abdomen. Not the kind of souvenir you'd want to bring home.
                    </li>
                    <li>
                        <b>Appendicitis:</b> Say hello to your appendix, or rather, its evil twin called appendicitis. When this little troublemaker decides to get all inflamed, you'll experience some serious lower right abdominal pain. And trust me, it's not something you want to mess around with. Seek medical attention, pronto! 
                    </li>
                    <li>
                        <b>Menstrual Cramps:</b> Ladies, this one's for you. We all know that time of the month can be a rollercoaster of emotions and, yep, abdominal pain too. Uterine contractions during your menstrual cycle can give you some serious belly blues.
                    </li>
                </div>
            </div>
        </div>
    </div>
</section>  
<div class="container-strip" style="background: #eeb03f";>
    <div class="container" >
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz px-2.5" >
            <img src="<?php echo e(URL::asset('front/images/Abdominal-pain-symptoms.webp')); ?>" class="img-fluid"alt="symptoms of abdominal pain" title="symptoms of abdominal pain">
        </div>
        <div class="cvn sym" >
              <h2 class="heading1">Symptoms <b>of Abdominal Pain</b></h2>
                    <p style="line-height:2">
                        Ah, the wily symptoms of abdominal pain. They come in different shapes and sizes, varying in location and intensity. These little rascals can give us some hints about what's causing your tummy turmoil. Here are the main ones to look out for:
                    </p>
                    <li>
                        <b>Sharp or Cramping Pain:</b> Oh, the range of sensations is quite something. You might experience a dull ache, or bam! A sharp, stabbing sensation that comes and goes like a mischievous ninja.
                    </li>
                    <li>
                        <b>Location, Location, Location:</b> Abdominal pain loves to play hide-and-seek. Lower stomach pain might be pointing fingers at your intestines, bladder, or reproductive organs. And the upper stomach pain? Well, that could be related to your stomach, liver, or gallbladder. It's like a guessing game, but less fun. 
                    </li>
                    <li>
                        <b>Nausea and Vomiting:</b> Ah, the delightful duo. When abdominal pain strikes, nausea often comes along for the ride. And sometimes, it brings its partner in crime—vomiting. Fun times, right? 
                    </li>
                    <li>
                        <b>Bloating and Gas:</b> Picture this—your belly inflates like a balloon, and you feel like you're auditioning for a role in the Macy's Thanksgiving Day Parade. Abdominal pain loves to team up with bloating and excessive gas, leaving you feeling uncomfortably full.
                    </li>
                    <p>
                        Now, remember, these symptoms are like puzzle pieces. They can hint at different conditions, so getting a proper diagnosis is crucial. Your unique needs and the right abdominal pain treatment go hand in hand.
                    </p>
        </div>
    </div>
</div>

<div class="split"></div>

<section class="dsa-why-choose" id="treatments">
    <div class="container-fluid">
        <div class="row cut-row" >
              <div class="cvn sym" >
            <h2><span>Abdominal pain</span> Ayurvedic treatment</h2>
            <p>Alrighty, let's talk about the magic of Ayurvedic treatment for that bellyache of yours. We've got some ancient wisdom up our sleeves to manage and alleviate abdominal pain. Brace yourself for some goodness:</p>
            <li>
                <b>Herbal Remedies:</b>Nature's little helpers are here to save the day. We're talking about Ayurvedic herbs like ginger, fennel, coriander, and peppermint. These bad boys have some serious digestive superpowers. Sip 'em as teas, sprinkle 'em as powders, or embrace Ayurvedic formulations. They can be your new best friends.
            </li>
            <li>
                <b>Dietary Modifications:</b> Ah, the key to a happy tummy lies in what you put on your plate. Ayurveda emphasizes balance and appropriateness. So, we're talking about choosing easily digestible foods, bidding adieu to those spicy and heavy meals, and welcoming fiber-rich foods to regulate your bowels. It's all about finding your food groove. 
            </li>
            <li>
                <b>Lifestyle Practices:</b> Time to embrace a healthier way of living. We're talking about mindful eating, regular exercise (yes, get that body moving), stress management techniques (breathe in, breathe out), and giving your body the rest it deserves. It's like a self-care party for your digestion. 
            </li>
            <li>
                <b>Panchakarma Treatment:</b> Now, this is where things get interesting. Ayurvedic treatments are like the superheroes of abdominal pain management. Fancy an oil massage (Abhyanga)? How about therapeutic purgation (Virechana) or a meditated enema (Basti)? These ancient techniques can detoxify your system and bring that digestive balance back.
            </li>
            <br>
            <p>
                But hey, here's the deal—it's important to consult with Dr. Sharada Ayurveda's doctors. They're the experts who can assess your condition and craft a personalised abdominal pain treatment plan just for you.
            </p>
        </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="G2SXEXmtMqY" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="<?php echo e(URL::asset('front/images/G2SXEXmtMqY.webp')); ?>" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('<?php echo e(URL::asset('front/images/video-icon.png')); ?>') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Patient's <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="<?php echo e(URL::asset('front/images/patient-testimonial-male.webp')); ?>" class="img-fluid" alt="karnail singh patient's testimonial of abdominal pain" title="karnail singh patient's testimonial of abdominal pain" >
                            <h3 class="usrname">Karnail Singh</h3>
                            <p class="desig">Abdominal Pain Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>

                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                My stomach used to hurt every couple of days or so. I had my tummy scanned, and everything came back clear. Dr. Sharda Ayurveda was my source for it. My research helped me come to the conclusion that my stomach pain was caused by gastritis problems. They started with Ayurvedic remedies for gastrointestinal discomfort. I experienced stomach pain relief after a few days, and I gradually began to heal from acidity as well.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="<?php echo e(URL::asset('front/images/patient-testimonial-male.webp')); ?>" class="img-fluid" alt="anmol singh patient's testimonial of abdominal pain" title="anmol singh patient's testimonial of abdominal pain">
                            <h3 class="usrname">Anmol Singh</h3>
                            <p class="desig">Abdominal Pain Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                There aren't enough kind words to say about Dr. Sharda Ayurveda medical staff. They greatly aided me, and I'm now able to return to my regular daily activities. I've had bloating and terrible stomach pain for previous five months. I was unable to find a doctor who could treat me, but since I started practicing Ayurveda, I feel much better. In addition to the medications, their eating plan was a huge assistance to me.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <i class="fa" aria-hidden="true"></i>
                            When is abdominal pain serious?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                        Talk to your doctor if you have Abdominal pain that lasts 1 week or more. Abdominal pain that fails to improve within 24 to 48 hours, or becomes more severe and frequent and occurs with nausea and vomiting.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                What does the pain in the abdomen indicate?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                        Causes of abdominal pain are several but the main causes are bacterial infection, abnormal tissue growth, inflammation, and blockage in the intestines. Throat infection and blood infection can cause bacteria to enter the digestive tract thus, leading to pain in the stomach.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                What foods to be avoided in abdominal pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                        Soft drinks, spicy foods and garlic are 3 foods that you should not eat if you suffer from abdominal pain. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                How to relieve abdominal pain ?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                        Place a warm bottle or bag over the abdomen or drink plenty of water, eat fresh fruit, and drink ajwain juices and water.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Why do I only get lower abdominal pain?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                        It may be caused due to multiple reasons i.e. infections, abnormal growth, inflammation, and obstruction. 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                   How do you treat abdominal infection?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                            Abdominal infections are treated through resuscitation, abdominal drainage, control of the source of infection and antimicrobials.
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                How long does abdominal pain last? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                        Normally abdominal pain subsides itself within 2-3 hours but if the reason is serious and related to another problem then it can be long-lasting. Ayurvedic medicine for stomach pain is safe and has sureshot results to get relief from pain.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                How is acute abdominal pain treated?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                        According to Ayurveda, diet is very helpful to heal the disease so for abdominal disorders Ayurveda advises eating less food, and fasting once a week. Vamana and Virechana are suggested to be free from abdominal disorders.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                Is abdominal pain life-threatening?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                        Yes, if the pain is severe then it is an indication of another abdominal problem in which the organ is inflamed or infected. Abdominal pain Ayurvedic treatment provides you relief from pain and discomfort. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                What causes abdominal pain daily?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                        Indigestion, gas, and some heavy load work that pulled muscles can cause abdominal pain in daily routine life.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

<?php echo $__env->make('../partials/frontend/blogs_post_section', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php echo $__env->make('../partials/frontend/form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Abdominal Pain",
    "item": "https://www.drshardaayurveda.com/abdominal-pain"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "When should I worry about abdominal pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If the pain in the stomach goes very sharp, severe, sudden and is accompanied with pain in the chest, neck followed by vomiting, bloody diarrhea, or black stools."
    }
  },{
    "@type": "Question",
    "name": "What does the pain in the abdomen indicate?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Causes of abdominal pain are several but the main causes are bacterial Infection, abnormal growths, inflammation, and blockage in the intestines. Throat infection, blood infection can cause bacteria to enter the digestive tract thus leading to pain in the stomach."
    }
  },{
    "@type": "Question",
    "name": "What are the 3 foods to be avoided in abdominal pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Carbonated beverages, spicy food, Garlic are 3 foods that you should not eat in abdominal pain."
    }
  },{
    "@type": "Question",
    "name": "How can I lower abdominal pain naturally?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Place a hot bottle or heated bag on the abdomen or drink plenty of water like Ajwain water, drink juices and eat fruits."
    }
  },{
    "@type": "Question",
    "name": "Why do I only get lower abdominal pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It may be caused due to multiple reasons i.e. infections, abnormal growth, inflammations, and obstruction."
    }
  },{
    "@type": "Question",
    "name": "What type of investigation is necessary for abdominal pain?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "USG (ultrasonography) is the best test for abdominal pain. It will clear the exact cause and area of pain."
    }
  },{
    "@type": "Question",
    "name": "How long does abdominal pain last?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Normally abdominal pain subsides itself within 2-3 hours but if the reason is seriously related to another problem then it can be long-lasting. Ayurvedic medicine for stomach pain is safe and has sure results to get relief from pain."
    }
  },{
    "@type": "Question",
    "name": "How is acute abdominal pain treated?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "According to Ayurveda diet is very helpful to cure disease so in abdominal disorders Ayurveda advises eating less food, fasting is advised once a week. Vamana and Virechana are advised to cure abdominal disorders."
    }
  },{
    "@type": "Question",
    "name": "Is abdominal pain life-threatening?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes if the pain is severe then it is an indication of another abdominal problem in which any organ is inflamed or infected. Abdominal pain Ayurvedic treatment does not provide you temporary relief from pain."
    }
  },{
    "@type": "Question",
    "name": "What causes abdominal pain daily?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Indigestion, gas, and some heavy work which pulled muscles can cause abdominal pain in daily routine life."
    }
  }]
}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\drshardaayurveda\resources\views/frontend/services/abdominal_pain.blade.php ENDPATH**/ ?>