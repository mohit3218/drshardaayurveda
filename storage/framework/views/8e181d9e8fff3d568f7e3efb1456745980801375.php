<?php
/**
 * Piles Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>

<?php $__env->startSection('content'); ?>
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h1 class="ser-heading mt-4" >Ayurvedic Treatment for Piles</h1>
                    <!--<p style="font-size: 24px;">Easy Bowel Movements with Ayurveda <br /> Not just symptomatic, Get Root cause treatment for Gout</p>-->
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/piles">Piles</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatments">TREATMENTS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="definition">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="<?php echo e(URL::asset('front/images/piles-treatment.webp')); ?>" class="why-choose-us img-fluid" alt="Piles treatment">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h2 class="heading1">What are <b>Piles?</b></h2>
                <p>
                    Piles- an extremely discomforting and painful digestive disorder. It is also known as haemorrhoids primarily caused by distention and congestion of the internal and external venous plexuses around the anal canal.
                    <br>
                    In Ayurveda, it is recognized as “Arsha Roga” caused by an imbalance of Tridosha. In such conditions, the veins of the lower anus and rectum get swollen and there are lumps or inflamed tissues around the anal canal. It is interlinked with constipation and straining at defecation (while passing stool). 
                    <br>
                    Bright red bleeding after defecation or prolapse of something through the rectum which disappears after defecation are certain common complications that patients with piles experience. Aging is a major risk factor for the development of piles because of weakened tissues supporting the rectum and anus. 
                </p>
                <p>
                    Worry not! <br>
                    With Ayurveda, it can be cured
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                  <span id="more-content">
                    <p>
                        Unlike other treatments, Ayurvedic treatment for piles does not work solely on symptom management. In fact, it digs up to the root causes of the disease and treats them naturally. It is absolutely safe and effective measure that provides strength to the digestive system and enhances the overall harmony of the human body.
                    </p>
                    <div class="btn-ayurvedic">
                        <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                    </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec" id="types">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h2 class="heading1">Different Stages of <b>Piles (Hemorrhoids)</b></h2>
                <p font align="center">
                    Piles can be of two types- Internal which originates in the rectum, and External which develops in the anus. Whereas if categorized as per the complications, it can be categorized into 4 stages. Ayurvedic treatment for piles is a natural way of ensuring relief that provides individualized & natural treatment depending upon the stage and type, the patient is suffering from. 
                </p>
            </div>
        </div>
        <div class="row cust-iba">
            <div class="owl-carousel owl-theme" >
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="<?php echo e(URL::asset('front/images/First-stage-of-piles.webp')); ?>"  alt="First Stage of Piles" title="First Stage of Piles">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">1st degree Piles</h4>
                            <p class="card-text">
                                Hemorrhoids that bleed but do not prolapse. These are small and do not protrude outside the anus. 
                            </p>
                        </div>
                    </div>
                </a>
                </div>
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="<?php echo e(URL::asset('front/images/Second-stage-of-piles.webp')); ?>"  alt="Second Stage of Piles" title="Second Stage of Piles" >
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">2nd degree Piles</h4>
                            <p class="card-text">
                                Hemorrhoids that prolapse and retract on their own and come out of the anus and rectum back itself. 
                            </p>
                        </div>
                    </div>
                </a>
                </div>
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="<?php echo e(URL::asset('front/images/Third-stage-of-piles.webp')); ?>"  alt="Third Stage of Piles" title="Third Stage of Piles">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">3rd degree Piles</h4>
                            <p class="card-text">
                            Hemorrhoids that prolapse and is pushed back into the anus by finger. 
                        </p>
                        </div>
                    </div>
                </a>
                </div>
                <div class="item col-md-12 text-center aos-init aos-animate" data-aos="fade-up" data-aos-duration="4000">
                    <div class="card mb-2"> 
                        <img class="card-img-top lazyload" src="<?php echo e(URL::asset('front/images/Fourth-stage-of-piles.webp')); ?>"  alt="Fourth Stage of Piles" title="Fourth Stage of Piles">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold pdt10">4th degree Piles</h4>
                            <p class="card-text">
                                Hemorrhoids that prolapse and cannot be pushed back in the anus. 
                            </p>
                        </div>
                    </div>
                </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec our-expert" style="background-color:#f3f3f3" id="causes">
    <div class="container-fluid" >
        <div class="row cut-row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Piles (Hemorrhoids)</b></h2>
                    <p>
                        There are several factors that could be responsible for the occurrence of piles. A brief of these factors is as follows:
                    </p>
                    <li>
                        <b>Untreated Constipation: </b>
                        Irregular bowel movements or chronic constipation is the root cause of piles.
                    </li>
                    <li>
                        <b>Low-fiber diet: </b>
                        Hemorrhoids can arise if your diet is not satisfactory or lacks adequate amounts of fiber and other nutritional elements.
                    </li>
                    <li>
                        <b>Pregnancy: </b>
                        There is pressure on the enlarged uterus on the rectum and anus that cease strain while bowel movements. 
                    </li>
                    <li>
                        <b>Sitting or standing for extended periods: </b>
                        Such a sedentary lifestyle reduces the efficient functioning of the digestive system. As a result, diseases like piles can easily develop. 
                    </li>
                    <li>
                        <b>Straining while Passing Stool: </b>
                        Straining while excreting can also contribute to several digestive issues including piles.
                    </li>
                </div>
            </div>
        </div>
    </div>
</section>  

<div class="container-strip" style="background: #eeb03f";>
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="<?php echo e(URL::asset('front/images/piles-symptoms.webp')); ?>" class="img-fluid"alt="Piles symptoms"title="Piles symptoms">
        </div>
        <div class="cvn sym" >
            <h2 class="heading1">What are the Symptoms of <b>Piles (Hemorrhoids)?</b></h2>
            <p>
                Below are some common symptoms that patients with piles generally experience:
            </p>
                <li>
                    <b>Severe pain: </b>
                    A hard, sometimes painful lump is felt around the anus region. Sometimes an extreme burning sensation is also experienced. 
                </li>
                <li>
                    <b>Itching: </b>
                    The areas around the anus after defecation becomes red, itchy, and sore. 
                </li>
                <li>
                    <b>Incomplete Evacuation: </b>
                    The patients do not feel fresh as they still feel that their bowel is full or heavy even after excreting.  
                </li>
                <li>
                    <b>Discharge of blood: </b>
                    In chronic and serious cases of piles, blood can also be seen after a bowel movement.  
                </li>
                <li>
                    <b>Painful excreting: </b>
                    The patient experience unbearable pain while passing the stool.  
                </li>
                <p>
                    Experiencing any of these signs?
                    Do not delay and consult with our Ayurvedic experts who will provide you with the most effective Ayurvedic treatment for piles.
                </p>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="treatment">
    <div class="container-fluid">
        <h2 class="client-test-title"style="font-size: 24px!important;" text-al><b>Piles (Haemorrhoids)</b> Ayurvedic Treatment</h2>
            <p style="text-align:center;">
                For centuries, Ayurveda has been benefiting our ancestors as well as the coming generations with its healing powers. Ayurveda follows the philosophy of yielding root-based and permanent relief, rather than suppressing the symptoms. 
                Piles Ayurvedic Treatment involves the following ideologies and practices;
            </p>
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000" style="line-height:1.9rem">
                    <li>
                        Ayurveda grabs its healing power from mother earth in the form of herbs. Several home remedies are available that can be used as internal piles treatment. Being nature-derived and herbal, they yield only desired and effective results.
                    </li>
                    <li>
                        Panchkarma is another healing branch of Ayurveda. It involves several rejuvenating therapies that aid in combatting vitiated body-governing doshas inside the body. These therapies used for Piles Ayurvedic treatment are;
                        <p>
                            <li>Vamana</li>
                            <li>Nasya</li>
                            <li>Virechana</li>
                            <li>Niruha</li>
                            <li>Anuvasana vasti</li>
                            These karma’s (therapies) eliminate the accumulated toxins throughout the body and restores digestive health.
                        </p>
                    </li>
                    <li>
                        External Piles treatment also includes the administration of herbal oils through the anus.
                    </li>
                    <li>
                        Ayurveda suggests taking a high-fiber diet for promoting bowel movements and overall digestive health
                    </li>
                    <p>
                        It is advised to consult a specialist or Ayurveda expert before opting for these home remedies for piles, who will provide you with piles Ayurvedic treatment and precautions as per your body's prakriti and immune capacity.
                    </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="G2SXEXmtMqY" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="<?php echo e(URL::asset('front/images/G2SXEXmtMqY.webp')); ?>" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('<?php echo e(URL::asset('front/images/video-icon.png')); ?>') no-repeat;">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Patient's <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="<?php echo e(URL::asset('front/images/patient-testimonial-male.webp')); ?>" class="img-fluid" alt="Piles Testimonial" >
                            <h3 class="usrname">Vijay Kumar</h3>
                            <p class="desig">Haemorrhoids Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I suffered 13 months in pain from piles. I was using a variety of treatments to get rid of it, but they were never more than a band-aid. I spoke with Dr. Sharda. For the treatment of piles, they have pretty reliable and secure remedies. I received relief from their Ayurvedic medications after just two weeks of treatment, but I had to keep taking them for another three months to fully recover.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="<?php echo e(URL::asset('front/images/patient-testimonial-male.webp')); ?>" class="img-fluid" alt="Haemorrhoids Testimonial" >
                            <h3 class="usrname">Sagar</h3>
                            <p class="desig">Haemorrhoids Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                 I, Sagar, a resident of the model town of Ludhiana, contacted Dr. Sharda Ayurveda for Ayurvedic haemorrhoid treatment. Although I was instructed to seek surgery, I was avoiding it and looking for alternatives. I sought advice here, and I was treated without having any surgery. So anybody seeking non-surgical therapy for piles can speak with Dr. Sharda Ayurveda.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                            Can prolong sitting and standing cause piles?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                        Yes, prolonged sitting on a hard mattress or chair or standing continuously for long hours can cause piles.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Is surgery the only solution for piles?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                        Although piles can usually be treated using lifestyle changes, creams, ointments or non-surgical techniques, around one in ten individuals will require surgery.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                What food can reduce piles?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                            Eating foods that are high in fiber can make stools softer and easier to pass and can help treat and prevent hemorrhoids. Drinking water and other liquids, such as fruit juices and clear soups, can help the fiber in your diet work better.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Is it safe to take Triphala daily to treat piles?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                        Yes, it is a good supplement that helps to manage blood glucose levels, intestinal infection, and constipation, it can be taken daily. It will not harm the body.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Do piles burst?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion3">
                        <div class="card-body">
                        Yes, it can burst when it gets filled with blood inside it and further it bursts out and blood comes out during passing stool.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    What happens if piles remain untreated?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion4">
                            <div class="card-body">
                            Untreated piles may cause bleeding. This bleeding may lead to anemia if these hemorrhoids remain for a long inactive stage and can also cause several health complications. 
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                What is the main cause of piles?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                        <div class="card-body">
                        Hemorrhoids can develop from increased pressure in the lower rectum due to: Straining during bowel movements. Sitting for long periods of time on the toilet. Having chronic diarrhea or <a class="green-anchor"href="https://www.drshardaayurveda.com/constipation">constipation</a>.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Is coffee, tea bad for piles?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                        Yes, tea and coffee are not good in piles as it makes stool dry, hence more pain and straining during defecation.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                At what age can piles occur?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                        Piles can occur at any age but mostly in age between 45-65. This problem is most common in women who are pregnant.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                How long do piles last?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                        Piles improve within 1 week to 12 days without surgery by Ayurvedic treatment. Whether it is internal hemorrhoid treatment it can be treated without surgery too. It may take 2-3weeks for a lump to completely go off.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php echo $__env->make('../partials/frontend/blogs_post_section', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php echo $__env->make('../partials/frontend/form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<style>
    .carousel-inner {position: relative;width: 100%;overflow: hidden;}
    .carousel-item {position: relative;float: left;width: 100%;margin-right: -100%;-webkit-backface-visibility: hidden;backface-visibility: hidden;transition: -webkit-transform .6s ease-in-out;transition: transform .6s ease-in-out;transition: transform .6s ease-in-out,-webkit-transform .6s ease-in-out;}
    .card-body {padding-top: 1.5rem;padding-bottom: 1.5rem;-webkit-border-radius: 0!important;border-radius: 0!important;flex: 1 1 auto;-ms-flex: 1 1 auto;}
    .card .card-body a h3{font-size: 18px;color: #ff9000;line-height: 1.4;}
    .card .card-body h3{color: #373433;font-size: 18px;    line-height: 1.4;}
    .font-weight-bold {font-weight: 700!important;}
    .card-title {margin-bottom: .75rem;}
    .card-img-top {width: 100%;border-top-left-radius: calc(.25rem - 1px);border-top-right-radius: calc(.25rem - 1px);}
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            responsiveClass: !0,
            navigation : true,
            autoplay: true,
            loop: true,
            smartSpeed: 2500,
            singleItem : true,
            navigationText: ["<i class='fa fa-angle-left fa-2'></i>", "<i class='fa fa-angle-right fa-2'></i>"],
            items: 2,
            responsive: {
                0: {
                    items: 1,
                    nav: !1
                },
                480: {
                    items: 1,
                    nav: !1
                },
                768: {
                    items: 2,
                    nav: !1
                },
                1000: {
                    items: 2,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1024: {
                    items: 3,
                    nav: !1,
                    loop: !1,
                    margin: 20
                },
                1366: {
                    items: 3,
                    nav: !1
                }
            }
        })
    });

</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Piles",
    "item": "https://www.drshardaayurveda.com/piles"  
  }]
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "Can prolonged sitting and standing cause piles?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, prolonged sitting on a hard mattress or chair or standing continuously for long hours can cause piles."
    }
  },{
    "@type": "Question",
    "name": "I am suffering from chronic piles, would it be better for me to have surgery?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "“NO,” Ayurveda has a very effective treatment for chronic piles. Before going for surgery you should consult an Ayurvedic physician or visit Dr. Sharda Ayurveda for piles treatment without any surgery."
    }
  },{
    "@type": "Question",
    "name": "Which food should be avoided to get relief from piles?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Deep-fried, processed food, high sugar, spicy food, and alcohol should be avoided in piles."
    }
  },{
    "@type": "Question",
    "name": "Is it safe to take Triphala daily to treat piles?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it is a good supplement that helps to manage blood glucose levels, intestinal infection, and constipation, it can be taken daily. It will not harm the body."
    }
  },{
    "@type": "Question",
    "name": "Do piles burst?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it can burst when it gets filled with blood inside it and further it bursts out and blood comes out during passing stool."
    }
  },{
    "@type": "Question",
    "name": "What happens if piles remain untreated?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Untreated piles may cause bleeding. This bleeding may lead to anemia if these hemorrhoids remain for a long inactive stage and can also cause several health complications."
    }
  },{
    "@type": "Question",
    "name": "How do piles look like?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Piles are described as uncomfortable, enlarged, painful mass around or protruded outward in anal is seen when checked. It is usually a red or discolored lump, painful in nature."
    }
  },{
    "@type": "Question",
    "name": "Is coffee, tea bad for piles?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, tea and coffee are not good in piles as it makes stool dry, hence more pain and straining during defecation."
    }
  },{
    "@type": "Question",
    "name": "At what age can piles occur?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Piles can occur at any age but mostly in age between 45-65. This problem is most common in women who are pregnant."
    }
  },{
    "@type": "Question",
    "name": "How long do piles last?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Piles improve within 1 week to 12 days without surgery by Ayurvedic treatment. Whether it is internal hemorrhoid treatment it can be treated without surgery too. It may take 2-3weeks for a lump to completely go off."
    }
  }]
}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\drshardaayurveda\resources\views/frontend/services/piles.blade.php ENDPATH**/ ?>