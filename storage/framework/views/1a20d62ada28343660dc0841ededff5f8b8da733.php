<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>

<?php $__env->startSection('content'); ?>
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4">Rheumatoid Arthritis (RA) Ayurvedic Treatment</h3>
                    <p style="font-size: 15px;">Prolonged pain can be treated well with Ayurveda</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/rheumatoid-arthritis">Rheumatoid Arthritis</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatments">TREATMENTS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="definition">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <br>
                    <img src="<?php echo e(URL::asset('front/images/Ra.webp')); ?>" class="why-choose-us img-fluid" alt="Rheumatoid Arthritis"title="Rheumatoid Arthritis">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1"> Rheumatoid Arthritis</h1>
                <p>As per Ayurveda. Rheumatoid arthritis is termed <strong style="font-weight: bold;">‘Amavata’</strong>. It is a serious disease in which there is inflammation in the lining of joints. It is because of the imbalance in <strong style="font-weight: bold;">Vata Dosha</strong>. It is an autoimmune disorder that becomes chronic and causes pain and stiffness in the small joints. By autoimmune disorder, it means when the immunity cells instead of fighting with the foreign substances, start attacking the healthy cells. This causes the dysfunction of the defense mechanism. There is a range of signs which depict this condition. Usually, a person experiences morning stiffness in joints which lasts for a few hours that results in immobility. With the passage of time, Rheumatoid arthritis causes the destruction of cartilage, bones, or ligaments which not only leads to deformity but additionally, nodules are formed too.
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">...</span>
                </p>
                <span id="more-content">
                    <p>According to the Ayurveda, the two primary reasons which revolve around this disorder are an unhealthy diet and a sedentary lifestyle. The Ayurvedic treatment of rheumatoid arthritis is the most refined approach which works to remove the root cause of the disease. Moreover, it also gives lifelong results which fight the reversibility of the disease. Ayurvedic treatment for rheumatoid arthritis from Dr. Sharda Ayurveda is based on a detailed diagnosis and accordingly, the experts recommend the guidelines.
                    </p></span>
                    <div class="btn-ayurvedic">
                        <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                    </div>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" style="background-color:#f3f3f3" id="causes">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Rheumatoid Arthritis</b></h2>
                    <p>The causes of <strong style="font-weight: bold;">Rheumatoid Arthritis(Amavata)</strong> are still unknown but some causes are mostly seen</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid cses">
        <div class="row">
            <div class="col-md-6 mb-4">
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Genetics
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                This joint condition is associated to be inherited through generations.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    Compromised Immune System
                                </button>
                            </h5>
                        </div>

                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion1">
                            <div class="card-body">
                                The main triggering factor of the disease is the inability of the immune system to attack or generate an effective response against foreign invaders.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Unhealthy Diet
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                Neglecting your diet for a prolonged time is an alarming sign of serious conditions. 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-4">
                <div id="accordion1">
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="ture" aria-controls="collapseThree">
                                    Sedentary Lifestyle
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion1">
                            <div class="card-body">
                                Lack of physical activity and remaining always lethargic are also the primary reasons.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Excessive Smoking
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                                The presence of silica minerals increases the chances to get affected by rheumatoid arthritis.
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Obesity
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                                Excessive weight, somewhat higher risk of developing arthritis.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	


<div class="container-fluid" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="<?php echo e(URL::asset('front/images/RA-Symstoms.webp')); ?>" class="img-fluid"alt="Rheumatoid Arthritis symptoms"title="Rheumatoid Arthritis symptoms">
        </div>
        <div class="cvn sym" >
            <h3>Symptoms of <span>Rheumatoid Arthritis</span></h3>
            <li><b>Pain in Joint:</b> The primary or the characterizing symptom of arthritis is constant pain in the joints.</li>
            <li><b>Inflammation in Joint:</b> Along with the pain, people may observe severe swelling in one or more joints.</li>
            <li><b>Redness and warmness:</b> Joints mostly are seen as warmer and due to inflammation, they change their color to red.</li>
            <li><b>Stiffness:</b> The stiffness of the joints can lead to restricted movement and hampers overall functioning.</li>
            <li><b>Fever:</b> Usually low-grade fever with body ache happens due to rheumatoid arthritis.</li>
            <li><b>Anemia:</b> Anemia can be one of the reasons, as it can decrease the production of RBC in the bone marrow.</li>
        </div>
    </div>
</div>
<div class="split"></div>

<section class="dsa-why-choose" id="treatments">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;"><b>Rheumatoid Arthritis (Amavata)</b> Ayurvedic Treatment</h2>
                <li>Practicing yoga and exercise daily can help provide relief from the discomforting symptoms of rheumatoid arthritis.</li><br>
                <li>Regular massaging of the affected joints with Ayurvedic herbal oils can provide beneficial results. The Ayurvedic treatment approach, which comes under the Panchakarma therapy, is the first line of the method. Examples - <strong style="font-weight: bold;">Cow’s ghee, sesame, and lemongrass oils.</strong>.</li><br>
                <li>The Ayurvedic treatment procedure called Choorna Kizki is shown to provide positive results. In this, the mixture of natural herbs is applied to the patient's body until it sweats. It is the most recommended method for detoxifying the body.</li><br>
                <li>Anti-inflammatory herbs including ginger and turmeric are extensively recommended and are the components of herbal medicines that are prescribed by doctors as part of the treatment of rheumatoid arthritis. It significantly helps reduce inflammation and pain of the joints.</li>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <div id="dna_video">
                        <div class="youtube-player" data-id="w8UaP6U1H18" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                            <img src="<?php echo e(URL::asset('front/images/w8UaP6U1H18.webp')); ?>" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                            <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('<?php echo e(URL::asset('front/images/video-icon.png')); ?>') no-repeat;" title="Amavata (RA) Complete Ayurvedic Treatment | Rheumatoid Arthritis with Ayurveda">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="dsa-why-choose" id="testimonials">
    <div class="container-fluid">
        <h2 text align="center"style="font-size: 32px !important;">Kind Words of Rheumatoid Arthritis Patients -<strong>Dr. Sharda Ayurveda</strong></h2>
        <br>    
        
        <div class="row cut-row" >
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div id="dna_video">
                    <div class="youtube-player" data-id="OPjhs7F1U_Q" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                        <img src="<?php echo e(URL::asset('front/images/OPjhs7F1U_Q.webp')); ?>" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                        <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('<?php echo e(URL::asset('front/images/video-icon.png')); ?>') no-repeat;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div id="dna_video">
                    <div class="youtube-player" data-id="1xN6UVZ-6p8" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                        <img src="<?php echo e(URL::asset('front/images/1xN6UVZ-6p8.webp')); ?>" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                        <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('<?php echo e(URL::asset('front/images/video-icon.png')); ?>') no-repeat;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Patient's <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="<?php echo e(URL::asset('front/images/ashok-kumar.webp')); ?>" class="img-fluid" alt="Ashok Kumar RA Patient Testimonial" title="Ashok Kumar RA Patient Testimonial">
                            <h3 class="usrname">Ashok Kumar</h3>
                            <p class="desig">Rheumatoid Arthritis-RA Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was suffering from Rheumatoid Arthritis-RA. I took western medicines for more than 6 years but got no relief. A friend of mine told me about Dr. Sharda Ayurveda. He also told me that there you will find the best ayurvedic doctor for rheumatoid arthritis. At first, I did not believe him, but after consulting, there was a big change in my health. I am really happy that I took the treatment from here. I highly recommend this hospital to anyone who is facing any kind of health issue. Thankyou Dr. Sharda Ayurveda!
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="<?php echo e(URL::asset('front/images/patient-testimonial-female.webp')); ?>" class="img-fluid" alt="Harbhajan Kaur Testimonial" Title="Harbhajan Kaur Testimonial">
                            <h3 class="usrname">Harbhajan Kaur</h3>
                            <p class="desig">Rheumatoid Arthritis-RA Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I, Harbhajan Kaur was suffering from Rheumatoid Arthritis (RA) for the past 10 years. I fully relied upon painkillers for many years, but the sign of relief was still far away. Then I got to know about Dr. Sharda Ayurveda and visited their clinic. They got various tests done, and I was diagnosed with RA positive. In July 2019 I started with rheumatoid arthritis treatment in Ayurveda and today I am perfectly fine. Big thanks to Dr. Sharda Ayurveda!
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="<?php echo e(URL::asset('front/images/Mrs. Seema.webp')); ?>" class="img-fluid" alt="Mrs. Seema Testimonial" title="Mrs. Seema Testimonial">
                            <h3 class="usrname">Mrs. Seema</h3>
                            <p class="desig">Rheumatoid Arthritis-RA Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was suffering from Rheumatoid Arthritis-RA from the past 10 years. I took western medicines for more than 8 years but got no relief. Then I got to know about <a class="green-anchor"href="https://www.drshardaayurveda.com/">Dr. Sharda Ayurveda</a> and visited their Ludhiana clinic. They got my various blood investigation done and my rheumatoid arthritis was positive. In July 2019 I started with their course of treatment. I did not drop out taking my medicines. Then on 6th August 2020 I got my blood investigation done and my rheumatoid arthritis turned out to be negative. A big thanks to Dr. Sharda Ayurveda who developed a lost hope of mine to ever recover from rheumatoid arthritis, and finally I can say I am free from rheumatoid arthritis.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="<?php echo e(URL::asset('front/images/patient-testimonial-female.webp')); ?>" class="img-fluid" alt="Manjinder Kaur Testimonial" title="Manjinder Kaur Testimonial">
                            <h3 class="usrname">Manjinder Kaur</h3>
                            <p class="desig">Rheumatoid Arthritis-RA Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was suffering from Rheumatoid Arthritis for the past 9-10 years. Even after consulting various renowned specialists, still there was a health improvement. Randomly one day while scrolling my Facebook I got to know about Dr. Sharda's Ayurveda clinic. I took their Ayurvedic treatment of rheumatoid arthritis for approximately 8 months, and today I am fully recovered and satisfied with their treatment results. I would highly recommend everyone to visit Dr. Sharda Ayurveda.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>

    </div>
</section>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                Can Rheumatoid arthritis be cured by Ayurveda?
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            Yes, treatment of rheumatoid arthritis in Ayurveda can provide the best and most effective results. The holistic and authentic approach of Ayurveda is the safest and most reliable choice to get lifelong results. The Ayurvedic treatment for rheumatoid arthritis from Dr. Sharda Ayurveda has successfully healed thousands of patients naturally by just suggesting adopting a healthy diet and lifestyle regime along with taking herbal medications.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Which vitamins are good for rheumatoid arthritis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                            It is suggested that the levels of folic acid, calcium, and vitamin D strictly need to be monitored along with taking Ayurvedic treatment of rheumatoid arthritis for early results.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                What are the different stages of rheumatoid arthritis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            There are mostly 4 stages in RA
                            <ul>
                                <li>Stage 1- The initial stage of rheumatoid arthritis.</li>
                                <li>Stage 2- Antibodies develop in the body and swelling of a joint becomes severe.</li>
                                <li>Stage 3- Inflammation and pain in joints become hard to bear.</li>
                                <li>Stage 4- Joints become fused and deformity might occur.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                What time of day do the rheumatoid arthritis symptoms get worse?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                        <div class="card-body">
                            It gets worsens during the morning with extreme stiffness of the joints. Sometimes it becomes difficult to even perform daily tasks. In gout, pain is more in the evening but in fibromyalgia, it is intense after a poor night's sleep.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion2">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingFive" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <i class="fa" aria-hidden="true"></i>
                                    Can stress cause Rheumatoid arthritis?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion2">
                            <div class="card-body">
                                Yes, the children also get affected by rheumatoid arthritis. Childhood arthritis called juvenile arthritis is a serious disease that can cause permanent damage to the joints. Early diagnosis and timely Ayurvedic treatment of rheumatoid arthritis can minimize the chances of symptoms getting worst.
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSix" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                <i class="fa" aria-hidden="true"></i>
                                Which food items can worsen Rheumatoid arthritis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion2">
                        <div class="card-body">
                            The food items that can make the condition more chronic and worst are- Sugar, saturated fats, dairy products, Omega-6 fatty acids, Meat, refined, carbohydrates, Gluten, Aspartame, and Alcohol. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Can children get arthritis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion2">
                        <div class="card-body">
                            Yes, the children also get affected by rheumatoid arthritis. Childhood arthritis called juvenile arthritis is a serious disease that can cause permanent damage to the joints. Early diagnosis and timely Ayurvedic treatment can minimize the chances of symptoms getting worst. 
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                How can Rheumatoid arthritis be prevented?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion2">
                        <div class="card-body">
                            One can reduce the risk of getting rheumatoid arthritis by adopting a healthy daily regime.
                            <li>Keep your weight in check.</li>
                            <li>If you get a joint injury, get it checked timely.</li>
                            <li>Avoid excessive smoking and drinking.</li>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                How is Rheumatoid arthritis diagnosed?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion2">
                        <div class="card-body">
                            It is usually diagnosed by the patient medical history, blood investigation, and X-rays.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Should a person exercise if he/she is diagnosed with rheumatoid arthritis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion2">
                        <div class="card-body">
                            Yes, you can exercise, but first, consult your experts for better results. Regular exercising will help relieve arthritis pain and stiffness, therefore will provide energy.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $__env->make('../partials/frontend/blogs_post_section', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php echo $__env->make('../partials/frontend/form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Rheumatoid Arthritis",
    "item": "https://www.drshardaayurveda.com/rheumatoid-arthritis"  
  }]
}
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "Can Rheumatoid arthritis be cured by Ayurveda?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, Rheumatoid arthritis can be effectively cured with Ayurvedic treatment. The holistic and authentic approach of Ayurveda is the safest and most reliable choice to get lifelong results. The Ayurvedic treatment for rheumatoid arthritis from Dr. Sharda Ayurveda has successfully healed thousands of patients naturally by just suggesting adopting a healthy diet and lifestyle regime along with taking herbal medications."
    }
  },{
    "@type": "Question",
    "name": "Which vitamins are good for rheumatoid arthritis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It is suggested that the levels of folic acid, calcium, and vitamin D strictly need to be monitored along with the Ayurvedic treatment for early results."
    }
  },{
    "@type": "Question",
    "name": "What are the different stages of rheumatoid arthritis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "There are mostly 4 stages in RA
Stage 1- The initial stage of rheumatoid arthritis.
Stage 2- Antibodies develop in the body and swelling of a joint becomes severe. 
Stage 3- Inflammation and pain in joints become hard to bear. 
Stage 4- Joints become fused and deformity might occur."
    }
  },{
    "@type": "Question",
    "name": "What time of day do the rheumatoid arthritis symptoms get worse?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It gets worsens during the morning with extreme stiffness of the joints. Sometimes it becomes difficult to even perform daily tasks. In gout, pain is more in the evening but in fibromyalgia, it is intense after a poor night's sleep."
    }
  },{
    "@type": "Question",
    "name": "Can stress cause Rheumatoid arthritis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, stress is one of the main triggers of RA that affects the immune system of the body and there is the release of cytokines that lead to inflammation in joints. But the Ayurvedic treatment and the expert's suggestions can provide positive results."
    }
  },{
    "@type": "Question",
    "name": "Which food items can worsen Rheumatoid arthritis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The food items that can make the condition more chronic and worst are- 
Sugar, saturated fats, dairy products, Omega-6 fatty acids, Meat, refined, carbohydrates, Gluten, Aspartame, and Alcohol."
    }
  },{
    "@type": "Question",
    "name": "Can children get arthritis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, the children also get affected by rheumatoid arthritis. Childhood arthritis called juvenile arthritis is a serious disease that can cause permanent damage to the joints. Early diagnosis and timely Ayurvedic treatment can minimize the chances of symptoms getting worst."
    }
  },{
    "@type": "Question",
    "name": "How can Rheumatoid arthritis be prevented?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "One can reduce the risk of getting rheumatoid arthritis by adopting a healthy daily regime. 
Keep your weight in check.
If you get a joint injury, get it checked timely.
Avoid excessive smoking and drinking."
    }
  },{
    "@type": "Question",
    "name": "How is Rheumatoid arthritis diagnosed?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It is usually diagnosed by the patient medical history, blood investigation, and X-rays."
    }
  },{
    "@type": "Question",
    "name": "Should a person exercise if he/she is diagnosed with rheumatoid arthritis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, you can exercise, but first, consult your experts for better results. Regular exercising will help relieve arthritis pain and stiffness, therefore will provide energy."
    }
  }]
}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\drshardaayurveda\resources\views/frontend/services/rheumatoid_arthritis.blade.php ENDPATH**/ ?>