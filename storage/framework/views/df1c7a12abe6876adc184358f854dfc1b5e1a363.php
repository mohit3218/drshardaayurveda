<!DOCTYPE html>
<html lang="en-US">
    <head>
        <?php echo $__env->make('../partials/frontend/main-head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </head>
    <body>
        <?php echo $__env->make('../partials/frontend/main-header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="main-sec">
        <a style="cursor: pointer;z-index:99999;" data-toggle="modal" data-target=".bd-example-modal-xl" class="online-consult">ONLINE CONSULTATION</a>
            <?php echo $__env->yieldContent('content'); ?>
        </div>
        <?php echo $__env->make('../partials/frontend/main-footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </body>
</html><?php /**PATH D:\xampp\htdocs\drshardaayurveda\resources\views/layouts/main.blade.php ENDPATH**/ ?>