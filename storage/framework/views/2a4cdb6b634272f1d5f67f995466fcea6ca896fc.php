<?php
/**
 * Diabetes Page 
 * 
 * @created    08/04/2020
 * @package    Dr Sharda Ayurveda
 * @copyright  Copyright (C) 2020
 * @license    Proprietary
 * @author     Mohit Thakur
 */
?>

<?php $__env->startSection('content'); ?>
<section class="dsa-banner-sec2">
    <div class="container-fluid">
        <div class="row cust-dsa-banner">
            <div class="col-md-12" dalta-aos="zoom-in" data-aos-duration="4000">
                <div class="service">
                    <small>POWER TO HEAL</small>
                    <h3 class="ser-heading mt-4" >Ankylosing Spondylitis Ayurvedic Treatment</h3>
                    <p style="font-size: 18px;">Protect Your Joints with Timely Treatment</p>
                    <ul class="bread">
                        <li class="breadcrumb-item active" aria-current="page">Home&nbsp;<i class="fa fa-angle-double-right" aria-hidden="true"></i></li>
                        <li ><a href="https://www.drshardaayurveda.com/ankylosing-spondylitis">Ankylosing Spondylitis </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="container-fluid pt-5 pb-2">
    <div class="container">
        <div class="navi">
            <div class="col-lg-12">
                <ul class="serlisting">
                    <li>
                        <a href="#definition">DEFINITION <span class="sr-only">(current)</span></a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#causes">CAUSES</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#symptoms">SYMPTOMS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#treatment">TREATMENT</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#testimonials">TESTIMONIALS</a><span class="ln">&#124;</span>
                    </li>
                    <li>
                        <a href="#faqs">FAQ’S</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="dsa-why-choose" id="definition">
    <div class="container-fluid">
        <div class="row cut-row">
            <div class="col-xl-6 col-md-12 mb-5 col-sm-12 mb-2">
                <div class="left-img crc" data-aos="zoom-in" data-aos-duration="4000">
                    <img src="<?php echo e(URL::asset('front/images/Ankylosing-Spondylitis.webp')); ?>" class="why-choose-us img-fluid" alt="Ankylosing Spondylitis">
                </div>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12" data-aos="fade-up" data-aos-duration="4000">
                <h1 class="heading1">What is <b>Ankylosing Spondylitis?</b></h1>
                <p>
                    Ankylosis" means fused bones and "Spondylitis" refers to inflammation in spinal bones or vertebrae. It is a form of arthritis also known as Axial Spondyloarthritis. Chief complications are extreme pain & stiffness in the back and spinal Inflammation. As per Ayurveda, it is a disorder of “Amavata” (Prishtagata/ Katigata/ Grivagata) primarily caused by Vata Dosha and Kapha Dosha aggravation. Ankylosing spondylitis treatment in Ayurveda is recommended to prevent the radiation of pain to other parts like the shoulder, neck, hip, ribs, and small joints of the hands & feet. 
                    Usually, teens and people of 20+ age experience this due to poor diet, incorrect posture, exertion, or the presence of <a class="green-anchor"href="https://emedicine.medscape.com/article/1201027-overview" rel=nofollow>HLA-B27</a>. Bone growth and stiffness are witnessed due to excess calcium production around the bones. Sometimes the bones even get fused, thus responsible for a less flexible spine or a hunched back. 
                    <span id="dots" data-aos="fade-up" data-aos-duration="3000">....</span>
                </p>
                <span id="more-content">
                    <p>
                        In Ankylosing Spondylitis, the ethereal fibrocartilage gets affected which causes synovitis in bone and hence original cartilage gets replaced by bone through fusion. This burdensome condition is completely manageable through Ankylosing Spondylitis Ayurvedic treatment including herbal medications, dietary & lifestyle changes, pranayama, yoga, and meditation.
                    </p>
                    <div class="btn-ayurvedic">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
                </span>
                <button class="btn-gen mt-2" onclick="readMoreReadLess()" id="myBtn" data-aos="fade-up" data-aos-duration="3000">Read more</button>
            </div>
        </div>
    </div>
</section>

<section class="dsa-india-ayurvedic-sec our-expert" style="background-color:#f3f3f3" id="causes">
    <div class="container-fluid" >
        <div class="row cut-row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h2 class="heading1">Causes of <b>Ankylosing Spondylitis</b></h2>
                    <p font align="center" style="line-height:2">There are multiple causes resposible for Ankylosing Spondylitis disease in the early ages of human life, A few of the Specific causes of Ankylosing Spondylitis are mentioned below:</p>
                    <li>The presence of the HLA-B27 gene variant might be responsible for its occurrence.</li>
                    <li>Ankylosing spondylitis may also develop due to a weak immune system where the immunity cells start harming healthy tissues instead of bacteria, or foreign substances.</li>
                    <li>Low bone density due to a lack of nutrients, calcium, and vitamin D in food is the biggest factor contributing to ankylosing spondylitis.</li>
                    <li>Incorrect posture while sitting, standing, or lying could also result in stiffness and hamper the flexibility of joints.</li>
                    <li>It could also arise due to excessive work or lack of rest.</li>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container-strip" style="background-color: #eeb03f">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-4 col-sm-12">
                <p class="acc">Get a <strong>free Assessment</strong> Report</p>
            </div>
            <div class="col-lg-4 col-sm-12">
                <div class="btn-ayurvedic apn">
                    <button data-toggle="modal" data-target=".bd-example-modal-xl" class="btn-appointment">BOOK AN APPOINTMENT</button>
                </div>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
        </div>
    </div>
</div>

<div class="wrp pt-5 mt-5 pb-5" id="symptoms">
    <div class="container-fluid">
        <div class="kmoz" >
            <img src="<?php echo e(URL::asset('front/images/ASS-Symstoms.webp')); ?>" class="img-fluid"alt="infographic showing symptoms of irritable bowel syndrome" title="infographic showing symptoms of irritable bowel syndrome">
        </div>
        <div class="cvn sym" >
            <h3>Symptoms of <b>Ankylosing Spondylitis</b></h3>
            <li>Stiffness and pain in the lower body are witnessed especially in the early morning.</li>
            <li>Swelling and constant pain in fingers and toes.</li>
            <li>Soreness in the heels and arch of the foot.</li>
            <li>Patients often complain of tiredness all the time.</li>
            <li>Extreme body aches over the pelvis, shoulders, hips, knees, and regions between the spine and ribs.</li>
        </div>
    </div>
</div>

<div class="split"></div>

<section class="dsa-why-choose" id="treatments">
    <div class="container-fluid">
        <div class="row cut-row" >
            <div id="hgte2" class="col-xl-6 col-md-12 col-sm-12 " data-aos="fade-up" data-aos-duration="4000">
                <h2 class="client-test-title" style="font-size: 24px !important;">Ayurvedic treatment for <b>Ankylosing Spondylitis</b></h2>
                <p class="im3">
                    <img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="3.png">
                    Panchkarma manages Ankylosing Spondylitis with ancient healing practices. The body is rejuvenated and relief is ensured by eliminating the toxins from the body. 
                </p>
                <p class="im3">
                    <img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="3.png">
                    Ayurvedic herbs are a pivotal component of Ayurvedic treatment through which root-based improvements are ensured. 
                </p>
                <p class="im3">
                    <img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="3.png">
                    A healthy diet consisting of fresh fruits and vegetables is recommended.
                    A healthy diet will provide the body with strength and power, altogether making it capable to fight against several health issues. 
                </p>
                <p class="im3">
                    <img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="3.png">
                    According to Ayurveda, Yoga, and Meditation, can help in increasing flexibility and reducing stress levels. 
                </p>
                <p class="im3">
                    <img src="<?php echo e(URL::asset('front/images/2.png')); ?>" alt="3.png">
                    Pain and swelling can be alleviated through Patra Pottali Pinda Sweda (hot fomentation). Warmth may reduce inflammation and stiffness in muscles. 
                </p>
            </div>
            <div class="col-xl-6 col-md-12 col-sm-12">
                <div id="dna_video">
                    <div class="youtube-player" data-id="1xN6UVZ-6p8" data-related="0" data-control="0" data-info="0" data-fullscreen="0" style="width:100%;display: block; position: relative;cursor: pointer;max-height:360px;height:100%; overflow:hidden;padding-bottom:56.25%;margin:0 auto"> 
                        <img src="<?php echo e(URL::asset('front/images/1xN6UVZ-6p8.webp')); ?>" style="bottom: -100%; display: block; left: 0; margin: auto; max-width: 100%; width: 94%;height:auto; position: absolute; right: 0; top: -100%;">

                        <div style="height: 72px; width: 72px; left: 50%; top: 50%; margin-left: -36px; margin-top: -36px; position: absolute; background: url('<?php echo e(URL::asset('front/images/video-icon.png')); ?>') no-repeat;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="client-testimonial" data-aos="zoom-in" data-aos-duration="4000" id="testimonials">
    <div class="container-fluid">
        <div class="row">
            <div class="cl-test">
                <h2 class="client-test-title">Patient's <b>Testimonial</b></h2>
            </div>
        </div>
        <div class="row cust-test">
            <div class="col-md-12">
                <div id="client-test" class="owl-carousel owl-theme">
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="<?php echo e(URL::asset('front/images/patient-testimonial-female.webp')); ?>" class="img-fluid" alt="Pooja Gaur Ankylosing Spondylitis Patient Review" title="Pooja Gaur Ankylosing Spondylitis Patient Review">
                            <h3 class="usrname">Pooja Gaur</h3>
                            <p class="desig">Ankylosing Spondylitis Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Hello, My name is Pooja Gaur and I reside in Maharashtra. My life was totally disturbed by chronic ankylosing spondylitis. Even after consulting with several specialists, I got no improvement. One day I came to know about Dr. Sharda Ayurveda through Facebook. Due to my hectic schedule, I was not able to visit the clinic in person. So I consulted with the Ayurvedic doctors via a call and after knowing my problem they delivered Ayurvedic medicines for Ankylosing Spondylitis to my doorstep. I consumed them for almost 6 months and now I am absolutely fine. A huge thanks- Dr. Sharda Ayurveda.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="<?php echo e(URL::asset('front/images/patient-testimonial-male.webp')); ?>" class="img-fluid" alt="Sayed Ankylosing Spondylitis Patient Review" Title="Sayed Ankylosing Spondylitis Patient Review">
                            <h3 class="usrname">Sayed</h3>
                            <p class="desig">Ankylosing Spondylitis Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                Saying from my personal experience that Dr. Sharda Ayurveda has the best Ayurvedic doctors for ankylosing spondylitis. I suffered from Ankylosing Spondylitis for a chronic period. I consulted with several doctors and tried many medications but got no relief. One day I came to know about Dr. Sharda Ayurveda from my relatives. I visited the hospital and took treatment. Within 15 days, I started having improvements. My course of treatment lasted for almost 9 months. Now, I am completely fine and able to live my life to the fullest.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial-content">
                            <img src="<?php echo e(URL::asset('front/images/Mrs. Seema.webp')); ?>" class="img-fluid" alt="Sushil Rana Ankylosing Spondylitis Patient Review" title="Sushil Rana Ankylosing Spondylitis Patient Review">
                            <h3 class="usrname">Sushil Rana</h3>
                            <p class="desig">Ankylosing Spondylitis Patient</p>
                            <ul>
                                <li>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                    <span class="fa fa-star checked"></span>
                                </li>
                            </ul>
                            <p>
                                <span><i class="fa fa-quote-left" aria-hidden="true"></i></span>
                                I was suffering from back pain (Ankylosing Spondylitis) for the past 3 years. Even after consulting various renowned specialists still their was no sign of improvement. But thanks to Dr. Sharda Ayurveda that resolved all my back pain related health issues within just 3 months of Ayurvedic treatment.
                                <span><i class="fa fa-quote-right" aria-hidden="true"></i></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="our-product causes" id="faqs">
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12">
                <div class="prod-tp-content aos-init aos-animate" data-aos="zoom-in" data-aos-duration="4000">
                    <h3 class="heading1"><b>FAQ'S</b></h3>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <div id="accordion3">
                <div class="card">
                    <div class="card-header" id="headingOne" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">               <i class="fa" aria-hidden="true"></i>
                                What are Ankylosing Spondylitis pain areas? 
                            </button>
                        </h5>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion3">
                        <div class="card-body">
                            It has adverse effects on the shoulders, pelvis, hips, knees, or regions between the spine and ribs.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa" aria-hidden="true"></i>
                                Can you live a long life with ankylosing spondylitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion3">
                        <div class="card-body">
                            Almost all people with ankylosing spondylitis can expect to lead normal and productive lives. Despite the chronic nature of the illness, only a few people with Ankylosing spondylitis will become severely disabled.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa" aria-hidden="true"></i>
                                How long does Ankylosing Spondylitis swelling end?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion3">
                        <div class="card-body">
                           It depends upon the Prakriti of the patient and according to the personalized treatment, every individual needs a different time to heal with timely medications and proper precautions.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFour" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                <i class="fa" aria-hidden="true"></i>
                                Describe the pain associated with ankylosing spondylitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion3">
                        <div class="card-body">
                            Pain origin in the back from deep within the buttocks and is accompanied by morning stiffness. It worsens or gets better or stops completely at regular intervals.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingFive" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                <i class="fa" aria-hidden="true"></i>
                                Is heat or cold better for ankylosing spondylitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion3">
                        <div class="card-body">
                            Heat is good for pain and stiffness in joints and muscles. Hence hot fermentation helps patients to get relief from pain in the morning. Cold reduces inflammation of joints.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12">
            <div id="accordion4">
                <div class="card">
                    <div class="card">
                        <div class="card-header" id="headingSix" style="background-color:transparent;">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                    <i class="fa" aria-hidden="true"></i>
                                    Can ankylosing spondylitis cause bowel problems?
                                </button>
                            </h5>
                        </div>
                        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion4">
                            <div class="card-body">
                                Yes, it can develop bowel problems known as inflammatory bowel disease or colitis. Ayurvedic treatment for ankylosing spondylitis is best for a speedy recovery. 
                            </div>
                        </div>
                    </div>
                    <div class="card-header" id="headingSeven" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                <i class="fa" aria-hidden="true"></i>
                                Will I have to take medications for ankylosing spondylitis lifetime?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion4">
                        <div class="card-body">
                            The symptoms and the level of disease an individual suffers from depends on how long they have to keep taking Ayurvedic medicines. Ayurvedic medicines do take a little extra time to recover but treat disease from its root cause. Ankylosing spondylitis treatment clinic in India at Dr. Sharda Ayurveda is reliable and effective.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingEight" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                <i class="fa" aria-hidden="true"></i>
                                Does ankylosing spondylitis in pregnant women cause any complications to their baby?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion4">
                        <div class="card-body">
                            Ankylosing spondylitis will not harm a baby in anyways. However, a pregnant woman is advised to take Ayurvedic medicines to control her symptoms but only after consulting an Ayurvedic expert before.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingNine" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                                <i class="fa" aria-hidden="true"></i>
                                What dietary precautions should be taken in ankylosing spondylitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion4">
                        <div class="card-body">
                            Include more fruits and veggies in your daily diet. Add whole food and grains. 
                            avoid alcohol, sugar, sodium, and fat.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTen" style="background-color:transparent;">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                <i class="fa" aria-hidden="true"></i>
                                Does a sedentary lifestyle cause ankylosing spondylitis?
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion4">
                        <div class="card-body">
                            Yes, lack of physical activity, wrong posture, and prolonged sitting also cause ankylosing spondylitis.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>

<?php echo $__env->make('../partials/frontend/blogs_post_section', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php echo $__env->make('../partials/frontend/form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://www.drshardaayurveda.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "Ankylosing Spondylitis",
    "item": "https://www.drshardaayurveda.com/ankylosing-spondylitis"  
  }]
}
</script>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What is a serious complication of Ankylosing spondylitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It causes difficulty in breathing it reduced the ability to expand the chest and hence it causes Ankylosing spondylitis."
    }
  },{
    "@type": "Question",
    "name": "Can you live a long life with Ankylosing spondylitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Almost all people with Ankylosing spondylitis can expect to lead normal and productive lives. Despite the chronic nature of the illness, only a few people with Ankylosing spondylitis will become severely disabled."
    }
  },{
    "@type": "Question",
    "name": "Do I need to follow a special diet to cure Ankylosing Spondylitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it is very important to follow a healthy diet plan to cure such disease and reduce weight, carrying extra weight adds stress to joints."
    }
  },{
    "@type": "Question",
    "name": "Describe the pain associated with Ankylosing Spondylitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Pain origin in the back from deep within the buttocks and is accompanied by morning stiffness. It worsens or gets better or stops completely at regular intervals."
    }
  },{
    "@type": "Question",
    "name": "Is heat or cold better for Ankylosing Spondylitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Heat is good for pain and stiffness in joints and muscles. Hence hot fermentation helps patients to get relief from pain in the morning. Cold reduces inflammation of joints."
    }
  },{
    "@type": "Question",
    "name": "Can Ankylosing Spondylitis cause bowel problems?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it can develop bowel problems known as inflammatory bowel disease or colitis. Ayurvedic Treatment for Ankylosing Spondylitis is best for a speedy recovery."
    }
  },{
    "@type": "Question",
    "name": "Will I have to take medications for Ankylosing Spondylitis lifetime?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The symptoms and the level of disease an Individual suffers from depends on how long they have to keep taking Ayurvedic medicines. Ayurvedic medicines do take a little extra time to recover but treat disease from its root cause. Ankylosing Spondylitis treatment clinic in India at Dr. Sharda Ayurveda is reliable and effective."
    }
  },{
    "@type": "Question",
    "name": "Does Ankylosing Spondylitis in pregnant women cause any complications to their baby?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ankylosing Spondylitis will not harm a baby in anyways. However, a pregnant woman is advised to take Ayurvedic medicines to control her symptoms but only after consulting an Ayurvedic expert before."
    }
  },{
    "@type": "Question",
    "name": "What Dietary precautions should be taken in Ankylosing Spondylitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Include more Fruits and Veggies in your daily diet. Add whole food and grains. 
 Avoid Alcohol, Sugar, sodium, and fat."
    }
  },{
    "@type": "Question",
    "name": "Does a sedentary lifestyle cause Ankylosing Spondylitis?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, lack of physical activity, wrong posture, and prolonged sitting also cause Ankylosing Spondylitis."
    }
  }]
}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH D:\xampp\htdocs\drshardaayurveda\resources\views/frontend/services/ankylosing_spondylitis.blade.php ENDPATH**/ ?>