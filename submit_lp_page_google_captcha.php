<?php

session_destroy();
session_start();

if ($_REQUEST['action'] == 'user_mail') {
    $name = $_REQUEST['name'];
    $email = $_REQUEST['email'];
    $phone = $_REQUEST['mobile'];
    $page_url = $_REQUEST['page_url'];
    $city = $_REQUEST['city'];
    $country = $_REQUEST['country'];
    $sub = $_REQUEST['subject'];
    $desc = $_REQUEST['description'];


    // Google reCAPTCHA API keys settings  
    $secretKey     = '6LfEsOclAAAAAHsm5uF_rNDxIIMpDSYLDCJKWD5p';  //Local
    //$secretKey     = '6LfEsOclAAAAAHsm5uF_rNDxIIMpDSYLDCJKWD5p';  //Live
    
    
    // Assign default values 
    $valErr = $statusMsg = '';
    
    
    // Validate input fields  
    if(empty($name)){  
        $valErr .= 'Please enter your name.<br/>';  
    }  

    if(empty($phone)){  
        $valErr .= 'Please enter mobile number.<br/>';  
    }

    if ($phone != '') 
    {
        
        if(empty($valErr))
        {
            
            // Validate reCAPTCHA response  
            if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
            { 
                // Google reCAPTCHA verification API Request  
                $api_url = 'https://www.google.com/recaptcha/api/siteverify';  
                $resq_data = array(  
                    'secret' => $secretKey,  
                    'response' => $_POST['g-recaptcha-response'],  
                    'remoteip' => $_SERVER['REMOTE_ADDR']  
                );  
      
                $curlConfig = array(  
                    CURLOPT_URL => $api_url,  
                    CURLOPT_POST => true,  
                    CURLOPT_RETURNTRANSFER => true,  
                    CURLOPT_POSTFIELDS => $resq_data  
                );  
      
                $ch = curl_init();  
                curl_setopt_array($ch, $curlConfig);  
                $response = curl_exec($ch);  
                curl_close($ch);  
      
                // Decode JSON data of API response in array  
                $responseData = json_decode($response);
                
                if($responseData->success)
                { 
                    $from_email = 'digital.shardamedilifeayurveda@gmail.com';
                    //$from_email = 'dev032692@gmail.com';
            
                    $subject = $sub . ' Lead Enquiry';
                    $message_mail = "
                        <html>
                            <head>
                                <title>Booking Enquiry</title>
                            </head>
                            <body>
                                <p>
                                    Thanks for Showing Interest in our Services. One of Executive will contact you ASAP. Please feel free to call us on +91- 9876035500.
                                </p>
                                <table>
                                    <tr>
                                        <th>Name</th>
                                        <td>" . $name . "</td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td>" . $email . "</td>
                                    </tr>
                                    <tr>
                                        <th>Phone</th>
                                        <td>" . $phone . "</td>
                                    </tr>
                                    <tr>
                                        <th>City</th>
                                        <td>" .$city . "</td>
                                    </tr>
                                    <tr>
                                        <th>Country</th>
                                        <td>" . $country . "</td>
                                    </tr>
                                    <tr>
                                        <th>Lead Page</th>
                                        <td>" . $page_url . "</td>
                                    </tr>
                                     <tr>
                                        <th>Description</th>
                                        <td>" . $desc . "</td>
                                    </tr>
                                </table>
                            </body>
                        </html>";
            
                    // To send HTML mail, the Content-type header must be set
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
                    // Create email headers
                    $headers .= "From: " . $from_email . "\r\n" .
                            "Reply-To: " . $from_email . "\r\n" .
                            "X-Mailer: PHP/" . phpversion(); 
                    
                    $our_email = "digital.shardamedilifeayurveda@gmail.com";
                    //$our_email = "dev032692@gmail.com";
                    $to = $email;
                    $to1 = $our_email;
                    $send = mail($to, $subject, $message_mail, $headers);
                    
                    if ($send) {
                        $msg = 'Your Booking Enquiry Successfully Submit. We will contact you shortly';
                        
                    } else {
                        $msg = 'Your Booking Enquiry For Unsuccessfully! Please Fill Again Data';
                    }
                    
                    $send1 = mail($to1, $subject, $message_mail, $headers);
                    
                    if ($send1) {
                        $msg = 'Your Booking Enquiry Successfully Submit. We will contact you shortly';
                        
                    } else {
                        $msg = 'Your Booking For Unsuccessfully! Please Fill Again Data';
                    }
                    
                    $retrun_url = $_REQUEST['return_url'];
                    header("location:$retrun_url");
                    //header("location:https://www.drshardaayurveda.com/");
                }
                else
                {  
                    $statusMsg = 'The reCAPTCHA verification failed, please try again.'; 
                    $_SESSION['statusMsg']   = $statusMsg;
                    header("location:".$page_url."?error=".$statusMsg);
                }
            }
            else{
                $statusMsg = 'Something went wrong, please try again.';  
                $_SESSION['statusMsg']   = $statusMsg;
                header("location:".$page_url."?error=".$statusMsg);
            }  
        }
        else{  
            $valErr = !empty($valErr)?'<br/>'.trim($valErr, '<br/>'):'';  
            $statusMsg = 'Please fill all the mandatory fields:'.$valErr;  
           // header("location:$retrun_url/?$statusMsg");
           $_SESSION['statusMsg']   = $statusMsg;
            header("location:".$page_url."?error=".$statusMsg);
        }
    }
    else{  
        $statusMsg = 'Please fill all the mandatory fields:'.$valErr; 
        $_SESSION['statusMsg']   = $statusMsg;
        header("location:".$page_url."?error=".$statusMsg);
    }
}
?>