<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::any('/refresh_captcha', [App\Http\Controllers\HomeController::class, 'refreshCaptcha'])->name('refresh_captcha');
Route::any('/lead-submission', [App\Http\Controllers\HomeController::class, 'lead_submission'])->name('lead-submission');


Route::get('/insomnia', [App\Http\Controllers\HomeController::class, 'insomnia'])->name('insomnia');
Route::get('/anxiety-disorder', [App\Http\Controllers\HomeController::class, 'anxiety_disorder'])->name('anxiety-disorder');
Route::get('/migraine', [App\Http\Controllers\HomeController::class, 'migraine'])->name('migraine');

Route::get('/rhinitis', [App\Http\Controllers\HomeController::class, 'rhinitis'])->name('rhinitis');


Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/about-us', [App\Http\Controllers\HomeController::class, 'about_us'])->name('about-us');
Route::get('/dr-mukesh-sharda', [App\Http\Controllers\HomeController::class, 'dr_mukesh_sharda'])->name('dr-mukesh-sharda');
Route::get('/dr-arjun-sharda', [App\Http\Controllers\HomeController::class, 'dr_arjun_sharda'])->name('dr-arjun-sharda');
Route::get('/contact-us', [App\Http\Controllers\HomeController::class, 'contact_us'])->name('contact-us');
Route::get('/ayurvedic-treatments', [App\Http\Controllers\HomeController::class, 'services'])->name('ayurvedic-treatments');
//Endocrine
Route::get('/endocrine', [App\Http\Controllers\HomeController::class, 'endocrine'])->name('endocrine');
Route::get('/obesity', [App\Http\Controllers\HomeController::class, 'obesity'])->name('obesity');
Route::get('/asthma', [App\Http\Controllers\HomeController::class, 'asthma'])->name('asthma');
Route::get('/diabetes', [App\Http\Controllers\HomeController::class, 'diabetes'])->name('diabetes');
Route::get('/constipation', [App\Http\Controllers\HomeController::class, 'constipation'])->name('constipation');
Route::get('/thyroid', [App\Http\Controllers\HomeController::class, 'thyroid'])->name('thyroid');
Route::get('/rheumatoid-arthritis', [App\Http\Controllers\HomeController::class, 'rheumatoid_arthritis'])->name('rheumatoid-arthritis');
Route::get('/gout', [App\Http\Controllers\HomeController::class, 'gout'])->name('gout');
Route::get('/osteoarthritis', [App\Http\Controllers\HomeController::class, 'osteoarthritis'])->name('osteoarthritis');
Route::get('/psoriatic-arthritis', [App\Http\Controllers\HomeController::class, 'psoriatic_arthritis'])->name('psoriatic-arthritis');
Route::get('/erectile-dysfunction', [App\Http\Controllers\HomeController::class, 'erectile_dysfunction'])->name('erectile-dysfunction');
Route::get('/oligospermia', [App\Http\Controllers\HomeController::class, 'oligospermia'])->name('oligospermia');
Route::get('/neck-pain', [App\Http\Controllers\HomeController::class, 'neck_pain'])->name('neck-pain');

Route::get('/chronic-fatigue-syndrome', [App\Http\Controllers\HomeController::class, 'chronic_fatigue_syndrome'])->name('chronic-fatigue-syndrome');
Route::get('/reactive-arthritis', [App\Http\Controllers\HomeController::class, 'reactive_arthritis'])->name('reactive-arthritis');
Route::get('/ankylosing-spondylitis', [App\Http\Controllers\HomeController::class, 'ankylosing_spondylitis'])->name('ankylosing-spondylitis');

//Eczema
Route::get('/eczema/types-of-eczema', [App\Http\Controllers\HomeController::class, 'types_of_eczema'])->name('types-of-eczema');
Route::get('/eczema/contact-dermatitis', [App\Http\Controllers\HomeController::class, 'contact_dermatitis'])->name('contact-dermatitis');
Route::get('/eczema/stasis-dermatitis', [App\Http\Controllers\HomeController::class, 'stasis_dermatitis'])->name('stasis-dermatitis');
Route::get('/psoriasis/inverse-psoriasis-treatment', [App\Http\Controllers\HomeController::class, 'inverse_psoriasis_treatment'])->name('inverse-psoriasis-treatment');

//Digestive
Route::get('/digestive', [App\Http\Controllers\HomeController::class, 'digestive'])->name('digestive');
Route::get('/acidity', [App\Http\Controllers\HomeController::class, 'acidity'])->name('acidity');
Route::get('/ibs', [App\Http\Controllers\HomeController::class, 'ibs'])->name('ibs');
Route::get('/constipation', [App\Http\Controllers\HomeController::class, 'constipation'])->name('constipation');
Route::get('/anorexia-nervosa', [App\Http\Controllers\HomeController::class, 'anorexia_nervosa'])->name('anorexia-nervosa');
Route::get('/abdominal-pain', [App\Http\Controllers\HomeController::class, 'abdominal_pain'])->name('abdominal-pain');
Route::get('/piles', [App\Http\Controllers\HomeController::class, 'piles'])->name('piles');
Route::get('/ulcerative-colitis', [App\Http\Controllers\HomeController::class, 'ulcerative_colitis'])->name('ulcerative-colitis');
Route::get('/flatulence', [App\Http\Controllers\HomeController::class, 'flatulence'])->name('flatulence');

//Hair
Route::get('/hair', [App\Http\Controllers\HomeController::class, 'hair'])->name('hair');
Route::get('/hair-fall', [App\Http\Controllers\HomeController::class, 'hair_fall'])->name('hair-fall');
Route::get('/dandruff', [App\Http\Controllers\HomeController::class, 'dandruff'])->name('dandruff');

//Skin
Route::get('/skin', [App\Http\Controllers\HomeController::class, 'skin'])->name('skin');
Route::get('/hair', [App\Http\Controllers\HomeController::class, 'hair'])->name('hair');
Route::get('/vitiligo', [App\Http\Controllers\HomeController::class, 'vitiligo'])->name('vitiligo');
Route::get('/hives', [App\Http\Controllers\HomeController::class, 'hives'])->name('hives');
Route::get('/psoriasis', [App\Http\Controllers\HomeController::class, 'psoriasis'])->name('psoriasis');
Route::get('/eczema', [App\Http\Controllers\HomeController::class, 'eczema'])->name('eczema');
Route::get('/tinea', [App\Http\Controllers\HomeController::class, 'tinea'])->name('tinea');
Route::get('/fungal-infection', [App\Http\Controllers\HomeController::class, 'fungal_infection'])->name('fungal-infection');
Route::get('/eczema', [App\Http\Controllers\HomeController::class, 'eczema'])->name('eczema');


//Joint Pain
Route::get('/hair', [App\Http\Controllers\HomeController::class, 'hair'])->name('hair');
Route::get('/cervical', [App\Http\Controllers\HomeController::class, 'cervical_sponylosis'])->name('cervical');
Route::get('/back-pain', [App\Http\Controllers\HomeController::class, 'back_pain'])->name('back-pain');
Route::get('/fibromyalgia', [App\Http\Controllers\HomeController::class, 'fibro_myalagia'])->name('fibromyalgia');
Route::get('/lumbar-spondylosis', [App\Http\Controllers\HomeController::class, 'lumbar_spondylosis'])->name('lumbar-spondylosis');
Route::get('/osteoporosis', [App\Http\Controllers\HomeController::class, 'osteoporosis'])->name('osteoporosis');
Route::get('/uric-acid', [App\Http\Controllers\HomeController::class, 'uric_acid'])->name('uric-acid');
Route::get('/premature-ejaculation', [App\Http\Controllers\HomeController::class, 'pre_mature_ejaculation'])->name('premature-ejaculation');
Route::get('/hip-joint-pain', [App\Http\Controllers\HomeController::class, 'hip_joint_pain'])->name('hip-joint-pain');
Route::get('/knee-pain', [App\Http\Controllers\HomeController::class, 'knee_pain'])->name('knee-pain');
Route::get('/post-viral-arthritis', [App\Http\Controllers\HomeController::class, 'post_viral_arthritis'])->name('post-viral-arthritis');
//Book Appointment
Route::get('/book-appointment', [App\Http\Controllers\HomeController::class, 'book_appointment'])->name('book-appointment');
//Gallery
Route::get('/gallery', [App\Http\Controllers\HomeController::class, 'gallery'])->name('gallery');
//Blog
Route::get('/blog', [App\Http\Controllers\HomeController::class, 'blog'])->name('blog');

//Circulatory
Route::get('/anemia', [App\Http\Controllers\HomeController::class, 'anemia'])->name('anemia');
Route::get('/blood-pressure', [App\Http\Controllers\HomeController::class, 'blood_pressure'])->name('blood-pressure');
Route::get('/high-cholesterol', [App\Http\Controllers\HomeController::class, 'high_cholestrol'])->name('high-cholesterol');
Route::get('/coronary-artery-disease', [App\Http\Controllers\HomeController::class, 'coronary_artery_disease'])->name('coronary-artery-disease');
//Kidney
Route::get('/kidney-failure', [App\Http\Controllers\HomeController::class, 'kidney_failure'])->name('kidney-failure');
Route::get('/kidney-stones', [App\Http\Controllers\HomeController::class, 'kidney_stones'])->name('kidney-stones');
//Neurological
Route::get('/sciatica-pain', [App\Http\Controllers\HomeController::class, 'sciatica_pain'])->name('sciatica-pain');
Route::get('/depression', [App\Http\Controllers\HomeController::class, 'depression'])->name('depression');
//Gynae
Route::get('/menopause', [App\Http\Controllers\HomeController::class, 'menopause'])->name('menopause');
Route::get('/leucorrhoea', [App\Http\Controllers\HomeController::class, 'leucorrhoea'])->name('leucorrhoea');
Route::get('/pcod-pcos', [App\Http\Controllers\HomeController::class, 'pcod_pcos'])->name('pcod-pcos');
//Respiratory
Route::get('/sinusitis', [App\Http\Controllers\HomeController::class, 'sinusitis'])->name('sinusitis');
Route::get('/chronic-laryngitis', [App\Http\Controllers\HomeController::class, 'chronic_laryngitis'])->name('chronic-laryngitis');

//De Addiction
Route::get('/alcohol-de-addiction', [App\Http\Controllers\HomeController::class, 'de_aaddiction_alcohol'])->name('alcohol-de-addiction');
Route::get('/drug-de-addiction', [App\Http\Controllers\HomeController::class, 'drug_de_addiction'])->name('drug-de-addiction');
Route::get('/smoking-de-addiction', [App\Http\Controllers\HomeController::class, 'smoking'])->name('smoking-de-addiction');
Route::get('/tobacco', [App\Http\Controllers\HomeController::class, 'tobacco'])->name('tobacco');

//Liver
Route::get('/fatty-liver', [App\Http\Controllers\HomeController::class, 'fatty_liver'])->name('fatty-liver');
Route::get('/jaundice', [App\Http\Controllers\HomeController::class, 'jaundice'])->name('jaundice');
Route::get('/liver-cirrhosis', [App\Http\Controllers\HomeController::class, 'liver_cirrhosis'])->name('liver-cirrhosis');
//UTI
Route::get('/urinary-tract-infection', [App\Http\Controllers\HomeController::class, 'urinary_tract_infection'])->name('urinary-tract-infection');
Route::get('/cystitis', [App\Http\Controllers\HomeController::class, 'cystitis'])->name('cystitis');
//Privacy Policy
Route::get('/privacy-policy', [App\Http\Controllers\HomeController::class, 'privacy_policy'])->name('privacy-policy');
Route::get('/shipping-policy', [App\Http\Controllers\HomeController::class, 'shipping_policy'])->name('shipping-policy');
//Locations
Route::get('/locations/ludhiana', [App\Http\Controllers\HomeController::class, 'ludhiana'])->name('ludhiana');
Route::get('/locations/mohali', [App\Http\Controllers\HomeController::class, 'mohali'])->name('mohali');
Route::get('/locations/muktsar', [App\Http\Controllers\HomeController::class, 'muktsar'])->name('muktsar');
Route::get('/locations/bathinda', [App\Http\Controllers\HomeController::class, 'bathinda'])->name('bathinda');

Route::get('/testimonial', [App\Http\Controllers\HomeController::class, 'success_stories'])->name('testimonial');

Route::get('/ayurvedic-online-consultation', [App\Http\Controllers\HomeController::class, 'consultation'])->name('ayurvedic-online-consultation');

//Blogs
Route::get('/blogs', [App\Http\Controllers\HomeController::class, 'blogs'])->name('blogs');
Route::get('/search', [App\Http\Controllers\HomeController::class, 'search'])->name('search');
//Author Page
Route::get('/author/dr-mukesh', [App\Http\Controllers\HomeController::class, 'dr_mukesh_sharda_author'])->name('dr-mukesh');
Route::get('/author/dr-sharda-ayurveda', [App\Http\Controllers\HomeController::class, 'dr_sharda_author'])->name('dr-sharda-ayurveda');

Route::get('/about-us/important-disclaimer', [App\Http\Controllers\HomeController::class, 'important_disclaimer'])->name('important-disclaimer');
Route::get('/about-us/dosha-test', [App\Http\Controllers\HomeController::class, 'dosha_test'])->name('dosha-test');

Route::get('/blogs/{post}', [App\Http\Controllers\HomeController::class, 'post']);

Route::get('/blogs/category/{category}', [App\Http\Controllers\HomeController::class, 'category_blogs']);

Route::get('/wellness-program/', [App\Http\Controllers\HomeController::class, 'wellness_program'])->name('wellness-program');
Route::post('razorpay-payment',[App\Http\Controllers\HomeController::class,'razorpay_payment'])->name('razorpay-payment');

Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function() {
    //Page routes
    Route::resource('pages', App\Http\Controllers\PageController::class);
    Route::resource('posts', App\Http\Controllers\PostController::class);
    Route::post('posts/upload', 'PostController@upload')->name('posts.upload');
});

Route::any('/ckfinder/connector', '\CKSource\CKFinderBridge\Controller\CKFinderController@requestAction')
    ->name('ckfinder_connector');

Route::any('/ckfinder/browser', '\CKSource\CKFinderBridge\Controller\CKFinderController@browserAction')
    ->name('ckfinder_browser');

Route::get('/logout', [App\Http\Controllers\PageController::class, 'logout'])->name('logout');